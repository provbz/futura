INSERT INTO `dictionary` (`group`, `key`, `value`, `order`) VALUES ('e_model_unita_oraria', '50', '50 minuti', '0');
INSERT INTO `dictionary` (`group`, `key`, `value`, `order`) VALUES ('e_model_unita_oraria', '60', '60 minuti', '1');
INSERT INTO `dictionary` (`group`, `key`, `value`, `order`) VALUES ('e_model_unita_oraria', '50-60', 'miste 50-60 minuti ', '2');
INSERT INTO `dictionary` (`group`, `key`, `value`, `order`) VALUES ('e_model_unita_oraria', 'altro', 'Altro', '3');

alter table e_model
add unita_oraria text;

alter table e_model
add pluriclasse bit default 0,
add alunni_per_classe int default 0;

ALTER TABLE `futurabolzano-it`.`e_model` 
CHANGE COLUMN `pluriclasse` `pluriclasse` INT NULL DEFAULT b'0' ;
