CREATE TABLE `rilevazione_cattedre` (
  `rilevazione_cattedre_id` INT NOT NULL AUTO_INCREMENT,
  `structure_id` INT NULL,
  `insert_user_id` INT NULL,
  `insert_date` DATETIME NULL,
  `last_edit_user_id` INT NULL,
  `last_edit_date` DATETIME NULL,
  `school_year_id` INT NULL,
  `numero_alunni_no_italiano` INT NULL,
  `numero_alunni_sostegno_linguistico` INT NULL,
  `uso_risorse_proprie` INT(1) NULL,
  `dettaglio_risorse` TEXT NULL,
  PRIMARY KEY (`rilevazione_cattedre_id`),
  INDEX `fk_rc_structure_idx` (`structure_id` ASC) VISIBLE,
  INDEX `fk_rc_school_year_idx` (`school_year_id` ASC) VISIBLE,
  INDEX `fk_rc_insert_user_idx` (`insert_user_id` ASC) VISIBLE,
  INDEX `fk_rc_edit_user_idx` (`last_edit_user_id` ASC) VISIBLE,
  CONSTRAINT `fk_rc_structure`
    FOREIGN KEY (`structure_id`)
    REFERENCES `structure` (`structure_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rc_school_year`
    FOREIGN KEY (`school_year_id`)
    REFERENCES `school_year` (`school_year_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rc_insert_user`
    FOREIGN KEY (`insert_user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rc_edit_user`
    FOREIGN KEY (`last_edit_user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE);
