CREATE TABLE `user_year_document_import` (
  `user_year_document_import_id` INT NOT NULL AUTO_INCREMENT,
  `user_year_document_id` INT NULL,
  `date` DATETIME NULL,
  `edit_user_id` INT NULL,
  `from_class` VARCHAR(45) NULL,
  `to_class` VARCHAR(45) NULL,
  PRIMARY KEY (`user_year_document_import_id`),
  INDEX `uydi_uyd_idx` (`user_year_document_id` ASC) VISIBLE,
  INDEX `uydi_eu_idx` (`edit_user_id` ASC) VISIBLE,
  CONSTRAINT `uydi_uyd`
    FOREIGN KEY (`user_year_document_id`)
    REFERENCES `user_year_document` (`user_year_document_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `uydi_eu`
    FOREIGN KEY (`edit_user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE);

alter table user_year_document_import
add column school_year_id int;
