

alter table structure
add meccanografico nvarchar(100);

ALTER TABLE `user` 
ADD COLUMN `token` VARCHAR(45) NULL AFTER `birth_date`,
ADD COLUMN `token_date` DATETIME NULL AFTER `token`;

ALTER TABLE `user` 
ADD COLUMN `api_key` VARCHAR(45) NULL AFTER `token_date`;

ALTER TABLE `user` 
DROP COLUMN `tocken`;

update `user`
set api_key='1a17d8d7-591c-430a-b6ed-feb7da2081dd'
where user_id=25321;
