### 1.7 (2024-12-31)

* Migrazione a PHP 8.2

* Integrazione docker

* Mondo delle parole: 
    * esportazione excel statistiche
    * allegato per prove non eseguite

* Referenti: aggiunti nuovi ruoli

* Modello E: nuove evolutive, gestione anno scolastico, chiusura separata secondaria

* Cattedre A023ter: nuova sezione

* PEI-PDP: 
    * nuove funzioni del pannello amministrativo
    + blocco documenti codici unificati

* Anno di prova: nuova struttura, possibilità di caricare i materiali da pannello amministrativo


### 1.6 (2023-12-31)

* Mondo delle parole: 
    * nuovi grafici statistiche
    * gestione trasferimenti locale alla sezione
    * nuovi permessi

* PDP background migratorio: nuova sezione

* PEI di tirocinio: nuova sezione

* Anno di prova: nuova sezione materiali, revisione struttura


### 1.5 (2022-12-31)

* Integrazione componenti di interfaccia VueJs

* Supporto all'integrazione di piattaforme esterne con l'introduzione di nuove API per gestire un approccio SSO interno.

* Evolutive Modello E

* Mondo delle parole: allegati alla restituzione, evolutive ai test

* PEI-PDP: import avanzato da anni precedenti con selezione multipla, PCTO, 

* Drop Out: revisione statistiche


### 1.3 (2021-12-31)

* Gestione refereinti di plessi e istituti

* Sistema di messaggistica interna

* Gestione segnalazione Modello E


### 1.2 (2020-12-31)

* Progetto Mondo delle parole e Letto-Scrittura: caricamento dei dai dei test, valutazione e analisi dei dati


### 1.1 (2019-01-14)

* Gestione Portfolio Docenti in anno di prova

* Gestione progetti Educazione alla Salute


### 1.0 (2018-09-13)

* Rilascio delle funzioni legate alla gestione PEI-PDP
