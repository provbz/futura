<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of Scheduler
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class SomministrazioniInfanziaTrigger implements ITrigger{

    public function execute(){
        $this->creaSomministrazione(TestPeriodo::INIZIALE);
        $this->creaSomministrazione(TestPeriodo::FINALE);
    }

    private function creaSomministrazione($periodoDictionaryValue){
        $currentYear = SchoolYearService::findCurrentYear();
        if ($currentYear == null){
            return;
        }

        $somministrazione = SomministrazioneService::findForYearAndGradeAndPeriodo($currentYear["school_year_id"], ClassDictionaryValue::C_3I, $periodoDictionaryValue);
        if ($somministrazione != null){
            return;
        }

        $syc = explode("/", $currentYear["school_year"]);

        $dateFrom = $syc[0] . "/11/01";
        $dateTo = $syc[0] . "/11/30";
        if ($periodoDictionaryValue == TestPeriodo::FINALE){
            $dateFrom = $syc[1] . "/04/15";
            $dateTo = $syc[1] . "/05/15";    
        }

        $somministrazioneId = EM::insertEntity("somministrazione", [
            "school_year_id" => $currentYear["school_year_id"],
            "data_inizio" => $dateFrom,
            "data_fine" => $dateTo,
            "class_dictionary_value" => ClassDictionaryValue::C_3I,
            "periodo_dictionary_value" => $periodoDictionaryValue,
            "insert_date" => "NOW()",
            "last_edit_date" => "NOW()",
            "row_status" => RowStatus::READY
        ]);

        $tests = TestService::findAllForClasseAndPeriodo(ClassDictionaryValue::C_3I, $periodoDictionaryValue);
        foreach ($tests as $test) {
            $somministrazioneTestId = EM::insertEntity("somministrazione_test", [
                "somministrazione_id" => $somministrazioneId,
                "test_id" => $test["test_id"],
                "insert_date" => "NOW()",
                "last_edit_date" => "NOW()",
                "row_status" => RowStatus::READY
            ]);

            $testFile = TestService::loadTest($test["code"]);
            if ($testFile == null){
                continue;
            }

            $variables = $testFile->getVariablesNames();
            foreach ($variables as $variable) {
                $soglia = TestSogliaService::findInUseForTestAndVariableName($test["test_id"], $variable["name"]);
                EM::insertEntity("somministrazione_test_soglia", [
                    "somministrazione_test_id" => $somministrazioneTestId,
                    "test_soglia_id" => $soglia["test_soglia_id"],
                    "insert_date" => "NOW()",
                    "last_edit_date" => "NOW()"
                ]);
            }
        }
    }

}