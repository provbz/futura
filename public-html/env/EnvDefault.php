<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EndDefault
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

Constants::$devMode = false;
Constants::$errorReportingEnabled = true;

//DB
Constants::$db_user_name = '';
Constants::$db_password = '';
Constants::$db_host = '';
Constants::$db_schema = '';
    
//encrypt Key
Constants::$encrypt_key = "";
Constants::$encript_iv = "";
    
//Path/URL
Constants::$APP_PROTOCOL = "";
Constants::$APP_URL = "";
Constants::$APP_PRINT_NAME = "";
Constants::$app_name = "";
Constants::$ATTACHMENTS_DIR = "";
    
//SMTP
Constants::$SMTP_FROM_NAME = "";
Constants::$SMTP_FROM_MAIL = "";
Constants::$SMTP_HOST = "";
Constants::$SMTP_PORT = 587;
Constants::$SMTP_MODE = 'tls';
Constants::$SMTP_USERNAME = "";
Constants::$SMTP_PASSWORD = "";
    
Constants::$maxSeenTimeDelay = 120;
Constants::$onlineInterval = 60;
Constants::$defaultActionMethod = "_default";
        
Constants::$cookieSectret = "";

Office365Constants::$CLIENT_ID = '';
Office365Constants::$CLIENT_SECRET = '';
Office365Constants::$REDIRECT_URI = '';
Office365Constants::$AUTHORITY_URL = '';
Office365Constants::$AUTHORIZE_ENDPOINT = '';
Office365Constants::$TOKEN_ENDPOINT = '';
Office365Constants::$RESOURCE_ID = '';
Office365Constants::$SCOPES= '';

RecaptchaConstants::$CLIENT_SECRET = '';
RecaptchaConstants::$SERVER_SECRET = '';