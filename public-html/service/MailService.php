<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class MailService {
    
    private static $sender = "MailService";
    
    public static function persist($message){
        $message["insert_date"] = "NOW()";
        $message["type"] = NoticeUserChannel::EMAIL;
        $message["count_attempts"] = 0;
        $message["status"] = MessageStatus::NEW_;
        EM::insertEntity("message", $message);
    }

    public static function sendMessages(){
        $messages = self::findWaitingOrErrors();
        foreach ($messages as $message) {
            $user = UserService::findUserByEmail($message["to"]);
            if ($user != null && $user["status"] != UserStatus::ENABLED){
                continue;
            }

            try{
                $status = self::send($message);
                $message["status"] = $status;
                $message["count_attempts"] = $message["count_attempts"] + 1;

                EM::replaceEntity("message", $message);
            } catch (Exception $ex){
                //LogService::info(self::$sender, "error sending message ". $message["message_id"], $ex);
            }
        }
    }

    public static function findWaitingOrErrors(){
        return EM::execQuery("SELECT * 
                FROM message 
                WHERE status=:new OR (status=:error AND count_attempts<:max_count_attempts) 
                ORDER BY message_id", [
            ":new" => MessageStatus::NEW_,
            ":error" => MessageStatus::ERROR,
            ":max_count_attempts" => 5
        ]);
    }
    
    public static function send($message){
        if (StringUtils::isBlank($message['to'])){
            return MessageStatus::SKIPPED;
        }

        if (Constants::$ALLOWED_EMAIL_ADDRESSES != null && 
                array_search(strtolower($message['to']), Constants::$ALLOWED_EMAIL_ADDRESSES) === false){
            return MessageStatus::SKIPPED;
        }
        
        try{
            $mail = new PHPMailer\PHPMailer\PHPMailer();
            $mail->IsSMTP();          
            $mail->SMTPDebug = PHPMailer\PHPMailer\SMTP::DEBUG_OFF;
            $mail->CharSet = 'UTF-8';
            $mail->Host = Constants::$SMTP_HOST;
            $mail->Port = Constants::$SMTP_PORT;

            if (StringUtils::isNotBlank(Constants::$SMTP_USERNAME)){
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = Constants::$SMTP_MODE;
                $mail->Username = Constants::$SMTP_USERNAME;
                $mail->Password = Constants::$SMTP_PASSWORD;
            } else {
                $mail->SMTPAuth = false;
                $mail->SMTPAutoTLS = false; 
            }

            $mail->SetFrom(Constants::$SMTP_FROM_MAIL, Constants::$SMTP_FROM_NAME);
            $mail->AddAddress(strtolower($message['to']));

            $mail->IsHTML(true);
            $mail->Subject = $message['subject'];
            $mail->Body    = $message['message'];

            if(!$mail->Send()){
                echo $mail->ErrorInfo;
                //LogService::error_(self::$sender, "Error sending mail " . $mail->ErrorInfo);
                return MessageStatus::ERROR;
            } 
            
            return MessageStatus::SENDED;
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }
}
