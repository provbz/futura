<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of SchoolYearService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SchoolYearService {
    
    public static $sender = "SchoolYearService";
    
    public static function addNextSchoolYear(){
        $year = date("Y");
        $schoolYear = $year . '/' . ($year + 1);
        $dbSchoolYear = SchoolYearService::findByName($schoolYear);
        if ($dbSchoolYear == NULL){
            SchoolYearService::persist($schoolYear, 0);
            LogService::info(self::$sender, "Next school year added");
        }
    }
    
    public static function updateCurrentSchoolYear(){
        $year = date("Y");
        $month = date("m");
        if ($month < 7){
            return;
        }
        
        $schoolYear = $year . '/' . ($year + 1);
        $dbSchoolYear = SchoolYearService::findByName($schoolYear);
        if ($dbSchoolYear == NULL){
            SchoolYearService::persist($schoolYear, 1);
        } else {
            EM::updateEntity("school_year", ["current" => 0], ["current" => 1]);
            EM::updateEntity("school_year", ["current" => 1], ["school_year_id" => $dbSchoolYear['school_year_id']]);    
        }
        
        LogService::info(self::$sender, "Update current school year");
    }
    
    public static function findCurrentYear(){
        $query = "SELECT * FROM school_year WHERE current=1";
        return EM::execQuerySingleResult($query);
    }

    public static function findPrevYear(){
        $query = "SELECT *
            from school_year
            order by school_year_id desc
            limit 1, 1";
        return EM::execQuerySingleResult($query);
    }
    
    public static function findByName($schoolYear){
        $query = "SELECT * FROM school_year WHERE school_year=:school_year";
        return EM::execQuerySingleResult($query, [
            "school_year" => $schoolYear
        ]);
    }
    
    public static function persist($schoolYear, $current){
        if ($current == 1){
            EM::updateEntity("school_year", ["current" => 0], ["current" => 1]);
        }
        
        return EM::insertEntity("school_year", [
            "school_year" => $schoolYear,
            "current" => $current
        ]);
    }
    
    public static function findAllAsObject(){
        $stmt = self::findAll();
        $obj = [];
        foreach ($stmt as $row){
            $obj[$row['school_year_id']] = $row['school_year'];
        }
        return $obj;
    }

    public static function find($id) {
        return EM::execQuerySingleResult("SELECT * FROM school_year WHERE school_year_id=:id", [
            "id" => $id
        ]);
    }

    public static function findAll() {
        return EM::execQuery("SELECT * FROM school_year ORDER By school_year_id");
    }

    public static function findLast($n = 1) {
        return EM::execQuery("SELECT * FROM school_year ORDER By school_year_id DESC LIMIT 0,:n", [
            "n" => $n
        ]);
    }
}
