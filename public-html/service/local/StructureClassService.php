<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class StructureClassService{

    public static function find($id){
        return EM::execQuerySingleResult("SELECT * FROM structure_class 
            WHERE structure_class_id=:id",
            [
                "id" => $id
            ]);
    }

    public static function addStructureClassPermission($parentStructureId){
        global $currentUser, 
            $currentUserAllowedAction,
            $currentUserRoles;

        $roleIds = self::findStructureClassRoles($parentStructureId, $currentUser['user_id']);
        foreach ($roleIds as $roleId){
            $role = UserRoleService::find($roleId["role_id"]);
    
            $currentUserRoles[$roleId["role_id"]] = $role;
            $additionalActions = UserRoleService::findActionForRole($roleId['role_id']);
            foreach ($additionalActions as $value){
                $currentUserAllowedAction[$value] = "";
            }
        }
    }

    public static function findStructureClassRoles($parentStructureId, $userId){
        return EM::execQuery("SELECT role_id
            from structure_class_user scu
            left join structure_class sc ON sc.structure_class_id=scu.structure_class_id
            left join structure s ON s.structure_id=sc.structure_id
            where user_id=:user_id 
            AND s.parent_structure_id=:parent_structure_id", [
                "user_id" => $userId,
                "parent_structure_id" => $parentStructureId
            ]);
    }

    public static function findAllInsegnanteConstraints($userId){
        $structureId = $_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID];
        return EM::execQuery("SELECT sc.*
            from structure_class_user scu
            left join structure_class sc ON sc.structure_class_id=scu.structure_class_id
            left join structure s ON s.structure_id=sc.structure_id
            where user_id=:user_id AND s.parent_structure_id=:parent_structure_id AND role_id=:role_id", [
                "user_id" => $userId,
                "parent_structure_id" => $structureId,
                "role_id" => UserRole::INSEGNANTE_SOMMINISTRAZIONI
            ]);
    }

    public static function findAllForStructure($schoolYearId, $structureId){
        return EM::execQuery("SELECT * 
            FROM structure_class 
            WHERE structure_id=:structure_id 
            AND school_year_id=:school_year_id", [
                "structure_id" => $structureId,
                "school_year_id" => $schoolYearId
            ]);
    }

    public static function findAllForParentStructureAndClass($schoolYearId, $parentStructureId, $classDictionaryValue){
        return EM::execQuery("SELECT * 
            FROM structure_class sc 
            LEFT JOIN structure s ON sc.structure_id=s.structure_id
            WHERE s.parent_structure_id=:structure_id 
            AND school_year_id=:school_year_id
            AND class_dictionary_value=:class_dictionary_value", [
                "structure_id" => $parentStructureId,
                "school_year_id" => $schoolYearId,
                "class_dictionary_value" => $classDictionaryValue
            ]);
    }

    public static function findAllForStructureAndClass($schoolYearId, $structureId, $classDictionaryValue){
        return EM::execQuery("SELECT * 
            FROM structure_class 
            WHERE structure_id=:structure_id 
            AND school_year_id=:school_year_id
            AND class_dictionary_value=:class_dictionary_value
            AND row_status=:ready", [
                "structure_id" => $structureId,
                "school_year_id" => $schoolYearId,
                "class_dictionary_value" => $classDictionaryValue,
                "ready" => RowStatus::READY
            ]);
    }

    public static function findForStructureAndClassAndSezione($schoolYearId, $structureId, $class, $sezione){
        return EM::execQuerySingleResult("SELECT * FROM structure_class 
            WHERE structure_id=:structure_id 
            AND school_year_id=:school_year_id
            AND class_dictionary_value=:class_dictionary_value
            AND section=:sezione", [
                "structure_id" => $structureId,
                "school_year_id" => $schoolYearId,
                "class_dictionary_value" => $class,
                "sezione" => $sezione
            ]);
    }

}