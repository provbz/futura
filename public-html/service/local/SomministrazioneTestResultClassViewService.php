<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class SomministrazioneTestResultClassViewService {

    public static function addToSomministrazioneClassTestResult($somministrazioneClassTestResultId, $userId){
        EM::insertEntity("somministrazione_test_result_class_view", [
            "somministrazione_test_result_class_id" => $somministrazioneClassTestResultId,
            "user_id" => $userId,
            "insert_date" => "NOW()"
        ]);
    }

    public static function findForSomministrazioneTestClassResult($somministrazioneClassTestResultId){
        return EM::execQuery("SELECT *, v.insert_date as v_insert_date
            FROM somministrazione_test_result_class_view v
            LEFT JOIN user u ON v.user_id=u.user_id
            WHERE somministrazione_test_result_class_id=:somministrazioneClassTestResultId
            order by v.insert_date desc",[
                "somministrazioneClassTestResultId" => $somministrazioneClassTestResultId
            ]);
    }

}