<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class SomministrazioneTestResultClassService{

    public static function find($somministrazioneTestResultId){
        return EM::execQuerySingleResult("SELECT * FROM somministrazione_test_result_class 
            WHERE somministrazione_test_result_class_id=:somministrazione_test_result_class_id",[
                "somministrazione_test_result_class_id" => $somministrazioneTestResultId
            ]);
    }

    public static function findForTestAndClass($testId, $classId){
        return EM::execQuerySingleResult("SELECT * FROM somministrazione_test_result_class 
            WHERE somministrazione_test_id=:somministrazione_test_id 
            AND structure_class_id=:structure_class_id",[
                "somministrazione_test_id" => $testId,
                "structure_class_id" => $classId
            ]);
    }

}