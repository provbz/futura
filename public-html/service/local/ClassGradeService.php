<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ClassGradeService {

    public static function classToGrade($class){
        if (StringUtils::isBlank($class)){
            return $class;
        } else if ($class == "N"){
            return "N";
        } else if ($class == "4II"){
            return "I";
        }

        return substr($class, 1, strlen($class));
    }

    public static function GradeDictionaryTOClass($grade){
        
    }

    public static function findNextClass($actualClassKey){
        if ($actualClassKey == "5S2" || 
            $actualClassKey == "APP" ||
            $actualClassKey == "TIR"){
                return $actualClassKey;
            }

        $classes = DictionaryService::findGroup(Dictionary::CLASS_GRADE);
        $nextClass = null;
        $getNext = false;

        foreach($classes as $class){
            if ($getNext){
                $nextClass = $class;
                break;
            }

            if ($class['key'] == $actualClassKey){
                $getNext = true;
            }
        }

        if ($nextClass == null){
            return $actualClassKey;
        }

        return $nextClass["key"];
    }
}