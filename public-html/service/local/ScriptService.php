<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class ScriptService{

    public static function includeVue(){
        $vueCacheFile = __DIR__ .'/../../cache/vue_header.cache';
        if (!file_exists($vueCacheFile)){
            self::prepareVue($vueCacheFile);
        }

        $content = file_get_contents($vueCacheFile);
        echo $content;
    }

    private static function prepareVue($vueCacheFile){
        $jsFiles = scandir(__DIR__ . '/../../script/mb/vue-components/js/');

        $content = "";
        foreach ($jsFiles as $file) {
            if ($file == '.' || $file == '..'){
                continue;
            }
            
            if (strpos($file, "app.") == 0){
                $content .= "<script src=\"/script/mb/vue-components/js/$file\"></script>";
            } else if (strpo($file, "chunk-vendors")){
                $content .= "<script src=\"/script/mb/vue-components/js/$file\"></script>";
            }
        }

        $cssFiles = scandir(__DIR__ . '/../../script/mb/vue-components/css/');
        foreach ($cssFiles as $file) {
            if ($file == '.' || $file == '..'){
                continue;
            }

            if (strpos($file, "app.") == 0){
                $content .= "<link rel=\"stylesheet\" href=\"/script/mb/vue-components/css/$file\" />";
            } else if (strpo($file, "chunk-vendors")){
                $content .= "<link rel=\"stylesheet\" href=\"/script/mb/vue-components/css/$file\" />";
            }
        }

        file_put_contents($vueCacheFile, $content);
    }
}