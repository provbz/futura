<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class StructureClassUserService{

    public static function findAllForClass($id){
        return EM::execQuery("SELECT * 
            FROM structure_class_user scu
            LEFT JOIN user u ON scu.user_id = u.user_id
            WHERE structure_class_id = :id AND row_status=:row_status_ready
            ORDER BY surname, name", [
            "id" => $id,
            "row_status_ready" => RowStatus::READY
        ]);
    }
    
    public static function findForUserAndYear($userId, $schoolYearId){
        return EM::execQuerySingleResult("SELECT * 
            FROM structure_class_user scu
            LEFT JOIN structure_class sc ON scu.structure_class_id=sc.structure_class_id
            WHERE 
            scu.user_id=:user_id AND
            sc.school_year_id=:school_year_id AND
            scu.row_status=:row_status_ready", [
            "user_id" => $userId,
            "school_year_id" => $schoolYearId,
            "row_status_ready" => RowStatus::READY
        ]);
    }

    public static function findForUserAndClass($userId, $structureClassId){
        return EM::execQuerySingleResult("SELECT * FROM structure_class_user 
            WHERE structure_class_id=:structure_class_id
            AND user_id=:user_id", [
                "structure_class_id" => $structureClassId,
                "user_id" => $userId
            ]);
    }

    /**
     * 
     */
    public static function FindWithRole($structureClassId, $roleId){
        return EM::execQuery("SELECT * 
            FROM structure_class_user scu
            LEFT JOIN user u ON scu.user_id=u.user_id
            WHERE scu.structure_class_id=:structure_class_id
            AND scu.role_id=:role_id
            AND scu.row_status=:row_status_ready
            ORDER BY u.surname, u.name", [
            "structure_class_id" => $structureClassId,
            "role_id" => $roleId,
            "row_status_ready" => RowStatus::READY
        ]);
    }

    public static function insert($structureClassId, $userId, $roleId){
        global $currentUser;

        $structureClassUser = self::findForUserAndYear($structureClassId, $userId);
        if ($structureClassUser != null){
            return;
        }

        EM::insertEntity("structure_class_user", [
            "structure_class_id" => $structureClassId,
            "user_id" => $userId,
            "role_id" => $roleId,
            "insert_date" => "NOW()",
            "insert_user_id" => $currentUser["user_id"],
            "last_edit_user_id" => "NOW()",
            "last_edit_user_id" => $currentUser["user_id"],
            "row_status" => RowStatus::READY
        ]);
    }

    public static function remove($structureClassId, $userId){
        EM::updateEntity("structure_class_user", [
            "row_status" => RowStatus::DELETED
        ], [
            "structure_class_id" => $structureClassId,
            "user_id" => $userId
        ]);
    }
}