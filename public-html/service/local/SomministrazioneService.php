<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class SomministrazioneService{

    public static function getStatus($somministrazione){
        $now = new DateTime();
        $dateStart = new DateTime($somministrazione["data_inizio"]);
        $dateEnd = new DateTime($somministrazione["data_fine"]);

        $status = SomministrazioneStatus::OPEN;
        if ($now->format('Y-m-d') < $dateStart->format('Y-m-d')){
            $status = SomministrazioneStatus::WAITING;
        } else if ($dateEnd->format('Y-m-d') < $now->format('Y-m-d')) {
            $status = SomministrazioneStatus::CLOSED;
        }

        return $status;
    }

    public static function isStructureEnabled($structure){
        $structureIds = [1, 45];

        $eModelEnabled = PropertyService::find(PropertyNamespace::TEST_DIDATTICI, PropertyKey::ENABLED);
        return $eModelEnabled["value"];
    }

    public static function find($id){
        return EM::execQuerySingleResult("SELECT * FROM somministrazione WHERE somministrazione_id=:id", [
            "id" => $id
        ]);
    }

    public static function findForYearAndGradeAndPeriodo($schoolYearId, $classDictionaryValue, $periodoDictionaryValue){
        return EM::execQuerySingleResult("SELECT * 
            FROM somministrazione s
            WHERE school_year_id=:school_year_id
            AND class_dictionary_value=:class_dictionary_value
            AND periodo_dictionary_value=:periodo_dictionary_value", [
                    "school_year_id" => $schoolYearId,
                    "class_dictionary_value" => $classDictionaryValue,
                    "periodo_dictionary_value" => $periodoDictionaryValue
                ]);
    }

    public static function findAllTestsForSomministrazione($id){
        return EM::execQuery("SELECT * 
            FROM somministrazione_test st
            LEFT JOIN test t ON st.test_id=t.test_id
            WHERE somministrazione_id=:id
            AND st.row_status=:row_status_ready
            AND t.row_status=:row_status_ready_2", [
            "id" => $id,
            "row_status_ready" => RowStatus::READY,
            "row_status_ready_2" => RowStatus::READY
        ]);
    }

    public static function findSomministrazioneTest($id){
        return EM::execQuerySingleResult("SELECT * FROM somministrazione_test WHERE somministrazione_test_id=:id", [
            "id" => $id
        ]);
    }

    public static function findSomministrazioneTestForSomministrazioneAndTest($id, $testId){
        return EM::execQuerySingleResult("SELECT * 
            FROM somministrazione_test 
            WHERE somministrazione_id=:id 
            AND test_id=:test_id
            AND row_status=:row_status_ready", [
            "id" => $id,
            "test_id" => $testId,
            "row_status_ready" => RowStatus::READY
        ]);
    }

    public static function findSomministrazioneTestVariableSoglia($somministrazioneTestId, $variableName){
        return EM::execQuerySingleResult("SELECT *
            FROM somministrazione_test_soglia sts
            LEFT JOIN test_soglia ts ON ts.test_soglia_id=sts.test_soglia_id
            WHERE sts.somministrazione_test_id=:somministrazione_test_id 
            AND ts.variable_name = :variable_name", [
                "somministrazione_test_id" => $somministrazioneTestId,
                "variable_name" => $variableName
            ]);
    }

    public static function findSoglieForSomministrazioneTest($somministrazioneTestId, $variables){
        $soglie = [];

        foreach ($variables as $variable){
            $soglia = SomministrazioneService::findSomministrazioneTestVariableSoglia($somministrazioneTestId, $variable["name"]);
            if ($soglia == null){
                continue;
            }

            $soglie[$variable["name"]] = [
                "s1" => $soglia["s1"],
                "s2" => $soglia["s2"],
                "type" => $variable["type"]
            ];
        }

        return $soglie;
    }

    public static function evaluateScore($soglie, $varName, $value){
        if (!isset($soglie[$varName])){
            return null;
        }

        $soglia = $soglie[$varName];
        
        if ($soglia["type"] == TestVariableType::RIGHTS){
            if ($value < $soglia["s1"]){
                return 0;        
            } else if ($value >= $soglia["s1"] && $value < $soglia["s2"]){
                return 1;
            } else {
                return 2;
            }
        } else {
            if ($value < $soglia["s1"]){
                return 2;        
            } else if ($value >= $soglia["s1"] && $value < $soglia["s2"]){
                return 1;
            } else {
                return 0;
            }
        }
    }

    public static function evaluateAllResultsFor($somministrazioneTestId, $variable){
        $sogliae = self::findSoglieForSomministrazioneTest($somministrazioneTestId, [$variable]);

        if (!isset($sogliae[$variable["name"]]) || $sogliae[$variable["name"]] == null){
            EM::execQuery("update somministrazione_test_result_user_variable stuv
                LEFT JOIN somministrazione_test_result_user stu ON stuv.somministrazione_test_result_user_id=stu.somministrazione_test_result_user_id
                set fascia=NULL
                WHERE somministrazione_test_id=:somministrazione_test_id
                AND variable_name=:variable_name", [
                    "somministrazione_test_id" => $somministrazioneTestId,
                    "variable_name" => $variable["name"]
                ]);
        } else {
            $soglia = $sogliae[$variable["name"]];

            if ($soglia["type"] == TestVariableType::RIGHTS){
                EM::execQuery("update somministrazione_test_result_user_variable stuv
                    LEFT JOIN somministrazione_test_result_user stu ON stuv.somministrazione_test_result_user_id=stu.somministrazione_test_result_user_id
                    set fascia=0
                    WHERE somministrazione_test_id=:somministrazione_test_id
                    AND variable_name=:variable_name
                    AND value < :s1", [
                        "somministrazione_test_id" => $somministrazioneTestId,
                        "variable_name" => $variable["name"],
                        "s1" => $soglia["s1"]
                    ]);

                EM::execQuery("update somministrazione_test_result_user_variable stuv
                    LEFT JOIN somministrazione_test_result_user stu ON stuv.somministrazione_test_result_user_id=stu.somministrazione_test_result_user_id
                    set fascia=1
                    WHERE somministrazione_test_id=:somministrazione_test_id
                    AND variable_name=:variable_name
                    AND value >= :s1 AND value < :s2", [
                        "somministrazione_test_id" => $somministrazioneTestId,
                        "variable_name" => $variable["name"],
                        "s1" => $soglia["s1"],
                        "s2" => $soglia["s2"]
                    ]);

                EM::execQuery("update somministrazione_test_result_user_variable stuv
                    LEFT JOIN somministrazione_test_result_user stu ON stuv.somministrazione_test_result_user_id=stu.somministrazione_test_result_user_id
                    set fascia=2
                    WHERE somministrazione_test_id=:somministrazione_test_id
                    AND variable_name=:variable_name
                    AND value >= :s2", [
                        "somministrazione_test_id" => $somministrazioneTestId,
                        "variable_name" => $variable["name"],
                        "s2" => $soglia["s2"]
                    ]);
            } else {
                EM::execQuery("update somministrazione_test_result_user_variable stuv
                    LEFT JOIN somministrazione_test_result_user stu ON stuv.somministrazione_test_result_user_id=stu.somministrazione_test_result_user_id
                    set fascia=2
                    WHERE somministrazione_test_id=:somministrazione_test_id
                    AND variable_name=:variable_name
                    AND value < :s1", [
                        "somministrazione_test_id" => $somministrazioneTestId,
                        "variable_name" => $variable["name"],
                        "s1" => $soglia["s1"]
                    ]);

                EM::execQuery("update somministrazione_test_result_user_variable stuv
                    LEFT JOIN somministrazione_test_result_user stu ON stuv.somministrazione_test_result_user_id=stu.somministrazione_test_result_user_id
                    set fascia=1
                    WHERE somministrazione_test_id=:somministrazione_test_id
                    AND variable_name=:variable_name
                    AND value >= :s1 AND value < :s2", [
                        "somministrazione_test_id" => $somministrazioneTestId,
                        "variable_name" => $variable["name"],
                        "s1" => $soglia["s1"],
                        "s2" => $soglia["s2"]
                    ]);

                EM::execQuery("update somministrazione_test_result_user_variable stuv
                    LEFT JOIN somministrazione_test_result_user stu ON stuv.somministrazione_test_result_user_id=stu.somministrazione_test_result_user_id
                    set fascia=0
                    WHERE somministrazione_test_id=:somministrazione_test_id
                    AND variable_name=:variable_name
                    AND value >= :s2", [
                        "somministrazione_test_id" => $somministrazioneTestId,
                        "variable_name" => $variable["name"],
                        "s2" => $soglia["s2"]
                    ]);
            }
        }
    }

}