<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserTransferService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserTransferService {
    
    const STATUS_PENDING = "pending";
    const STATUS_APPROVED = "approved";
    const STATUS_APPROVED_ONLY_DATA = "approved_only_data";
    const STATUS_REJECTED = "rejected";
    
    public static function find($utrId){
        return EM::execQuerySingleResult("SELECT * FROM user_transfer_request utr 
                WHERE user_transfer_request_id=:id", ["id" => $utrId]);
    }
    
    public static function findForUser($userId){
        return EM::execQuery("SELECT * 
                FROM user_transfer_request
                WHERE status=:status 
                AND user_id=:user_id ", [
                    "status" => self::STATUS_PENDING,
                    "user_id" => $userId
                ]);
    }

    public static function findFromUserToStructure($userId, $structureId){
        return EM::execQuerySingleResult("SELECT * 
                FROM user_transfer_request
                WHERE status=:status 
                AND user_id=:user_id 
                AND to_structure_id=:structure_id", [
                    "status" => self::STATUS_PENDING,
                    "user_id" => $userId,
                    "structure_id" => $structureId
                ]);
    }
    
    public static function addRequest($userId, $toStructureId, $type = null, $schoolYearId = null, $toClassId = null) {
        global $currentUser;
        
        if ($type == null){
            $type = UserTransferRequestType::FROM_DESTINATION;
        }
        if ($schoolYearId == null){
            $currentSchoolYear = SchoolYearService::findCurrentYear();
            $schoolYearId = $currentSchoolYear['school_year_id'];
        }
        
        $existing = self::findFromUserToStructure($userId, $toStructureId);
        if ($existing != null){
            return $existing;
        }
        
        $structures = StructureService::findUserStructures($userId);
        if ($structures->rowCount() == 0){
            throw new Exception("Dati non validi, l'utente non è associato a nessuna scuola.");
        }
        $structure = $structures->fetch();
        
        self::deletePendingRequests($userId, $schoolYearId);
           
        $utrId = EM::insertEntity("user_transfer_request", [
            "user_id" => $userId,
            "from_structure_id" => $structure['structure_id'],
            "to_structure_id" => $toStructureId,
            "requester_user_id" => $currentUser['user_id'],
            "status" => self::STATUS_PENDING,
            "type" => $type,
            "school_year_id" => $schoolYearId,
            "to_class_id" => $toClassId,
            "insert_date" => "NOW()"
        ]);
        
        $entity = self::find($utrId);
        
        NoticeService::persist([
            "type" => NoticeType::USER_TRANSFER_ADD,
            "parameters" => json_encode($entity)
        ]);
        
        return $entity;
    }
    
    public static function deletePendingRequests($userId, $schoolYearId){
        EM::deleteEntity("user_transfer_request", [
            "user_id" => $userId,
            "school_year_id" => $schoolYearId,
            "status" => self::STATUS_PENDING
        ]);
    }
    
    public static function setStatus($utrId, $newStatus, $note, $changeSchoolYear = false){
        global $currentUser;
        
        EM::updateEntity("user_transfer_request", [
            "update_status_user_id" => $currentUser['user_id'],
            "update_date" => "NOW()",
            "status" => $newStatus,
            "note" => $note
        ], ["user_transfer_request_id" => $utrId]);
        
        $request = self::find($utrId);
        $currentYear = SchoolYearService::findCurrentYear();
        $user = UserService::find($request['user_id']);
        $userYear = UserYearService::findUserYearByUser($request['user_id']);
        
        if ($newStatus == self::STATUS_APPROVED || $newStatus == self::STATUS_APPROVED_ONLY_DATA){
            if ($userYear != null){
                $userYearPar = [
                    "plesso_structure_id" => null,
                    "sezione" => ""
                ];

                if ($changeSchoolYear){
                    $userYearPar["classe"] = ClassGradeService::findNextClass($userYear["classe"]);
                }

                EM::updateEntity("user_year", $userYearPar, [
                    "user_year_id" => $userYear["user_year_id"]
                ]);
            }
            
            StructureService::removeUserStructure($request['user_id'], $request['from_structure_id']);
            StructureService::addUserStructure($request['user_id'], $request['to_structure_id'], [UserRole::STUDENTE]);

            self::classTransfer($request, $userYear);
            
            UserUserService::remove($request['user_id']);
            /*
            if ($request['type'] == UserTransferRequestType::FROM_DESTINATION){
                UserUserService::persist([
                    "user_id" => $request['user_id'],
                    "to_user_id" => $request["requester_user_id"]
                ]);
            }
            */
            
            $yearId = $currentYear["school_year_id"];
            if ($changeSchoolYear){
                $prevYear = SchoolYearService::findPrevYear();
                $yearId = $prevYear["school_year_id"];
            }
            
            $documents = DocumentService::findForUserAndYearAndType($user['user_id'], $yearId);
            foreach ($documents as $document) {
                if ($document["type"] != DocumentType::PDP 
                    && $document["type"] != DocumentType::PDP_LINGUA
                    && $document["type"] != DocumentType::PEI
                    && $document["type"] != DocumentType::PEI_TIROCINIO){
                    continue;
                }
                
                DocumentService::toCurrentYear($document['user_year_document_id']);
            }

            UserService::updateUserStatus($request['user_id'], UserStatus::ENABLED);
        }
        
        if ($newStatus == self::STATUS_APPROVED_ONLY_DATA) {
            $documents = DocumentService::findAllForUser($user["user_id"]);
            foreach ($documents as $document) {
                DocumentService::updateStatus($document["user_year_document_id"], UserYearDocumentStatus::DELETED);
            }

            $attachments = AttachmentService::findAllForEntity(EntityName::USER, $user['user_id']);
            foreach ($attachments as $attachment) {
                AttachmentService::deleteAttachmentAndAttachmentEntity($attachment["attachment_id"]);
            }
        }
        
        $entity = self::find($utrId);
        NoticeService::persist([
            "type" => NoticeType::USER_TRANSFER_UPDATE,
            "parameters" => json_encode($entity)
        ]);
    }

    public static function classTransfer($request, $userYear){
        if ($request["to_class_id"] == null){
            return;
        }

        $toClass = StructureClassService::find($request["to_class_id"]);
        if ($toClass == null){
            return;
        }

        $fromClass = StructureClassUserService::findForUserAndYear($request["user_id"], $toClass["school_year_id"]);
        if ($fromClass == null){
            return;
        }

        $fromClass["class_id"] = $request["to_class_id"];
        $structureClassUserId = $fromClass["structure_class_user_id"];
        
        EM::updateEntity("structure_class_user", [
            "structure_class_id" => $request["to_class_id"]
        ], [
            "structure_class_user_id" => $structureClassUserId
        ]);

        EM::updateEntity("user_year", [
            "plesso_structure_id" => $toClass["structure_id"],
            "sezione" => $toClass["section"]
        ], [
            "user_year_id" => $userYear["user_year_id"]
        ]);
    }

    public static function updateFromSourcePending() {
        $requests = EM::execQuery("SELECT * FROM user_transfer_request WHERE type=:type AND status=:status", [
            "type" => UserTransferRequestType::FROM_SOURCE,
            "status" => self::STATUS_PENDING
        ]);
        
        foreach ($requests as $request){
            self::setStatus($request['user_transfer_request_id'], self::STATUS_APPROVED, "", true);
        }
    }

}
