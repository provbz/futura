<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class TestSogliaService {

    public static function find($id){
        return EM::execQuerySingleResult("SELECT * FROM test_soglia WHERE test_soglia_id=:id", [
            "id" => $id
        ]);
    }

    public static function findInUseForTest($testId){
        return EM::execQuery("SELECT * FROM test_soglia 
            WHERE test_id=:test_id AND in_use=1", [
            "test_id" => $testId
        ]);
    }

    public static function findInUseForTestAndVariableName($testId, $variableName){
        return EM::execQuerySingleResult("SELECT * FROM test_soglia 
            WHERE test_id=:test_id
            AND in_use=1 
            AND variable_name=:variable_name
            ORDER BY in_use DESC", [
                "variable_name" => $variableName,
                "test_id" => $testId
            ]);
    }

    public static function findForTestAndVariableName($testId, $variableName){
        return EM::execQuery("SELECT * FROM test_soglia 
            WHERE test_id=:test_id 
            AND variable_name=:variable_name
            ORDER BY in_use DESC", [
                "variable_name" => $variableName,
                "test_id" => $testId
            ]);
    }
}