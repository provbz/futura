<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EModelService {
    
    public static function isStructureEnabled($structure){
        $structureIds = [1, 12, 15, 25, 45];

        $eModelEnabled = PropertyService::find(PropertyNamespace::MODELLO_E, PropertyKey::ENABLED);
        return $eModelEnabled["value"] && (array_search($structure["structure_id"], $structureIds) !== false);
    }

    public static function find($id){
        return EM::execQuerySingleResult("SELECT * FROM e_model WHERE e_model_id=:id", ['id' => $id]);
    }

    public static function findByCodeAndSchoolYear($code, $schoolYearId){
        return EM::execQuerySingleResult("SELECT * 
            FROM e_model 
            WHERE school_year_id=:school_year_id AND
                codice=:codice", [
                    'codice' => $code,
                    'school_year_id' => $schoolYearId,
                ]);
    }
 
    public static function findByDocumentId($documentId){
        return EM::execQuerySingleResult("SELECT * FROM e_model WHERE user_year_document_id=:id", ['id' => $documentId]);
    }

    public static function findCodes($eModelId){
        return EM::execQuery("SELECT * FROM e_model_code WHERE e_model_id=:id", ['id' => $eModelId]);
    }

    public static function detachCodes($id) {
        EM::deleteEntity("e_model_code", ["e_model_id" => $id]);
    }

}
