<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserTransferService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class UserYearService {

    public static function find($userYearId){
        return EM::execQuerySingleResult("SELECT * FROM user_year WHERE user_year_id=:id", [
            "id" => $userYearId
        ]);
    }
    
    public static function findUserYearByUser($userId){
        $query = "SELECT * FROM user_year uy WHERE user_id=:user_id ";
        return EM::execQuerySingleResult($query, [
            "user_id" => $userId
        ]);
    }
    
    public static function findUserYearToRemove(){
        $currentSchoolYear = SchoolYearService::findCurrentYear();

        return EM::execQuery("SELECT distinct(uyd.user_id)
            from user_year_document uyd
            left join user_structure us on uyd.user_id=us.user_id
            where uyd.type IN ('pei', 'pdp')
                AND uyd.user_id not in 
                (
                    select uyd2.user_id 
                    from user_year_document uyd2 
                    where school_year_id >= :current_school_year_id
                    OR uyd.type NOT IN ('pei', 'pdp')
                )", [
                ":current_school_year_id" => $currentSchoolYear["school_year_id"]
            ]);
    }

    public static function findUserYearToRemoveInStructure($structureId){
        $currentSchoolYear = SchoolYearService::findCurrentYear();
        return EM::execQuery("SELECT distinct(uyd.user_id)
            from user_year_document uyd
            left join user_structure us on uyd.user_id=us.user_id
            where uyd.type IN ('pei', 'pdp')
            AND us.structure_id=:structure_id AND uyd.user_id not in 
                (select uyd2.user_id 
                from user_year_document uyd2 
                where school_year_id=:current_school_year_id)", [
                    ":structure_id" => $structureId,
                    ":current_school_year_id" => $currentSchoolYear["school_year_id"]
                ]);
    }
    
    public static function findLastInsertedUserYear($structureId){
        $stmt = EM::prepare("SELECT uy.*
                                FROM user u
                                LEFT JOIN user_year uy ON u.user_id=uy.user_id
                                LEFT JOIN user_structure us ON us.user_id=u.user_id
                                LEFT JOIN user_structure_role usr On us.user_structure_id=usr.user_structure_id
                                WHERE us.structure_id=:id AND role_id=:role
                                order BY u.insert_date desc, uy.user_year_id DESC
                                LIMIT 0,1");
        $stmt->execute(['id' => $structureId, 'role' => UserRole::STUDENTE]);
        
        return $stmt->fetch();
    }
}