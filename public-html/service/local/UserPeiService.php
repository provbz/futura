<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserTransferService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserPeiService{
    
    public static function findForUser($userId){
        $query = sprintf("SELECT * "
                . "FROM user_year_document uyp "
                . "LEFT JOIN user_year uy ON uyp.user_year_id=uy.user_year_id "
                . "WHERE uy.user_id=%d "
                . "ORDER BY uyp.insert_date", 
                $userId);
        return EM::execQuery($query);
    }
    
    public static function countForUserYear($userYearId){
        $query = sprintf("SELECT COUNT(*) as c "
                . "FROM user_year_document uyp "
                . "WHERE user_year_id=%d ",
                $userYearId);
        $row = EM::execQuerySingleResult($query);
        return $row['c'];
    }
    
    public static function countPeiRow($peiId){
        $query = sprintf("SELECT COUNT(*) as c FROM pei_row WHERE pei_id=%d", $peiId);
        $row = EM::execQuerySingleResult($query);
        return $row['c'];
    }
    
    public static function findPeiRow($peiRowId){
        $query = sprintf("SELECT * FROM pei_row WHERE pei_row_id=%d", $peiRowId);
        return EM::execQuerySingleResult($query);
    }
    
    public static function findPeiRowTargetByRow($peiRowId, $status = NULL){
        $query = "SELECT prt.*, prt.activity as prt_activity, prt.target as prt_target, ue.name as ue_name, ue.surname as ue_surname
                    FROM pei_row_target prt 
                    LEFT JOIN user ue ON prt.last_edit_user_id = ue.user_id
                    WHERE pei_row_id=:id";
        
        $par = ['id' => $peiRowId];
        if ($status != NULL){
            $query .= "AND status=:status";
            $par['status'] = $status;
        }
                
        $stmt = EM::prepare($query);
        $stmt->execute($par);
        
        return $stmt;
    }
        
}