<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class SomministrazioneStatsService{

    public static function findUserResult($userId, $somministrazioneTestId){
        return EM::execQuerySingleResult("SELECT * 
            FROM somministrazione_test_result_user
            WHERE somministrazione_test_id=:somministrazione_test_id 
            AND user_id=:user_id",
            [
                "somministrazione_test_id" => $somministrazioneTestId,
                "user_id" => $userId
            ]);
    }

    public static function findUserResultVars($userId, $somministrazioneTestId){
        return EM::execQuery("SELECT variable_name, value, fascia
            from somministrazione_test_result_user_variable struv
            left join somministrazione_test_result_user stru ON struv.somministrazione_test_result_user_id=stru.somministrazione_test_result_user_id
            WHERE somministrazione_test_id=:somministrazione_test_id
            AND user_id=:user_id",
            [
                "somministrazione_test_id" => $somministrazioneTestId,
                "user_id" => $userId
            ]);
    }

    public static function computeTotal($tableData, $testsData){
        $total = [
            "tests" => [],
            "totale_studenti" => 0
        ];

        foreach ($tableData as $row) {
            $total["totale_studenti"] += ArrayUtils::getIndex($row, "totale_studenti", 0);

            foreach ($testsData as $test) {
                $testResult = $row["tests"][$test["test"]["test_id"]];
                $testSum = isset($total["tests"][$test["test"]["test_id"]]) ? $total["tests"][$test["test"]["test_id"]] : [
                    "not_set" => 0,
                    "non_valida" => 0,
                    "assente" => 0
                ];

                $testSum["not_set"] += ArrayUtils::getIndex($testResult, "not_set", 0);
                $testSum["non_valida"] += ArrayUtils::getIndex($testResult, "non_valida", 0);
                $testSum["assente"] += ArrayUtils::getIndex($testResult, "assente", 0);

                foreach ($test["variables"] as $key => $value) {
                    $variableSum = isset($testSum[$key]) ? $testSum[$key] : [
                        "f_0" => 0,
                        "f_1" => 0,
                        "f_2" => 0
                    ];

                    $variableResult = ArrayUtils::getIndex($testResult, $key, []);
                    $variableSum["f_0"] += ArrayUtils::getIndex($variableResult, "f_0", 0);
                    $variableSum["f_1"] += ArrayUtils::getIndex($variableResult, "f_1", 0);
                    $variableSum["f_2"] += ArrayUtils::getIndex($variableResult, "f_2", 0);

                    $testSum[$key] = $variableSum;
                }

                $total["tests"][$test["test"]["test_id"]] = $testSum;
            }
        }

        return $total;
    }
}