<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of RilevazioneCattedreService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class RilevazioneCattedreService{

    public static function isStructureEnabled($structure){
        $structureIds = [1];

        $eModelEnabled = PropertyService::find(PropertyNamespace::RILEVAZIONE_CATTEDRE, PropertyKey::ENABLED);
        return $eModelEnabled && $eModelEnabled["value"] && (array_search($structure["structure_id"], $structureIds) !== false);
    }

    public static function find($id){
        return EM::execQuerySingleResult("SELECT * FROM rilevazione_cattedre WHERE rilevazione_cattedre_id = :id", ["id" => $id]);
    }

    public static function remove($id){
        EM::deleteEntity("rilevazione_cattedre", ["rilevazione_cattedre_id" => $id]);
    }
}