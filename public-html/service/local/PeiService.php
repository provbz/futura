<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PeiService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PeiService {
    
    public static function updatePeiStatus($userYearDocumentId){
        $status = PeiStatus::RED;
        $document = DocumentService::find($userYearDocumentId);

        if ($document == null){
            return;
        }

        $currentSchoolYear = SchoolYearService::findCurrentYear();
        $files = 0;

        if ($document["type"] == DocumentType::PDP_LINGUA) {
            $rows = AttachmentService::findAllForEntity(EntityName::USER, $document["user_id"], DocumentType::PDP_LINGUA . "_sy_" . $currentSchoolYear["school_year_id"]);
            $files = $rows->rowCount();
        } else if ($document["type"] == DocumentType::PEI_TIROCINIO) {
            $rows = AttachmentService::findAllForEntity(EntityName::USER, $document["user_id"], DocumentType::PEI_TIROCINIO . "_sy_" . $currentSchoolYear["school_year_id"]);
            $files = $rows->rowCount();
        } else if ($document["type"] == DocumentType::PEI){
            $rows = AttachmentService::findAllForEntity(EntityName::USER, $document["user_id"], DocumentType::PEI . "_sy_" . $currentSchoolYear["school_year_id"]);
            $files = $rows->rowCount();
        } else if ($document["type"] == DocumentType::PDP) {
            $rows = AttachmentService::findAllForEntity(EntityName::USER, $document["user_id"], DocumentType::PDP . "_sy_" . $currentSchoolYear["school_year_id"]);
            $files = $rows->rowCount();
        }

        if ($files > 0){
            $status = PeiStatus::GREEN;
        } else {
            $peiRows = PeiService::findPeiRows($document["user_year_document_id"]);
            if ($peiRows->rowCount() > 0){
                $status = PeiStatus::YELLOW;
            }
        }

        EM::updateEntity("user_year_document", [
            "pei_status" => $status
        ], [
            "user_year_document_id" => $userYearDocumentId
        ]);
    }

    public static function clonePei($userYearId, $createdByUserId, $sourcePei, $type){
        $peiId = NULL;
        
        try{
            EM::transactionStart();
            $peiId = PeiService::create($userYearId, $createdByUserId, $type);
            $peiPar = ReflectionUtils::copyKeys($sourcePei, ['note', 'author', 'status']);
            EM::updateEntity("user_year_pei", $peiPar, sprintf('user_year_pei_id=%d', $peiId));
            
            foreach (PeiService::findRows($sourcePei['user_year_pei_id']) as $peiRow){
                $rowPar = ReflectionUtils::copyNotNumericKeys($peiRow);
                $rowPar['pei_id'] = $peiId;
                $oldPeiRowId = $rowPar['pei_row_id'];
                unset($rowPar['pei_row_id']);
                $peiRowId = EM::insertEntity("pei_row", $rowPar);
                
                foreach(PeiService::findPeiRowTargetsClean($oldPeiRowId) as $peiRowTarget){
                    $rowTargetPar = ReflectionUtils::copyNotNumericKeys($peiRowTarget);
                    $rowTargetPar['pei_row_id'] = $peiRowId;
                    unset($rowTargetPar['pei_row_target_id']);
                    EM::insertEntity("pei_row_target", $rowTargetPar);    
                }
            }
            
            foreach(PeiService::findPeiQuestions($sourcePei['user_year_pei_id']) as $peiQuestion){
                $questionPar = ReflectionUtils::copyNotNumericKeys($peiQuestion);
                $questionPar['user_year_pei_id'] = $peiId;
                unset($questionPar['user_year_pei_question_id']);
                $peiRowId = EM::insertEntity("user_year_pei_question", $questionPar);
            }
            
            EM::transactionCommit();
        }catch(Error $e){
            EM::transactionRoolback();
        }
        
        return $peiId;
    }
    
    public static function findPeiQuestions($peiId){
        $stmt = EM::prepare("SELECT *
                                FROM user_year_pei_question
                                WHERE user_year_pei_id=:id");
        $stmt->execute(['id' => $peiId]);
        return $stmt;
    }
    
    public static function findRows($peiId){
        $stmt = EM::prepare("SELECT * FROM pei_row WHERE pei_id=:id");
        $stmt->execute(['id' => $peiId]);
        return $stmt;
    }
    
    public static function countNodeChildren($documentType, $peiNodeId, $view = NULL){
        $query = "select COUNT(*) as c
                    FROM " . $documentType . "_node 
                    WHERE father_pei_node_id=:id";
        $par = ['id' => $peiNodeId];
        
        if ($view != NULL){
            $query .= " AND view_type LIKE :view_type";
            $par['view_type'] = '%"'.$view.'"%';
        }
        
        $stmt = EM::prepare($query);
        $stmt->execute($par);
        $res = $stmt->fetch();
        return $res['c'];
    }
    
    public static function findByTitle($title, $documentType){
        $query = "SELECT * FROM " . $documentType  . "_node pn 
                    WHERE label like :label 
                    ORDER By `order`, label";
        
        $par = ["label" => '%'.$title .'%'];
        
        $stmt = EM::prepare($query);
        $stmt->execute($par);
        return $stmt;
    }
    
    public static function findNodeWithFather($documentType, $peiNodeId = NULL, $profile = NULL, $userYear = NULL, $view = NULL){
        $query = "SELECT * FROM " . $documentType . "_node pn WHERE ";
        
        $par = [];
        if ($peiNodeId == NULL){
            $query .= "father_pei_node_id IS NULL";
        } else {
            $query .= "father_pei_node_id = :node_id";
            $par['node_id'] = $peiNodeId;
        }
        
        $gender = ReflectionUtils::getValue($profile, UserMetadata::GENDER_ID);
        if (StringUtils::isNotBlank($gender)){
            $query .= " AND gender LIKE :gender";
            $par['gender'] = '%"'.$gender.'%';
        }
        
        $grade = ReflectionUtils::getValue($userYear, 'classe');
        if (StringUtils::isNotBlank($grade)){
            $grade = substr($grade, 1);
            $query .= " AND grades LIKE :grade";
            $par['grade'] = '%"'.$grade.'"%';
        }
        
        if ($view != NULL){
            $query .= " AND view_type LIKE :view_type";
            $par['view_type'] = '%"'.$view.'"%';
        }
        
        $query .= " ORDER BY pn.order";
        
        $stmt = EM::prepare($query);
        $stmt->execute($par);
        return $stmt;
    }
    
    public static function findNode($nodeId, $documentType){
        $query = sprintf("SELECT * FROM " . $documentType . "_node WHERE pei_node_id=%d", $nodeId);
        return EM::execQuerySingleResult($query);
    }
    
    public static function findNodeByUri($uri, $documentType){
        if ($documentType == DocumentType::PDP_LINGUA){
            $documentType = DocumentType::PDP;
        }

        $stmt = EM::prepare("SELECT * FROM " . $documentType . "_node WHERE uri=:uri");
        $stmt->execute(['uri' => $uri]);
        return $stmt->fetch();
    }
    
    public static function findNodeQuestions($peiNodeId, $documentType){
        if ($documentType == DocumentType::PDP_LINGUA){
            $documentType = DocumentType::PDP;
        }

        $stmt = EM::prepare("SELECT * FROM " . $documentType . "_node_question WHERE pei_node_id=:id");
        $stmt->execute(['id' => $peiNodeId]);
        return $stmt;
    }
    
    public static function findNodeTarget($nodeTargetId){
        $stmt = EM::prepare("SELECT * FROM pei_node_target WHERE pei_target_id=:id");
        $stmt->execute(['id' => $nodeTargetId]);
        return $stmt->fetch();
    }
    
    public static function findTargetByPeiNodeAndTerm($peiNodeId, $term){
        $stmt = EM::prepare("SELECT * FROM pei_node_target WHERE pei_node=:pei_node_id AND term=:term");
        $stmt->execute(['pei_node_id' => $peiNodeId, 'term' => $term]);
        return $stmt->fetchAll();
    }
    
    public static function findTargets($peiNodeId){
        $stmt = EM::prepare("SELECT * FROM pei_node_target WHERE pei_node_id=:id");
        $stmt->execute(['id' => $peiNodeId]);
        return $stmt;
    }
    
    public static function findTarget($peiTargetId){
        $stmt = EM::prepare("SELECT * FROM pei_node_target WHERE pei_target_id=:id");
        $stmt->execute(['id' => $peiTargetId]);
        return $stmt->fetch();
    }
    
    public static function findTargetFor($peiNodeId, $grade, $term, $level){
        $query = "SELECT * FROM pei_node_target WHERE
                        pei_node_id=:id AND term_id=:term AND level_id=:level AND grade_id=:grade";
        
        $par = [
            'id' => $peiNodeId,
            'term' => $term,
            'level' => $level,
            'grade' => $grade
        ];
        
        $stmt = EM::prepare($query);
        $stmt->execute($par);
        return $stmt;
    }
    
    public static function buildNodePath($uri, $documentType, $offset = 0, $length = NULL){
        $node = PeiService::findNodeByUri($uri, $documentType);

        $arr = PeiService::buildNodePathArray($node['pei_node_id'], $documentType);
        $arr = array_slice($arr, $offset, $length);
        $arr = array_map(function($obj) { return $obj['label']; }, $arr);
        $out = implode(" -> ", $arr);
         return $out;
    }
    
    public static function buildNodePathArray($nodeId, $documentType){
        $arr = [];
        do{
            $node = PeiService::findNode($nodeId, $documentType);
            $arr[] = $node;
            $nodeId = $node['father_pei_node_id'];
        } while ($nodeId != NULL);
        
        return array_reverse($arr);
    }
    
    public static function removeReplies($peiId, $step = NULL){
        $cond = sprintf("user_year_pei_id=%d", $peiId);
        if ($step !== NULL){
            $cond .= sprintf(' AND step=%d', $step);
        }
        EM::deleteEntity("user_year_pei_question", $cond);
    }
    
    public static function findReplies($peiId, $step){
        $stmt = EM::prepare("SELECT uri, weight, question_num
                FROM user_year_pei_question
                WHERE user_year_pei_id=:id AND step=:step");
        $stmt->execute(['id' => $peiId, 'step' => $step]);
        return $stmt;
    }
    
    public static function countPeiRowWithStatus($peiId, $status){
        $stmt = EM::prepare("SELECT COUNT(*)
                    FROM pei_row as pr
                    WHERE pei_id=:pei_id AND status=:status AND uri<>'apprendimento'");
        $stmt->execute(['pei_id' => $peiId, 'status' => $status ]);
        $row = $stmt->fetch();
        return $row[0];
    }
    
    public static function findPeiRowWithStatus($documentType, $peiId, $status, $from = -1, $limit = -1, $orderBy = 'position, title', $excludeApprendimento = false){
        $par = [];

        if ($documentType == DocumentType::PDP_LINGUA){
            $documentType = DocumentType::PDP;
        }
        
        $query = "SELECT pn.description as pn_description,
                    pn.pei_node_id as pn_pei_node_id, pn.uri as pn_uri, pn.label AS node_label, 
                    pr.uri, pr.pei_row_id, pr.title, pr.uri as pr_uri, prn.label as root_label,
                    pr.last_edit_date,
                    ue.name as ue_name, ue.surname as ue_surname
                    FROM pei_row as pr
                    LEFT JOIN " . $documentType . "_node pn ON pr.uri=pn.uri
                    LEFT JOIN " . $documentType . "_node prn ON pn.root_pei_node_id=prn.pei_node_id
                    LEFT JOIN user ue ON pr.last_edit_user_id = ue.user_id
                    WHERE pei_id=:pei_id ";
        
        if (StringUtils::isNotBlank($status)){
            $query .= " AND pr.status=:status";
        }

        if ($excludeApprendimento){
            $query .= " AND pr.uri<>:apprendimentoUri";
            $par['apprendimentoUri'] = 'apprendimento';
        }
        
        $query .= " ORDER BY ". $orderBy;
        
        if ($from != -1 && $limit != -1){
            $query .= " LIMIT ".$from.", ". $limit;
        }
        
        $par['pei_id'] = $peiId;
        $par['status'] = $status;
        $stmt = EM::prepare($query);
        $stmt->execute($par);
        return $stmt;
    }
    
    public static function findPeiRowTarget($rowTargetId){
        $stmt = EM::prepare("SELECT * FROM pei_row_target WHERE pei_row_target_id=:id");
        $stmt->execute(['id' => $rowTargetId]);
        return $stmt->fetch();
    }
    
    public static function findPeiRowTargetsClean($peiRowId){
        $stmt = EM::prepare("SELECT *
                                FROM pei_row_target prt
                                WHERE pei_row_id=:id");
        $stmt->execute(['id' => $peiRowId]);
        return $stmt;
    }
    
    public static function findPeiRowTargets($peiRowId){
        $stmt = EM::prepare("SELECT *
                                FROM pei_row_target prt
                                WHERE pei_row_id=:id");
        $stmt->execute(['id' => $peiRowId]);
        return $stmt;
    }
    
    public static function findPeiRowTargetWithPeiId($peiRowId){
        $stmt = EM::prepare("SELECT *
                                FROM pei_row_target prt
                                WHERE pei_row_id=:id");
        $stmt->execute(['id' => $peiRowId]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    public static function findPeiRow($peiRowId){
        $stmt = EM::prepare("SELECT * FROM pei_row
                                WHERE pei_row_id=:id");
        $stmt->execute(['id' => $peiRowId]);
        return $stmt->fetch();
    }

    public static function findPeiRows($peiId){
        $stmt = EM::execQuery("SELECT * FROM pei_row WHERE pei_id=:id", ["id" => $peiId]);
        return $stmt;
    }
    
    public static function findPeiRowByUri($peiId, $uri){
        $stmt = EM::prepare("SELECT * FROM pei_row
                                WHERE pei_id=:id AND uri=:uri");
        $stmt->execute(['uri' => $uri, 'id' => $peiId]);
        return $stmt->fetch();
    }
    
    public static function findRowTargetStatus($peiRowTarget){
        $stmt = EM::prepare("SELECT * FROM pei_row_target_status
                                WHERE pei_row_target_id=:id");
        $stmt->execute(['id' => $peiRowTarget]);
        return $stmt;
    }
    
    public static function findNodeQuestion($nodeId, $documentType){
        if ($documentType == DocumentType::PDP_LINGUA){
            $documentType = DocumentType::PDP;
        } else if ($documentType == DocumentType::PEI_TIROCINIO){
            $documentType = DocumentType::PEI;
        }

        $stmt = EM::prepare("SELECT * from " . $documentType . "_node_question WHERE pei_node_id=:id");
        $stmt->execute(['id' => $nodeId]);
        return $stmt;
    }
    
    public static function findNodeQuestionById($questionId){
        $stmt = EM::prepare("SELECT * from pei_node_question WHERE pei_node_question_id=:id");
        $stmt->execute(['id' => $questionId]);
        return $stmt->fetch();
    }
    
    public static function findRowWithUri($peiId, $uri){
        $stmt = EM::prepare("SELECT * FROM pei_row 
                                WHERE pei_id=:id AND uri=:uri");
        $stmt->execute(['id' => $peiId, 'uri' => $uri]);
        return $stmt;
    }
    
    public static function hasToDoOnlyApprendimento($peiId){
        $stmt = EM::prepare("SELECT count(*)
                                FROM user_year_pei_question
                                where user_year_pei_id=:id AND uri<>'apprendimento' AND weight=1");
        $stmt->execute(['id' => $peiId]);
        $row = $stmt->fetch();
        return $row[0] == 0;
    }
    
    public static function hasToDoApprendimento($peiId){
        $stmt = EM::prepare("SELECT count(*)
                                FROM user_year_pei_question
                                where user_year_pei_id=:id AND uri='apprendimento' AND weight=1");
        $stmt->execute(['id' => $peiId]);
        $row = $stmt->fetch();
        return $row[0] > 0;
    }
}
