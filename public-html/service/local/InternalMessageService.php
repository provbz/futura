<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of IcfService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class InternalMessageService {
    
    public static function find($id){
        return EM::execQuerySingleResult("SELECT *
            FROM internal_message
            WHERE internal_message_id=:id", [
                "id" => $id
            ]);
    }
    
    public static function findModulesForMessage($id){
        return EM::execQuery("SELECT *
            FROM internal_message_module
            WHERE internal_message_id=:id", [
                "id" => $id
            ]);
    }

    public static function findVisibleMessages($user, $limit = 0){
        $visibleActions = "";
        $qParams = [];

        $i = 0;
        foreach (ModuleBaseAction::getConstants() as $name => $value) {
            if (UserRoleService::canCurrentUserDo($value)){
                $pName = "m_" . $i;

                $visibleActions .= $visibleActions == "" ? "" : ", ";
                $visibleActions .= ":" . $pName;
                $qParams[$pName] = $value;
                $i++;
            }
        }

        if (count($qParams) == 0){
            return null;
        }

        $query = "SELECT ime.*
            FROM internal_message ime
            WHERE ime.internal_message_id IN (
                SELECT DISTINCT(im.internal_message_id)
                FROM internal_message im
                LEFT JOIN internal_message_module imm ON im.internal_message_id=imm.internal_message_id
                WHERE imm.module_base_action IN (" . $visibleActions . ")
            ) 
            AND (visible_from_date IS NULL OR visible_from_date <= NOW())
            AND (visible_to_date IS NULL OR visible_to_date >= NOW())
            ORDER BY ime.insert_date desc
        ";

        if ($limit != 0){
            $query .= 'LIMIT 0,' . $limit;
        }

        return EM::execQuery($query, $qParams);
    }

    public static function getUserVisibleSections(){
        $sections = [
            ModuleBaseAction::PEI_PDP => "PEI - PDP",
            ModuleBaseAction::DROP_OUT => "Drop out",
            ModuleBaseAction::TEST_DIDATTICI => "Mondo delle parole progetto letto scrittura",
            ModuleBaseAction::PORTFOLIO_INSEGNANTI => "Portfolio docenti",
            ModuleBaseAction::EDUCAZIONE_SALUTE => "Educazione salute",
            ModuleBaseAction::MODELLO_E => "Modello E"
        ];

        $visibleSections = [];

        foreach ($sections as $key => $label) {
            if (UserRoleService::canCurrentUserDo($key)){
                $visibleSections[$key] = $label;
            }
        }

        return $visibleSections;
    }

    public static function isMessageVisible($messageId){
        $visibleSections = self::getUserVisibleSections();

        $modules = InternalMessageService::findModulesForMessage($messageId);

        foreach ($modules as $module) {
            if (isset($visibleSections[$module['module_base_action']])){
                return true;
            }
        }

        return false;
    }
}
