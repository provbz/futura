<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DropOutService {

    public static function isStructureEnabled($structure){
        return true;
    }
    
    public static function findRow($id){
        return EM::execQuerySingleResult("SELECT * 
            FROM drop_out_row 
            WHERE drop_out_row_id=:id", [
                "id" => $id
            ]);
    }

    public static function findRows($documentId){
        return EM::execQuerySingleResult("SELECT * 
            FROM drop_out_row 
            WHERE user_year_document_id=:id", [
            "id" => $id
        ]);
    }
    
    public static function setStatus($documentId, $status){
        EM::updateEntity("user_year_document", [
            "drop_out_status" => $status
        ], [
            "user_year_document_id" => $documentId
        ]);
    }

    public static function insertUserTranser($documentId, $toStructureId){
        $currentUser = UserService::getLoggedUser();
        $schoolYear = SchoolYearService::findCurrentYear();

        EM::insertEntity("drop_out_row", [
            "user_year_document_id" => $documentId,
            "status" => RowStatus::READY,
            "plesso_structure_id" => $toStructureId,
            "dro_out_row_type_dictionary_id" => DropOutAzioneKey::TRASFERIMENTO_DICTIONARY_ID,
            "insert_date" => "NOW()",
            "insert_user_id" => $currentUser["user_id"],
            "last_edit_date" => "NOW()",
            "last_edit_user_id" => $currentUser["user_id"],
            "school_year_id" => $schoolYear["school_year_id"],
            "type" => DropOutRowType::TRASFERIMENTO
        ]);
    }

    public static function findRowLastWithDocumentIdAndType($documentId, $schoolYearId, $type = null){
        $pars = [
            "id" => $documentId,
            "status_deleted" => RowStatus::DELETED,
            "school_year_id" => $schoolYearId
        ];

        $query = "SELECT * FROM drop_out_row
            WHERE user_year_document_id=:id AND status <> :status_deleted AND school_year_id=:school_year_id ";
        
        if ($type != null){
            $query .= 'AND type=:type ';
            $pars["type"] = $type;
        }

        $query .= "ORDER By drop_out_row_id DESC LIMIT 0, 1";

        return EM::execQuerySingleResult($query, $pars);
    }
}