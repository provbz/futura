<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/../../template/test/ITest.php';

class TestService {

    public static function find($id){
        return EM::execQuerySingleResult("SELECT * FROM test WHERE test_id=:id", [
            "id" => $id
        ]);
    }
    
    public static function findAll(){
        return EM::execQuery("SELECT * FROM test WHERE row_status=:row_status_ready", [
            "row_status_ready" => RowStatus::READY
        ]);
    }

    public static function remove($id){
        EM::updateEntity("test", [
            "row_status" => RowStatus::DELETED
        ], [
            "test_id" => $id
        ]);
    }

    /**
     * @var ITest
     */
    public static function loadTest($code){
        $filePath = __DIR__ . '/../../template/test/' . $code . '.php';
        if (!file_exists($filePath)){
            return null;
        }
        
        require_once $filePath;
        $test = new $code();
        return $test;
    }

    public static function findAllForClasseAndPeriodo($classDictionaryValue, $periodoDictionaryValue){
        return EM::execQuery("SELECT *
            FROM test
            WHERE class_dictionary_value=:class_dictionary_value
            AND periodo_dictionary_value=:periodo_dictionary_value", [
                "class_dictionary_value" => $classDictionaryValue,
                "periodo_dictionary_value" => $periodoDictionaryValue
            ]);
    }
}