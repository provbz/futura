<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of LogService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class LogService {
    
    public static function info($sender, $message, $ex = NULL){
        self::isertMessage(LogLevel::INFO, $sender, $message, $ex);
    }
    
    public static function warn($sender, $message, $ex = NULL){
        self::isertMessage(LogLevel::WARN, $sender, $message, $ex);
    }
    
    public static function error_($sender, $message, $ex = NULL){
        self::isertMessage(LogLevel::ERROR, $sender, $message, $ex);
    }
    
    public static function isertMessage($level, $sender, $message, $ex){
        global $currentUser;
        
        $senderName = $sender;
        if (is_object( $sender )){
            $senderName = get_class($sender);
        }
        
        $requestUrl = ArrayUtils::getIndex($_SERVER, 'REQUEST_URI');
        if (StringUtils::isNotBlank($requestUrl) && strlen($requestUrl) > 1024){
            $requestUrl = substr($requestUrl, 0, 1024);
        }

        $parameters = [
            'level' => $level,
            'sender' => $senderName,
            'message' => $message,
            'ex' => $ex,
            'request_url' => $requestUrl,
            'request_ip' => ArrayUtils::getIndex($_SERVER, 'REMOTE_ADDR'),
            'date' => 'NOW()'
        ];
        
        if ($currentUser != NULL){
            $parameters['user_id'] = $currentUser['user_id'];
        }
            
        EM::insertEntity("log", $parameters);

        if ($level == LogLevel::ERROR){
            NoticeService::persist([
                "type" => NoticeType::LOG_ERROR,
                "parameters" => json_encode(["user_id" => $parameters])
            ]);
        }
    }
    
}
