<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of JournalService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class JournalService {

    public static function findLastFor($entity, $entityId, $section){
        $query = "SELECT j.*, ue.name as ue_name, ue.surname as ue_surname
            FROM journal j
            LEFT JOIN user ue ON j.user_id = ue.user_id
            WHERE entity = :entity AND
                entity_id = :entity_id ";

        $qParams = [
            "entity" => $entity,
            "entity_id" => $entityId
        ];

        if ($section != null){
            $query .= " AND j.section = :section";
            $qParams["section"] = $section;
        }

        $query .= " ORDER BY j.insert_date DESC LIMIT 1";

        return EM::execQuerySingleResult($query, $qParams);
    }

    public static function findJournalRowsForJournalId($journalId){
        return EM::execQuery("SELECT field_name, old_value, new_value
            from journal_row
            where journal_id = :journal_id", [
                "journal_id" => $journalId
            ]);
    }

}