<?php
/* 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UriService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UriService {
    
    public static function isSecurePage(){
        return true;
    }

    public static function isPublicPage(){
        $publicPage = array('login.php', 'operation.php');

        foreach($publicPage as $key => $value){
            $page = $_SERVER['PHP_SELF'];
            if (strpos($page, $value) !== false){
                return true;
            }
        }
        return false;
    }

    public static function buildImageUrl($imageFile){
        return UriService::basePath().'/img/'.$imageFile;
    }
    
    public static function buildScriptUrl($file){
        return UriService::basePath().'/script/'.$file;
    }
    
    public static function buildCSSUrl($file){
        return UriService::basePath().'/css/'.$file;
    }

    public static function buildPageUrl($action, $method = NULL, $parameters = NULL){
        $base = UriService::basePath();
                
        if (StringUtils::isNotBlank($method) && $method != Constants::$defaultActionMethod){
            $action .= '-'.$method;
        }
        $url = "";
        if ($parameters != null && is_array($parameters)){
            foreach ($parameters as $key => $value){
                $url .= ($url == "") ? "" : "&";
                $url .= $key.'=' . _t($value);
            }
        }
        $url = ($url != "") ? $action.'?'.$url : $action;
        if (strlen($url) > 0 && $url[0] == '/'){
            $url = $base.$url;
        } else {
            $url = $base.'/'.$url;
        }
        
        return $url;
    }
    
    public static function builAbsoluteUrl(){
        return Constants::$APP_PROTOCOL . Constants::$APP_URL;
    }

    public static function basePath(){
        global $app_name;
        if (StringUtils::isBlank($app_name)){
            return "";
        } else {
            return $app_name;
        }
    }
    
    public static function buildContentPath($content){
        if ($content['type'] == ContentType::TOOL){
            $folder = Constants::$TOOLS_FOLDER;
        } else {
            $folder = Constants::$ILO_FOLDER;
        }
        $folder .= '/'. $content['code_dev'];
        return $folder;
    }
    
    public static function buildContentUri($content){
        if ($content['type'] == ContentType::TOOL){
            $folder = Constants::$TOOLS_URL;
        } else {
            $folder = Constants::$ILO_URL;
        }
        $folder .= '/' . $content['code_dev'];
        return $folder;
    }
    
    public static function buildPlayerUrl(){
        return Constants::$STATIC_CONTENT_URL.'/player/epro';
    }
    
    public static function accessDanyActionUrl(){
        return UriService::buildPageUrl("/public/AccessDenyAction");
    }
    
    public static function buildStructureUploadPath($structureId, $toolId){
        $path = Constants::$STRUCTURE_UPLOAD_FOLDER;
        if (StringUtils::isNotBlank( $structureId )){
            $path .= '/'.$structureId;
        }
        
        if (StringUtils::isNotBlank( $toolId )){
            $path .= '/' . $toolId;
        }
        
        return $path;
    }
    
    public static function buildAttachmentPath(){
        $path = Constants::$ATTACHMENTS_DIR;
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        return $path;
    }
    
    public static function buildAttachmentTmpPath(){
        $path = Constants::$ATTACHMENTS_DIR . "/tmp";
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        return $path;
    }
    
    public static function buildUserAttachmentPath($userId){
        $path = Constants::$ATTACHMENTS_DIR . "/user/" . $userId;
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        return $path;
    }
    
    public static function buildAttachmentUrl($attachment, $attachmentEntity){
        if ($attachment["status"] == AttachmentStatus::TEMP){
            return "/public/api/file/GetAction?Id=" . $attachment['attachment_id'];
        } else {
            return "/public/api/file/GetAction?aeId=" . $attachmentEntity['attachment_entity_id'];
        }
    }
}
