<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

use function GuzzleHttp\json_encode;

/**
 * Description of StructureService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class StructureService {
    
    public static function getCurrentStructureId(){
        return ArrayUtils::getIndex($_SESSION, GlobalSessionData::SELECTED_STRUCTURE_ID);
    }

    public static function find($structured){
        $stmt = EM::prepare ("SELECT * FROM structure WHERE structure_id=:structure_id");
        $stmt->execute(['structure_id' => $structured]);
        return $stmt->fetch();
    }
    
    public static function findRootByName($name){
        $query = "SELECT * FROM structure WHERE name=:name AND parent_structure_id IS NULL";
        return EM::execQuerySingleResult($query, [
            'name' => $name
        ]);
    }

    public static function findUserStructure($userId, $structureId){
        return EM::execQuerySingleResult("SELECT * FROM user_structure WHERE 
                            structure_id=:structure_id AND
                            user_id=:user_id", [
                                'structure_id' => $structureId,
                                'user_id' => $userId
                            ]);
    }

    public static function findUserStructureRole($userId, $structureId){
        return EM::execQuery("SELECT * 
            FROM user_structure us 
            LEFT JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id
            WHERE structure_id=:structure_id AND user_id=:user_id", [
                'structure_id' => $structureId,
                'user_id' => $userId
            ]);
    }

    public static function findUserStructureWithParent($userId, $parentStructureId){
        return EM::execQuery("SELECT * 
            FROM user_structure us
            LEFT JOIN structure s ON us.structure_id=s.structure_id
            WHERE user_id=:user_id 
            AND s.parent_structure_id=:parent_structure_id", [
                'user_id' => $userId,
                'parent_structure_id' => $parentStructureId
            ]);
    }

    /**
     * 
     * @param int $structured
     */
    public static function findUserStructureForStructureId($structureId){
        return EM::execQuery("SELECT * 
            FROM user_structure us
            LEFT JOIN user u ON us.user_id = u.user_id
            LEFT JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id
            WHERE structure_id=:structure_id
            ORDER BY surname, name", [
                "structure_id" => $structureId
            ]);
    }

    public static function findUserStructureForUserIdId($userId){
        return EM::execQuerySingleResult("SELECT * 
            FROM user_structure us
            WHERE user_id=:user_id", [
                "user_id" => $userId
            ]);
    }

    public static function findUserStructureWithRole($structureId, $roleId){
        return self::findUserStructureWithRoles($structureId, [$roleId]);
    }

    /**
     * 
     * @param int $structured
     * @param int $roleIds
     */
    public static function findUserStructureWithRoles($structureId, $roleIds){
        $in  = str_repeat('?,', count($roleIds) - 1) . '?';
        $par = array_merge([$structureId], $roleIds);

        $query = "SELECT *
            FROM user_structure us
            LEFT JOIN user u ON us.user_id=u.user_id
            LEFT JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id
            WHERE structure_id=? AND role_id IN ($in)
            and u.status='enabled'
            ORDER BY u.surname, u.name";

        return EM::execQuery($query, $par);
    }

    public static function findUserStructureWithoutRoles($structureId, $roleIds){
        $in  = str_repeat('?,', count($roleIds) - 1) . '?';
        $par = array_merge([$structureId], $roleIds);

        $query = "SELECT *
            FROM user_structure us
            LEFT JOIN user u ON us.user_id=u.user_id
            LEFT JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id
            WHERE structure_id=? AND role_id NOT IN ($in)
            and u.status='enabled'
            ORDER BY u.surname, u.name";

        return EM::execQuery($query, $par);
    }

    public static function findUserStructureForUserStructureAndRole($userId, $structureId, $roleId){
        return EM::execQuerySingleResult("SELECT * 
            FROM user_structure us
            LEFT JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id
            WHERE structure_id=:structure_id AND user_id=:user_id AND role_id=:role_id", [
                "structure_id" => $structureId,
                "user_id" => $userId,
                "role_id" => $roleId
            ]);
    }
    
    public static function findAllUserStructureForUserParentStructureAndRole($userId, $parentStructureId, $roleId){
        return EM::execQuery("SELECT * 
            FROM user_structure us
            LEFT JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id
            WHERE parent_structure_id=:structure_id AND user_id=:user_id AND role_id=:role_id", [
                "structure_id" => $parentStructureId,
                "user_id" => $userId,
                "role_id" => $roleId
            ]);
    }

    public static function findUserWithRole($roleId){
        return EM::execQuery("SELECT * 
            FROM user_structure us
            LEFT JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id
            WHERE role_id=:role_id", [
            "role_id" => $roleId
        ]);
    }
    
    public static function findUserStructuresRoot($userId){
        return EM::execQuery("SELECT s.structure_id, s.name, s.meccanografico
                FROM user_structure us 
                LEFT JOIN structure s ON us.structure_id= s.structure_id 
                WHERE user_id=:user_id AND us.parent_structure_id IS NULL
                    AND s.enabled = 1
                ORDER BY s.name", [
                    "user_id" => $userId
                ]);
    }

    public static function findUserStructures($userId){
        return EM::execQuery("SELECT s.structure_id, s.name
                FROM user_structure us 
                LEFT JOIN structure s ON us.structure_id= s.structure_id 
                WHERE user_id=:user_id AND s.enabled = 1
                ORDER BY s.name", [
                    "user_id" => $userId
                ]);
    }
    
    public static function createStructure($ownerUserId, $type){
        $structureId = EM::insertEntity("structure", [
            "insert_user_id" => $ownerUserId,
            "insert_date" => "NOW()",
            "enabled" => 1,
            "type" => $type
        ]);
        return $structureId;
    }
    
    /**
     * Remove a structure
     * 
     * @param int $structureId
     */
    public static function remove($structureId){
        $query = sprintf("DELETE FROM structure WHERE structure_id=%d", $structureId);
        EM::execQuery($query);
    }
    
    /**
     * Check if a user is associated to a structure
     * 
     * @param type $userId
     * @param type $structureId
     * @return boolean
     */
    public static function hasUserStructure($userId, $structureId, $exludeAdmin = false){
        if (!$exludeAdmin && UserRoleService::hasUserRole($userId, UserRole::ADMIN)){
            return true;
        }
        
        $res = EM::execQuerySingleResult("SELECT COUNT(*) c FROM user_structure 
                                    WHERE user_id=:user_id 
                                        AND structure_id=:structure_id", [
                                       "user_id" => $userId,
                                        "structure_id" => $structureId
                                    ]);
        return $res['c'] > 0;
    }
    
    public static function hasUserParentStructure($userId, $structureId, $exludeAdmin = false){
        if (!$exludeAdmin && UserRoleService::hasUserRole($userId, UserRole::ADMIN)){
            return true;
        }
        
        $res = EM::execQuerySingleResult("SELECT COUNT(*) c FROM user_structure 
                                    WHERE user_id=:user_id 
                                        AND parent_structure_id=:structure_id", [
                                       "user_id" => $userId,
                                        "structure_id" => $structureId
                                    ]);
        return $res['c'] > 0;
    }

    public static function hasUserStructureRole($userId, $structureId, $roleId){
        return self::hasUserStructureRoleAnyOf($userId, $structureId, [$roleId]);
    }
    
    public static function hasUserStructureRoleAnyOf($userId, $structureId, $roleIds){
        $userStructures = self::findUserStructureRole($userId, $structureId);
        foreach ($userStructures as $userStructure){
            foreach($roleIds as $roleId){
                if ($userStructure['role_id'] == $roleId){
                    return true;
                }
            }
        }
        return false;
    }
    
    public static function removeUserStructure($userId, $structureId){
        EM::deleteEntity("user_structure", [
            "structure_id" => $structureId,
            "user_id" => $userId,
        ]);

        EM::deleteEntity("user_structure", [
            "parent_structure_id" => $structureId,
            "user_id" => $userId,
        ]);
    }
    
    public static function removeUserStructureRole($userId, $structureId, $roleId){
        $userStructure = self::findUserStructure($userId, $structureId);
        if ($userStructure == null){
            return;
        }

        EM::deleteEntity("user_structure_role", [
            "user_structure_id" => $userStructure["user_structure_id"],
            "role_id" => $roleId
        ]);
    }

    /**
     * Add a user to a structure
     * 
     * @param type $userId
     * @param type $structureId
     * @param array $roleIds
     */
    public static function addUserStructure($userId, $structureId, $roleIds = null, $saveNotice = true){
        global $currentUser;
        
        $structure = self::find($structureId);
        if ($structure == null){
            return;
        }

        $entity = [
            "structure_id" => $structureId,
            "parent_structure_id" => $structure["parent_structure_id"],
            "user_id" => $userId,
            "insert_user_id" => $currentUser['user_id'],
            "insert_date" => "NOW()"
        ];
        $userStructureId = EM::insertEntity("user_structure", $entity);

        if ($roleIds != null){
            foreach ($roleIds as $roleId) {
                self::addUserStructureRole($userStructureId, $roleId);
            }
        }
        
        if ($saveNotice){
            NoticeService::persist([
                "type" => NoticeType::USER_STRUCTURE_ADD,
                "parameters" => json_encode($entity)
            ]);
        }

        return $userStructureId;
    }

    public static function deleteAlLUserStructureRole($userStructureId){
        EM::deleteEntity("user_structure_role", [
            "user_structure_id" => $userStructureId
        ]);
    }

    public static function addUserStructureRole($userStructureId, $roleId){
        EM::insertEntity("user_structure_role", [
            "user_structure_id" => $userStructureId,
            "role_id" => $roleId
        ]);
    }

    public static function findAll() {
        return EM::execQuery("SELECT * FROM structure ORDER By name");
    }

    public static function findWithParentWithName($structureId, $name){
        return EM::execQuerySingleResult("SELECT * 
            FROM structure 
            WHERE parent_structure_id=:parent_structure_id AND enabled=1
                AND name=:name", [
                ":parent_structure_id" => $structureId,
                ":name" => $name
            ]);
    }

    public static function findAllWithParent($structureId){
        return EM::execQuery("SELECT * 
            FROM structure 
            WHERE parent_structure_id=:parent_structure_id AND enabled=1
            ORDER By name", [
                ":parent_structure_id" => $structureId
            ]);
    }

    public static function findAllRoot() {
        return EM::execQuery("SELECT * 
            FROM structure 
            WHERE parent_structure_id IS NULL 
                AND enabled = 1
            ORDER By name");
    }

    public static function findAllRootAsDictionary($addEmpty = false) {
        $results = [];
        if ($addEmpty){
            $results[""] = "";
        }
        
        $structures = self::findAllRoot();
        foreach ($structures as $structure){
            $results[$structure["structure_id"]] = $structure['name'];
        }
        return $results;
    }

    public static function findAllAsDictionary($addEmpty = false) {
        $results = [];
        if ($addEmpty){
            $results[""] = "";
        }
        
        $structures = self::findAll();
        foreach ($structures as $structure){
            $results[$structure["structure_id"]] = $structure['name'];
        }
        return $results;
    }

    public static function printFullStructureName($structureId){
        $res = "";

        $structure = self::find($structureId);
        if ($structure == null){
            return $res;
        }
        
        if ($structure["parent_structure_id"] != null){
            $res .= self::printFullStructureName($structure["parent_structure_id"]);
            $res .= " - " . $structure["name"];
        } else {
            $res = $structure["name"];
        }

        return $res;
    }

    public static function addStructurePermissions(){
        global $currentUser, 
            $currentUserAllowedAction,
            $currentUserRoles;

        if (!isset($_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID]) || $_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID] == null){
            return;
        }

        $parentStructureId = $_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID];
        self::addStructurePermissionsWithStructureId($parentStructureId);
        $childStructures = self::findUserStructureWithParent($currentUser["user_id"], $parentStructureId);
        foreach($childStructures as $structure){
            self::addStructurePermissionsWithStructureId($structure["structure_id"], false);
        }

        StructureClassService::addStructureClassPermission($parentStructureId);
    }

    private static function addStructurePermissionsWithStructureId($structureId, $requireRoles = true){
        global $currentUser, 
            $currentUserAllowedAction,
            $currentUserRoles;

        $userStructureRoles = StructureService::findUserStructureRole($currentUser['user_id'], $structureId);
        if ($requireRoles && !UserRoleService::canCurrentUserDo(UserPermission::STRUCTURE_ACCESS_ALL) &&  $userStructureRoles->rowCount() == 0){
            $_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID] = NULL;
            redirect(UriService::accessDanyActionUrl());
        }

        foreach ($userStructureRoles as $userStructure){
            $role = UserRoleService::find($userStructure["role_id"]);
    
            $currentUserRoles[$userStructure["role_id"]] = $role;
            $additionalActions = UserRoleService::findActionForRole($userStructure['role_id']);
            foreach ($additionalActions as $value){
                $currentUserAllowedAction[$value] = "";
            }
        }
    }

    public static function findByMeccanografico($meccanografico){
        return EM::execQuerySingleResult("SELECT * 
            FROM structure
            WHERE meccanografico=:meccanografico
            LIMIT 0, 1", [
            "meccanografico" => $meccanografico
        ]);
    }

    public static function findByName($name){
        return EM::execQuerySingleResult("SELECT * 
            FROM structure
            WHERE name=:name
            LIMIT 0, 1", [
            "name" => $name
        ]);
    }
}
