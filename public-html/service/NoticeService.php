<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of NoticeService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class NoticeService {
    
    public static $sender = "NoticeService";

    public static function find($id){
        return EM::execQuerySingleResult("SELECT * FROM notice WHERE notice_id=:id", ['id' => $id]);
    }
    
    public static function findNotEvaluated(){
        return EM::execQuery("SELECT * FROM notice WHERE evaluated=0 OR evaluated IS NULL");
    }

    public static function noticeExists($entity, $entityId, $type) {
        $res = EM::execQuerySingleResult("SELECT 1 
            FROM notice 
            WHERE (evaluated=0 OR evaluated IS NULL)
                AND type=:type 
                AND entity=:entity
                AND entity_id=:entity_id", [
                    "type" => $type,
                    "entity" => $entity,
                    "entity_id" => $entityId
                ]);

        return $res != null;
    }
    
    public static function evaluateNotice(){
        $notices = self::findNotEvaluated();
        
        foreach ($notices as $notice){
            try{
                self::evaluateNoticeSingle($notice);
            } catch (Exception $ex){
                LogService::error_(self::$sender, "Exception executing notice " . $notice['notice_id'] . " type: " . $notice["type"], $ex);
            }
        }
    }
    
    public static function findNoticeUserConf($type){
        return EM::execQuery("SELECT * FROM notice_user_conf WHERE notice_type=:type", [
            'type' => $type
        ]);
    }
    
    public static function evaluateNoticeSingle($notice){
        self::processCompulsoryNotice($notice);
        self::processNoticeUserConf($notice);
        
        EM::updateEntity("notice", ['evaluated' => 1], "notice_id=".$notice['notice_id']);
    }
    
    private static function processCompulsoryNotice($notice){
        $schoolYearId = SchoolYearService::findCurrentYear();
        $par = null;
        if (StringUtils::isNotBlank($notice['parameters'])){
            $par = json_decode( $notice['parameters'], true );
        }
        
        $noticeUser = [
            "notice_id" => $notice['notice_id'],
            "channel" => NoticeUserChannel::EMAIL,
            "evaluated" => 0,
            "insert_date" => "NOW()",
            "type" => NoticeUserType::IMMEDIATE
        ];
        
        switch ($notice['type']){
            case NoticeType::USER_RETRIVE_DATA:
                $noticeUser['user_id'] = $par['user_id'];    
                self::persistNoticeUser($noticeUser);
                break;
            case NoticeType::USER_ADD:
                $noticeUser['user_id'] = $par['user_id'];    
                self::persistNoticeUser($noticeUser);
                break;
            case NoticeType::USER_ACCESS:
                $noticeUser["user_id"] = $notice["entity_id"];
                $noticeUser["type"] = NoticeUserType::DIGEST;
                self::persistNoticeUser($noticeUser);
                break;
            case NoticeType::USER_STRUCTURE_ADD: 
                if (StructureService::hasUserStructureRole($par['user_id'], $par["structure_id"], UserRole::STUDENTE)){
                    break;
                }
                if ($par['user_id'] == $par['insert_user_id']){
                    break;
                }
                $noticeUser['user_id'] = $par['user_id'];    
                self::persistNoticeUser($noticeUser);
                break;
            case NoticeType::USER_USER_ADD:
                $noticeUser['user_id'] = $par['to_user_id'];  
                $noticeUser['type'] = NoticeUserType::DIGEST;
                self::persistNoticeUser($noticeUser);
                break;
            case NoticeType::USER_TRANSFER_ADD:
                $structureId = 0;
                if ($par['type'] == UserTransferRequestType::FROM_DESTINATION){
                    $strutureId = $par['from_structure_id'];
                } else if ($par['type'] == UserTransferRequestType::FROM_SOURCE) {
                    if ($schoolYearId['school_year_id'] == $par['school_year_id']){
                        break;
                    }
                    $strutureId = $par['to_structure_id'];
                }
                
                $noticeUser["type"] = NoticeUserType::DIGEST;
                self::explodeToStructureUser($noticeUser, $strutureId, UserRole::DIRIGENTE);
                self::explodeToStructureUser($noticeUser, $strutureId, UserRole::AMMINISTRATIVI);
                self::explodeToStructureUser($noticeUser, $strutureId, UserRole::REFERENTE_BES);
                break;
            case NoticeType::USER_TRANSFER_UPDATE:
                $noticeUser['user_id'] = $par['requester_user_id'];
                $noticeUser["type"] = NoticeUserType::DIGEST;
                
                if ($par['type'] == UserTransferRequestType::FROM_DESTINATION){
                    self::persistNoticeUser($noticeUser);
                } else if ($par['type'] == UserTransferRequestType::FROM_SOURCE) {
                    self::explodeToStructureUser($noticeUser, $par['to_structure_id'], UserRole::DIRIGENTE);    
                    self::explodeToStructureUser($noticeUser, $par['to_structure_id'], UserRole::AMMINISTRATIVI);    
                    self::explodeToStructureUser($noticeUser, $par['to_structure_id'], UserRole::REFERENTE_BES);    
                }
                break;
            case NoticeType::USER_DIAGNOSI_NEW:
                $structures = StructureService::findUserStructures($notice['entity_id']);
                foreach ($structures as $structure){
                    self::explodeToStructureUser($noticeUser, $structure['structure_id'], UserRole::REFERENTE_BES, [$par['insert_user_id']]);
                }

                $toUsers = UserUserService::findForUserId($notice['entity_id']);
                foreach ($toUsers as $toUser){
                    if ($toUser['user_id'] == $par['insert_user_id']){
                        continue;
                    }
                    
                    $noticeUser['user_id'] = $toUser['to_user_id'];
                    self::persistNoticeUser($noticeUser);
                }

                break;
            case NoticeType::PORTFOLIO_CLOSING:
                $usersToNotify = StructureService::findUserWithRole(UserRole::INSEGNANTE_ANNO_DI_PROVA);
                foreach ($usersToNotify as $user){
                    $noticeUser['user_id'] = $user['user_id'];  
                    $noticeUser['type'] = NoticeUserType::IMMEDIATE;
                    self::persistNoticeUser($noticeUser);
                }
                break;
            case NoticeType::STRUCTURE_CHECK_USERS:
                $roles = [UserRole::DIRIGENTE, UserRole::AMMINISTRATIVI];
                foreach ($roles as $role) {
                    $usersToNotify = StructureService::findUserWithRole($role);
                    foreach($usersToNotify as $user){
                        $noticeUser['user_id'] = $user['user_id'];  
                        $noticeUser['type'] = NoticeUserType::IMMEDIATE;
                        self::persistNoticeUser($noticeUser);
                    }
                }
                break;
            case NoticeType::DOCUMENTS_DELETE_WARNING:
                $roles = [
                    UserRole::DIRIGENTE, 
                    UserRole::AMMINISTRATIVI,
                    UserRole::REFERENTE_BES
                ];

                $structureIds = UserService::findStructuresWithdUserYearToRemove();
                foreach($structureIds as $structureId){
                    foreach($roles as $roleId){
                        $users = StructureService::findUserStructureWithRole($structureId["structure_id"], $roleId);
                        foreach($users as $user){
                            $noticeUser['user_id'] = $user['user_id'];  
                            $noticeUser['type'] = NoticeUserType::DIGEST;
                            self::persistNoticeUser($noticeUser);
                        }
                    }
                }
                break;
            case NoticeType::USER_DELETED:
                $roles = [UserRole::ADMIN, UserRole::AMMINISTRATORE_PEI_PDP];
                foreach ($roles as $role) {
                    $usersToNotify = StructureService::findUserWithRole($role);
                    foreach($usersToNotify as $user){
                        $noticeUser['user_id'] = $user['user_id'];  
                        $noticeUser['type'] = NoticeUserType::DIGEST;
                        self::persistNoticeUser($noticeUser);
                    }
                }
                break;
            case NoticeType::E_MODEL_EDIT:
                {
                    $usersToNotify = UserService::findWithRole(UserRole::AMMINISTRATORE_MODELLO_E);
                    foreach($usersToNotify as $user){
                        $noticeUser['user_id'] = $user['user_id'];  
                        $noticeUser['type'] = NoticeUserType::DIGEST;
                        self::persistNoticeUser($noticeUser);
                    }
                }
                break;
            case NoticeType::E_MODEL_APPROVED:
                {
                    $eModel = EModelService::find($notice["entity_id"]);
                    $usersToNotify = UserService::findStructureRole($eModel["structure_id"], UserRole::REFERENTE_MODELLO_E);
                    foreach($usersToNotify as $user){
                        $noticeUser['user_id'] = $user['user_id'];  
                        $noticeUser['type'] = NoticeUserType::DIGEST;
                        self::persistNoticeUser($noticeUser);
                    }
                }
                break;
            case NoticeType::DROP_OUT_NEW_DOCUMENT:
                $usersToNotify = UserService::findWithRole(UserRole::AMMINISTRATORE_DROP_OUT);
                foreach($usersToNotify as $user){
                    $noticeUser['user_id'] = $user['user_id'];  
                    $noticeUser['type'] = NoticeUserType::DIGEST;
                    self::persistNoticeUser($noticeUser);
                }
                break;
            case NoticeType::DROP_OUT_NEW_ROW:
                $usersToNotify = UserService::findWithRole(UserRole::AMMINISTRATORE_DROP_OUT);
                foreach($usersToNotify as $user){
                    $noticeUser['user_id'] = $user['user_id'];  
                    $noticeUser['type'] = NoticeUserType::DIGEST;
                    self::persistNoticeUser($noticeUser);
                }
                break;
            case NoticeType::MONDO_PAROLE_CAMPO_NOTE:
                $strc = SomministrazioneTestResultClassService::find($notice["entity_id"]);
                $insegnanti = StructureClassUserService::FindWithRole($strc["structure_class_id"], UserRole::INSEGNANTE_SOMMINISTRAZIONI);
                $usersToNotify = $insegnanti->fetchAll();

                $class = StructureClassService::find($strc["structure_class_id"]);
                $referentiPlesso = StructureService::findUserStructureWithRoles($class["structure_id"], [
                    UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI
                ]);

                foreach ($referentiPlesso as $referente) {
                    $grades = MetadataService::findMetadataValue(MetadataEntity::USER, $referente["user_id"], UserMetadata::SCHOOL_GRADE);
                    foreach ($grades as $grade) {
                        if($grade['value'] == SchoolGrade::INFANZIA && strpos($class['class_dictionary_value'], "I") == 1){
                            $usersToNotify[] = $referente;
                        } else if($grade['value'] == SchoolGrade::PRIMARIA && strpos($class['class_dictionary_value'], "P") == 1){
                            $usersToNotify[] = $referente;
                        }
                    }
                }

                $structure = StructureService::find($class["structure_id"]);
                $referentiIstituto = StructureService::findUserStructureWithRole($structure["parent_structure_id"], UserRole::REFERENTE_ISTITUTO_SOMMINISTRAZIONI);
                foreach ($referentiIstituto as $referente) {
                    $grades = MetadataService::findMetadataValue(MetadataEntity::USER, $referente["user_id"], UserMetadata::SCHOOL_GRADE);
                    foreach ($grades as $grade) {
                        if($grade['value'] == SchoolGrade::INFANZIA && strpos($class['class_dictionary_value'], "I") == 1){
                            $usersToNotify[] = $referente;
                        } else if($grade['value'] == SchoolGrade::PRIMARIA && strpos($class['class_dictionary_value'], "P") == 1){
                            $usersToNotify[] = $referente;
                        }
                    }
                }
                
                foreach($usersToNotify as $user){
                    $noticeUser['user_id'] = $user['user_id'];  
                    $noticeUser['type'] = NoticeUserType::DIGEST;
                    self::persistNoticeUser($noticeUser);
                }
                break;
        }
    }
    
    private static function explodeToStructureUser($noticeUser, $structureId, $roleId, $noUserId = []){
        $usersStructure = StructureService::findUserStructureWithRole($structureId, $roleId);
        foreach ($usersStructure as $userStructure){
            if (in_array($userStructure['user_id'], $noUserId)){
                continue;
            }

            $noticeUser['user_id'] = $userStructure['user_id'];
            self::persistNoticeUser($noticeUser);
        }
    }
    
    private static function notifyToAllUsers($noticeUser, $usersIds, $excludeUsersIds = null){
        foreach ($usersIds as $userId => $value){
            if ($userId == null || ($excludeUsersIds != null && isset($excludeUsersIds[$userId]))){
                continue;
            }
            
            $noticeUser['user_id'] = $userId;
            self::persistNoticeUser($noticeUser);
        }
    }
    
    private static function processNoticeUserConf($notice){
        $noticeUser = [
            "notice_id" => $notice['notice_id'],
            "evaluated" => 0
        ];
        
        $confs = self::findNoticeUserConf($notice["type"]);
        foreach ($confs as $conf){
            if ($notice['type'] != $conf["notice_type"]){
                continue;
            }
            
            $noticeUser['user_id'] = $conf['user_id'];
            $noticeUser["channel"] = $conf['channel'];
            $noticeUser["type"] = $conf["send_policy"];
            self::persistNoticeUser($noticeUser);
        }
    }
    
    public static function persistNoticeUser($noticeUser){
        $user = UserService::find($noticeUser["user_id"]);
        if ($user == null || $user["status"] != UserStatus::ENABLED){
            return;
        }

        $noticeUser['insert_date'] = "NOW()";
        EM::insertEntity("notice_user", $noticeUser);
    }
 
    public static function persist($notice){
        $notice['insert_date'] = "NOW()";
        $notice['evaluated'] = 0;
        EM::insertEntity("notice", $notice);
    }
}