<?php
/* 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserService {

    public static $PASSWORD_SECRET = 'erickson_2011';
    
    public static function checkUserName($username, $userId = ""){
        if (StringUtils::isBlank($username)){
            return false;
        }
        
        $query = "SELECT COUNT(*) as c FROM user WHERE username=:username";
        $par['username'] = $username;
        if (StringUtils::isNotBlank($userId)){
            $query .= " AND user_id <> :user_id";
            $par['user_id'] = $userId;
        }
        
        $stmt = EM::prepare($query);
        $stmt->execute($par);
        $row = $stmt->fetch();
        if ($row['c'] > 0){
            return false;
        }
        return true;
    }
    
    public static function find($userId){
        $stmt = EM::prepare("SELECT * FROM user WHERE user_id=:user_id");
        $stmt->execute(['user_id' => $userId]);
        return $stmt->fetch();
    }
    
    public static function findByCode($code){
        return EM::execQuerySingleResult("SELECT * FROM user WHERE code=:code", [
            'code' => $code
        ]);
    }

    public static function findByNameSurnameAndBirthDate($name, $surname, $birthDate){
        return EM::execQuerySingleResult("SELECT * FROM user WHERE 
            `name`=:name AND
            surname=:surname AND
            birth_date=:birth_date", [
                'name' => $name,
                'surname' => $surname,
                'birth_date' => $birthDate
        ]);
    }
    
    public static function findByOtp($otp){
        if (StringUtils::isBlank($otp) || strlen($otp) != 20){
            return NULL;
        }
        
        $stmt = EM::prepare("SELECT * FROM user WHERE ota=:ota");
        $stmt->execute(['ota' => $otp]);
        return $stmt->fetch();
    }

    public static function encodePassword($password){
        return hash ('md5', hash('md5', $password) . Constants::$cookieSectret );
    }
    
    public static function findUserByPassword($username, $password){
        $stmt = EM::prepare("SELECT * FROM user WHERE username=:username AND password=:password");
        $stmt->execute(['username' => $username, 'password' => self::encodePassword($password) ]);
        return $stmt->fetch();
    }
    
    public static function findUserByEmailAndPassword($email, $password){
        $query = "SELECT * FROM user WHERE LOWER(TRIM(email))=:email AND password=:password";
        return EM::execQuerySingleResult($query, [
            'email' => strtolower(trim($email)), 
            'password' => self::encodePassword($password) 
        ]);
    }
    
    public static function findUserByEmail($email){
        if (StringUtils::isBlank($email)){
            return null;
        }
        
        $query = "SELECT * FROM user WHERE LOWER(TRIM(email))=:email";
        return EM::execQuerySingleResult($query, [
            'email' => strtolower(trim($email))
        ]);
    }

    public static function findUserLikeEmail($email){
        $query = "SELECT * FROM user WHERE email LIKE :email";
        return EM::execQuerySingleResult($query, [
            'email' => $email
        ]);
    }
    
    public static function findUserByOffice365Account($email){
        $query = "SELECT * FROM user WHERE LOWER(TRIM(email))=:email AND status=:status";
        return EM::execQuerySingleResult($query, [
            'email' => strtolower( trim( $email )),
            "status" => UserStatus::ENABLED
        ]);
    }
    
    public static function findUserByUsername($userName){
        $query = "SELECT * FROM user WHERE username=:username";
        return EM::execQuerySingleResult($query, [
            'username' => $userName
        ]);
    }
    
    public static function findUserByUsernameCaseInsensitive($userName){
        $query = "SELECT * FROM user WHERE LCASE(username)=:username";
        return EM::execQuerySingleResult($query, [
            'username' => strtolower( $userName ) 
        ]);
    }
    
    public static function findUserByAppKey($password){
        $stmt = EM::prepare("SELECT * FROM user WHERE password=:password");
        $stmt->execute(["password" => $password]);
        return $stmt->fetch();
    }
    
    public static function addAccess($userId){
        EM::insertEntity("user_access", [
            "user_id" => $userId,
            "date" => "NOW()",
            "ip" => $_SERVER['REMOTE_ADDR'],
            "browser" => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null
        ]);

        self::prevUserAccessMessage($userId);
        
        NoticeService::persist([
            "entity" => EntityName::USER,
            "entity_id" => $userId,
            "type" => NoticeType::USER_ACCESS
        ]);        
    }

    public static function prevUserAccessMessage($userId){
        $prevUserAccess = EM::execQuerySingleResult("SELECT * FROM notice 
            WHERE type=:user_access AND entity_id=:user_id
            ORDER BY notice_id desc
            LIMIT 0, 1", [
            ":user_access" => NoticeType::USER_ACCESS,
            ":user_id" => $userId
        ]);

        if ($prevUserAccess == null){
            return;
        }

        $_SESSION[GlobalSessionData::PREV_USER_ACCESS] = $prevUserAccess;
    }
        
    public static function updateOnlineUser(){
        global $currentUser;
        
        if ($currentUser != NULL){
            EM::updateEntity("user", ["last_seen_time" => time()], ["user_id" => $currentUser['user_id']]);
                        
            $lastUserAccess = EM::execQuerySingleResult("SELECT * 
                FROM user_access 
                WHERE user_id=:user_id 
                ORDER BY user_access_id DESC 
                LIMIT 0,1", [
                    "user_id" => $currentUser['user_id']
                ]);

            if ($lastUserAccess != NULL){
                EM::updateEntity("user_access", ["date_last_check" => "NOW()"], ["user_access_id" => $lastUserAccess['user_access_id']]);
            } else {
                EM::insertEntity("user_access", [
                    "user_id" => $currentUser['user_id'],
                    "date" => "NOW()",
                    "date_last_check" => "NOW()"
                ]);
            }
        }
    }
    
    public static function countOnline(){
        $onlineTime = time() - Constants::$maxSeenTimeDelay;
        $query = sprintf("SELECT count(*) as c FROM user WHERE last_seen_time>%d", $onlineTime);
        $row = EM::execQuerySingleResult($query);
        return $row['c'];
    }
    
    public static function findOnline(){
        $onlineTime = time() - Constants::$maxSeenTimeDelay;
        $query = sprintf("SELECT * FROM user WHERE last_seen_time > %d", $onlineTime);
        return EM::execQuery($query);
    }
    
    public static function remove($userId){
        EM::deleteEntity("user", sprintf("user_id=%d", $userId));
    }
    
    public static function countStructureRole($structureId, $roleId){
        $query = sprintf("SELECT COUNT(*) c 
                FROM user u
                LEFT JOIN user_role ur ON u.user_id=ur.user_id 
                WHERE u.structure_id=%d AND ur.role_id=%d", 
                $structureId, $roleId);
        
        $res = EM::execQuerySingleResult($query);
        return $res['c'];
    }
    
    public static function countStructureUserYear($structureId, $yearId){
        $res = EM::execQuerySingleResult("SELECT count(*)
            from user_year uy
            left join user u ON uy.user_id=u.user_id
            left join user_structure us ON us.user_id=u.user_id
            where us.structure_id=:structure_id AND school_year_id = :user_year_id", 
            [
            'structure_id' => $structureId, 
            'user_year_id' => $yearId
        ]);
        return $res[0];
    }

    public static function findStructuresWithdUserYearToRemove(){
        $currentSchoolYear = SchoolYearService::findCurrentYear();
        return EM::execQuery("SELECT distinct(structure_id)
            from user_year uy
                left join user_structure us on uy.user_id=us.user_id
                left join user_structure_role usr ON usr.user_structure_id=us.user_structure_id
                left join user_year_document uyd ON uyd.user_id=uy.user_id
            where uyd.type IN ('pei', 'pdp')
            AND usr.role_id=:student_role_id
            AND uy.user_id not in (select user_id from user_year where school_year_id=:current_school_year_id)
            order by structure_id", [
                "current_school_year_id" => $currentSchoolYear["school_year_id"],
                "student_role_id" => UserRole::STUDENTE
            ]);
    }

    public static function findStructureRole($structureId, $role, $limit = -1){
        $query = sprintf("SELECT * FROM user u 
                left join user_structure us ON us.user_id=u.user_id
                left join user_structure_role usr ON us.user_structure_id=usr.user_structure_id
                WHERE us.structure_id=%d 
                AND usr.role_id=%d 
                ORDER BY surname, name DESC ", 
                $structureId, $role);
        if ($limit != -1){
            $query .= sprintf(" LIMIT 0,%d", $limit);
        }
        return EM::execQuery($query);
    }
    
    public static function getLoggedUser(){
        global $requestPackage;

        if (strpos($requestPackage, "/api/") === 0){
            return self::getLoggedUserApi();
        }

        $currentUser = null;
        $userId = getSessionValue("user_id");
        $sid = getSessionValue("sid");
        
        if (StringUtils::isNotBlank( $userId ) && StringUtils::isNotBlank( $sid )){
            $userId = $_SESSION['user_id'];
            $sid = $_SESSION['sid'];
        } else if (isset($_COOKIE['user_id']) && StringUtils::isNotBlank($_COOKIE['user_id']) &&
                    isset($_COOKIE['sid']) && StringUtils::isNotBlank($_COOKIE['sid'])){
            $userId = $_COOKIE['user_id'];
            $sid = $_COOKIE['sid'];
        }
        
        if (StringUtils::isNotBlank($userId) && StringUtils::isNotBlank($sid)){
            $currentUser = UserService::find($userId);
            if ($sid != md5($currentUser['password'] . Constants::$cookieSectret)){
                $currentUser = null;
            }
        }

        return $currentUser;
    }

    private static function getLoggedUserApi(){
        $headers = getallheaders();
        
        foreach($headers as $key => $value){
            if ($key == 'X-Apikey'){
                return self::findByApiKey($value);
            }
        }

        return null;
    }

    public static function findByApiKey($apiKey){
        return EM::execQuerySingleResult("SELECT * 
            FROM user
            WHERE api_key = :api_key", [
                "api_key" => $apiKey
            ]);
    }

    public static function findByToken($token){
        return EM::execQuerySingleResult("SELECT * 
            FROM user
            WHERE token = :token", [
                "token" => $token
            ]);
    }

    public static function setLoggedUser($user, $remember = false, $addNotice = true){
        //Disabled user
        if ($user['status'] != UserStatus::ENABLED){
            throw new Exception("Profilo utente disattivato.");
        }
        
        //Set logged user
        self::updateUserSessionLogin($user);
        if ($addNotice){
            UserService::addAccess($user['user_id']);
        }
        
        if(StringUtils::isBlank($user['date_first_access'])){
            EM::updateEntity("user", ["date_first_access" => "NOW()"], ["user_id" => $user['user_id']] );
        }
        
        EM::updateEntity("user", [
            "ota" => null
        ], [ "user_id" => $user['user_id'] ]);

        self::CreateAndGetToken($user['user_id']);
        
        //Remember me
        if ($remember){
            $expTime = time() + 60 * 60 * 24 * 7;
            setcookie("user_id", $user['user_id'], $expTime);
            setcookie("sid", md5($user['password'] . Constants::$cookieSectret), $expTime);
        }
    }
    
    public static function updateUserSessionLogin($user){
        $_SESSION['user_id'] = $user['user_id'];
        $_SESSION['sid'] = md5($user['password'] . Constants::$cookieSectret);
    }
    
    public static function generateOtp($userId){
        $otp = StringUtils::generateRandomChars();
        EM::updateEntity("user", ["one_time_access" => $otp], ["user_id" => $userId]);
        return $otp;
    }
    
    public static function logout($userId){
        EM::updateEntity("user", [
            "last_seen_time" => time() - 60,
            "token" => NULL
        ], [
            "user_id" => $userId 
        ]);
        
        unset ($_SESSION['user_id']);
        session_unset(); 
        session_destroy();
        
        setcookie("user_id");
        setcookie("sid");
    }
    
    public static function countAccess($userId){
        $row = EM::execQuerySingleResult("SELECT count(*) as c 
            FROM user_access 
            WHERE user_id=:user_id", [
                "user_id" => $userId
            ]);
        return $row['c'];
    }
    
    public static function isOnline($user){
        return $user['last_seen_time'] > Time() - 60;
    }
    
    public static function buildUsername($userId){
        EM::updateEntity("user", ['username' => 'u' . $userId ], ["user_id" => $userId]);
    }
        
    public static function checkLastPassword($currentUser, $password){
        if (StringUtils::isBlank($currentUser['old_passwords'])){
            return true;
        }
        
        $passwords = json_decode($currentUser['old_passwords']);
        return !in_array( UserService::encodePassword($password), $passwords);
    }
    
    public static function updatePassword($userId, $password){
        $user = UserService::find($userId);
        $oldPasswords = $user['old_passwords'];
        if (StringUtils::isBlank( $oldPasswords )){
            $oldPasswords = [];
        } else {
            $oldPasswords = json_decode($oldPasswords);
        }
        
        $oldPasswords[] = UserService::encodePassword( $password );
        
        if (count( $oldPasswords ) > 3){
            array_shift( $oldPasswords );
        }
        
        EM::updateEntity("user", [
            "password" => UserService::encodePassword( $password ), 
            'date_last_password_change' => 'NOW()',
            'ota' => '',
            'old_passwords' => json_encode($oldPasswords)
        ], sprintf("user_id=%d", $userId));
    }
    
    public static function updateUserStatus($user_id, $status) {
        EM::updateEntity("user", [
            "status" => $status
        ], ["user_id" => $user_id]);
    }

    public static function updateLastEdit($user_id) {
        $loggedUser = UserService::getLoggedUser();

        EM::updateEntity("user", [
            "last_edit_date" => 'NOW()',
            'last_edit_user_id' => $loggedUser["user_id"]
        ], ["user_id" => $user_id]);
    }

    public static function findWithRole($roleId) {
        return EM::execQuery("SELECT * FROM user u 
                LEFT JOIN user_role ur ON u.user_id=ur.user_id
                WHERE ur.role_id=:role_id", [
                    "role_id" => $roleId
                ]);
    }

    public static function fullDelete($userId){
        $user = UserService::find($userId);
        $userYear = UserYearService::findUserYearByUser($userId);

        NoticeService::persist([
            "type" => NoticeType::USER_DELETED,
            "entity" => EntityName::USER,
            "entity_id" => $userId,
            "data" => json_decode([
                "user" => $user,
                "user_year" => $userYear
            ])
        ]);

        EM::execQuery("DELETE FROM user WHERE user_id=:user_id", [
            ":user_id" => $userId
        ]);

        AttachmentService::detachAllAttachmentEntity(EntityName::USER, $userId);
    }

    public static function CreateAndGetToken($userId){
        $token = self::GUID();

        EM::updateEntity("user", [
            "token" => $token,
            "token_date" => "NOW()"
        ], [
            "user_id" => $userId
        ]);

        return $token;
    }

    private static function GUID()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
}
