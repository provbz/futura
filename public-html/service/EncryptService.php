<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EncryptService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EncryptService {
    
    private static function pkcs7_pad($data, $size){
        $length = $size - ($data != null ? strlen($data) % $size : 0);
        return $data . str_repeat(chr($length), $length);
    }
    
    private static function pkcs7_unpad($data){
        if ($data == null){
            return $data;
        }
        return substr($data, 0, -ord($data[strlen($data) - 1]));
    }
    
    public static function encrypt($str){
        $key = base64_decode( Constants::$encrypt_key );
        $iv = base64_decode( Constants::$encript_iv );
        
        $enc = openssl_encrypt(self::pkcs7_pad($str, 16), 'AES-256-CBC', $key, 0, $iv);
        
        return base64_encode($enc);
    }
    
    public static function decrypt($str){
        if ($str == null) {   
            return $str;
        }

        $key = base64_decode( Constants::$encrypt_key );
        $iv = base64_decode( Constants::$encript_iv );
        
        $encStr = base64_decode($str);
        $decrypt = self::pkcs7_unpad(openssl_decrypt($encStr, 'AES-256-CBC', $key, 0, $iv));
        
        return $decrypt;
    }
    
    public static function encryptfile($targetFile){
        $f = fopen($targetFile,"r");
        $str = fread($f,filesize($targetFile));
        fclose($f);
        
        $str=self::encrypt($str);
        
        $fp = fopen($targetFile,"w");
        fwrite($fp, $str);
        fclose($fp); 
    }   
   
    public static function decryptfileInMemory($filePath){
        if (!file_exists($filePath)){
            LogService::error_("EncryptService", "Richiesto file non trovato " . $filePath);
            return "";
        }
        
        $f = fopen($filePath, "r");
        $str = fread($f,filesize($filePath));
        fclose($f);
        
        $decrypt = self::decrypt($str);
        return $decrypt;
    }
    
    public static function decryptfile($filePath, $newfile){
        $f = fopen($filePath, "r");
        $str = fread($f,filesize($filePath));
        fclose($f);
        
        $decrypt = self::decrypt($str);
        
        $fp = fopen($newfile, "w");
        fwrite($fp, $decrypt);
        fclose($fp);
    }
}
