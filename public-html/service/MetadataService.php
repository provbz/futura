<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MetadataService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class MetadataService {
    
    public static function findDictionaryForEntity($entity){
        $res = [];
        $query = sprintf("SELECT * FROM metadata WHERE entity='%s' ORDER BY position", $entity);
        $result = EM::execQuery($query);
        foreach($result as $row ){
            $res[$row['metadata_id']] = $row;
        }
        return $res;
    }
    
    public static function find($entity, $id){
        $stmt = EM::prepare("SELECT * FROM
            {$entity}_metadata WHERE {$entity}_id=:id");
        $stmt->execute(["id" => $id]);
        return $stmt->fetchAll();
    }
    
    public static function findMetadataValue($entity, $id, $metadataId){
        $stmt = EM::prepare("SELECT * FROM
            {$entity}_metadata 
            WHERE {$entity}_id=:id AND metadata_id=:metadata_id 
            ORDER BY position");

        $stmt->execute([
            "id" => $id,
            "metadata_id" => $metadataId
        ]);

        return $stmt->fetchAll();
    }

    public static function findDictionary($entity, $id, $encrypted = true){
        $stmt = self::find($entity, $id);
        
        $out = [];
        foreach($stmt as $meta){
            $out[$meta['metadata_id']] = $encrypted ? EncryptService::decrypt( $meta['value'] ) : $meta['value'];
        }
        return $out;
    }
    
    public static function removeMetadata($entity, $entityId){
        $stmt = EM::prepare("DELETE FROM {$entity}_metadata WHERE {$entity}_id=:id");
        $stmt->execute(['id' => $entityId]);
    }
    
    public static function removeMetadataWithKey($entity, $entityId, $metadataIds){
        $inQuery = "";
        $par = [
            'id' => $entityId
        ];
        
        $i = 0;
        foreach ($metadataIds as $key => $value){
            $inQuery .= $inQuery == '' ? '' : ', ';
            $mPlace = ':m'. $i++;
            $inQuery .= $mPlace;
            $par[$mPlace] = $key;
        }
        
        $query = "DELETE FROM {$entity}_metadata WHERE {$entity}_id=:id AND metadata_id IN (".$inQuery .")";
        $stmt = EM::prepare($query);
        $stmt->execute($par);
    }

    public static function removeMetadataWithMetadataIdAndValue($entity, $entityId, $metadataId, $value){
        EM::deleteEntity("{$entity}_metadata", [
            "metadata_id" => $metadataId,
            "{$entity}_id" => $entityId,
            "value" => $value
        ]);
    }
    
    public static function persist($entity, $id, $metadatas, $encrypted = true, $encodeArray = true){
        foreach($metadatas as $key => $value){
            if (is_array($value) || is_object($value)){
                if ($encodeArray){
                    $value = json_encode($value);
                    self::persistMetadataValue($entity, $id, $key, $value, $encrypted, 0);
                } else {
                    $i = 0;
                    foreach ($value as $mv) {
                        self::persistMetadataValue($entity, $id, $key, $mv, $encrypted, $i++);
                    }
                }
            } else {
                self::persistMetadataValue($entity, $id, $key, $value, $encrypted, 0);
            }
        }
    }

    public static function persistMetadataValue($entity, $id, $metadataId, $value, $encrypted = true, $position = 0){
        $stmt = EM::prepare("INSERT INTO {$entity}_metadata
            ({$entity}_id, metadata_id, value)
            VALUES (:id, :metadata_id, :value)");

        $stmt->execute([
            'id' => $id,
            'metadata_id' => $metadataId,
            "value" => $encrypted ? EncryptService::encrypt( $value ) : $value
        ]);
    }
}
