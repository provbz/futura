<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class NoticeUserService{

    static $sender = "NoticeUserService";

    public static function evaluateNoticeUser(){
        $noticeUsers = self::findNoticeUserWithChannel(NoticeUserChannel::EMAIL);
        foreach ($noticeUsers as $noticeUser){
            if ($noticeUser['type'] != NoticeUserType::IMMEDIATE){
                continue;
            }
            
            $mail = [
                "notice_user_id" => $noticeUser['notice_user_id'],
            ];
            
            $user = UserService::find($noticeUser['user_id']);
            if ($user == null){
                LogService::error_(self::$sender, "Notice user invalid user " . $noticeUser['notice_user_id']);
                continue;
            }
            
            $mail['to'] = $user['email'];

            $message = self::createMessage($noticeUser, false, true);
            if ($message == null){
                continue;
            }

            $mail["subject"] = $message["subject"];
            $mail["message"] = $message["message"];

            MailService::persist($mail);
            self::noticeUserEvaluated($noticeUser['notice_user_id']);
        }
    }
    
    public static function evaluateNoticesUserDigest(){
        $users = self::findNoticeUserDigestUsers(NoticeUserChannel::EMAIL);
        foreach ($users as $userId){
            $user = UserService::find($userId['user_id']);
            if ($user == null){
                LogService::error_(self::$sender, "Notice user invalid user " . $userId['user_id']);
                continue;
            }

            $mail = [];
            $mail['to'] = $user['email'];
            $mail["subject"] = "Digest piattaforma Futura";    
            
            $noticesUserDigest = self::findNoticeUserDigestForUser(NoticeUserChannel::EMAIL, $userId['user_id']);
            $prevType = "";
            $isFirst = true;

            require_once __DIR__ . '/../template/mail/EmptyTemplate.php';
            $baseTemplate = new EmptyTemplate();

            $mail["message"] = $baseTemplate->getHeader();
            $mail["message"] .= "Di seguito il riepilogo delle attività:<br/><br/>\n";
            $digestBody = "";

            foreach($noticesUserDigest as $noticeUser){
                $notice = NoticeService::find($noticeUser['notice_id']);
                $par = null;
                if (StringUtils::isNotBlank($notice['parameters'])){
                    $par = json_decode($notice['parameters'], true);
                }

                if ($prevType != $noticeUser["type"]){
                    $isFirst = true;
                    $prevType = $noticeUser["type"];
                } else {
                    $isFirst = false;
                }

                $message = self::createMessage($noticeUser, true, $isFirst);                
                self::noticeUserEvaluated($noticeUser['notice_user_id']);

                if ($message == null){
                    continue;
                }

                if ($prevType != $noticeUser["type"]){
                    $digestBody .= "<h4>". $message["subject"] ."</h4>";
                }

                $digestBody .= $message["message"];
            }

            if (StringUtils::isBlank($digestBody)){
                continue;
            }

            $mail["message"] .= "<div>" . $digestBody . "</div>";
            $mail["message"] .= $baseTemplate->getFooter();

            MailService::persist($mail);
        }
    }

    public static function createMessage($noticeUser, $isDigest = false, $isFirst = true){
        $notice = NoticeService::find($noticeUser['notice_id']);
        $par = null;
        if (StringUtils::isNotBlank($notice['parameters'])){
            $par = json_decode($notice['parameters'], true);
        }
        
        $className = $notice['type'] . '_mail';
        $templatePath = __DIR__ . '/../template/mail/' .$className . '.php';
        $message = [];

        if (file_exists($templatePath)){
            require_once $templatePath;

            $mailTemplate = new $className();
            $mailTemplate->notice = $notice;
            $mailTemplate->par = $par;
            $mailTemplate->noticeUser = $noticeUser;
            $mailTemplate->isDigest = $isDigest;
            $mailTemplate->isFirst = $isFirst;
            
            $message["subject"] = $mailTemplate->getSubject();    
            $message["message"] = $mailTemplate->getMessage();
        } else {
            LogService::error_(self::$sender, "Template non trovato per la notice user " . $noticeUser['notice_user_id'] . " " . $templatePath);
            return null;             
        }

        return $message;
    }

    public static function noticeUserEvaluated($id){
        EM::updateEntity("notice_user", ["evaluated" => 1], "notice_user_id=".$id );
    }

    public static function findNoticeUserWithChannel($channel){
        return EM::execQuery("SELECT * FROM notice_user WHERE evaluated=0 AND channel=:channel",[
            "channel" => $channel
        ]);
    }
    
    public static function findNoticeUserDigestUsers($channel){
        return EM::execQuery("SELECT DISTINCT(nu.user_id)
                                FROM notice_user nu
                                WHERE nu.evaluated=0 AND nu.channel=:channel AND nu.type=:type",[
            "channel" => $channel,
            "type" => NoticeUserType::DIGEST
        ]);
    }
    
    public static function findNoticeUserDigestForUser($channel, $userId){
        return EM::execQuery("SELECT *
                                FROM notice_user nu
                                LEFT JOIN notice n ON nu.notice_id=n.notice_id
                                WHERE nu.evaluated=0 AND nu.channel=:channel AND nu.type=:type AND nu.user_id=:user_id
                                ORDER BY n.type",[
            "channel" => $channel,
            "user_id" => $userId,
            "type" => NoticeUserType::DIGEST
        ]);
    }
}