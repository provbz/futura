<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserRoleService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserRoleService {
    
    public static function find($roleId){
        return EM::execQuerySingleResult("SELECT * FROM role WHERE role_id=:role_id", [
            "role_id" => $roleId
        ]);
    }
    
    public static function findAll(){
        return EM::execQuery("SELECT * FROM role order by name");
    }
    
    public static function findAllWithLevel($level){
        return EM::execQuery("select * from role WHERE level=:level order by grade DESC", [":level" => $level]);
    }
    
    public static function removeAllRoles($userId){
        EM::deleteEntity("user_role", "user_id=".$userId);
    }
    
    public static function findUserRole($userId){
        return EM::execQuery("SELECT * 
                FROM user_role ur 
                LEFT JOIN role r ON ur.role_id=r.role_id 
                WHERE user_id=:user_id order by name", 
                ["user_id" => $userId]);
    }
    
    public static function addUserRole($userId, $roleId){
        EM::insertEntity("user_role", ["user_id" => $userId, "role_id"=>$roleId]);
    }
    
    public static function hasCurrentUser($roleId){
        global $currentUserRoles;
        return isset($currentUserRoles[$roleId]);
    }
    
    public static function hasUserRoleWithLevel($userId, $level){
        $roles = self::findUserRole($userId);
        
        foreach($roles as $role){
            if ($role['level'] == $level){
                return true;
            }
        }
        return false;
    }
    
    public static function hasCurrentUserRoleWithLevel($level){
        global $currentUserRoles;
        
        foreach($currentUserRoles as $role){
            if ($role['level'] == $level){
                return true;
            }
        }
        return false;
    }
    
    public static function hasUserRole($userId, $roleId){
        if (StringUtils::isBlank($userId) || StringUtils::isBlank($roleId)){
            return false;
        }
        
        $result = UserRoleService::findUserRole($userId);
        foreach ($result as $role){
            if ($role['role_id'] == $roleId){
                return true;
            }
        }
        return false;
    }
        
    public static function canCurrentUserDo($actionUrl){
        global $currentUserAllowedAction;
                
        if (count($currentUserAllowedAction) == 0){
            return false;
        }
        if (isset( $currentUserAllowedAction['*'] )){
            return true;
        }
        if (isset( $currentUserAllowedAction[$actionUrl] )){
            return true;
        }
        
        foreach ($currentUserAllowedAction as $key => $value){
            $key = str_replace('/', '\/', $key);
            $key = trim($key);
            $key = '/'.$key.'/';
            
            if (preg_match(trim($key), $actionUrl)){
                return true;
            }       
        }
        return false;
    }
    
    /**
     * Ritorna un array di action consentite
     * @param type $userId 
     */
    public static function findActionForUser($userId){
        $stmt = EM::execQuery("SELECT action 
                        FROM user_role ur 
                        LEFT JOIN role_action ra ON ra.role_id=ur.role_id 
                        WHERE ur.user_id=:user_id
                        ORDER BY action", [
                            'user_id' => $userId
                ]);
        
        $actions = [];
        foreach ($stmt as $action ){
            $actions[$action['action']] = $action['action'];
        }
        
        //Default actions
        $actions["^/public/.*"] = "";
        $actions["^/user/.*"] = "";
        $actions["^/files/.*"] = "";
        return $actions;
    }
    
    public static function findActionForRole($roleId){
        $query = "SELECT action ".
                "FROM role_action ra ".
                "WHERE ra.role_id=:role_id ".
                "ORDER BY action";
        $result = EM::execQuery($query, ["role_id" => $roleId]);
        
        $actions = [];
        foreach($result as $action){
            $actions[] = $action['action'];
        }
        return $actions;
    }
    
    public static function persistRoleActionsTest($roleId, $str){
        EM::deleteEntity("role_action", ["role_id" => $roleId]);
        
        $actions = mb_split("\n", $str);
        foreach($actions as $key => $action){
            if (StringUtils::isNotBlank($action)){
                EM::insertEntity("role_action", ["role_id" => $roleId, "action" => $action]);
            }
        }
    }
    
    public static function findBaseAction($userId){
        $result = UserRoleService::findUserRole($userId);
        foreach($result as $role){
            if (StringUtils::isNotBlank($role['home_action'])){
                return $role['home_action'];
            }
        }
    }
}
