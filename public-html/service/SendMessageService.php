<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of SendMessageService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once "Mail.php";

class SendMessageService {
    
    public static function enqueueMailMessage(MailBaseTemplate $message){
        if ($message == NULL){
            return;
        }

        try{
            $messageId = SendMessageService::persist($message);
            SendMessageService::send($messageId);
        } catch (Exception $e){
            LogService::error_("SendMessageService", "Unable to send mail!", $e);
        }
    }
    
    public static function send($messageId){
        error_reporting(E_ALL & ~E_STRICT);
        set_error_handler(function(){});
            
        try{
            $message = SendMessageService::find($messageId);
            
            $mail = new PHPMailer();
            $mail->IsSMTP();          
            $mail->SMTPDebug = 0;
            $mail->CharSet = 'UTF-8';
            $mail->Host = Constants::$SMTP_HOST;
            $mail->Port = Constants::$SMTP_PORT;
            
            $mail->SMTPAuth = false;
            $m->Username = Constants::$SMTP_USERNAME;
            $m->Password = Constants::$SMTP_PASSWORD;
            
            $mail->FromName = Constants::$SMTP_FROM_NAME;
            $mail->From = Constants::$SMTP_FROM_MAIL;
            if (Constants::$ALLOWED_EMAIL_ADDRESSES != null && array_search(Constants::$ALLOWED_EMAIL_ADDRESSES, strtolower($message['to'])) === false){
                SendMessageService::setStatus($message['message_id'], MessageStatus::SKIPPED);
                return;
            }
            
            $mail->IsHTML(true);
            $mail->Subject = $message['subject'];
            $mail->Body    = $message['message'];
            
            if(!$mail->Send()){
                SendMessageService::setStatus($message['message_id'], MessageStatus::ERROR);
                throw new Exception("Impossibile inviare la mail. ".  $mail->ErrorInfo);
            } else {
                SendMessageService::setStatus($message['message_id'], MessageStatus::SENDED);
            }
        } catch (Exception $ex){
            LogService::error_("SendMessageService", "send_mail_error", $ex);
            throw $ex;
        }
        set_error_handler("exception_error_handler");
    }
    
    public static function find($messageId){
        return EM::execQuerySingleResult(sprintf("SELECT * FROM message WHERE message_id=%d", $messageId));
    }

    public static function setStatus($messageId, $newStatus){
        EM::updateEntity("message", ['status' => $newStatus], sprintf('message_id=%d', $messageId));
    }
    
    public static function persist(MailBaseTemplate $message){
        $parameters = [
            'type' => get_class($message),
            'date_creation' => 'NOW()',
            'to' => $message->getDestAddress(),
            'status' => MessageStatus::NEW_,
            'subject' => $message->getSubject(),
            'message' => $message->getMessage()
        ];

        if ($message instanceof MailUserMessage){
            $parameters['user_id'] = $message->user['user_id'];
        }
        return EM::insertEntity("message", $parameters);
    }
}
