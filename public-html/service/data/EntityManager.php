<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EntityManager
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EntityManager {
    public $host;
    public $schemaName;
    public $username;
    public $password;
    public $persistent;
    
    public $connection;
    public $displayQuery = false;

    /**
     *
     * @var PDO 
     */
    public $db;
    
    public function __construct($host, $schema, $username, $password, $persistent = false) {
        $this->host = $host;
        $this->schemaName = $schema;
        $this->username = $username;
        $this->password = $password;
        $this->persistent = $persistent;
    }
    
    public function connect(){
        //$connectionString = 'mysql:unix_socket=' . Constants::$db_socket . ';dbname=' . $this->schemaName . ';charset=utf8';
        $connectionString = 'mysql:host=' . Constants::$db_host . ';dbname=' . $this->schemaName . ';charset=utf8';
        
        $this->db = new PDO($connectionString, $this->username, $this->password);
        $this->db->setAttribute(PDO::ATTR_PERSISTENT, true);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }
    
    public function selectTables(){
        $result = $this->execQuery("SHOW TABLES");
        while($table = mysql_fetch_array($result)){
            echo $table[0].'<br/>';
        }
    }
    
    /**
     * 
     * @param type $query
     * @return PDOStatement
     */
    public function execQuery($query, $par = NULL){
        $stmt = $this->db->prepare($query);
        $stmt->execute($par);
        return $stmt;
    }

    public function execQuerySingleResult($query, $par = NULL){
        $stmt = $this->execQuery($query, $par);
        if ($stmt === NULL){
            return NULL;
        }
        if ($stmt->rowCount() == 0){
            return NULL;
        }
        $obj = $stmt->fetch();
        return $obj;
    }
    
    /**
     * 
     * @param type $query
     * @return PDOStatement
     */
    public function prepare($query){
        return $this->db->prepare($query);
    }
    
    public function dbEscape($str, $maxLength = -1){
        if ($str == NULL){
            return "";
        }
        
        $res = str_replace("'", "\'", $str);
        if ($maxLength > -1 && strlen($res) > $maxLength){
            $res = substr($res, $maxLength);
        }
        return $res;
    }
    
    public function replaceEntity($name, $parameters){
        if (StringUtils::isBlank($name) ){
            return;
        }
        
        $query = "REPLACE INTO ".$name." ";
                    
        $fields = "";
        $values = "";
        foreach($parameters as $key => $value){
            $fields .= $fields == "" ? "": ", ";
            $values .= $values == "" ? "": ", ";
            
            $fields .= '`'.$key.'`';
            $values .= ':'.$key;
        }
        $stmt = EM::prepare($query.' ('.$fields. ') VALUES ('.$values.')');
        $stmt->execute($parameters);
        return $this->db->lastInsertId();
    }
    
    public function insertEntity($name, $parameters){
        if (StringUtils::isBlank($name) ){
            return;
        }
        
        $query = "INSERT INTO ".$name." SET ";
        $par = [];
        
        $set = "";
        foreach($parameters as $key => $value){
            $set .= $set == "" ? "": ", ";
            if (is_numeric($value)){
                $set .= sprintf("`%s`=:%s", $key, $key);
                $par[$key] = $value;
            } else if (StringUtils::isBlank($value)){
                $set .= sprintf("`%s`=null", $key);
            } else if ($value == "NOW()" || strpos($value, "INTERVAL") !== false){
                $set .= sprintf("`%s`=%s", $key, $value);
            } else {
                $set .= sprintf("`%s`=:%s", $key, $key);
                $par[$key] = $value;
            }
        }
        
        $query = $query.$set;
        $stmt = EM::prepare($query);
        $stmt->execute($par);
        return $this->db->lastInsertId();
    }
    
    public function updateEntity($name, $parameters, $condition ){
        if (StringUtils::isBlank($name) ){
            return;
        }
        if (count($parameters) == 0){
            return;
        }
        
        $par = [];
        $query = "UPDATE ".$name." SET ";
        
        $set = "";
        $i = 0;
        foreach($parameters as $key => $value){
            $set .= $set == "" ? "": ", ";
            if ($value === NULL) {
                $set .= sprintf("`%s`=NULL", $key); 
            } else if ($value === "NOW()" || strpos($value, "INTERVAL") === 0){
                $set .= sprintf("`%s`=%s", $key, $value);        
            } else if (strpos($value, "NOT_ESCAPE") === 0) {
                $value = str_replace("NOT_ESCAPE ", "", $value);
                $set .= sprintf("`%s`=%s", $key, $value);        
            } else if (strpos($value, ":: ") === 0) {
                $value = str_replace(":: ", "", $value);
                $set .= sprintf("`%s`=%s", $key, $value);        
            } else {
                $parName = $key.$i;
                $set .= sprintf("`".$key."`=:". $parName );
                $par[$parName] = $value;
            }

            $i++;
        }
        
        $query .= $set;
        
        if (!is_array($condition)){
            $query .= " WHERE ".$condition;
        } else {
            $set = "";
            foreach($condition as $key => $value){
                $set .= $set == "" ? "": " AND ";
                if ($value == "NOW()" || strpos($value, "INTERVAL") !== false){
                    $set .= sprintf("`%s`=%s", $key, $value);        
                } else if (strpos($value, "NOT_ESCAPE") === 0) {
                    $value = str_replace("NOT_ESCAPE ", "", $value);
                    $set .= sprintf("`%s`=%s", $key, $value);        
                } else if (strpos($value, ":: ") === 0) {
                    $value = str_replace(":: ", "", $value);
                    $set .= sprintf("`%s`=%s", $key, $value);        
                } else if ($value == NULL) {
                    $set .= sprintf("`%s`=NULL", $key);        
                } else {
                    $parName = $key.$i;
                    $set .= sprintf("`".$key."`=:". $parName );
                    $par[$parName] = $value;
                }

                $i++;
            }
            $query .= " WHERE ".$set;
        }
        
        if ($this->displayQuery){
            echo "<pre>".$query ."</pre>";
            echo "<pre>". json_encode($par) . "</pre>";
        }

        $stmt = EM::prepare($query);
        $stmt->execute($par);
    }
    
    public function deleteEntity($tableName, $conditions){
        if (StringUtils::isBlank($tableName) ){
            return;
        }
        
        $par = [];
        
        $query = "DELETE FROM ".$tableName." ";
        $set = "";
        if (!is_array($conditions)){
            $set = $conditions;
        } else {
            foreach($conditions as $key => $value){
                $set .= $set == "" ? "": " AND ";
                $set .= sprintf("`%s`=:%s", $key, $key);
                $par[$key] = $value;
            }
        }
        
        $query .= " WHERE ".$set;
        $stmt = $this->prepare($query);
        $stmt->execute($par);
    }
    
    public function transactionStart(){
        $this->db->beginTransaction();
    }
    
    public function transactionRoolback(){
        $this->db->rollBack();
    }
    
    public function transactionCommit(){
        $this->db->commit();
    }
}
