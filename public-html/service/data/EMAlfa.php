<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EM
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EMAlfa {

    public static $entityManager;

    public static function init(){
        if (Constants::$db_enabled_alfa == false){
            return;
        }
        
        self::$entityManager = new EntityManager(Constants::$db_host_alfa, Constants::$db_schema_alfa, Constants::$db_user_name_alfa, Constants::$db_password_alfa, true);
        self::$entityManager->connect();
    }

    public static function execQuery($query, $par = NULL){
        return self::$entityManager->execQuery($query, $par);
    }

    public static function execQuerySingleResult($query, $par = NULL){
        return self::$entityManager->execQuerySingleResult($query, $par);
    }
    
    public static function dbEscape($str, $maxLength = -1){
        return self::$entityManager->dbEscape($str, $maxLength = -1);
    }
    
    public static function insertEntity($name, $parameters){
        return self::$entityManager->insertEntity($name, $parameters);
    }
    
    public static function replaceEntity($name, $parameters){
        return self::$entityManager->replaceEntity($name, $parameters);
    }
    
    public static function updateEntity($name, $parameters, $condition ){
        self::$entityManager->updateEntity($name, $parameters, $condition );
    }
    
    public static function deleteEntity($tableName, $conditions){
        self::$entityManager->deleteEntity($tableName, $conditions);
    }
    
    public static function transactionStart(){
        self::$entityManager->transactionStart();
    }
    
    public static function transactionRoolback(){
        self::$entityManager->transactionRoolback();
    }
    
    public static function transactionCommit(){
        EM::$entityManager->transactionCommit();
    }
    
    public static function prepare($query){
        return EM::$entityManager->prepare($query);
    }
}