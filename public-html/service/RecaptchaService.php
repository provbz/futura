<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of RecaptchaService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class RecaptchaService {
    
    public static function check($recaptcha, $serverSecret = null){
        $url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
            "secret" => $serverSecret == null ? RecaptchaConstants::$SERVER_SECRET : $serverSecret,
            "response" => $recaptcha,
            "remoteip" => $_SERVER['REMOTE_ADDR']
	);
        $query = http_build_query($data);
	$options = array(
            'http' => array (
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                    "Content-Length: ".strlen($query)."\r\n".
                    "User-Agent:MyAgent/1.0\r\n",
                'method' => 'POST',
                'content' => $query
            )
	);
	$context  = stream_context_create($options);
	$verify = file_get_contents($url, false, $context);
	$res = json_decode($verify);
        return $res;
    }
    
}
