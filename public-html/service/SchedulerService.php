<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of SchedulerService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SchedulerService {

    public static function find($schedulerId){
        return EM::execQuerySingleResult("SELECT * FROM scheduler WHERE scheduler_id=:scheduler_id", [
            "scheduler_id" => $schedulerId
        ]);
    }

    public static function fundAllReady(){
        return EM::execQuery("SELECT * FROM scheduler WHERE status=:status_ready", [
            "status_ready" => SchedulerStatus::READY
        ]);
    }

    public static function updateStatus($schedulerId, $status){
        $par = [
            "status" => $status
        ];

        if ($status == SchedulerStatus::RUNNING){
            $par["last_run_date"] = "NOW()";
        } else if ($status == SchedulerStatus::READY){
            $par["date_end"] = "NOW()";
        }

        EM::updateEntity("scheduler", $par, [
            "scheduler_id" => $schedulerId
        ]);
    }
}