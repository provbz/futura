<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of TextService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class TextService {
    
    public static function formatDate($str, $dateOnly = false){
        $out = "";
        if (StringUtils::isBlank($str)){
            return $out;
        }
        
        $str = str_replace("/", "-", $str);
        if (strpos($str, "-") != -1){
            $date = DateUtils::getDate($str);
            $time = DateUtils::getTime($str);
            if (StringUtils::isNotBlank($date)){
                $out = DateUtils::convertDate($date, "-", "/");
                if (!$dateOnly && StringUtils::isNotBlank($time) && $time != "00:00:00"){
                    $out .= " ".$time;
                }
            }
        }
        return $out;
    }
    
    public static function parseDate($str){
        $out = "";
        
        if (StringUtils::isBlank($str)){
            return $out;
        }
        
        $date = DateUtils::getDate($str);
        $time = DateUtils::getTime($str);
        if (StringUtils::isNotBlank($date)){
            $out = DateUtils::convertDateToSQL($date, "/", "-");
            if (StringUtils::isNotBlank($time)){
                $out .= ' '.$time;
            }
        }
        return $out;
    }
    
    public static function printText($str){
        return str_replace("\n", "<br/>", $str);
    }
    
}
