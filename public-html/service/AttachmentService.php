<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AttachmentService
 *
 * @author marco
 */
class AttachmentService {
    
    public static function find($id){
        return EM::execQuerySingleResult("SELECT * FROM attachment WHERE attachment_id=:id", ["id" => $id]);
    }

    public static function findAttachmentEntityForAttachmentId($attachmentId){
        return EM::execQuery("SELECT * FROM attachment_entity WHERE attachment_id=:id", [
            "id" => $attachmentId
        ]);
    }

    public static function findAttachmentEntityForAttachmentEntityId($attachmentEntityId){
        return EM::execQuerySingleResult("SELECT * FROM attachment_entity WHERE attachment_entity_id=:id", [
            "id" => $attachmentEntityId
        ]);
    }

    public static function findAttachmentEntity($entity, $entityId, $attachmentId){
        return EM::execQuerySingleResult("SELECT * 
            FROM attachment_entity 
            WHERE attachment_id=:id AND entity=:entity AND entity_id=:entity_id", [
                "id" => $attachmentId,
                "entity" => $entity,
                "entity_id" => $entityId
            ]);
    }

    public static function findAllForEntity($entity, $id, $type = null) {
        $par = [
            "entity" => $entity,
            "id" => $id
        ];

        $query = "SELECT *, a.insert_date as a_insert_date
            FROM attachment_entity ae
            LEFT JOIN attachment a ON ae.attachment_id=a.attachment_id 
            WHERE ae.entity=:entity 
            AND ae.entity_id=:id ";

        if (StringUtils::isNotBlank($type)){
            $query .= "AND ae.type=:type ";
            $par["type"] = $type;
        }
            
        $query .= "ORDER BY sort";

        $attachments = EM::execQuery($query, $par);
        return $attachments;
    }

    public static function findInMV($AttachmentEntityId) {
        $attachmentEntity = self::findAttachmentEntityForAttachmentEntityId($AttachmentEntityId);
        if ($attachmentEntity == null){
            return null;
        }

        $attachment = self::find($attachmentEntity["attachment_id"]);

        $a = new ModelViewAttachment();
        $a->AttachmentId = $attachmentEntity['attachment_id'];
        $a->attachmentEntityId = $attachmentEntity["attachment_entity_id"];
        $a->FileName = $attachment["filename"];
        $a->Size = $attachment["size"];
        $a->insertDate = $attachment["insert_date"];
        $a->Url = UriService::buildAttachmentUrl($attachment, $attachmentEntity);
        
        return $a;
    }

    public static function findAllForEntityInMV($entity, $id, $type = null) {
        $attachments = self::findAllForEntity($entity, $id, $type);
        
        $results = [];
        foreach ($attachments as $attachment){
            $a = new ModelViewAttachment();
            $a->attachmentEntityId = $attachment["attachment_entity_id"];
            $a->AttachmentId = $attachment['attachment_id'];
            $a->FileName = $attachment["filename"];
            $a->Size = $attachment["size"];
            $a->insertDate = $attachment["a_insert_date"];
            $a->Url = UriService::buildAttachmentUrl($attachment, $attachment);
            array_push($results, $a);
        }
        return $results;
    }

    public static function detachAllAttachmentEntity($entity, $entityId, $type = NULL) {
        $attachments = self::findAllForEntity($entity, $entityId);
        foreach($attachments as $attachment){
            $path = UriService::buildAttachmentPath($attachment);
            if (file_exists($path)){
                unlink($path);
            }

            EM::deleteEntity("attachment", ["attachment_id" => $attachment["attachment_id"]]);
            EM::deleteEntity("attachment_entity", ["attachment_id" => $attachment["attachment_id"]]);
        }
    }

    public static function persist(array $attachments, string $entity, $entityId, string $type){
        global $currentUser;

        EM::deleteEntity("attachment_entity", [
            "entity" => $entity,
            "entity_id" => $entityId, 
            "type" => $type
        ]);

        $i = 0;
        foreach($attachments as $attachmentMv){
            $attachment = self::find($attachmentMv->AttachmentId);
            if ($attachment['status'] == AttachmentStatus::TEMP){
                $filePath = "/" . $entity . "/" . $entityId . "/" . $type;
                $fullPath = UriService::buildAttachmentPath() . $filePath;
                if (!file_exists($fullPath)){
                    mkdir($fullPath, 0777, true);
                }

                $tmpPath = UriService::buildAttachmentTmpPath() . "/" . $attachment['attachment_id'];
                rename($tmpPath, $fullPath . "/" . $attachment['filename']);

                EM::updateEntity("attachment", [
                    "status" => AttachmentStatus::READY,
                    "path" => $filePath
                    ], ["attachment_id" => $attachmentMv->AttachmentId]);
            }
            
            EM::insertEntity("attachment_entity", [
                "attachment_id" => $attachment['attachment_id'],
                "entity" => $entity,
                "entity_id" => $entityId,
                "type" => $type,
                "sort" => $i,
                "insert_date" => "NOW()",
                "insert_user_id" => $currentUser != null ? $currentUser['user_id'] : null
            ]);
            $i++;
        }
    }

    public static function buildAttachmentPath($attachment){
        $path = "";
        if ($attachment['status'] == AttachmentStatus::TEMP){
            $path = UriService::buildAttachmentTmpPath() . "/" . $attachment['attachment_id'];
        } else {
            $path = UriService::buildAttachmentPath() . $attachment['path'] . "/" . $attachment['filename'];
        }

        return $path;
    }

    public static function deleteAttachmentAndAttachmentEntity($attachmentId){
        $attachment = self::find($attachmentId);
        if ($attachment == null){
            return;
        }

        $path = self::buildAttachmentPath($attachment);
        EM::deleteEntity("attachment", [
            "attachment_id" => $attachmentId
        ]);
        
        if (file_exists($path)){
            unlink($path);
        }
    }

    public static function loadAttachmentEntityContent($attachmentEntityId, $attachmentId, $method){
        $attachmentEntity = null;
        $attachment = null;

        if ($attachmentEntityId != null){
            $attachmentEntity = self::findAttachmentEntityForAttachmentEntityId($attachmentEntityId);
            if ($attachmentEntity == null){
                redirect(UriService::accessDanyActionUrl());
            }

            if ($attachmentEntity != null){
                $attachment = self::find($attachmentEntity["attachment_id"]);
                if ($attachment == null){
                    redirect(UriService::accessDanyActionUrl());
                }
            }

            if (!self::canUserAccess($attachmentEntity)){
                redirect(UriService::accessDanyActionUrl());
            }
        } else if ($attachmentId != null){
            $attachment = self::find($attachmentId);
            if ($attachment == null){
                redirect(UriService::accessDanyActionUrl());
            }

            if ($attachment["status"] != AttachmentStatus::TEMP){
                redirect(UriService::accessDanyActionUrl());
            }
        } else {
            redirect(UriService::accessDanyActionUrl());
        }
        
        $path = AttachmentService::buildAttachmentPath($attachment);
        $fileContent = EncryptService::decryptfileInMemory($path);
        
        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public");
        header("Content-Type: ". $attachment["content_type"]);
        header("Content-Transfer-Encoding: Binary");
        //header("Content-Length:" . $attachment['size']);
        header("Content-Disposition: " . $method ."; filename=" . $attachment['filename']);
         
        echo $fileContent;
        die();
    }

    public static function canUserAccess($attachmentEntity){
        global $currentUser;
        
        if(UserRoleService::hasCurrentUser(UserRole::ADMIN)){
            return true;
        }

        StructureService::addStructurePermissions();

        switch ($attachmentEntity["entity"]){
            case EntityName::SYSTEM: 
                if (strpos($attachmentEntity["type"],AttachmentType::PATTO_INCLUSIONE) === 0) {
                    if(UserRoleService::hasCurrentUser(UserRole::REFERENTE_BES) ||
                        UserRoleService::hasCurrentUser(UserRole::DIRIGENTE) ||
                        UserRoleService::hasCurrentUser(UserRole::AMMINISTRATIVI)){
                        return true;
                    }
                } else if (strpos($attachmentEntity["type"], "pf_materiali_") === 0){
                    return true;
                }
                
                break;
            case EntityName::USER: 
                $portfolioAttachments = [
                    AttachmentType::PORTFOLIO_ATTIVITA_1,
                    AttachmentType::PORTFOLIO_VALUTAZIONE_1,
                    AttachmentType::PORTFOLIO_VALUTAZIONE_2,
                    AttachmentType::PORTFOLIO_ANALISI_1,
                    AttachmentType::PORTFOLIO_ANALISI_2,
                    AttachmentType::PORTFOLIO_ANALISI_3,
                    AttachmentType::PORTFOLIO_ANALISI_4,
                    AttachmentType::PORTFOLIO_DOCUMENTAZIONE_1,
                    AttachmentType::PORTFOLIO_DOCUMENTAZIONE_2,
                    AttachmentType::PORTFOLIO_DOCUMENTAZIONE_3,
                    AttachmentType::PORTFOLIO_DOCUMENTAZIONE_4,
                    AttachmentType::PORTFOLIO_DOCUMENTAZIONE_5
                ];
                
                if (strpos($attachmentEntity["type"], "pf_ap_") === 0 || array_search($attachmentEntity["type"], $portfolioAttachments) !== false){
                    if (!UserRoleService::canCurrentUserDo("/structure/portfolio/PortfolioAction")){
                        return false;
                    }

                    if (UserRoleService::hasCurrentUser(UserRole::AMMINISTRATORE_PORTFOLIO)){
                        return true;
                    }

                    if ($currentUser["user_id"] == $attachmentEntity["entity_id"]){
                        return true;
                    }

                    $structures = StructureService::findUserStructures($attachmentEntity["entity_id"]);
                    foreach($structures as $structure){
                        if (StructureService::hasUserStructure($currentUser["user_id"], $structure["structure_id"])){
                            return true;
                        }
                    }
                } else if (self::isPeiPdpFile(strtolower($attachmentEntity["type"])) !== false){
                    if (!UserRoleService::canCurrentUserDo("/structure/pei/UserAttachmentAction")){
                        return false;
                    }

                    if (UserRoleService::hasCurrentUser(UserRole::AMMINISTRATORE_PEI_PDP)){
                        return true;
                    }

                    if (UserRoleService::canCurrentUserDo(UserPermission::STRUCTURE_STUDENTS_READ_ALL)){
                        $structures = StructureService::findUserStructures($attachmentEntity["entity_id"]);
                        foreach($structures as $structure){
                            if (StructureService::hasUserStructure($currentUser["user_id"], $structure["structure_id"])){
                                return true;
                            }
                        }
                    }

                    if (UserUserService::find($attachmentEntity["entity_id"], $currentUser["user_id"]) != null){
                        return true;
                    }

                    if (UserRoleService::canCurrentUserDo("/structure/UserTransferRequestAction-manage")){
                        $transfers = UserTransferService::findForUser($attachmentEntity["entity_id"]);
                        foreach($transfers as $transfer){
                            if (StructureService::hasUserStructure($currentUser["user_id"], $transfer["to_structure_id"])){
                                return true;
                            }
                        }
                    }
                }

                break;
            case EntityName::ED_SALUTE:
                if (UserRoleService::hasCurrentUser(UserRole::AMMINISTRATORE_EDUCAZIONE_SALUTE)){
                    return true;
                }

                if (!UserRoleService::canCurrentUserDo("/structure/edSalute/EdSaluteAction-edit")){
                    return false;
                }

                $edSalute = EdSaluteService::find($attachmentEntity["entity_id"]);
                if ($edSalute == null || $edSalute["row_status"] == RowStatus::DELETED){
                    return false;
                }

                if (StructureService::hasUserStructure($currentUser["user_id"], $edSalute["structure_id"])){
                    return true;
                }
                break;
            case EntityName::SOMMINISTRAZIONE_TEST_RESULT_USER:
                if (UserRoleService::hasCurrentUser(UserRole::AMMINISTRATORE_SOMMINISTRAZIONI)){
                    return true;
                }
                
                if (!UserRoleService::canCurrentUserDo("/structure/test/SomministrazioneTestAction")){
                    return false;
                }

                $structure = EM::execQuerySingleResult("SELECT sc.structure_id
                    from somministrazione_test_result_user stru
                    left join structure_class_user scu ON scu.user_id = stru.user_id
                    left join structure_class sc on sc.structure_class_id = scu.structure_class_id
                    where somministrazione_test_result_user_id=:scuid", [
                        "scuid" => $attachmentEntity["entity_id"]
                    ]);

                if (!StructureService::hasUserStructure($currentUser["user_id"], $structure["structure_id"])){
                    return false;
                }

                return true;
            case EntityName::TEST ||
                EntityName::SOMMINISTRAZIONE ||
                EntityName::SOMMINISTRAZIONE_TEST:
                if (UserRoleService::hasCurrentUser(UserRole::AMMINISTRATORE_SOMMINISTRAZIONI)){
                    return true;
                }

                if (UserRoleService::hasCurrentUser(UserRole::REFERENTE_ISTITUTO_SOMMINISTRAZIONI)){
                    return true;
                }
                if (UserRoleService::hasCurrentUser(UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI)){
                    return true;
                }
                if (UserRoleService::hasCurrentUser(UserRole::INSEGNANTE_SOMMINISTRAZIONI)){
                    return true;
                }
                if (UserRoleService::hasCurrentUser(UserRole::DIRIGENTE)){
                    return true;
                }
                if (UserRoleService::hasCurrentUser(UserRole::AMMINISTRATIVI)){
                    return true;
                }
                break;
            case EntityName::SOMMINISTRAZIONE_TEST_RESULT_CLASS:
                if (UserRoleService::hasCurrentUser(UserRole::AMMINISTRATORE_SOMMINISTRAZIONI)){
                    return true;
                }
                if (UserRoleService::hasCurrentUser(UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI)){
                    return true;
                }
                if (UserRoleService::hasCurrentUser(UserRole::INSEGNANTE_SOMMINISTRAZIONI)){
                    return true;
                }
                if (UserRoleService::hasCurrentUser(UserRole::DIRIGENTE)){
                    return true;
                }
                if (UserRoleService::hasCurrentUser(UserRole::AMMINISTRATIVI)){
                    return true;
                }
                break;
            
        }

        return false;
    }

    private static function isPeiPdpFile($type){
        $peipdpFiles = [
            DocumentType::PEI . "_sy_",
            DocumentType::PDP . "_sy_",
            AttachmentType::pei_verbale,
            AttachmentType::PEI_PDP_DIAGNOSI,
            AttachmentType::PEI_PDP_PDF,
            AttachmentType::PEI_PDP_CONSENSO,
            AttachmentType::PEI_PDP_ALTRO,
            AttachmentType::pei_progetti_inclusivi,
            AttachmentType::at_certificazione_competenze_3p1,
            AttachmentType::at_certificazione_competenze_5p,
            AttachmentType::at_certificazione_competenze_5s2,
            DocumentType::PEI_TIROCINIO,
            DocumentType::PDP_LINGUA
        ];

        foreach ($peipdpFiles as $name) {
            if (strpos($type, strtolower($name)) === 0){
                return true;
            }
        }

        return false;
    }
}

class ModelViewAttachment
{
    public $attachmentEntityId;
    public $AttachmentId;
    public $FileName;
    public $Size;
    public $insertDate;
    public $Url;
}
