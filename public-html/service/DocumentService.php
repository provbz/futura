<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DocumentService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DocumentService {

    public static function getDocumentName($documentType){
        if ($documentType == DocumentType::E_MODEL){
            return "Modello E";
        } else if ($documentType == DocumentType::DROP_OUT){
            return "Segnalazioni di frequenze irregolari";
        } else if ($documentType == DocumentType::PEI_TIROCINIO) {
            return "PEI di tirocinio";
        } else if ($documentType == DocumentType::PDP_LINGUA) {
            return "PDP per bisogni Linguistici specifici";
        }

        return strtoupper($documentType);
    }

    public static function find($userYearDocumentId){
        return EM::execQuerySingleResult("SELECT * from user_year_document WHERE user_year_document_id=:id", [
            "id" => $userYearDocumentId
        ]);
    }

    public static function findAllForUser($userId){
        return EM::execQuery("SELECT *
            from user_year_document
            where user_id=:user_id", [
                "user_id" => $userId
            ]);
    }
    
    public static function findForUserAndYearAndType($userId, $schoolYearId, $type = null, $status = null){
        $query = "SELECT * FROM user_year_document 
            WHERE user_id=:user_id AND school_year_id=:school_year_id AND status <> 'deleted' ";
        
        $pars = [
            "user_id" => $userId,
            "school_year_id" => $schoolYearId
        ];

        if ($type != null){
            $query .= " AND type = :type";
            $pars["type"] = $type;
        }

        if ($status != null){
            $query .= " AND status = :status";
            $pars["status"] = $status;
        }

        $query .= " ORDER BY user_year_document_id DESC";

        return EM::execQuery($query, $pars);
    }

    public static function findOtherDocuments(string $documentType, int $userId, array $currentYear): array{
        $otherTyoesToCheck = [
            DocumentType::PEI => [
                DocumentType::PDP,
                DocumentType::PDP_LINGUA
            ],
            DocumentType::PEI_TIROCINIO => [
                DocumentType::PDP,
                DocumentType::PDP_LINGUA
            ],
            DocumentType::PDP => [
                DocumentType::PEI,
                DocumentType::PEI_TIROCINIO,
                DocumentType::PDP_LINGUA
            ],
            DocumentType::PDP_LINGUA => [
                DocumentType::PEI,
                DocumentType::PEI_TIROCINIO,
                DocumentType::PDP
            ]
        ];

        $others = [];
        if (isset($otherTyoesToCheck[$documentType])){
            $others = $otherTyoesToCheck[$documentType];
        } else {
            return [];
        }

        $qParams = [
            "user_id" => $userId,
            "school_year_id" => $currentYear["school_year_id"],
            "statusReady" => UserYearDocumentStatus::READY
        ];

        $qIn = "";
        foreach ($others as $other) {
            $qIn .= $qIn == "" ? "" : ", ";
            $qIn .= "'$other'";
        }

        $query = "SELECT * FROM user_year_document 
            WHERE user_id=:user_id 
                AND type IN ({$qIn})
                AND school_year_id = :school_year_id
                AND status = :statusReady
                AND replaced_by_document_id IS NULL";

        $othersDocumentsStmt = EM::execQuery($query, $qParams);
        return $othersDocumentsStmt->fetchAll();
    }

    public static function findReplacedDocuments($documentId) : PDOStatement{
        return EM::execQuery("SELECT * 
            FROM user_year_document 
            WHERE replaced_by_document_id = :document_id 
            AND status=:status", [
            "document_id" => $documentId,
            "status" => UserYearDocumentStatus::READY
        ]);
    }
    
    public static function updateStatus($userYearDocumentId, $status){
        if ($status == UserYearDocumentStatus::DELETED){
            EM::updateEntity(("user_year_document"), [
                "replaced_by_document_id" => null
            ], [
                "replaced_by_document_id" => $userYearDocumentId
            ]);
        }

        EM::updateEntity("user_year_document", [
            "status" => $status,
            "replaced_by_document_id" => null
        ], [
            "user_year_document_id" => $userYearDocumentId
        ]);
    }

    public static function findAllByType($type){
        return EM::execQuery("SELECT * FROM user_year_document WHERE type=:type", ["type" => $type]);
    }

    /**
     * 
     * @param type $userYearId
     * @param type $createdByUserId
     * @param type $type
     * @return PEI
     */
    public static function create(int $userId, int $createdByUserId, string $type){
        $currentSchoolYear = SchoolYearService::findCurrentYear();

        $documentId = EM::insertEntity("user_year_document", 
                [
                    'user_id' => $userId,
                    "school_year_id" => $currentSchoolYear["school_year_id"],
                    'insert_date' => 'NOW()',
                    'insert_user_id' => $createdByUserId,
                    'last_edit_user_id' => $createdByUserId,
                    'last_edit_date' => 'NOW()',
                    'type' => $type,
                    'status' => UserYearDocumentStatus::READY,
                    'pei_status' => PeiStatus::RED
                ]);

        $otherDocuments = self::findOtherDocuments($type, $userId, $currentSchoolYear);
        if ($otherDocuments != null){
            foreach ($otherDocuments as $otherDocument) {
                EM::updateEntity("user_year_document", [
                    "replaced_by_document_id" => $documentId
                ], [
                    "user_year_document_id" => $otherDocument["user_year_document_id"]
                ]);
            }
        }
        
        return $documentId;
    }

    public static function updateLastEdit($documentId){
        $loggedUser = UserService::getLoggedUser();

        EM::updateEntity("user_year_document", [
                'last_edit_user_id' => $loggedUser["user_id"],
                'last_edit_date' => 'NOW()'
            ], [
                "user_year_document_id" => $documentId
            ]);
    }
    
    public static function toCurrentYear($documentId){
        $currentYear = SchoolYearService::findCurrentYear();

        EM::updateEntity("user_year_document", [
            "school_year_id" => $currentYear["school_year_id"],
            "pei_status" => PeiStatus::YELLOW
        ], [
            "user_year_document_id" => $documentId
        ]);
    }

    public static function update($documentId){
        $document = self::find($documentId);
        if ($document == null){
            return;
        }
        
        $loggedUser = UserService::getLoggedUser();
        
        self::updateLastEdit($documentId);
        
        if ($document["pei_status"] == PeiStatus::RED){
            PeiService::updatePeiStatus($documentId, PeiStatus::YELLOW);
        }
    }
}


