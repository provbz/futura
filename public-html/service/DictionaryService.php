<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DictionaryService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DictionaryService {
    
    public static function find($id){
        $stmt = EM::prepare("SELECT * FROM dictionary WHERE dictionary_id=:id");
        $stmt->execute(['id' => $id]);
        return $stmt->fetch();
    }
    
    public static function findByKey($key){
        $stmt = EM::prepare("SELECT * FROM dictionary WHERE `key`=:key");
        $stmt->execute(['key' => $key]);
        return $stmt->fetch();
    }

    public static function findByGroupAndKeys($group, $keys){
        $pars = [
            "group" => $group
        ];

        $inq = "";
        foreach ($keys as $key => $value) {
            $kv = "key_" . $key;
            $inq .= $inq == "" ? "" : ", ";
            $inq .= ":" . $kv;
            $pars[$kv] = $value;
        }

        $query = "SELECT * FROM dictionary WHERE `group`=:group AND `key` IN ({$inq})";
        return EM::execQuery($query, $pars);
    }
    
    public static function findByGroupAndKey($group, $key){
        $stmt = EM::prepare("SELECT * FROM dictionary WHERE `key`=:key AND `group`=:group");
        $stmt->execute([
            'key' => $key,
            'group' => $group
        ]);
        return $stmt->fetch();
    }

    public static function findByGroupAndValue($group, $value){
        $stmt = EM::prepare("SELECT * FROM dictionary WHERE `value`=:value AND `group`=:group");
        $stmt->execute([
            'value' => $value,
            'group' => $group
        ]);
        return $stmt->fetch();
    }

    public static function findByUri($uri){
        $query = sprintf("SELECT * FROM dictionary WHERE `uri`='%s'",
                mysql_real_escape_string($uri));
        return EM::execQuerySingleResult($query);
    }
    
    public static function findGroups(){
        $stmt = EM::prepare("SELECT distinct(`group`) as g FROM dictionary order by `group`");
        $stmt->execute();
        return $stmt;
    }
    
    public static function findGroup($group, $alphabeticalOrder = false){
        $query = "SELECT * FROM dictionary WHERE `group`=:group ";
        if ($alphabeticalOrder){
            $query .= 'ORDER BY `value`';
        } else {
            $query .= 'ORDER BY `order`';
        }

        $stmt = EM::execQuery($query, ['group' => $group]);
        return $stmt->fetchAll();
    }
    
    public static function findGroupAsDictionary($group, $alphabeticalOrder = false, $keyAttribute = "key", $addEmptyValue = false, $keyPrefix = ""){
        $result = DictionaryService::findGroup($group, $alphabeticalOrder);
        $res = [];
        
        if ($addEmptyValue){
            $res[""] = "";
        }

        foreach($result as $row){
            $res[$keyPrefix . $row[$keyAttribute]] = $row['value'];
        }

        return $res;
    }

    public static function findGroupAsKeyValuePair($group, $alphabeticalOrder = false, $keyAttribute = "key", $exludeKeys = [], $addEmptyValue = false ){
        $result = DictionaryService::findGroup($group, $alphabeticalOrder);
        $res = [];
        if ($addEmptyValue){
            $res[] = new KeyValuePair("", "");
        }

        foreach($result as $row){
            $key = $row[$keyAttribute];

            if (array_search($key, $exludeKeys) !== false){
                continue;
            }
            
            $res[] = new KeyValuePair($key, $row['value']);
        }
        return $res;
    }

    public static function findGroupAsDictionaryObj($group, $keyAttribute = "key", $alphabeticalOrder = false){
        $result = DictionaryService::findGroup($group, $alphabeticalOrder);
        $res = [];
        foreach($result as $row){
            $res[$row[$keyAttribute]] = $row;
        }
        return $res;
    }

    public static function findKeyInValues($arr, $key){
        foreach ($arr as $index => $value) {
            if ($value instanceof KeyValuePair && $value->key == $key){
                return $value->value;
            } else if($index == $key){
                return $value;
            }
        }
        return null;
    }
}
