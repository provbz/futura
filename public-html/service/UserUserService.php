<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserUserService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserUserService {
    
    public static function find($userId, $toUserId){
        return EM::execQuerySingleResult("SELECT * FROM user_user WHERE 
                user_id = :user_id
                AND to_user_id=:to_user_id", [
                    "user_id" => $userId,
                    "to_user_id" => $toUserId
                ]);
    }

    public static function findForUserId($userId){
        return EM::execQuery("SELECT * FROM user_user WHERE 
                user_id = :user_id", [
                    "user_id" => $userId
                ]);
    }
    
    public static function persist($entity){
        global $currentUser;
        
        $entity["insert_by_user_id"] = $currentUser["user_id"];
        $entity["insert_date"] = "NOW()";
        EM::insertEntity("user_user", $entity);
        
        NoticeService::persist([
            "type" => NoticeType::USER_USER_ADD,
            "parameters" => json_encode($entity)
        ]);
    }
    
    public static function remove($userId){
        EM::deleteEntity("user_user", [
            "user_id" => $userId
        ]);
    }
}
