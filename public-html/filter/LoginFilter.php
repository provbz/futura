<?php
/* 
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of LoginFilter
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class LoginFilter {
    
    public static function exec(){
        global $currentUser, 
                $currentUserAllowedAction, 
                $currentUserRoles, 
                $requestAction, 
                $requestMethod, 
                $requestPackage, 
                $local,
                $action;
        
        $currentUser = UserService::getLoggedUser();
        if ($currentUser == NULL){
            return;
        }

        $baseAction = "";
        $currentUserAllowedAction = UserRoleService::findActionForUser($currentUser['user_id']);
        $result = UserRoleService::findUserRole($currentUser['user_id']);
        foreach($result as $role){
            $currentUserRoles[$role['role_id']] = $role;
            
            if (StringUtils::isBlank($baseAction) && StringUtils::isNotBlank($role['home_action'])){
                $baseAction = $role['home_action'];
            }
        }
        $now = new DateTime('now');
        $lastPasswordChange = null;
        if ($currentUser['date_last_password_change'] != null){
            $lastPasswordChange = new DateTime($currentUser['date_last_password_change']);
            $lastPasswordChange->add(new DateInterval("P3M"));
        }
        
        //Global access rules
        if ($requestPackage === "/public" && $requestAction == "LoginAction" && $requestMethod != "signout"){
            redirect($baseAction);
        } else if ($currentUser['status'] != UserStatus::ENABLED){
            redirect( UriService::accessDanyActionUrl() );
        } else if ($currentUser['privacy_version'] != 1 && $requestAction != 'PrivacyAction'){
            redirect('/user/PrivacyAction');
        } else if ($currentUser['reset_password'] == 1 && $requestAction != 'ResetPasswordAction'){
            redirect(UriService::buildPageUrl("/public/user/ResetPasswordAction"));
        } else if (StringUtils::isNotBlank ( $currentUser['ota_mail_verification'] ) && $requestAction != 'NewSubscriptionAction'){
            redirect( UriService::buildPageUrl("/public/NewSubscriptionAction", "confirmMail"));
        } else if ($requestAction != 'ChangePasswordAction' &&
                !ArrayUtils::getIndex ($_SESSION, 'isOffice365Account', false) &&
                StringUtils::isBlank($currentUser['ota']) &&
                (StringUtils::isBlank($currentUser['date_last_password_change']) || $lastPasswordChange < $now)){
            //redirect('/user/ChangePasswordAction');
        }

        UserService::updateOnlineUser();
    }
}
