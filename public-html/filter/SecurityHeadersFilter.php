<?php
/**
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of SelectStructureFilter
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SecurityHeadersFilter {

    public static function exec(){
        header("Strict-Transport-Security: max-age=31536000; includeSubDomains");
        header("Referrer-Policy: no-referrer-when-downgrade");
        header("X-Content-Type-Options: nosniff");
        header("X-Frame-Options: SAMEORIGIN");
        header("X-Permitted-Cross-Domain-Policies: none");
        
        header("Content-Security-Policy: default-src 'self';" .
            "connect-src 'self' *.google-analytics.com 192.168.8.176:8080 ws://192.168.8.176:8080 localhost;" .
            "script-src 'self' 'unsafe-eval' 'unsafe-inline' localhost:8080 blob: cdn.jsdelivr.net www.google.com www.gstatic.com cdnjs.cloudflare.com www.googletagmanager.com www.google-analytics.com;".
            "style-src 'self' 'unsafe-inline' localhost:8080 use.fontawesome.com fonts.googleapis.com cdn.jsdelivr.net cdnjs.cloudflare.com www.gstatic.com;".
            "img-src 'self' data: www.gstatic.com static.erickson.it www.google-analytics.com s3.eu-central-1.amazonaws.com;".
            "frame-src 'self' www.google.com s3.eu-central-1.amazonaws.com serviziovalutazione.futurabolzano.it; ".
            "font-src 'self' data: www.google.com fonts.googleapis.com fonts.gstatic.com cdnjs.cloudflare.com use.fontawesome.com;".
            "object-src 'none'; ".
            "base-uri 'none';" );
    }

}
