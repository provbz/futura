<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of SelectStructureFilter
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SelectStructureFilter {
    public static function exec(){
        global  $currentUser,
                $currentUserRoles,
                $currentUserAllowedAction,
                $requestPackage,
                $requestAction,
                $requestUri,
                $action;
        
        if ($currentUser == null){
            return;
        }

        $selectedStructureId = getRequestValue(GlobalSessionData::SELECTED_STRUCTURE_ID, null);
        if ($selectedStructureId != null){
            if ($selectedStructureId == 0){
                $_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID] = null;
            } else {
                $_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID] = $selectedStructureId;

                if (UserRoleService::canCurrentUserDo(UserPermission::STRUCTURE_ACCESS_ALL)){
                    if (strpos($requestPackage, "/admin") === 0){
                        redirect("/structure/HomeAction");
                    }
                }
            }
        }
        
        if (!UserRoleService::canCurrentUserDo(UserPermission::STRUCTURE_ACCESS_ALL)){
            if ($requestPackage == "/structure"){
                if (!isset($_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID]) || StringUtils::isBlank($_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID])){
                    $structures = StructureService::findUserStructuresRoot($currentUser['user_id']);
                    if (UserRoleService::canCurrentUserDo(UserPermission::STRUCTURE_ACCESS_ALL) ){
                        $structures = StructureService::findAll();
                    }

                    if ($structures->rowCount() == 0){
                        redirect(UriService::accessDanyActionUrl());
                        return;
                    }

                    $structure = $structures->fetch();
                    $_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID] = $structure['structure_id'];

                    if ($requestAction != "HomeAction" && $requestPackage != "/structure"){
                        redirect("/structure/HomeAction");
                    }
                }
            }
        }

        if ($currentUser != NULL && isset($_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID])){
            $action->structure = StructureService::find($_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID]);
            StructureService::addStructurePermissions();
        }
    }
}
