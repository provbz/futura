var activeDropDown = null;
var loading_img = '<img src="img/loading.gif"/>';

jQuery(function($){
	$.datepicker.regional['it'] = {clearText: 'Cancella', clearStatus: '',
		closeText: 'Chiudi', closeStatus: 'Chiudi senza modificare',
		prevText: '<Prec', prevStatus: 'Mese precedente',
		nextText: 'Succ>', nextStatus: 'Mese seguente',
		currentText: 'Corrente', currentStatus: 'Guarda il mese correnet',
		monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno',
		'Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
		monthNamesShort: ['Gen','Féb','Mar','Apr','Mag','Giu',
		'Giu','Ago','Set','Ott','Nov','Dic'],
		monthStatus: 'Vedi un altro mese', yearStatus: 'Altro anno',
		weekHeader: 'Sett', weekStatus: '',
		dayNames: ['Domenica','Lunedì','Martedì','Mercoledì','Giovedì','Venerdì','Sabato'],
		dayNamesShort: ['Dom','Lun','Mar','Mer','Gio','Ven','Sab'],
		dayNamesMin: ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
		dayStatus: 'Utilizza DD come primo giorno della settimana', dateStatus: 'Scegli il DD, MM d',
		dateFormat: 'dd/mm/yy', firstDay: 1, 
		initStatus: 'Scegliere la data', isRTL: false};
	$.datepicker.setDefaults($.datepicker.regional['it']);
});

$(function() {
    initConponents();

    setTimeout(() => {
        $(window).on("scroll", updateSubmitPosition);
        updateSubmitPosition();
    }, 10);
});

function updateSubmitPosition() {
    if ($(".campiObbligatori").length > 1) {
        return;
    }

	$(".campiObbligatori").removeClass("form-submit-fixed");
	if (!$(".footer-wrapper").isInViewport()) {
		$(".campiObbligatori").addClass("form-submit-fixed");
	}
}

$.fn.isInViewport = function () {
    var elementTop = 0;
    if ($(this).offset()) {
        elementTop = $(this).offset().top;
    }
    
	var elementBottom = elementTop + $(this).outerHeight();

	var viewportTop = $(window).scrollTop();
	var viewportBottom = viewportTop + $(window).height();

	return elementBottom > viewportTop && elementTop < viewportBottom;
};

function initConponents(){
    $('input, textarea').placeholder();
    $('.datepicker').datepicker( );
    $('textarea').autosize();
    $('.summernote').summernote();
    
    $(document).foundation({
        tab: {
                callback : function (tab) {
                    if ($(tab[0]).attr('go-to-page')){
                        self.location = $('a', tab).attr('href');
                    }
                }
            }
        });
        
    $('form input').keypress(function(e) {
        if(e.which === 13) {
            $('form').has(this).submit();
        }
    });
    
    $(window).resize(function() {
        pushDown();
    });
    pushDown();
}

function pushDown(){
    $('#push').css('height', '0px');
    if ($('footer').size() > 0){
        var height = $(window).innerHeight() - ($('footer').offset().top + $('footer').innerHeight());
        if (height > -1){
            $('#push').css('height', height + 'px');
        }
    } else {
        if ($('.footer-wrapper').offset() === undefined){
            return;
        }
        var height = $(window).innerHeight() - ($('.footer-wrapper').offset().top + $('.footer-wrapper').height());
        if (height > -1){
            $('#push').css('height', height + 'px');
        }
        
        if ($('.global-wrapper').height() < $(window).innerHeight()){
            //$('body').css('overflow', 'hidden');
        } else{
            $('body').css('overflow', 'auto');
        }
    }
}

function submitParentForm(el){
    var form = $(el).parents('form:first');
    submit(form);
}

function goBack(){
     window.history.back();
}

function toggle(selector, speed){
    if (speed === undefined){
        speed = 'slow';
    }
    $(selector).toggle(speed);
}

function openDialog(selector){
    $(selector).foundation('reveal', 'open');
}
function closeDialog(selector){
    $(selector).foundation('reveal', 'close');
}

function checkRequired(elem){
    var value = $(elem).val();
    var required = $(elem).attr("mandatory");
    if (required === "1" && StringUtils.isBlank(value)){
        $(elem).addClass("field_error");
    } else {
        $(elem).removeClass("field_error");
    }
}

function loadDiv(selector, url, parameters, onResult ){
    $(selector).html('<img src="/img/loading.gif"/>');
    var request = $.post(url, parameters,
        function (data){
            $(selector).html(data);
            initConponents();
            if (onResult !== undefined){
                onResult();
            }
        }
    );
        return request;
}

var currentTabId = "";
var currentTabUrl = "";

function openTab(tabGroup, tabIndex, divId, url){
    $('#' + tabGroup + " li").removeClass('TabbedPanelsTabSelected');
    $("li#" + tabGroup + "_" + tabIndex).addClass('TabbedPanelsTabSelected');

    currentTabId = divId;
    currentTabUrl = url;

    load(divId, url);
}

function refreshTab(additionalParameters){
    load(currentTabId, currentTabUrl, additionalParameters);
}

function tableSelectAll(){
    $("table.data input.select_row").attr("checked", true);
}

function tableDeselectAll(){
    $("table.data input.select_row").attr("checked", false);
}

function doConfirm(msg, url){
    var res = confirm(msg);
    if (res && StringUtils.isNotBlank(url)){
        window.location = url;
        return false;
    }
    return res;
}


//Adds new uniqueArr values to temp array
var ArrayUtils = {};
ArrayUtils.unique = function(a) {
     temp = new Array();
     for(i=0;i<a.length;i++){
      if(!ArrayUtils.contains(temp, a[i])){
       temp.length+=1;
       temp[temp.length-1]=a[i];
      }
     }
     return temp;
};

    //Will check for the Uniqueness
ArrayUtils.contains = function(a, e) {
    for(j=0;j<a.length;j++)if(a[j] === e)return true;
    return false;
};

ArrayUtils.removeElement = function (arrayName,arrayElement){
    for(var i=0; i<arrayName.length;i++ ){ 
        if(arrayName[i] == arrayElement){
            arrayName.splice(i,1); 
        }
    } 
};

ArrayUtils.indexOf = function (array, compareFunction){
    for(var i=0; i<array.length; i++ ){ 
        if( compareFunction( array[i] )){
            return i;
        }
    } 
    return -1;
};

function TabManager(tabSelector){
    this.tabSelector = tabSelector;
    
    $(tabSelector).tabs( { 
        show: { effect: "slide", duration: 300, direction: "right" },
        activate: function( event, ui ) {
            window.location.hash = ui.newTab[0].id;
        }
    });
    
    if (window.location.hash === ""){
        window.location.hash = "ui_tabs_1";
    }
    
    $.address.change(function(event){
        $(tabSelector + " li" + window.location.hash + " a").trigger( "click" );
    });
}

function FormManager(selectorId){
    this.selectorId = selectorId;
    
    var me = this;
    
    $( this.selectorId ).on( "submit", function( event ) {
        event.preventDefault();
        me.submit();
    });
}
FormManager.prototype.serialize = function(){
    var array = $(this.selectorId).serializeArray();
    var obj = {};
    for (var i in array) {
        var field = array[i];
        obj[field.name] = field.value;
    }
    return obj;
};
FormManager.prototype.submit = function(){
    var data = $(this.selectorId).serialize();
    var url = $(this.selectorId).attr('action');
    if (url.indexOf("?") === -1){
        url += "?" + data;
    } else {
        url += "&" + data;
    }
    loadDiv(this.selectorId + "_container", url);
};

var StringUtils = {};
StringUtils.isBlank = function(str){
    if (!str || str === undefined){
        return true;
    }
    if (typeof myVar !== 'string' || !(myVar instanceof String)){
        return false;
    }
    str = str.trim(str);
    if (str === ""){
        return true;
    }
    return false;
};

StringUtils.isNotBlank = function(str){
    return !StringUtils.isBlank(str);
};

StringUtils.trim = function(str){
    return str.replace(/^\s*/, "").replace(/\s*$/, "");
};

StringUtils.removeNotMatching = function(str, allowedChars){
    var out = "";
    for(var i = 0; i < str.length; i++){
        if(allowedChars.indexOf(str[i]) !== -1){
            out += str[i];
        }
    }
    return out;
};

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function submit(selector){
    $(selector).submit();
}

function signout(){
    if (typeof(Storage) !== "undefined") {
        localStorage.setItem("jstree", "");
    }
    
    window.location.href = "/public/LoginAction-signout";
    return false;
}

function MBPutData(selector, data) {
    var encoded = JSON.stringify(data);
    encoded = encoded.trim();
    encoded = encodeURI(encoded);
    $(selector).val(encoded);
}

function MBGetData(selector, def) {
    var value = null;
    try {
        value = $(selector).val();
        if (!value || value == "") {
            return def;
        }

        if (value[0] == "%") {
            value = decodeURI(value);
        }
        
        while (value[value.length - 1] == ",") {
            value = value.substr(0, value.length - 1);
        }

        value = JSON.parse(value);
    } catch (ex) { 
        console.log(ex);
    }

    if (!value || typeof value == 'undefined' || value == null || value == "") {
        return def;
    }

    return value;
}
