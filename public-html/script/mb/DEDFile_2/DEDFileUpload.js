'use strict';
;
function DEDFileUplad(options) {
    var isAdvancedUpload = function ()
    {
        var div = document.createElement('div');
        return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
    }();

    vueComponents["DEDFileUpload_" + options.selector] = new Vue({
		el: "#attachment-" + options.selector,
		data: {
			extensions: options.extensions,
			maxSize: options.maxSize ? options.maxSize : 0,
			uploadUrl: options.uploadUrl,
			removeUrl: options.removeUrl,
			canRemove: options.canRemove ? options.canRemove : false,
			maxAttachments: options.MaxAttachments ? options.MaxAttachments : 0,
			attachments: [],
		},
		computed: {
			accept() {
				if (!this.extensions) {
					return "";
				}

				let accept = "";
				for (let e of this.extensions) {
					accept += accept == "" ? "" : ",";
					accept += "." + e;
				}

				return accept;
			},
		},
		watch: {
			attachments: {
				handler(val) {
					MBPutData("#" + options.selector, this.attachments);
				},
				deep: true,
			},
		},
		mounted: function () {
			this.attachments = MBGetData("#" + options.selector, []);

			var _this = this;

			var forms = document.querySelectorAll(
				"#upload-" + options.selector
			);
			Array.prototype.forEach.call(forms, function (form) {
				_this.createUploader(form);
			});
		},
		methods: {
			removeAttachment: function (index) {
				if (!confirm("Rimuovere allegato?")) {
					return;
				}

				if (this.removeUrl) {
					$.post(
						this.removeUrl,
						{
							attachmentId: this.attachments[index].AttachmentId,
						},
						function () {}
					);
				}

				this.attachments.splice(index, 1);
			},
			createUploader: function (form) {
				var _this = this;

				var input = form.querySelector('input[type="file"]'),
					label = form.querySelector("label"),
					errorMsg = form.querySelector(".box__error span"),
					restart = form.querySelectorAll(".box__restart"),
					droppedFiles = false,
					showFiles = function (files) {
						label.textContent =
							files.length > 1
								? (
										input.getAttribute(
											"data-multiple-caption"
										) || ""
								  ).replace("{count}", files.length)
								: files[0].name;
					},
					triggerFormSubmit = function () {
						var event = document.createEvent("HTMLEvents");
						event.initEvent("submit", true, true);
						form.dispatchEvent(event);
						return false;
					};

				// letting the server side to know we are going to make an Ajax request
				var ajaxFlag = document.createElement("input");
				ajaxFlag.setAttribute("type", "hidden");
				ajaxFlag.setAttribute("name", "ajax");
				ajaxFlag.setAttribute("value", 1);
				form.appendChild(ajaxFlag);

				label.addEventListener("click", function (e) {
					e.preventDefault();
					e.stopPropagation();
					input.value = "";
					$(input).click();
					return false;
				});

				// automatically submit the form on file select
				input.addEventListener("change", function (e) {
					showFiles(e.target.files);
					droppedFiles = e.target.files;
					triggerFormSubmit();
					return false;
				});

				// drag&drop files if the feature is available
				if (isAdvancedUpload) {
					form.classList.add("has-advanced-upload"); // letting the CSS part to know drag&drop is supported by the browser

					[
						"drag",
						"dragstart",
						"dragend",
						"dragover",
						"dragenter",
						"dragleave",
						"drop",
					].forEach(function (event) {
						form.addEventListener(event, function (e) {
							// preventing the unwanted behaviours
							e.preventDefault();
							e.stopPropagation();
						});
					});
					["dragover", "dragenter"].forEach(function (event) {
						form.addEventListener(event, function () {
							form.classList.add("is-dragover");
						});
					});
					["dragleave", "dragend", "drop"].forEach(function (event) {
						form.addEventListener(event, function () {
							form.classList.remove("is-dragover");
						});
					});

					form.addEventListener("drop", function (e) {
						droppedFiles = e.dataTransfer.files; // the files that were dropped
						showFiles(droppedFiles);
						triggerFormSubmit();
					});
				}

				// if the form was submitted
				form.addEventListener("submit", function (e) {
					// preventing the duplicate submissions if the current one is in progress
					if (form.classList.contains("is-uploading")) {
						return false;
					}

					form.classList.add("is-uploading");
					form.classList.remove("is-error");

					if (isAdvancedUpload) {
						// ajax file upload for modern browsers
						e.preventDefault();

						// gathering the form data
						var f = document.createElement("form");
						var ajaxData = new FormData(f);
						if (droppedFiles) {
							Array.prototype.forEach.call(
								droppedFiles,
								function (file) {
									if (!_this.checkFileExtension(file.name)) {
										alert("Formato file non valido.");
										return;
									}

									if (_this.maxSize > 0) {
										if (
											_this.maxSize * 1024000 <
											file.size
										) {
											alert(
												"Il file supera le dimensioni massime consentite."
											);
											return;
										}
									}

									ajaxData.append(
										input.getAttribute("name"),
										file
									);
								}
							);
						}

						// ajax request
						var ajax = new XMLHttpRequest();
						ajax.open("post", _this.uploadUrl, true);

						ajax.onprogress = function (e) {
							if (e.lengthComputable) {
								console.log(e.loaded + " / " + e.total);
							}
						};

						ajax.onload = function () {
							form.classList.remove("is-uploading");
							if (ajax.status >= 200 && ajax.status < 400) {
								var data = JSON.parse(ajax.responseText);
								form.classList.add(
									data.HasErrors == false
										? "is-success"
										: "is-error"
								);
								if (data.HasErrors) {
									errorMsg.textContent = data.error;
								} else {
									_this.attachments.push(data.Attachment);

									if (
										options &&
										options.handlers &&
										options.handlers.onSuccess
									) {
										options.handlers.onSuccess(data);
									}
								}
							} else {
								alert("Error. Please, contact the webmaster!");
							}
						};

						ajax.onerror = function () {
							form.classList.remove("is-uploading");
							alert("Error. Please, try again!");
						};

						ajax.send(ajaxData);
					} else {
						var iframeName = "uploadiframe" + new Date().getTime(),
							iframe = document.createElement("iframe");

						$iframe = $(
							'<iframe name="' +
								iframeName +
								'" style="display: none;"></iframe>'
						);

						iframe.setAttribute("name", iframeName);
						iframe.style.display = "none";

						document.body.appendChild(iframe);
						form.setAttribute("target", iframeName);

						iframe.addEventListener("load", function () {
							var data = JSON.parse(
								iframe.contentDocument.body.innerHTML
							);
							form.classList.remove("is-uploading");
							form.classList.add(
								data.success == true ? "is-success" : "is-error"
							);
							form.removeAttribute("target");

							if (!data.success) {
								errorMsg.textContent = data.error;
							}

							iframe.parentNode.removeChild(iframe);
						});
					}

					return false;
				});

				// restart the form if has a state of error/success
				Array.prototype.forEach.call(restart, function (entry) {
					entry.addEventListener("click", function (e) {
						e.preventDefault();
						form.classList.remove("is-error", "is-success");
						input.value = "";
						input.click();
					});
				});

				// Firefox focus bug fix for file input
				input.addEventListener("focus", function () {
					input.classList.add("has-focus");
				});
				input.addEventListener("blur", function () {
					input.classList.remove("has-focus");
				});
			},
			checkFileExtension(fileName) {
				if (
					!this.extensions ||
					this.extensions.length == 0 ||
					!fileName
				) {
					return true;
				}

				var extension = fileName
					.substring(fileName.lastIndexOf(".") + 1)
					.toLowerCase()
					.trim();
				if (this.extensions.indexOf(extension) === -1) {
					return false;
				}
				return true;
			},
			onMoveLeft: function (index) {
				var tmp = this.attachments[index - 1];
				this.attachments[index - 1] = this.attachments[index];
				this.attachments[index] = tmp;
				this.$forceUpdate();
				MBPutData("#" + options.selector, this.attachments);
			},
			onMoveRight: function (index) {
				var tmp = this.attachments[index + 1];
				this.attachments[index + 1] = this.attachments[index];
				this.attachments[index] = tmp;
				this.$forceUpdate();
				MBPutData("#" + options.selector, this.attachments);
			},
		},
	});
};

