Vue.filter('prettyBytes', function (num) {
    // jacked from: https://github.com/sindresorhus/pretty-bytes
    if (typeof num !== 'number' || isNaN(num)) {
        //throw new TypeError('Expected a number');
        return "";
    }

    var exponent;
    var unit;
    var neg = num < 0;
    var units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    if (neg) {
        num = -num;
    }

    if (num < 1) {
        return (neg ? '-' : '') + num + ' B';
    }

    exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1);
    num = (num / Math.pow(1000, exponent)).toFixed(2) * 1;
    unit = units[exponent];

    return (neg ? '-' : '') + num + ' ' + unit;
});

Vue.filter('dateTime', function (str) {
    if (!str || str.indexOf(" ") == -1) {
        return str;
    }
    
    var chunks = str.split(" ");
    return reverseDate(chunks[0]) + " " + chunks[1];    
});

function reverseDate(str) {
    var chunks = str.split("-");
    chunks = chunks.reverse(chunks);  
    return chunks.join("/");
}