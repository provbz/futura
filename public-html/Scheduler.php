<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of Scheduler
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
require_once __DIR__ . '/init.php';
EM::init();

$scheduler = new Scheduler();
if (!is_array($argv) || count($argv) == 2){
    $triggerName = $task = $argv[1];
    $scheduler->executeTrigger($triggerName);
} else {
    $scheduler->execute();
}

echo "Task done\n";

class Scheduler {
    
    public static $sender = "Scheduler";
    public $startDate = null;

    public function __construct() {
        $this->startDate = new DateTime();
    }
    
    public function execute(){
        $schedulers = SchedulerService::fundAllReady();
        foreach($schedulers as $scheduler){
            //refresh the scheduler object
            $scheduler = SchedulerService::find($scheduler["scheduler_id"]);
            if ($scheduler["status"] != SchedulerStatus::READY){
                continue;
            }

            if (!$this->isToExecute($scheduler["cron"])){
                continue;
            }

            LogService::info(self::$sender, "Executing scheduler " . $scheduler[" scheduler date " . $this->getDate()]);
            SchedulerService::updateStatus($scheduler["scheduler_id"], SchedulerStatus::RUNNING);
            $this->executeTrigger($scheduler["trigger"]);
            SchedulerService::updateStatus($scheduler["scheduler_id"], SchedulerStatus::READY);
            LogService::info(self::$sender, "Done scheduler " . $scheduler["name"]);
        }
    }

    public function executeTrigger($triggerName){
        global $currentUserAllowedAction;
        $currentUserAllowedAction["*"] = 1;

        $path = __DIR__ . '/trigger/' .$triggerName . '.php';
                
        if (!file_exists($path)){
            LogService::error_(self::$sender, "Trigger not found " . $triggerName);
            return;
        }

        try{
            require_once $path;
            $instance = new $triggerName();
            $instance->execute();
        } catch (Exception $ex){
            LogService::error_(self::$sender, "Exception executing trigger ". $triggerName, $ex);
        }
    }

    private function isToExecute($cron){
        $date = $this->getDate();
        $cronChunks = mb_split(" ", $cron);

        for ($i = 0; $i < count($cronChunks); $i++) {
            if($cronChunks[$i] == "*"){
                continue;
            }

            if ($cronChunks[$i] != $date[$i]){
                return false;
            }
        }

        return true;
    }

    private function getDate(){
        $cronTime = [
            $this->startDate->format("i"),
            $this->startDate->format("H"),
            $this->startDate->format("d"),
            $this->startDate->format("n"),
            $this->startDate->format("Y")
        ];

        return $cronTime;
    }
}
