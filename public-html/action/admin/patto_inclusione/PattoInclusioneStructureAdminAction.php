<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PattoInclusioneStructureAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PattoInclusioneStructureAdminAction extends AdminAction {

    public $schoolYearId;

    public function _default(){
        $structures = StructureService::findAllRoot();
        $sy = SchoolYearService::find($this->schoolYearId);

        if ($sy == null){
            redirectWithMessage(UriService::buildPageUrl("/admin/patto_inclusione/PattoInclusioneAction"), "Dati non validi");
            return;
        }

        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Piano per l'inclusione <?= $sy["school_year"] ?></h3>
            </div>
        </div>

        <table class="table displaytable table-striped table-sm table-hover">
            <thead>
                <tr>
                    <th>Istituto</th>
                    <td>Utente</td>
                    <th>Data</th>
                    <th>File</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($structures as $structure) { ?>
                    <tr>
                        <td><?= $structure["name"] ?></td>
                        <?php
                        $attachments = $this->findAllForEntity(EntityName::STRUCTURE, $structure["structure_id"], "structure_" . AttachmentType::PATTO_INCLUSIONE . '_' . $sy["school_year_id"]);
                        $attachment = $attachments->fetch();
                        if ($attachment != null){
                            $url = UriService::buildAttachmentUrl($attachment, $attachment); ?>
                            <td><?= $attachment["u_name"] . ' ' . $attachment["u_surname"] ?> </td>
                            <td><?= TextService::formatDate( $attachment["insert_date"] ) ?></td>
                            <td>
                                <a href="<?= $url ?>" class="button small">
                                    <i class="<?= Icon::DOWNLOAD ?>"></i>
                                    Scarica
                                </a>
                            </td>
                            <?php
                        } else { ?>
                            <td></td>
                            <td></td>
                            <td></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php
        $this->footer();
    }

    private function findAllForEntity($entity, $id, $type = null) {
        $par = [
            "entity" => $entity,
            "id" => $id
        ];

        $query = "SELECT *, u.name as u_name, u.surname as u_surname, ae.insert_date as insert_date
            FROM attachment_entity ae
            LEFT JOIN attachment a ON ae.attachment_id=a.attachment_id 
            LEFT JOIN user u ON u.user_id = ae.insert_user_id
            WHERE ae.entity=:entity 
            AND ae.entity_id=:id ";

        if (StringUtils::isNotBlank($type)){
            $query .= "AND ae.type=:type ";
            $par["type"] = $type;
        }
            
        $query .= "ORDER BY sort";

        $attachments = EM::execQuery($query, $par);
        return $attachments;
    }
}