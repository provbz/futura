<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PattoInclusioneAdminAcion
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PattoInclusioneAdminAction extends AdminAction {

    public $attachmentId;

    public function _default(){
        $schoolYears = SchoolYearService::findAll();

        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Piano per l'inclusione</h3>
            </div>
        </div>

        <?php
        $form = new Form($this->actionUrl());
        $form->displaySubmit = false;

        $this->attachmentField($form, "Modello:", AttachmentType::PATTO_INCLUSIONE . '_11', false);

        $nextCurrent = false;
        foreach($schoolYears as $sy){
            if ($sy["current"] || $nextCurrent){
                $this->buttonField($form, $sy);
                $nextCurrent = true;
            } else {
                $attachments = AttachmentService::findAllForEntity(EntityName::SYSTEM, 1, AttachmentType::PATTO_INCLUSIONE . '_' . $sy["school_year_id"]);
                if ($attachments->rowCount() > 0){
                    $this->buttonField($form, $sy);
                }
            }
        }

        $form->draw();

        $this->footer();
    }

    private function buttonField($form, $schoolYear){
        ob_start();
        ?>
        <div class="row-100 form-row" style="margin-top:10px;">
            <div class="large-3 medium-3 columns"></div>
            <div class="large-9 medium-9 columns">
                <a class="button" href="<?= UriService::buildPageUrl("/admin/patto_inclusione/PattoInclusioneStructureAdminAction?schoolYearId=" . $schoolYear["school_year_id"] ) ?>">
                    Vedi documenti caricati degli istituti <?= $schoolYear["school_year"] ?>
                </a>
            </div>
        </div>
        <?php
        $content = ob_get_clean();
        $form->addField(new FormHtmlField($content));
    }

    private function attachmentField($form, $label, $name, $readOnly = true){
        $attachmentField = new FormAttachmentsField($label, $name, EntityName::SYSTEM, 1);
        $attachmentField->manAttachments = 0;
        $attachmentField->removeUrl = $this->actionUrl("removeAttachment");
        $attachmentField->noTemp = true;
        $attachmentField->canRemove = !$readOnly;
        $attachmentField->readOnly = $readOnly;
        $attachmentField->extensions = ["pdf", "docx"];
        $form->addField($attachmentField);
    }

    public function removeAttachment(){
        $attachmentEntities = AttachmentService::findAttachmentEntityForAttachmentId($this->attachmentId);
        $attachmentEntity = $attachmentEntities->fetch();
        if ($attachmentEntity == null){
            return;
        }

        if ($attachmentEntity["entity"] != EntityName::SYSTEM 
            || $attachmentEntity["entity_id"] != 1){
            return;
        }

        AttachmentService::deleteAttachmentAndAttachmentEntity($this->attachmentId);
    }
}