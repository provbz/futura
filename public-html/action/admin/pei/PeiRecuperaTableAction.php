<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PeiRecuperaTableAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PeiRecuperaTableAction extends AdminAction{
    
    public function _default(){
        $schoolYears = SchoolYearService::findAllAsObject();
        $currentSchoolYear = SchoolYearService::findCurrentYear();

        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Recupero documenti da anni precedenti</h3>
            </div>
        </div>

        <div class="mb-admin-pei-restore-table"></div>
        <?php
        $this->footer();
    }
}