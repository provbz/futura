<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DictionaryAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PortfolioAdminAction extends AdminAction{
    
    public function _default(){
        $table = new Table();
        $table->allowExportXLS = true;
        $table->entityName = "user u";
        $table->addSelect("s.name as s_name, u.name as u_name, email, surname");
        $table->addJoin("INNER JOIN user_structure us ON us.user_id=u.user_id");
        $table->addJoin("INNER JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id AND usr.role_id=". UserRole::INSEGNANTE_ANNO_DI_PROVA);
        $table->addJoin("LEFT JOIN structure s ON s.structure_id=us.structure_id");
        
        $table->addColumn(new TableColumn("s_name", "Istituto"));
        $table->addColumn(new TableColumn("email", "Email"));
        $table->addColumn(new TableColumn("u_name", "Nome"));
        $table->addColumn(new TableColumn("surname", "Cognome"));
        
        $table->addFilter(new TableFilterCustom("structure_id", function($v) use ($table){ 
            $table->params[":structure_id"] = $v;
            return "us.structure_id = :structure_id"; 
        }));
        $table->addFilter(new TableFilterCustom("email", function($v) use ($table){ 
            $table->params[":email"] = "%$v%";
            return "email LIKE :email"; 
        }));
        $table->addFilter(new TableFilterCustom("name", function($v) use ($table){ 
            $table->params[":name"] = "%$v%";
            $table->params[":surname"] = "%$v%";
            return "u.name LIKE :name OR u.surname LIKE :surname "; 
        }));
        
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Portfolio docenti anno di prova</h3>
            </div>    
        </div>
        <div class="table-row row-100">
            <div class="large-12 columns">
                <div class="filters">
                    <form method="POST" action="<?= $this->actionUrl() ?>">
                        <div class="row-100 margin-bottom">
                            <div class="columns small-12 medium-6 large-4 float-left">
                                <?php $structures = StructureService::findAllRootAsDictionary(true); ?>
                                <?= FilterUtils::drawSelectFilter("Istituto", "filter_structure_id", $structures) ?>
                            </div>
                            <div class="columns small-12 medium-6 large-4">
                                <?= FilterUtils::drawTextFilter("Nome", "filter_name") ?>
                            </div>
                            <div class="columns small-12 medium-6 large-4 float-left">
                                <?= FilterUtils::drawTextFilter("eMail", "filter_email") ?>
                            </div>
                        </div>
                        
                        <?= FilterUtils::drawSubmit(); ?>
                    </form>
                </div>
            </div>
            <div class="large-12 columns">
                <?php
                $table->draw();
                ?>
            </div>
        </div>
        <?php
        
        $this->footer();
    }
}
