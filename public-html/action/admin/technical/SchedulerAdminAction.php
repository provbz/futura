<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of SchedulerAdminction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SchedulerAdminAction extends AdminAction{

    public $schedulerId;

    public function _default() {
        $this->header();
        ?>
        <div class="mb-scheduler-admin-page"></div>
        <?php
        $this->footer();
    }

    public function execute(){
        if (StringUtils::isBlank($this->schedulerId)){
            redirectWithMessage($this->actionUrl("_default"), "Trigger non specificato");
        }

        $scheduler = SchedulerService::find($this->schedulerId);
        if ($scheduler == null){
            redirectWithMessage($this->actionUrl("_default"), "Trigger non valido");
        }

        SchedulerService::updateStatus($scheduler["scheduler_id"], SchedulerStatus::RUNNING);
        $this->executeScheduler($scheduler);
        SchedulerService::updateStatus($scheduler["scheduler_id"], SchedulerStatus::READY);

        redirectWithMessage($this->actionUrl("_default"), "Operazione eseguita");
    }

    private function executeScheduler($scheduler){
        try{
            $triggerName = $scheduler["trigger"];
            $path = __DIR__ . '/../../../trigger/' . $triggerName . '.php';
                    
            if (!file_exists($path)){
                LogService::error_(self::$sender, "Trigger not found " . $triggerName);
                redirectWithMessage($this->actionUrl("_default"), "Trigger class non valido");
            }

            require_once $path;
            $instance = new $triggerName();
            $instance->execute();
        } catch (Exception $ex){
            LogService::error_(self::$sender, "Exception executing trigger ". $triggerName, $ex);
            redirectWithMessage($this->actionUrl("_default"), "Errore di esecuzuine");
        }
    }
}