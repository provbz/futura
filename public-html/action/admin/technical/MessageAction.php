<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MessageAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class MessageAction extends AdminAction{
    
    public $messageId;
    
    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Messaggi</h3>
            </div>
        </div>
        <div class="filters">
            <form method="POST" action="<?= $this->actionUrl() ?>">
                <div class="row-100 margin-bottom">
                    <div class="columns small-12 medium-6 large-4">
                        <?= FilterUtils::drawTextFilter("Destinatario", "filter_email") ?>
                    </div>
                    <div class="columns small-12 medium-6 large-4 float-left">
                        <?= FilterUtils::drawTextFilter("Stato", "filter_status") ?>
                    </div>
                </div>
                
                <?= FilterUtils::drawSubmit() ?>
            </form>
        </div>
        <?php
        
        $table = new Table();
        $table->entityName = "message";
        $table->addFilter(new TableFilter("status"));
        $table->addFilter(new TableFilterCustom("email", function($v) use ($table) { 
            $table->params["email"] = "%{$v}%";
            return "`to` LIKE :email"; 
        } ));
        $table->addOrder("insert_date DESC");
        $table->addColumn(new TableColumn("insert_date", "Data"));
        $table->addColumn(new TableColumn("type", "Tipo"));
        $table->addColumn(new TableColumn("status", "Stato"));
        $table->addColumn(new TableColumn("to", "Destinatario"));
        $table->addColumn(new TableColumn("subject", "Titolo"));
        $table->addColumn(new TableColumn("message_id", "Messaggio", function($v){
            return '<iframe src="'.
                $this->actionUrl("drawMessage", ['messageId' => $v]).
                '"></iframe>';
        }));
                
        $table->draw();
        
        $this->footer();
    }
    
    public function drawMessage(){
        $message = SendMessageService::find($this->messageId);
        if ($message != NULL){
            echo $message['message'];
        }
    }
}
