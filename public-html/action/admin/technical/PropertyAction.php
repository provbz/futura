<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PropertyAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PropertyAction extends AdminAction{
    
    public $entityNamespace;
    public $entityKey;
    
    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Gestione properties</h3>
            </div>
            <div class="large-4 medium-4 columns">
                <a class="button radius float_right" href="<?= $this->actionUrl('edit') ?>">
                    <i class="fa fa-plus-circle"></i>
                    Nuova
                </a>
            </div>
        </div>
        <?php
        
        $table = new Table();
        $table->entityName = "property";
        $table->addOrder("`namespace`, `key`");
        $table->addColumn(new TableColumn("namespace", "Namespace"));
        $table->addColumn(new TableColumn("key", "Key"));
        $table->addColumn(new TableColumn("value", "Value"));
        $table->addColumn(new TableColumn("namespace", "", function($id, $row) {
            $out = Formatter::buildTool("", $this->actionUrl("edit", ['entityNamespace' => $row['namespace'], 'entityKey' => $row['key']]), Icon::PENCIL);
            return $out;
        }));
        
        $table->draw();
        $this->footer();
    }
    
    public function edit(){
        $form = new Form($this->actionUrl("edit", ['entityNamespace' => $this->entityNamespace, 'entityKey' => $this->entityKey]));
        $form->cancellAction = $this->actionUrl("_default");
        $form->entityName = "property";
        $form->entity = PropertyService::find($this->entityNamespace, $this->entityKey);
        
        if (StringUtils::isBlank($this->entityNamespace)){
            $form->addField(new FormTextField("Namespace", "namespace", FormFieldType::STRING, true));
        }
        if (StringUtils::isBlank($this->entityKey)){
            $form->addField(new FormTextField("Key", "key", FormFieldType::STRING, true));
        }
        $form->addField(new FormTextField("Value", "value", FormFieldType::STRING, true));
        
        if ($form->isSubmit() && $form->checkFields()){
            if (StringUtils::isNotBlank($this->entityNamespace) && StringUtils::isNotBlank($this->entityKey)){
                $form->persist("namespace='" . $this->entityNamespace . "' AND `key`='" . $this->entityKey . "' ");   
            } else {
                $form->persist();   
            }
            
            redirect($this->actionUrl("_default"));
        }
        
        $this->header();
        ?>
        <div class="section-title row">
            <div class="large-6 medium-4 columns">
                <h3>Gestione properties: edit</h3>
            </div>
        </div>
        <?php
        $form->draw();
        $this->footer();
    }
}
