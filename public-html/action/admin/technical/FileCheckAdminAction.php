<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DictionaryAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class FileCheckAdminAction extends AdminAction{

    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>File check</h3>
            </div>
        </div>

        <div>
            <a href="<?= $this->actionUrl("dbNoOnDisc") ?>" class="button">Verifica file a db non presenti su disco</a><br/><br/>

            <a href="<?= $this->actionUrl("diskNoDb") ?>" class="button">Verifica file su disco non a db</a>
        </div>

        <?php
        $this->footer();
    }

    public function diskNoDb(){
        //find all files recursively in path
        $files = [];
        $this->getDirContents(Constants::$ATTACHMENTS_DIR, $files);

        $attachmentsStmt = EM::execQuery("SELECT path, filename, insert_date
            FROM attachment");
        $attachments = [];
        foreach ($attachmentsStmt as $row) {
            $attachments[] = $row["path"] . '/' . $row["filename"];
        }

        $rows = [];
        foreach ($files as $file) {
                $row = [
                "full_path" => $file,
                "path" => str_replace(Constants::$ATTACHMENTS_DIR, "", $file),
                "exists" => false
            ];

            $row["exists"] = in_array($row["path"], $attachments);

            if ($row["exists"] == false){
                $rows[] = $row;
            }
        }

        $this->header();
        ?>
        <table class="table">
            <thead>
                <tr>
                    <th>Path</th>
                    <th>Exists</th>
                </tr>                
            </thead>
            <tbody>
                <?php foreach ($rows as $row) {?>
                    <tr>
                        <td><?= $row["full_path"] ?></td>
                        <td><?= $row["exists"] == false ? 'N0' : 'Sì' ?></td>
                    </tr>
                <?php } ?>

                <tr>
                    <td>Totale</td>
                    <td><?= count($rows) ?></td>
                </tr>

                <?php if (count($rows) == 0) {?>
                    <tr>
                        <td colspan="3">Tutti i file sono presenti a db</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php
        $this->footer();
    }

    private function getDirContents($dir, &$results = array()) {
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
            }
        }

        return $results;
    }

    public function dbNoOnDisc(){
        $attachments = $this->findAllAttachments();
        $rows = [];
        foreach ($attachments as $attachment) {
            $row = [
                "attachment" => $attachment,
                "exists" => true
            ];

            $row["full_path"] = Constants::$ATTACHMENTS_DIR . $attachment["path"] . '/' . $attachment["filename"];
            
            if (!file_exists($row["full_path"])) {
                $row["exists"] = false;
                $rows[] = $row;
            }
        }

        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>File check</h3>
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th>Attachment</th>
                    <th>Data</th>
                    <th>Path</th>
                    <th>Exists</th>
                </tr>                
            </thead>
            <tbody>
                <?php foreach ($rows as $row) {?>
                    <tr>
                        <td><?= $row["attachment"]["attachment_id"] ?></td>
                        <td><?= $row["attachment"]["insert_date"] ?></td>
                        <td><?= $row["full_path"] ?></td>
                        <td><?= $row["exists"] == false ? 'N0' : 'Sì' ?></td>
                    </tr>
                <?php } ?>

                <?php if (count($rows) == 0) {?>
                    <tr>
                        <td colspan="3">Tutti i file sono presenti su disco</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <?php
        $this->footer();
    }

    private function findAllAttachments(){
        return EM::execQuery("SELECT * 
            FROM attachment");
    }
}