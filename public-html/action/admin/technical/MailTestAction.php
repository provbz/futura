<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailTestAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class MailTestAction extends AdminAction{
    
    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Invio mail di test</h3>
            </div>
        </div>
        <?php
        
        $form = new Form($this->actionUrl());
        $hostField = new FormTextField("SMTP host", "host", FormFieldType::STRING, false);
        $hostField->readOnly = true;
        $hostField->defaultValue = Constants::$SMTP_HOST;
        $form->addField($hostField);
        $form->addField(new FormTextField("Mail Address", "to", FormFieldType::STRING, true));
        $form->addField(new FormTextField("Oggetto", "subject", FormFieldType::STRING, true));
        $form->addField(new FormTextareaField("Messaggio", "message", FormFieldType::STRING, true));
        
        if ($form->isSubmit() && $form->checkFields()){
            $message = $form->getEntity();
            $status = MailService::send($message);
            echo "Mail inviata, risultato " . $status;
        } else {
            $form->draw();
        }
        
        $this->footer();
    }
}
