<?php
/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MessageAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class NoticeAction extends AdminAction{
    
    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Notice</h3>
            </div>
        </div>
        <div class="filters">
            <form method="POST" action="<?= $this->actionUrl() ?>">
                <div class="row-100 margin-bottom">
                    <div class="columns small-12 medium-6 large-4">
                        <?= FilterUtils::drawTextFilter("Tipo", "filter_type") ?>
                    </div>
                </div>
                
                <?= FilterUtils::drawSubmit() ?>
            </form>
        </div>
        <?php
        
        $table = new Table();
        $table->entityName = "notice";
        $table->addOrder("notice_id DESC");
        $table->addColumn(new TableColumn("notice_id", "ID"));
        $table->addColumn(new TableColumn("insert_date", "Data"));
        $table->addColumn(new TableColumn("type", "Tipo"));
        $table->addColumn(new TableColumn("evaluated", "Gestita", function($v, $row){
            return $v ? "Sì" : "No";
        }));
        $table->addColumn(new TableColumn("parameters", "Parametri"));
        $table->addFilter(new TableFilter("type"));
                
        $table->draw();
        $this->footer();
    }
    
}
