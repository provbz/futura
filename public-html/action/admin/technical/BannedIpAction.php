<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PropertyAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class BannedIpAction extends AdminAction{

    public $ip;
    
    public function _default(){
        $table = new Table();
        $table->entityName = "banned_ip";
        $table->addOrder("ip");
        $table->addColumn(new TableColumn("ip", "IP"));
        $table->addColumn(new TableColumn("insert_date", "Data inserimento"));
        $table->addColumn(new TableColumn("ip", "", function($v){
            $out = Formatter::buildTool("", $this->actionUrl("remove", ['ip' => $v]), Icon::TRASH);
            return $out;
        }));
        $table->addFilter(new TableFilterCustom("ip", function($v) use ($table){
            $query = "";
            if (StringUtils::isNotBlank($v)){
                $query .= "ip LIKE :ip";
                $table->params["ip"] = $v;
            }
            return $query;
        }));

        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>IP bloccati</h3>
            </div>
            <div class="large-4 medium-4 columns">
                
            </div>
        </div>
        
        <form method="POST" action="<?= $this->actionUrl() ?>">
            <div class="row-100 margin-bottom">
                <div class="columns small-12 medium-6 large-4">
                    <?= FilterUtils::drawTextFilter("IP", "filter_ip") ?>
                </div>
            </div>
            
            <?= FilterUtils::drawSubmit() ?>
        </form>
        <?php
        
        $table->draw();
        $this->footer();
    }

    public function remove(){
        EM::deleteEntity("banned_ip", [
            "ip" => $this->ip
        ]);

        redirectWithMessage($this->actionUrl("_default"), "Operazione eseguita");
    }
}
