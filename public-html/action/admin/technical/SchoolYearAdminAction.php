<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PropertyAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SchoolYearAdminAction extends AdminAction {
    
    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Anni scolastici</h3>
            </div>
            <div class="large-4 medium-4 columns">
                
            </div>
        </div>
        <?php
        
        $table = new Table();
        $table->entityName = "school_year";
        $table->addOrder("school_year");
        $table->addColumn(new TableColumn("school_year_id", "Id"));
        $table->addColumn(new TableColumn("school_year", "Anno"));
        $table->addColumn(new TableColumn("current", "Attuale"));
        
        $table->draw();
        $this->footer();
    }
}
