<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DictionaryAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DictionaryAction extends AdminAction {
    
    public $id;
    
    public function _default(){
        $table = new Table();
        $table->entityName = "dictionary";

        $table->addFilter(new TableFilter("group"));
        $table->addOrder("`group`, `key`, `order`");
        $table->addColumn(new TableColumn("dictionary_id", "Id"));
        $table->addColumn(new TableColumn("group", "Group"));
        $table->addColumn(new TableColumn("key", "Key"));
        $table->addColumn(new TableColumn("value", "Value"));
        $table->addColumn(new TableColumn("order", "Order"));
        $table->addColumn(new TableColumn("dictionary_id", "", function($id) {
            $out = Formatter::buildTool("", $this->actionUrl("edit") . '?id='.$id, Icon::PENCIL);
            $out .= Formatter::buildTool("", $this->actionUrl("remove") . '?id='.$id, Icon::TRASH, "Rimuovere la voce di dizionario?");
            return $out;
        }));
        
        $groupsResult = DictionaryService::findGroups();
        $groups = [];
        foreach ($groupsResult as $group){
            $groups[$group['g']] = $group['g'];
        }
        
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Dizionari</h3>
            </div>
            <div class="large-4 medium-4 columns">
                <a class="button radius small float_right" href="<?= $this->actionUrl('edit') ?>">
                    <i class="fa fa-plus-circle"></i>
                    Nuovo
                </a>
            </div>
        </div>
        
        <form method="POST" action="<?= $this->actionUrl() ?>">
            <div class="row-100 margin-bottom">
                <div class="columns small-12 medium-6 large-4">
                    <?= FilterUtils::drawSelectFilter("Gruppo", "filter_group", $groups) ?>
                </div>
            </div>
            
            <?= FilterUtils::drawSubmit() ?>
        </form>
        <?php
        $table->draw();
        $this->footer();
    }

    public function remove(){
        EM::deleteEntity("dictionary", ["dictionary_id" => $this->id]);
        redirectWithMessage($this->actionUrl("_default"), "Operazione eseguita");
    }
 
    public function edit(){
        $form = new Form($this->actionUrl("edit", ["id" => $this->id]));
        $form->cancellAction = $this->actionUrl("_default");
        $form->entityName = "dictionary";
        $form->entity = DictionaryService::find($this->id);
        if ($form->entity == null){
            $form->entity = [];
        }
        $groupField = new FormTextField("Group", "group", FormFieldType::STRING, true);
        $form->addField($groupField);

        $keyField = new FormTextField("Chiave", "key", FormFieldType::STRING, true);
        $keyField->checkFunction = function($field, $value) use ($groupField){
            $dbEntity = DictionaryService::findByGroupAndKey($groupField->getValueForDb(), $value);
            if ($dbEntity != null && $dbEntity["dictionary_id"] != $this->id){
                $field->addFieldError("La chiave esiste già per questo gruppo");
            }
        };
        $form->addField($keyField);
        $form->addField(new FormTextField("Valore", "value", FormFieldType::STRING, true));

        $orderField = new FormTextField("Posizione", "order", FormFieldType::STRING, false, 0, ["numeric"]);
        $orderField->checkFunction = function($field, $value){
            if (StringUtils::isBlank($value)){
                $field->addFieldError("Deve essere un numero");
            }
        };
        $form->addField($orderField);
        
        if ($form->isSubmit() && $form->checkFields()){
            if (StringUtils::isBlank($this->id)){
                $form->persist();
            } else {
                $form->persist("dictionary_id=" . $this->id);
            }
            redirectWithMessage($this->actionUrl("_default"), "Operazione eseguita");
        }
        
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Edit dizionario</h3>
            </div>
        </div>
        <?php
        $form->draw();
        $this->footer();
    }
}
