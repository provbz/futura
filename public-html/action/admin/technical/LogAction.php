<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of LogAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class LogAction  extends AdminAction{
    
    public function _default(){
        $table = new Table();
        $table->allowExportXLS = true;
        $table->entityName = "log l";
        $table->addJoin("LEFT JOIN user u ON l.user_id=u.user_id");
        
        $table->addOrder("date DESC");
        $table->addColumn(new TableColumn("date", "Data"));
        $table->addColumn(new TableColumn("level", "Livello"));
        $table->addColumn(new TableColumn("sender", "Sender"));
        $table->addColumn(new TableColumn("message", "Messaggio"));
        $table->addColumn(new TableColumn("user_id", "user_id", function ($v, $row){
            if (StringUtils::isNotBlank($v )){
                return $row['email'];
            }
        }));
        $table->addColumn(new TableColumn("ex", "Eccezione"));
        $table->addColumn(new TableColumn("request_url", "Url"));
        $table->addColumn(new TableColumn("referer", "referer"));
        $table->addColumn(new TableColumn("request_ip", "ip"));
        $table->addColumn(new TableColumn("header", "Header"));
        $table->addFilter(new TableFilter("level"));
        $table->addFilter(new TableFilter("sender"));
        
        $sender[''] = '';
        $result = EM::execQuery("SELECT DISTINCT(sender) FROM log ");
        foreach($result as $row){
            $sender[$row['sender']] = $row['sender'];
        }
        
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Log</h3>
            </div>
        </div>
        
        <form method="POST" action="<?= $this->actionUrl() ?>">
            <div class="row-100 margin-bottom">
                <div class="columns small-12 medium-6 large-4">
                    <?php
                    $levels = array_merge(["" => ""], DictionaryService::findGroupAsDictionary(Dictionary::LOG_LEVEL));
                    echo FilterUtils::drawSelectFilter("Livello", "filter_level", $levels);
                    ?>
                </div>
                <div class="columns small-12 medium-6 large-4 float-left">
                    <?= FilterUtils::drawTextFilter("Sender", "filter_sender") ?>
                </div>
            </div>
            
            <?= FilterUtils::drawSubmit() ?>
        </form>
        <a href="<?= $this->actionUrl("removeAll") ?>">Svuota</a>

        <?php
        $table->draw();
        
        $this->footer();
    }
    
    public function removeAll(){
        EM::execQuery("DELETE FROM log");
        redirectWithMessage($this->actionUrl("_default"), "Operazione completata.");
    }
}
