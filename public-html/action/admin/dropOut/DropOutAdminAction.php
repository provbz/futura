<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DropOutAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DropOutAdminAction extends AdminAction{

    public $documentId;
    public $id;
    public $type;
    private $userMetadata;

    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Drop out: Dati</h3>
            </div>    
        </div>
        
        <div class="mb-drop-out-admin-table"></div>
        <?php
        
        $this->footer();
    }

    public function details(){
        require_once __DIR__ ."/../../structure/dropOut/DropOutDetailsAction.php";
        $action = new DropOutDetailsAction();
        $action->documentId = $this->documentId;
        $action->currentYear = SchoolYearService::findCurrentYear();
        $action->displayTools = false;
        $action->loadData(true);
        $action->getSessionData();
        
        $this->header();
        $this->bradcrumbs->add(new Bradcrumb("Drop out", $this->actionUrl("_default")));
        $this->bradcrumbs->add(new Bradcrumb("Segnalazione"));
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Drop out: Dati</h3>
                <?php $this->bradcrumbs->draw(); ?>
            </div>    
        </div>
        <?php $action->_defaultBody(); ?>
        <?php
        $this->footer();
    }

    public function edit(){
        require_once __DIR__ ."/../../structure/dropOut/DropOutDetailsAction.php";
        $action = new DropOutDetailsAction();
        $action->id = $this->id;
        $action->type = $this->type;
        $action->editable = false;
        $action->currentUser = SchoolYearService::findCurrentYear();
        $action->getSessionData();
        
        $this->header();
        $this->bradcrumbs->add(new Bradcrumb("Drop out", $this->actionUrl("_default")));
        $this->bradcrumbs->add(new Bradcrumb("Studente " . $action->user["code"], $this->actionUrl("details", ["documentId" => $action->document["user_year_document_id"]])));
        $this->bradcrumbs->add(new Bradcrumb("Segnalazione"));
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Drop out: Dati</h3>
                <?php $this->bradcrumbs->draw(); ?>
            </div>    
        </div>
        <?php $action->editBody(); ?>
        <?php
        $this->footer();
    }
}