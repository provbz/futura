<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DropOutDashboardAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DropOutDashboardAdminAction extends AdminAction{

    public $school_year_id;

    public function _default(){
                
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Drop out: analisi dati</h3>
            </div>    
        </div>
        <div class="row-100">
            <div class="mb-drop-out-admin-pivot"></div>
        </div>
        <?php
        $this->footer();
    }

    public function getData(){
        $query = "SELECT sy.school_year, 
                s.name as istituto, ps.name as plesso,
                dc.value as classe, uyd.drop_out_classe as grado,
                drop_out_age, drop_out_status, 
                n.nome as nazione, n.regione as regione, umg.value as genere,
                1 as quantita,
                drop_out_segnalazione_procura as 'Seegnalazione in procura',
                if(s.structure_id IN (1, 45), 'Sì', 'No') as 'Dati di test',
                d_esito.value as 'Esito',
                d_stato_iscrizione.value as 'Stato iscrizione'
            FROM user_year_document uyd
            LEFT JOIN user u ON uyd.user_id=u.user_id
            LEFT JOIN user_year uy ON uyd.user_id=uy.user_id
            LEFT JOIN user_metadata umg ON umg.user_id=uyd.user_id AND umg.metadata_id='gender_id'
            LEFT JOIN school_year sy ON uyd.school_year_id=sy.school_year_id
            left join nazione n ON cittadinanza_1_nazione_id=n.nazione_id
            left join structure ps ON uyd.drop_out_plesso_structure_id = ps.structure_id
            left join structure s ON ps.parent_structure_id = s.structure_id
            left join dictionary dc On uyd.drop_out_classe = dc.key AND dc.group='class'
            left join dictionary d_esito ON uyd.drop_out_esito_dictionary_id = d_esito.dictionary_id
            left join dictionary d_stato_iscrizione ON uyd.drop_out_stato_iscrizione_dictionary_id = d_stato_iscrizione.dictionary_id
            where 
                uyd.type='drop_out' 
                AND uyd.status='ready'
                AND uyd.drop_out_plesso_structure_id IS NOT NULL";
                
        $par = [];

        $results = EM::execQuery($query, $par);
        $data = [];
        foreach ($results as $row){
            $row['genere'] = EncryptService::decrypt($row['genere']);

            if ($row["grado"] != null){
                if (strstr($row['grado'], 'P') !== false){
                    $row['grado'] =  "Primaria";
                } else if (strstr($row['grado'], 'S1') !== false){
                    $row['grado'] =  "Secondaria di 1* grado";
                } else if (strstr($row['grado'], 'S2') !== false){
                    $row['grado'] =  "Secondaria di 2* grado";
                }
            }
            
            $data[] = $row;
        }

        echo json_encode($data);
    }
}