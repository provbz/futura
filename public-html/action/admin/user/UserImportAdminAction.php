<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminUserAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class UserImportAdminAction extends AdminAction{

    public function _default(){
        $this->header();
        
        $form = new Form($this->actionUrl());
        $fileField = new FormAttachmentsField("File", "file_insegnanti", "", true);
        $fileField->manAttachments = 1;
        $fileField->extensions = ["xls"];
        $form->addField($fileField);

        $form->addField(new FormCheckField("Rimuovi utenti esistenti", "delete_existing", FormFieldType::NUMBER, ["true" => "Sì"]));

        if ($form->isSubmit() && $form->checkFields()){
            $deleteExistings = getRequestValue("delete_existing");
            
            $attachments = $fileField->getDecodedValue();
            $attachment = $attachments[0];

            $attachmentEntity = AttachmentService::find($attachment->AttachmentId);
            $path = AttachmentService::buildAttachmentPath($attachmentEntity);

            $fileContent = EncryptService::decryptfileInMemory($path);
            file_put_contents($path . ".xlsx", $fileContent);
            $path .= ".xlsx";

            $objPHPExcel = PhpOffice\PhpSpreadsheet\IOFactory::load($path);
            $sheet = $objPHPExcel->getSheet(0);

            $istitutiNonTrovati = [];
            $newUserCount = 0;
            $newAssociation = 0;
            $userOk = 0;

            ?>
            Nome file: <?= _t($path) ?><br/>
            Righe presenti nel file: <?= $sheet->getHighestRow() ?>

            <table>
                <tr>
                    <td><strong>Riga</strong></td>
                    <td><strong>Istituto</strong></td>
                    <td><strong>Email</strong></td>
                    <td><strong>Nome</strong></td>
                    <td><strong>Cognome</strong></td>
                    <td><strong>Risultato</strong></td>
                </tr>
                
                <?php for ($r = 2; $r < $sheet->getHighestRow(); $r++){
                    set_time_limit(30);
                    $istituto = $sheet->getCellByColumnAndRow(1, $r);
                    $nome = $sheet->getCellByColumnAndRow(2, $r);
                    $nome = trim($nome);

                    $cognome = $sheet->getCellByColumnAndRow(3, $r);
                    $cognome = trim($cognome);

                    $structure = StructureService::findRootByName($istituto);

                    $email = str_replace(" ", "-", trim($nome));
                    $email .= "." . str_replace(" ", "-", trim($cognome)) . "@";
                    $email = strtolower($email);
                    $user = UserService::findUserLikeEmail($email . "%");

                    ?>
                    <tr>
                        <td><?= _t($r) ?></td>
                        <td><?= _t($istituto) ?></td>
                        <td><?= _t($email) ?></td>
                        <td><?= _t($nome) ?></td>
                        <td><?= _t($cognome) ?></td>
                        <td>
                        <?php 
                        if ($user != null && $deleteExistings){
                            EM::deleteEntity("user", ["user_id" => $user["user_id"]]);
                            echo "Utente rimosso";
                        } else if ($structure == null){
                            echo "Istituto non trovato.<br/>";
                            $istitutiNonTrovati[] = $istituto;
                        } else if ($user == null){
                            $email .= "scuola.alto-adige.it";

                            $user['name'] = $nome;
                            $user['surname'] = $cognome;
                            $user['email'] = $email;
                            $user['insert_date'] = 'NOW()';
                            $user['created_by_user_id'] = $this->currentUser['user_id'];
                            $userId = EM::insertEntity("user", $user);
                            UserRoleService::addUserRole($userId, UserRole::USER);
                            StructureService::addUserStructure($userId, $structure["structure_id"], [UserRole::INSEGNANTE], true);

                            $user = UserService::find($userId);
                            $newUserCount++;

                            echo "Utente inserito<br/>";
                        } else {
                            $userStructure = StructureService::findUserStructure($user["user_id"], $structure["structure_id"]);
                            if ($userStructure != null){
                                $userOk++;
                                echo "Utente presente e già associato alla struttura<br/>";
                            } else {
                                $newAssociation++;
                                StructureService::addUserStructure($user["user_id"], $structure["structure_id"], [UserRole::INSEGNANTE], true);
                                echo "Utente aggiunto alla struttura: " . $user["email"] . "<br/>";
                            }
                        }                          
                        ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>

            <h3>Istituti non trovati</h3>
            <?php foreach($istitutiNonTrovati as $istituto) { ?>
                <?= _t($istituto) ?><br/>
            <?php } ?>

            <h3>Riepilogo</h3>
            Numero: <?= _t($newUserCount) ?><br/>
            Nuove associazioni: <?= _t($newAssociation) ?><br/>
            Utenti già corretti: <?= _t($userOk) ?><br/>

            <?php
            die();
        }

        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Import massivo utenti</h3>
            </div>
        </div>
        <p>
            Selezionare un file excel che rispetti le seguenti condizioni:
            <ul>
                <li>
                    Riga 1: intestazione, la riga non verrà considerata;
                </li>
                <li>
                    Colonna 1: Nome Istituto, il nome deve essere presente in piattaforma;
                </li>
                <li>
                    Colonna 2: Nome insegnante;
                </li>
                <li>
                    Colonna 3: Cognome insegnante;
                </li>
            </ul>
        </p>
        <?php

        $form->draw();
        $this->footer();
    }
}