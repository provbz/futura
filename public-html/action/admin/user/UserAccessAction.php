<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminUserAccessAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserAccessAction extends AdminAction{
    
    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Accessi al sistema</h3>
            </div>
        </div>
        <div class="filters">
            <form method="POST" action="<?= $this->actionUrl() ?>">
                <div class="row-100 margin-bottom">
                    <div class="columns small-12 medium-6 large-4">
                        <?= FilterUtils::drawTextFilter("Nome", "filter_name") ?>
                    </div>
                </div>
                <div class="row-100 margin-bottom">
                    <div class="columns small-12 medium-4">
                        <?= FilterUtils::drawDateFilter("Inserito da", "filter_date_from") ?>
                    </div>
                    <div class="columns small-12 medium-4 float-left">
                        <?= FilterUtils::drawDateFilter("Inserito a", "filter_date_to") ?>
                    </div>
                </div>
                <?= FilterUtils::drawSubmit() ?>
            </form>
        </div>

        <?php
        $formatUsername = function($v, $row){
            return sprintf('<a href="%s">%s</a>', UriService::buildPageUrl("/admin/user/UserDetailsAction", "", ["userId" => $row['user_id']] ), $v);
        };
        
        $formatTime = function($v){
            return Formatter::formatTime($v);
        };
        
        $table = new Table();
        $table->select = array("*", "UNIX_TIMESTAMP(date_last_check) - UNIX_TIMESTAMP(date) as deltat");
        $table->entityName = "user_access ua";
        $table->addJoin("LEFT JOIN user u ON ua.user_id=u.user_id");
        $table->addOrder("date DESC");
        
        $table->addColumn(new TableColumn("email", "Utente", $formatUsername));
        $table->addColumn(new TableColumn("date", "Data"));
        $table->addColumn(new TableColumn("deltat", "Permanenza", $formatTime ));
        $table->addFilter(new TableFilterCustom("name", function($v) use($table){ 
            $table->params['name_1'] = "%{$v}%";
            $table->params['name_2'] = "%{$v}%";
            $table->params['name_3'] = "%{$v}%";
            return "name LIKE :name_1 OR surname LIKE :name_2 OR email LIKE :name_3";
        } ));
        $table->addFilter(new TableFilterCustom("date_from", function($v) use ($table) { 
            $table->params['date_from'] = DateUtils::convertDateToSQL($v, "/");
            return "date >= :date_from"; 
        } ));
        $table->addFilter(new TableFilterCustom("date_to", function($v) use ($table){
            $table->params['date_to'] = DateUtils::convertDateToSQL($v, "/");
            return "date <= :date_to"; 
        } ));

        $table->draw();
        $this->footer();
    }    
}
