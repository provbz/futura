<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminUserAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserICAction extends AdminAction{
    
    public $user_id;
    
    public function _default(){
        $this->header();
        
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Gestione Utenti IC</h3>
            </div>
            <div class="large-4 medium-4 columns">
            </div>
        </div>

        <div class="mb-user-ic-table"></div>
        <?php        
        $this->footer();
    }
        
    public function buildForm(){
        $form = new Form($this->actionUrl("edit", ['user_id' => $this->user_id]));
        $form->entityName = "user";
        $form->cancellAction = $this->actionUrl("_default");
        
        if (StringUtils::isNotBlank($this->user_id)){
            $form->entity = UserService::find($this->user_id);
        }
        
        $emailField = new FormTextField("e-mail", "email", FormFieldType::STRING, true);
        $emailField->checkFunction = function($field, $value) {
            $user = UserService::findUserByEmail($value);
            
            if ($user != null && $user['user_id'] != $this->user_id){
                $field->addFieldError("Indirizzo email in uso da parte di un altro utente.");
            }
        };
        $form->addField($emailField);
        
        $passwordField = new FormSinglePasswordField("Password", "new_password", FormFieldType::STRING);
        $passwordField->persist = false;
        $passwordField->note = "Inserire la password solo se si desidera modificarla.";
        $form->addField($passwordField);    
        
        $form->addField(new FormSelectField("Abilitato", "status", FormFieldType::STRING, 
            [
                UserStatus::ENABLED => UserStatus::ENABLED,
                UserStatus::DISABLED => UserStatus::DISABLED,
                UserStatus::DELETED => UserStatus::DELETED
            ], true, UserStatus::ENABLED));
        
        $form->addField(new FormTextField("Nome", "name", FormFieldType::STRING, true));
        $form->addField(new FormTextField("Cognome", "surname", FormFieldType::STRING, true));
        $form->addField(new FormTextField("Privacy", "privacy_version", FormFieldType::NUMBER, true));
        
        $roleId = null;
        if (StringUtils::isNotBlank($this->user_id)){
            $roles = UserRoleService::findUserRole($this->user_id);
            $firstRole = $roles->fetch();
            $roleId = $firstRole['role_id'];
        }
        
        $result = UserRoleService::findAllWithLevel(UserRole::LEVEL_GLOBAL);
        $roles = ["" => ""];
        foreach($result as $role){
            $roles[$role['role_id']] = $role['name'];
        }
        
        $slRole = new FormSelectField("Ruolo", "role_id", FormFieldType::NUMBER, $roles, true, $roleId);
        $slRole->persist = false;
        $form->addField($slRole);
        
        return $form;
    }
    
    public function edit(){
        $form = $this->buildForm();
        
        if ($form->isSubmit() && $form->checkFields()){
            if (StringUtils::isNotBlank($this->user_id)){
                $form->persist(sprintf("user_id=%d", $this->user_id ));
            } else {
                $this->user_id = $form->persist("", ["cinsert_date" => "NOW()"]);
            }
            
            $roleId = getRequestValue("role_id");
            if (StringUtils::isNotBlank( $roleId )){
                EM::deleteEntity("user_role", sprintf("user_id=%d", $this->user_id));
                EM::insertEntity("user_role", array("user_id" => $this->user_id, "role_id" => $roleId));
            }
            
            $newPassword = getRequestValue("new_password");
            IF (StringUtils::isNotBlank($newPassword)){
                EM::updateEntity("user", ["password" => UserService::encodePassword( $newPassword )], sprintf("user_id=%d", $this->user_id));    
            }
            
            NoticeService::persist([
                "type" => NoticeType::USER_ADD,
                "parameters" => json_encode(["user_id" => $this->user_id])
            ]);

            redirectWithMessage($this->actionUrl("_default"), "Utente inserito.");
            return;
        }
        
        $this->header();
        ?>
        <div class="section-title row">
            <div class="large-6 medium-4 columns">
                <h3>Edit Utente</h3>
            </div>
        </div>
        <?php
        $form->draw();
        
        if ($form->entity != null){
            $table = new Table();
            $table->paginateElements = false;
            $table->entityName = "user_structure us";
            $table->addSelect("s.name as s_name");
            $table->addSelect("r.name as r_name");
            $table->addJoin("LEFT JOIN structure s ON s.structure_id=us.structure_id");
            $table->addJoin("LEFT JOIN role r ON r.role_id=us.role_id");
            $table->addWhere("user_id=" . $this->user_id );
            $table->addColumn(new TableColumn("s_name", "Struttura"));
            $table->addColumn(new TableColumn("r_name", "Ruolo"));
            $table->draw();
        }
        
        $this->footer();
    }
    
}
