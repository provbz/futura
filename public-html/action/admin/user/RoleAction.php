<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of RoleAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class RoleAction extends AdminAction{
    
    public $role_id;
    private $form = null;
    
    public function _default(){
        $this->header();
        
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Gestione ruoli</h3>
            </div>
            <div class="large-4 medium-4 columns">
                <a class="button radius float_right" href="<?= $this->actionUrl('edit') ?>">
                    <i class="fa fa-plus-circle"></i>
                    Nuovo
                </a>
            </div>
        </div>

        <div class="mb-role-table"></div>
        <?php
        
        $this->footer();
    }
    
    public function buildForm(){
        $this->form = new Form(UriService::buildPageUrl( $this->actionUrl("save") ));
        $this->form->entityName = "role";
        $this->form->cancellAction = $this->actionUrl("_default");
        
        $actionStr = "";
        $id = getRequestValue("role_id");
        if (StringUtils::isNotBlank($id)){
            $entity = UserRoleService::find($id);
            if ($entity != NULL){
                $this->form->entity = $entity;
                $this->form->addField(new FormHiddenField("role_id", FormFieldType::NUMBER, $id));
                
                $actions = UserRoleService::findActionForRole($id);
                foreach($actions as $value){
                    $actionStr .= $value."\n";
                }
            }
        }
        
        $this->form->addField(new FormTextField("Nome", "name", FormFieldType::STRING, true));
        
        $levels = DictionaryService::findGroupAsDictionary(Dictionary::ROLE_LEVEL);
        $this->form->addField(new FormSelectField("Livello", "level", FormFieldType::STRING, $levels));
        
        $this->form->addField(new FormTextField("Home action", "home_action", FormFieldType::STRING, true));
        
        $taActions = new FormTextareaField("Azioni consentite", "actions", FormFieldType::STRING, true, $actionStr, array("rows"=>15));
        $taActions->persist = false;
        $this->form->addField($taActions);
    }
    
    public function save(){
        $this->buildForm();
        
        $actions = $this->form->findFieldValue("actions");
        if (StringUtils::isBlank($actions)){
            $this->edit();
            return;
        }
        
        if (!$this->form->checkFields()){
            $this->edit();
        } else {
            $id = $this->form->findFieldValue("role_id");
            
            if (StringUtils::isNotBlank($id)){
                $this->form->persist(sprintf("role_id=%d", $id ));
            } else {
                $id = $this->form->persist();
            }
            
            UserRoleService::persistRoleActionsTest($id, $actions);
            redirectWithMessage( $this->actionUrl("_default") , "Utente inserito.");
        }
    }
    
    public function remove(){
        if (StringUtils::isBlank($this->role_id)){
            redirect( UriService::accessDanyActionUrl() );
        }
        
        EM::deleteEntity("role", ['role_id' => $this->role_id]);
        redirect($this->actionUrl("_default"));
    }
    
    public function edit(){
        $this->bradcrumbs->add(new Bradcrumb("Ruoli", $this->actionUrl("_default") ));
        $this->bradcrumbs->add(new Bradcrumb("Modifica"));
        $this->buildForm();
        
        $this->header();
        $this->form->draw();
        $this->footer();
    }
}
