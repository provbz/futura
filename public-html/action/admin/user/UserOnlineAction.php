<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserOnlineAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserOnlineAction extends AdminAction{

    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Utenti online</h3>
            </div>
        </div>
        <?php

        $formatUsername = function($v, $row){
            return sprintf('<a href="%s">%s</a>', UriService::buildPageUrl("/admin/user/UserAction-edit", "", ["user_id" => $row['user_id']] ), $v);
        };

        $formatTime = function($v){
            return Formatter::formatTime($v);
        };

        $table = new Table();
        $table->select = ["*", "UNIX_TIMESTAMP(date_last_check) - UNIX_TIMESTAMP(date) as deltat"];
        $table->entityName = "user_access ua";
        $table->addJoin("LEFT JOIN user u ON ua.user_id=u.user_id");
        $table->addWhere("date_last_check >= NOW() - INTERVAL 1 MINUTE");
        $table->addOrder("date DESC");

        $table->addColumn(new TableColumn("email", "Utente", $formatUsername));
        $table->addColumn(new TableColumn("date", "Data"));
        $table->addColumn(new TableColumn("deltat", "Permanenza", $formatTime ));
        $table->addColumn(new TableColumn("ip", "IP" ));
        $table->addColumn(new TableColumn("browser", "Browser"));
        
        $table->draw();

        $this->footer();
    }
}