<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminUserAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserAction extends AdminAction {
    
    public $user_id;
    
    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Gestione Utenti</h3>
            </div>
            <div class="large-4 medium-4 columns">
                <a class="button radius small float_right" href="<?= $this->actionUrl('edit') ?>">
                    <i class="fa fa-plus-circle"></i>
                    Nuovo
                </a>
            </div>
        </div>
        <div class="mb-admin-user-table"></div>
        <?php
        $this->footer();
    }

    public function access(){
        $user = UserService::find($this->user_id);
        if ($user == null){
            redirect($this->actionUrl("_default"));
        }

        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Accessi <?= $user["email"] ?></h3>
            </div>
        </div>
        <div class="mb-admin-user-access-table-container"></div>
        <script>
            MbCreateVue('mb-admin-user-access-table', 'mb-admin-user-access-table-container', {
                "userId": <?= $this->user_id ?>
            } );
        </script>
        <?php
        $this->footer();
    }

    public function impersonificate(){
        $user = UserService::find($this->user_id);
        if ($user == null){
            redirect($this->actionUrl("_default"));
        }

        UserService::setLoggedUser($user, false, false);

        NoticeService::persist([
            "type" => NoticeType::USER_IMPERSONIFICATE,
            "entity" => EntityName::USER,
            "entity_id" => $this->currentUser["user_id"],
            "parameters" => json_encode(["to_user_id" => $this->user_id])
        ]);

        redirect("/");
    }
        
    public function buildForm(){
        $form = new Form($this->actionUrl("edit", ['user_id' => $this->user_id]));
        $form->entityName = "user";
        $form->cancellAction = $this->actionUrl("_default");
        $form->entity = UserService::find($this->user_id);
        
        $emailField = new FormTextField("e-mail", "email", FormFieldType::STRING, true);
        $emailField->checkFunction = function($field, $value) {
            $user = UserService::findUserByEmail($value);

            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $field->addFieldError("Indirizzo mail non valido.");
                return false;
            }
            
            if ($user != null && $user["status"] != UserStatus::DELETED && $user['user_id'] != $this->user_id){
                $field->addFieldError("Indirizzo email in uso da parte di un altro utente.");
            }
        };
        $form->addField($emailField);
        
        $passwordField = new FormSinglePasswordField("Password", "new_password", FormFieldType::STRING);
        $passwordField->persist = false;
        $passwordField->note = "Inserire la password solo se si desidera modificarla.";
        $form->addField($passwordField);    
        
        $form->addField(new FormSelectField("Abilitato", "status", FormFieldType::STRING, 
            [
                UserStatus::ENABLED => UserStatus::ENABLED,
                UserStatus::DISABLED => UserStatus::DISABLED,
                UserStatus::DELETED => UserStatus::DELETED
            ], true, UserStatus::ENABLED));
        
        $form->addField(new FormTextField("Nome", "name", FormFieldType::STRING, true));
        $form->addField(new FormTextField("Cognome", "surname", FormFieldType::STRING, true));
        $form->addField(new FormTextField("Privacy", "privacy_version", FormFieldType::NUMBER, true));
        
        $selectedRoleIds = [];
        if (StringUtils::isNotBlank($this->user_id)){
            $roles = UserRoleService::findUserRole($this->user_id);
            foreach($roles as $role){
                array_push($selectedRoleIds, $role['role_id']);
            }
        }
        
        $result = UserRoleService::findAllWithLevel(UserRole::LEVEL_GLOBAL);
        $roles = [];
        foreach($result as $role){
            $roles[$role['role_id']] = $role['name'];
        }
        
        $slRole = new FormCheckboxMultiField("Ruoli", "role_id", FormFieldType::NUMBER, $roles, true, $selectedRoleIds);
        $slRole->persist = false;
        $form->addField($slRole);

        $apiKeyField = new FormTextField("Api key", "api_key", FormFieldType::STRING, false);
        $apiKeyField->readOnly = true;
        $form->addField($apiKeyField);
        
        return $form;
    }
    
    public function edit(){
        $form = $this->buildForm();
        
        if ($form->isSubmit() && $form->checkFields()){
            $action = NoticeType::USER_ADD;
            if (StringUtils::isNotBlank($this->user_id)){
                $action = NoticeType::USER_UPDATE;
                $form->persist(sprintf("user_id=%d", $this->user_id ));
            } else {
                $this->user_id = $form->persist("", ["insert_date" => "NOW()"]);
            }
            
            $roleIds = getRequestValue("role_id");
            if (is_array($roleIds)){
                UserRoleService::removeAllRoles($this->user_id);
                foreach ($roleIds as $roleId) {
                    EM::insertEntity("user_role", ["user_id" => $this->user_id, "role_id" => $roleId]);
                }
            }
            
            $newPassword = getRequestValue("new_password");
            if (StringUtils::isNotBlank($newPassword)){
                EM::updateEntity("user", ["password" => UserService::encodePassword( $newPassword )], sprintf("user_id=%d", $this->user_id));    
            }
            
            NoticeService::persist([
                "type" => $action,
                "parameters" => json_encode(["user_id" => $this->user_id])
            ]);

            redirectWithMessage($this->actionUrl("_default"), "Utente inserito.");
            return;
        }
        
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Edit Utente</h3>
            </div>
        </div>
        <?php
        $form->draw();
        
        if ($form->entity != null){
            $table = new Table();
            $table->paginateElements = false;
            $table->entityName = "user_structure us";
            $table->addSelect("s.name as s_name");
            $table->addSelect("r.name as r_name");
            $table->addJoin("LEFT JOIN structure s ON s.structure_id=us.structure_id");
            $table->addJoin("LEFT JOIN user_structure_role usr ON usr.user_structure_id=us.user_structure_id");
            $table->addJoin("LEFT JOIN role r ON r.role_id=usr.role_id");
            $table->addWhere("user_id=" . $this->user_id );
            $table->addColumn(new TableColumn("s_name", "Struttura"));
            $table->addColumn(new TableColumn("r_name", "Ruolo"));
            $table->draw();
        }
        
        $this->footer();
    }
    
}
