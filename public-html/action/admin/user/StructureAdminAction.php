<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of StructureAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class StructureAdminAction extends AdminAction {
    
    public $id;
    
    public function _default(){
        $this->header();
        ?>
            <div class="table-container"></div>
            <script>
                MbCreateVue('MbStructureAdminTable', 'table-container');
            </script>
        <?php
        $this->footer();
    }
    
    public function delete(){
        StructureService::remove($this->id);
        redirect($this->actionUrl("_default"));
    }
    
    public function edit(){
        $form = new Form($this->actionUrl('edit', ["id" => $this->id]));
        $form->cancellAction = $this->actionUrl("_default");
        $form->entityName = "structure";
        if (StringUtils::isNotBlank($this->id)){
            $form->entity = StructureService::find($this->id);
        }
        
        $nameField = new FormTextField("Nome", "name", FormFieldType::STRING, true, NULL, ["maxLength" => 255]);
        $nameField->checkFunction = function($field, $value){
            if (StringUtils::isBlank($value)){
                return;
            }

            $dbStructure = StructureService::findByName($value);
            if ($dbStructure != null && $dbStructure["structure_id"] != $this->id){
                $field->addFieldError("Il nome è in uso per " . $dbStructure["name"]);
            }
        };
        $form->addField($nameField);

        $meccanograficoField = new FormTextField("Meccanografico", "meccanografico", FormFieldType::STRING, true, NULL, ["maxLength" => 100]);
        $meccanograficoField->checkFunction = function($field, $value){
            if (StringUtils::isBlank($value)){
                return;
            }

            $dbStructure = StructureService::findByMeccanografico($value);
            if ($dbStructure != null && $dbStructure["structure_id"] != $this->id){
                $field->addFieldError("Il meccanografico è in uso per " . $dbStructure["name"]);
            }
        };
        $form->addField($meccanograficoField);

        $form->addField(new FormSelect2Field("Scuola professionale", "is_scuola_professionale", FormFieldType::NUMBER, [
            0 => "No",
            1 => "Sì"
        ], true, "0"));

        $form->addField(new FormSelect2Field("Scuola paritaria, privata/riconosciuta", "is_scuola_paritaria", FormFieldType::NUMBER, [
            0 => "No",
            1 => "Sì"
        ], true, "0"));
        
        if ($form->isSubmit() && $form->checkFields()){
            if (StringUtils::isBlank($this->id)){
                $form->persist();
            } else {
                $form->persist(sprintf("structure_id=%d", $this->id));
            }
            redirect($this->actionUrl("_default"));
        }
        
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Edit Struttura</h3>
            </div>
        </div>
        <?php
        $form->draw();
        $this->footer();
    }
    
    public function import(){
        if (StringUtils::isBlank($this->structureId)){
            redirectWithMessage($this->actionUrl('_default'), "Struttura non selezionata.");
            return;
        }
        if (!isset($_FILES["importFile"])){
            redirectWithMessage($this->actionUrl('_default'), "Selezionare un file da caricare.");
            return;
        }
        
        $this->header();
        $this->importFile($_FILES["importFile"]["tmp_name"], $this->structureId);
        $this->footer();
    }
    
    private function importFile($inputFileName, $structureId){   
        $objPHPExcel = PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
        $sheet = $objPHPExcel->getSheet(0);
        for ($r = 3; $r < $sheet->getHighestRow(); $r++){
            $c0 = $sheet->getCellByColumnAndRow(0, $r);
            $c0 = trim($c0);
            $c0 = str_replace("'", "", $c0);
            $nameArray = explode(" ", $c0);
            
            $name = "";
            $surname = "";
            
            if (count($nameArray) == 2){
                $surname = $nameArray[0];
                $name = $nameArray[1];
            } else if (count($nameArray) > 2){
                for ($i = 0; $i < count($nameArray) - 2; $i++){
                    $surname .= $nameArray[$i];
                }
                $name = $nameArray[count($nameArray) - 1];
            } else {
                echo "Caso da valutare " . $r. " " . $c0 . " " . count($nameArray) . "<br/>";
                return;
            }
            
            $username = strtolower($name . "." . $surname);
            $email = $sheet->getCellByColumnAndRow(2, $r);    
            $email = strtolower($email);
            
            $stmt = EM::prepare("SELECT COUNT(*) as c FROM user WHERE username=:u");
            $stmt->execute([':u' => $username]);
            $res = $stmt->fetch();
            
            if ($res['c'] > 0){
                echo $username .' Utente già presente</br>';
                continue;
            }
            
            $userId = EM::insertEntity("user", [
                'name' => $name,
                'surname' => $surname,
                'email' => $email,
                'username' => $username,
                'password' => UserService::encodePassword($username),
                'insert_date' => 'NOW()',
                'confirmed' => 1,
                'status' => UserStatus::ENABLED,
                'structure_id' => $structureId
            ]);
            
            UserRoleService::addUserRole($userId, UserRole::STRUCTURE_ADMIN);
            
            echo $username. ' inserito<br/>';
        }
    }
        
}

