<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminUserAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class InternalMessageAdminAcion extends AdminAction{

    public $id;

    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Messaggi interni</h3>
            </div>

            <div class="large-4 medium-4 columns">
                <a class="button radius float_right" href="<?= $this->actionUrl("edit") ?>">
                    <i class="fa fa-plus-circle"></i>
                    Nuovo
                </a>
            </div>
        </div>

        <div class="mb-admin-internal-message-table"></div>
        <?php
        $this->footer();
    }

    public function edit(){
        $visibleSections = InternalMessageService::getUserVisibleSections();

        $form = new Form($this->actionUrl("edit", ['id' => $this->id]));
        $form->entityName = "internal_message";
        $form->cancellAction = $this->actionUrl("_default");
        $form->entity = InternalMessageService::find($this->id);

        if ($form->entity != null && !InternalMessageService::isMessageVisible($this->id)){
            redirectWithMessage($this->actionUrl("_default"), "Messaggio non accessibile");
        }

        $form->addField(new FormDateField("Visibile da", "visible_from_date", FormFieldType::DATE));
        $form->addField(new FormDateField("Visibile a", "visible_to_date", FormFieldType::DATE));
        $form->addField(new FormTextField("Oggetto", "subject", FormFieldType::STRING, true));

        $messageField = new FormTextareaField("Messaggio", "message", FormFieldType::STRING, true);
        $messageField->isWysiwyg = true;
        $form->addField($messageField);

        $dbSelectedModules = InternalMessageService::findModulesForMessage($this->id);
        $selectedModules = [];
        foreach($dbSelectedModules as $module){
            $selectedModules[] = $module["module_base_action"];
        }

        $sezioniField = new FormCheckboxMultiField("Visibile per sezioni", "sections", FormFieldType::STRING, $visibleSections, true, $selectedModules);
        $sezioniField->persist = false;
        $form->addField($sezioniField);

        if ($form->isSubmit() && $form->checkFields()){
            if(StringUtils::isBlank($this->id)){
                $this->id = $form->persist("", [
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser['user_id']
                ]);
            } else {
                $form->persist("internal_message_id=" . $this->id);
            }
            
            EM::updateEntity("internal_message", [
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser['user_id']
            ], ["internal_message_id" => $this->id]);

            $selectedModules = $sezioniField->getValue(null);
            EM::deleteEntity("internal_message_module", [
                "internal_message_id" => $this->id
            ]);

            foreach($selectedModules as $module){
                EM::insertEntity("internal_message_module", [
                    "internal_message_id" => $this->id,
                    "module_base_action" => $module
                ]);
            }

            redirectWithMessage($this->actionUrl("_default"), "Operazione eseguita");
        }

        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Edit messaggio</h3>
            </div>
        </div>

        <?= $form->draw() ?>
        <?php
        $this->footer();
    }

    public function remove(){
        if (!InternalMessageService::isMessageVisible($this->id)){
            redirectWithMessage($this->actionUrl("_default"), "Messaggio non accessibile");
            die();
        }

        EM::deleteEntity("internal_message", [
            "internal_message_id" => $this->id
        ]);
        redirectWithMessage($this->actionUrl("_default"), "Operazione eseguita");
    }
}