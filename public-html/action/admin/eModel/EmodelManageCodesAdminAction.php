<?php

class EmodelManageCodesAdminAction extends AdminAction{

    public $id;

    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Modello E: gestione codici</h3>

                <p class="ms-2">
                    La tabella riporta i codici e le descrizioni suggeriti dal sistema in fase di inserimento del modello E.
                </p>
            </div>    
            <div class="large-4 medium-4 columns">
                <a class="button radius float_right" href="<?= $this->actionUrl("edit") ?>">
                    <i class="fa fa-plus-circle"></i>
                    Nuovo
                </a>
            </div>
        </div>

        <div class="mb-e-model-manage-codes-admin-table"></div>
        <?php

        $this->footer();
    }

    public function edit(){
        $this->header();

        $dictionary = DictionaryService::find($this->id);
        if ($dictionary != null && $dictionary["group"] != Dictionary::e_model_code){
            redirect($this->actionUrl("_default"));
        }

        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Modello E: modifica codice</h3>
            </div>
        </div>
        <?php

        $form = new Form($this->actionUrl("edit", ["id" => $this->id]));
        $form->cancellAction = $this->actionUrl("_default");
        $form->entityName = "dictionary";
        $form->entity = $dictionary;

        $keyField = new FormTextField("Codice", "key", FormFieldType::STRING, true);
        $keyField->checkFunction = function($field, $value) use ($dictionary) {
            $dictionary = DictionaryService::findByGroupAndKey(Dictionary::e_model_code, $value);
            if ($dictionary != null && $dictionary["dictionary_id"] != $this->id){
                $field->addFieldError("Il codice è già presente");
            }
        };
        $form->addField($keyField);

        $fieldNote = new FormTextareaField("Descrizione", "value", FormFieldType::STRING, true);
        $form->addField($fieldNote);

        if ($form->isSubmit() && $form->checkFields()){
            if (StringUtils::isNotBlank( $this->id )){
                $form->persist("dictionary_id=" . $this->id);
            } else {
                $form->persist("", [
                    "group" => Dictionary::e_model_code
                ]);
            }

            redirect($this->actionUrl("_default"));
        }

        $form->draw();
        $this->footer();
    }

    public function import(){
        $path = __DIR__ . "/../../../files/modello_e_codici.xlsx";
        $objPHPExcel = PhpOffice\PhpSpreadsheet\IOFactory::load($path);
        $sheet = $objPHPExcel->getSheet(0);

        for ($r = 2; $r < $sheet->getHighestRow(); $r++){
            $key = $sheet->getCell("A$r")->getValue();
            $value = $sheet->getCell("B$r")->getValue();

            $dictionary = DictionaryService::findByGroupAndKey(Dictionary::e_model_code, $key);
            if ($dictionary == null){
                $dictionary = [
                    "group" => Dictionary::e_model_code,
                    "key" => $key,
                    "value" => $value
                ];
                EM::insertEntity("dictionary", $dictionary);
            } else {
                $dictionary["value"] = $value;
                EM::updateEntity("dictionary", $dictionary, [
                    "group" => Dictionary::e_model_code,
                    "key" => $key
                ]);
            }
        }

        redirect($this->actionUrl("_default"));
    }
}