<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EmodelEditedAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EmodelEditedAdminAction extends AdminAction {
    
    public $id;
    
    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Modello E: Moduli modificati</h3>

                <p class="ms-2">
                    La tabella riporta i moduli che hanno subito una modifica da parte dei referenti.
                </p>
            </div>    
        </div>

        <div class="mb-e-model-edited-admin-table"></div>
        <?php
        
        $this->footer();
    }
    
    public function edit(){
        $emodel = EModelService::find($this->id);
        if ($emodel == null){
            redirect(UriService::accessDanyActionUrl());
        }

        require_once './action/structure/eModel/EmodelAction.php';
        $userAction = new EmodelAction();
        $userAction->id = $this->id;
        $form = $userAction->buildForm($emodel);

        foreach($form->fields as $field){
            $field->readOnly = true;
        }

        $form->addField(new FormHtmlField("<h4>Gestione richiesta</h4>"));

        $oreField = new FormTextField("Ore assegnate", "ore_assegnate", FormFieldType::NUMBER, false, "", [
            "numeric" => true
        ]);
        
        $oreField->checkFunction = function($field, $value){
            if (StringUtils::isBlank($value)){
                return;
            }

            $intValue = intval($value);

            if (strval($intValue) !== strval($value)){
                $field->addFieldError("È possibile specificare solamente un valore intero.");
            } else if ($intValue > 38){
                $field->addFieldError("Numero ore massimo consentito: 38");
            } else if ($intValue < 0){
                $field->addFieldError("È necessario specificare un valore positivo");
            }
        };
        $form->addField($oreField);

        $fieldNote = new FormTextareaField("Note amministratore", "em_note_amministratore", FormFieldType::STRING);
        $form->addField($fieldNote);
        
        if ($form->isSubmit() && $form->checkFields()){
            EM::updateEntity("e_model", [
                "note_amministratore" => $fieldNote->getValueForDb(),
                "ore_assegnate" => $oreField->getValueForDb(),
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser['user_id']
            ], ["e_model_id" => $this->id]);
            
            redirectWithMessage($this->actionUrl("_default"), "Dati salvati");
        }
        
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Modello E: Dati</h3>
            </div>    
        </div>
        <?php
        $form->draw();
        $this->footer();
    }
}
