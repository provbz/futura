<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EModelCodesAdminAction
 *
 * @author marco
 */
class EModelCodesAdminAction extends AdminAction{
    
    public $school_year_id;
    
    public function _default(){
        $table = new Table();
        $table->allowExportXLS = true;
        
        $query = "SELECT code, count(*) as count
            FROM e_model_code ec
            LEFT JOIN e_model em ON ec.e_model_id = em.e_model_id
            where 1=1 ";
        
        $par = [];
        $schoolYears = SchoolYearService::findAllAsObject();
        $currentSchoolYear = SchoolYearService::findCurrentYear();
        if (StringUtils::isBlank($this->school_year_id)){
            $this->school_year_id = $currentSchoolYear["school_year_id"];
        }

        $query .= "AND school_year_id=:school_year_id ";
        $par["school_year_id"] = $this->school_year_id;
        
        $query .= "group by ec.code ";
        $results = EM::execQuery($query, $par);
        $table->data = $results;
        
        $table->addColumn(new TableColumn("code", "Codice"));
        $table->addColumn(new TableColumn("count", "Numero"));
        
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Modello E: Riepilogo codici di diagnosi</h3>
            </div>    
        </div>
        <div class="table-row row-100">
            <div class="large-12 columns">
                <div class="filters">
                    <form method="POST" action="<?= $this->actionUrl() ?>">
                        <div class="row-100 margin-bottom">
                            <div class="columns small-12 medium-6 large-4">
                                <?= FilterUtils::drawSelectFilter("Anno scolastico", "school_year_id", $schoolYears, $this->school_year_id) ?>
                            </div>
                        </div>
                        <?= FilterUtils::drawSubmit() ?>
                    </form>          
                </div>
            </div>
        </div>
        <div class="row-100">
            <?= $table->draw() ?>
        </div>
        <?php
        $this->footer();
    }
    
}
