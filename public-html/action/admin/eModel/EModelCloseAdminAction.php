<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EModelCloseAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EModelCloseAdminAction extends AdminAction {

    public $type;

    public function _default(){
        $res = SchoolYearService::findLast();
        $schoolYear = $res->fetch();

        $primariaSchoolYearId = PropertyService::find(PropertyNamespace::MODELLO_E, PropertyKey::primaria_current_year_id);
        $secondariaSchoolYearId = PropertyService::find(PropertyNamespace::MODELLO_E, PropertyKey::secondaria_current_year_id);

        $data = [
            "currentSchoolYear" => $schoolYear,
            "primariaSchoolYear" => SchoolYearService::find($primariaSchoolYearId["value"]),
            "secondariaSchoolYear" => SchoolYearService::find($secondariaSchoolYearId["value"])
        ];

        $this->header();
        ?>
        <div class="mb-e-model-close-admin-container"></div>
        <script>
            MbCreateVue('mb-e-model-close-admin', 'mb-e-model-close-admin-container', <?= json_encode($data) ?>);
        </script>
        <?php
        $this->footer();
    }

    public function close(){
        $request = $this->getPostBodyAsObject();

        if (!property_exists($request, "type")){
            return;
        }

        if ($request->type != 'primaria' && $request->type != 'secondaria'){
            return;
        }

        $condition = "";
        if ($request->type == "primaria"){
            $condition = " NOT ";
        }

        $notVerified = EM::execQuerySingleResult("SELECT 1 
            FROM e_model 
            WHERE class {$condition} LIKE '%S2' AND closed=0 AND school_year_id = :from_school_year_id AND edited=1", [
                "from_school_year_id" => $request->from_school_year_id,
            ]);
        if ($notVerified != null){
            echo json_encode([
                "hasErrors" => true,
                "error" => "Sono presenti modelli non verificati. Completare la verifica prima di eseguire la chiusura."
            ]);

            die();
        }

        $eModels = EM::execQuery("SELECT * FROM e_model WHERE class {$condition} LIKE '%S2' AND closed=0 AND school_year_id = :from_school_year_id", [
            "from_school_year_id" => $request->from_school_year_id,
        ]); 

        foreach ($eModels as $eModel) {
            EM::updateEntity("e_model", [
                "closed" => 1
            ], [
                "e_model_id" => $eModel["e_model_id"]
            ]);

            $oldId = $eModel["e_model_id"];
            unset($eModel["e_model_id"]);
            $eModel["school_year_id"] = $request->to_school_year_id;
            $newId = EM::insertEntity("e_model", $eModel);

            $insertQuery = "INSERT INTO e_model_code (e_model_id, code, description)
                SELECT {$newId}, code, description
                FROM e_model_code
                WHERE e_model_id={$oldId}
            ";
            EM::execQuery($insertQuery);

            EM::execQuery("INSERT INTO attachment_entity (entity, entity_id, type, attachment_id)
                SELECT entity, {$newId}, type, attachment_id
                FROM attachment_entity
                where entity = :entity AND entity_id={$oldId}", [
                    "entity" => EntityName::E_MODEL
                ]);
        }

        if ($request->type == 'primaria'){
            PropertyService::persist(PropertyNamespace::MODELLO_E, PropertyKey::primaria_current_year_id, $request->to_school_year_id);
        } else {
            PropertyService::persist(PropertyNamespace::MODELLO_E, PropertyKey::secondaria_current_year_id, $request->to_school_year_id);
        }

        echo "ok";
    }
}