<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EmodelAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EmodelAdminAction extends AdminAction {
    
    public $id;
    
    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Modello E: Dati</h3>
            </div>    
        </div>

        <div class="mb-e-model-admin-table"></div>
        <?php
        
        $this->footer();
    }

    public function updateOreAssegnate(){
        $data = json_decode(file_get_contents('php://input'), true);
        EM::updateEntity("e_model", [
            "ore_assegnate" => $data["ore_assegnate"]
        ], [
            "e_model_id" => $data["e_model_id"]
        ]);
    }
    
    public function edit(){
        $emodel = EModelService::find($this->id);
        if ($emodel == null){
            redirect(UriService::accessDanyActionUrl());
        }

        require_once './action/structure/eModel/EmodelAction.php';
        $eModelAction = new EmodelAction();
        $eModelAction->id = $this->id;
        $eModelAction->structureId = $emodel["structure_id"];
        $eModelAction->structure = StructureService::find($eModelAction->structureId);
        $eModelAction->currentUser = $this->currentUser;
        $form = $eModelAction->buildForm($emodel);

        foreach($form->fields as $field){
            if ($field->name == "school_year_id"){
                continue;
            }
            $field->readOnly = $emodel["edited"] == 0;
        }

        $form->addField(new FormHtmlField("<h4>Gestione richiesta</h4>"));

        $form->addField(new FormCheckField("Disabilità visiva", "disabilita_visiva", FormFieldType::NUMBER, [1 => "Sì"]));
        $form->addField(new FormCheckField("Disabilità uditiva", "disabilita_uditiva", FormFieldType::NUMBER, [1 => "Sì"]));
        $form->addField(new FormCheckField("Disabilità motoria ausilio carrozzina", "disabilita_motoria", FormFieldType::NUMBER, [1 => "Sì"]));

        if ($emodel["edited"] == 1){
            $fieldNoteRef = new FormTextareaField("Note per i referenti", "note_per_referenti", FormFieldType::STRING);
            $fieldNoteRef->note = "Le note saranno inviate al referente nella notifica di approvazione";
            $form->addField($fieldNoteRef);

            $fieldNote = new FormTextareaField("Note amministratore", "note_amministratore", FormFieldType::STRING);
            $fieldNote->note = "Note per l'amministratore, non visibili ai referenti";
            $form->addField($fieldNote);

            $eModelAction->persist($form);
            if ($form->isSubmit() && $form->isValid()){
                EM::updateEntity("e_model", [
                    "edited" => 0
                ], [
                    "e_model_id" => $emodel["e_model_id"]
                ]);

                if (!NoticeService::noticeExists(EntityName::E_MODEL, $this->id, NoticeType::E_MODEL_APPROVED)){
                    NoticeService::persist([
                        "type" => NoticeType::E_MODEL_APPROVED,
                        "entity" => EntityName::E_MODEL,
                        "entity_id" => $this->id
                    ]);
                }

                redirectWithMessage(UriService::buildPageUrl("/admin/eModel/EmodelEditedAdminAction"), "Dati salvati");
            }
        } else {
            $oreField = new FormTextField("Ore assegnate", "ore_assegnate", FormFieldType::NUMBER, false, "", [
                "numeric" => true
            ]);
            $oreField->readOnly = $emodel["closed"] == 1;
            
            $oreField->checkFunction = function($field, $value){
                if (StringUtils::isBlank($value)){
                    return;
                }

                $intValue = intval($value);

                if (strval($intValue) !== strval($value)){
                    $field->addFieldError("È possibile specificare solamente un valore intero.");
                } else if ($intValue > 38){
                    $field->addFieldError("Numero ore massimo consentito: 38");
                } else if ($intValue < 0){
                    $field->addFieldError("È necessario specificare un valore positivo");
                }
            };
            $form->addField($oreField);

            $fieldNote = new FormTextareaField("Note amministratore", "note_amministratore", FormFieldType::STRING);
            $fieldNote->note = "Note per l'amministratore, non visibili ai referenti";
            $fieldNote->readOnly = $emodel["closed"] == 1;
            $form->addField($fieldNote);

            $eModelAction->persist($form);
        }
        
        $this->header();
        $editUser = UserService::find($emodel["last_edit_user_id"]);

        $eModelAction->formScripts();
        ?>
        <style>
            .highlight{
                background-color: #ffff008f;
            }
        </style>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Modello E: Dati</h3>
            </div>    
        </div>
        <?php $this->drawTabs() ?>

        <?php if ($emodel["edited"] == 1){ ?>
            <div class="alert">
                <p class="ms-2">
                    Il modulo è stato modificato dal referente e richiede la verifica dei dati.
                </p>

                <div class="ms-2 my-5">
                    Modificato il: <?= _t(TextService::parseDate( $emodel["last_edit_date"] )) ?>, da <?= _t($editUser["email"]) ?>
                </div>
            </div>
        <?php 
            $this->highlight();
        } 
        ?>

        <div class="tabs-content">
            <?php $form->draw(); ?>
        </div>

        <?php
        $this->footer();
    }

    function highlight(){
        $journal = JournalService::findLastFor(EntityName::E_MODEL, $this->id, null);
        if ($journal == null){
            return;
        }

        $journalRows = JournalService::findJournalRowsForJournalId($journal["journal_id"])->fetchAll();
        ?>
        <script>
            $(function(){
                var journalRows = <?= json_encode($journalRows) ?>;
                var $form = $("#form_generic");

                for (var i = 0; i < journalRows.length; i++){
                    var row = journalRows[i];
                    var $field = $form.find("#campo_form_" + row.field_name);
                    if ($field.length == 0){
                        continue;
                    }

                    $field.addClass("highlight");
                }
            });
        </script>
        <?php
    }

    public function history(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Modello E: Dati</h3>
            </div>    
        </div>
        <?php $this->drawTabs(1) ?>

        <div class="tabs-content">
            <div class="MbEModelJournalTable-container"></div>
            <script>
                MbCreateVue('MbEModelJournalTable', 'MbEModelJournalTable-container', <?= json_encode([
                    "id" => $this->id 
                ]) ?>);
            </script>
        </div>
        <?php
        $this->footer();
    }

    private function drawTabs($selectedTab = 0){
        ?>
        <ul class="tabs" data-tabs="02wqw0-tabs" id="example-tabs" role="tablist">
            <li class="tabs-title <?= $selectedTab == 0 ? "is-active" : "" ?>" role="presentation">
                <a href="<?= $this->actionUrl("edit", ["id" => $this->id]) ?>" aria-selected="<?= $selectedTab == 0 ? "true" : "" ?>" role="tab">Form</a>
            </li>
            <li class="tabs-title <?= $selectedTab == 1 ? "is-active" : "" ?>" role="presentation">
                <a href="<?= $this->actionUrl("history", ["id" => $this->id]) ?>" role="tab" aria-selected="<?= $selectedTab == 1 ? "true" : "" ?>">Log modifiche</a>
            </li>
        </ul>
        <?php
    }
}
