<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EModelDiagnosyAdminAction
 *
 * @author marco
 */
class EModelDiagnosyAdminAction extends AdminAction {
    
    public $filter_school_year_id;
    
    public function _default(){
        $table = new Table();
        $table->allowExportXLS = true;
        
        $query = "SELECT diagnosy_type_dictionary, count(*) as count
            FROM e_model es 
            WHERE 1=1 ";
        
        $par = [];
        
        $schoolYears = SchoolYearService::findAllAsObject();
        $currentSchoolYear = SchoolYearService::findCurrentYear();
        if (StringUtils::isBlank($this->filter_school_year_id)){
            $this->filter_school_year_id = $currentSchoolYear["school_year_id"];
        }

        $query .= "AND school_year_id=:school_year_id ";
        $par["school_year_id"] = $this->filter_school_year_id;
        
        $query .= "GROUP BY diagnosy_type_dictionary ";
        $results = EM::execQuery($query, $par);
        $table->data = $results;
        
        $types = DictionaryService::findGroupAsDictionary(Dictionary::MODEL_E_DIAGNOSY_TYPE);
        $table->addColumn(new TableColumn("diagnosy_type_dictionary", "Tipo diagnosi", function($v) use($types){
            return isset($types[$v]) ? $types[$v] : "";
        }));
        $table->addColumn(new TableColumn("count", "Numero"));
        
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Modello E: Riepilogo per tipologia diagnosi</h3>
            </div>    
        </div>
        <div class="table-row row-100">
            <div class="large-12 columns">
                <div class="filters">
                    <form method="POST" action="<?= $this->actionUrl() ?>">
                        <div class="row-100 margin-bottom">
                            <div class="columns small-12 medium-6 large-4">
                                <?= FilterUtils::drawSelectFilter("Anno scolastico", "filter_school_year_id", $schoolYears, $this->filter_school_year_id) ?>
                            </div>
                        </div>
                        <?= FilterUtils::drawSubmit() ?>
                    </form>          
                </div>
            </div>
        </div>
        <div class="row-100">
            <?= $table->draw() ?>
        </div>
        <?php
        $this->footer();
    }
    
}
