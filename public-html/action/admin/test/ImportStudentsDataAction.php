<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ImportStudentsDataAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ImportStudentsDataAction extends AdminAction {

    public function _default(){
        $this->header();

        $form = new Form($this->actionUrl());
        $form->entity = [
            "write" => 0
        ];

        $writeField = new FormCheckField('Scrivi dati', 'write', FormFieldType::STRING, [1 => 'Sì'] );
        $form->addField($writeField);

        $grades = DictionaryService::findGroupAsDictionary(Dictionary::GRADE);
        $gradoField = new FormSelect2Field("Grado scolastico", "grado_scolastico", FormFieldType::STRING, $grades, true);
        $form->addField($gradoField);

        $fileField = new FormAttachmentsField("File", "file_studenti", "", 0, true);
        $fileField->manAttachments = 1;
        $fileField->extensions = ["xls", "xlsx"];
        $form->addField($fileField);

        if ($form->isSubmit() && $form->checkFields()){
            $gradoScolastico = $gradoField->getDecodedValue(null)[0];

            $attachments = $fileField->getDecodedValue();
            $attachment = $attachments[0];
            
            $attachmentEntity = AttachmentService::find($attachment["AttachmentId"]);
            $path = AttachmentService::buildAttachmentPath($attachmentEntity);

            $this->evaluateExcel($writeField->getValue($form->entity), $path, $gradoScolastico);
            die();
        }

        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Import dati studenti</h3>
            </div>
            <div class="large-4 medium-4 columns">
            </div>
        </div>

        <div class="row-100">
            <div class="columns small-12">
                <p>Caricare un file che rispetti il modello dati concordato.</p>

                <ul>
                    <li>
                        Riga 1: intestazione, la riga non verrà considerata;
                    </li>
                    <li>
                        Colonna 1: Nome Istituto, il nome deve essere presente in piattaforma;
                    </li>
                    <li>
                        Colonna 2: Nome Plesso, presente in piattaforma nell'istituto;
                    </li>
                    <li>
                        Colonna 3: Classe;
                    </li>
                    <li>
                        Colonna 4: Sezione: per l'infanzia può essere lasciata vuota;
                    </li>
                    <li>
                        Colonna 5: Cognome studente;
                    </li>
                    <li>
                        Colonna 6: Nome studente;
                    </li>
                    <li>
                        Colonna 7: Data di nascita;
                    </li>
                    <li>
                        Colonna 8: Nazionalità;
                    </li>
                </ul>

                <p>ATTENZIONE: il caricamento dei dati degli studenti sovrascriverà le informazioni attualmente presenti basandosi su Nome, Cognome e data di nascita dello studente.</p>
            </div>
        </div>

        <?php
        $form->draw();
        $this->footer();
    }

    private function evaluateExcel($write, $path, $gradoScolastico){
        $fileContent = EncryptService::decryptfileInMemory($path);
        $path .= ".xlsx";
        file_put_contents($path, $fileContent);
        
        $objPHPExcel = PhpOffice\PhpSpreadsheet\IOFactory::load($path);
        $sheet = $objPHPExcel->getSheet(0);

        ?>
        <table>
            <thead>
                <tr>
                    <th>Riga</th>
                    <th>Istituto</th>
                    <th>Plesso</th>
                    <th>Classe</th>
                    <th>Sezione</th>
                    <th>Cognome</th>
                    <th>Nome</th>
                    <th>Data nascita</th>
                    <th>Nazionalità</th>
                    <th>Esito</th>
                </tr>
            </thead>
            <tbody>
            <?php
            error_reporting(E_ALL ^ E_WARNING );
            for ($r = 2; $r < $sheet->getHighestRow(); $r++){
                try{
                    set_time_limit(30);
                    $istituto = $sheet->getCellByColumnAndRow(1, $r);
                    $istituto = trim($istituto);

                    $plesso = $sheet->getCellByColumnAndRow(2, $r);
                    $plesso = trim($plesso);

                    $classe = $sheet->getCellByColumnAndRow(3, $r);
                    $classe = trim($classe);

                    $sezione = $sheet->getCellByColumnAndRow(4, $r);
                    $sezione = trim($sezione);

                    $cognome = $sheet->getCellByColumnAndRow(5, $r);
                    $cognome = trim($cognome);

                    $nome = $sheet->getCellByColumnAndRow(6, $r);
                    $nome = trim($nome);

                    $dataNascita = $sheet->getCellByColumnAndRow(7, $r);
                    $dataNascita = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($dataNascita->getValue());

                    $nazionalita = $sheet->getCellByColumnAndRow(8, $r);
                    $nazionalita = trim($nazionalita);

                    $esito = $this->importStudent(
                        $write,
                        $gradoScolastico,
                        $istituto, $plesso,
                        $classe, $sezione,
                        $cognome, $nome, $dataNascita, $nazionalita
                    );

                    ?>
                    <tr>
                        <td><?= _t($r) ?></td>
                        <td><?= _t($istituto) ?></td>
                        <td><?= _t($plesso) ?></td>
                        <td><?= _t($classe) ?></td>
                        <td><?= _t($sezione) ?></td>
                        <td><?= _t($cognome) ?></td>
                        <td><?= _t($nome) ?></td>
                        <td><?= _t($dataNascita->format('d-m-Y')) ?></td>
                        <td><?= _t($nazionalita) ?></td>
                        <td><?= _t($esito) ?></td>
                    </tr>
                    <?php
                } catch (ErrorException $ee){
                    ?>
                    <tr>
                        <td><?= $r ?></td>
                        <td colspan="9">
                            <?= _t($ee->getMessage()) ?><br/>
                            <?= _t($ee->getTraceAsString()) ?>
                        </td>
                    </tr>
                    <?php
                }

                if (ob_get_length()){
                    ob_end_flush();
                }
                flush();
            }
            ?>
            </tbody>
        </table>
        <?php

        $this->footer();
    }

    private function importStudent($write, $gradoScolastico, $istitutoNome, $plessoNome,
                    $classeNome, $sezione,
                    $cognome, $nome, $dataNascita, $nazionalita){
        
        $structure = StructureService::findRootByName($istitutoNome);
        if ($structure == null){
            return "Istituto non trovato";
        }

        $plesso = StructureService::findWithParentWithName($structure["structure_id"], $plessoNome);
        if ($plesso == null){
            return "Plesso non trovato";
        }

        $currentYear = SchoolYearService::findCurrentYear();
        $classDictionaryValue = "";
        if ($gradoScolastico == "I"){
            $classDictionaryValue = "3I";
            $sezione = "Unica";
        } else if ($gradoScolastico == "P"){
            $classDictionaryValue = substr($classeNome, 0, 1) . "P";
        } else {
            return "Grado scolastico non supportato";
        }

        $classDictionary = DictionaryService::findByGroupAndKey(Dictionary::CLASS_GRADE, $classDictionaryValue);
        if ($classDictionary == null){
            return "Classe non valida";
        }

        $nazione = DictionaryService::findByGroupAndValue(Dictionary::NAZIONALITA_ITA, $nazionalita);
        if ($nazione == null){
            return "Nazione non trovata a db";
        }

        $esito = '';

        $classe = StructureClassService::findForStructureAndClassAndSezione($currentYear["school_year_id"],
            $plesso["structure_id"],
            $classDictionaryValue, $sezione);
        
        if ($classe == null){
            if ($write){
                $classId = EM::insertEntity("structure_class", [
                    "structure_id" => $plesso["structure_id"],
                    "school_year_id" => $currentYear["school_year_id"],
                    "class_dictionary_value" => $classDictionaryValue,
                    "section" => $sezione,
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser["user_id"],
                    "last_edit_date" => "NOW()",
                    "last_edit_user_id" => $this->currentUser["user_id"],
                    "row_status" => RowStatus::READY
                ]);
                $classe = StructureClassService::find($classId);
            }

            $esito .= 'Classe inserita';
        }

        $student = UserService::findByNameSurnameAndBirthDate($nome, $cognome, $dataNascita->format("Y-m-d"));
        if ($student == null){
            if ($write) {
                $userId = EM::insertEntity("user", [
                    "name" => $nome,
                    "surname" => $cognome,
                    "birth_date" => $dataNascita->format("Y-m-d"),
                    "section" => UserSection::MONDO_PAROLE
                ]);
                EM::insertEntity("user_year", [
                    "user_id" => $userId,
                    "plesso_structure_id" => $plesso["structure_id"],
                    "classe" => $classDictionaryValue,
                    "sezione" => $sezione,
                    "nazionalita_ita_dictionary_key" => $nazione["key"]
                ]);
                $student = UserService::find($userId);

                EM::insertEntity("user_role", [
                    'user_id' => $userId, 
                    'role_id' => UserRole::USER
                ]);
            }

            $esito .= ' | Nuovo studente';
        } else {
            $studentUserYear = UserYearService::findUserYearByUser($student['user_id']);

            if ($student['birth_date'] != $dataNascita->format("Y-m-d") . ' 00:00:00' ||
                $studentUserYear['classe'] != $classDictionaryValue ||
                $studentUserYear['sezione'] != $sezione){
                if ($write){
                    EM::updateEntity("user", [
                        "birth_date" => $dataNascita->format("Y-m-d"),
                        "section" => UserSection::MONDO_PAROLE
                    ], [
                        "user_id" => $student["user_id"]
                    ]);

                    EM::updateEntity("user_year", [
                        "classe" => $classDictionaryValue,
                        "sezione" => $sezione,
                        "nazionalita_ita_dictionary_key" => $nazione["key"]
                    ], [
                        "user_id" => $student["user_id"]
                    ]);
                }

                $esito .= ' | Studente aggiornato';
            }
        }

        if ($student != null){
            $userStructure = StructureService::findUserStructure($student["user_id"], $structure["structure_id"]);
            if ($userStructure == null){
                if ($write){
                    StructureService::addUserStructure($student["user_id"], $structure["structure_id"], [UserRole::STUDENTE]);

                    $write .= ' | Utente aggiunto all\'istituto';
                }
            }

            $structureClassUser = StructureClassUserService::findForUserAndYear($student["user_id"], $currentYear["school_year_id"]);
            if ($structureClassUser == null){
                if ($write){
                    EM::insertEntity("structure_class_user", [
                        "structure_class_id" => $classe["structure_class_id"],
                        "user_id" => $student["user_id"],
                        "row_status" => RowStatus::READY,
                        "insert_user_id" => $this->currentUser["user_id"],
                        "insert_date" => "NOW()",
                        "last_edit_user_id" => $this->currentUser["user_id"],
                        "last_edit_date" => "NOW()",
                        "role_id" => UserRole::STUDENTE
                    ]);
                }

                $esito .= ' |  Nuova associazione classe';
            } else {
                if ($structureClassUser['structure_class_id'] != $classe["structure_class_id"]){
                    if($write){
                        EM::updateEntity("structure_class_user", [
                            "structure_class_id" => $classe["structure_class_id"]
                        ], [
                            "structure_class_user_id" => $structureClassUser["structure_class_user_id"]
                        ]);
                    }

                    $esito .= ' | Aggiornata associazione classe';
                }
            }
        }

        return $esito;
    }
}