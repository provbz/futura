<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of SomministrazioneStatsAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SomministrazioneStatsAdminAction extends AdminAction{

    public $id;
    private $somministrazione;

    public function _prepare(){
        parent::_prepare();

        $this->somministrazione = SomministrazioneService::find($this->id);
        if ($this->somministrazione == null){
            redirectWithMessage(UriService::accessDanyActionUrl(), "Dati non validi");
            return;
        }
    }

    public function _default(){
        $this->header();

        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Statistiche prove</h3>
            </div>
            <div class="large-4 medium-4 columns">
            </div>
        </div>

        <div class="mb-somministrazione-stats-admin-page-cnt"></div>
        <script type="text/javascript">
        MbCreateVue("mb-somministrazione-stats-admin-page", "mb-somministrazione-stats-admin-page-cnt", {
            "somministrazioneId": <?= _t($this->id) ?>
        })
        </script>
        <?php

        $this->footer();
    }

    public function loadData(){
        $structures = StructureService::findAllRoot();
        $testsData = $this->loadTestData($this->somministrazione["somministrazione_id"]);        
        $totalStudentsForStructures = $this->countStudentsForRootStructures($this->somministrazione["school_year_id"], $this->somministrazione["class_dictionary_value"]);

        $tableData = [];
        foreach ($structures as $structure) {
            $totalStudents = ReflectionUtils::getValue($totalStudentsForStructures, $structure["structure_id"], 0);

            if ($totalStudents == 0){
                continue;
            }

            $row = [];
            $row["structure_name"] = $structure["name"];
            $row["structure_id"] = $structure["structure_id"];
            $row["totale_studenti"] = $totalStudents;
            $row["tests"] = []; 

            foreach ($testsData as $test) {
                $testResults = $this->loadTestResults($this->somministrazione["school_year_id"], $structure["structure_id"], $test["test"]["somministrazione_test_id"]);

                if (count($testResults) == 0){
                    $testResults["not_set"] = $totalStudents;
                } else if (ArrayUtils::sum($testResults) < $totalStudents){
                    if (!isset($testResults["not_set"])){
                        $testResults["not_set"] = 0;
                    }
                    $testResults["not_set"] += $totalStudents - ArrayUtils::sum($testResults);
                }

                $row["tests"][$test["test"]["test_id"]] = $testResults;
            }
            
            $tableData[] = $row;
        }

        $total = SomministrazioneStatsService::computeTotal($tableData, $testsData);

        header('Content-Type: application/json');
        $response = new ApiResponse([
            "test_data" => $testsData,
            "table" => $tableData,
            "total" => $total
        ]);
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    private function loadTestData($somministrazioneId){
        $tests = SomministrazioneService::findAllTestsForSomministrazione($this->id);

        $results = [];
        foreach($tests as $test){
            $testFile = TestService::loadTest($test["code"]);

            $row = [
                "test" => $test,
                "variables" => $testFile->getVariablesNames()
            ];

            $results[] = $row;
        }

        return $results;
    }

    public function countStudentsForRootStructures($schoolYearId, $classDictionaryValue){
        $rows = EM::execQuery("select s.parent_structure_id, count(*) as c
            from structure_class_user scu
            left join structure_class sc on sc.structure_class_id=scu.structure_class_id
            left join structure s ON sc.structure_id=s.structure_id
            where scu.row_status='ready' 
                and scu.role_id=:student_role_id
                and sc.class_dictionary_value=:class_dictionary_value
                and sc.school_year_id=:school_year_id
            group by s.parent_structure_id", [
                "school_year_id" => $schoolYearId,
                "class_dictionary_value" => $classDictionaryValue,
                "student_role_id" => UserRole::STUDENTE
            ]);

        $results = [];
        foreach($rows as $row){
            $results[$row["parent_structure_id"]] = $row["c"];
        }
        return $results;
    }

    public function loadTestResults($schoolYearId, $parentStructureId, $somministrazioneTestId){
        $rows = EM::execQuery("SELECT stru.status, variable_name, fascia, count(*) as c
            from somministrazione_test_result_user stru
            left join somministrazione_test_result_user_variable struv ON struv.somministrazione_test_result_user_id=stru.somministrazione_test_result_user_id
                and stru.status<>'non_valida'
            left join structure_class_user scu ON scu.user_id=stru.user_id
            left join structure_class sc on sc.structure_class_id=scu.structure_class_id
            left join structure s on sc.structure_id=s.structure_id
            where s.parent_structure_id=:parent_structure_id
                AND scu.role_id=:student_role_id
                and sc.school_year_id=:school_year_id
                AND somministrazione_test_id=:somministrazione_test_id
                and scu.row_status = 'ready'
            group by stru.status, variable_name, fascia
            order by stru.status, variable_name, fascia", [
                "school_year_id" => $schoolYearId,
                "parent_structure_id" => $parentStructureId,
                "somministrazione_test_id" => $somministrazioneTestId,
                "student_role_id" => UserRole::STUDENTE
            ]);

        $results = [];
        foreach($rows as $row){
            if ($row["status"] == SomministrazioneUserStatus::SOMMINISTRATA){
                $results[$row["variable_name"]]["f_" . $row["fascia"]] = $row["c"];
            } else if ($row["status"] == null){
                $results["not_set"] = $row["c"];
            } else {
                $results[$row["status"]] = $row["c"];
            }
        }

        return $results;
    }
}