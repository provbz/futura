<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of TestStatsAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class TestStatsAdminAction extends AdminAction {

    public $test_id;
    private $test;
    private $testFile;

    public function _default(){
        $this->test = TestService::find($this->test_id);
        if ($this->test == null){
            redirectWithMessage(UriService::buildPageUrl("/admin/test/TestAction"), "Test non valido");
        }

        $this->testFile = TestService::loadTest($this->test["code"]);

        $varArray = [];
        foreach($this->testFile->getVariablesNames() as $var){
            $varArray[] = $var;
        }

        $componentParameters = [
            "testId" => $this->test_id,
            "variables" => $varArray,
            "schoolYears" => SchoolYearService::findAll()->fetchAll()
        ];

        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Analisi dati test: <?= _t($this->test["name"]) ?> - <?= _t($this->test["periodo_dictionary_value"]) ?></h3>
            </div>
        </div>

        <div class="mb-test-stats-admin-page-1"></div>
        <script type="text/javascript">
        MbCreateVue('mb-test-stats-admin-page', 'mb-test-stats-admin-page-1', 
            <?php echo json_encode($componentParameters) ?>);
        </script>
        <?php        
        $this->footer();
    }

    public function loadData(){
        $request = new LoadDataRequest();
        $request->setJson(file_get_contents("php://input"));
        
        if (StringUtils::isBlank($request->testId)){
            $response = new ApiResponse($request);    
            $response->success = false;
            $response->writeResponse();
        }

        $test = TestService::find($request->testId);
        if ($test == null){
            $response = new ApiResponse($request);    
            $response->success = false;
            $response->writeResponse();
        }

        $testFile = TestService::loadTest($test["code"]);
        $variables = $testFile->getVariablesNames();
        $variable = $variables[$request->variableName];
        if ($variable == null){
            $response = new ApiResponse($request);    
            $response->success = false;
            $response->writeResponse();
        }

        $testResults = $this->loadTestResults($request->testId, $request->schoolYearId, $request->variableName, $request->minValue, $request->maxValue);
        $frequencyTable = $this->prepareFrequencyTable($testResults);
        $centiliTable = $this->prepareCentili($frequencyTable);
        
        $response = new ApiResponse([
            "chartData" => $testResults,
            "frequencyTable" => $frequencyTable,
            "centiliTable" => $centiliTable
        ]);
        $response->writeResponse();
    }

    private function prepareCentili($frequencyTable){
        $table = [];

        if ($frequencyTable == null || count($frequencyTable) == 0){
            return $table;
        }

        $requestCentils = [];
        for ($i = 5; $i <= 95; $i += 5){
            $requestCentils[] = $i;
        }
        
        $N = $frequencyTable[count($frequencyTable) - 1]["fc"];
        foreach ($requestCentils as $requestCentil) {
            $posc = $this->getCentilPos($requestCentil, $N);

            $frequencyIndex = intval($posc);
            $indexInferiore = $this->getValueWithFrequency($frequencyTable, $frequencyIndex);
            $punteggioInf = $frequencyTable[$indexInferiore]["value"];

            $frequencyIndexScarto = $posc - floor($posc);
            $punteggio = 0;

            if ($frequencyIndexScarto == 0){
                $punteggio = $frequencyTable[$indexInferiore]["value"];
            } else if ($frequencyIndexScarto > 0){
                $indexInSuperiore = $this->getValueWithFrequency($frequencyTable, $frequencyIndex + 1);
                $punteggioInf = $frequencyTable[$indexInferiore]["value"];
                 $punteggioSup = $frequencyTable[$indexInSuperiore]["value"];

                $punteggio = $punteggioInf + ($punteggioSup - $punteggioInf) * $frequencyIndexScarto;
                $punteggio = intval($punteggio);
            }

            $row = [
                "centile" => $requestCentil,
                "value" => $punteggio
            ];

            $table[] = $row;
        }

        return $table;
    }

    private function getValueWithFrequency($table, $index){
        for ($i = 0; $i < count($table); $i++) {
            if ($index <= $table[$i]["fc"]){
                return $i;
            } 
        }

        return count($table) - 1;
    }

    private function getCentilPos($centil, $N){
        return ($N + 1) / 100 * $centil;
    }

    private function prepareFrequencyTable($testResilts){
        $table = [];

        $fc = 0;
        foreach ($testResilts as $testResult) {
            $fc += $testResult["c"];

            $row = [
                "value" => $testResult["value"],
                "f" => $testResult["c"],
                "fc" => $fc
            ];
            $table[] = $row;
        }

        return $table;
    }

    private function loadTestResults($testId, $schoolYearId, $variableName, $minValue = null, $maxValue = null){
        $params = [
            "s_row_status" => RowStatus::READY,
            "test_id" => $testId,
            "school_year_id" => $schoolYearId,
            "variable_name" => $variableName
        ];

        $query = "SELECT value, count(1) as c
            from somministrazione_test st
            left join somministrazione s ON st.somministrazione_id=s.somministrazione_id
            left join somministrazione_test_result_user stru ON stru.somministrazione_test_id = st.somministrazione_test_id
            left join somministrazione_test_result_user_variable struv ON struv.somministrazione_test_result_user_id = stru.somministrazione_test_result_user_id
            where 
                s.row_status=:s_row_status
                AND test_id=:test_id
                AND school_year_id=:school_year_id
                AND stru.status='somministrata'
                AND struv.variable_name=:variable_name";

        if ($minValue != null){
            $query .= " AND value >= :min_value";
            $params["min_value"] = $minValue;
        }

        if ($maxValue != null){
            $query .= " AND value <= :max_value";
            $params["max_value"] = $maxValue;
        }

        $query .= " group by value
            order by value";

        $rows = EM::execQuery($query, $params);

        return $rows->fetchAll();
    }
}

class LoadDataRequest{
    public $testId;
    public $schoolYearId;
    public $variableName;
    public $minValue;
    public $maxValue;

    public function setJson($json) {
        $data = json_decode($json);
        foreach ($data AS $key => $value) $this->{$key} = $value;
    }

    public function set($data) {
        foreach ($data AS $key => $value) $this->{$key} = $value;
    }
}