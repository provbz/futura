<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of SomministrazioniAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SomministrazioniAdminAction extends AdminAction{

    public $id;

    public function _default(){
        $this->header();
        
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Prove</h3>
            </div>
            <div class="large-4 medium-4 columns">
                <a class="button radius float_right" href="<?= $this->actionUrl("edit") ?>">
                    <i class="fa fa-plus-circle"></i>
                    Nuovo
                </a>
            </div>
        </div>

        <div class="mb-somministrazione-admin-table"></div>
        <?php
        $this->footer();
    }

    public function remove(){
        EM::updateEntity("somministrazione", [
            "row_status" => RowStatus::DELETED
        ], [
            "somministrazione_id" => $this->id
        ]);

        redirectWithMessage($this->actionUrl("_default"), "Operazione eseguita");
    }

    public function edit(){
        $form = new Form($this->actionUrl("edit", ['id' => $this->id]));
        $form->entityName = "somministrazione";
        $form->cancellAction = $this->actionUrl("_default");
        $form->entity = SomministrazioneService::find($this->id);

        $somministrazioneStato = SomministrazioneStatus::WAITING;
        if ($form->entity != null){
            $somministrazioneStato = SomministrazioneService::getStatus($form->entity);
        }

        $schoolYears = SchoolYearService::findAllAsObject();
        $form->addField(new FormSelect2Field("Anno scolastico", "school_year_id", FormFieldType::STRING, $schoolYears, true));

        $classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $form->addField(new FormSelect2Field("Classe", "class_dictionary_value", FormFieldType::STRING, $classes, true));

        $periodo = DictionaryService::findGroupAsDictionary(Dictionary::TEST_PERIODO);
        $form->addField(new FormSelect2Field("Periodo", "periodo_dictionary_value", FormFieldType::STRING, $periodo, true));

        $tests = TestService::findAll();
        $selectTestValues = [];
        foreach ($tests as $test) {
            $selectTestValues[] = [
                "value" => $test["test_id"],
                "text" => $test["name"],
                "class_dictionary_value" => $test["class_dictionary_value"],
                "periodo_dictionary_value" => $test["periodo_dictionary_value"]
            ];
        }

        $selectedTests = SomministrazioneService::findAllTestsForSomministrazione($this->id);
        $selectSelectedTests = [];
        foreach ($selectedTests as $test) {
            $selectSelectedTests[] = $test["test_id"];
        }

        $testSelectField = new FormSelect2Field("Test", "somministrazione_test", FormFieldType::STRING, $selectTestValues, true);
        $testSelectField->multiple = true;
        $testSelectField->persist = false;
        $testSelectField->defaultValue = $selectSelectedTests;
        $form->addField($testSelectField);

        foreach ($form->fields as $field) {
            $field->readOnly = $somministrazioneStato != SomministrazioneStatus::WAITING;
        }

        $form->addField(new FormDateField("Da", "data_inizio", FormFieldType::DATE, true));
        $form->addField(new FormDateField("A", "data_fine", FormFieldType::DATE, true));

        $attachmentField = new FormAttachmentsField("Materiali/prodocolli", 
            AttachmentType::SOMMINISTRAZIONE_MATERIALI, EntityName::SOMMINISTRAZIONE, $this->id);
        $form->addField($attachmentField);

        if ($form->isSubmit() && $form->checkFields()){
            if(StringUtils::isBlank($this->id)){
                $this->id = $form->persist("", [
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser['user_id'],
                    "row_status" => RowStatus::READY
                ]);
            } else {
                $form->persist("somministrazione_id=" . $this->id);
            }
            
            EM::updateEntity("somministrazione", [
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser['user_id']
            ], ["somministrazione_id" => $this->id]);

            $tests = $testSelectField->getValueForDb();
            $testIds = [];
            foreach ($tests as $test) {
                $testId = $test;
                if (is_array($test)){
                    $testId = $test["value"];
                }

                $testIds[] = $testId;

                $st = SomministrazioneService::findSomministrazioneTestForSomministrazioneAndTest($this->id, $testId);
                if ($st != null){
                    continue;
                }

                $somministrazioneTestId = EM::insertEntity("somministrazione_test", [
                    "somministrazione_id" => $this->id,
                    "test_id" => $test["value"],
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser["user_id"],
                    "last_edit_date" => "NOW()",
                    "last_edit_user_id" => $this->currentUser["user_id"],
                    "row_status" => RowStatus::READY
                ]);

                $this->setDefaultSomministrazioneTestSoglia($somministrazioneTestId);
            }

            $testsNoiIn = "";
            $testsPars = [
                "somministrazione_id" => $this->id,
            ];
            foreach ($testIds as $key => $value) {
                $testsNoiIn .= $testsNoiIn == '' ? '' : ', ';
                $testsNoiIn .= ':test_id_' . $key;
                $testsPars[':test_id_' . $key] = $value;
            }
            EM::execQuery("DELETE FROM somministrazione_test 
                WHERE somministrazione_id=:somministrazione_id
                AND test_id NOT IN (" . $testsNoiIn . ")",
                $testsPars);

            $attachmentField->persist($this->id);

            redirectWithMessage($this->actionUrl("_default"), "Operazione eseguita");
        }

        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Edit</h3>
            </div>
        </div>

        <?= $form->draw(); ?>

        <script type="text/javascript">
        var tests = vueComponents["select2-somministrazione_test"].values;

        function updateTests(){
            var periodo = null;
            var classe = null;

            if (vueComponents["select2-periodo_dictionary_value"].selectedValue != null){
                periodo = vueComponents["select2-periodo_dictionary_value"].selectedValue.value;
            }

            if (vueComponents["select2-class_dictionary_value"].selectedValue != null){
                classe = vueComponents["select2-class_dictionary_value"].selectedValue.value;
            }

            var visibleTests = [];
            for (var i = 0; i < tests.length; i++){
                if (tests[i]["class_dictionary_value"] == classe && tests[i]["periodo_dictionary_value"] == periodo){
                    visibleTests.push(tests[i]);
                }
            }

            vueComponents["select2-somministrazione_test"].values = visibleTests;
        }

        vueComponents["select2-class_dictionary_value"].$on('input', updateTests);
        vueComponents["select2-periodo_dictionary_value"].$on('input', updateTests);
        updateTests();
        </script>
        <?php
        $this->footer();
    }

    private function setDefaultSomministrazioneTestSoglia($somministrazioneTestId){
        $somministrazioneTest = SomministrazioneService::findSomministrazioneTest($somministrazioneTestId);
        if ($somministrazioneTest == null){
            return;
        }

        $test = TestService::find($somministrazioneTest["test_id"]);
        if ($test == null){
            return;
        }

        $testClass = TestService::loadTest($test["code"]);

        if (!method_exists($testClass, "getVariablesNames")){
            return;
        }

        foreach ($testClass->getVariablesNames() as $variable) {
            $soglia = TestSogliaService::findInUseForTestAndVariableName($test["test_id"], $variable["name"]);
            if ($soglia == null){
                continue;
            }

            EM::insertEntity("somministrazione_test_soglia", [
                "somministrazione_test_id" => $somministrazioneTestId,
                "test_soglia_id" => $soglia["test_soglia_id"],
                "insert_date" => "NOW()",
                "insert_user_id" => $this->currentUser["user_id"],
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser["user_id"]
            ]);
        }
    }

    public function testParameters(){
        $somministrazioneTest = SomministrazioneService::findSomministrazioneTest($this->id);
        if ($somministrazioneTest == null){
            redirectWithMessage($this->actionUrl("_default"), "Dati non validi");
        }

        $test = TestService::find($somministrazioneTest["test_id"]);
        if ($test == null){
            redirectWithMessage($this->actionUrl("_default"), "Test non trovato");
        }

        $somministrazione = SomministrazioneService::find($somministrazioneTest["somministrazione_id"]);
        $somministrazioneStato = SomministrazioneService::getStatus($somministrazione);

        $testClass = TestService::loadTest($test["code"]);

        $form = new Form($this->actionUrl("testParameters", ["id" => $this->id]));
        $form->entity = [];
        $form->cancellAction = $this->actionUrl("_default");

        $attachmentFieldMateriali = new FormAttachmentsField("Materiali", 
            AttachmentType::SOMMINISTRAZIONE_TEST_MATERIALI, EntityName::SOMMINISTRAZIONE_TEST, $this->id);
        $form->addField($attachmentFieldMateriali);

        $this->addSoglieFields($testClass, $form, $somministrazioneTest, $somministrazioneStato);
        $this->addParametersFields($testClass, $form, $somministrazione, $somministrazioneTest, $somministrazioneStato);
        
        if ($form->isSubmit() && $form->checkFields()){
            $newEntity = $form->getFieldsValues();
            
            if ($somministrazioneStato != SomministrazioneStatus::CLOSED){
                $parameters = ReflectionUtils::getPropertyWithPrefix($newEntity, "p_");
                EM::updateEntity("somministrazione_test", [
                    "parameters" => json_encode($parameters)
                ], [
                    "somministrazione_test_id" => $this->id
                ]);
            }

            $variables = $testClass->getVariablesNames();
            $soglie = ReflectionUtils::getPropertyWithPrefix($newEntity, "s_");
            foreach($variables as $variable){
                $dbSoglia = SomministrazioneService::findSomministrazioneTestVariableSoglia($this->id, $variable["name"]);
                $newSoglia = null;
                if ($soglie != null && isset($soglie[$variable["name"]])){
                    $newSoglia = $soglie[$variable["name"]];
                }

                if ($dbSoglia != null && $dbSoglia["test_soglia_id"] == $newSoglia){
                    continue;
                }

                if ($dbSoglia != null){
                    EM::deleteEntity("somministrazione_test_soglia", [
                        "somministrazione_test_id" => $this->id,
                        "test_soglia_id" => $dbSoglia["test_soglia_id"]
                    ]);
                }
                
                if (StringUtils::isNotBlank($newSoglia)){
                    EM::insertEntity("somministrazione_test_soglia", [
                        "somministrazione_test_id" => $this->id,
                        "test_soglia_id" => $newSoglia,
                        "insert_date" => "NOW()",
                        "insert_user_id" => $this->currentUser["user_id"],
                        "last_edit_date" => "NOW()",
                        "last_edit_user_id" => $this->currentUser["user_id"]
                    ]);
                }

                SomministrazioneService::evaluateAllResultsFor($this->id, $variable);
            }
            
            $attachmentFieldMateriali->persist($this->id);

            redirectWithMessage($this->actionUrl("_default"), "Operazione eseguita");
        }

        $this->header();

        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Edit dati test - <?= _t($test["name"]) ?></h3>
            </div>
        </div>
        <?php
        $form->draw();

        $this->footer();
    }

    private function addSoglieFields($testClass, $form, $somministrazioneTest, $somministrazioneStato){
        if (!method_exists($testClass, "getVariablesNames")){
            return;
        }

        $variables = $testClass->getVariablesNames();
        if (count($variables) == 0){
            return;
        }

        ob_start();
        ?>
        <h4 style='margin-top:30px;'>Soglie di valutazione</h4>
        <p>
            <strong>Attenzione: </strong> associando una nuova soglia verranno rivalutitai tutti i risultati presenti nel sistema per questa somministrazione.
        </p>
        <?php
        $content = ob_get_clean();
        $form->addField(new FormHtmlField($content));
        
        foreach ($variables as $variable) {
            $fieldName = "s_" . $variable["name"];
            $form->entity[$fieldName] = "";

            $variableSoglia = SomministrazioneService::findSomministrazioneTestVariableSoglia($somministrazioneTest["somministrazione_test_id"], $variable["name"]);
            if ($variableSoglia != null){
                $form->entity[$fieldName] = $variableSoglia["test_soglia_id"];
            }
            
            $soglie = TestSogliaService::findForTestAndVariableName($somministrazioneTest["test_id"], $variable["name"]);
            $values = [];
            foreach ($soglie as $soglia) {
                $values[$soglia["test_soglia_id"]] = "In uso: " . $soglia["in_use"] . "; s1: " . $soglia["s1"]. "; s2: " . $soglia["s2"];
            }
            $form->addField(new FormSelect2Field($variable["label"], $fieldName, FormFieldType::INTEGER, $values, false));
        }
    }

    private function addParametersFields($testClass, $form, $somministrazione, $somministrazioneTest, $somministrazioneStato){
        if (StringUtils::isNotBlank($somministrazioneTest["parameters"])){
            $parameters = json_decode($somministrazioneTest["parameters"], true);
            ReflectionUtils::fillWithPrefix($parameters, "p_", $form->entity);
        }

        if (!method_exists($testClass, "adminFormFields")){
            return;
        }

        $testFields = $testClass->adminFormFields($somministrazione);
        if (count($testFields) == 0){
            return;
        }

        $form->addField(new FormHtmlField("<h4 style='margin-top:30px;'>Parametri del test</h4>"));
        
        for ($i = 0; $i < count($testFields); $i++) {
            $fieldName = "p_" . $testFields[$i]->name;
            $testFields[$i]->name = $fieldName;
            $testFields[$i]->readOnly = $somministrazioneStato == SomministrazioneStatus::CLOSED;
            if (!isset($form->entity[$fieldName])){
                $form->entity[$fieldName] = "";
            }
        }
        $form->fields = array_merge($form->fields, $testFields);
    }
}