<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AlfaTableAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class AlfaChildAdminAction extends AdminAction {

    public $id;

    public function _default(){
        $this->header();

        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Dati piattaforma Alfa</h3>
            </div>
            <div class="large-4 medium-4 columns">
            </div>
        </div>

        <div class="mb-alfa-child-admin-i"></div>
        <script>
        MbCreateVue('mb-alfa-child-admin', 'mb-alfa-child-admin-i', {
            "id": <?= _t($this->id) ?>
        })
        </script>
        <?php        
        
        $this->footer();
    }

    public function childData(){
        if ($this->id == null){
            $this->writeJson(null, false, "Codice studente non valido.");
            return;
        }

        $details = $this->findChildDetails($this->id);
        if ($details == null){
            $this->writeJson(null, false, "Dati studente non trovati.");
            return;
        }

        $examsStmt = $this->findExamForSchoolClassId($details["school_class_id"]);
        $exams = $examsStmt->fetchAll();

        $resultsStmt = $this->findExamResultsForChildId($this->id);
        $results = $resultsStmt->fetchAll();

        $examResults = [];
        for ($i = 0; $i < count($exams); $i++) {
            $examResults[$i] = [
                "exam" => $exams[$i],
                "result" => count($results) > $i ? $results[$i] : null
            ];
        }

        $this->writeJson([
            "details" => $details,
            "examResults" => $examResults
        ]);
    }

    public function writeJson($data, $success = true, $message = ''){
        $res = new ApiResponse($data);
        $res->success = $success;
        $res->message = $message;

        echo json_encode($res);
        die();
    }

    private function findChildDetails($id){
        return EMAlfa::execQuerySingleResult("SELECT 
                sg.name as sg_name,
                s.name as s_name,
                sc.id as school_class_id,
                sc.name,
                c.name, c.surname
            FROM child_child c
            left join school_evaluationyear sy on c.school_year_id=sy.id
            left join school_school s ON sy.school_id=s.id
            left join school_schoolgroup sg ON s.group_id=sg.id
            left join child_child_classes cc ON c.id=cc.child_id
            left join school_schoolclass sc ON cc.schoolclass_id=sc.id
            where c.id=:id", [
                "id" => $id
            ]);
    }

    private function findExamForSchoolClassId($schoolClassId){
        return EMAlfa::execQuery("SELECT teacher, examiner, exam_id, year, month, type, content
            from exam_primary_classexam ec
            left join exam_primary_exam e ON ec.exam_id=e.id
            where schoolclass_id=:school_class_id", [
                "school_class_id" => $schoolClassId
            ]);
    }

    private function findExamResultsForChildId($childId){
        return EMAlfa::execQuery("SELECT * 
            from exam_primary_testresult er
            left join exam_primary_exam e ON er.exam_id=e.id
            where child_id=:id", [
                "id" => $childId
            ]);
    }
}