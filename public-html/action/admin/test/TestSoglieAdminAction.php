<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of TestSoglieAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class TestSoglieAdminAction extends AdminAction{

    public $id;
    public $test_id;
    public $variable_name;
    private $test;
    
    public function _prepare(){
        parent::_prepare();

        $this->test = TestService::find($this->test_id);
        if ($this->test == null){
            redirectWithMessage(UriService::buildPageUrl("/admin/test/TestAction"), "Test non valido");
        }
    }

    public function _default(){
        $this->header();
        
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Soglie test: <?= _t($this->test["name"]) ?> - <?= _t($this->test["periodo_dictionary_value"]) ?></h3>
            </div>
            <div class="large-4 medium-4 columns">
                <a class="button radius float_right" href="<?= $this->actionUrl("edit", ["test_id" => $this->test_id]) ?>">
                    <i class="fa fa-plus-circle"></i>
                    Nuovo
                </a>
            </div>
        </div>

        <div class="mb-test-soglia-admin-table-1"></div>
        <script type="text/javascript">
        MbCreateVue('mb-test-soglia-admin-table', 'mb-test-soglia-admin-table-1', {
            "test_id": <?= $this->test_id ?>
        })
        </script>
        <?php        
        $this->footer();
    }

    public function edit(){
        $form = new Form($this->actionUrl("edit", ['test_id' => $this->test_id, 'id' => $this->id]));
        $form->entityName = "test_soglia";
        $form->cancellAction = $this->actionUrl("_default", ["test_id" => $this->test_id]);
        $form->entity = [];

        $test = TestService::loadTest($this->test["code"]);
        $variables = $test->getVariablesNames();
        $selectNames = [];
        foreach ($variables as $variable) {
            $selectNames[$variable["name"]] = $variable["label"];
        }
        
        $form->addField(new FormSelect2Field("Variabile", "variable_name", FormFieldType::STRING, $selectNames, true));
        $form->addField(new FormTextField("Soglia 1", "s1", FormFieldType::NUMBER));
        $form->addField(new FormTextField("Soglia 2", "s2", FormFieldType::NUMBER));
        
        if ($form->isSubmit() && $form->checkFields()){
            EM::updateEntity("test_soglia", [
                "in_use" => 0
            ], [
                "test_id" => $this->test_id,
                "variable_name" => $this->variable_name
            ]);

            $form->persist("", [
                "test_id" => $this->test_id,
                "insert_date" => "NOW()",
                "insert_user_id" => $this->currentUser['user_id'],
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser['user_id'],
                "in_use" => 1,
                "row_status" => RowStatus::READY
            ]);

            redirectWithMessage($this->actionUrl("_default", ["test_id" => $this->test_id]), "Operazione eseguita");
        }

        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Nuova soglia</h3>
            </div>
        </div>

        <div class="row-100">
            <div class="small-12 columns">    
                <h4>Regole di valutazione</h4>
            </div>

            <div class="small-6 columns">    
                <p>
                    <strong>Risposte corrette</strong><br/>
                    Soglia 1: 10* centile + 1<br/>
                    Soglia 2: 20* centile + 1<br/>
                    <br/>
                    ROSSO: p < Soglia 1<br/>
                    GIALLO: Soglia 1 <= p < Soglia 2<br/>
                    VERDE:  Soglia 2 <= p<br/>
                </p>
            </div>
            <div class="small-6 columns">
                <p>
                    <strong>Errori</strong><br/>
                    Soglia 1: 80* centile<br/>
                    Soglia 2: 90* centile<br/>
                    <br/>
                    ROSSO: p >= Soglia 2<br/>
                    GIALLO: Soglia 2 > p >= Soglia 1<br/>
                    VERDE:  Soglia 1 < p<br/>
                </p>
            </div>
        </div>

        <?= $form->draw(); ?>

        <?php
        $this->footer();
    }
}