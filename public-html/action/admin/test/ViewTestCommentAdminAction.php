<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ViewTestCommentAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class ViewTestCommentAdminAction extends AdminAction {

    public function _default(){
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Visualizzazione analisi prove</h3>
            </div>
        </div>

        <div class="mb-view-test-analyses-admin-page-container"></div>
        <script type="text/javascript">
            MbCreateVue('mb-view-test-analyses-admin-page', 'mb-view-test-analyses-admin-page-container');
        </script>
        <?php        
        $this->footer();
    }

}