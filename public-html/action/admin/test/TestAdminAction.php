<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of TestAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class TestAdminAction extends AdminAction{

    public $id;

    public function _default(){
        $this->header();
        
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Test</h3>
            </div>
            <div class="large-4 medium-4 columns">
                <a class="button radius float_right" href="<?= $this->actionUrl("edit") ?>">
                    <i class="fa fa-plus-circle"></i>
                    Nuovo
                </a>
            </div>
        </div>

        <div class="mb-test-admin-table"></div>
        <?php      
          
        $this->footer();
    }

    public function edit(){
        $form = new Form($this->actionUrl("edit", ['id' => $this->id]));
        $form->entityName = "test";
        $form->cancellAction = $this->actionUrl("_default");
        $form->entity = TestService::find($this->id);

        $codeField = new FormTextField("Codice", "code", FormFieldType::STRING, true);
        $codeField->checkFunction = function($field){
            $test = TestService::loadTest($field->getValue(null));
            if ($test == null){
                $field->addFieldError("Codice test non trovato tra i test disponibili");
            }
        };
        $form->addField($codeField);
        $form->addField(new FormTextField("Titolo", "name", FormFieldType::STRING, true));

        $classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $form->addField(new FormSelect2Field("Classe", "class_dictionary_value", FormFieldType::STRING, $classes, true));

        $periodo = DictionaryService::findGroupAsDictionary(Dictionary::TEST_PERIODO);
        $form->addField(new FormSelect2Field("Periodo", "periodo_dictionary_value", FormFieldType::STRING, $periodo, true));

        $noteCompilazione = new FormTextareaField("Note compilazione", "note_compilazione", FormFieldType::STRING);
        $noteCompilazione->isWysiwyg = true;
        $form->addField($noteCompilazione);

        $attachmentFieldIstruzioni = new FormAttachmentsField("Istruzioni", 
            AttachmentType::TEST_ISTRUZIONI, EntityName::TEST, $this->id);
        $form->addField($attachmentFieldIstruzioni);

        $attachmentFieldMateriali = new FormAttachmentsField("Materiali", 
            AttachmentType::TEST_MATERIALI, EntityName::TEST, $this->id);
        $form->addField($attachmentFieldMateriali);

        if ($form->isSubmit() && $form->checkFields()){
            if(StringUtils::isBlank($this->id)){
                $this->id = $form->persist("", [
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser['user_id'],
                    "row_status" => RowStatus::READY
                ]);
            } else {
                $form->persist("test_id=" . $this->id);
            }
            
            EM::updateEntity("test", [
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser['user_id']
            ], ["test_id" => $this->id]);

            $attachmentFieldIstruzioni->persist($this->id);
            $attachmentFieldMateriali->persist($this->id);

            redirectWithMessage($this->actionUrl("_default"), "Operazione eseguita");
        }

        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Edit test</h3>
            </div>
        </div>

        <?= $form->draw(); ?>

        <?php
        $this->footer();
    }

    public function remove(){
        TestService::remove($this->id);
        redirectWithMessage($this->actionUrl("_default"), "Operazione eseguita");
    }
}