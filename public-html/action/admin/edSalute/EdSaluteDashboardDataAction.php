<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EdSaluteDashboardDataAction extends AdminAction{
    
    public $filter_school_year_id;
    public $filter_structure_id;
    public $filter_ed_salute_type;
    public $filter_school_grade;
    
    public function _default(){
        $table = new Table();
        $table->allowExportXLS = true;
        
        $query = "SELECT plesso_structure_id, max(plesso_num_alunni) as spna, sum(num_classi) as snc, sum(num_alunni) as sna, sum(num_ore_annuali) as snoa
            FROM ed_salute es
            left join structure s on es.structure_id=s.structure_id
            where es.row_status=:row_status_ready ";
        
        $par = [];
        $par['row_status_ready'] = RowStatus::READY;
        
        $currentSchoolYear = SchoolYearService::findCurrentYear();
        if (StringUtils::isBlank($this->filter_school_year_id)){
            $this->filter_school_year_id = $currentSchoolYear['school_year_id'];
        }

        $query .= "AND school_year_id=:school_year_id ";
        $par["school_year_id"] = $this->filter_school_year_id;
        
        if (StringUtils::isNotBlank($this->filter_structure_id)){
            $query .= "AND s.structure_id=:structure_id ";
            $par["structure_id"] = $this->filter_structure_id;
        }

        if (StringUtils::isNotBlank($this->filter_ed_salute_type)){
            $query .= "AND ed_salute_type=:ed_salute_type ";
            $par["ed_salute_type"] = $this->filter_ed_salute_type;
        }
        
        if (StringUtils::isNotBlank($this->filter_school_grade)){
            $query .= "AND school_grade_dictionary_id=:school_grade ";
            $par["school_grade"] = $this->filter_school_grade;
        }
        
        $query .= "group by plesso_structure_id";
        
        $results = EM::execQuery($query, $par);
        $table->data = $results;
        $table->addColumn(new TableColumn("plesso_structure_id", "Istituto", function($v){
            return StructureService::printFullStructureName($v);
        }));
        $table->addColumn(new TableColumn("spna", "Alunni nel plesso"));
        $table->addColumn(new TableColumn("snc", "Classi"));
        $table->addColumn(new TableColumn("sna", "Alunni"));
        $table->addColumn(new TableColumn("snoa", "Ore"));
        
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Educazione alla salute: Riepilogo</h3>
            </div>    
        </div>
        <div class="table-row row-100">
            <div class="large-12 columns">
                <div class="filters">
                    <form method="POST" action="<?= $this->actionUrl() ?>">
                        <div class="row-100 margin-bottom">
                            <div class="columns small-12 medium-6 large-4">
                                <?php
                                    $schoolYears = SchoolYearService::findAllAsObject();
                                ?>
                                <?= FilterUtils::drawSelectFilter("Anno scolastico", "filter_school_year_id", $schoolYears, $this->filter_school_year_id) ?>
                            </div>
                            <div class="columns small-12 medium-6 large-4">
                                <?php $structures = StructureService::findAllRootAsDictionary(true); ?>
                                <?= FilterUtils::drawSelectFilter("Istituto", "filter_structure_id", $structures) ?>
                            </div>
                            <div class="columns small-12 medium-6 large-4">
                                <?php
                                $types = array_merge(["" => ""], DictionaryService::findGroupAsDictionary(Dictionary::ED_SALUTE_TYPE));
                                ?>
                                <?= FilterUtils::drawSelectFilter("Ambito", "filter_ed_salute_type", $types) ?>
                            </div>
                        </div>
                        <div class="row-100 margin-bottom">
                            <div class="columns small-12 medium-6 large-4">
                                <?php 
                                $grades = array_merge(["" => ""], DictionaryService::findGroupAsDictionary(Dictionary::SCHOOL_GRADE));
                                ?>
                                <?= FilterUtils::drawSelectFilter("Grado scolastico", "filter_school_grade", $grades) ?>
                            </div>
                        </div>
                        
                        <?= FilterUtils::drawSubmit() ?>
                    </form>          
                </div>
            </div>
            <div class="large-12 columns">
                <?php
                $table->draw();
                ?>
            </div>
        </div>
        <?php
        
        $this->footer();
    }
    
}
