<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EdSaluteAllDataAction extends AdminAction{
    
    public function _default(){
        $table = new Table();
        $table->allowExportXLS = true;
        $table->entityName = "ed_salute";
        
        $table->addWhere("row_status<>'" . RowStatus::DELETED . "'");
        $table->addColumn(new TableColumn("plesso_structure_id", "Istituto", function($v){
            return StructureService::printFullStructureName($v);
        }));
        $table->addColumn(new TableColumn("plesso_num_alunni", "Alunni"));
        $table->addColumn(new TableColumn("title", "Titolo"));
        
        $types = DictionaryService::findGroupAsDictionary(Dictionary::ED_SALUTE_TYPE);
        $table->addColumn(new TableColumn("ed_salute_type", "Ambito", function($v) use ($types){
            return isset($types[$v]) ?$types[$v] : '';
        }));
        
        $grades = DictionaryService::findGroupAsDictionary(Dictionary::SCHOOL_GRADE);
        $table->addColumn(new TableColumn("school_grade_dictionary_id", "Grado", function($v) use ($grades){
            return isset($grades[$v]) ?$grades[$v] : '';
        }));
        
        if ($table->isExportingExcel){
            $table->addColumn(new TableColumn("proposto_da", "Proposto da", function($v){
                $dictionary = DictionaryService::findByKey($v);
                if ($dictionary == null){
                    return "";
                }
                return $dictionary['value'];
            }));
        }
        
        $table->addColumn(new TableColumn("num_classi", "Classi"));
        $table->addColumn(new TableColumn("num_alunni", "Alunni"));
        $table->addColumn(new TableColumn("num_ore_annuali", "Ore"));
        $table->addColumn(new TableColumn("insert_date", "Data inserimento", function($v){
            return TextService::formatDate($v);
        }));

        if ($table->isExportingExcel){
            $table->addColumn(new TableColumn("insert_user_id", "Utente inserimento", function($v){
                $user = UserService::find($v);
                if ($user == null){
                    return "";
                }
                return $user['name'] . ' ' . $user['surname'];
            }));
        }
        
        $table->addColumn(new TableColumn("last_edit_date", "Data modifica", function($v){
            return TextService::formatDate($v);
        }));
        
        if ($table->isExportingExcel){
            $table->addColumn(new TableColumn("last_edit_user_id", "Utente modifica", function($v){
                $user = UserService::find($v);
                if ($user == null){
                    return "";
                }
                return $user['name'] . ' ' . $user['surname'];
            }));
            $table->addColumn(new TableColumn("costi_carico_scuola", "Costi carico scuola"));
            $table->addColumn(new TableColumn("progetto_anni_precedenti", "Presente in anni precedenti"));
            $table->addColumn(new TableColumn("note", "Note"));
        }
        
        $this->header();
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Educazione alla salute: Progetti</h3>
            </div>    
        </div>
        <div class="table-row row-100">
            <div class="large-12 columns">
                <div class="filters">
                    <form method="GET" action="<?= $this->actionUrl() ?>">
                        <div class="row-100 margin-bottom">
                            <div class="columns small-12 medium-6 large-4">
                                <?php
                                    $schoolYears = SchoolYearService::findAllAsObject();
                                    $currentSchoolYear = SchoolYearService::findCurrentYear();
                                ?>
                                <?= FilterUtils::drawSelectFilter("Anno scolastico", "filter_school_year_id", $schoolYears, $currentSchoolYear["school_year_id"]) ?>
                            </div>
                            <div class="columns small-12 medium-6 large-4">
                                <?php $structures = StructureService::findAllRootAsDictionary(true); ?>
                                <?= FilterUtils::drawSelectFilter("Istituto", "filter_structure_id", $structures) ?>
                            </div>
                            <div class="columns small-12 medium-6 large-4 float-right">
                                <?= FilterUtils::drawTextFilter("Titolo", "filter_title") ?>
                            </div>
                        </div>

                        <?= FilterUtils::drawSubmit() ?>
                    </form>          
                </div>
                <?php
                $table->addFilter(new TableFilter("school_year_id", "=", "", $currentSchoolYear["school_year_id"]));
                $table->addFilter(new TableFilter("structure_id"));
                $table->addFilter(new TableFilterCustom("title", function($v){ return "title like '%$v%'"; } ));
                ?>
            </div>
            <div class="large-12 columns">
                <?php
                $table->draw();
                ?>
            </div>
        </div>
        <?php
        
        $this->footer();
    }
    
}
