<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EncriptAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ImportStatesAction extends AdminAction{

    public function _default(){
        $file_lines = file(__DIR__ . '/../../../files/elenco_nazioni.csv');
        foreach ($file_lines as $index => $line) {
            if ($index == 0){
                continue;
            }
            echo $line . "<br/>>";
            $arr = explode(",", $line);
            $nazione = EM::execQuerySingleResult("SELECT * FROM nazione WHERE nazione_id=:id", ["id" => $arr[0]]);
            if ($nazione != null){
                continue;
            }

            $nazioniUw = ["austria", "lettonia", "belgio", "lituania", "bulgaria", "lussemburgo", "cechia", "malta", "cipro", "paesi bassi", 
                "croazia", "polonia", "danimarca", "portogallo", "estonia", "regno unito", "finlandia", "romaia", "francia", "slovacchia",
                "germania", "slovenia", "grecia", "spagna", "irlanda", "svezia", "italia", "ungheria"];

            $isUe = false;
            if (array_search(strtolower($arr[1]), $nazioniUw)){
                $isUe = true;
            }

            EM::insertEntity("nazione", [
                "nazione_id" => $arr[0],
                "nome" => $arr[1],
                "sigla" => $arr[2],
                "sigla_estesa" => $arr[3],
                "is_ue" => $isUe
            ]);
        }
    }
}