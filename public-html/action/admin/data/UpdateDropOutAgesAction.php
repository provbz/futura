<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EncriptAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UpdateDropOutAgesAction extends AdminAction{
    public function _default(){
        $dos = DocumentService::findAllByType(DocumentType::DROP_OUT);
        foreach($dos as $d){
            $this->updateAges($d);
        }
    }
    

    private function updateAges($document){
        $metas = MetadataService::findMetadataValue(MetadataEntity::USER, $document["user_id"], UserMetadata::BIRTH_DATE);
        if (count($metas) == 0){
            return;
        }

        $birthDateMeta = $metas[0];
        $birthDateStr = EncryptService::decrypt($birthDateMeta["value"]);
        if (StringUtils::isBlank($birthDateStr)){
            return "";
        }
        
        $birthDate = new DateTime($birthDateStr);
        $now = new DateTime();
        $diff = $now->diff($birthDate);
        $age = $diff->y;
        EM::updateEntity("user_year_document", ["drop_out_age" => $age], ["user_year_document_id" => $document["user_year_document_id"]]);
    }
}