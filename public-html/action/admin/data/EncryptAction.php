<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EncriptAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EncryptAction extends AdminAction{
    
    public $clearText;
    public $encodedText;
    
    public function _default(){
        $this->header();
        
        ?>
        <div class="section-title row-100">
            <div class="large-6 medium-4 columns">
                <h3>Test di codifica dati</h3>
            </div>
        </div>

        <a href="<?= $this->actionUrl('encrypt') ?>">Codifica dati</a><br/>
        
        <form action="<?= $this->actionUrl() ?>">
            <textarea name="clearText"></textarea>
            <textarea name="encodedText"></textarea>
            <input type="submit"/>
        </form>
        <?php
        
        if (StringUtils::isNotBlank($this->clearText)){
            echo  "Cifrato: <pre>" . EncryptService::encrypt($this->clearText) . "</pre><br/>";
        }
        
        if (StringUtils::isNotBlank($this->encodedText)){
            echo  "Decifrato: ". EncryptService::decrypt($this->encodedText)  . "\n";
        }
        
        $this->footer();
    }
    
    public function encrypt(){
        $this->encryptUserMetadata();
        $this->encryptPeiRowTarget();
    }
    
    private function encryptPeiRowTarget(){
        $stmt = EM::prepare("SELECT * FROM pei_row_target");
        $stmt->execute();
        foreach ($stmt as $row){
            $decode = EncryptService::decrypt($row['target']);
            if (StringUtils::isBlank($decode)){
                EM::updateEntity("pei_row_target",[
                    'target' => EncryptService::encrypt($row['target']),
                    'activity' => EncryptService::encrypt($row['activity']),
                    'note' => EncryptService::encrypt($row['note'])
                ], sprintf('pei_row_target_id=%d', $row['pei_row_target_id']));
            }
        }
    }
    
    private function encryptUserMetadata(){
        $stmtUserMetadata = EM::prepare("SELECT * FROM user_metadata");
        $stmtUserMetadata->execute();
        foreach ($stmtUserMetadata as $row){
            $decode = EncryptService::decrypt($row['value']);
            if (StringUtils::isBlank($decode)){
                EM::updateEntity("user_metadata",[
                    'value' => EncryptService::encrypt($row['value'])
                ], sprintf('user_metadata_id=%d', $row['user_metadata_id']));
            }
        }
    }
}
