<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of RilevazioneCattedreAdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class RilevazioneCattedreAdminAction extends AdminAction {

    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());
        
        $this->header();
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row-100">
                    <div class="large-8 medium-7 small-7 columns">
                        <h3>Rilevazione dati finalizzata alla distribuzione delle cattedre A023ter</h3>
                    </div>
                </div>
                
                <div class="mb-admin-rilevazione-cattedre-table-container"></div>
                <script>
                    MbCreateVue('MbAdminRilevazioneCattedreTable', 'mb-admin-rilevazione-cattedre-table-container', <?= json_encode([]) ?>);
                </script>
            </div>
        </div>
        <?php
        $this->footer();
    }

}