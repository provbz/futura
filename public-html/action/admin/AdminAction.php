<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class AdminAction extends Page {

    public $structure;
    
    public function _prepare() {
        $_SESSION[GlobalSessionData::SELECTED_STRUCTURE_ID] = "";
        parent::_prepare();
    }

    protected function prepareMenu(){
        $baseAction = $this->getBaseAction();
        
        $adminMenuItem = new MenuItem("Amministrazione", UriService::buildPageUrl($baseAction));
        $this->menu->addMenuItem($adminMenuItem);

        $menuItem = new MenuItem("Messaggi interni");
        $adminMenuItem->addMenuItem($menuItem);
        $menuItem->addMenuItem(new MenuItem("Elenco", UriService::buildPageUrl('/admin/internal_message/InternalMessageAdminAcion')));

        $testEnabled = PropertyService::find(PropertyNamespace::TEST_DIDATTICI, PropertyKey::ENABLED);
        if ($testEnabled['value']){
            $menuItem = new MenuItem("Mondo delle Parole e Progetto Letto-Scrittura");
            $adminMenuItem->addMenuItem($menuItem);
            $menuItem->addMenuItem(new MenuItem("Test", UriService::buildPageUrl('/admin/test/TestAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Prove", UriService::buildPageUrl('/admin/test/SomministrazioniAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Visualizzazione analisi prove", UriService::buildPageUrl('/admin/test/ViewTestCommentAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Importa dati studenti", UriService::buildPageUrl('/admin/test/ImportStudentsDataAction')));
            $menuItem->addMenuItem(new MenuItem("Elenco studenti", UriService::buildPageUrl('/admin/test/ElencoStudentiAction')));
            $menuItem->addMenuItem(new MenuItem("Alfa", UriService::buildPageUrl('/admin/test/AlfaTableAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Esis", UriService::buildPageUrl('/admin/test/EsisTableAdminAction')));
        }

        $portgolioEnabled = PropertyService::find(PropertyNamespace::PORTFOLIO_INSEGNANTI, PropertyKey::ENABLED);
        if ($portgolioEnabled['value']){
            $menuItem = new MenuItem("Portfolio docenti");
            $adminMenuItem->addMenuItem($menuItem);
            $menuItem->addMenuItem(new MenuItem("Riepilogo", UriService::buildPageUrl('/admin/portfolio/PortfolioAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Materiali", UriService::buildPageUrl('/admin/portfolio/PortfolioMaterialiAdminAction')));
        }

        $menuItem = new MenuItem("Patto inclusione");
        $adminMenuItem->addMenuItem($menuItem);
        $menuItem->addMenuItem(new MenuItem("File", UriService::buildPageUrl('/admin/patto_inclusione/PattoInclusioneAdminAction')));
        
        $rilevazioneCattedreEnabled = PropertyService::find(PropertyNamespace::RILEVAZIONE_CATTEDRE, PropertyKey::ENABLED);
        if ($rilevazioneCattedreEnabled && $rilevazioneCattedreEnabled['value']){
            $menuItem = new MenuItem("Rilevazione cattedre");
            $adminMenuItem->addMenuItem($menuItem);
            $menuItem->addMenuItem(new MenuItem("Dati", UriService::buildPageUrl('/admin/rilevazione_cattedre/RilevazioneCattedreAdminAction')));
        }

        $educazioneSaluteEnabled = PropertyService::find(PropertyNamespace::EDUCAZIONE_SALUTE, PropertyKey::ENABLED);
        if ($educazioneSaluteEnabled['value']){
            $menuItem = new MenuItem("Ed Salute");
            $adminMenuItem->addMenuItem($menuItem);
            $menuItem->addMenuItem(new MenuItem("Dati", UriService::buildPageUrl('/admin/edSalute/EdSaluteAllDataAction')));
            $menuItem->addMenuItem(new MenuItem("Riepilogo", UriService::buildPageUrl('/admin/edSalute/EdSaluteDashboardDataAction')));
        }
        
        $eModelEnabled = PropertyService::find(PropertyNamespace::MODELLO_E, PropertyKey::ENABLED);
        if ($eModelEnabled['value']){
            $menuItem = new MenuItem("Modello E");
            $adminMenuItem->addMenuItem($menuItem);
            $menuItem->addMenuItem(new MenuItem("Da verificare", UriService::buildPageUrl('/admin/eModel/EmodelEditedAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Verificati", UriService::buildPageUrl('/admin/eModel/EmodelAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Chiusura documenti", UriService::buildPageUrl('/admin/eModel/EModelCloseAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Riepilogo diagnosi", UriService::buildPageUrl('/admin/eModel/EModelDiagnosyAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Riepilogo codici", UriService::buildPageUrl('/admin/eModel/EModelCodesAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Gestione codici", UriService::buildPageUrl('/admin/eModel/EmodelManageCodesAdminAction')));
        }
        
        $dropOutEnabled = PropertyService::find(PropertyNamespace::DROP_OUT, PropertyKey::ENABLED);
        if ($dropOutEnabled['value']){
            $menuItem = new MenuItem("Drop out");
            $adminMenuItem->addMenuItem($menuItem);
            $menuItem->addMenuItem(new MenuItem("Dati", UriService::buildPageUrl('/admin/dropOut/DropOutAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Analisi dati", UriService::buildPageUrl('/admin/dropOut/DropOutDashboardAdminAction')));
            $menuItem->addMenuItem(new MenuItem("Referenti", UriService::buildPageUrl('/admin/dropOut/DropOutAdminReferentiAction')));
        }

        $menuItem = new MenuItem("Servizio valutazione");
        $adminMenuItem->addMenuItem($menuItem);
        $menuItem->addMenuItem(new MenuItem("Accedi", UriService::buildPageUrl('/admin/servizio_valutazione/ServizioValutazioneAdminAction')));

        $menuItem = new MenuItem("PEI-PDP");
        $adminMenuItem->addMenuItem($menuItem);
        $menuItem->addMenuItem(new MenuItem("Studenti", UriService::buildPageUrl('/admin/pei/PeiAction')));
        $menuItem->addMenuItem(new MenuItem("Recupero da anni precedenti", UriService::buildPageUrl('/admin/pei/PeiRecuperaTableAction')));

        $menuItem = new MenuItem("Utenti");
        $adminMenuItem->addMenuItem($menuItem);
        $menuItem->addMenuItem(new MenuItem("Strutture", UriService::buildPageUrl('/admin/user/StructureAdminAction')));
        $menuItem->addMenuItem(new MenuItem("Richieste trasferimenti", UriService::buildPageUrl('/admin/user/UserTransferRequestAdminAction')));
        $menuItem->addMenuItem(new MenuItem("Gestione utenti", UriService::buildPageUrl('/admin/user/UserAction')));
        $menuItem->addMenuItem(new MenuItem("Gestione utenti IC", UriService::buildPageUrl('/admin/user/UserICAction')));
        $menuItem->addMenuItem(new MenuItem("Import insegnanti", UriService::buildPageUrl('/admin/user/UserImportAdminAction')));
        $menuItem->addMenuItem(new MenuItem("Ruoli", UriService::buildPageUrl('/admin/user/RoleAction')));
        $menuItem->addMenuItem(new MenuItem("Accessi", UriService::buildPageUrl('/admin/user/UserAccessAction')));
        $menuItem->addMenuItem(new MenuItem("Utenti online", UriService::buildPageUrl('/admin/user/UserOnlineAction')));
        
        $menuItem = new MenuItem("Messaggi");
        $adminMenuItem->addMenuItem($menuItem);
        $menuItem->addMenuItem(new MenuItem("Notice", UriService::buildPageUrl('/admin/technical/NoticeAction')));
        $menuItem->addMenuItem(new MenuItem("Messaggi", UriService::buildPageUrl('/admin/technical/MessageAction')));
        $menuItem->addMenuItem(new MenuItem("Mail test", UriService::buildPageUrl('/admin/technical/MailTestAction')));

        $menuItem = new MenuItem("Tecnica");
        $adminMenuItem->addMenuItem($menuItem);
        $menuItem->addMenuItem(new MenuItem("Anni scolastici", UriService::buildPageUrl('/admin/technical/SchoolYearAdminAction')));
        $menuItem->addMenuItem(new MenuItem("Property", UriService::buildPageUrl('/admin/technical/PropertyAction')));
        $menuItem->addMenuItem(new MenuItem("Dictionary", UriService::buildPageUrl('/admin/technical/DictionaryAction')));
        $menuItem->addMenuItem(new MenuItem("Scheduler", UriService::buildPageUrl('/admin/technical/SchedulerAdminAction')));
        $menuItem->addMenuItem(new MenuItem("Log", UriService::buildPageUrl('/admin/technical/LogAction')));
        $menuItem->addMenuItem(new MenuItem("File check", UriService::buildPageUrl('/admin/technical/FileCheckAdminAction')));
        $menuItem->addMenuItem(new MenuItem("IP bannati", UriService::buildPageUrl('/admin/technical/BannedIpAction')));
        $menuItem->addMenuItem(new MenuItem("Data Encrypt", UriService::buildPageUrl('/admin/data/EncryptAction')));
    }

    public function header($displayPageHeader = true, $angularApp = NULL){
        $this->menu->setSelectedMenuItem("/admin/user/UserAction");
        
        parent::header($displayPageHeader, $angularApp);
        ?>
        <div class="content active">
            <div class="row-100">
                <div class="small-3 large-2 columns">
                    <?= $this->menu->drawSubmenu("Amministrazione") ?>
                </div>
                <div class="small-9 large-10 columns">
        <?php
    }
    
    public function footer($displayFooter = true){
            echo '</div></div>';
        echo '</div>';
        parent::footer();
    }    
}
