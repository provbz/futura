<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * Description of QueryAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class JsonToXlsxAction extends Page{

    public $request;
    public $structure;

    public function _prepare(){
        $this->request = json_decode(file_get_contents('php://input'));
    }

    public function _default(){
        if ($this->currentUser == null){
            redirect(UriService::accessDanyActionUrl());
        }

        $objPHPExcel = new Spreadsheet();
        $objPHPExcel->getProperties()->setCreator(Constants::$APP_PRINT_NAME)
                    ->setLastModifiedBy(Constants::$APP_PRINT_NAME);

        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        if ($this->request != null){
            foreach($this->request->rows as $row){
                foreach ($row->columns as $column){
                    if (property_exists($column, 'value')){
                        $sheet->setCellValueByColumnAndRow($column->index, $row->index, $column->value);        
                    }
                    
                    if (property_exists($row, 'isHeader') && $row->isHeader){
                        $sheet->getColumnDimensionByColumn($column->index)->setAutoSize(true);
                        $sheet->getStyleByColumnAndRow($column->index, $row->index)->getFont()->setBold(true);   
                    } else {
                        $sheet->getStyleByColumnAndRow($column->index, $row->index)->getAlignment()->setWrapText(true);
                    }
                }
            }
        }
        
        $objPHPExcel->setActiveSheetIndex(0);
        
        header_remove();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="export.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, 'Xls');
        $objWriter->save('php://output');
        exit();
    }

}