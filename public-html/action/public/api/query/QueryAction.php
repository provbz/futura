<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require_once __DIR__ ."/../../../../template/query/IQuery.php";
require_once __DIR__ ."/../../../../template/query/QueryBase.php";
require_once __DIR__ ."/../../../../template/query/QueryType.php";
require_once __DIR__ ."/QueryResponse.php";

class QueryAction {
    
    public $request;
    public $structure;

    public function _prepare(){
        $this->request = json_decode(file_get_contents('php://input'));
    }

    public function descriptor(){
        $queryClass = $this->loadQuery($this->request);
        if ($queryClass == null){
            echo "Query descriptor not found";
            return;
        }

        header('Content-Type: application/json');
        echo json_encode($queryClass->descriptor());
        die();
    }
    
    public function find(){
        global $currentUser;

        $queryClass = $this->loadQuery($this->request);
        $response = new QueryResponse();

        if ($queryClass == null){
            $response->info->errors = ["Query non valida"];
            $response->info->hasErrors = true;
            $this->printResponse($response);
            return;
        }

        $queryClass->descriptor = $queryClass->descriptor();
        $queryClass->currentUser = $currentUser;

        $countQuery = "";
        $selectQuery = "";
        
        try{
            if ($this->request->paginator == QueryPaginatorType::PAGINATOR){
                $countQuery = $queryClass->buildQuery(QueryType::COUNT);
                $countResult = 0;
                if (!isset($queryClass->descriptor["dataSource"]) || $queryClass->descriptor["dataSource"] == DataSource::FUTURA){
                    $countResult = EM::execQuerySingleResult($countQuery, $queryClass->params);
                }
                else if ($queryClass->descriptor["dataSource"] == DataSource::ALFA){
                    $countResult = EMAlfa::execQuerySingleResult($countQuery, $queryClass->params);
                }
                else if ($queryClass->descriptor["dataSource"] == DataSource::ESIS){
                    $countResult = EMEsis::execQuerySingleResult($countQuery, $queryClass->params);
                } 

                $response->info->availlableResults = $countResult["COUNT(*)"];
            }

            $selectQuery = $queryClass->buildQuery(QueryType::DATA);
            if ($this->request->paginator == QueryPaginatorType::PAGINATOR){
                $selectQuery .= " LIMIT :first_result, :num_results"; 
                $queryClass->params[":first_result"] = $this->request->firstResult;
                $queryClass->params[":num_results"] = $this->request->numResults;
            }

            $selectedRows = null;
            if (!isset($queryClass->descriptor["dataSource"]) || $queryClass->descriptor["dataSource"] == DataSource::FUTURA){
                $selectResults = EM::execQuery($selectQuery, $queryClass->params);
            }
            else if ($queryClass->descriptor["dataSource"] == DataSource::ALFA){
                $selectResults = EMAlfa::execQuery($selectQuery, $queryClass->params);
            }
            else if ($queryClass->descriptor["dataSource"] == DataSource::ESIS){
                $selectResults = EMEsis::execQuery($selectQuery, $queryClass->params);
            }   

            $response->results = $selectResults->fetchAll();

            $this->expandEntities($queryClass, $response);

            if(method_exists($queryClass, "updateComputedColumns")){
                $response->results = $queryClass->updateComputedColumns($response->results);
            }
        } catch (PDOException $e){
            $response->info->hasErrors = true;
            $response->info->errors[] = $selectQuery;
            $response->info->errors[] = $countQuery;
            $response->info->errors[] = print_r($queryClass->params, true);
            $response->info->errors[] = print_r($e, true);
        } catch (Exception $ex){
            $response->info->hasErrors = true;
            $response->info->errors[] = $ex->getMessage();
        }

        $this->printResponse($response);
    }

    private function expandEntities($queryClass, $response){
        $entityFields = $this->findEntityFields($queryClass);
        
        foreach ($response->results as $row) {
            foreach($entityFields as $key => $field){
                if (!isset($row[$key]) || $row[$key] == null){
                    continue;
                }
                
                if (array_search($row[$key], $entityFields[$key]['ids']) !== false){
                    continue;
                }
                $entityFields[$key]['ids'][] = $row[$key];
            }
        }

        foreach($entityFields as $key => $field){
            if (count($field["ids"]) > 0){
                if ($field["entity"]["type"] == EntityName::DICTIONARY){
                    $res = DictionaryService::findByGroupAndKeys($field["entity"]["group"], $field["ids"]);
                    foreach($res as $dRow){
                        $entityFields[$key]["values"][$dRow["key"]] = $dRow;
                    }
                }
            }
        }
        
        foreach ($response->results as $rowKey => $row) {
            foreach($entityFields as $key => $field){
                if (!isset($row[$key]) || $row[$key] == null){
                    continue;
                }
                
                if (isset($field["values"][$row[$key]])){
                    $response->results[$rowKey][$key] = $field["values"][$row[$key]];
                }
            }
        }
    }

    private function findEntityFields($queryClass){
        $results = [];

        foreach ($queryClass->descriptor["fields"] as $key => $field){
            if (!isset($field["entity"])){
                continue;
            }

            $field["ids"] = [];
            $field["values"] = [];
            $results[$key] = $field;
        }

        return $results;
    }

    public function export(){
        $this->request->firstResult = 0;
        $this->request->numResults = 10000;

        $queryClass = $this->loadQuery($this->request);
        $response = new QueryResponse();

        if ($queryClass == null){
            $response->info->errors = ["No query found"];
            $response->info->hasErrors = true;
            $this->printResponse($response);
            return;
        }

        $selectQuery = "";
        try{
            $this->request->fields = $this->request->excelFields;
            $queryClass->descriptor = $queryClass->descriptor();
            $queryClass->descriptor["fields"] = $queryClass->descriptor["excelFields"];

            $selectQuery = $queryClass->buildQuery(QueryType::DATA);
            $selectResults = EM::execQuery($selectQuery, $queryClass->params);
            $results = $selectResults->fetchAll();
            if (method_exists($queryClass, 'updateComputedColumns')){
                $results = $queryClass->updateComputedColumns($results, true);
            }

            $objPHPExcel = new Spreadsheet();
            $objPHPExcel->getProperties()->setCreator(Constants::$APP_PRINT_NAME)
                        ->setLastModifiedBy(Constants::$APP_PRINT_NAME);

            $sheet = $objPHPExcel->setActiveSheetIndex(0);

            $col = 1;
            foreach ($this->request->fields as $fieldName){
                if (!isset($queryClass->descriptor["fields"][$fieldName])){
                    continue;
                }

                $field = $queryClass->descriptor["fields"][$fieldName];
                
                $sheet->getColumnDimensionByColumn($col)->setAutoSize(true);
                $sheet->getStyleByColumnAndRow($col, 1)->getFont()->setBold(true);
                if (isset($field["label"])){
                    $sheet->setCellValueByColumnAndRow($col, 1, $field["label"]);
                }

                $col++;
            }
            
            $rowCount = 2;
            foreach($results as $row){
                $col = 1;
                foreach ($this->request->fields as $fieldName){
                    if (!isset($queryClass->descriptor["fields"][$fieldName])){
                        continue;
                    }
    
                    $field = $queryClass->descriptor["fields"][$fieldName];
                    
                    if (isset($row[$fieldName])){
                        $value = $row[$fieldName];
                        if (is_array( $value)){
                            $value = json_encode($value);
                        }

                        $sheet->setCellValueByColumnAndRow($col, $rowCount, $value);        
                        $sheet->getStyleByColumnAndRow($col, $rowCount)->getAlignment()->setWrapText(true);
                    }
                    $col++;
                }
                
                $rowCount++;
            }

            $objPHPExcel->setActiveSheetIndex(0);
            
            header_remove();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="export.xls"');
            header('Cache-Control: max-age=0');

            $objWriter = PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, 'Xls');
            $objWriter->save('php://output');
            exit();

        } catch (PDOException $e){
            $response->info->hasErrors = true;
            $response->info->errors[] = $selectQuery;
            $response->info->errors[] = print_r($queryClass->params, true);
            $response->info->errors[] = print_r($e, true);
        }
    }

    private function printResponse($response){
        header('Content-Type: application/json');
        echo json_encode($response);
        die();
    }

    /**
     * @var IQuery
     */
    private function loadQuery($request){
        if ($request == null || !property_exists($request, "queryName") || StringUtils::isBlank($request->queryName)){
            return;
        }

        $className = substr($request->queryName, strrpos($request->queryName, ".") + 1);
        $filePath = str_replace(".", "/", $request->queryName);
        $queryClassFilePath = __DIR__ ."/../../../../template/query/" . $filePath . ".php";

        if (!file_exists($queryClassFilePath)){
            return;
        }

        try{
            require_once ($queryClassFilePath);
            if (!class_exists($className)){
                return;
            }
            
            $queryClass = new $className();
            $queryClass->request = $request;

            $descriptor = $queryClass->descriptor();
            if (isset($descriptor["permissions"])){
                foreach ($descriptor["permissions"] as $action) {
                    if (UserRoleService::canCurrentUserDo($action)){
                        return $queryClass;
                    }
                }
            }
        } catch(Error $ex){
            LogService::error_($this, "Errore caricamento query", $ex);
        }

        return;
    }
}
