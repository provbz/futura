<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GetAction
 *
 * @author marco
 */
class GetAction extends PublicAction {
    
    public $Id;
    public $aeId;
    public $method = "inline";
    
    public function _default(){
        if ($this->currentUser == null){
            redirect(UriService::accessDanyActionUrl());
        }
        
        AttachmentService::loadAttachmentEntityContent($this->aeId, $this->Id, $this->method);
    }
    
}
