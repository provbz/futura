<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileAction
 *
 * @author marco
 */
class UploadAction extends PublicAction{
    
    public $ajax;
    public $noTemp = false;
    public $entity;
    public $entityId;
    public $type;
    public $visibility;
    
    public function _default(){
        if ($this->currentUser == null){
            $this->SendResult(true, "Operazione non consentita.");    
            die();
        }
        
        if (!isset($_FILES["file"])){
            $this->SendResult(true, "Nessun file associato.");    
            die();
        }

        $file = $_FILES["file"];

        if ($_FILES['file']['error'] !== UPLOAD_ERR_OK) {
            $this->SendResult(true, "Errore nel caricamento. ". $_FILES['file']['error'] . ' ' . $this->file_upload_max_size());    
            die();
         }

        if ($file["size"] > 10485760){
            $this->SendResult(true, "Dimensione massima 10Mb.");    
            die();
        }

        $allowedExtensions = array('pdf', "doc", "docx", "xls", "xlsx", 'png', 'jpg');
        $filename = $_FILES['file']['name'];

        if (strlen($filename) > 255){
            $this->SendResult(true, "Il nome del file non deve superare i 256 caratteri");    
            die();
        }

        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if (!in_array(strtolower($ext), $allowedExtensions)) {
            $this->SendResult(true, "Estensione file non ammessa");    
            die();
        }

        $allowedMime = [
            'application/pdf', 
            "application/msword", 
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document", 
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.ms-excel", 
            'image/x-png', 
            'image/jpeg',
            ""
        ];
        if (!in_array($file["type"], $allowedMime)) {
            $this->SendResult(true, "mime type non ammesso " . $file["type"]);
            die();
        }

        if ($this->visibility == null){
            $this->visibility = AttachmentVisibility::LOGGED_USER;
        }

        $attachment = [
            "filename" => $file["name"],
            "size" => $file["size"],
            "content_type" => $file["type"],
            "status" => AttachmentStatus::TEMP,
            "insert_date" => "NOW()",
            "insert_user_id" => $this->currentUser["user_id"]
        ];
        
        $attachmentId = EM::insertEntity("attachment", $attachment);
        EM::updateEntity("attachment", [
            "filename" => $attachmentId . "_" . $file["name"]
        ], [
            "attachment_id" => $attachmentId
        ]);

        $attachment = AttachmentService::find($attachmentId);

        $path = UriService::buildAttachmentTmpPath() . "/" . $attachmentId;
        move_uploaded_file($file["tmp_name"], $path);
        EncryptService::encryptfile($path);
        
        if ($this->noTemp){
            $this->uploadDefFile($attachment);
        } else {
            $this->prepareTempMv($attachment, $file);
        }
    }

    private function file_upload_max_size() {
        static $max_size = -1;
      
        if ($max_size < 0) {
          // Start with post_max_size.
          $post_max_size = $this->parse_size(ini_get('post_max_size'));
          if ($post_max_size > 0) {
            $max_size = $post_max_size;
          }
      
          // If upload_max_size is less, then reduce. Except if upload_max_size is
          // zero, which indicates no limit.
          $upload_max = $this->parse_size(ini_get('upload_max_filesize'));
          if ($upload_max > 0 && $upload_max < $max_size) {
            $max_size = $upload_max;
          }
        }
        return $max_size;
    }

    private function parse_size($size) {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
          // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
          return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
          return round($size);
        }
      }

    private function prepareTempMv($attachment, $file){
        $result = new ModelViewResult();
        $result->HasErrors = false;
        $result->Message = "Caricamento eseguito";
        $result->Attachment = new ModelViewAttachment();
        $result->Attachment->AttachmentId = $attachment["attachment_id"];
        $result->Attachment->FileName = $file["name"];
        $result->Attachment->Size = $file["size"];
        $result->Attachment->insertDate = $attachment["insert_date"];
        $result->Attachment->Url = UriService::buildAttachmentUrl($attachment, null);
                
        $this->write($result);
    }

    private function uploadDefFile($attachment){
        if (StringUtils::isBlank($this->entity)){
            return;
        }

        $filePath = "/" . $this->entity . "/" . $this->entityId . "/" . $this->type;
        $fullPath = UriService::buildAttachmentPath() . $filePath;
        if (!file_exists($fullPath)){
            mkdir($fullPath, 0777, true);
        }

        $tmpPath = UriService::buildAttachmentTmpPath() . "/" . $attachment['attachment_id'];
        rename($tmpPath, $fullPath . "/" . $attachment['filename']);

        EM::updateEntity("attachment", [
            "status" => AttachmentStatus::READY,
            "path" => $filePath
            ], 
            [
                "attachment_id" => $attachment["attachment_id"]
            ]
        );
    
        $attachmentEntityId = EM::insertEntity("attachment_entity", [
            "attachment_id" => $attachment['attachment_id'],
            "entity" => $this->entity,
            "entity_id" => $this->entityId,
            "type" => $this->type,
            "sort" => 0,
            "insert_date" => "NOW()",
            "insert_user_id" => $this->currentUser['user_id']
        ]);

        $attachmentMv = AttachmentService::findInMV($attachmentEntityId);
        $res = new ModelViewResult();
        $res->Attachment = $attachmentMv;
        $res->HasErrors = false;
        $res->Message = "Caricamento eseguito";

        $this->postUploadHandlers($attachmentEntityId);

        $this->write($res); 
    }

    private function postUploadHandlers($attachmentEntityId){
        $ae = AttachmentService::findAttachmentEntityForAttachmentEntityId($attachmentEntityId);
        if ($ae == null){
            return;
        }

        switch ($ae["entity"]){
            case EntityName::USER: 
                if (strpos($ae["type"], DocumentType::PEI . "_sy_") === 0 ||
                    strpos($ae["type"], DocumentType::PDP . "_sy_") === 0 ||
                    strpos($ae["type"], DocumentType::PDP_LINGUA . "_sy_") === 0 ||
                    strpos($ae["type"], DocumentType::PEI_TIROCINIO . "_sy_") === 0){
                    
                    $sy = substr($ae["type"], strrpos($ae["type"], "_") + 1);
                    $type = substr($ae["type"], 0, strpos($ae["type"], "_"));

                    if (strpos($ae["type"], DocumentType::PDP_LINGUA . "_sy_") === 0){
                        $type = DocumentType::PDP_LINGUA;
                    } else if (strpos($ae["type"], DocumentType::PEI_TIROCINIO . "_sy_") === 0){
                        $type = DocumentType::PEI_TIROCINIO;
                    }
                    
                    $documents = DocumentService::findForUserAndYearAndType($ae["entity_id"], $sy, $type);
                    $document = $documents->fetch();
                    if ($document != null){
                        PeiService::updatePeiStatus($document["user_year_document_id"]);
                    }
                }

                if ($ae["type"] == AttachmentType::AT_DIAGNOSI){
                    NoticeService::persist([
                        "type" => NoticeType::USER_DIAGNOSI_NEW,
                        "entity_id" => $ae["entity_id"],
                        "entity" => $ae["entity"],
                        "parameters" => json_encode([
                            "attachment_entity_id" => $ae["attachment_entity_id"],
                            "attachment_id" => $ae["attachment_id"],
                            "insert_user_id" => $this->currentUser["user_id"]
                        ])
                    ]);
                }
                break;
        }
    }

    private function SendResult($hasErrors, $message){
        $result = new ModelViewResult();
        $result->HasErrors = $hasErrors;
        $result->Message = $message;
        
        $this->write($result);
    }
    
    private function write($result)
    { 
        header("ContentType: application/json");
        echo json_encode($result);
        die();
    }
}

class ModelViewResult
{
    public $HasErrors;
    public $Message;
    public $Attachment;
}
