<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of LoginAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class LoginAction extends PublicAction {

    public $code;
    public $error;
    
    public function _default() {
        $par = [
            'clientId' => Office365Constants::$CLIENT_ID,
            'clientSecret' => Office365Constants::$CLIENT_SECRET,
            'redirectUri' => Office365Constants::$REDIRECT_URI,
            'urlAuthorize' => Office365Constants::$AUTHORITY_URL . Office365Constants::$AUTHORIZE_ENDPOINT,
            'urlAccessToken' => Office365Constants::$AUTHORITY_URL . Office365Constants::$TOKEN_ENDPOINT,
            'urlResourceOwnerDetails' => '',
            'scopes' => Office365Constants::$SCOPES
        ];
        
        $provider = new \League\OAuth2\Client\Provider\GenericProvider($par);

        if (StringUtils::isBlank($this->code) && StringUtils::isBlank($this->error)) {
            $authorizationUrl = $provider->getAuthorizationUrl();
            $_SESSION['state'] = $provider->getState();
            header('Location: ' . $authorizationUrl);
            return;
        } else if (StringUtils::isNotBlank($this->code)) {
            if (isset($_SESSION["state"])){
                if (!isset($_GET['state'])){
                    unset($_SESSION['state']);
                    redirect(UriService::accessDanyActionUrl());
                }

                if ($_GET['state'] !== $_SESSION['state']) {
                    unset($_SESSION['state']);
                    exit('Codice state non valido.');
                    die();
                }
            }

            if (!isset($_SESSION['access_token'])){
                try {
                    $accessToken = $provider->getAccessToken('authorization_code', [
                        'code' => $_GET['code']
                    ]);
                    
                    $microsoftGraphTocken = $accessToken->getToken();
                    $_SESSION['access_token'] = $microsoftGraphTocken;

                    $idToken = $accessToken->getValues()['id_token'];
                    $decodedAccessTokenPayload = base64_decode(
                        explode('.', $idToken)[1]
                    );

                    $jsonAccessTokenPayload = json_decode($decodedAccessTokenPayload, true);
                    if (!isset($jsonAccessTokenPayload['preferred_username']) || !isset($jsonAccessTokenPayload['name'])){
                        printf('Impossibile recuperare il token');
                        LogService::error_($this, "errore TOKEN non valido\n" . print_r($jsonAccessTokenPayload, true));
                        die();    
                    }
                    
                    $_SESSION['preferred_username'] = $jsonAccessTokenPayload['preferred_username'];
                    $_SESSION['given_name'] = $jsonAccessTokenPayload['name'];
                } catch (League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                    printf('Impossibile recuperare il TOKEN: %s', $e->getMessage());
                    die();
                }
            }
            
            if(isset($_SESSION["access_token"])){
                $microsoftGraphTocken = $_SESSION['access_token'];
                $res = WebClient::exec(WebClient::GET, "https://graph.microsoft.com/v1.0/me/",
                    array(
                    'Authorization: Bearer ' . $microsoftGraphTocken,
                    'Content-Type: application/json;' .
                                'odata.metadata=minimal;' .
                                'odata.streaming=true'
                ));
                
                if ($res == null || !is_object($res)){
                    echo "Dati di login non validi";
                    die();
                }

                $mail = "";
                if (property_exists($res, "mail") && StringUtils::isNotBlank($res->mail)){
                    $mail = trim( $res->mail );
                }
                if (StringUtils::isBlank($mail) && property_exists($res, 'userPrincipalName') && $res->userPrincipalName != null){
                    $mail = trim( $res->userPrincipalName );
                }

                $mail = strtolower( $mail );
                $user = UserService::findUserByOffice365Account($mail);
                
                $sudtirolDomain = "@shule.suedtirol.it";
                if ($user == null && strpos($mail, "@shule.suedtirol.it") !== false){
                    $mailName = str_replace($sudtirolDomain, "", $mail);
                    $user = UserService::findUserLikeEmail($mailName . "@%");
                    if ($uesr != null){
                        EM::updateEntity("user", [
                            "email" => $mail
                        ], ["user_id" => $user["user_id"]]);
                    }
                } 

                if ($user != null){
                    UserService::setLoggedUser($user);
                    $_SESSION['isOffice365Account'] = true;
                    redirect(UriService::buildPageUrl("/public/LoginAction"), false);
                    return;
                } else {
                    LogService::info("Office365Login", print_r($res, true));
                    $this->header();    
                    $_SESSION['access_token'] = null;
                    ?>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 columns">
                                <h2>Attenzione</h2>
                                <p>L'utente non è censito nel sistema, contattare il proprio responsabile per aggiornare i diritti di accesso.</p>
                                <p>Indirizzo ricevuto: <?= _t($mail) ?></p>
                                <div>
                                    <pre>
                                    <?= print_r($res); ?>
                                    </pre>
                                </div>
                                
                                <a href="/public/LoginAction">Torna al login</a>
                            </div>
                        </div>
                    <?php
                    $this->footer();
                }
            }
        }
        
        if (StringUtils::isNotBlank($this->error)) {
            $this->header();
            ?>
                <div class="row">
                    <div class="col-sm-12 col-md-6 columns">
                        <h2>Attenzione</h2>
                        <p>Errore di autenticazione: <?= _t($this->error) ?></p>
                        <p>
                            <?php if (isset($_GET['error_description'])){ ?>
                                <?= _t($_GET['error_description']) ?>
                            <?php } ?>
                        </p>
                        <a href="/public/LoginAction">Torna al login</a>
                    </div>
                </div>
            <?php
            $this->footer();
        }
    }

}
