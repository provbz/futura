<?php

class LoginAction extends PublicAction{
    
    public $username;
    public $password;
    public $formErrors;
    
    public $errors = [];
    
    public function _default(){
        if (isset($_SESSION[SESSION_REDIRECT_MESSAGE])){
            $message = $_SESSION[SESSION_REDIRECT_MESSAGE];
            unset ($_SESSION[SESSION_REDIRECT_MESSAGE]);
        }
        
        $this->header();

        ?>
        <script type="text/javascript">
        var onSubmit = function(token) {
            document.getElementById("login").submit();
        };

        <?php if (RecaptchaConstants::$ENABLED){ ?>
        var onloadCallback = function() {
            grecaptcha.render('invia', {
                'sitekey' : '<?= RecaptchaConstants::$CLIENT_INVISIBLE_SECRET ?>',
                'callback' : onSubmit
            });
        };
        <?php } ?>
        </script>
     
        <div class="row ingradimento-1024 distanza-main">   
            <form name="login" id="login" action="<?= $this->actionUrl('signin') ?>" method="post" onsubmit="getTocken()">
                <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE || strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== FALSE)) { ?>
                    <div class="large-6 medium-6 small-12 columns larghezza-form small-centered margin-top">
                        <h3>Il browser non è supportato</h3>
                        <h4>Eseguire l'accesso utilizzando un altro browser</h4>
                    </div>
                <?php } else { ?>
                    <div class="large-6 medium-6 small-12 columns">
                        <h3 class="login-form margin-bottom">Effettua login LASIS</h3>
                        <div class="row collapse larghezza-campi-form" style="height: 124px; padding-top: 18px;">
                            <p>Premere il pulsante sotto e inserire le proprie credenziali LASIS.</p>
                        </div>
                        <div class="margin-bottom">
                            <div class="row collapse larghezza-campi-form">
                                <a class="button radius expand small" href="<?= UriService::buildPageUrl("/public/office365/LoginAction") ?>">Accedi con credenziali LASIS</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="large-6 medium-6 small-12 columns">
                        <h3 class="login-form margin-bottom">Effettua login locale</h3>
                        
                        <?php if (count($this->errors) > 0){ ?>
                        <div class="row collapse larghezza-campi-form">
                            <small class="error" style="display: block;">
                                <?= _t( implode("<br/>", $this->errors) ) ?>
                            </small>
                        </div>
                        <?php } ?>

                        <div class="row collapse larghezza-campi-form margin-bottom">
                            <div class="large-12 medium-12 small-12 columns">
                                <input type="text" name="username" id="nome" placeholder="e-mail" value="<?= _t( $this->username ) ?>" required class="nome-utente">
                            </div>
                        </div>	
                        <div class="row collapse larghezza-campi-form margin-bottom">
                            <div class="large-12 medium-12 small-12 columns left">
                                <input type="password" name="password" id="password" placeholder="Password" required>
                            </div>
                        </div>	
                        <div class="row collapse larghezza-campi-form">
                            <input type="submit" name="invia" id="invia" class="button success radius small expand" value="Accedi con dati utente">
                        </div>	

                        <!--recupera password -->	
                        <div class="row collapse larghezza-campi-form">		
                            <p class="left">
                                <a href="<?= UriService::buildPageUrl("/public/RetriveDataAction") ?>" class="recuperapassword"><img src="/img/ico-recupera-dati.png"> Recupera i dati di accesso
                                </a>
                            </p>
                        </div>
                    </div>
                <?php } ?>
            </form>
        </div>
        <?php if (RecaptchaConstants::$ENABLED){ ?>
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
        <?php } ?>
        <?php
        $this->footer();
    }
    
    public function signup(){
        $password1 = getRequestValue("password_1");
        $password2 = getRequestValue("password_2");
        $otp = getRequestValue("otp");
        
        if (StringUtils::isBlank($otp)){
            redirect(UriService::buildPageUrl("LoginAction"));
            die();
        }
        $user = UserService::findByOtp($otp);
        if ($user == NULL){
            redirect(UriService::buildPageUrl("LoginAction"));
            die();
        }
        
        if (StringUtils::isBlank($password1)){
            array_push($this->errors, "Inserire la password da utilizzare!");
        }
        if (strlen($password1) < 8){
            array_push($this->errors, "La password deve essere almeno di 8 caratteri!");
        }
        if ($password1 != $password2){
            array_push($this->errors, "Le password non coincidono!");
        }
        
        if (count($this->errors) > 0){
            $this->_default();
            return;
        }
        
        EM::updateEntity("user", [
            "password"=> $password1, 
            "one_time_access"=>""
        ],  [
            "user_id" => $user['user_id']
        ]);

        $_REQUEST['user_id'] = $user['username'];
        $_REQUEST['password'] = $password1;

        $this->signin();
    }
    
    public function signin(){
        $this->formErrors = [];
        $user = null;
                
        if (StringUtils::isBlank($this->username)){
            array_push($this->errors, "L'indirizzo mail è richiesto!");
        }

        if (StringUtils::isBlank($this->password)){
            array_push($this->errors, "La password è richiesta!");
        }
        
        if (RecaptchaConstants::$ENABLED){
            $recaptcha = '';
            if (!isset($_REQUEST['g-recaptcha-response'])){
                array_push($this->errors, "Richiesta non valida.");
            } else {
                $recaptcha = $_REQUEST['g-recaptcha-response'];
            }
            
            if (StringUtils::isBlank($recaptcha)){
                array_push($this->errors, "Richiesta non valida.");
            } else {
                $res = RecaptchaService::check($recaptcha, RecaptchaConstants::$SERVER_INVISIBLE_SECRET);

                if (!$res->success){
                    array_push($this->errors, "Richiesta non valida.");
                    LogService::warn("LoginAction", json_encode($res));
                }
            }
        }
        
        if (count($this->errors) > 0){
            $this->_default();    
            return;
        }
        
        if (Constants::$devMode){
            $user = UserService::findUserByEmail($this->username);
        } else {
            $user = UserService::findUserByEmailAndPassword($this->username, $this->password);
        }
        
        if ($user == NULL){
            array_push($this->errors, "Dati di accesso non validi!");
            $this->_default();    
            return;
        }
        
        try{
            UserService::setLoggedUser($user);
            redirect("/");
        } catch(Exception $ex){
            array_push($this->errors, $ex->getMessage());
            $this->_default();
        }        
    }
    
    public function signout(){
        header( 'Cache-Control: no-store, no-cache, must-revalidate' ); 
        header( 'Cache-Control: post-check=0, pre-check=0', false ); 
        header( 'Pragma: no-cache' ); 

        if (isset($_SESSION['user_id'])){
            UserService::logout($_SESSION['user_id']);
        }
        
        redirect( $this->actionUrl("_default") );
    }
}
