<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AccessDenyAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class AccessDenyAction extends PublicAction {
    
    public function _default(){
        http_response_code(400);
        
        $this->header();
        ?>
        <h2>Accesso negato!</h2>
        <?php if ($this->currentUser != NULL){ ?>
        <p>Non è possibile visualizzare questa pagina</p>
        <a href="/" class="button">Torna alla home</a>
        <?php } else { ?>
        <a href="/" class="button">Esegui il login</a>
        <?php
        }
        $this->footer();
    }
    
}