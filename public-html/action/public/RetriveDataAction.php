<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of RetriveDataAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class RetriveDataAction extends PublicAction{
    
    public $email;
    
    public $ota;
    public $password1;
    public $password2;
    
    public function _default(){
        if ($this->currentUser != null){
            redirect("/");
        }
        
        $form = new Form($this->actionUrl());
        $form->submitButtonLabel = "Invia";
        $form->cancellAction = UriService::buildPageUrl("/public/LoginAction");
        
        $mailField = new FormTextField("e-mail", "email", FormFieldType::STRING, true);
        $mailField->checkFunction = function($field, $value) {
            if (!StringUtils::validEmail($this->email)){
                $field->addFieldError('Indirizzo e-mail non valido.');            
            } else {
                $user = UserService::findUserByEmail($this->email);
                if ($user == NULL || $user['status'] != UserStatus::ENABLED){
                    $field->addFieldError('Nessun utente registrato con questo indirizzo e-mail. O account disabilitato.');
                }
            }    
        };
        
        $form->addField($mailField);
        if (RecaptchaConstants::$ENABLED){
            $form->addField(new FormRecaptchaField());
        }
        
        if ($form->isSubmit() && $form->checkFields()){
            $user = UserService::findUserByEmail($this->email);

            NoticeService::persist([
                "type" => NoticeType::USER_RETRIVE_DATA,
                "parameters" => json_encode(["user_id" => $user['user_id']])
            ]);

            redirect($this->actionUrl("completed"));        
        }
        
        $this->header();
        if (RecaptchaConstants::$ENABLED){
            ?>
            <script src="https://www.google.com/recaptcha/api.js" async defer></script>
            <?php
        }
        ?>
        <div class="row ingradimento-1024 distanza-main">   
            <div class="tabs-content">
                <div class="intestazione">
                    <h2>Recupero dati di accesso</h2>
                </div>
                <p>
                    Inserire l’indirizzo e-mail usato al momento della registrazione per eseguire il recupero dei dati di accesso.
                </p>
                <?php $form->draw(); ?>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    public function completed(){
        $this->header();
        ?>
        <div class="row ingradimento-1024 distanza-main">   
            <div class="intestazione">
                <h2>e-mail inviata</h2>
            </div>
            <p>
                Verifica la tua casella di posta, riceverai a breve un messaggio con le istruzioni per effettuare l'accesso al sistema.
            </p>
        </div>
        <?php
        $this->footer();
    }
    
    public function signup(){
        if (StringUtils::isBlank($this->ota)){
            redirect(UriService::accessDanyActionUrl());
        }

        $user = UserService::findByOtp($this->ota);
        if ($user == NULL){
            redirect(UriService::accessDanyActionUrl());
        }
        
        if ($user['email'] != $this->email){
            redirect(UriService::accessDanyActionUrl());
        }
        
        $form = new Form($this->actionUrl("signup"));
        $form->addField(new FormHiddenField("ota", "ota", FormFieldType::STRING, $this->ota));
        $form->addField(new FormHiddenField("email", "email", FormFieldType::STRING, $this->email));
        
        require_once './template/user/UserFormTemplate.php';
        $form->addField(UserFormTemplate::buildPasswordField($user));
        $form->addField(UserFormTemplate::buildPassword2Field());
        
        if ($form->isSubmit() && $form->checkFields()){
            UserService::updatePassword($user['user_id'], $this->password1);
            redirect(UriService::buildPageUrl("/"));
        }
        
        $this->header();
        ?>
        <div class="intestazione">
            <h2>Definizione nuova password di accesso</h2>
        </div>
        <?php 
        $form->draw();
        
        $this->footer();
    }   
}
