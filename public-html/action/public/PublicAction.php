<?php

class PublicAction extends Page {
    
    public $structure;
    
    public function _prepare() {
        parent::_prepare();
        $this->menu = NULL;
    }
    
    public function header($displayPageHeader = true, $angularApp = NULL){
        $this->addStyleFile("style_login.css");
        $this->addStyleFile("normalize_login.css");
        $this->addStyleFile("developer_login.css");
        
        parent::header($displayPageHeader, $angularApp);
    }
    
    public function footer($displayFooter = false) {
        ?>
                </div>
            </div>
        </div>
        <div id="push"></div>
        <div class="row fullWidth pattern-righe">   &nbsp; </div>
          <!--footer -->	
        <footer>
            <div class="row ingradimento-1024 text-center">
                <p class="testo-footer ">
                    
                </p>
             </div>
        </footer>
        <?php
        parent::footer($displayFooter);
    }
}
