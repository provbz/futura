<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
require_once __DIR__ . "/../UserYearDocumentAction.php";

class DropOutUsersAction extends UserYearDocumentAction{

    public $id;
    public $filter_school_year_id;

    public function _default(){
        $this->setGlobalSessionValue("documentType", DocumentType::DROP_OUT);
        $this->menu->setSelectedMenuItem($this->actionUrl());

        $data = [
            "documentToClose" => $this->findNotClosedDocuments()
        ];

        $this->header();
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row-100">
                    <div class="large-8 medium-7 small-7 columns">
                        <h3>Segnalazioni di frequenze irregolari</h3>
                    </div>
                    <div class="large-4 medium-3 small-5 columns buttontoolbar">
                    <?php if ($this->filter_school_year_id == null || $this->filter_school_year_id == $currentSchoolYear["school_year_id"]){ ?>
                        <a href="<?= $this->actionUrl("newUser") ?>" class="button radius nuovo float-right">
                            <i class="<?= Icon::PLUS ?> margin-right"></i> Inserisci
                        </a>
                    <?php } ?>
                    </div>
                </div>
                
                <div class="mb-structure-drop-out-table-container"></div>
                <script>
                    MbCreateVue('mb-structure-drop-out-table', 'mb-structure-drop-out-table-container', <?= json_encode($data) ?>);
                </script>
            </div>
        </div>
        <?php
        $this->footer();
    }

    private function findNotClosedDocuments(){
        $currentSchoolYear = SchoolYearService::findCurrentYear();

        $res = EM::execQuery("SELECT sy.school_year, u.code, u.name, u.surname, uyd.user_year_document_id
            from user_year_document uyd
            LEFT JOIN user u on uyd.user_id=u.user_id
            LEFT JOIN user_structure us ON uyd.user_id  = us.user_id
            LEFT JOIN school_year sy ON sy.school_year_id = uyd.school_year_id
            where structure_id = :structure_id
                AND uyd.status = 'ready' AND uyd.type='drop_out'
                and uyd.school_year_id < :school_year_id
                and drop_out_esito_dictionary_id IS NULL
            ORDER BY sy.school_year", [
                    "structure_id" => $this->structure["structure_id"],
                    "school_year_id" => $currentSchoolYear["school_year_id"]
                ]);

        return $res->fetchAll();
    }

    public function thisStructureUser($code, $user, $additionalRequestParameters = []){
        $currentYear = SchoolYearService::findCurrentYear();
        $documents = DocumentService::findForUserAndYearAndType($user['user_id'], $currentYear['school_year_id'], DocumentType::DROP_OUT);
        $document = $documents->fetch();

        $params = ["u_code" => $code];
        if ($document != null){

            $params = ["documentId" => $document["user_year_document_id"]];
        }

        redirect($this->actionUrl("newUserFull", $params));
    }

    public function newUserFull(){
        $currentYear = SchoolYearService::findCurrentYear();

        $document = DocumentService::find($this->documentId);
        $user = null;
        $userYear = null;
        $metadata = null;

        if ($document != null){
            $user = UserService::find($document["user_id"]);
            $userYear = UserYearService::findUserYearByUser($document["user_id"]);
        } else if (StringUtils::isNotBlank($this->u_code)){
            $user = UserService::findByCode($this->u_code);
            if ($user != null){
                $userYear = UserYearService::findUserYearByUser($user["user_id"]);
            }
        }

        if ($user != NULL ){
            $userStructure = StructureService::hasUserStructureRole($user["user_id"], $this->structureId, UserRole::STUDENTE);
            if ($userStructure == null){
                redirect(UriService::accessDanyActionUrl());
            }

            $metadata = MetadataService::findDictionary("user", $user["user_id"]);
        }
        
        $form = new Form($this->actionUrl("newUserFull", ["documentId" => $this->documentId, "u_code" => $this->u_code]));
        if ($document == null){
            $form->cancellAction = $this->actionUrl("_default");
        } else {
            $form->cancellAction = UriService::buildPageUrl("/structure/dropOut/DropOutDetailsAction", "", ["documentId" => $document["user_year_document_id"]]);
        }
        
        $entity = [];
        ReflectionUtils::fillWithPrefix($user, "u_", $entity);
        ReflectionUtils::fillWithPrefix($userYear, "uy_", $entity);
        ReflectionUtils::fillWithPrefix($metadata, "um_", $entity);
        $form->entity = $entity;
        $form->entity["section"] = UserSection::PEI;
        
        $year = new FormTextField("Anno scolastico", "uyn_school_year", FormFieldType::STRING, false, $currentYear['school_year']);
        $year->readOnly = true;
        $form->addField($year);
        
        $form->addField(new FormHtmlField("<h4>Dati studente</h4>"));

        /* Dati condivisi */
        $popcornField = $this->createPopCornField($user);
        $form->addField($popcornField);
        
        /*
        $form->addField(new FormTextField("Nome", "u_name", FormFieldType::STRING, true));
        $form->addField(new FormTextField("Cognome", "u_surname", FormFieldType::STRING, true));
        */
        
        $genders = ['' => ''] + DictionaryService::findGroupAsDictionary(Dictionary::GENDER);
        $form->addField(new FormSelectField("Sesso", "um_gender_id", FormFieldType::STRING, $genders, true));

        $form->addField(new FormDateMultipleField("Data di nascita", "um_" . UserMetadata::BIRTH_DATE, FormFieldType::DATE_TIME, true));
        $form->addField(new FormTextField("Luogo di nascita", "um_birth_place", FormFieldType::DATE_TIME));
        $form->addField(new FormTextField("Stato di nascita", "um_stato_nascita", FormFieldType::STRING, false));

        //$form->addField(new FormTextField("Cittadinanza", "um_cittadinanza", FormFieldType::STRING, false));
        $nazioni = NazioneService::findAll();
        $states = [];
        foreach ($nazioni as $nazione) {
            $states[$nazione["nazione_id"]] = $nazione["nome"];
        }
        $form->addField(new FormSelect2Field("Cittadinanza", "uy_cittadinanza_1_nazione_id", FormFieldType::NUMBER, $states, true));

        $plessi = StructureService::findAllWithParent($this->structureId);
        $values = ["" => ""];
        foreach ($plessi as $plesso) {
            $values[$plesso["structure_id"]] = $this->structure["name"] . " - " . $plesso["name"];
        }
        $form->addField(new FormSelectField("Plesso", "uy_plesso_structure_id", FormFieldType::NUMBER, $values, true));

        $classes = ['' => ''] + DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $form->addField(new FormSelectField("Classe", "uy_classe", FormFieldType::STRING, $classes, true));
        $form->addField(new FormTextField("Sezione", "uy_sezione", FormFieldType::STRING, false, "", [
            "maxlength" => 20
        ]));
        /* Fine dati condivisi */

        if ($form->isSubmit() && $form->checkFields()){
            $data = [];
            foreach($form->fields as $key => $value){
                $data[$value->name] = $value->getValueForDb();
            }
            
            $newUser = ReflectionUtils::getPropertyWithPrefix($data, "u_");
            if ($user == null){
                $newUser['created_by_user_id'] = $this->currentUser['user_id'];
                $newUser['insert_date'] = 'NOW()';
                $userId = EM::insertEntity("user", $newUser);
                $user = UserService::find($userId);
                EM::insertEntity("user_role", [
                    'user_id' => $userId, 
                    'role_id' => UserRole::USER
                ]);
                StructureService::addUserStructure($userId, $this->structureId, [UserRole::STUDENTE]);
            } else {
                unset($newUser['code']);
                EM::updateEntity("user", $newUser, [
                    "user_id" => $user["user_id"]
                ]);
            }

            $newUserYear = ReflectionUtils::getPropertyWithPrefix($data, "uy_");
            
            if ($userYear == null){
                $newUserYear['user_id'] = $user["user_id"];
                $userYearId = EM::insertEntity("user_year", $newUserYear);
                $userYear = UserYearService::find($userYearId);
            } else {
                EM::updateEntity("user_year", $newUserYear, ["user_year_id" => $userYear["user_year_id"]]);
            }

            if ($document == null){
                $this->documentId = DocumentService::create($user["user_id"], $this->currentUser['user_id'], DocumentType::DROP_OUT);
                DropOutService::setStatus($this->documentId, DropOutStatus::GREEN);

                NoticeService::persist([
                    "type" => NoticeType::DROP_OUT_NEW_DOCUMENT,
                    "entity" => EntityName::USER_YEAR_DOCUMENT,
                    "entity_id" => $this->documentId
                ]);
            } 
            
            $metadata = ReflectionUtils::getPropertyWithPrefix($data, "um_");
            MetadataService::removeMetadataWithKey("user", $user["user_id"], $metadata);
            MetadataService::persist("user", $user["user_id"], $metadata);

            redirectWithMessage(UriService::buildPageUrl("/structure/dropOut/DropOutDetailsAction", "", ["documentId" => $this->documentId]), "Dati salvati");
        }
        
        $this->header();
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row">
                    <div class="large-10 medium-9 small-7 columns">
                        <h3>Segnalazioni di frequenze irregolari</h3>
                    </div>
                </div>
                <div class="table-row row">
                    <div class="large-12 columns">
                        <?php
                        $form->draw();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function removeDropOut(){
        if ($this->id == null){
            return "Documento non valido";
        }

        $document = DocumentService::find($this->id);
        if ($document == null){
            return "Documento non trovato";
        }
        
        DocumentService::updateStatus($this->id, UserYearDocumentStatus::DELETED);

        echo 'ok';
    }
}