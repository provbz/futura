<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
 class DropOutDetailsAction extends StructureUserDocumentAction{

    public $id;
    public $type;
    public $displayTools = true;
    public $editable = true;
    
    public function _prepare(){
        $this->checkUserToUser = false;
        $this->allowNotCurrentYearAccess = true;
        parent::_prepare();

        if ($this->document != null && $this->document['type'] != DocumentType::DROP_OUT){
            redirect(UriService::accessDanyActionUrl());
        }
    }

    public function _default(){
        $this->setGlobalSessionValue("backUrl", "");
        $this->header();
        $this->_defaultBody();
        $this->footer();
    }

    public function _defaultBody(){
        $dropOutActions = DictionaryService::findGroupAsKeyValuePair(Dictionary::DROP_OUT_AZIONI, false, "dictionary_id");
        $dropOutEsito = DictionaryService::findGroupAsKeyValuePair(Dictionary::DROP_OUT_ESITO, false, "dictionary_id");
        $dropOutStatoIscrizione = DictionaryService::findGroupAsKeyValuePair(Dictionary::DROP_OUT_STATO_ISCRIZIONE, false, "dictionary_id");
        
        $table = new Table();
        $table->paginateElements = false;
        $table->entityName = "drop_out_row dor";
        $table->addSelect("*");
        $table->addJoin("LEFT JOIN school_year sy ON sy.school_year_id=dor.school_year_id");

        $table->addWhere("user_year_document_id=:id", "id", $this->document["user_year_document_id"]);
        $table->addWhere("dor.status=:status_ready", "status_ready", RowStatus::READY);

        $table->addColumn(new TableColumn("plesso_structure_id", "Istituto", function($id){
            return StructureService::printFullStructureName($id);
        }));
        $table->addColumn(new TableColumn("data_segnalazione", "Data", function($v){
            return TextService::formatDate( DateUtils::getDate($v) );
        }));
        $table->addColumn(new TableColumn("insert_user_id", "Inserito da", function($userId){
            $user = UserService::find($userId);
            return $user["name"] . ' ' . $user["surname"];
        }));
        $table->addColumn(new TableColumn("type", "Tipo", function($type, $row) use ($dropOutActions, $dropOutEsito, $dropOutStatoIscrizione) {
            $res = ucfirst($type);
            return $res;
        }));
        $table->addColumn(new TableColumn("type", "Dettagli", function($type, $row) use ($dropOutActions, $dropOutEsito, $dropOutStatoIscrizione) {
            $res = '';
            if ($type == DropOutRowType::SEGNALAZIONE){
                if ($row["giorni_assenza_dad"] == null){
                    $row["giorni_assenza_dad"] = 0;
                }
                $res .= "Giorni previsti frequenza: <strong>{$row["giorni_frequenza"]}</strong> Assenza: <strong>{$row["giorni_assenza"]}</strong> (in presenza) 
                        <strong>{$row["giorni_assenza_dad"]}</strong> (DAD)";
                
                $azione = DictionaryService::findKeyInValues($dropOutActions, $row["dro_out_row_type_dictionary_id"]);
                if (StringUtils::isNotBlank($azione)){
                    $res .= "<br/>Azione intrapresa: <strong>{$azione}</strong>";
                }
            } else if ($type == DropOutRowType::CHIUSURA_ANNO){
                $esito = DictionaryService::findKeyInValues($dropOutEsito, $row["esito_dictionary_id"]);
                if (StringUtils::isNotBlank($esito)){
                    $res .= "<br>Esito: <strong>{$esito}</strong>";
                }

                if ($row["esito_dictionary_id"] == DropOutEsito::non_ammesso_classe_successiva){
                    $motivazione = DictionaryService::find($row['non_ammesso_motivazioni_dictionary_id']);
                    $res .= " " . $motivazione["value"];
                }

                $iscrizione = DictionaryService::findKeyInValues($dropOutStatoIscrizione, $row["stato_iscrizione_dictionary_id"]);
                if (StringUtils::isNotBlank($iscrizione)){
                    $res .= "<br>Iscrizione: <strong>{$iscrizione}</strong>";

                    if ($row["stato_iscrizione_dictionary_id"] == DropOutStatoIscrizione::non_iscritto_id){
                        $nonIscrittoMotivazioni = DictionaryService::find($row['non_iscritto_motivazioni_dictionary_id']);
                        $res .= ' '. $nonIscrittoMotivazioni["value"];
                    } else if ($row["stato_iscrizione_dictionary_id"] == DropOutStatoIscrizione::iscritto_id){
                        $classe = DictionaryService::find($row['iscritto_classe_dictionary_id']);
                        $scuola = DictionaryService::find($row['iscritto_scuola_dictionary_id']);
                        $res .= ", " . $classe["value"];
                        $res .= ", " . $scuola["value"];
                    }
                }
            }
            return $res;
        }));
        $table->addColumn(new TableColumn("drop_out_row_id", "", function($id, $row){
            $out = "";
            $out .= Formatter::buildTool("", $this->actionUrl("edit", ["id" => $id, "type" => $row["type"]]), Icon::PENCIL);
            
            if ($row["dro_out_row_type_dictionary_id"] == DropOutAzioneKey::SEGNALAZIONE_PROCURA_DICTIONARY_ID){
                $out .= '<a class="button tiny secondary radius" href="/files/dropout/ALLEGATO-2-prot.drop-out 2019.pdf" target="_blank"><i class="fas fa-download"></i> </a>';
            }

            $out .= Formatter::buildTool("", $this->actionUrl("remove", ["id" => $id]), Icon::TRASH, "Procedere alla rimozione dei dati?");
            
            return $out;
        }));

        $table->addOrder("data_segnalazione DESC");

        $prevDropOutRow = DropOutService::findRowLastWithDocumentIdAndType($this->document["user_year_document_id"], $this->document["school_year_id"]);

        ?>
        <style type="text/css">
        .tabs-content .button.tool{
            width: 200px;
        }
        </style>
        <div class="content active tab-studente" id="panelTabStudenti">
            <div class="section-title row-100">
                <div class="large-8 medium-8 columns">
                    <h3>Segnalazione di frequenze irregolari - <?= _t($this->user['code']) ?></h3>
                </div>
                <?php if ($this->displayTools && $prevDropOutRow == null){ ?>
                <div class="large-4 medium-4 columns">                    
                    <a class="button radius alert tiny float_right with-itocn"
                        onclick="return confirm('Rimuovere i dati relativi alle segnalazioni di frequenze irregolari?')" 
                        href="<?= UriService::buildPageUrl("/structure/dropOut/DropOutUsersAction", "remove", ["documentId" => $this->document['user_year_document_id']]) ?>">
                        <i class="<?= Icon::TRASH ?>"></i>Rimuovi
                    </a>
                </div>
                <?php } ?>
            </div>
            
            <div class="areagrigia">
                <div class="table-row row-100">
                    <div class="large-12 medium-12 small-12 columns">
                        <?php $this->drawUserData(); ?>
                    </div>
                </div>

                <div class="section-title row-100">
                    <?php if ($this->displayTools) { ?>
                        
                        <div class="large-12 medium-12 columns student-buttons">                
                            <?php if ($this->document["school_year_id"] == $this->currentYear["school_year_id"]){ ?>
                                <a class="button radius tiny with-itocn tool" href="<?= UriService::buildPageUrl("/structure/dropOut/DropOutUsersAction", 'newUserFull', [
                                    'documentId' => $this->document['user_year_document_id']
                                    ]) ?>">
                                    <i class="<?= Icon::PENCIL ?>"></i> Anagrafica
                                </a>
                                <?php if (!isset($prevDropOutRow["type"]) || $prevDropOutRow["type"] != DropOutRowType::CHIUSURA_ANNO){ ?>
                                    <a class="button radius tiny with-itocn tool" href="<?= $this->actionUrl('edit', [
                                        'documentId' => $this->document['user_year_document_id'],
                                        "type" => DropOutRowType::SEGNALAZIONE
                                        ]) ?>">
                                        <i class="<?= Icon::ADD ?>"></i> Nuova assenza
                                    </a>

                                    <?php if ($this->document["drop_out_status"] != DropOutStatus::GREEN){ ?>
                                        <a class="button radius tiny with-itocn tool" href="<?= $this->actionUrl('edit', [
                                            'documentId' => $this->document['user_year_document_id'],
                                            "type" => DropOutRowType::RIPRESA_FREQUENZA
                                            ]) ?>">
                                            <i class="<?= Icon::CHECK_CIRCLE ?>"></i> Ripresa freq
                                        </a>
                                    <?php } ?>
                                <?php } ?>

                                <a class="button radius tiny with-itocn tool" href="<?= UriService::buildPageUrl("/structure/user/UserTransferAction", "", ["userId" => $this->user["user_id"]]) ?>">
                                    <i class="fas fa-arrow-right"></i> Invia dati trasferimento
                                </a>
                            <?php }  ?> 

                            <?php if (!isset($prevDropOutRow["type"]) || $prevDropOutRow["type"] != DropOutRowType::CHIUSURA_ANNO){ ?>
                                <a class="button radius tiny with-itocn tool" href="<?= $this->actionUrl('edit', [
                                    'documentId' => $this->document['user_year_document_id'], 
                                    "type" => DropOutRowType::CHIUSURA_ANNO
                                    ]) ?>">
                                    <i class="<?= Icon::LOCK ?>"></i> Chiudi anno
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
         
                <?php $table->draw() ?>
            </div>
        </div>
        <?php
    }

    public function drawUserData(){
        $genders = DictionaryService::findGroupAsDictionary(Dictionary::GENDER);
        $class_grades = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $school_year = SchoolYearService::find($this->document["school_year_id"]);
        
        $userData['Identificativo'] = $this->structureId . '-' . $this->user['user_id'] .'-'. $this->userYear['user_year_id'];
        if ($school_year != null){
            $userData['Anno scolastico'] = $school_year["school_year"];
        }

        $userData['Codice'] = $this->user['code'];
        $userData['Data di nascita'] = TextService::formatDate( ReflectionUtils::getValue($this->userMetadata, UserMetadata::BIRTH_DATE) );
        $userData['Classe/Sezione'] = ArrayUtils::getIndex($class_grades, $this->userYear['classe']) . '/' . $this->userYear['sezione'];
        $userData['Plesso'] = StructureService::printFullStructureName($this->userYear["plesso_structure_id"]);
        ?>
        <ul class="dettaglio-studente">
            <?php foreach($userData as $label => $value){ ?>
            <li>
                <div class="columns small-3">
                    <?= _t($label) ?>
                </div>
                <div class="columns small-9">
                    <?= _t($value) ?>
                </div>
            </li>
            <?php } ?>
        </ul>
        <?php
    }

    public function remove(){
        $dropOutRow = DropOutService::findRow($this->id);
        if ($dropOutRow == null){
            redirectWithMessage($this->actionUrl("_default"), "Dati non validi");
        }

        if ($dropOutRow["user_year_document_id"] != $this->document["user_year_document_id"]){
            redirectWithMessage($this->actionUrl("_default"), "Dati non validi");
        }

        EM::updateEntity("drop_out_row", [
            "status" => RowStatus::DELETED
        ], [
            "drop_out_row_id" => $dropOutRow["drop_out_row_id"]
        ]);

        redirectWithMessage($this->actionUrl("_default"), "Azione eseguita");
    }

    public function edit(){
        $this->header();
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], $this->actionUrl("_default")));
        $this->bradcrumbs->add(new Bradcrumb("Segnalazione"));
        $this->editBody();
        $this->footer();
    }

    private function getStudentYearsOld(){
        $birthDateStr = $this->userMetadata["birth_date"];
        $birthDate = new DateTime($birthDateStr);
        $now = new DateTime();
        $diff = $now->diff($birthDate);
        $year = $diff->y;

        return $year;
    }

    public function editBody(){
        if ($this->currentYear == null){
            $this->currentYear = SchoolYearService::findCurrentYear();
        }
        
        $form = new Form($this->actionUrl("edit", ["id" => $this->id, "type" => $this->type]));
        $form->cancellAction = $this->actionUrl("_default");
        $form->entityName = "drop_out_row";
        $form->entity = DropOutService::findRow($this->id);

        if ($this->type == DropOutRowType::SEGNALAZIONE){
            $prevDropOutRow = DropOutService::findRowLastWithDocumentIdAndType($this->document["user_year_document_id"], $this->currentYear["school_year_id"], DropOutRowType::SEGNALAZIONE);

            $form->addField(new FormDateField("Data", "data_segnalazione", FormFieldType::DATE_TIME, true, date("d/m/Y")));

            $fieldGiorni = new FormTextField("Giorni di frequenza previsti", "giorni_frequenza", FormFieldType::NUMBER, true, 
                $prevDropOutRow != null ? $prevDropOutRow['giorni_frequenza'] : "0", 
                ["numeric" => 1]);
            $fieldGiorni->checkFunction = function($field, $v) use ($prevDropOutRow){
                if ($v <= 0){
                    $field->addFieldError("Inserire un valore maggiore di 0");
                }
            };
            $form->addField($fieldGiorni);

            $fieldAssenzaField = new FormTextField("Giorni di assenza (lezioni in presenza)", "giorni_assenza", FormFieldType::NUMBER, true, 
                $prevDropOutRow != null ? $prevDropOutRow['giorni_assenza'] : "0", ["numeric" => 1]);
            $form->addField($fieldAssenzaField);

            $fieldAssenzaDADField = new FormTextField("Giorni di assenza (lezioni DAD)", "giorni_assenza_dad", FormFieldType::NUMBER, true, 
                $prevDropOutRow != null ? $prevDropOutRow['giorni_assenza_dad'] : "0", ["numeric" => 1]);
            $form->addField($fieldAssenzaDADField);

            $form->checkFunction = function($form) use ($prevDropOutRow, $fieldAssenzaField, $fieldAssenzaDADField){
                if ($fieldAssenzaField->getValue(null) <= 0 && $fieldAssenzaDADField->getValue(null) <= 0){
                    $form->errors[] = "Inserire un valore almeno per un tipo di assenza.";
                }
            };

            $motiviAssenza = array_merge([new KeyValuePair("", "")], DictionaryService::findGroupAsKeyValuePair(Dictionary::DROP_OUT_MOTIVI, false, "dictionary_id"));
            $motiviAssenzaField = new FormSelectField("Motivazione presunta", "drop_out_assenza_motivazioni_dictionary_id", FormFieldType::NUMBER, $motiviAssenza, false);
            $form->addField($motiviAssenzaField);
        
            $actions = DictionaryService::findGroupAsKeyValuePair(Dictionary::DROP_OUT_AZIONI, false, "dictionary_id", [], true);
            $visibleActios = [];
            foreach ($actions as $action) {
                if ($action->key == DropOutAzioneKey::RIPRESA_FREQUENZA_DICTIONARY_ID){
                    continue;
                }
                $visibleActios[] = $action;
            }

            $azioneField = new FormSelectField("Azione intrapresa dalla scuola", "dro_out_row_type_dictionary_id", FormFieldType::NUMBER, $visibleActios, true);
            $azioneField->checkFunction = function($field, $value){
                $schoolGrade = substr($this->userYear["classe"], 1, strlen($this->userYear["classe"]));
                if ($schoolGrade == "I" || $schoolGrade == "P" || $schoolGrade == "S1") {
                    return;
                }

                if ($value == 488){
                    if (StringUtils::isNotBlank($this->userMetadata["birth_date"])){
                        $year = $this->getStudentYearsOld();
                        if ($year >= 16){
                            $field->addFieldError("Lo studente non è in età di obbligo scolastico. Non è possibile eseguire la segnalazione in procura.");
                        }
                    }
                }
            };

            $form->addField($azioneField);
            $form->addField(new FormTextField("Se altro specificare", "other", FormFieldType::STRING, false));
        } else if ($this->type == DropOutRowType::CHIUSURA_ANNO){
            $form->addField(new FormDateField("Data", "data_segnalazione", FormFieldType::DATE_TIME, true, date("d/m/Y")));

            $editoDictionary = DictionaryService::findGroupAsKeyValuePair(Dictionary::DROP_OUT_ESITO, false, "dictionary_id", [], true);
            $esitoField = new FormSelectField("Esito scolastico", "esito_dictionary_id", FormFieldType::NUMBER, $editoDictionary, true);
            $form->addField($esitoField);

            $nonAmmessoMotivazioniDictionary = DictionaryService::findGroupAsKeyValuePair(Dictionary::DROP_OUT_NON_AMMESSO_MOTIVAZIONI, false, "dictionary_id", [], true);
            $motivazioniField = new FormSelectField("Motivazioni", "non_ammesso_motivazioni_dictionary_id", FormFieldType::NUMBER, $nonAmmessoMotivazioniDictionary, true);
            $motivazioniField->checkFunction = function($field, $value) use ($esitoField){
                if ($esitoField->getValueForDb() == DropOutEsito::ammesso_classe_successiva_id){
                    $field->validationErrors = [];
                }
            };
            $form->addField($motivazioniField);

            $statoIscrizioneDictionary = DictionaryService::findGroupAsKeyValuePair(Dictionary::DROP_OUT_STATO_ISCRIZIONE, false, "dictionary_id", [], true);
            $statoIscrizioneField = new FormSelectField("Verifica iscrizione a.s. successivo", "stato_iscrizione_dictionary_id", FormFieldType::NUMBER, $statoIscrizioneDictionary, true);
            $form->addField($statoIscrizioneField);

            $excludeKeys = [];
            $year = $this->getStudentYearsOld();
            if ($year >= 16){
                $excludeKeys = [DropOutNonIscrittoMotivazioni::abbandonmo_in_obbligo_id];
            } else {
                $excludeKeys = [DropOutNonIscrittoMotivazioni::abbandonmo_id];
            }
            $statoIscrizioneDictionary = DictionaryService::findGroupAsKeyValuePair(Dictionary::DROP_OUT_NON_ISCRITTO_MOTIVAZIONI, false, "dictionary_id", $excludeKeys, true);
            $year = $this->getStudentYearsOld();
            $nonIscrittoField = new FormSelectField("Se non iscritto", "non_iscritto_motivazioni_dictionary_id", FormFieldType::NUMBER, $statoIscrizioneDictionary, true);
            $nonIscrittoField->checkFunction = function($field) use ($statoIscrizioneField){
                if ($statoIscrizioneField->getValueForDb() == DropOutStatoIscrizione::iscritto_id){
                    $field->validationErrors = [];
                }
            };
            $form->addField($nonIscrittoField);

            $statoIscrizioneDictionary = DictionaryService::findGroupAsKeyValuePair(Dictionary::CLASS_GRADE, false, "dictionary_id", [], true);
            $classeField = new FormSelectField("Classe", "iscritto_classe_dictionary_id", FormFieldType::NUMBER, $statoIscrizioneDictionary, true);
            $classeField->checkFunction = function($field) use ($statoIscrizioneField){
                if ($statoIscrizioneField->getValueForDb() == DropOutStatoIscrizione::non_iscritto_id){
                    $field->validationErrors = [];
                }
            };
            $form->addField($classeField);

            $statoIscrizioneDictionary = DictionaryService::findGroupAsKeyValuePair(Dictionary::DROP_OUT_ISCRITTO_SCUOLA, false, "dictionary_id", [], true);
            $scuolaField = new FormSelectField("Scuola", "iscritto_scuola_dictionary_id", FormFieldType::NUMBER, $statoIscrizioneDictionary, true);
            $scuolaField->checkFunction = function($field) use ($statoIscrizioneField){
                if ($statoIscrizioneField->getValueForDb() == DropOutStatoIscrizione::non_iscritto_id){
                    $field->validationErrors = [];
                }
            };
            $form->addField($scuolaField);

            $form->addField(new FormTextField("Se altro specificare", "other", FormFieldType::STRING, false));
        } else if ($this->type == DropOutRowType::RIPRESA_FREQUENZA){
            $form->addField(new FormDateField("Data ripresa frequenza", "data_segnalazione", FormFieldType::DATE_TIME, true, date("d/m/Y")));
        }

        $form->addField(new FormTextareaField("Note", "note", FormFieldType::STRING, false));

        if (!$this->editable){
            foreach($form->fields as $field){
                $field->readOnly = true;
            }
            $form->displaySubmit = false;
        }

        if ($form->isSubmit() && $form->checkFields()){
            $documentParams = [
                'last_edit_date' => "NOW()"
            ];

            if (StringUtils::isBlank($this->id)){
                $additionalFields = [
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser["user_id"],
                    "last_edit_date" => "NOW()",
                    "last_edit_user_id" => $this->currentUser["user_id"],
                    "user_year_document_id" => $this->document["user_year_document_id"],
                    "status" => RowStatus::READY,
                    "plesso_structure_id" => $this->userYear["plesso_structure_id"],
                    "school_year_id" => $this->currentYear["school_year_id"],
                    "type" => $this->type
                ];

                if ($this->type == DropOutRowType::CHIUSURA_ANNO){
                    $additionalFields["school_year_id"] = $this->document["school_year_id"];
                }

                $this->id = $form->persist(null, $additionalFields);

                $dropOutRow = DropOutService::findRow($this->id);
                $azione = DictionaryService::find($dropOutRow["dro_out_row_type_dictionary_id"]);
                if ($azione != null){
                    if ($azione["key"] == DropOutAzioneKey::SEGNALAZIONE_PROCURA){
                        DropOutService::setStatus($this->document["user_year_document_id"], DropOutStatus::RED);
                        $documentParams["drop_out_segnalazione_procura"] = 1;
                    } else if (
                        $azione["key"] == DropOutAzioneKey::LETTERA_FAMIGLIA ||
                        $azione["key"] == DropOutAzioneKey::CONVOCAZIONE_FAMIGLIA || 
                        $azione["key"] == DropOutAzioneKey::TELEFONATA_FAMIGLIA){
                        if ($this->document['drop_out_status'] == DropOutStatus::GREEN){
                            DropOutService::setStatus($this->document["user_year_document_id"], DropOutStatus::YELLOW);
                        }
                    } else {
                        DropOutService::setStatus($this->document["user_year_document_id"], DropOutStatus::GREEN);
                    }
                } else if ($this->type == DropOutRowType::RIPRESA_FREQUENZA){
                    DropOutService::setStatus($this->document["user_year_document_id"], DropOutStatus::GREEN);
                }

                $this->documentId = $this->document["user_year_document_id"];

                NoticeService::persist([
                    "type" => NoticeType::DROP_OUT_NEW_ROW,
                    "entity" => EntityName::DROP_OUT_ROW,
                    "entity_id" => $this->id,
                    "parameters" => json_encode($dropOutRow)
                ]);

                $this->_prepare();
            } else {
                $form->persist("drop_out_row_id=" . $this->id, [
                    "last_edit_date" => "NOW()",
                    "last_edit_user_id" => $this->currentUser["user_id"]
                ]);
            }

            if ($this->type == DropOutRowType::CHIUSURA_ANNO){
                if ($esitoField->getValueForDb() == DropOutEsito::ammesso_classe_successiva_id){
                    EM::updateEntity("drop_out_row", [
                        "non_ammesso_motivazioni_dictionary_id" => null
                    ], ["drop_out_row_id" => $this->id]);
                }

                if ($statoIscrizioneField->getValueForDb() == DropOutStatoIscrizione::iscritto_id){
                    EM::updateEntity("drop_out_row", [
                        "non_iscritto_motivazioni_dictionary_id" => null
                    ], ["drop_out_row_id" => $this->id]);
                } else {
                    EM::updateEntity("drop_out_row", [
                        "iscritto_classe_dictionary_id" => null,
                        "iscritto_scuola_dictionary_id" => null
                    ], ["drop_out_row_id" => $this->id]);
                }

                $updatedEntity = DropOutService::findRow($this->id);
                $documentParams['drop_out_esito_dictionary_id'] = $updatedEntity["esito_dictionary_id"];
                $documentParams['drop_out_non_ammesso_motivazioni_dictionary_id'] = $updatedEntity["non_ammesso_motivazioni_dictionary_id"];
                $documentParams['drop_out_non_iscritto_motivazioni_dictionary_id'] = $updatedEntity["non_iscritto_motivazioni_dictionary_id"];
                $documentParams['drop_out_stato_iscrizione_dictionary_id'] = $updatedEntity["stato_iscrizione_dictionary_id"];
            } else {
                if ($this->userYear["plesso_structure_id"] != null){
                    $documentParams['drop_out_plesso_structure_id'] = $this->userYear["plesso_structure_id"];
                }

                if ($this->userYear["classe"] != null){
                    $documentParams['drop_out_classe'] = $this->userYear["classe"];
                }

                if ($this->userYear["sezione"] != null){
                    $documentParams['drop_out_sezione'] = $this->userYear["sezione"];
                }
            }

            EM::updateEntity('user_year_document', 
                $documentParams, 
                [
                    "user_year_document_id" => $this->document["user_year_document_id"] 
                ]);

            $this->updateAges();
            
            redirectWithMessage($this->actionUrl("_default"), "Inserimento eseguito");
        }

        ?>
        <script type="text/javascript">
        $(function(){
            $('#dro_out_row_type_dictionary_id').on('change', onAzioneChange);
            onAzioneChange();

            $('#esito_dictionary_id').on('change', onEsitoChange);
            onEsitoChange();

            $('#stato_iscrizione_dictionary_id').on('change', onStatoIscrizioneChange);
            onStatoIscrizioneChange();
        });

        function onAzioneChange(){
            var azione = $('#dro_out_row_type_dictionary_id').val();
            $('#campo_form_other').hide();
            if (azione == "altro"){
                $('#campo_form_other').show();
            }
        }

        function onEsitoChange(){
            var value = $('#esito_dictionary_id').val();
            $('#campo_form_non_ammesso_motivazioni_dictionary_id').hide();
            if (value == 526){
                $('#campo_form_non_ammesso_motivazioni_dictionary_id').show();
            }
        }

        function onStatoIscrizioneChange(){
            var value = $('#stato_iscrizione_dictionary_id').val();

            $('#campo_form_non_iscritto_motivazioni_dictionary_id').hide();
            $('#campo_form_iscritto_classe_dictionary_id').hide();
            $('#campo_form_iscritto_scuola_dictionary_id').hide();

            if (value == 527){
                $('#campo_form_iscritto_classe_dictionary_id').show();
                $('#campo_form_iscritto_scuola_dictionary_id').show();
            } else if (value == 528){
                $('#campo_form_non_iscritto_motivazioni_dictionary_id').show();
            }

            console.log(value);
        }
        </script>
        <div class="content active tab-nuovo-studente row" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php 
                if ($this->bradcrumbs != null){
                    $this->bradcrumbs->draw(); 
                }
                ?>
                <div class="medium-12 columns">
                    <h3>
                    <?php if ($this->type == DropOutRowType::SEGNALAZIONE){ ?>
                    Segnalazione
                    <?php } else if ($this->type == DropOutRowType::RIPRESA_FREQUENZA) { ?>
                    Ripresa frequenza scolastica
                    <?php } else { ?>
                    Chiusura anno scolastico
                    <?php } ?>
                    </h3>
                </div>
            </div>
            <div class="sezioni">
                <?= $form->draw(); ?>
            </div>
        </div>
        <?php
    }

    private function updateAges(){
        $metas = MetadataService::findMetadataValue(MetadataEntity::USER, $this->user["user_id"], UserMetadata::BIRTH_DATE);
        if (count($metas) == 0){
            return;
        }

        $birthDateMeta = $metas[0];
        $birthDateStr = EncryptService::decrypt($birthDateMeta["value"]);
        if (StringUtils::isBlank($birthDateStr)){
            return "";
        }
        
        $birthDate = new DateTime($birthDateStr);
        $now = new DateTime();
        $diff = $now->diff($birthDate);
        $age = $diff->y;
        EM::updateEntity("user_year_document", ["drop_out_age" => $age], ["user_year_document_id" => $this->document["user_year_document_id"]]);
    }
 }