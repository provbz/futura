<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class UserAction extends StructureUserDocumentAction {
    
    public $id;
    public $hide;
    public $type;
    public $empty = false;
    public $clone = false;
    public $method;
    
    public function _default(){
        $this->setGlobalSessionValue("backUrl", NULL);
        $this->setGlobalSessionValue('selectedPeiRowId', NULL);
        $this->setGlobalSessionValue("PeiAction_pei", NULL);
        $this->setGlobalSessionValue("PdpProfileAction_selectedGroup", NULL);
        
        $pei = $this->document;
        if ($pei["type"] != DocumentType::PEI && 
            $pei["type"] != DocumentType::PEI_TIROCINIO &&
            $pei["type"] != DocumentType::PDP && 
            $pei["type"] != DocumentType::PDP_LINGUA ){
            redirect(UriService::accessDanyActionUrl());
        }

        $replaceDocuments = DocumentService::findReplacedDocuments($this->document["user_year_document_id"])->fetchAll();

        $this->header();
        ?>
        <div class="content active tab-studente row-100" id="panelTabStudenti">
            <div class="section-title row-100">
                <div class="large-8 medium-8 columns">
                    <h3><?= _t(DocumentService::getDocumentName ($this->documentType) ) ?> - <?= _t($this->user['code']) ?></h3>
                </div>
                <div class="large-4 medium-4 columns">
                    <?php if(UserRoleService::canCurrentUserDo("pei_remove") &&
                        ($this->document["replaced_by_document_id"] == null || count($replaceDocuments) == 0)){ ?>
                        <a class="button radius alert tiny float_right with-itocn"
                            onclick="return confirm('Rimuovere l\'utente e tutti i dati associati?')" 
                            href="<?= UriService::buildPageUrl("/structure/PEIPDPBaseAction", "remove", ["documentId" => _t( $pei['user_year_document_id'] ) ]) ?>">
                            <i class="<?= Icon::TRASH ?>"></i>Rimuovi
                        </a>
                    <?php } ?>
                </div>
            </div>
            
            <?php if (count($replaceDocuments) > 0){ ?>
                <div class="alert alert-danger my-5">
                    Questo documento sostituisce i seguenti documenti: <br/>
                    <ul>
                        <?php foreach($replaceDocuments as $replaceDocument){ ?>
                            <li>
                            <a href="<?= UriService::buildPageUrl("/structure/UserAction", "_default", ["documentId" => $replaceDocument["user_year_document_id"]]) ?>">
                                <?= DocumentService::getDocumentName($replaceDocument["type"]) ?> - <?= $replaceDocument["user_year_document_id"] ?>
                            </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>

            <?php if ($this->documentType == DocumentType::PEI && (!isset($this->userMetadata["family_note"]) || StringUtils::isBlank( $this->userMetadata["family_note"] )) ){ ?>
            <div class="alert alert-danger my-5">
                Si ricorda di compilare il quadro informativo famiglia nell'anagrafica studente.
            </div>
            <?php } ?>

            <div class="areagrigia">
                <div class="table-row row-100">
                    <div class="large-12 medium-12 small-12 columns">
                        <?php $this->drawUserData(); ?>
                    </div>
                </div>

                <div class="section-title row-100">
                    <div class="large-12 medium-12 columns student-buttons text-center">                
                        <a class="button radius tiny with-itoc my-5" href="<?= UriService::buildPageUrl("/structure/PEIPDPBaseAction", 'newUserFull', ['documentId' => $this->document['user_year_document_id']]) ?>">
                            <i class="<?= Icon::PENCIL ?>"></i>Anagrafica
                        </a>

                        <?php if ($this->documentType == DocumentType::PDP_LINGUA) { ?>
                            <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pdp_lingua/SituazionePartenzaAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                                <i class="fa fa-calendar"></i> Situazione partenza
                            </a>

                            <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pdp_lingua/CompetenzeLinguisticheAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                                <i class="fa fa-calendar"></i> Competenze linguistiche
                            </a>

                            <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pei/PlanAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                                <i class="fa fa-calendar"></i> Calendario
                            </a>

                            <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pei/PattoAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                                <i class="fas fa-align-justify"></i> Patto
                            </a>
                        <?php } ?>

                        <?php if ($this->documentType == DocumentType::PEI_TIROCINIO){ ?>
                            <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pei_tirocinio/PropostaProgettoAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                                style="width: 150px;">
                                <i class="fas fa-book"></i> Proposta di progetto
                            </a>    
                            <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pei_tirocinio/AziendaAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                                style="width: 150px;">
                                <i class="fas fa-building"></i> Aziende
                            </a>

                            <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pei/PlanAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                                <i class="fa fa-calendar"></i> Calendario
                            </a>

                            <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pei/PattoAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                                <i class="fas fa-align-justify"></i> Patto
                            </a>
                        <?php } ?>
                        
                        <?php if ($this->documentType == DocumentType::PEI && StringUtils::isNotBlank($this->userYear['classe']) && strstr($this->userYear['classe'], "S2") !== false) { ?>
                            <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pei/PctoAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                                <i class="fas fa-align-justify"></i> PCTO/STAGE
                            </a>
                        <?php } ?>

                        <?php if ($this->documentType == DocumentType::PEI || $this->documentType == DocumentType::PDP){ ?>
                            <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pei/PattoAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                                <i class="fas fa-align-justify"></i> Patto
                            </a>

                            <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pei/PlanAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                                <i class="fa fa-calendar"></i> Calendario
                            </a>
                        <?php } ?>

                        <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/pei/UserAttachmentAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                            <i class="fas fa-paperclip"></i> Allegati
                        </a>
                        <a class="button radius tiny with-itocn" my-5 href="<?= UriService::buildPageUrl("/structure/pei/ShareAction", null, ['documentId' => $this->document['user_year_document_id']]) ?>"
                            <i class="fas fa-share-alt"></i> Condividi
                        </a>
                        
                        <?php if(UserRoleService::canCurrentUserDo("/structure/user/UserTransferAction")){ ?>
                        <a class="button radius tiny with-itocn my-5" href="<?= UriService::buildPageUrl("/structure/user/UserTransferAction", "", [
                            "userId" => $this->user["user_id"],
                            'documentId' => $this->document['user_year_document_id']
                            ]) ?>">
                            <i class="fas fa-arrow-right"></i> Invia dati
                        </a>
                        <?php } ?>
                    </div>
                </div>
         
                <?php
                if ($pei != NULL){
                    if ($this->documentType == DocumentType::PEI){
                        ?>
                        <div class="row pianodidattico">
                            <div class="medium-6 columns">
                                <a href="<?= UriService::buildPageUrl("/structure/pei/PeiAction", "details", ['documentId' => $pei['user_year_document_id']]) ?>" class="button success radius with-itocn">
                                    <i class="<?= Icon::PENCIL ?> margin-right"></i>ELABORA</a>
                                <p>Modifica, personalizza, integra o elimina ambiti di lavoro, obiettivi e attività.</p>
                            </div>
                            <div class="medium-6 columns">
                                <a href="<?= UriService::buildPageUrl("/structure/pei/PeiViewAction", '', ['documentId' => $pei['user_year_document_id'],'code' => $this->user['code']]) ?>" class="button success radius with-itocn">
                                    <i class="fa fa-lg fa-print margin-right"></i>VISUALIZZA E SCARICA
                                </a>
                                <p>Visualizza, esporta e stampa il PEI.</p>
                            </div>
                        </div>        
                        <?php 
                    } else if ($this->documentType == DocumentType::PEI_TIROCINIO){
                        ?>
                        <div class="row pianodidattico">
                            <div class="medium-6 columns">
                                <a href="<?= UriService::buildPageUrl("/structure/pei/PeiAction", "details", ['documentId' => $pei['user_year_document_id']]) ?>" class="button success radius with-itocn">
                                    <i class="<?= Icon::PENCIL ?> margin-right"></i>ELABORA</a>
                                <p>Modifica, personalizza, integra o elimina ambiti di lavoro, obiettivi e attività.</p>
                            </div>
                            <div class="medium-6 columns">
                                <a href="<?= UriService::buildPageUrl("/structure/pei_tirocinio/ViewAction", '', ['documentId' => $pei['user_year_document_id'],'code' => $this->user['code']]) ?>" class="button success radius with-itocn">
                                    <i class="fa fa-lg fa-print margin-right"></i>VISUALIZZA E SCARICA
                                </a>
                                <p>Visualizza, esporta e stampa il PEI.</p>
                            </div>
                        </div>        
                        <?php 
                    } else if ($this->documentType == DocumentType::PDP){
                        ?>
                        <div class="row pianodidattico">
                            <div class="large-6 medium-6 columns">
                                <a href="<?= UriService::buildPageUrl("/structure/pei/PeiAction", "details", ['documentId' => $pei['user_year_document_id']]) ?>" class="button radius">
                                    <i class="fa fa-lg fa-puzzle-piece margin-right"></i>ELABORA</a>
                                <p>
                                    Analisi della situazione attuale nelle singole aree e
                                    individuazione delle relative possibilità di sviluppo
                                </p>
                            </div>
                            <div class="large-6 medium-6 columns">
                                <a href="<?= UriService::buildPageUrl("/structure/pdp/PdpViewAction", "", ['documentId' => $pei['user_year_document_id'],'code' => $this->user['code']]) ?>" class="button radius">
                                    <i class="fa fa-lg fa-print margin-right"></i>VISUALIZZA E SCARICA
                                </a>
                                <p><p>Visualizza e salva il PDP creato</p>
                            </div>
                        </div>          
                        <?php
                    } else if ($this->documentType == DocumentType::PDP_LINGUA){
                        ?>
                        <div class="row pianodidattico">
                            <div class="large-6 medium-6 columns">
                                <a href="<?= UriService::buildPageUrl("/structure/pei/PeiAction", "details", ['documentId' => $pei['user_year_document_id']]) ?>" class="button radius">
                                    <i class="fa fa-lg fa-puzzle-piece margin-right"></i>ELABORA</a>
                                <p>
                                    Analisi della situazione attuale nelle singole aree e
                                    individuazione delle relative possibilità di sviluppo
                                </p>
                            </div>
                            <div class="large-6 medium-6 columns">
                                <a href="<?= UriService::buildPageUrl("/structure/pdp_lingua/ViewAction", "", ['documentId' => $pei['user_year_document_id'],'code' => $this->user['code']]) ?>" class="button radius">
                                    <i class="fa fa-lg fa-print margin-right"></i>VISUALIZZA E SCARICA
                                </a>
                                <p><p>Visualizza e salva il PDP creato</p>
                            </div>
                        </div>          
                        <?php
                    }
                } 
                ?>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    public function drawUserData(){
        $genders = DictionaryService::findGroupAsDictionary(Dictionary::GENDER);
        $class_grades = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        
        $userData['Identificativo'] = $this->structureId . '-' . $this->user['user_id'] .'-'. $this->userYear['user_year_id'];
        $userData['Codice'] = $this->user['code'];
        $userData['Data di nascita'] = TextService::formatDate( ReflectionUtils::getValue($this->userMetadata, UserMetadata::BIRTH_DATE) );
        $userData['Classe/Sezione'] = ArrayUtils::getIndex($class_grades, $this->userYear['classe']) . '/' . $this->userYear['sezione'];
        $userData['Plesso'] = StructureService::printFullStructureName($this->userYear["plesso_structure_id"]);
        $userData["Ultima modifica"] = TextService::formatDate($this->document["last_edit_date"]);

        if(UserRoleService::canCurrentUserDo(UserPermission::LAST_EDIT_USER_VIEW)){
            $user = UserService::find($this->document["last_edit_user_id"]);
            if ($user != null){
                $userData["Modificato da"] = $user["name"] . ' ' . $user["surname"];
            }
        }

        ?>
        <ul class="dettaglio-studente">
            <?php foreach($userData as $label => $value){ ?>
            <li>
                <div class="columns small-3">
                    <?= _t($label) ?>
                </div>
                <div class="columns small-9">
                    <?= _t($value) ?>
                </div>
            </li>
            <?php } ?>
        </ul>
        <?php
    }
}
