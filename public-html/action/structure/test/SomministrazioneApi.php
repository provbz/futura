<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once __DIR__ . '/../../../model/api/ApiResponse.php';

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SomministrazioneApi extends StructureAction {

    private $request;

    public function _prepare(){
        parent::_prepare();
        $json = file_get_contents("php://input");
        $this->request = json_decode($json, true);
    }

    private function checkStructureReferentPermissions(){
        if (!UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_REFERENTI_PLESSO_ADD)){
            echo json_encode( new ApiResponse(
                false,
                "No permission"
            ));    
            die();
        }

        if (!isset($this->request["userId"]) || !isset($this->request["structureId"])){
            echo json_encode( new ApiResponse(
                false,
                "Invalid data"
            ));    
            die();
        }

        $user = UserService::find($this->request["userId"]);
        if ($user == null){
            echo json_encode( new ApiResponse(
                false,
                "Invalid user"
            ));    
            die();
        }

        $userStructure = StructureService::findUserStructure($this->request["userId"], $this->structure["structure_id"]);
        if ($userStructure == null){
            echo json_encode( new ApiResponse(
                false,
                "Invalid user"
            ));    
            die();
        }

        $structure = StructureService::find($this->request["structureId"]);
        if ($structure == null){
            echo json_encode( new ApiResponse(
                false,
                "Invalid structure"
            ));    
            die();
        }

        if ($structure["parent_structure_id"] != $this->structure["structure_id"]){
            echo json_encode( new ApiResponse(
                false,
                "Invalid structure"
            ));    
            die();
        }
    }

    public function addUserToStructureReferent(){
        $this->checkStructureReferentPermissions();

        $userStructure = StructureService::findUserStructure($this->request["userId"], $this->request["structureId"]);
        if ($userStructure == null){
            StructureService::addUserStructure($this->request["userId"], $this->request["structureId"], null, false);
            $userStructure = StructureService::findUserStructure($this->request["userId"], $this->request["structureId"]);
        }

        $userStructureRole = StructureService::findUserStructureForUserStructureAndRole($this->request["userId"],
            $this->request["structureId"],
            UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI);
        if ($userStructureRole != null){
            echo json_encode( new ApiResponse(
                true
            ));    
            die();
        }

        StructureService::addUserStructureRole($userStructure["user_structure_id"], UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI);
        
        echo json_encode( new ApiResponse(
            true
        ));    
    }

    public function removeUserFromStructureReferent(){
        $this->checkStructureReferentPermissions();

        StructureService::removeUserStructureRole($this->request["userId"], $this->request["structureId"], UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI);
        
        echo json_encode( new ApiResponse(
            true
        ));    
    }

    private function checkStructureTeacherPermissions(){
    }

    public function addUserToStructureClassTeacher(){
        $this->checkStructureTeacherPermissions();

        StructureClassUserService::insert($this->request["structureClassId"], 
            $this->request["userId"],
            UserRole::INSEGNANTE_SOMMINISTRAZIONI
        );

        echo json_encode( new ApiResponse(
            true
        ));    
    }

    public function removeUserFromStructureClassTeacher(){
        $this->checkStructureTeacherPermissions();

        StructureClassUserService::remove($this->request["structureClassId"], $this->request["userId"]);
        
        echo json_encode( new ApiResponse(
            true
        ));    
    }
}