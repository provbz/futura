<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SomministrazioneBaseAction extends StructureAction {

    public $id;
    public $somministrazione;
    public $status;
    public $somministrazioneTests;
    public $currentYear;

    public function _prepare(){
        parent::_prepare();

        $this->currentYear = SchoolYearService::findCurrentYear();
        
        $this->somministrazione = SomministrazioneService::find($this->id);
        if ($this->somministrazione == null || $this->somministrazione["row_status"] != RowStatus::READY){
            redirectWithMessage(UriService::buildPageUrl("/structure/test/SomministraziuoniAction"), "Dati non validi");
        }

        $this->status = SomministrazioneService::getStatus($this->somministrazione);
        $this->somministrazioneTests = SomministrazioneService::findAllTestsForSomministrazione($this->id);
        $this->somministrazioneTests = $this->somministrazioneTests->fetchAll();
        for($i = 0; $i < count($this->somministrazioneTests); $i++){ 
            $this->somministrazioneTests[$i]["test"] = TestService::find($this->somministrazioneTests[$i]["test_id"]);
        }
    }

    protected function drawSomministrazioneDetails(){
        $class = DictionaryService::findByGroupAndKey(Dictionary::CLASS_GRADE, $this->somministrazione["class_dictionary_value"]);
        $periodo = DictionaryService::findByGroupAndKey(Dictionary::TEST_PERIODO, $this->somministrazione["periodo_dictionary_value"]);
        $attachments = AttachmentService::findAllForEntityInMV(EntityName::SOMMINISTRAZIONE, $this->somministrazione["somministrazione_id"], AttachmentType::SOMMINISTRAZIONE_MATERIALI);

        ?>
        <div class="somministrazione-status <?= $this->status ?>">
        <?php
            switch($this->status){
                case SomministrazioneStatus::CLOSED: 
                    echo 'Prova chiusa';
                    break;
                case SomministrazioneStatus::WAITING: 
                    echo 'In attesa di apertura';
                    break;
                case SomministrazioneStatus::OPEN: 
                    echo 'Prova aperta';
                    break;
            }
            ?>
        </div>
        <style type="text/css">
        .materiali{
            word-break: break-all;
            overflow: hidden;
        }
        .materiali .materiale {
            margin: 15px 0px 7px 0px;
            display: block;
            font-size: 0.9rem;
        }
        .materiali .materiale i {
            font-size: 26px;
            float: left;
        }
        .materiali .materiale a {
            margin-left: 29px;
            display: block;
        }
        .test-title{
            padding-left: 10px;
            border-bottom: 1px solid grey;
            margin-bottom: 17px;
        }
        </style>

        <div class="somministrazione-info">
            <div class="row">
                <div class="columns small-3">Anno scolastico:</div>
                <div class="columns small-9"><?php 
                    $schoolYear = SchoolYearService::find($this->somministrazione["school_year_id"]);
                    if ($schoolYear != null){
                        echo $schoolYear["school_year"];
                    }
                ?></div>
            </div>

            <div class="row">
                <div class="columns small-3">Apertura:</div>
                <div class="columns small-9"><?= DateUtils::convertDate( DateUtils::getDate($this->somministrazione["data_inizio"]) ) ?></div>
            </div>
        
            <div class="row">
                <div class="columns small-3">Chiusura:</div>
                <div class="columns small-9"><?= DateUtils::convertDate( DateUtils::getDate($this->somministrazione["data_fine"]) ) ?></div>
            </div>
            <div class="row">
                <div class="columns small-3">Classe:</div>
                <div class="columns small-9"><?= _t($class["value"]) ?></div>
            </div>
            <div class="row">
                <div class="columns small-3">Periodo:</div>
                <div class="columns small-9"><?= _t($periodo["value"]) ?></div>
            </div>
            <?php if (count($attachments) > 0) { ?> 
            <div class="row">
                <div class="columns small-12">Materiali:</div>
                <div class="columns small-12 materiali">
                    <?php foreach ($attachments  as $file) {?>
                        <div class="materiale">
                            <i class="fas fa-file"></i>
                            <a href="<?= $file->Url ?>" target="_blank">
                                <?= _t($file->FileName) ?>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        </div>
        
        <?php if (count($this->somministrazioneTests) > 0){ ?> 
            <div class="test-title row">
                <H4>Test</H4>
            </div>
            <?php foreach($this->somministrazioneTests as $somministrazioneTest){ ?>
            <div class="somministrazione-test">
                <h5><?= _t($somministrazioneTest["test"]["name"]) ?></h5>
                <?= $this->drawAttachments("Istruzioni", EntityName::TEST, $somministrazioneTest["test_id"], AttachmentType::TEST_ISTRUZIONI); ?>
                <?= $this->drawAttachments("Materiali", EntityName::TEST, $somministrazioneTest["test_id"], AttachmentType::TEST_MATERIALI); ?>
                <?= $this->drawAttachments("Materiali somministrazione", EntityName::SOMMINISTRAZIONE_TEST, $somministrazioneTest["somministrazione_test_id"], AttachmentType::SOMMINISTRAZIONE_TEST_MATERIALI); ?>
            </div>
            <?php 
            } 
        }
    }

    protected function drawAttachments($label, $entity, $entityId, $type){
        $attachments = AttachmentService::findAllForEntityInMV($entity, $entityId, $type);
        if (count($attachments) == 0){
            return;
        }
        ?>
        <div class="test-files materiali">
            <div class="test-file-title"><?= _t($label) ?></div>
        <?php foreach($attachments as $file){ ?>
            <div class="materiale">
                <i class="fas fa-file"></i>
                <a href="<?= $file->Url ?>" target="_blank">
                    <?= _t($file->FileName) ?>
                </a>
            </div>
        <?php } ?>
        </div>
        <?php
    }

}