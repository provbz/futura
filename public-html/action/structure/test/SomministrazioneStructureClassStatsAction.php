<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once dirname(__FILE__) .  '/SomministrazioneStructureClassDetailsAction.php';

/**
 * Description of SomministrazioneClassStatsAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class SomministrazioneStructureClassStatsAction extends SomministrazioneStructureClassDetailsAction {

    public $actionParameters;

    public function _default(){
        $this->actionParameters = [
            "id" => $this->somministrazione["somministrazione_id"],
            "classId" => $this->classId
        ];
        $this->backAction = UriService::buildPageUrl("/structure/test/SomministrazioneStructureClassDetailsAction", "", $this->actionParameters);

        $subStructure = StructureService::find($this->class["structure_id"]);
        if ($subStructure == null || $subStructure["parent_structure_id"] != $this->structure["structure_id"]){
            redirectWithMessage(UriService::buildPageUrl("/structure/test/SomministrazioneAction", "", [
                "id" => $this->id
            ]), "Dati non validi");
        }
        $classDictionary = DictionaryService::findByGroupAndKey(Dictionary::CLASS_GRADE, $this->somministrazione["class_dictionary_value"]);
        $this->class["name"] = $this->structure["name"] . ' - ' . $subStructure["name"] . ' - ' . $classDictionary["value"] .'/'. $this->class["section"];
        $this->bradcrumbs->add(new Bradcrumb("Plessi", UriService::buildPageUrl("/structure/test/SomministrazioneAction?id=" .$this->id)));
        $this->bradcrumbs->add(new Bradcrumb("Classe", $this->backAction));
        $this->bradcrumbs->add(new Bradcrumb("Statistiche"));

        $this->header();
        ?>
        <div class="content active tab-studente row-100 somministrazione">
            <div class="section-title row-100">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-8 medium-7 small-7 columns">
                    <h3>Prove</h3>
                </div>
                <div class="large-4 medium-3 small-5 columns buttontoolbar">
                </div>
            </div>
            <div class="columns small-2 right-column">
                <?php $this->drawSomministrazioneDetails(); ?>
            </div>
            <div class="columns small-10">
                <div class="mb-somministrazione-class-stats-cnt"></div>
                <script type="text/javascript">
                <?php
                $widgetParameters = [
                    "somministrazioneId" => $this->id,
                    "classId" => $this->classId,
                    "selectedClass" => $this->class
                ]
                ?>
                MbCreateVue("mb-somministrazione-class-stats-table", "mb-somministrazione-class-stats-cnt", <?php echo json_encode($widgetParameters) ?>)
                </script>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function loadData(){
        $classUsers = StructureClassUserService::FindWithRole($this->classId, UserRole::STUDENTE);
        $testsData = $this->loadTestData($this->somministrazione["somministrazione_id"]);        
        
        $tableData = [];
        foreach ($classUsers as $classUser) {
            $row = [];
            $row["class_user"] = [
                "name" => $classUser["name"],
                "surname" => $classUser["surname"],
                "code" => $classUser["code"],
                "user_id" => $classUser["user_id"],
                "structure_class_user_id" => $classUser["structure_class_user_id"]
            ];
            $row["tests"] = []; 

            foreach ($testsData as $test) {
                $userResultDb = SomministrazioneStatsService::findUserResult($classUser["user_id"], $test["test"]["somministrazione_test_id"]);
                $userResult["status"] = null;                
                if ($userResultDb != null){
                    $userResult["status"] = $userResultDb["status"];
                }

                $userResultVarsDb = SomministrazioneStatsService::findUserResultVars($classUser["user_id"], $test["test"]["somministrazione_test_id"]);
                $variables = [];
                foreach ($userResultVarsDb as $variable) {
                    $variables[$variable['variable_name']] = $variable;
                }
                $userResult["variables"] = $variables;
                
                $row["tests"][$test["test"]["test_id"]] = $userResult;
            }
            
            $tableData[] = $row;
        }

        header('Content-Type: application/json');
        $response = new ApiResponse([
            "test_data" => $testsData,
            "table" => $tableData
        ]);
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    private function loadTestData($somministrazioneId){
        $tests = SomministrazioneService::findAllTestsForSomministrazione($this->id);

        $results = [];
        foreach($tests as $test){
            $testFile = TestService::loadTest($test["code"]);

            $row = [
                "test" => $test,
                "variables" => $testFile->getVariablesNames()
            ];

            $results[] = $row;
        }

        return $results;
    }

    public function remove(){
        if (!UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_CLASSE_ADD)){
            redirect(UriService::accessDanyActionUrl());
        }

        EM::updateEntity("structure_class", [
            "row_status" => RowStatus::DELETED
        ], [
            "structure_class_id" => $this->classId
        ]);
        
        redirect(UriService::buildPageUrl("/structure/test/SomministrazioneAction", "", [
            "id" => $this->id
        ]));
    }

}