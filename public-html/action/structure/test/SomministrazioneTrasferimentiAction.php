<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */


class SomministrazioneTrasferimentiAction extends StructureAction{

    public $utrId;

    public function _default(){
        $this->header();
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row-100">
                    <div class="large-8 medium-7 small-7 columns">
                        <h3>Mondo delle parole e progetto letto scrittura: Trasferimenti</h3>
                    </div>
                </div>
                <?php
                $this->drawUserTransferRequests(UserTransferRequestType::FROM_SOURCE);
                $this->drawUserTransferRequests(UserTransferRequestType::FROM_DESTINATION);        
                ?>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function drawUserTransferRequests($type){
        $table = new Table();
        $table->entityName = "user_transfer_request utr";
        $table->addSelect("*, s.name as s_name, ru.email as ru_email, u.code as u_code, u.name as u_name, u.surname as u_surname, s2.name as s2_name ");
        $table->addJoin("LEFT JOIN structure s ON s.structure_id=utr.to_structure_id");
        $table->addJoin("LEFT JOIN structure s2 ON s2.structure_id=utr.from_structure_id");
        $table->addJoin("LEFT JOIN user u ON u.user_id=utr.user_id");
        $table->addJoin("LEFT JOIN user ru ON ru.user_id=utr.requester_user_id");
        if ($type == UserTransferRequestType::FROM_DESTINATION){
            $table->addWhere("from_structure_id=" . $this->structureId);
        } else {
            $table->addWhere("to_structure_id=" . $this->structureId);
        }
        
        $table->addWhere(sprintf("u.section='%s'", UserSection::MONDO_PAROLE));
        $table->addWhere(sprintf("utr.status='%s'", UserTransferService::STATUS_PENDING));
        $table->addWhere(sprintf("utr.type='%s'", $type));
        $table->addOrder("utr.insert_date");
        $table->addColumn(new TableColumn("user_transfer_request_id", "ID"));
        $table->addColumn(new TableColumn("insert_date", "Data"));
        $table->addColumn(new TableColumn("u_code", "Codice studente"));
        $table->addColumn(new TableColumn("u_name", "Nome"));
        $table->addColumn(new TableColumn("u_surname", "Cognome"));
        
        if ($type == UserTransferRequestType::FROM_DESTINATION){
            $table->addColumn(new TableColumn("s_name", "Dalla scuola"));
            $table->addColumn(new TableColumn("ru_email", "e-mail richiedente"));    
        } else {
            $table->addColumn(new TableColumn("s2_name", "Dalla scuola"));
            $table->addColumn(new TableColumn("ru_email", "e-mail utente invio"));
        }
        $table->addColumn(new TableColumn("user_transfer_request_id", "", function($v, $row) use ($type){
            ob_start();
            if ($type == UserTransferRequestType::FROM_DESTINATION){ ?>
                <a href="<?= $this->actionUrl("manage", ['utrId' => $v]) ?>" class="button tiny">Gestione</a>
            <?php } else { ?>
                <a href="<?= $this->actionUrl("manage", ['utrId' => $v]) ?>" class="button tiny">
                    Completa
                </a>
            <?php }
            return ob_get_clean();
        }));
        
        ?>
        <div class="content active" id="panelTabStudenti">
            <div class="section-title row-100">
                <div class="large-12 medium-12 columns">
                    <h3>
                        <?php if ($type == UserTransferRequestType::FROM_DESTINATION){ ?>
                            Richieste di trasferimento verso altri istituti
                        <?php } else { ?>
                            Avvisi di trasferimento da altri istituti
                        <?php } ?>
                    </h3>
                </div>
            </div>
            <div class="section row-100">
                <p>
                    <?php if ($type == UserTransferRequestType::FROM_DESTINATION){ ?>
                        In questa tabella sono elencate le richieste di trasferimento degli studenti da parte di altri istituti.
                        Approvando le richieste tutti i dati dello studente potranno essere trasferiti al nuovo Istituco Comprensivo.
                    <?php } else { ?>
                        Nella tabella sono riportati gli avvisi di trasferimento inseriti da altri istituti.<br/>
                        Per completare il trasferimento è necessario inserire la classe di destinazione.
                    <?php } ?>
                </p>
                
                <?= $table->draw() ?>
            </div>
        </div>
        <?php
    }
    
    public function manage(){
        $_SESSION['utrId'] = $this->utrId;
        $userRequest = UserTransferService::find($this->utrId);
        if ($userRequest == NULL){
            redirect(UriService::accessDanyActionUrl());
        }
        
        if ($userRequest["status"] != UserTransferService::STATUS_PENDING){
            redirect(UriService::accessDanyActionUrl());
        }

        $user = UserService::find($userRequest["user_id"]);
        if ($user == null || $user['section'] != UserSection::MONDO_PAROLE){
            redirect(UriService::accessDanyActionUrl());
        }
        
        if ($userRequest["type"] == UserTransferRequestType::FROM_DESTINATION && $userRequest['from_structure_id'] == $this->structureId){
            $this->manageFromDestination($userRequest);
        } else if ($userRequest["type"] == UserTransferRequestType::FROM_SOURCE && $userRequest['to_structure_id'] == $this->structureId){
            $this->manageFromSource($userRequest);
        } else {
            redirect(UriService::accessDanyActionUrl());
        }
    }
    
    private function manageFromSource($userRequest){
        $currentSchoolYear = SchoolYearService::findCurrentYear();

        $user = UserService::find($userRequest["user_id"]);
        $userYear = UserYearService::findUserYearByUser($userRequest["user_id"]);
        $userYearDocuments = DocumentService::findForUserAndYearAndType($userRequest["user_id"], $currentSchoolYear["school_year_id"]);
        $userYearDocument = $userYearDocuments->fetch();

        $form = new Form($this->actionUrl("manage", ["utrId" => $this->utrId]));
        $form->entityName = "user_transfer_request";
        $form->entity = $userRequest;
        $form->cancellAction = $this->actionUrl("_default");
        
        $codeField = new FormTextField("Codice richeista", "user_transfer_request_id", FormFieldType::STRING, false);
        $codeField->persist = false;
        $codeField->readOnly = true;
        $form->addField($codeField);
        
        $user = UserService::find($userRequest["user_id"]);
        
        $codeField = new FormTextField("Nome", "name", FormFieldType::STRING, false, $user['name']);
        $codeField->persist = false;
        $codeField->readOnly = true;
        $form->addField($codeField);

        $codeField = new FormTextField("Cognome", "surname", FormFieldType::STRING, false, $user['surname']);
        $codeField->persist = false;
        $codeField->readOnly = true;
        $form->addField($codeField);

        $codeField = new FormTextField("Data nascita", "birth_date", FormFieldType::STRING, false, $user['birth_date']);
        $codeField->persist = false;
        $codeField->readOnly = true;
        $form->addField($codeField);

        $classess = $this->findClassi();
        $classeField = new FormSelectField("Classe", "status", FormFieldType::NUMBER, $classess, true);
        $form->addField($classeField);

        if ($form->isSubmit() && $form->checkFields()){
            EM::updateEntity("", [
                "to_class_id" => $classeField->getValue(null)
            ], [
                "user_transfer_request_id" => $this->utrId
            ]);

            UserTransferService::setStatus($this->utrId, UserTransferService::STATUS_APPROVED, null);
            redirect($this->actionUrl("_default"));
        }

        $this->bradcrumbs->add(new Bradcrumb("Trasferimenti", $this->actionUrl("_default")));
        $this->bradcrumbs->add(new Bradcrumb("Richiesta di trasferimento " . $userRequest["user_transfer_request_id"]));
        $this->header();
        ?>
        <div class="tabs-content">             
            <div class="content active" id="panelTabStudenti">
                <div class="section-title row">
                    <?php $this->bradcrumbs->draw(); ?>
                    <div class="large-12 medium-12 columns">
                        <h3>Inserire i dati per completare il trasferimento</h3>
                    </div>
                </div>
                <div class="sezioni row">
                    <?= $form->draw() ?>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }

    private function findClassi(){
        $rows = EM::execQuery("SELECT s.name, sc.structure_class_id, sc.class_dictionary_value, dc.value, sc.section
            from structure_class sc
            left join structure s ON sc.structure_id=s.structure_id
            left join dictionary dc ON dc.key = sc.class_dictionary_value AND dc.group='class'
            left join school_year sy ON sc.school_year_id=sy.school_year_id
            where s.parent_structure_id = :structure_id
                and sy.current = 1
            order by s.name, sc.class_dictionary_value, sc.section", [
                "structure_id" => $this->structureId
            ]);

        $classi = ["" => ""];
        foreach($rows as $row){
            $classi[$row['structure_class_id']] = $row['name'] . ' - ' . $row["class_dictionary_value"] . ' / ' . $row["section"];
        }

        return $classi;
    }
    
    private function manageFromDestination($userRequest){
        $form = new Form($this->actionUrl("manage", ["utrId" => $this->utrId]));
        $form->entityName = "user_transfer_request";
        $form->entity = $userRequest;
        $form->cancellAction = $this->actionUrl("_default");
        
        $codeField = new FormTextField("Codice richeista", "user_transfer_request_id", FormFieldType::STRING, false);
        $codeField->persist = false;
        $codeField->readOnly = true;
        $form->addField($codeField);
        
        $user = UserService::find($userRequest["user_id"]);
        
        $codeField = new FormTextField("Nome", "name", FormFieldType::STRING, false, $user['name']);
        $codeField->persist = false;
        $codeField->readOnly = true;
        $form->addField($codeField);

        $codeField = new FormTextField("Cognome", "surname", FormFieldType::STRING, false, $user['surname']);
        $codeField->persist = false;
        $codeField->readOnly = true;
        $form->addField($codeField);

        $codeField = new FormTextField("Data nascita", "birth_date", FormFieldType::STRING, false, $user['birth_date']);
        $codeField->persist = false;
        $codeField->readOnly = true;
        $form->addField($codeField);

        $statuses = [
            "" => "",
            UserTransferService::STATUS_APPROVED => "Approvata",
            UserTransferService::STATUS_REJECTED => "Rifiutata"
        ];
        $statusField = new FormSelectField("Stato", "status", FormFieldType::STRING, $statuses, true);
        $statusField->note = "<strong>ATTENZIONE:</strong> rifiutando la richiesta si impedisce alla scuola di destinazione di gestire qualsiasi tipo di informazione per lo studente";
        $form->addField($statusField);
        $noteField = new FormTextareaField("Note", "note", FormFieldType::STRING, false, null, ["maxLength" => 1000]);
        $form->addField($noteField);
        
        if ($form->isSubmit() && $form->checkFields()){
            UserTransferService::setStatus($this->utrId, $statusField->getValue(null), $noteField->getValue(null));
            redirect($this->actionUrl("_default"));
        }
        
        $this->bradcrumbs->add(new Bradcrumb("Trasferimenti", $this->actionUrl("_default")));
        $this->bradcrumbs->add(new Bradcrumb("Richiesta di trasferimento " . $userRequest["user_transfer_request_id"]));
        $this->header();
        ?>
        <div class="content active" id="panelTabStudenti">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-4 medium-4 columns">
                    <h3>Gestione richiesta</h3>
                </div>
            </div>
            <div class="section row">
                <?= $form->draw(); ?>
            </div>
        </div>
        <?php
        $this->footer();
    }


}