<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once dirname(__FILE__) .  '/SomministrazioneBaseAction.php';

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SomministrazioneAction extends SomministrazioneBaseAction{

    public function _default(){
        $this->header();
        ?>
        <div class="content active tab-studente row-100 somministrazione">
            <div class="section-title row-100">
                <div class="large-8 medium-7 small-7 columns">
                    <h3>Prove</h3>
                </div>
            </div>
            <div class="columns small-2 right-column">
                <?php $this->drawSomministrazioneDetails(); ?>
            </div>
            <div class="columns small-10">
                <?php $this->drawStructures(); ?>
            </div>
        </div>
        <?php
        $this->footer();
    }

    private function loadUsers(){
        $users = StructureService::findUserStructureForStructureId($this->structureId);
        $excludeRoles = [
            UserRole::DIRIGENTE,
            UserRole::REFERENTE_ISTITUTO_SOMMINISTRAZIONI,
            UserRole::STUDENTE
        ];

        $userValues = [];
        $addedUserIds = [];
        foreach ($users as $userStructure) {
            if (array_search($userStructure["role_id"], $excludeRoles) !== false){
                continue;
            }

            if (array_search($userStructure["user_id"], $addedUserIds) !== false){
                continue;
            }
            $addedUserIds[] = $userStructure["user_id"];

            $userValues[] = [
                "text" => $userStructure['name'] . ' ' . $userStructure['surname'],
                "value" => $userStructure["user_id"]
            ];
        }

        return $userValues;
    }

    private function loadStructureClasses($structureId, $class){
        $structureClassess = StructureClassService::findAllForStructureAndClass(
            $this->somministrazione["school_year_id"], 
            $structureId,
            $this->somministrazione["class_dictionary_value"]);

        $structureClassess = $structureClassess->fetchAll();

        for ($i = 0; $i < count($structureClassess); $i++) {
            $structureClassess[$i]["name"] = $class["value"];
        }

        return $structureClassess;
    }

    private function loadStructureReferent($structureId){
        $usersStructure = StructureService::findUserStructureWithRoles($structureId, [
            UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI
        ]);

        $structureUsersReferent = [];
        foreach ($usersStructure as $user) {
            $structureUsersReferent[] = [
                "text" => $user["name"] . ' ' . $user["surname"],
                "value" => $user["user_id"]
            ];
        }

        return $structureUsersReferent;
    }

    private function drawStructures(){
        $class = DictionaryService::findByGroupAndKey(Dictionary::CLASS_GRADE, $this->somministrazione["class_dictionary_value"]);

        $structures = [];
        if (UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_PLESSI_VISUALIZZA_TUTTI)){
            $stmt = StructureService::findAllWithParent($this->structureId);
            $structures = $stmt->fetchAll();
        } else if (UserRoleService::hasCurrentUser(UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI)){
            $usersStructures = StructureService::findAllUserStructureForUserParentStructureAndRole(
                $this->currentUser["user_id"],
                $this->structureId,
                UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI
            );
 
            $structures = $usersStructures->fetchAll();
        } else if (UserRoleService::hasCurrentUser(UserRole::INSEGNANTE_SOMMINISTRAZIONI)){
            $stmt = StructureClassService::findAllInsegnanteConstraints($this->currentUser["user_id"]);
            $structures = $stmt->fetchAll();
        }

        foreach ($structures as $structure) {
            $structureClassess = $this->loadStructureClasses($structure["structure_id"], $class);

            if (!UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_CLASSI_VISUALIZZA_TUTTI)){
                $structureClassess = $this->filterVisibleClassessForInsegnante($structureClassess, $structures);
            }

            $json = json_encode([
                "structure" => $structure,
                "somministrazione" => $this->somministrazione,
                "readOnly" => $this->status == SomministrazioneStatus::CLOSED,
                "somministrazioneTests" => $this->somministrazioneTests,
                "structureUsers" => $this->loadUsers(),
                "structureUsersReferent" => $this->loadStructureReferent($structure["structure_id"]),
                "structureClassess" => $structureClassess
            ]);
            ?>
            <div class="somministrazione-structure-<?= $structure["structure_id"] ?>"></div>
            <script>
                MbCreateVue("mb-somministrazione-structure", "somministrazione-structure-<?= $structure["structure_id"] ?>", <?= $json ?>);
            </script>
            <?php
        }
    }

    private function filterVisibleClassessForInsegnante($structureClass, $visibleClassess){
        $classess = [];

        foreach ($structureClass as $class) {
            foreach ($visibleClassess as $vc) {
                if ($vc["structure_class_id"] == $class["structure_class_id"]){
                    $classess[] = $class;
                    break;
                }
            }
        }

        return $classess;
    }
}