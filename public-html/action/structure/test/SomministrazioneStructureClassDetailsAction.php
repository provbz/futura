<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once __DIR__ .  '/SomministrazioneBaseAction.php';
require_once __DIR__ . "/../UserYearDocumentAction.php";

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class SomministrazioneStructureClassDetailsAction extends SomministrazioneBaseAction{

    public $classId;
    public $class;

    public $u_code;
    public $u_name;
    public $u_surname;
    public $u_birth_date;
    public $userId;

    public function _prepare(){
        parent::_prepare();

        $this->class = StructureClassService::find($this->classId);
        if ($this->class == null){
            redirectWithMessage(UriService::buildPageUrl("/structure/test/SomministrazioneAction", "", [
                "id" => $this->id
            ]), "Dati non validi");
        } else if ($this->class["row_status"] != RowStatus::READY){
            redirectWithMessage(UriService::buildPageUrl("/structure/test/SomministrazioneAction", "", [
                "id" => $this->id
            ]), "Classe rimossa");
        } 

        $plessoStructure = StructureService::find($this->class["structure_id"]);
        if ($plessoStructure["parent_structure_id"] != $this->structureId){
            redirectWithMessage(UriService::buildPageUrl("/structure/test/SomministrazioneAction", "", [
                "id" => $this->id
            ]), "Classe non valida");
        }
    }

    public function _default(){
        $this->setGlobalSessionValue("backUrl", $this->actionUrl("_default", $this->getParameters()));

        $subStructure = StructureService::find($this->class["structure_id"]);
        if ($subStructure == null || $subStructure["parent_structure_id"] != $this->structure["structure_id"]){
            redirectWithMessage(UriService::buildPageUrl("/structure/test/SomministrazioneAction", "", [
                "id" => $this->id
            ]), "Dati non validi");
        }

        $this->bradcrumbs->add(new Bradcrumb("Prove", UriService::buildPageUrl("/structure/test/SomministrazioneAction?id=" .$this->id)));
        $this->bradcrumbs->add(new Bradcrumb("Classe"));

        $this->header();
        ?>
        <div class="content active tab-studente row-100 somministrazione">
            <div class="section-title row-100">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-8 medium-7 small-7 columns">
                    <h3>Prove</h3>
                </div>
                <div class="large-4 medium-3 small-5 columns buttontoolbar">
                </div>
            </div>
            <div class="columns small-2 right-column">
                <?php $this->drawSomministrazioneDetails(); ?>
            </div>
            <div class="columns small-10">
                <?php $this->drawClass($subStructure); ?>
            </div>
        </div>
        <?php
        $this->footer();
    }

    private function loadStructureUsers(){
        $users = StructureService::findUserStructureForStructureId($this->structureId);
        $excludeRoles = [
            UserRole::DIRIGENTE,
            UserRole::REFERENTE_ISTITUTO_SOMMINISTRAZIONI,
            UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI,
            UserRole::STUDENTE
        ];

        $userValues = [];
        $addedUserIds = [];
        foreach ($users as $userStructure) {
            if (array_search($userStructure["role_id"], $excludeRoles) !== false){
                continue;
            }

            if (array_search($userStructure["user_id"], $addedUserIds) !== false){
                continue;
            }
            $addedUserIds[] = $userStructure["user_id"];

            $userValues[] = [
                "text" => $userStructure['name'] . ' ' . $userStructure['surname'],
                "value" => $userStructure["user_id"]
            ];
        }

        return $userValues;
    }

    public function drawClass($subStructure){
        $classDictionary = DictionaryService::findByGroupAndKey(Dictionary::CLASS_GRADE, $this->somministrazione["class_dictionary_value"]);
        $this->class["name"] = $this->structure["name"] . ' - ' . $subStructure["name"] . ' - ' . $classDictionary["value"] .'/'. $this->class["section"];

        $userClass = StructureClassUserService::FindWithRole($this->class["structure_class_id"], UserRole::INSEGNANTE_SOMMINISTRAZIONI);
        $userClassValues = [];
        foreach ($userClass as $user) {
            $userClassValues[] = [
                "text" => $user["name"] . ' ' . $user["surname"],
                "value" => $user["user_id"]
            ];
        }

        $json = json_encode([
            "somministrazione" => $this->somministrazione,
            "status" => $this->status,
            "readOnly" => $this->status == SomministrazioneStatus::CLOSED || $this->currentYear["school_year_id"] != $this->somministrazione["school_year_id"],
            "somministrazioneTests" => $this->somministrazioneTests,
            "structureUsers" => $this->loadStructureUsers(),
            "classTeachers" => $userClassValues,
            "selectedClass" => $this->class
        ]);
        ?>
        <div class="mb-somministrazione-class-1"></div>
        <script>
            MbCreateVue("mb-somministrazione-class", "mb-somministrazione-class-1", <?= $json ?>);
        </script>
        <?php
    }

    public function newUser(){
        $actionParameters = ["id" => $this->id, "classId" => $this->classId];
        $backAction = $this->actionUrl("_default", $actionParameters);

        $form = new Form($this->actionUrl("newUser", [
            "id" => $this->id,
            "classId" => $this->classId
        ]));
        $form->cancellAction = $backAction;
        
        $form->addField(new FormTextField("Nome", "u_name", FormFieldType::STRING, true));
        $form->addField(new FormTextField("Cognome", "u_surname", FormFieldType::STRING, true));
        $form->addField(new FormDateField("Data di nascita", "u_birth_date", FormFieldType::DATE, true));

        $this->bradcrumbs->add(new Bradcrumb("Somministrazione", UriService::buildPageUrl("/structure/test/SomministrazioneAction?id=" .$this->id)));
        $this->bradcrumbs->add(new Bradcrumb("Classe", $backAction));
        $this->bradcrumbs->add(new Bradcrumb("Nuovo studente"));

        $this->header();
        ?>
        <div class="content active tab-studente row-100 somministrazione">
            <div class="section-title row-100">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-8 medium-7 small-7 columns">
                    <h3>Somministrazione - Studente</h3>
                </div>
                <div class="large-4 medium-3 small-5 columns buttontoolbar">
                </div>
            </div>
            <div class="columns small-2 right-column">
                <?php $this->drawSomministrazioneDetails(); ?>
            </div>
            <div class="columns small-10">
                <?php 
                if ($form->isSubmit() && $form->checkFields()){
                    $user = UserService::findByNameSurnameAndBirthDate($this->u_name, $this->u_surname, 
                        TextService::parseDate($this->u_birth_date));

                    if ($user == null){
                        redirect($this->actionUrl("newUserFull", array_merge($actionParameters, [
                            "u_name" => $this->u_name,
                            "u_surname" => $this->u_surname,
                            "u_birth_date" => $this->u_birth_date
                        ])));
                    } 
                    
                    $isStructureStudent = StructureService::hasUserStructureRole($user['user_id'], $this->structureId, UserRole::STUDENTE);
                    
                    $request = UserTransferService::findFromUserToStructure($user['user_id'], $this->structureId);
                    $otherStructures = StructureService::findUserStructures($user['user_id']);
                    $otherStructure = null;
                    foreach ($otherStructures as $structure) {
                        if ($structure["structure_id"] != $this->structureId){
                            $otherStructure = $structure;
                        }
                    }

                    if ($request != null){ 
                        $this->waitRequest($request, $user, $actionParameters);
                    } else if ($otherStructure != NULL){ 
                        $this->otherStructureUser($otherStructure, $user, $actionParameters);
                    } else if ($isStructureStudent){  
                        redirect($this->actionUrl("newUserFull", array_merge($actionParameters, [
                            "userId" => $user["user_id"],
                            "u_name" => $this->u_name,
                            "u_surname" => $this->u_surname,
                            "u_birth_date" => $this->u_birth_date
                        ])));
                    } else {
                        echo 'Dati non inseriti correttamente, associazione alla struttura assente.';
                    }
                } else {
                    $form->draw();
                }
                ?>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function waitRequest($request, $user, $additionalParameters = null){
        $backAction = $this->actionUrl("_default", $additionalParameters);
        
        ?>
        <div class="message">
            <p>La richiesta di trasferimento per lo studente è in attesa di approvazione.</p>
            <table>
                <tr><td>Identificativo:</td><td><?= _t($request['user_transfer_request_id']) ?></td></tr>
                <tr><td>Data:</td><td><?= _t($request['insert_date']) ?></td></tr>
                <tr><td>Nome:</td><td><?= _t($user['name']) ?></td></tr>
                <tr><td>Cognome:</td><td><?= _t($user['surname']) ?></td></tr>
                <tr><td>Data nascita:</td><td><?= TextService::formatDate( _t($user['birth_date'] ), true) ?></td></tr>
                <tr><td>Stato:</td><td><?= _t($request['status']) ?></td></tr>
            </table>
            <div class="padding-top">
                <a class="button tiny alert" href="<?= $backAction ?>">
                    <i class="<?= Icon::CANCELL ?>"></i>
                    Annulla
                </a>
            </div>
        </div>
        <?php
    }

    public function otherStructureUser($otherStructure, $user, $additionalRequestParameters = []){
        $backAction = $this->actionUrl("newUser", $additionalRequestParameters);
        
        $structure = StructureService::find($otherStructure['structure_id']);
        ?>
            <div class="message">
                <p>Lo studente risulta assegnato ad un'altra scuola: <strong><?= $structure['name'] ?></strong>.</p>
                <p>E'possibile inviare una richiesta di trasferimento dei dati direttamente al responsabile della scuola premendo il pulsante sotto.</p>
                <p style="padding-bottom:20px;">Per completare la procedura sarà necessario attendere l'approvazione da parte del precedente referente.</p>
                <p style="padding-bottom: 10px;"><strong>ATTENZIONE:</strong> prima di procedere all'invio della richiesta verificare con attenzione i dati.</p>

                <table style="padding-bottom: 30px;">
                    <tr><td>Nome:</td><td><?= _t($user['name']) ?></td></tr>
                    <tr><td>Cognome:</td><td><?= _t($user['surname']) ?></td></tr>
                    <tr><td>Data nascita:</td><td><?= TextService::formatDate( _t($user['birth_date']), true ) ?></td></tr>
                </table>

                <a class="button tiny" onclick="return confirm('Si è sicuri di voler procedere?');" href="<?= $this->actionUrl("transferRequest", 
                    array_merge(['userId' => $user['user_id']], $additionalRequestParameters)) ?>">
                    Invia richiesta
                </a>
                <a class="button tiny alert" href="<?= $backAction ?>">
                    <i class="<?= Icon::CANCELL ?>"></i>
                    Annulla
                </a>
            </div>
        <?php 
    }

    public function changeSection(){
        $actionParameters = ["id" => $this->id, "classId" => $this->classId];
        $backAction = $this->actionUrl("_default", $actionParameters);

        $this->bradcrumbs->add(new Bradcrumb("Somministrazione", UriService::buildPageUrl("/structure/test/SomministrazioneAction?id=" .$this->id)));
        $this->bradcrumbs->add(new Bradcrumb("Classe", $backAction));
        $this->bradcrumbs->add(new Bradcrumb("Sposta di sezione"));

        $form = new Form($this->actionUrl("changeSection", $actionParameters));
        $form->cancellAction = $backAction;

        $structureClassess = StructureClassService::findAllForStructureAndClass(
            $this->somministrazione["school_year_id"], 
            $this->class["structure_id"],
            $this->somministrazione["class_dictionary_value"]);

        $classGrades = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        
        $classes = ["" => ""];
        foreach($structureClassess as $class){
            if ($class["structure_class_id"] == $this->class["structure_class_id"]){
                continue;
            }

            $classes[$class["structure_class_id"]] = $classGrades[$class['class_dictionary_value']] . ' - ' . $class["section"];
        }

        $classField = new FormSelectField("Nuova sezione", "newClassId", FormFieldType::NUMBER, $classes, true);
        $form->addField($classField);

        if ($form->isSubmit() && $form->checkFields()){
            $newClassId = $classField->getValue(null);
            $newClass = StructureClassService::find($newClassId);
            if ($newClass == null || $newClass["structure_id"] != $this->class["structure_id"]){
                redirectWithMessage($this->actionUrl("changeSection", "Dati non validi"));
            }

            $userClass = StructureClassUserService::FindWithRole($this->class["structure_class_id"], UserRole::STUDENTE);
            foreach($userClass as $user){
                $userYear = UserYearService::findUserYearByUser($user["user_id"]);
                EM::updateEntity("user_year", 
                    [
                        "sezione" => $newClass["section"]
                    ], 
                    [
                        "user_year_id" => $userYear["user_year_id"]
                    ]);

                $this->updateUserStructureClass($user, $newClass["structure_id"], $newClass["class_dictionary_value"], $newClass["section"], $user);
            }

            redirectWithMessage($backAction, "Operazione eseguita");
        }

        $this->header();
        ?>
        <div class="content active tab-studente row-100 somministrazione">
            <div class="section-title row-100">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-8 medium-7 small-7 columns">
                    <h3>Sposta studenti in altra sezione</h3>
                </div>
                <div class="large-4 medium-3 small-5 columns buttontoolbar">
                </div>
            </div>
            <div class="columns small-2 right-column">
                <?php $this->drawSomministrazioneDetails(); ?>
            </div>
            <div class="columns small-10">
                <p class="columns small-12">Trasferisci tutti gli studenti della classe <?= _t($classGrades[$this->class['class_dictionary_value']]) ?> - <?= _t($this->class["section"]) ?></p>
                <?= $form->draw() ?>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function transferRequest(){
        $actionParameters = ["id" => $this->id, "classId" => $this->classId];
        $backAction = $this->actionUrl("_default", $actionParameters);

        $user = UserService::find($this->userId);
        if ($user == null){
            redirect(UriService::accessDanyActionUrl());
        }

        $this->bradcrumbs->add(new Bradcrumb("Somministrazione", UriService::buildPageUrl("/structure/test/SomministrazioneAction?id=" .$this->id)));
        $this->bradcrumbs->add(new Bradcrumb("Classe", $backAction));
        $this->bradcrumbs->add(new Bradcrumb("Nuovo studente"));

        $this->header();
        ?>

        <div class="content active tab-studente row-100 somministrazione">
            <div class="section-title row-100">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-8 medium-7 small-7 columns">
                    <h3>Somministrazione - Studente</h3>
                </div>
                <div class="large-4 medium-3 small-5 columns buttontoolbar">
                </div>
            </div>
            <div class="columns small-2 right-column">
                <?php $this->drawSomministrazioneDetails(); ?>
            </div>
            <div class="columns small-10">
                <?php
                    try{
                        $request = UserTransferService::addRequest($this->userId, $this->structureId, UserTransferRequestType::FROM_DESTINATION, null, $this->classId);
                        ?>
                        <p>Richiesta inserita:</p>
                        <table style="padding-bottom: 20px;">
                            <tr><td>Identificativo:</td><td><?= _t($request['user_transfer_request_id']) ?></td></tr>
                            <tr><td>Data:</td><td><?= TextService::formatDate( _t($request['insert_date']) ) ?></td></tr>
                            <tr><td>Nome:</td><td><?= _t($user['name']) ?></td></tr>
                            <tr><td>Cognome:</td><td><?= _t($user['surname']) ?></td></tr>
                            <tr><td>Data di nascita:</td><td><?= TextService::formatDate( _t($user['birth_date']), true)  ?></td></tr>
                            <tr><td>Stato:</td><td><?= _t($request['status']) ?></td></tr>
                        </table>
                    <?php } catch (Exception $ex){ ?>
                        <p>Erriri nella richiesta</p>
                        <p><?= _t($ex->getMessage()) ?></p>
                    <?php }
                ?>
                <a href="<?= $backAction ?>" class="button tiny">Ok</a>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function remove(){
        $actionParameters = ["id" => $this->id, "classId" => $this->classId];
        $backAction = $this->actionUrl("_default", $actionParameters);

        $user = UserService::find($this->userId);
        if ($user == null){
            redirectWithMessage($backAction, "Dati non validi");
        }

        $structureClassUser = StructureClassUserService::findForUserAndYear($user["user_id"], $this->somministrazione["school_year_id"]);
        if ($structureClassUser == null){
            redirectWithMessage($backAction, "Dati non validi");
        }

        if ($structureClassUser["school_year_id"] != $this->currentYear["school_year_id"]){
            redirectWithMessage($backAction, "Dati relativi ad anni precedenti non modificabili");
        }

        $userStructure = StructureService::hasUserStructureRole($user["user_id"], $this->structureId, UserRole::STUDENTE);
        if ($userStructure == null){
            redirectWithMessage($backAction, "Dati non validi o utente trasferito in altro istituto");
        }

        EM::updateEntity("structure_class_user", [
            "row_status" => RowStatus::DELETED
        ], [
            "structure_class_user_id" => $structureClassUser["structure_class_user_id"]
        ]);

        redirectWithMessage($backAction, "Studente rimosso");
    }

    public function newUserFull(){
        $actionParameters = ["id" => $this->id, "classId" => $this->classId];
        $backAction = $this->actionUrl("_default", $actionParameters);

        if ($this->currentYear["school_year_id"] != $this->somministrazione["school_year_id"]){
            redirectWithMessage($backAction, "Dati relativi ad anni precedenti non modificabili");
        }
        
        $user = UserService::find($this->userId);
        $userYear = null;
        $structureClassUser = null;

        if ($user == null && StringUtils::isNotBlank($this->u_code)){
            $user = UserService::findByCode($this->u_code);
        }

        if ($user != NULL ){
            $structureClassUser = StructureClassUserService::findForUserAndYear($user["user_id"], $this->somministrazione["school_year_id"]);
            if ($structureClassUser != null){
                if ($structureClassUser["school_year_id"] != $this->currentYear["school_year_id"]){
                    redirectWithMessage($backAction, "Dati relativi in anni precedenti non modificabili");
                }
            }

            $userStructure = StructureService::hasUserStructureRole($user["user_id"], $this->structureId, UserRole::STUDENTE);
            if ($userStructure == null){
                redirectWithMessage($backAction, "Dati non validi o utente trasferito in altro istituto");
            }
        }

        if ($user != null){
            $userYear = UserYearService::findUserYearByUser($user["user_id"]);
        }
        
        $entity = [];

        if ($this->u_name != null){
            $actionParameters['u_name'] = $this->u_name;
            $actionParameters['u_surname'] = $this->u_surname;
            $actionParameters['u_birth_date'] = $this->u_birth_date;
            
            $entity = [
                "u_name" => $this->u_name,
                "u_surname" => $this->u_surname,
                "u_birth_date" => $this->u_birth_date
            ];
        } else {
            $actionParameters['u_name'] = $user['name'];
            $actionParameters['u_surname'] = $user['surname'];
            $actionParameters['u_birth_date'] = $user['birth_date'];
            
            $entity = [
                "u_name" => $user['name'],
                "u_surname" => $user['surname'],
                "u_birth_date" => $user['birth_date']
            ];
        }

        $form = new Form($this->actionUrl("newUserFull", array_merge($actionParameters, [
            "userId" => $this->userId
        ])));

        $form->cancellAction = $backAction;
        ReflectionUtils::fillWithPrefix($user, "u_", $entity);
        ReflectionUtils::fillWithPrefix($userYear, "uy_", $entity);
        $form->entity = $entity;
        
        $form->addField(new FormHtmlField("<h4>Dati studente</h4>"));

        /* Dati condivisi */
        $uya = new UserYearDocumentAction();
        
        $nameField = new FormTextField("Nome", "u_name", FormFieldType::STRING, true);
        if (!UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_MODIFICA_DATI_BASE_STUDENTE)){
            $nameField->readOnly = true;
        }
        $form->addField($nameField);

        $surnameField = new FormTextField("Cognome", "u_surname", FormFieldType::STRING, true);
        if (!UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_MODIFICA_DATI_BASE_STUDENTE)){
            $surnameField->readOnly = true;
        }
        $form->addField($surnameField);

        $birthDateField = new FormDateField("Data di nascita", "u_birth_date", FormFieldType::DATE, true);
        if (!UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_MODIFICA_DATI_BASE_STUDENTE)){
            $birthDateField->readOnly = true;
        }
        $form->addField($birthDateField);

        $plessi = StructureService::findAllWithParent($this->structureId);
        $values = ["" => ""];
        foreach ($plessi as $plesso) {
            $values[$plesso["structure_id"]] = $this->structure["name"] . " - " . $plesso["name"];
        }
        $plessoField = new FormSelectField("Plesso", "uy_plesso_structure_id", FormFieldType::NUMBER, $values, true, $this->class["structure_id"]);
        $form->addField($plessoField);

        $classes = ['' => ''] + DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $classField = new FormSelectField("Classe", "uy_classe", FormFieldType::STRING, $classes, true, $this->class["class_dictionary_value"]);
        $classField->readOnly = !UserRoleService::canCurrentUserDo(UserPermission::somministrazione_studente_class_edit);
        $form->addField($classField);

        $sezioneField = new FormTextField("Sezione", "uy_sezione", FormFieldType::STRING, false, $this->class["section"], [
            "maxlength" => 20
        ]);
        $sezioneField->checkFunction = function($field, $value) use ($plessoField, $classField){
            $structureId = $plessoField->getValue(null);

            $classDictionary = $this->class["class_dictionary_value"];
            if (UserRoleService::canCurrentUserDo(UserPermission::somministrazione_studente_class_edit)){
                $classDictionary = $classField->getValue(null);
            }

            $classe = StructureClassService::findForStructureAndClassAndSezione($this->class["school_year_id"], 
                $structureId,
                $classDictionary, 
                $value
            );

            if ($classe == null){
                $field->addFieldError("La classe/sezione non è presente, crearla prima di inserire lo studente.");
            }
        };
        $form->addField($sezioneField);

        $nazionalitaIta = DictionaryService::findGroupAsDictionary(Dictionary::NAZIONALITA_ITA);
        $form->addField(new FormSelect2Field("Nazionalità", "uy_nazionalita_ita_dictionary_key", FormFieldType::NUMBER, $nazionalitaIta, true));

        if ($form->isSubmit() && $form->checkFields()){
            $data = [];
            foreach($form->fields as $key => $value){
                $data[$value->name] = $value->getValueForDb();
            }
            
            $newUser = ReflectionUtils::getPropertyWithPrefix($data, "u_");
            if ($user == null){
                $newUser['created_by_user_id'] = $this->currentUser['user_id'];
                $newUser['insert_date'] = 'NOW()';
                $newUser["section"] = UserSection::MONDO_PAROLE;
                
                $this->userId = EM::insertEntity("user", $newUser);
                $user = UserService::find($this->userId);
                
                EM::insertEntity("user_role", ['user_id' => $this->userId, 'role_id' => UserRole::USER]);
                StructureService::addUserStructure($this->userId, $this->structureId, [UserRole::STUDENTE]);
            } else {
                unset($newUser['code']);
                EM::updateEntity("user", $newUser, ["user_id" => $user["user_id"]]);
            }

            $newUserYear = ReflectionUtils::getPropertyWithPrefix($data, "uy_");
            if ($userYear == null){
                $newUserYear['user_id'] = $user["user_id"];
                $userYearId = EM::insertEntity("user_year", $newUserYear);
                $userYear = UserYearService::find($userYearId);
            } else {
                EM::updateEntity("user_year", 
                    array_merge($newUserYear, ["classe" => $this->class["class_dictionary_value"]]), 
                    ["user_year_id" => $userYear["user_year_id"]]);
            }
            
            $sezione = $sezioneField->getValue(null);
            $structureId = $plessoField->getValue(null);
            if (UserRoleService::canCurrentUserDo(UserPermission::somministrazione_studente_class_edit)){
                $newClass = $classField->getValue(null);
                $this->updateUserStructureClass($user, $structureId, $newClass, $sezione, $structureClassUser);
            } else {
                $this->setUserStructureClass($user, $structureId, $structureClassUser, $sezione);
            }
            
            redirectWithMessage($backAction, "Dati salvati");
        }

        $this->bradcrumbs->add(new Bradcrumb("Somministrazione", UriService::buildPageUrl("/structure/test/SomministrazioneAction?id=" .$this->id)));
        $this->bradcrumbs->add(new Bradcrumb("Classe", $backAction));
        $this->bradcrumbs->add(new Bradcrumb("Nuovo studente"));

        $this->header();
        ?>
        <div class="content active tab-studente row-100 somministrazione">
            <div class="section-title row-100">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-8 medium-7 small-7 columns">
                    <h3>Somministrazione - Studente</h3>
                </div>
                <div class="large-4 medium-3 small-5 columns buttontoolbar">
                </div>
            </div>
            <div class="columns small-2 right-column">
                <?php $this->drawSomministrazioneDetails(); ?>
            </div>
            <div class="columns small-10">
                <?= $form->draw() ?>
            </div>
        </div>
        <?php
        $this->footer();
    }

    private function updateUserStructureClass($user, $structureId, $newClassDictionary, $sezione, $structureClassUser){
        $classe = StructureClassService::findForStructureAndClassAndSezione($this->class["school_year_id"], 
            $structureId, 
            $newClassDictionary, 
            $sezione
        );

        if ($classe == null){
            return;
        }

        if ($structureClassUser == null){
            EM::insertEntity("structure_class_user", [
                "structure_class_id" => $classe["structure_class_id"],
                "user_id" => $user["user_id"],
                "row_status" => RowStatus::READY,
                "insert_user_id" => $this->currentUser["user_id"],
                "insert_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser["user_id"],
                "last_edit_date" => "NOW()",
                "role_id" => UserRole::STUDENTE
            ]);
        } else {
            EM::updateEntity("structure_class_user", [
                "structure_class_id" => $classe["structure_class_id"]
            ], [
                "structure_class_user_id" => $structureClassUser["structure_class_user_id"]
            ]);
        }
    }

    private function setUserStructureClass($user, $structureId, $structureClassUser, $sezione){
        $classe = StructureClassService::findForStructureAndClassAndSezione($this->class["school_year_id"], 
            $structureId, 
            $this->class["class_dictionary_value"], 
            $sezione
        );

        if ($structureClassUser == null){
            EM::insertEntity("structure_class_user", [
                "structure_class_id" => $classe["structure_class_id"],
                "user_id" => $user["user_id"],
                "row_status" => RowStatus::READY,
                "insert_user_id" => $this->currentUser["user_id"],
                "insert_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser["user_id"],
                "last_edit_date" => "NOW()",
                "role_id" => UserRole::STUDENTE
            ]);
        } else {
            EM::updateEntity("structure_class_user", [
                "structure_class_id" => $classe["structure_class_id"]
            ], [
                "structure_class_user_id" => $structureClassUser["structure_class_user_id"]
            ]);
        }
    }
}