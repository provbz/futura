<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class SomministrazioneStructureClassAction extends StructureAction{

    public $id;
    public $subStructureId;
    public $classId;
    public $actionPatameters;
    
    public function _default(){
        $subStructure = StructureService::find($this->subStructureId);
        if ($subStructure["parent_structure_id"] != $this->structure["structure_id"]){
            redirectWithMessage(UriService::buildPageUrl("/structure/test/SomministrazioniAction", "", ["id" => $this->id]), "Dati non validi");
        }

        $somministrazione = SomministrazioneService::find($this->id);
        if ($somministrazione == null){
            redirectWithMessage(UriService::buildPageUrl("/structure/test/SomministrazioneAction", "", ["id" => $this->id]), "Dati non validi");
        }

        $form = new Form($this->actionUrl("", [
            'id' => $this->id, 
            "subStructureId" => $this->subStructureId,
            "classId" => $this->classId
        ]));
        $form->entityName = "structure_class";
        $form->entity = StructureClassService::find($this->classId);
        $form->cancellAction = UriService::buildPageUrl("/structure/test/SomministrazioneAction", "", ["id" => $this->id]);

        $plessoField = new FormTextField("Plesso", "plesso", FormFieldType::STRING, true, $subStructure["name"]);
        $plessoField->readOnly = true;
        $form->addField($plessoField);

        $classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $classField = new FormSelect2Field("Classe", "class_dictionary_value", FormFieldType::STRING, $classes, true, $somministrazione["class_dictionary_value"]);
        $classField->readOnly = true;
        $form->addField($classField);
        $form->addField(new FormTextField("Sezione", "section", FormFieldType::STRING));

        if ($form->isSubmit() && $form->checkFields()){
            if(StringUtils::isBlank($this->classId)){
                $this->classId = $form->persist("", [
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser['user_id'],
                    "row_status" => RowStatus::READY,
                    "structure_id" => $this->subStructureId,
                    "class_dictionary_value" => $somministrazione["class_dictionary_value"],
                    "school_year_id" => $somministrazione["school_year_id"]
                ]);
            } else {
                $form->persist("structure_class_id=" . $this->classId);
            }
            
            EM::updateEntity("structure_class", [
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser['user_id']
            ], ["structure_class_id" => $this->classId]);

            redirectWithMessage(UriService::buildPageUrl("/structure/test/SomministrazioneAction", '', ['id' => $this->id]), "Operazione eseguita");
        }

        $this->bradcrumbs->add(new Bradcrumb("Somministrazione", UriService::buildPageUrl("/structure/test/SomministrazioneAction?id=" .$this->id)));
        $this->bradcrumbs->add(new Bradcrumb("Classe"));

        $this->header();
        ?>
        <div class="content active tab-studente row-100 somministrazione">
            <div class="section-title row-100">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-8 medium-7 small-7 columns">
                    <h3>Edit classe</h3>
                </div>
            </div>

            <?= $form->draw(); ?>
        </div>
        <?php
        $this->footer();
    }
}