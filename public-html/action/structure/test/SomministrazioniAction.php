<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of SomministrazioniAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class SomministrazioniAction extends StructureAction{

    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());
        $this->header();

        if (!UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_VISUALIZA_TUTTE)){
            if (UserRoleService::hasCurrentUser(UserRole::REFERENTE_ISTITUTO_SOMMINISTRAZIONI) ||
                UserRoleService::hasCurrentUser(UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI)){
                    $grades = MetadataService::findMetadataValue(MetadataEntity::USER, $this->currentUser["user_id"], UserMetadata::SCHOOL_GRADE);
                    if ($grades == null){
                    ?>
                        <div style="color: red; border: 1px solid red; padding: 10px; margin: 10px;">
                            Utente non associato ad un grado scolastico.<br/>
                            Chiedere al proprio responsabile di inserire il grado scolastico nella gestione utenti.<br/>
                            Senza grado scolastico non è possibile visualizzare nessuna somministrazione.
                        </div>
                    <?php
                        $this->footer();
                        die();
                    }
            }
        }
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row-100">
                    <div class="large-8 medium-7 small-7 columns">
                        <h3>Mondo delle parole e progetto letto scrittura</h3>
                    </div>
                    <div class="large-4 medium-3 small-5 columns buttontoolbar">
                        <?php if (UserRoleService::canCurrentUserDo( UriService::buildPageUrl("/structure/test/SomministrazioneTrasferimentiAction") )){ ?>
                            <a href="<?= UriService::buildPageUrl("/structure/test/SomministrazioneTrasferimentiAction") ?>" class="button radius nuovo float-right">
                                <i class="fas fa-arrow-right margin-right"></i> Trasferimenti
                            </a>
                        <?php } ?>
                    </div>
                </div>
                
                <div class="mb-somministrazione-table"></div>
            </div>
        </div>
        <?php
        $this->footer();
    }
}