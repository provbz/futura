<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once __DIR__ .  '/SomministrazioneBaseAction.php';

/**
 * Description of SomministrazioneStatsAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class SomministrazioneStatsAction extends SomministrazioneBaseAction {

    public function _default(){
        $this->header();
        ?>
        <div class="content active tab-studente row-100 somministrazione">
            <div class="section-title row-100">
                <div class="large-8 medium-7 small-7 columns">
                    <h3>Risultati</h3>
                </div>
            </div>
            <div class="columns small-2 right-column">
                <?php $this->drawSomministrazioneDetails(); ?>
            </div>
            <div class="columns small-10">
                <div class="mb-somministrazione-stats-admin-page-cnt"></div>
                <script type="text/javascript">
                MbCreateVue("mb-somministrazione-stats-table", "mb-somministrazione-stats-admin-page-cnt", {
                    "somministrazioneId": <?= $this->id ?>
                })
                </script>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function loadData(){
        $classes = StructureClassService::findAllForParentStructureAndClass($this->somministrazione["school_year_id"], 
            $this->structure["structure_id"], 
            $this->somministrazione["class_dictionary_value"]);
        $testsData = $this->loadTestData($this->somministrazione["somministrazione_id"]);        
        $totalStudentsForStructures = $this->countStudentsForClassesInStructureParent($this->somministrazione["school_year_id"], 
            $this->structure["structure_id"],
            $this->somministrazione["class_dictionary_value"]);

        $tableData = [];
        foreach ($classes as $class) {
            if (!$this->canUserViewClass($class)){
                continue;
            }

            $totalStudents = ReflectionUtils::getValue($totalStudentsForStructures, $class["structure_class_id"], 0);

            if ($totalStudents == 0){
                continue;
            }

            $row = [];
            $row["class"] = StructureService::printFullStructureName($class["structure_id"]) . ' ' . $class["section"];
            $row["structure_class_id"] = $class["structure_class_id"];
            $row["totale_studenti"] = $totalStudents;
            $row["tests"] = []; 

            foreach ($testsData as $test) {
                $testResults = $this->loadTestResults($class["structure_class_id"], $test["test"]["somministrazione_test_id"]);

                if (count($testResults) == 0){
                    $testResults["not_set"] = $totalStudents;
                } else if (ArrayUtils::sum($testResults) < $totalStudents){
                    $testResults["not_set"] += $totalStudents - ArrayUtils::sum($testResults);
                }
                
                $row["tests"][$test["test"]["test_id"]] = $testResults;
            }
            
            $tableData[] = $row;
        }

        $total = SomministrazioneStatsService::computeTotal($tableData, $testsData);

        header('Content-Type: application/json');
        $response = new ApiResponse([
            "test_data" => $testsData,
            "table" => $tableData,
            "total" => $total
        ]);
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    private function canUserViewClass($class){
        if (UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_PLESSI_VISUALIZZA_TUTTI)){
            return true;    
        } else if (UserRoleService::hasCurrentUser(UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI)){
            $usersStructures = StructureService::findAllUserStructureForUserParentStructureAndRole(
                $this->currentUser["user_id"],
                $this->structureId,
                UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI
            );
            foreach ($usersStructures as $structure) {
                if ($structure["structure_id"] == $class["structure_id"]){
                    return true;
                }
            }
        } else if (UserRoleService::hasCurrentUser(UserRole::INSEGNANTE_SOMMINISTRAZIONI)){

        }

        return false;
    }

    private function loadTestData($somministrazioneId){
        $tests = SomministrazioneService::findAllTestsForSomministrazione($this->id);

        $results = [];
        foreach($tests as $test){
            $testFile = TestService::loadTest($test["code"]);

            $row = [
                "test" => $test,
                "variables" => $testFile->getVariablesNames()
            ];

            $results[] = $row;
        }

        return $results;
    }

    public function countStudentsForClassesInStructureParent($schoolYearId, $parentStructureId, $classDictionaryValue){
        $rows = EM::execQuery("SELECT sc.structure_class_id, count(sc.structure_class_id) as c
            from structure s 
            left join structure_class sc on sc.structure_id=s.structure_id
            left join structure_class_user scu ON scu.structure_class_id=sc.structure_class_id
            where 
                scu.row_status = 'ready'
                AND scu.role_id=:student_role
                AND s.parent_structure_id = :parent_structure_id
                AND sc.school_year_id=:school_year_id
                AND sc.class_dictionary_value=:class_dictionary_value
            group by sc.structure_class_id", [
                "school_year_id" => $schoolYearId,
                "parent_structure_id" => $parentStructureId,
                "class_dictionary_value" => $classDictionaryValue,
                "student_role" => UserRole::STUDENTE
            ]);

        $results = [];
        foreach($rows as $row){
            $results[$row["structure_class_id"]] = $row["c"];
        }
        return $results;
    }

    public function loadTestResults($structureClassId, $somministrazioneTestId){
        $rows = EM::execQuery("SELECT stru.status, variable_name, fascia, count(*) as c
            from somministrazione_test_result_user stru
            left join somministrazione_test_result_user_variable struv ON struv.somministrazione_test_result_user_id=stru.somministrazione_test_result_user_id
            left join structure_class_user scu ON scu.user_id=stru.user_id AND scu.role_id=:student_role_id
            left join structure_class sc on sc.structure_class_id=scu.structure_class_id
            left join structure s on sc.structure_id=s.structure_id
            where 
                scu.row_status = 'ready'
                AND sc.structure_class_id=:structure_class_id
                AND somministrazione_test_id=:somministrazione_test_id
            group by stru.status, variable_name, fascia
            order by stru.status, variable_name, fascia", [
                "structure_class_id" => $structureClassId,
                "somministrazione_test_id" => $somministrazioneTestId,
                "student_role_id" => UserRole::STUDENTE
            ]);

        $results = [];
        foreach($rows as $row){
            if ($row["status"] == SomministrazioneUserStatus::SOMMINISTRATA){
                $results[$row["variable_name"]]["f_" . $row["fascia"]] = $row["c"];
            } else if ($row["status"] == null){
                $results["not_set"] = $row["c"];
            } else {
                $results[$row["status"]] = $row["c"];
            }
        }

        if (!isset( $results["not_set"] )){
            $results["not_set"] = 0;
        }
        if (!isset( $results["assente"] )){
            $results["assente"] = 0;
        }
        if (!isset( $results["non_valida"] )){
            $results["non_valida"] = 0;
        }

        return $results;
    }
}