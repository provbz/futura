<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once dirname(__FILE__) .  '/SomministrazioneStructureClassDetailsAction.php';

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SomministrazioneTestAction extends SomministrazioneStructureClassDetailsAction {

    public $testId;

    public $backAction;
    private $actionParameters;
    private $subStructure;
    private $somministrazioneTest;
    private $test;
    private $testData;

    public function _prepare(){
        parent::_prepare();

        $this->actionParameters = [
            "id" => $this->somministrazione["somministrazione_id"],
            "classId" => $this->classId
        ];
        $this->backAction = UriService::buildPageUrl("/structure/test/SomministrazioneStructureClassDetailsAction", "", $this->actionParameters);

        $this->somministrazioneTest = SomministrazioneService::findSomministrazioneTestForSomministrazioneAndTest($this->id, $this->testId);
        if ($this->somministrazioneTest == null) {
            redirectWithMessage($this->backAction, "Test non impostato nella somministrazione");
        }

        $this->subStructure = StructureService::find($this->class["structure_id"]);
        if ($this->subStructure == null || $this->subStructure["parent_structure_id"] != $this->structure["structure_id"]){
            redirectWithMessage($this->backAction, "Dati non validi");
        }

        $this->testData = TestService::find($this->testId);
        if ($this->testData == null) {
            redirectWithMessage($this->backAction, "Dati non validi");
        }

        $this->test = TestService::loadTest($this->testData["code"]);
        if ($this->test == null) {
            redirectWithMessage($this->backAction, "Test non trovato");
        }
    }

    private function loadUserResult(){
        $classUsers = StructureClassUserService::FindWithRole($this->classId, UserRole::STUDENTE);
        $usersResults = [];

        foreach ($classUsers as $classUser) {
            $userResult = [
                "user" => [
                    "name" => $classUser["name"],
                    "surname" => $classUser["surname"],
                    "code" => $classUser["code"],
                    "user_id" => $classUser["user_id"],
                    "structure_class_user_id" => $classUser["structure_class_user_id"]
                ]
            ];

            $userResultDb = EM::execQuerySingleResult("SELECT * FROM somministrazione_test_result_user
                WHERE somministrazione_test_id=:somministrazione_test_id AND
                user_id=:user_id",
                [
                    "somministrazione_test_id" => $this->somministrazioneTest["somministrazione_test_id"],
                    "user_id" => $classUser["user_id"]
                ]);
            
            $userResult["status"] = null;
            $userResult["raw_data"] = new stdClass();
            $userResult["nonValidaAttachments"] = [];

            if ($userResultDb != null){
                $userResult["status"] = $userResultDb["status"];
                $userResult["raw_data"] = json_decode($userResultDb["raw_data"]);
                $userResult["nonValidaAttachments"] = AttachmentService::findAllForEntityInMV(EntityName::SOMMINISTRAZIONE_TEST_RESULT_USER, 
                    $userResultDb["somministrazione_test_result_user_id"], 
                    AttachmentType::SOMMINISTRAZIONE_TEST_RESULT_USER_NON_VALIDA);
            }

            $usersResults[] = $userResult;
        }

        return $usersResults;
    }

    public function _default(){
        $usersResults = $this->loadUserResult();

        $soglie = SomministrazioneService::findSoglieForSomministrazioneTest($this->somministrazioneTest["somministrazione_test_id"], $this->test->getVariablesNames());
        if (count($soglie) == 0){
            $soglie = new StdClass();
        }

        $classDictionary = DictionaryService::findByGroupAndKey(Dictionary::CLASS_GRADE, $this->somministrazione["class_dictionary_value"]);
        $this->class["name"] = $this->structure["name"] . ' - ' . $this->subStructure["name"] . ' - ' . $classDictionary["value"] .'/'. $this->class["section"];

        $this->bradcrumbs->add(new Bradcrumb("Somministrazione", UriService::buildPageUrl("/structure/test/SomministrazioneAction?id=" .$this->id)));
        $this->bradcrumbs->add(new Bradcrumb("Classi", $this->backAction));
        $this->bradcrumbs->add(new Bradcrumb($this->testData["name"]));

        $this->header();
        ?>
        <style>
        .title{
            background-color: #eeeeee;
            padding: 15px;
            font-weight: 800;
        }
        .content{
            padding: 15px;
            overflow: auto;
        }
        </style>
        <div class="content active tab-studente row-100 somministrazione">
            <div class="section-title row-100">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-8 medium-7 small-7 columns">
                    <h3>Somministrazione</h3>
                </div>
            </div>
            <div class="columns small-12">
                <div class="">
                    <div class="title">
                        <a href="<?= $this->backAction ?>">
                            <i class="fas fa-arrow-left"></i>
                        </a>
                        <?= _t($this->class["name"]) ?> - <?= _t($this->testData["name"]) ?>
                    </div>
                    <div class="content">
                        <?php
                        $json = json_encode([
                                "readOnly" => $this->status != SomministrazioneStatus::OPEN || !UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_CARICAMENTO_RISULTATI),
                                "data" => $usersResults,
                                "noteCompilazione" => $this->testData["note_compilazione"],
                                "parameters" => $this->somministrazioneTest["parameters"] ? json_decode($this->somministrazioneTest["parameters"]) : null,
                                "soglie" => $soglie,
                                "backAction" => $this->backAction,
                                "submitAction" => $this->actionUrl("submit", array_merge(
                                    ["testId" => $this->testId],
                                    $this->actionParameters)
                                ),
                                "bag" => [
                                    "somministrazione" => $this->somministrazione
                                ]
                            ]);
                        ?>
                        <div class="mb-test-<?= _t($this->testData["code"]) ?>-1"></div>
                        <script>
                            MbCreateVue("mb-test-<?= _t($this->testData["code"]) ?>", "mb-test-<?= _t($this->testData["code"]) ?>-1", <?= $json ?>);
                        </script>
                    </div>
                </div>        
                
                <?= $this->drawFormNoteAmministratore() ?>
                
            </div>
        </div>
        <?php
        $this->footer();
    }

    private function drawFormNoteAmministratore(){
        $form = new Form($this->actionUrl("_default", array_merge(
            $this->actionParameters,
            ["testId" => $this->testId]
        )));
        $form->entityName = "somministrazione_test_result_class";

        $somministrazioneTestResultClass = SomministrazioneTestResultClassService::findForTestAndClass($this->somministrazioneTest["somministrazione_test_id"], $this->class["structure_class_id"]);
        if ($somministrazioneTestResultClass && $somministrazioneTestResultClass["send_notice"] == 1){
            if (!UserRoleService::canCurrentUserDo("/admin/test/")) {
                SomministrazioneTestResultClassViewService::addToSomministrazioneClassTestResult(
                    $somministrazioneTestResultClass["somministrazione_test_result_class_id"],
                    $this->currentUser["user_id"]
                );
            }
        }

        $form->entity = $somministrazioneTestResultClass;

        $stestClassId = $form->entity == null ? null : $form->entity["somministrazione_test_result_class_id"];

        $form->addField(new FormTextareaField("Note", "note", FormFieldType::STRING));

        if (UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_NOTE_SOMMINISTRAZIONE_EDIT)){
            $sendNoticeField = new FormCheckField("Invia notifica", "send_notice", FormFieldType::NUMBER, [1 => 'Sì']);
            $sendNoticeField->readOnly = $form->entity && $form->entity["send_notice"] == 1;
            $form->addField($sendNoticeField);
        }
        
        $attachmentFieldMateriali = new FormAttachmentsField("Materiali", 
            AttachmentType::SOMMINISTRAZIONE_TEST_RESULT_CLASS_MATERIALI, 
            EntityName::SOMMINISTRAZIONE_TEST_RESULT_CLASS, 
            $stestClassId);
        $form->addField($attachmentFieldMateriali);

        if (!UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_NOTE_SOMMINISTRAZIONE_EDIT)){
            $form->displaySubmit = false;
            foreach ($form->fields as $field) {
                $field->readOnly = true;
            }
        }

        if ($form->isSubmit() && $form->checkFields()){
            if (!UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_NOTE_SOMMINISTRAZIONE_EDIT)){
                redirectWithMessage(UriService::accessDanyActionUrl(), "Impossibile proseguire");
                return;
            }

            if($stestClassId == null){
                $stestClassId = $form->persist("", [
                    "somministrazione_test_id" => $this->somministrazioneTest["somministrazione_test_id"],
                    "structure_class_id" => $this->class["structure_class_id"],
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser['user_id']
                ]);
            } else {
                $form->persist("somministrazione_test_result_class_id=" . $form->entity["somministrazione_test_result_class_id"]);
            }
            
            EM::updateEntity("somministrazione_test_result_class", [
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser['user_id']
            ], [
                "somministrazione_test_result_class_id" => $stestClassId
            ]);

            $attachmentFieldMateriali->persist($stestClassId);

            if ($somministrazioneTestResultClass["send_notice"] == 0 && $sendNoticeField->getValue(null) == 1){
                NoticeService::persist([
                    "type" => NoticeType::MONDO_PAROLE_CAMPO_NOTE,
                    "parameters" => json_encode($form->entity),
                    "entity" => "somministrazione_test_result_class",
                    "entity_id" => $stestClassId
                ]);
            }

            redirectWithMessage($this->actionUrl("_default", array_merge(
                $this->actionParameters,
                ["testId" => $this->testId]
            )), "Operazione eseguita");
        }

        ?>
        <div class="row-100" style="margin-top:20px;">
            <div class="columns small-8">
                <div class="title">
                    <a href="<?= $this->backAction ?>">
                        <i class="fas fa-arrow-left"></i>
                    </a>
                    Note risultati
                </div>
                <div class="content">
                    <?php $form->draw() ?>
                </div>
            </div>        

            <?php if (UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_NOTE_SOMMINISTRAZIONE_EDIT)){ 
                $users = null;
                if ($somministrazioneTestResultClass){
                    $users = SomministrazioneTestResultClassViewService::findForSomministrazioneTestClassResult($somministrazioneTestResultClass["somministrazione_test_result_class_id"]);
                }
                ?>
            <div class="columns small-4">
                <div class="title">
                    Utenti che hanno visualizzato la pagina dopo il salvataggio delle note
                </div>
                <div class="content">
                    <?php if ($users == null || $users->rowCount() == 0) { ?>
                        <p style="padding: 10px;">Nessuna visualizzazione registrata.</p>
                    <?php } else { ?>
                        <table style="width:100%;">
                            <thead>
                                <tr>
                                    <th>Data</th>
                                    <th>Utente</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($users as $user) { ?>
                                <tr>
                                    <td><?= _t($user['v_insert_date']) ?></td>
                                    <td><?= _t($user["name"]) ?> <?= _t($user["surname"]) ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                </div>
            </div> 
            <?php } ?>
        </div>
        <?php
    }

    public function submit(){
        $request = json_decode(file_get_contents('php://input'));

        if ($request == null){
            echo json_encode([
                "success" => false,
                "errors" => [
                    "Richiesta non valida"
                ]
            ]);
            die();
        }

        if (!is_array($request->students)){
            echo json_encode([
                "success" => false,
                "errors" => [
                    "Dati studenti non trasmessi correttamente"
                ]
            ]);
            die();
        }

        foreach($request->students as $student){
            EM::deleteEntity("somministrazione_test_result_user", [
                "somministrazione_test_id" => $this->somministrazioneTest["somministrazione_test_id"],
                "user_id" => $student->user->user_id,
            ]);

            $struId = EM::insertEntity("somministrazione_test_result_user", [
                "somministrazione_test_id" => $this->somministrazioneTest["somministrazione_test_id"],
                "user_id" => $student->user->user_id,
                "status" => $student->status,
                "raw_data" => json_encode($student->raw_data),
                "insert_user_id" => $this->currentUser["user_id"],
                "insert_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser["user_id"],
                "last_edit_date" => "NOW()"
            ]);

            if (property_exists($student, "nonValidaAttachments")){
                AttachmentService::persist($student->nonValidaAttachments, 
                    EntityName::SOMMINISTRAZIONE_TEST_RESULT_USER, 
                    $struId, 
                    AttachmentType::SOMMINISTRAZIONE_TEST_RESULT_USER_NON_VALIDA);
            }

            $soglie = SomministrazioneService::findSoglieForSomministrazioneTest($this->somministrazioneTest["somministrazione_test_id"], $this->test->getVariablesNames());

            foreach($student->scores as $varName => $value){
                if ($student->status == SomministrazioneUserStatus::ASSENTE || $value === null){
                    continue;
                } 

                $fascia = SomministrazioneService::evaluateScore($soglie, $varName, $value);
                
                EM::insertEntity("somministrazione_test_result_user_variable", [
                    "somministrazione_test_result_user_id" => $struId,
                    "variable_name" => $varName,
                    "value" => $value,
                    "fascia" => $fascia,
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser["user_id"],
                    "last_edit_date" => "NOW()",
                    "last_edit_user_id" => $this->currentUser["user_id"],
                    "row_status" => RowStatus::READY
                ]);
            }
        }
        
        echo json_encode([
            "success" => true
        ]);
    }

}