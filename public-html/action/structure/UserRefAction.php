<?php
/*
---------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserRefAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserRefAction extends StructureAction{
    
    public $email;
    public $userId;
    public $role_id;
    public $sub_substructure_id;
    public $step;
    private $user;
    
    public function _prepare() {
        parent::_prepare();
        
        if (StringUtils::isNotBlank($this->userId)){
            $this->user = UserService::find($this->userId);
            if ($this->user == NULL || !StructureService::hasUserStructure($this->userId, $this->structureId)){
                redirect(UriService::accessDanyActionUrl());
            }
        }
    }

    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());
        
        $this->header();
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row-100">
                    <div class="large-8 medium-7 columns">
                        <h3>Elenco degli utenti</h3>
                    </div>
                    <div class="large-4 medium-3 columns">
                        <a href="<?= $this->actionUrl("edit") ?>" class="button radius nuovo float-right">
                            <i class="fa fa-plus-circle"></i> Nuovo
                        </a>
                    </div>
                </div>
                <div class="table-row row-100">
                    <div class="large-12 columns">
                        <div class="mb-structure-user-ref-table"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function buildTable($exportExcel = true, $paginateElements = true){
        
        if (StringUtils::isNotBlank($this->filter_email)){
            $query .= "AND email LIKE :email ";
            $pars["email"] = "%" . $this->filter_email . "%";
        }

        if (StringUtils::isNotBlank($this->filter_name)){
            $query .= "AND (name LIKE :name OR surname LIKE :name2) ";
            $pars["name"] = "%" . $this->filter_name . "%";
            $pars["name2"] = "%" . $this->filter_name . "%";
        }

        if (StringUtils::isNotBlank($this->filter_role_id)){
            $query .= "AND usr.role_id = :role_id ";
            $pars["role_id"] = $this->filter_role_id;
        }

        if (StringUtils::isNotBlank($this->filter_referent_for_id)){
            $query .= "AND usm_referer.value=:referer_for ";
            $pars["referer_for"] = $this->filter_referent_for_id;
        }

        if (!$table->isExportingExcel){
            $table->addColumn(new TableColumn("user_id", "", function($v) use ($portfolioEnabled){
                $out = '';
                
                
                if ($this->canLoggedUserManageUser($v)){
                    $out .= Formatter::buildTool("", $this->actionUrl("edit", ["userId" => $v]), Icon::PENCIL);
                    $out .= Formatter::buildTool("", $this->actionUrl("remove", ["userId" => $v]), Icon::TRASH, "Rimuovere l\'elemento?");
                }
                return $out;
            }, "width:150px;"));
        }

        return $table;
    }
    
    private function canLoggedUserManageUser($userId){
        if (UserRoleService::hasCurrentUser(UserRole::ADMIN) ||
            UserRoleService::hasCurrentUser(UserRole::AMMINISTRATIVI) || 
                UserRoleService::hasCurrentUser(UserRole::DIRIGENTE) || (
                        UserRoleService::hasCurrentUser(UserRole::REFERENTE_BES) && 
                        StructureService::hasUserStructureRole($userId, $this->structureId, UserRole::INSEGNANTE_PEI_PDP))){
            return true;
        }
        return false;
    }
    
    public function remove(){
        if (StringUtils::isBlank($this->userId)){
            redirect(UriService::accessDanyActionUrl());
        }
        if (!$this->canLoggedUserManageUser($this->userId)){
            redirect(UriService::accessDanyActionUrl());
        }
        
        StructureService::removeUserStructure($this->userId, $this->structureId);
        
        redirect ($this->actionUrl("_default"));
    }
    
    public function edit(){
        if ($this->step == 0 && StringUtils::isBlank($this->userId)){
            $form = $this->stepShort();
        } else {
            $form = $this->stepFull();
        }
        
        $this->header();
        ?>
        <div class="tabs-content">
            <div class="content active" id="panelTabStudenti">
                <div class="section-title row">
                    <div class="large-6 medium-4 columns">
                        <h3>Dati utente</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="large-12 medium-12 small-12 columns">
                        <?php $form->draw(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    private function stepShort(){
        $par = ['userId' => $this->userId, "step" => $this->step];
        
        $form = new Form($this->actionUrl('edit', $par));
        $form->cancellAction = $this->actionUrl("_default");
        
        $emailField = new FormTextField("e-mail", "email", FormFieldType::STRING, true, "", ["maxLength" => 255]);
        $emailField->note = "Inserendo l'email usata per l'account di Office365 sarà possibile accedere utilizzando le credenziali LASIS (Office 365).";
        $emailField->checkFunction = function($field, $value){
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $field->addFieldError("Indirizzo mail non valido.");
                return false;
            }

            $userDb = null;
            if (StringUtils::isNotBlank($value)){
                $userDb = UserService::findUserByEmail($value);
                if ($userDb != null){
                    if ($userDb["status"] != UserStatus::ENABLED){
                        $field->addFieldError("Utente disabilitato.");
                        return false;
                    }

                    $isAlreadyInStructure = StructureService::hasUserStructure($userDb['user_id'], $this->structureId, true);
                    if ($isAlreadyInStructure){
                        $field->addFieldError("Utente già associato alla scuola.");
                        return false;
                    }
                }    
            }
            return true;
        };
        $form->addField($emailField);

        if ($form->isSubmit() && $form->checkFields()){
            $userDb = UserService::findUserByEmail($this->email);
            if ($userDb != null){
                StructureService::addUserStructure($userDb["user_id"], $this->structureId, [UserRole::INSEGNANTE]);
                redirect($this->actionUrl("edit", ["userId" => $userDb["user_id"]]));
                return;
            } else {
                $_REQUEST['submit_check'] = NULL;
                return $this->stepFull();                
            }
        }
        
        return $form;
    }
    
    private function getRoles(){
        $portfolioEnabled = PropertyService::find(PropertyNamespace::PORTFOLIO_INSEGNANTI, PropertyKey::ENABLED);
        $educazioneSaluteEnabled = PropertyService::find(PropertyNamespace::EDUCAZIONE_SALUTE, PropertyKey::ENABLED);
        $eModelEnabled = PropertyService::find(PropertyNamespace::MODELLO_E, PropertyKey::ENABLED);
        $dropOutEnabled = PropertyService::find(PropertyNamespace::DROP_OUT, PropertyKey::ENABLED);
        
        $result = UserRoleService::findAllWithLevel(UserRole::LEVEL_CLIENT);
        $values = [];
        foreach ($result as $row){
            if ($row["role_id"] == UserRole::STUDENTE){
                continue;
            }
            
            if (!$portfolioEnabled['value'] && $row["role_id"] == UserRole::INSEGNANTE_ANNO_DI_PROVA){
                continue;
            }
            
            if (!$dropOutEnabled['value'] && 
                (
                    $row["role_id"] == UserRole::CORDINATORE_DROP_OUT ||
                    $row["role_id"] == UserRole::CORDINATORE_PLESO_DROP_OUT
                )){
                continue;
            }

            if (!$educazioneSaluteEnabled['value'] && (
                    $row["role_id"] == UserRole::REFERENTE_EDUCAZIONE_SALUTE ||
                    $row["role_id"] == UserRole::REFERENTE_CYBERBULLISMO
                    )){
                continue;
            }
            
            if (!$eModelEnabled['value'] && $row["role_id"] == UserRole::REFERENTE_MODELLO_E){
                continue;
            }
            
            $values[$row['role_id']] = $row['name'];
        }

        return $values;
    }
    
    private function stepFull(){
        $this->step = 1;
        
        $par = ['userId' => $this->userId, "step" => $this->step];
        $form = new Form($this->actionUrl('edit', $par));
        $form->entityName = "user";
        $form->cancellAction = $this->actionUrl("_default");
        
        if (StringUtils::isNotBlank( $this->userId )){
            $form->entity = UserService::find($this->userId);
        }
        
        $emailField = new FormTextField("e-mail", "email", FormFieldType::STRING, false);
        if (StringUtils::isNotBlank($this->userId)){
            $emailField->readOnly = true;
        }
        
        $emailField->note = "Inserendo l'email usata per l'account di Office365 sarà possibile accedere utilizzando le credenziali LASIS (Office 365).";
        $emailField->checkFunction = function($field, $value){
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $field->addFieldError("Indirizzo mail non valido.");
                return false;
            }

            $userDb = null;
            if (StringUtils::isNotBlank($value)){
                $userDb = UserService::findUserByEmail($value);
                if ($userDb != null){
                    if ($userDb["status"] != UserStatus::ENABLED){
                        $field->addFieldError("Utente disabilitato.");
                        return false;
                    }

                    if ($userDb["user_id"] != $this->userId){
                        $isAlreadyInStructure = StructureService::hasUserStructure($userDb['user_id'], $this->structureId);
                        if ($isAlreadyInStructure){
                            $field->addFieldError("Utente già associato all'IC.");
                            return false;
                        }
                    }
                }    
            }
            return true;
        };
        $form->addField($emailField);

        $roles = $this->getRoles();
        $roleOptions = [];
        foreach($roles as $k => $role){
            $roleOptions[] = [
                "value" => $k,
                "label" => $role
            ];
        }

        function cmp($a, $b) {
            return strcmp($a['label'], $b['label']);
        }
        usort($roleOptions, "cmp");

        $selectedRoles = [];
        if (StringUtils::isNotBlank($this->userId)){
            $userStructure = StructureService::findUserStructureRole($this->userId, $this->structureId);
            foreach ($userStructure as $us){
                array_push($selectedRoles, $us['role_id']);
            }
        }
        
        $roleField = new FormCheckboxMultiField("Ruolo", "role_id", FormFieldType::NUMBER, [], true, $selectedRoles);
        $roleField->options = $roleOptions;
        $roleField->persist = false;
        $form->addField($roleField);
        
        $form->addField(new FormTextField("Nome", "name", FormFieldType::STRING, true, "", ["maxLength" => 255]));
        $form->addField(new FormTextField("Cognome", "surname", FormFieldType::STRING, true, "", ["maxLength" => 255]));
        $form->addField(new FormTextField("Telefono", "phone_number", FormFieldType::STRING, false, "", ["maxLength" => 45]));
        
        $schoolGrades = DictionaryService::findGroupAsDictionary(Dictionary::SCHOOL_GRADE);
        $selectedSchoolGrades = [];
        if (StringUtils::isNotBlank($this->userId)){
            $mvs = MetadataService::findMetadataValue(MetadataEntity::USER, $this->userId, UserMetadata::SCHOOL_GRADE);
            foreach($mvs as $mv){
                $selectedSchoolGrades[] = $mv["value"];
            }
        }
        $schoolGradeField = new FormCheckboxMultiField("Grado scolastico di competenza", "school_grades", FormFieldType::STRING, $schoolGrades, true, $selectedSchoolGrades);
        $schoolGradeField->persist = false;
        $form->addField($schoolGradeField);
        
        $structureField = $this->buildPlessiSelectorField($form);
        
        if ($form->isSubmit() && $form->checkFields()){
            $isNew = $this->userId == NULL;

            if (StringUtils::isBlank($this->userId)){
                $user = UserService::findUserByEmail($this->email);
                if ($user != null){
                    $this->userId = $user['user_id'];
                } else {
                    $this->userId = $form->persist();
                    NoticeService::persist([
                        "type" => NoticeType::USER_ADD,
                        "parameters" => json_encode(['user_id' => $this->userId])
                    ]);
                    UserRoleService::addUserRole($this->userId, UserRole::USER);
                }

                $par = [];
                $par['insert_date'] = 'NOW()';
                $par['created_by_user_id'] = $this->currentUser['user_id'];
                EM::updateEntity("user", $par, sprintf("user_id=%d", $this->userId));
            } else {
                $form->persist(sprintf("user_id=%d", $this->userId));
            }

            $password = getRequestValue('password_1');
            if (StringUtils::isNotBlank($password)){
                UserService::updatePassword($this->userId, $password);
            }

            MetadataService::removeMetadataWithKey(MetadataEntity::USER, $this->userId, [UserMetadata::SCHOOL_GRADE => 0]);
            $grades = $schoolGradeField->getValue($form->entity);
            MetadataService::persist(MetadataEntity::USER, $this->userId, [UserMetadata::SCHOOL_GRADE => $grades], false, false);

            $userStructure = StructureService::findUserStructure($this->userId, $this->structure["structure_id"]);
            if ($userStructure == null){
                StructureService::addUserStructure($this->userId, $this->structureId, null, $isNew);
                $userStructure = StructureService::findUserStructure($this->userId, $this->structure["structure_id"]);
            }

            StructureService::deleteAlLUserStructureRole($userStructure["user_structure_id"]);
            if (is_array($this->role_id)){
                foreach ($this->role_id as $roleId) {
                    StructureService::addUserStructureRole($userStructure["user_structure_id"], $roleId);
                }
            } 

            $subStructures = $structureField->getDecodedValue();
            if (is_array($subStructures)){
                foreach ($subStructures as $structure) {
                    if ($structure["checked"]){
                        $userStructure = StructureService::findUserStructure($this->userId, $structure["id"]);

                        if ($userStructure == null){
                            $userStructureId = StructureService::addUserStructure($this->userId, $structure["id"], null, false);
                        } else {
                            $userStructureId = $userStructure["user_structure_id"];
                        }

                        MetadataService::removeMetadataWithKey(MetadataEntity::USER_STRUCTURE, $userStructureId, [UserStructureMetadata::REFERENT_FOR => 0]);
                        if (isset($structure["roles"]) && is_array($structure["roles"])){
                            foreach($structure["roles"] as $role){
                                MetadataService::persistMetadataValue(MetadataEntity::USER_STRUCTURE, $userStructureId, UserStructureMetadata::REFERENT_FOR, $role["value"], false);
                            }
                        }
                    } else {
                        StructureService::removeUserStructure($this->userId, $structure["id"]);
                    }
                }
            }

            redirect( $this->actionUrl('_default') );
        }
        
        return $form;
    }

    private function buildPlessiSelectorField($form){
        $references = DictionaryService::findGroupAsDictionary(Dictionary::STRUCTURE_REFERENT);

        $subStructures = StructureService::findAllWithParent($this->structureId);
        $structureValues = [];
        $selectedStructures = [];
        $userStructures = [];
        if (StringUtils::isNotBlank($this->userId)){
            $stmt = StructureService::findUserStructureWithParent($this->userId, $this->structureId);
            $userStructures = $stmt->fetchAll();
        }
        
        foreach($subStructures as $value){
            $structureValues[] = [
                "id" => $value["structure_id"],
                "name" => StructureService::printFullStructureName($value["structure_id"]),
                "checked" => $this->structureContains($userStructures, $value["structure_id"]),
                "roles" => $this->findUserStructureReferenceFor($value["structure_id"])
            ];
        }
        
        $structureField = new FormCustomContentField("Plessi", "sub_substructure_id", FormFieldType::NUMBER, true, $structureValues);
        $structureField->persist = false;
        $structureField->drawFn = function($field) use ($structureValues, $selectedStructures, $references){
            $updatedReferences = [];
            foreach ($references as $key => $text) {
                $updatedReferences[] = [
                    "text" => $text,
                    "value" => $key
                ];
            }

            $value = $field->getDecodedValue();
            if ($value == null){
                $value = [];
                foreach ($structureValues as $s){
                    $value[] = [
                        "id" => $s["id"],
                        "checked" => false
                    ];
                }
            }

            $options = [
                'inputName' => $field->name,
                'structures' => $structureValues,
                'roles' => $updatedReferences,
                'values' => $value
            ];

            ?>
            <div id="structure-ref-<?= $field->name ?>" class="structure-ref-<?= $field->name ?>"></div>
            <script type="text/javascript">
            MbCreateVue("mb-input-structure-ref", "structure-ref-<?= $field->name ?>", <?= json_encode($options) ?>);
            </script>
            <?= $field->drawErrorTag() ?>
            <?php
        };
        $structureField->checkFn = function($field){
            $values = $field->getDecodedValue();
            if ($values == null && !is_array($values)){
                return;
            }

            foreach($values as $value){
                if ($value["checked"]){
                    return;
                }
            }

            $field->addFieldError("Selezionare almeno un valore");
        };
        $form->addField($structureField);

        return $structureField;
    }

    private function findUserStructureReferenceFor($structureId){
        $userStructure = StructureService::findUserStructure($this->userId, $structureId);
        if ($userStructure == null){
            return [];
        }

        $metadatas = MetadataService::findMetadataValue(MetadataEntity::USER_STRUCTURE, $userStructure["user_structure_id"], UserStructureMetadata::REFERENT_FOR);
        $roles = [];
        foreach($metadatas as $mv){
            $dictionary = DictionaryService::findByGroupAndKey(Dictionary::STRUCTURE_REFERENT, $mv["value"]);
            $roles[] = [
                "value" => $mv["value"],
                "text" => $dictionary["value"],
            ];
        }
        return $roles;
    }

    private function structureContains($structures, $structureId){
        foreach($structures as $structure){
            if ($structure["structure_id"] == $structureId){
                return true;
            }
        }
        return false;
    }
}
