<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once(__DIR__ . '/PeiTirocinioBaseAction.php');

class Request extends RequestBaseClass {
    public $aziende = [];
}

/**
 * Description of AziendaAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class AziendaAction extends PeiTirocinioBaseAction{
    
    public function _default(){
        $data = new Request();

        if (isset($this->userMetadata[Metadata::PEI_TIROCINIO_AZIENDA])){
            try{
                $data = json_decode( $this->userMetadata[Metadata::PEI_TIROCINIO_AZIENDA] );
            } catch(Exception $e){
                LogService::error_($this, "Errore recupero dati", $ex);
            }
        }
        
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], UriService::buildPageUrl("/structure/UserAction", null, [
            "documentId" => $this->documentId
        ])));
        $this->bradcrumbs->add(new Bradcrumb("Azienda"));

        $this->header();
        ?>
        <div class="content active tab-nuovo-studente row" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
            </div>
            <div class="sezioni">
                <div class="mb-pei-tirocinio-azienda-container"></div>
                <script>
                    MbCreateVue("MbPeiTirocinioAzienda", "mb-pei-tirocinio-azienda-container", <?= json_encode([
                        "data" => $data,
                        "documentId" => $this->documentId
                    ]) ?>);
                </script>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    public function submit(){
        if ($this->getRequestMethod() != RequestMethod::POST){
            $this->notFound();
        }

        $request = new Request();
        $request->set($this->getBodyRequest());

        MetadataService::removeMetadataWithKey(MetadataEntity::USER, $this->user['user_id'], [Metadata::PEI_TIROCINIO_AZIENDA => 1]);
        MetadataService::persist(MetadataEntity::USER, $this->user["user_id"], [
            Metadata::PEI_TIROCINIO_AZIENDA => $request
        ]);

        $this->updateMetadata();
        $this->ok();
    }

}
