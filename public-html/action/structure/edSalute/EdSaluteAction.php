<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EdSaluteAction extends StructureAction{
    
    public $id;
    
    public function _default(){
        $schoolYears = SchoolYearService::findAllAsObject();
        $currentSchoolYear = SchoolYearService::findCurrentYear();

        $this->menu->setSelectedMenuItem($this->actionUrl());
        
        $status = DictionaryService::findGroupAsDictionary(Dictionary::ED_SALUTE_STATUS);
        $schoolGrades = DictionaryService::findGroupAsDictionary(Dictionary::SCHOOL_GRADE);
        $types = DictionaryService::findGroupAsDictionary(Dictionary::ED_SALUTE_TYPE);
        
        $table = new Table();
        $table->entityName = "ed_salute";
        $table->addWhere("structure_id=" . $this->structureId);
        $table->addWhere("row_status<>'" . RowStatus::DELETED . "'");
        $table->addColumn(new TableColumn("plesso_structure_id", "Plesso", function($v){
            return StructureService::printFullStructureName($v);
        }));
        $table->addColumn(new TableColumn("title", "Titolo"));
        $table->addColumn(new TableColumn("school_grade_dictionary_id", "Grado", function($v) use ($schoolGrades){
            return isset($schoolGrades[$v]) ? $schoolGrades[$v] : "";
        }));
        $table->addColumn(new TableColumn("ed_salute_type", "Ambito", function($v) use ($types){
            return isset($types[$v]) ? $types[$v] : "";
        }));
        $table->addColumn(new TableColumn("status_dictionary_id", "Stato", function($v) use ($status){
            return isset($status[$v]) ? $status[$v] : "";
        }));
        $table->addColumn(new TableColumn("insert_date", "Inserito il"));
        $table->addColumn(new TableColumn("insert_user_id", "Creato da", function($v){
            $user = UserService::find($v);
            if ($user == null){
                return "";
            }
            return $user['name'] . ' ' . $user['surname'];
        }));
        $table->addColumn(new TableColumn("ed_salute_id", "", function($id){
            $out = Formatter::buildTool("", $this->actionUrl("edit").'?id='.$id, "fas fa-pencil-alt");
            $out .= Formatter::buildTool("", $this->actionUrl("remove").'?id='.$id, "fas fa-trash-alt", "Rimuovere il progetto?");
            return $out;
        }));

        $table->addFilter(new TableFilter("school_year_id", "=", "", $currentSchoolYear["school_year_id"] ));
            
        $this->header();
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row-100">
                    <div class="large-8 medium-7 columns">
                        <h3>Progetti Educazione alla Salute</h3>
                    </div>
                    <div class="large-4 medium-3 columns">
                        <a href="<?= $this->actionUrl("edit") ?>" class="button radius nuovo float-right">
                            <i class="fa fa-plus-circle margin-right"></i>Nuovo
                        </a>
                    </div>
                </div>
                
                <div class="table-row row-100">
                    <div class="large-12 columns">
                        <div class="filters">
                            <form method="POST" action="<?= $this->actionUrl() ?>">
                                <div class="row-100 margin-bottom">
                                    <div class="columns small-12 medium-6 large-4">
                                        <?= FilterUtils::drawSelectFilter("Anno scolastico", "filter_school_year_id", $schoolYears, $currentSchoolYear['school_year_id']) ?>
                                    </div>
                                </div>

                                <?= FilterUtils::drawSubmit() ?>
                            </form>          
                        </div>
                    </div>
                    <div class="large-12 columns">
                        <?php $table->draw(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        
        $this->footer();
    }
    
    public function remove(){
        if(StringUtils::isBlank($this->id)){
            redirect($this->actionUrl("_default"));
        }
        
        $progetto = EdSaluteService::find($this->id);
        if ($progetto == null || $progetto['structure_id'] != $this->structure['structure_id']){
            redirect(UriService::accessDanyActionUrl());
        }
        
        EM::updateEntity("ed_salute", ["row_status" => RowStatus::DELETED], ['ed_salute_id' => $this->id]);
        redirectWithMessage($this->actionUrl("_default"), "Progetto rimosso");
    }

    public function edit(){
        $form = new Form($this->actionUrl("edit", ["id" => $this->id]));
        $form->cancellAction = $this->actionUrl("_default");
        $form->entityName = "ed_salute";
        
        $form->entity = EdSaluteService::find($this->id);
        
        $currentSchoolYear = SchoolYearService::findCurrentYear();
        $schoolYearsDb = SchoolYearService::findAllAsObject();
        $schoolYears = [];
        foreach ($schoolYearsDb as $id => $label){
            if ($id < $currentSchoolYear["school_year_id"]){
                continue;
            }
            
            $schoolYears[$id] = $label;
        }
        
        $form->addField(new FormSelectField("Anno scolastico", "school_year_id", FormFieldType::STRING, $schoolYears, true));
        
        $plessi = StructureService::findAllWithParent($this->structureId);
        $values = ["" => ""];
        foreach ($plessi as $plesso) {
            $values[$plesso["structure_id"]] = $this->structure["name"] . " - " . $plesso["name"];
        }
        $form->addField(new FormSelectField("Plesso", "plesso_structure_id", FormFieldType::NUMBER, $values, true));

        $form->addField(new FormTextField("Numero alunni plesso", "plesso_num_alunni", FormFieldType::NUMBER, true, "", ["numeric" => 1]));
        $form->addField(new FormTextField("Titolo", "title", FormFieldType::STRING, true, "", ["maxLength" => 255]));
        
        $schoolGrades = DictionaryService::findGroupAsDictionary(Dictionary::SCHOOL_GRADE, false, "key", true);
        $form->addField(new FormSelectField("Grado scolastico", "school_grade_dictionary_id", FormFieldType::STRING, $schoolGrades, true));
        
        $status = DictionaryService::findGroupAsDictionary(Dictionary::ED_SALUTE_STATUS, false, "key", true);
        $form->addField(new FormSelectField("Stato", "status_dictionary_id", FormFieldType::STRING, $status, true));
        
        $types = DictionaryService::findGroupAsDictionary(Dictionary::ED_SALUTE_TYPE, false, "key", true);
        $form->addField(new FormSelectField("Ambito", "ed_salute_type", FormFieldType::STRING, $types, true));
        $form->addField(new FormTextField("Altro", "type_other", FormFieldType::STRING, "", ["maxLength" => 255]));
        
        $propostoDa = DictionaryService::findGroupAsDictionary(Dictionary::PROPOSTO_DA, false, "key", true);
        $form->addField(new FormSelectField("Proposto da", "proposto_da", FormFieldType::STRING, $propostoDa, true));
                
        $form->addField(new FormTextField("Numero classi coinvolte", "num_classi", FormFieldType::NUMBER, true, "", ["numeric" => 1]));
        $form->addField(new FormTextField("Numero alunni coinvolti", "num_alunni", FormFieldType::NUMBER, true, "", ["numeric" => 1]));
        $form->addField(new FormTextField("Numero ore annuali", "num_ore_annuali", FormFieldType::NUMBER, true, "", ["numeric" => 1]));
        
        $form->addField(new FormCheckField("Costi a carico della scuola", "costi_carico_scuola", FormFieldType::NUMBER, [1 => "Sì"]));
        
        $attachmentFieldDescrizione = new FormAttachmentsField("Descrizione del progetto", 
            AttachmentType::ED_SALUTE_DESCRIZIONE_PROGETTO, EntityName::ED_SALUTE, $this->id, true);
        $form->addField($attachmentFieldDescrizione);

        $attachmentField = new FormAttachmentsField("Delibera del Consiglio di Istituto - approvazione finanziaria del progetto", 
                AttachmentType::ED_SALUTE_APPROVAZIONE_FINANZIARIA, EntityName::ED_SALUTE, $this->id, false);
        $attachmentField->note = "Da inserire dopo l'approvazione.";
        $form->addField($attachmentField);
        
        $form->addField(new FormCheckField("Progetto pluriennale", "progetto_pluriennale", FormFieldType::NUMBER, [1 => "Sì"]));
        $form->addField(new FormCheckField("Progetto già proposto negli anni precedenti", "progetto_anni_precedenti", FormFieldType::NUMBER, [1 => "Sì"]));
        
        $form->addField(new FormTextareaField("Osservazioni/Note", "note", FormFieldType::STRING));
        
        if ($form->isSubmit() && $form->checkFields()){
            if(StringUtils::isBlank($this->id)){
                $this->id = $form->persist("", [
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser['user_id'],
                    "structure_id" => $this->structure["structure_id"],
                    "row_status" => RowStatus::READY
                ]);
            } else {
                $form->persist("ed_salute_id=" . $this->id);
            }
            
            EM::updateEntity("ed_salute", [
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser['user_id']
            ], ["ed_salute_id" => $this->id]);
            
            $attachmentField->persist($this->id);
            $attachmentFieldDescrizione->persist($this->id);
            
            redirectWithMessage($this->actionUrl("_default"), "Progetto salvato");
        }
        
        $this->header();
        ?>
        <script type="text/javascript">
            $(function(){
               $('#ed_salute_type').on('change', function(){
                   edSaluteTypeChange();
               });
               edSaluteTypeChange();
               
               function edSaluteTypeChange(){
                   var val = $('#ed_salute_type').val();
                   $('#campo_form_type_other').hide();
                   if (val == 'altro'){
                        $('#campo_form_type_other').show();    
                   }
               }
            });
        </script>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row">
                    <div class="large-10 medium-9 small-7 columns">
                        <h3>Progetti Educazione alla Salute: Modifica Progetto</h3>
                    </div>
                </div>
                <div class="table-row row">
                    <div class="large-12 columns">
                        <?php $form->draw(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }
}
