<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PortfolioBaseAction
 *
 * @author marco
 */
class PortfolioBaseAction extends StructureAction{
    
    public $userId;
    public $user;
    
    public function _prepare(){
        $this->setGlobalSessionValue("documentType", DocumentType::PORTFOLIO_INSEGNANTI);
        parent::_prepare();
        
        if (StructureService::hasUserStructureRole($this->currentUser['user_id'], $this->structureId, UserRole::INSEGNANTE_ANNO_DI_PROVA)){
            $this->user = $this->currentUser;
        } else {
            if (StringUtils::isNotBlank($this->userId)){
                $this->user = UserService::find($this->userId);
                if ($this->user == NULL){
                    redirect(UriService::accessDanyActionUrl());
                    return;
                }

                $hasRole = StructureService::hasUserStructureRole($this->user['user_id'], $this->structureId, UserRole::INSEGNANTE_ANNO_DI_PROVA);
                if (!$hasRole){
                    redirect(UriService::accessDanyActionUrl());
                    return;
                }

                if (UserRoleService::hasCurrentUser(UserRole::INSEGNANTE_ANNO_DI_PROVA) && $this->userId != $this->currentUser['user_id']){
                    redirect(UriService::accessDanyActionUrl());
                    return;
                }

                $this->setGlobalSessionValue("structure_portfolio_user", $this->user);
            }

            $this->user = $this->getGlobalSessionValue("structure_portfolio_user", $this->user);
            if ($this->user == null){
                redirect(UriService::accessDanyActionUrl());
                return;
            }
        }
    }
    
}
