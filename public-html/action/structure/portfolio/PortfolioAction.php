<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'PortfolioBaseAction.php';

class PortfolioAction extends PortfolioBaseAction {

    const TAB_QUALIFICAZIONE = "qualificazione";
    const TAB_SVILUPPO = "sviluppo";
    const TAB_ANNO_DI_PROVA = "anno_di_prova";
    
    public $ajax;
    public $type;
    public $attachmentId;
    public $method;
    public $tab;
    
    public $dataSviluppo = [
            [
                "label" => "Attività svolte durante il periodo di inserimento professionale",
                "sections" => [
                    [
                        "title" => "Progettazione Documentazione (prova autentica)",
                        "description" => "",
                        "type" => AttachmentType::PORTFOLIO_ATTIVITA_1
                    ]
                ]
            ],
            [
                "label" => "Valutazione",
                "sections" => [
                    [
                        "title" => "Osservazioni reciproche in classe (griglia)",
                        "description" => "Riflessioni finali sul proprio sviluppo professionale",
                        "type" => AttachmentType::PORTFOLIO_VALUTAZIONE_2,
                        "downloads" => false
                    ]
                ]
            ]
        ];

    public $dataAnnoDiProva = [
            [
                "label" => "Analisi",
                "sections" => [
                    [
                        "title" => "Curricolo formativo",
                        "description" => "",
                        "type" => AttachmentType::PF_AP_ANALISI_1,
                        "downloads" => true
                    ],
                    [
                        "title" => "Bilancio iniziale delle competenze",
                        "description" => "",
                        "type" => AttachmentType::PF_AP_ANALISI_2,
                        "downloads" => true
                    ],
                    [
                        "title" => "Bilancio finale delle competenze e bisogni formativi futuri",
                        "description" => "",
                        "type" => AttachmentType::PF_AP_ANALISI_3,
                        "downloads" => true
                    ],
                    [
                        "title" => "Patto per lo sviluppo professionale",
                        "description" => "",
                        "type" => AttachmentType::PF_AP_ANALISI_4,
                        "downloads" => true
                    ]
                ]
            ],
            [
                "label" => "Documentazione",
                "sections" => [
                    [
                        "title" => "Partecipazioni a laboratori formativi/visiting ",
                        "description" => "",
                        "type" => AttachmentType::PF_AP_DOCUMENTAZIONE_1,
                        "downloads" => true
                    ],
                    [
                        "title" => "UDA Documentazione",
                        "description" => "",
                        "type" => AttachmentType::PF_AP_DOCUMENTAZIONE_3,
                        "downloads" => true
                    ],
                    [
                        "title" => "Griglie e relazioni",
                        "description" => "I documenti presenti in questa cartella possono essere solo scaricati e la trasmissione per la successiva archiviazione agli atti avverrà sulla base delle indicazioni fornite dal/dalla Dirigente Scolastico/a",
                        "type" => AttachmentType::PF_AP_DOCUMENTAZIONE_4,
                        "downloads" => true,
                        "uploadEnabled" => false
                    ],
                    [
                        "title" => "Materiali",
                        "description" => "",
                        "type" => AttachmentType::PF_AP_DOCUMENTAZIONE_5,
                        "uploads" => [
                            [
                                "title" => "Bisogni educativi speciali",
                                "description" => "",
                                "entity" => EntityName::SYSTEM,
                                "type" => AttachmentType::PORTFOLIO_MATERIALI_BES
                            ],
                            [
                                "title" => "Inclusione sociale, dinamiche interculturali, orientamento e successo formativo",
                                "description" => "",
                                "entity" => EntityName::SYSTEM,
                                "type" => AttachmentType::PORTFOLIO_MATERIALI_INCLUSIONE
                            ],
                            [
                                "title" => "Tedesco L2",
                                "description" => "",
                                "entity" => EntityName::SYSTEM,
                                "type" => AttachmentType::PORTFOLIO_MATERIALI_TEDESCO_L2
                            ],
                            [
                                "title" => "Tecnologie della didattica digitale e loro integrazione nel curricolo",
                                "description" => "",
                                "entity" => EntityName::SYSTEM,
                                "type" => AttachmentType::PORTFOLIO_MATERIALI_TECNOLOGIE_DIDATTICA
                            ],
                            [
                                "title" => "Valutazione per competenze e valutazione di sistema",
                                "description" => "",
                                "entity" => EntityName::SYSTEM,
                                "type" => AttachmentType::PORTFOLIO_MATERIALI_TECNOLOGIE_VALUTAZIONE
                            ]
                        ]
                    ]
                ]
            ]
        ];
    
    public $dataQualificazione = [
            [
                "label" => "Analisi",
                "sections" => [
                    [
                        "title" => "Curriculum",
                        "description" => "",
                        "type" => AttachmentType::PORTFOLIO_ANALISI_3,
                        "downloads" => false
                    ],
                    [
                        "title" => "Bilancio iniziale delle proprie competenze",
                        "description" => "",
                        "type" => AttachmentType::PORTFOLIO_ANALISI_4,
                        "downloads" => true
                    ],
                    [
                        "title" => "Autovalutazione sulle competenze (in uscita)",
                        "description" => "",
                        "type" => AttachmentType::PORTFOLIO_VALUTAZIONE_1,
                        "downloads" => true
                    ],
                ]
            ],
            [
                "label" => "Documentazione",
                "sections" => [
                    [
                        "title" => "Partecipazione alla formazione",
                        "description" => "<p>Documentazione della formazione effettuata (anche sulla base dei bisogni formativi rilevati).</p>",
                        "type" => AttachmentType::PORTFOLIO_DOCUMENTAZIONE_1,
                        "downloads" => true
                    ],
                    [
                        "title" => "UDA Documentazione (prova autentica)",
                        "description" => "<p>Inserire gli elementi che documentano le attività e/o le diverse fasi (schede, foto, filmati,…) con relativi commenti rispetto alla scelta.</p>",
                        "type" => AttachmentType::PORTFOLIO_DOCUMENTAZIONE_3,
                        "downloads" => true
                    ],
                    [
                        "title" => "Griglie e relazioni",
                        "description" => "I documenti presenti in questa cartella possono essere solo scaricati e la trasmissione per la successiva archiviazioni agli atti avverrà sulla base delle indicazioni fornite dal/dalla Dirigente Scolastico/a",
                        "type" => AttachmentType::PORTFOLIO_DOCUMENTAZIONE_5,
                        "downloads" => true
                    ]
                ]
            ]
        ];
    
    public function _default() {
        $this->menu->setSelectedMenuItem($this->actionUrl());
        
        if (StringUtils::isNotBlank($this->tab)){
            $this->setSessionValue("tab", $this->tab);
        }
        $this->tab = $this->getSessionValue("tab");
        if (StringUtils::isBlank($this->tab)){
            $this->tab = self::TAB_QUALIFICAZIONE;
        }

        $this->header();
        ?>
        <script type="text/javascript">
            $(function(){
                $('.download-file').change(function(){
                    var val = $(this).val();
                    if (val == ""){
                        return;
                    }
                    window.location = val; 
                });
            });
        </script>
        
        <div class="content active tab-portfolio" id="tab-portfolio">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="medium-12 columns">
                    <h3>Portfolio: <?= _t($this->user['name']) ?> <?= _t($this->user['surname']) ?></h3>
                    <a href="<?= $this->actionUrl("getAllAttachments") ?>" style="position:absolute; right: 20px; top: 20px;">
                        <i class="fa fa-download"></i> Scarica tutto
                    </a>
                </div>
            </div>
            
            <div class="my-tabs">
                <?php 
                $active = '';
                if($this->tab == self::TAB_QUALIFICAZIONE){
                    $active = 'active';
                }
                ?>
                <a href="<?= $this->actionUrl("", ["tab" => self::TAB_QUALIFICAZIONE]) ?>" class="tab-header <?= $active ?>" style="width: 49%;">
                    Inserimento professionale<br/>
                </a>
                
                <?php
                $active = '';
                if($this->tab == self::TAB_ANNO_DI_PROVA){
                    $active = 'active';
                }
                ?>
                <a href="<?= $this->actionUrl("", ["tab" => self::TAB_ANNO_DI_PROVA]) ?>" class="tab-header <?= $active ?>" style="width: 49%;">
                    Anno di formazione e di prova<br/>
                </a>
            </div>
            
            <?php
            if ($this->tab == self::TAB_QUALIFICAZIONE){
                $this->tabQualificazione();
            } else if ($this->tab == self::TAB_ANNO_DI_PROVA) {
                $this->tabAnnoDipRova();
            } 
            ?>
        </div>
        <?php
    }
    
    private function tabSviluppo(){
        $data = $this->dataSviluppo;
        
        foreach ($data as $dataSection){?> 
            <div class="margin-bottom ambito">
                <h4><?= _t($dataSection['label']) ?></h4>
                <?php 
                $index = 1;
                foreach ($dataSection['sections'] as $section){ 
                    $this->displayRow($index, $section['title'], $section['description'], $section['type'], isset($section['downloads']) ? $section['downloads'] : false);
                    $index++;
                } 
                ?>
            </div>
        <?php } 
    }
    
    private function tabAnnoDipRova(){
        $data = $this->dataAnnoDiProva;
        
        foreach ($data as $dataSection){?> 
            <div class="margin-bottom ambito">
                <h4><?= _t($dataSection['label']) ?></h4>
                <?php 
                $index = 1;
                foreach ($dataSection['sections'] as $section){ 
                    $this->displayRow($index, $section['title'], $section['description'], $section['type'], 
                        isset($section['downloads']) ? $section['downloads'] : false, 
                        isset($section["uploads"]) ? $section["uploads"] : null);
                    $index++;
                } 
                ?>
            </div>
        <?php } 
        $this->footer();
    }

    private function tabQualificazione(){
        $data = $this->dataQualificazione;
        
        foreach ($data as $dataSection){?> 
            <div class="margin-bottom ambito">
                <h4><?= _t($dataSection['label']) ?></h4>
                <?php 
                $index = 1;
                foreach ($dataSection['sections'] as $section){ 
                    $this->displayRow($index, $section['title'], $section['description'], $section['type'], isset($section['downloads']) ? $section['downloads'] : false);
                    $index++;
                } 
                ?>
            </div>
        <?php } 
        $this->footer();
    }
    
    private function displayRow($index, $title, $description, $type, $download = false, $uploads = null){
        $attachments = AttachmentService::findAllForEntity(EntityName::USER, $this->user['user_id'], $type);
        
        $colorClass = "red";
        
        if ($type == AttachmentType::PF_AP_DOCUMENTAZIONE_5){
            $colorClass = "white";
        } else if ($attachments->rowCount() > 0){
            $colorClass = "green";    
        }
        ?>
        <div id="section_<?= $type ?>" class="documento row">
            <div class="columns small-1 number">
                <h6 class="documento-number semaforo <?= $colorClass ?>"><?= $index ?></h6>
            </div>
            <div class="columns small-11">
                <h5><?= _t($title) ?></h5>
                <div>
                    <?= $description ?> 
                </div>

                <?php if($download){ ?>
                    <?= $this->buildSectionDownload($type) ?>
                <?php } ?>

                <?php
                if ($uploads == null) {
                    if ($type != AttachmentType::PF_AP_DOCUMENTAZIONE_4 &&
                        $type != AttachmentType::PORTFOLIO_DOCUMENTAZIONE_5){
                        $form = new Form($this->actionUrl());
                        $form->displaySubmit = false;
                        $attachmentField = new FormAttachmentsField("Allegati", $type, EntityName::USER, $this->user["user_id"]);
                        if (!UserRoleService::canCurrentUserDo(UserPermission::PORTFOLIO_CONTENT_WRITE)){
                            $attachmentField->readOnly = true;
                        }
                        $attachmentField->noTemp = true;
                        $attachmentField->removeUrl = $this->actionUrl("removeAttachment");
                        $attachmentField->canRemove = $this->canUserManageAttachment();
                        $form->addField($attachmentField);
                        $form->draw();
                    }
                } else {
                    foreach($uploads as $upload){
                        $form = new Form($this->actionUrl());
                        $form->displaySubmit = false;
                        
                        $entity = isset($upload["entity"]) ? $upload["entity"] : EntityName::USER;
                        $entityId = $entity == EntityName::USER ? $this->user["user_id"] : 1;

                        if ($entity == EntityName::SYSTEM){
                            $attachments = AttachmentService::findAllForEntity(EntityName::SYSTEM, 1, $upload["type"]);
                            if ($attachments->rowCount() == 0){
                                continue;
                            }
                        }

                        $attachmentField = new FormAttachmentsField($upload["title"], $upload["type"], $entity, $entityId);
                        if (!UserRoleService::canCurrentUserDo(UserPermission::PORTFOLIO_CONTENT_WRITE) || $entity == EntityName::SYSTEM){
                            $attachmentField->readOnly = true;
                        }

                        $attachmentField->noTemp = true;
                        $attachmentField->removeUrl = $this->actionUrl("removeAttachment");
                        $attachmentField->canRemove = $this->canUserManageAttachment();
                        $form->addField($attachmentField);
                        $form->draw();
                    }
                }
                ?>
            </div>
        </div>
        <?php
    }
    
    private function buildSectionDownload($section){
        ?>
        <div class="download-area">
            Scarica modello
            <?= $this->buildSectionLangDownload($section, "ita"); ?>
            <?= $this->buildSectionLangDownload($section, "de"); ?>
        </div>
        <?php
    }
    
    private function buildSectionLangDownload($section, $lang){
        $path = __DIR__ . "/../../../files/portfolio/" . $lang . "/" . $section . "/";
        $url = "/files/portfolio/" . $lang . "/" . $section . "/";
        if (!file_exists($path)){
            return;
        }
        
        $files = scandir($path);
        if (count($files) == 3){ ?>
            <a href="<?= $url . $files[2] ?>"><i class="fa fa-download"></i> <?= strtoupper($lang) ?></a>
        <?php } else { ?>
            <select class="download-file">
                <option value=""><?= strtoupper($lang) ?></option>
                <?php foreach ($files as $index => $file){ 
                    if ($index < 2){
                        continue;
                    }
                    ?>
                <option value="<?= $url . $file ?>"><?= _t($file) ?></option>
                <?php } ?>
            </select>
        <?php }
    }
    
    private function canUserManageAttachment(){
        if ($this->user["user_id"] == $this->currentUser['user_id']){
            return true;
        }
        
        if (UserRoleService::hasCurrentUser(UserRole::ADMIN)){
            return true;
        }
        
        if(StructureService::hasUserStructureRole($this->currentUser['user_id'], $this->structure['structure_id'], UserRole::DIRIGENTE)){
            return true;
        }
        
        return false;
    }

    public function removeAttachment(){
        if (StringUtils::isBlank($this->attachmentId)){
            echo "ko";
        }
        
        if (!$this->canUserManageAttachment()){
            echo "ko";
        }
        
        AttachmentService::deleteAttachmentAndAttachmentEntity($this->attachmentId);
        echo "ok";
    }
    
    public function getAllAttachments(){
        require 'vendor/autoload.php';
        
        $options = new ZipStream\Option\Archive();
        $options->setSendHttpHeaders(true);

        $filename = 'Portfolio_'. $this->user['surname'] .'_'. $this->user['surname'] .'.zip';
        $zip = new ZipStream\ZipStream($filename, $options);

        set_time_limit(300);
        $this->addAttachmentsForCategory($this->dataQualificazione, $zip);
        $this->addAttachmentsForCategory($this->dataAnnoDiProva, $zip);
        
        $zip->finish();
    }
    
    public function addAttachmentsForCategory($categories, $zip) {
        foreach($categories as $category){
            foreach ($category['sections'] as $section){
                $attachments = AttachmentService::findAllForEntity(EntityName::USER, $this->user['user_id'], $section['type']);
                foreach ($attachments as $attachment){
                    if (!$this->canUserManageAttachment()){
                        continue;
                    }

                    $path = AttachmentService::buildAttachmentPath($attachment);
                    $fileContent = EncryptService::decryptfileInMemory($path);
                    
                    $zip->addFile($section['title'] . '/' .  $attachment['filename'], $fileContent);
                }
            }
        }
    }
}
