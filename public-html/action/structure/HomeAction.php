<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class HomeAction extends StructureAction{
    
    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());
        
        $this->header();
        ?>
        <div class="content active" id="panelTabStudenti">
            <div class="row-100">
                <div class="columns large-8 medium-9 small-12">
                    <?php $this->displaySelectDocumentType(); ?>
                </div>
                <div class="columns large-4 medium-3 small-12">
                    <?php $this->displayInternalMessages() ?>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }

    private function displayInternalMessages(){
        $messages = InternalMessageService::findVisibleMessages($this->currentUser, 10);
        ?>
        <h2>Messaggi</h2>
        <div class="padding">
            <?php if ($messages == null || $messages->rowCount() == 0){ ?>
                <p>Nessun messaggio attualmente presente.</p>
            <?php } else { 
                foreach($messages as $message){ ?>
                <div>
                    <h5><?= _t($message["subject"]) ?></h5>
                    <div>
                        <?= $message["message"] ?>
                    </div>
                </div>
            <?php }
            } ?>
        </div>
        <?php
    }
    
    private function displaySelectDocumentType(){
        global $currentUserAllowedAction;
        
        ?>
            <h2>Documenti</h2>
            <div id="home-documents" class="padding">
                <p>Selezionare la tipologia di documento su cui si desidera lavorare:</p>
                
                <div class="pianodidattico">            
                    <div class="row">
                        <?php if (UserRoleService::canCurrentUserDo("/structure/PEIAction")){ ?>
                            <div class="box large-offset-1 large-4 medium-offset-1 medium-4 small-offset-1 small-10 columns">
                                <a class="button radius" href="<?= UriService::buildPageUrl("/structure/PEIAction") ?>">PEI</a>
                                <p>Stesura del Piano Educativo Individualizzato</p>
                            </div>
                        <?php } ?>

                        <?php if (UserRoleService::canCurrentUserDo("/structure/PEITirocinioAction") && $this->structure["is_scuola_professionale"] == 1){ ?>
                            <div class="box large-offset-1 large-4 medium-offset-1 medium-4 small-offset-1 small-10 columns">
                                <a class="button radius" href="<?= UriService::buildPageUrl("/structure/PEITirocinioAction") ?>">PEI di tirocinio</a>
                                <p>Azioni di orientamento e formazione al lavoro</p>
                            </div>
                        <?php } ?>

                        <?php if (UserRoleService::canCurrentUserDo("/structure/PDPAction")){ ?>
                            <div class="box large-offset-1 large-4 medium-offset-1 medium-4 small-offset-1 small-10 end columns">
                                <a class="button radius" href="<?= UriService::buildPageUrl("/structure/PDPAction") ?>">PDP</a>
                                <p>Stesura del Piano Didattico Personalizzato</p>
                            </div>
                        <?php } ?>

                        <?php if (UserRoleService::canCurrentUserDo("/structure/PDPLinguistocoAction")){ ?>
                            <div class="box large-offset-1 large-4 medium-offset-1 medium-4 small-offset-1 small-10 end columns">
                                <a class="button radius" href="<?= UriService::buildPageUrl("/structure/PDPLinguistocoAction") ?>">PDP per bisogni linguistici</a>
                                <p>Piano Didattico Personalizzato per bisogni linguistici specifici</p>
                            </div>
                        <?php } ?>
                    </div>
                    
                    <?php if (RilevazioneCattedreService::isStructureEnabled($this->structure) && UserRoleService::canCurrentUserDo("/structure/rilevazione_cattedre/RilevazioneCattedreTableAction")){ ?>
                        <div class="row">
                            <div class="box large-offset-1 large-9 small-offset-1 small-9 columns">
                                <a class="button radius" href="<?= UriService::buildPageUrl("/structure/rilevazione_cattedre/RilevazioneCattedreTableAction") ?>">A023ter</a>
                                <p>Rilevazione dati finalizzata alla distribuzione delle cattedre A023ter</p>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if (EModelService::isStructureEnabled($this->structure) && UserRoleService::canCurrentUserDo("/structure/eModel/EmodelAction")){ ?>
                        <div class="row">
                            <div class="box large-offset-1 large-9 small-offset-1 small-9 columns">
                                <a class="button radius" href="<?= UriService::buildPageUrl("/structure/eModel/EmodelAction") ?>">Modello E</a>
                                <p>Compila i dati per il modello E.</p>
                            </div>
                        </div>
                    <?php } ?>

                    <?php
                    if (DropOutService::isStructureEnabled($this->structure) && UserRoleService::canCurrentUserDo("/structure/dropOut/DropOutUsersAction")){ ?>
                        <div class="row">
                            <div class="box large-offset-1 large-9 small-offset-1 small-9 columns">
                                <a class="button radius" href="<?= UriService::buildPageUrl("/structure/dropOut/DropOutUsersAction") ?>">Drop out</a>
                                <p>Segnalazioni di frequenze irregolari.</p>
                            </div>
                        </div>
                    <?php } ?>

                    <?php 
                    $this->drawLink(PropertyNamespace::EDUCAZIONE_SALUTE, PropertyKey::ENABLED,  
                        UriService::buildPageUrl("/structure/edSalute/EdSaluteAction"),
                        "Educazione alla salute", "Progetti di educazione alla salute.");

                    if (UserRoleService::hasCurrentUser(UserRole::INSEGNANTE_ANNO_DI_PROVA)){
                        $this->drawLink(PropertyNamespace::PORTFOLIO_INSEGNANTI, PropertyKey::ENABLED,
                            UriService::buildPageUrl("/structure/portfolio/PortfolioAction"),
                            "Portfolio", "Compila il portfolio");
                    }    
                    ?>
                    
                    <?php
                    if (SomministrazioneService::isStructureEnabled($this->structure) && UserRoleService::canCurrentUserDo("/structure/test/SomministrazioniAction")){ ?>
                        <div class="row">
                            <div class="box large-offset-1 large-9 small-offset-1 small-9 columns">
                                <a class="button radius" href="<?= UriService::buildPageUrl("/structure/test/SomministrazioniAction") ?>">Mondo delle Parole e Progetto Letto Scrittura</a>
                                <p>Somministrazioni di test per l'individuazione precode ci difficoltà di letto-scrittura.</p>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if(UserRoleService::canCurrentUserDo("/structure/servizio_valutazione/HomeAction")){ ?>
                        <div class="row">
                            <div class="box large-offset-1 large-9 small-offset-1 small-9 columns">
                                <a class="button radius" href="<?= UriService::buildPageUrl("/structure/servizio_valutazione/HomeAction") ?>">Rilevazioni – Autovalutazione – Progettualità</a>
                                <p>Rilevazioni prove di Tedesco L2 – Processo dell’autovalutazione e della pianificazione.</p>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if(UserRoleService::canCurrentUserDo("/structure/patto_inclusione/PattoInclusioneAction")){ ?>
                        <div class="row">
                            <div class="box large-offset-1 large-9 small-offset-1 small-9 columns">
                                <a class="button radius" href="<?= UriService::buildPageUrl("/structure/patto_inclusione/PattoInclusioneAction") ?>">Piano Inclusione</a>
                                <p>Modelli e documenti caricati.</p>
                            </div>
                        </div>
                    <?php } ?>
                </div>  
            </div>
        <?php
    }
    
    private function drawLink($namespace, $property, $link, $label, $description){
        $portfolioEnabled = PropertyService::find($namespace, $property);
        if (!$portfolioEnabled['value'] || !UserRoleService::canCurrentUserDo($link)){ 
            return;
        }
        ?>
            <div class="row">
                <div class="box large-offset-1 large-9 small-offset-1 small-9 columns">
                    <a class="button radius" href="<?= $link ?>"><?= _t($label) ?></a>
                    <p><?= _t($description) ?></p>
                </div>
            </div>
        <?php
    }
    
}