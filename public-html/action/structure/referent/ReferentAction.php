<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ReferentAction extends StructureAction{
    
    public function _default(){
        $subStructures = StructureService::findAllWithParent($this->structureId);
        $references = DictionaryService::findGroupAsDictionary(Dictionary::STRUCTURE_REFERENT);
        $dbUsers = StructureService::findUserStructureWithoutRoles($this->structureId, [UserRole::STUDENTE]);

        $users = [];
        foreach ($dbUsers as $user) {
            if ($this->findObjectById($user["user_id"], $users) != null){
                continue;
            }

            $users[] = [
                "user_id" => $user["user_id"],
                "name" => $user["name"],
                "surname" => $user["surname"]
            ];
        }

        $this->menu->setSelectedMenuItem($this->actionUrl());
        $this->header();

        $params = [
            "references" => $references,
            "structures" => $subStructures->fetchAll(),
            "users" => $users
        ];
        ?>
        <div class="MbStructureUserRef-container"></div>
        <script>
            MbCreateVue('MbStructureUserRef', 'MbStructureUserRef-container', <?= json_encode($params) ?>);
        </script>
        <?php
        $this->footer();
    }

    private function findObjectById($id, $users){
        foreach ( $users as $element ) {
            if ( $id == $element["user_id"] ) {
                return $element;
            }
        }

        return false;
    }

    public function getData(){
        $response = [];
        $references = DictionaryService::findGroup(Dictionary::STRUCTURE_REFERENT, true);

        $users = $this->loadUsers();

        $out = [];
        foreach ($references as $ref) {
            $out[$ref["key"]] = [
                "referenceValue" => $ref["key"],
                "referenceName" => $ref["value"]
            ];
        }

        foreach ($users as $user) {
            if (!isset($out[$user["value"]]["users"])){
                $out[$user["value"]]["users"] = [];
            }

            if (!isset($out[$user["value"]]["users"][$user["user_id"]])){
                $out[$user["value"]]["users"][$user["user_id"]] = [
                    "user_id" => $user["user_id"],
                    "name" => $user["name"],
                    "surname" => $user["surname"],
                    "user_structure_id" => $user["user_structure_id"],
                    "structures" => []
                ];
            }

            $out[$user["value"]]["users"][$user["user_id"]]["structures"][] = $user["structure_id"];
        }

        echo json_encode($out);
    }

    private function loadUsers(){
        return EM::execQuery("SELECT u.user_id, usm.user_structure_id, usm.value, us.structure_id, u.user_id, u.name, u.surname
            from user_structure_metadata usm
            left join dictionary d ON d.key=usm.value
            left join user_structure us ON usm.user_structure_id=us.user_structure_id
            left join structure s ON us.structure_id=s.structure_id 
            left join user u ON us.user_id=u.user_id
            where 
                s.enabled = 1
                AND usm.metadata_id='referent_for'
                AND us.parent_structure_id=:structure_id
            order by d.value", [
                    "structure_id" => $this->structureId
                ]);
    }

    public function postUpdate(){
        $json = file_get_contents('php://input');
        $request = json_decode($json);

        $subStructures = StructureService::findAllWithParent($this->structureId);
        foreach($subStructures as $structure){
            $userStructure = StructureService::findUserStructure($request->user_id, $structure["structure_id"]);
            $userStructureId = null;
            if ($userStructure == null){
                continue;
            } else {
                $userStructureId = $userStructure["user_structure_id"];
            }

            MetadataService::removeMetadataWithMetadataIdAndValue(MetadataEntity::USER_STRUCTURE, $userStructureId, UserStructureMetadata::REFERENT_FOR, $request->ref_value);

            $metadatas = MetadataService::findMetadataValue(MetadataEntity::USER_STRUCTURE, $userStructureId, UserStructureMetadata::REFERENT_FOR);
            $structureRoles = StructureService::findUserStructureRole($request->user_id, $structure["structure_id"]);
            $structureRole = $structureRoles->fetch();
            if ($structureRole["role_id"] == null && count($metadatas) == 0){
                StructureService::removeUserStructure($request->user_id, $structure["structure_id"]);
            }
        }

        foreach ($request->structures as $structureId) {
            $userStructure = StructureService::findUserStructure($request->user_id, $structureId);
            $userStructureId = null;
            if ($userStructure == null){
                $userStructureId = StructureService::addUserStructure($request->user_id, $structureId, null, false);
            } else {
                $userStructureId = $userStructure["user_structure_id"];
            }

            MetadataService::persistMetadataValue(MetadataEntity::USER_STRUCTURE, $userStructureId, UserStructureMetadata::REFERENT_FOR, $request->ref_value, false);
        }
        
    }
}