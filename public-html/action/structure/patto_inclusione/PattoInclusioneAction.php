<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PattoInclusioneAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PattoInclusioneAction extends StructureAction {

    public $attachmentId;

    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());
        $schoolYears = SchoolYearService::findAll();
        
        $this->header();
        ?>
        <div class="content active" id="panelTabStudenti">
            <div class="row">
                <div class="columns large-12">
                    <h2>Piano per l'inclusione</h2>

                    <div style="margin: 20px 0px 20px 0px">
                        Il documento relativo al Piano inclusione , in formato Word, non deve essere modificato nella struttura. Si chiede di compilare le diverse voci presenti e caricare il documento in piattaforma in formato pdf.
                    </div>

                    <?php
                    $form = new Form($this->actionUrl());
                    $form->displaySubmit = false;

                    $type = AttachmentType::PATTO_INCLUSIONE . '_11';
                    $this->attachmentField($form, "Modello", EntityName::SYSTEM, 1, $type, true);

                    $nextCurrent = false;
                    foreach($schoolYears as $sy){
                        $type = AttachmentType::PATTO_INCLUSIONE . '_' . $sy["school_year_id"];

                        if ($sy["current"] || $nextCurrent){        
                            $this->attachmentField($form, "File compilato " . $sy["school_year"], EntityName::STRUCTURE, $this->structureId, "structure_" . $type, false);
                            $nextCurrent = true;
                        } else {
                            $attachments = AttachmentService::findAllForEntity(EntityName::SYSTEM, 1, $type);
                            if ($attachments->rowCount() > 0){
                                $this->attachmentField($form, "File compilato " . $sy["school_year"], EntityName::STRUCTURE, $this->structureId, "structure_" . $type, true);
                            }
                        }
                    }

                    $form->draw();
                    ?>
                </div>
            </div>
        </div>

        <?php
        $this->footer();
    }

    
    private function attachmentField($form, $label, $entity, $entityId, $name, $readOnly){
        $attachmentField = new FormAttachmentsField($label, $name, $entity, $entityId);
        $attachmentField->manAttachments = 1;
        $attachmentField->removeUrl = $this->actionUrl("removeAttachment");
        $attachmentField->noTemp = true;
        $attachmentField->canRemove = !$readOnly;
        $attachmentField->readOnly = $readOnly;
        $attachmentField->extensions = ["pdf"];
        $form->addField($attachmentField);
    }

    public function removeAttachment(){
        $attachmentEntities = AttachmentService::findAttachmentEntityForAttachmentId($this->attachmentId);
        $attachmentEntity = $attachmentEntities->fetch();
        if ($attachmentEntity == null){
            return;
        }

        if ($attachmentEntity["entity"] != EntityName::STRUCTURE 
            || $attachmentEntity["entity_id"] != $this->structureId){
            return;
        }

        AttachmentService::deleteAttachmentAndAttachmentEntity($this->attachmentId);
    }
}

