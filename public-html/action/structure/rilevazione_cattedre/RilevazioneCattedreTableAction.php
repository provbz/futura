<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of RilevazioneCattedreTableAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class RilevazioneCattedreTableAction extends StructureAction {

    public $id;

    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());
        
        $this->header();
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row-100">
                    <div class="large-8 medium-7 small-7 columns">
                        <h3>Rilevazione dati finalizzata alla distribuzione delle cattedre A023ter</h3>
                    </div>
                    <div class="large-4 medium-3 small-5 columns buttontoolbar">
                        <a href="<?= UriService::buildPageUrl("/structure/rilevazione_cattedre/RilevazioneCattedreAction-edit") ?>" class="button radius nuovo float-right">
                            <i class="<?= Icon::PLUS ?> margin-right"></i> Nuovo
                        </a>
                    </div>
                </div>
                
                <div class="mb-structure-rilevazione-cattedre-table-container"></div>
                <script>
                    MbCreateVue('MbStructureRilevazioneCattedreTable', 'mb-structure-rilevazione-cattedre-table-container', <?= json_encode([]) ?>);
                </script>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function remove(){
        if ($this->id == null){
            return;
        }

        $entity = RilevazioneCattedreService::find($this->id);
        if ($entity == null){
            return;
        }

        if ($entity["structure_id"] != $this->structureId){
            return;
        }

        RilevazioneCattedreService::remove($this->id);
    }
}