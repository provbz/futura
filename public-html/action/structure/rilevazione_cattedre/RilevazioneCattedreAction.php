<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of RilevazioneCattedreAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class RilevazioneCattedreAction extends StructureAction {

    public $id;

    public function edit(){
        $currentYear = SchoolYearService::findCurrentYear();
        $entity = RilevazioneCattedreService::find($this->id);
        if ($this->id != null && $entity == null){
            redirect(UriService::accessDanyActionUrl());
        }

        if ($entity != null && $entity["structure_id"] != $this->structureId){
            redirect(UriService::accessDanyActionUrl());
        }

        $alredyInserted = false;
        if ($this->id == null){
            $alredyInserted = EM::execQuerySingleResult("SELECT rilevazione_cattedre_id FROM rilevazione_cattedre WHERE structure_id = :structure_id AND school_year_id = :school_year_id", [
                "structure_id" => $this->structureId,
                "school_year_id" => $currentYear["school_year_id"]
            ]) != null;
        }

        $form = $this->buildForm($entity);
        if ($form->isSubmit() && $form->checkFields()){
            if ($this->id != null){
                $tc = new TableCondition("rilevazione_cattedre_id = :rilevazione_cattedre_id", "rilevazione_cattedre_id", $this->id);
                $form->persist($tc);
            } else {
                $this->id = $form->persist(null, [
                    "structure_id" => $this->structureId,
                    "insert_user_id" => $this->currentUser["user_id"],
                    "insert_date" => "NOW()",
                    "school_year_id" => $currentYear["school_year_id"]
                ]);
            }

            EM::updateEntity("rilevazione_cattedre", [
                "last_edit_user_id" => $this->currentUser["user_id"],
                "last_edit_date" => "NOW()"
            ], [
                "rilevazione_cattedre_id" => $this->id
            ]);
            
            redirectWithMessage(UriService::buildPageUrl("/structure/rilevazione_cattedre/RilevazioneCattedreTableAction"), "Dati salvati");
        }

        $this->header();
        $this->formScripts();
        ?>
        <div class="section-title row">
            <div class="large-10 medium-9 small-7 columns">
                <h3>Rilevazione dati finalizzata alla distribuzione delle cattedre A023ter</h3>
            </div>
        </div>
        <div class="table-row row">
            <div class="large-12 columns">
                <div class="tabs-content">
                    <?php if ($alredyInserted){ ?>
                        <div class="alert-box warning">
                            Attenzione: i dati per l'anno scolastico corrente sono già stati inseriti. 
                            Se vuoi modificarli, seleziona il documento dell'anno scolastico corrente.
                        </div>
                        <div class="row-100 campiObbligatori">
                            <div class="large-5 medium-5 end columns">
                                <a href="/structure/rilevazione_cattedre/RilevazioneCattedreTableAction" class="button radius alert"><i class="fa fa-undo margin-right"></i>Annulla</a>
                            </div>
                        </div>
                    <?php } else { ?>
                        <?php $form->draw(); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function formScripts(){
        ?>
        <script>
            $(document).ready(function(){
                $("input[name='uso_risorse_proprie']").change(function(){
                    if ($(this).is(":checked")){
                        $("#campo_form_dettaglio_risorse").show();
                    } else {
                        $("#campo_form_dettaglio_risorse").hide();
                        $('#dettaglio_risorse').val("");
                    }
                });

                $("input[name='uso_risorse_proprie']").change();
            });
        </script>
        <?php
    }

    private function buildForm($entity){
        $scollYears = SchoolYearService::findAllAsObject();
        $currentYear = SchoolYearService::findCurrentYear();

        if ($entity == null){
            $entity = [];
        }

        if (!isset($entity["school_year_id"])){
            $entity["school_year_id"] = $currentYear["school_year_id"];
        }

        $form = new Form($this->actionUrl("edit", ["id" => $this->id]));
        $form->cancellAction = UriService::buildPageUrl("/structure/rilevazione_cattedre/RilevazioneCattedreTableAction");
        
        $form->entityName = "rilevazione_cattedre";
        $form->entity = $entity;
        
        $yearField = new FormSelect2Field("Anno scolastico", "school_year_id", FormFieldType::NUMBER, $scollYears, true, $currentYear["school_year_id"]);
        $yearField->readOnly = true;
        $form->addField($yearField);

        $numero_alunni_no_italiano = new FormTextField("<strong>Numero totale</strong> di alunne/i neoarrivate/i in Italia non italofone/i e 
            <strong>non in grado di utilizzare l'Italiano come lingua di comunicazione e di studio</strong>, inserite/i a scuola 
            <strong>negli ultimi tre anni scolastici</strong> (incluso l’anno scolastico corrente)", "numero_alunni_no_italiano", FormFieldType::NUMBER, true, "", [
            "numeric" => true
        ]);
        $form->addField($numero_alunni_no_italiano);

        $numero_alunni_sostegno_linguistico = new FormTextField("<strong>Numero</strong> di alunne/i neoarrivate/i 
            <strong>nell’anno scolastico corrente con necessità di sostegno linguistico</strong> (incluse le iscrizioni pervenute prima dell’inizio dell’anno scolastico in corso, ovvero da giugno a fine agosto)", 
            "numero_alunni_sostegno_linguistico", FormFieldType::NUMBER, true, "", [
            "numeric" => true
        ]);
        $form->addField($numero_alunni_sostegno_linguistico);

        $uso_risorse_proprie = new FormCheckField("La scuola ha dovuto attingere nel corso di quest'anno scolastico a risorse 
            proprie extra per soddisfare il proprio bisogno di sostegno 
            linguistico (docenti con ore straordinarie, incarichi esterni ecc.)?", 
            "uso_risorse_proprie", FormFieldType::INTEGER, [1 => "Sì"]);
        $form->addField($uso_risorse_proprie);

        $specificare = new FormTextareaField("<strong>Specificare:</strong> tipo di intervento, ore settimanali, durata", "dettaglio_risorse", FormFieldType::STRING, false);
        $specificare->checkFunction = function($field, $value) use ($uso_risorse_proprie){
            if ($uso_risorse_proprie->getValueForDb() == 1){
                if (empty($value)){
                    $field->addFieldError("Dato richeisto.");
                }
            }
        };
        $form->addField($specificare);

        if ($entity != null && $entity["school_year_id"] !== $currentYear["school_year_id"]){
            foreach($form->fields as $field){
                $field->readOnly = true;
            }

            $form->displaySubmit = false;
        }

        return $form;
    }

}