<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AnalisiAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
require_once './action/structure/pei/PeiAction.php';

class AnalisiAction extends PeiAction {
    
    public function details(){
        $this->bradcrumbs->add(new Bradcrumb(
                ReflectionUtils::getValue($this->userMetadata, UserMetadata::NAME) . ' ' .
                ReflectionUtils::getValue($this->userMetadata, UserMetadata::SURNAME),
                UriService::buildPageUrl("/structure/UserAction")));
        $this->bradcrumbs->add(new Bradcrumb("Elabora"));
        
        $this->header();
        ?>
        <script type="text/javascript">
        var selectedRowTargetId = null;

        $(function(){
            $(document).on('open.fndtn.reveal', '[data-reveal]', function () {
                var id = $(this).attr('id');
                switch(id){
                    case 'dialog-find_nodes':
                        $('#find_nodes').attr('src', '<?= UriService::buildPageUrl('/structure/pei/TreeAction', '', 
                            [
                                'displayHeader' => false, 
                                'displayActivities' => false,
                                'displayAddButton' => true,
                                "documentType" => $this-documentType
                            ]
                        ) ?>');
                        break;    
                    case 'dialog-new_activity':
                        loadDiv('#dialog-new_activity', 
                            '<?= UriService::buildPageUrl('/structure/pei/PeiRowAction', 'addActivity') ?>', 
                            {'peiRowTargetId': selectedRowTargetId});
                        break;
                }
            });
        });
        </script>
        <div id="dialog-find_nodes" class="dialog reveal-modal" data-reveal>
            <iframe id="find_nodes" width="100%" height="500px" style="border: none;"></iframe>
            <a class="close-reveal-modal">×</a>
        </div>
        
        <div class="content active tab-nuovo-studente" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-9 medium-7 columns">
                    <h3>
                        Analisi della situazione attuale nelle singole aree e
                        individuazione delle relative possibilità di sviluppo
                    </h3>
                    <h5>Obiettivi programmati ed attività proposte in sezione/intersezione</h5>
                </div>
                <div class="large-offset-1 large-2 medium-5 columns">
                    <a class="button radius nuovo" href="javascript:openDialog('#dialog-find_nodes');">
                        <i class="fa fa-search"></i>Ricerca
                    </a>
                </div>    
            </div>
            
            <div class="row" id="pei_container">
                <?php $this->loadPei(); ?>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    
}
