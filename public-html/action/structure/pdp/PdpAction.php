<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PdpAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PdpAction extends StructureUserDocumentAction{
    
    public $pdpId;
    
    protected $pdp;
    
    public function _prepare() {
        parent::_prepare();
        
        if (StringUtils::isNotBlank( $this->pdpId )){
            $this->updatePdp($this->pdpId);
        }
        $this->pdp = $this->getGlobalSessionValue("PdpAction_pdp");
    }
    
    protected function updatePdp($pdpId){
        $this->pdp = PdpService::find($pdpId);
        $this->setGlobalSessionValue("PdpAction_pdp", $this->pdp);
    } 
    
    protected function buildBackBradcrumb(){
        return new Bradcrumb(ReflectionUtils::getValue($this->userMetadata, UserMetadata::NAME) . ' ' .ReflectionUtils::getValue($this->userMetadata, UserMetadata::SURNAME) ,
                UriService::buildPageUrl("/structure/UserAction"));
    }
}
