<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of StructureUserDocumentAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class StructureUserDocumentAction extends StructureAction{
    
    public $userYearId;
    public $documentId;

    public $user;
    public $document;
    public $userMetadata;
    public $userYear;
    public $currentYear;
    protected $checkUserToUser = true;
    protected $allowNotCurrentYearAccess = false;
    
    public function _prepare() {
        parent::_prepare();
        $this->currentYear = SchoolYearService::findCurrentYear();

        if (StringUtils::isNotBlank($this->documentId)){
            $this->loadData();
            
            if (!$this->allowNotCurrentYearAccess && $this->document['school_year_id'] != $this->currentYear['school_year_id']){
                redirect( UriService::accessDanyActionUrl() );
            }

            if ($this->userYear == NULL){
                redirect( UriService::accessDanyActionUrl() );
            }
            
            if ($this->user == NULL){
                redirect( UriService::accessDanyActionUrl() );
            }
            
            $isStudent = StructureService::hasUserStructureRole($this->user['user_id'], $this->structureId, UserRole::STUDENTE);
            if (!$isStudent){
                redirect( UriService::accessDanyActionUrl() );
            }

            if ($this->checkUserToUser){
                $userToUser = UserUserService::find($this->user['user_id'], $this->currentUser['user_id']);
                if (!UserRoleService::canCurrentUserDo(UserPermission::STRUCTURE_STUDENTS_READ_ALL) && $userToUser == NULL){
                    redirect( UriService::accessDanyActionUrl() );
                }
            }
        }
        
        $this->getSessionData();
    }

    public function getSessionData(){
        $this->document = getSessionValue("StructureUserAction_document", NULL);
        $this->user = getSessionValue("StructureUserAction_user", NULL);
        $this->userMetadata = getSessionValue("StructureUserAction_userMetadata", NULL);
        $this->userYear = getSessionValue("StructureUserAction_userYear", NULL);
        
        if ($this->user == NULL){
            redirect(UriService::accessDanyActionUrl());
        }
    }

    public function loadData($isAdmin = false){
        $this->document = DocumentService::find($this->documentId);
        if ($this->document == null || 
            $this->document["status"] == UserYearDocumentStatus::DELETED){
            redirect(UriService::accessDanyActionUrl());
        }
        
        $this->documentType = $this->document["type"];
        $this->userYear = UserYearService::findUserYearByUser($this->document["user_id"]);
        $this->userYearId = $this->userYear["user_year_id"];
        if ($this->userYear == NULL){
            return;
        }
        
        $this->user = UserService::find($this->document['user_id']);
        if ($this->user == NULL){
            return;
        }

        if (!$isAdmin && !StructureService::hasUserStructure($this->user["user_id"], $this->structure["structure_id"])){
            redirect(UriService::accessDanyActionUrl());
        }
        
        $this->updateMetadata();
        $this->setGlobalSessionValue("StructureUserAction_document", $this->document);
        $this->setGlobalSessionValue("StructureUserAction_userYear", $this->userYear);
        $this->setGlobalSessionValue("StructureUserAction_user", $this->user);
    }
 
    protected function updateUserYear(){
        $this->userYear = UserYearService::find($this->userYear['user_year_id']);
        $this->setGlobalSessionValue("StructureUserAction_userYear", $this->userYear);
    }
    
    protected function updateMetadata(){
        if ($this->user == null){
            return;
        }
        
        $this->userMetadata = MetadataService::findDictionary("user", $this->user['user_id']);
        $this->setGlobalSessionValue("StructureUserAction_userMetadata", $this->userMetadata);
    }

    protected function isReadOnly(){
        return $this->document["replaced_by_document_id"] != null;
    }

    public function header($displayPageHeader = true, $angularApp = NULL){
        require_once './template/Header.php';
     
        $replacedBy = EM::execQuerySingleResult("SELECT * 
            FROM user_year_document 
            WHERE user_year_document_id = :document_id 
            AND status = :status", [
            "document_id" => $this->document["replaced_by_document_id"],
            "status" => UserYearDocumentStatus::READY
        ]);
        if ($replacedBy != null){ ?>
            <div class="alert alert-danger my-5">
                <strong>Attenzione:</strong>
                Documento dismesso, questo documento è stato sostituito da: <br/>
                <ul>
                    <li>
                    <a href="<?= UriService::buildPageUrl("/structure/UserAction", "_default", ["documentId" => $replacedBy["user_year_document_id"]]) ?>">
                        <?= DocumentService::getDocumentName($replacedBy["type"]) ?> - <?= $replacedBy["user_year_document_id"] ?>
                    </a>
                    </li>
                </ul>
            </div>
        <?php }
    }
}
