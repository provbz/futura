<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once __DIR__ . '/StructureUserDocumentAction.php';

/**
 * Description of StructureAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class StructureAction extends Page {
    
    public $requestDocumentType;
    public $documentType;
    public $structureId;
    public $structure;
    
    public function _prepare() {
        parent::_prepare();
        
        if (StringUtils::isBlank($this->structureId)){
            $this->structureId = getSessionValue(GlobalSessionData::SELECTED_STRUCTURE_ID);
        }

        if (UserRoleService::canCurrentUserDo(UserPermission::STRUCTURE_ACCESS_ALL)){
            if (StringUtils::isNotBlank($this->structureId)){
                $this->structure = StructureService::find($this->structureId);
            }
        }
        
        if (StringUtils::isBlank( $this->structureId )){
            redirect(UriService::accessDanyActionUrl());
        }
        
        if (StringUtils::isNotBlank($this->requestDocumentType)){
            $this->setGlobalSessionValue(GlobalSessionData::DOCUMENT_TYPE, $this->requestDocumentType);
            redirect("/structure/HomeAction");
        }
        
        $this->documentType = $this->getGlobalSessionValue(GlobalSessionData::DOCUMENT_TYPE);
    }

    protected function prepareMenu(){
        $this->structureMenu();
    }
}
