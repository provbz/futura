<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PEIPDPBaseAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . "/UserYearDocumentAction.php";

class PEIPDPBaseAction extends UserYearDocumentAction {
    
    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());

        $this->header();
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row-100">
                    <div class="large-8 medium-7 small-7 columns">
                        <h3>Elenco <?= _t(DocumentService::getDocumentName($this->documentType)) ?></h3>
                    </div>
                    <div class="large-4 medium-3 small-5 columns buttontoolbar">
                        <?php if (UserRoleService::canCurrentUserDo("/structure/pei/AttachmentSummaryActionn")){ ?>
                            <a href="<?= UriService::buildPageUrl("/structure/pei/AttachmentSummaryAction", "", ["documentType" => $this->documentType]) ?>" class="button radius nuovo float-right" style="background-color: green !important;">
                                <i class="fa fa-folder margin-right" aria-hidden="true"></i>
                                Stato file
                            </a>
                        <?php } ?>

                        <?php if (UserRoleService::canCurrentUserDo($this->actionUrl("importFromPrevYear"))){ ?>
                            <a href="<?= $this->actionUrl("importFromPrevYear", ["documentType" => $this->documentType]) ?>" class="button radius nuovo float-right">
                                <i class="<?= Icon::DATABASE ?> margin-right"></i> Recupera
                            </a>
                        <?php } ?>

                        <a href="<?= $this->actionUrl("newUser", ["documentType" => $this->documentType]) ?>" class="button radius nuovo float-right">
                            <i class="<?= Icon::PLUS ?> margin-right"></i> Nuovo
                        </a>
                    </div>
                </div>
                
                <div class="mb-structure-pei-pdp-table"></div>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    public function newUserFull(){
        $currentYear = SchoolYearService::findCurrentYear();

        $lastUserYear = UserYearService::findLastInsertedUserYear($this->structureId);

        $document = DocumentService::find($this->documentId);
        $user = null;
        $userYear = null;

        if ($document != null){
            $user = UserService::find($document["user_id"]);
            $userYear = UserYearService::findUserYearByUser($document["user_id"]);
        } else if (StringUtils::isNotBlank($this->u_code)){
            $user = UserService::findByCode($this->u_code);
            if (isset($user["user_id"])){
                $userYear = UserYearService::findUserYearByUser($user["user_id"]);
            }
        }

        $metadata = [];
        if ($user != NULL){
            $userStructure = StructureService::hasUserStructureRole($user['user_id'], $this->structureId, UserRole::STUDENTE);
            if ($userStructure == null){
                redirect(UriService::accessDanyActionUrl());
            }
            
            $userToUser = UserUserService::find($user['user_id'], $this->currentUser['user_id']);
            if (!UserRoleService::canCurrentUserDo(UserPermission::STRUCTURE_STUDENTS_READ_ALL) && $userToUser == NULL){
                redirect(UriService::accessDanyActionUrl());   
            }

            $metadata = MetadataService::findDictionary("user", $user["user_id"]);
        }

        $entity = [];
        ReflectionUtils::fillWithPrefix($user, "u_", $entity);
        ReflectionUtils::fillWithPrefix($userYear, "uy_", $entity);
        ReflectionUtils::fillWithPrefix($metadata, "um_", $entity);
        
        $form = new Form($this->actionUrl("newUserFull", [
            'documentId' => $this->documentId, 
            "u_code" => $this->u_code
        ]));
        $form->entityName = "user";
        $form->entity = $entity;
        
        if ($document != null){
            $form->cancellAction = UriService::buildPageUrl("/structure/UserAction", '', ['userId' => $user['user_id']]);
        } else {
            $form->cancellAction = $this->actionUrl('_default');
        }
        
        $form->addField(new FormHtmlField('<div class="sezione">
                                                <div class="info">'));
        
        $popcornField = $this->createPopCornField($user);
        $form->addField($popcornField);
        $form->addField(new FormDateMultipleField("Data di nascita", "um_birth_date", FormFieldType::DATE_TIME, true));
        $form->addField(new FormTextField("Luogo di nascita", "um_birth_place", FormFieldType::STRING, false, null, ["maxLength" => 100]));
        
        $genders = ['' => ''] + DictionaryService::findGroupAsDictionary(Dictionary::GENDER);
        $form->addField(new FormSelectField("Sesso", "um_gender_id", FormFieldType::STRING, $genders, false));
        
        if ($this->documentType == DocumentType::PDP_LINGUA){
            $form->addField(new FormTextField("Lingue parlate in famiglia", "um_language_fam", FormFieldType::STRING));
            $form->addField(new FormTextField("Lingue parlate tra i pari", "um_language_pari", FormFieldType::STRING));
            $form->addField(new FormTextField("Altre lingue conosciute", "um_language_altre", FormFieldType::STRING));
            $form->addField(new FormDateMultipleField("Data di arrivo in Italia", "um_date_italy", FormFieldType::DATE_TIME));

            $permanenza = ['' => ''] + DictionaryService::findGroupAsDictionary(Dictionary::TIPO_PERMANENZA);
            $form->addField(new FormSelectField("Permanenza", "um_tipo_permanenza", FormFieldType::STRING, $permanenza, false,));

            $tipologiaBes = new FormMultiSelectField('Tipologia di BES', 'um_tipologia_bes');
            $tipologiaBes->options = DictionaryService::findGroup(Dictionary::TIPOLOGIA_BES);
            $altroDictionary = $tipologiaBes->options[count($tipologiaBes->options) - 1];
            if ($altroDictionary != null){
                $tipologiaBes->optionsWithTextArea = [
                    $altroDictionary["key"]
                ];
            }
            $form->addField($tipologiaBes);

            $form->addField(new FormTextAreaField("Livello di scolarizzazione paese di origine", "um_livello_scol_paese_orig", FormFieldType::STRING, null, ["maxLength" => 2000]));
            $percorsoField = new FormTextAreaField("Precedente percorso scolastico", "um_percorso_scolastico", FormFieldType::STRING, null, ["maxLength" => 2000]);
            $percorsoField->note = "(tipologia, frequenza, sistema di lingua alfabeto/scrittura nella scolarizzazione...)";
            $form->addField($percorsoField);
            $form->addField(new FormTextAreaField("Segnalazioni da parte della famiglia/esercenti la responsabilità genitoriale e dell’alunno", "um_segnalazioni_famiglia", FormFieldType::STRING, null, ["maxLength" => 2000]));
        } else {
            $languageField = new FormTextField("Lingua 1", "um_language", FormFieldType::STRING);
            $languageField->note = "Lingua madre";
            $form->addField($languageField);
            
            $form->addField(new FormCheckField("Studente con background migratorio", "um_pep", FormFieldType::STRING, ['1' => '']));
            $form->addField(new FormHtmlField('<div id="pep_fields" style="display:none;">'));
            
            $dateInItaliaField = new FormDateMultipleField("Data di arrivo in Italia", "um_date_italy", FormFieldType::DATE_TIME);
            $dateInItaliaField->displayDay = false;
            $dateInItaliaField->displayMonth = false;
            $form->addField($dateInItaliaField);
            $form->addField(new FormTextField("Livello di scolarizzazione paese di ordigine", "um_pep_school", FormFieldType::STRING, null, ["maxLength" => 200]));

            $diagnosy = new FormTextAreaField("Diagnosi", "um_diagnosis", FormFieldType::STRING);
            $form->addField($diagnosy);

            $form->addField(new FormHtmlField('</div>'));
        }
        
        $form->addField(new FormHtmlField('
                                    </div>
                                </div>
                                <div class="sezione">
                                    <div class="section-title margin row">
                                        <div class="large-9 medium-7 columns">
                                            <h3>Dati scuola</h3>
                                        </div>
                                    </div>
                                    <div class="info">'));
        $year = new FormTextField("Anno scolastico", "uyn_school_year", FormFieldType::STRING, false, $currentYear['school_year']);
        $year->readOnly = true;
        $form->addField($year);
        
        $plessi = StructureService::findAllWithParent($this->structureId);
        $values = ["" => ""];
        foreach ($plessi as $plesso) {
            $values[$plesso["structure_id"]] = $this->structure["name"] . " - " . $plesso["name"];
        }
        $form->addField(new FormSelectField("Plesso", "uy_plesso_structure_id", FormFieldType::NUMBER, $values, true));

        $classes = ['' => ''] + DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $form->addField(new FormSelectField("Classe", "uy_classe", FormFieldType::STRING, $classes, true));
        $form->addField(new FormTextField("Sezione", "uy_sezione", FormFieldType::STRING, false, "", [
            "maxlength" => 20
        ]));
        $form->addField(new FormTextField("Titolo di studio raggiunto", "um_titolo_di_studio", FormFieldType::STRING, false, "", [
            "maxlength" => 255
        ]));
        $form->addField(new FormTextField("Dirigente", "uy_manager", FormFieldType::STRING, false, $lastUserYear['manager'], [
            "maxlength" => 255
        ]));
        $form->addField(new FormTextField("Referente BES/DSA", "uy_ref_bes", FormFieldType::STRING, false, $lastUserYear['ref_bes'], [
            "maxlength" => 255
        ]));

        if ($this->documentType == DocumentType::PDP_LINGUA){
            $form->addField(new FormTextField("Referente Intercultura", "um_referente_intercultura", FormFieldType::STRING, false, null, ["maxlength" => 255]));
            $form->addField(new FormTextField("Coordinatore/Tutor di classe", "um_coordinatore_tutor", FormFieldType::STRING, false, $lastUserYear['ref_bes']));
            $form->addField(new FormTextField("Docente di sostegno linguistico", "um_docente_sost_ling", FormFieldType::STRING, false, null, ["maxLength" => 255]));
            $form->addField(new FormTextAreaField("Altri insegnanti di classe", "uy_ref_others", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
            $form->addField(new FormTextField("Ore di frequenza scolastica", "um_school_hours", FormFieldType::STRING, false, null, ["maxLength" => 100]));

            $yesNo = ['' => ''] + DictionaryService::findGroupAsDictionary(Dictionary::YES_NO);
            $form->addField(new FormSelectField("Attività di sostegno linguistico", "um_attivita_sostegno_linguistico", FormFieldType::STRING, $yesNo, false));

            $form->addField(new FormTextAreaField("Note", "um_note", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        } else {
            $form->addField(new FormTextField("Coordinatore/Tutor di classe", "um_coordinatore_tutor", FormFieldType::STRING, false, $lastUserYear['ref_bes']));
            
            $form->addField(new FormTextField("Insegnante di sostegno/ supplementare", "uy_ref2", FormFieldType::STRING, false, "", [
                "maxlength" => 255
            ]));
            $form->addField(new FormTextField("Ore di sostegno sulla classe", "um_sostegno_hours", FormFieldType::STRING, false, null, ["maxLength" => 100]));

            $form->addField(new FormTextField("Collaboratori all’integrazione", "uy_collaboratori_integrazione", FormFieldType::STRING, false));
            $form->addField(new FormTextField("Ore effettive di collaboratore escluse le ore di programmazione", "uy_collaboratori_integrazione_ore", FormFieldType::NUMBER, false, "0", [
                "numeric" => true
            ]));
            $form->addField(new FormTextAreaField("Altri insegnanti di classe", "uy_ref_others", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
            $form->addField(new FormTextAreaField("Altre figure extrascolastiche", "uy_others_extrasc", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
            
            $form->addField(new FormTextField("Ore di frequenza scolastica", "um_school_hours", FormFieldType::STRING, false, null, ["maxLength" => 100]));

            if ($this->documentType == DocumentType::PEI){
                $form->addField(new FormTextAreaField("Servizio di trasporto", "um_servizio_trasporto", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
            }

            $form->addField(new FormTextareaField("Osservazioni da parte dell'insegnante", "um_teacher_note", FormFieldType::STRING, null, ["maxLength" => 1000]));
            $form->addField(new FormTextField("Precedente percorso scolastico", "um_precedente_percorso", FormFieldType::STRING, false, null, ["maxLength" => 200]));

            if ($this->documentType == DocumentType::PEI){
                $form->addField(new FormTextAreaField("Quadro informativo famiglia (riportare le informazioni come riferite dalla famiglia)", "um_family_note", FormFieldType::STRING));
            }

            $form->addField(new FormTextAreaField("Segnalazioni da parte dell'alunno", "um_student_note", FormFieldType::STRING, null, ["maxLength" => 2000]));
            $form->addField(new FormTextAreaField("Progetti in alternanza scuola lavoro", "um_school_work_projects", FormFieldType::STRING, null, ["maxLength" => 2000]));
        }

        $form->addField((new FormTextareaField("Diagnosi", "uy_diagnosi", FormFieldType::STRING, false, null, ["maxLength" => 2000])));

        $journal = JournalService::findLastFor(JournalEntity::PEI, $this->documentId, JournalSection::PEI_ANAGRAFICA);
        if ($journal != null){

            ob_start();
            ?>
            <div class="row text-center" style="margin-top: 30px;">
                <strong>Modificato il: </strong> <?= _t(TextService::formatDate( $journal["insert_date"] )) ?>
                <?php if (UserRoleService::canCurrentUserDo(UserPermission::LAST_EDIT_USER_VIEW)) { ?>
                    <strong>da:</strong> <?= _t($journal["ue_name"]) ?> <?= _t($journal["ue_surname"]) ?>
                <?php } ?>
            </div>
            <?php
            $jdata = ob_get_clean();

            $form->addField(new FormHtmlField($jdata));
        }

        $form->addField(new FormHtmlField('</div></div>'));
        
        if ($user != null){
            $this->bradcrumbs->add(new Bradcrumb("Studente", UriService::buildPageUrl("/structure/UserAction")));
            $this->bradcrumbs->add(new Bradcrumb("Modifica"));
        }
        
        if ($form->isSubmit() && $form->checkFields()){
            $data = [];
            foreach($form->fields as $key => $value){
                $data[$value->name] = $value->getValueForDb();
            }
            
            $newUser = ReflectionUtils::getPropertyWithPrefix($data, "u_");
            if ($user == null){
                $newUser['created_by_user_id'] = $this->currentUser['user_id'];
                $newUser['insert_date'] = 'NOW()';
                $newUser["section"] = UserSection::PEI;

                $userId = EM::insertEntity("user", $newUser);
                $user = UserService::find($userId);

                EM::insertEntity("user_role", ['user_id' => $userId, 'role_id' => UserRole::USER]);
                StructureService::addUserStructure($userId, $this->structureId, [UserRole::STUDENTE]);

                if (!UserRoleService::canCurrentUserDo(UserPermission::STRUCTURE_STUDENTS_READ_ALL)){
                    EM::insertEntity("user_user", [
                        "user_id" => $userId,
                        "to_user_id" => $this->currentUser["user_id"],
                        "insert_by_user_id" => $this->currentUser["user_id"],
                        "insert_date" => "NOW()"
                    ]);
                }
            } else {
                unset($newUser['code']);
                EM::updateEntity("user", $newUser, ["user_id" => $user["user_id"]]);
            }
            
            $newUserYear = ReflectionUtils::getPropertyWithPrefix($data, "uy_");
            $journalAction = JournalAction::UPDATE;
            if ($userYear == null){
                $newUserYear['user_id'] = $userId;
                $userYearId = EM::insertEntity("user_year", $newUserYear);
                $userYear = UserYearService::find($userYearId);
                $journalAction = JournalAction::ADDED;
            } else {
                EM::updateEntity("user_year", $newUserYear, ["user_year_id" => $userYear["user_year_id"]]);
            }
            
            if ($document == null){  
                $this->documentId = DocumentService::create($user["user_id"], $this->currentUser['user_id'], $this->documentType);
            } 

            UserService::updateLastEdit($user["user_id"]);
            DocumentService::updateLastEdit($this->documentId);
            
            EM::insertEntity("journal", [
                "user_id" => $this->currentUser["user_id"],
                "insert_date" => "NOW()",
                "entity" => JournalEntity::PEI,
                "entity_id" => $this->documentId,
                "section" => JournalSection::PEI_ANAGRAFICA,
                "action" => $journalAction
            ]);
            
            $metadata = ReflectionUtils::getPropertyWithPrefix($data, "um_");
            MetadataService::removeMetadataWithKey("user", $user["user_id"], $metadata);
            MetadataService::persist("user", $user["user_id"], $metadata);
            
            redirect( UriService::buildPageUrl("/structure/UserAction", "", ["documentId" => $this->documentId]) );
        }
        
        $this->header();
        ?>
        <script type="text/javascript">
        $(function(){
            classeChanged();
            $('#uy_classe').on('change', classeChanged);
            
            pepChanged();
            $('#um_pep_1').on('change', pepChanged);
        });
        
        function pepChanged(){
            var value = $('#um_pep_1').is(':checked');
            if (value){
                $('#pep_fields').show();
            } else {
                $('#pep_fields').hide();
            }
        }
        
        function classeChanged(){
            var value = $('#uy_classe').val();
            if (value.indexOf("S2") == 1){
                $('#campo_form_um_titolo_di_studio').show();
                $('#campo_form_um_school_work_projects').show();
                $('#campo_form_um_student_note').show();
            } else {
                $('#um_titolo_di_studio').val('');
                $('#campo_form_um_titolo_di_studio').hide();
                $('#um_school_work_projects').val('');
                $('#campo_form_um_school_work_projects').hide();
                $('#campo_form_um_student_note').hide();
            }
        }
        </script>
        <div class="content active tab-nuovo-studente row" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-9 medium-7 columns">
                    <h3>Dati studente</h3>
                </div>
            </div>
            <div class="sezioni">
                <?php $form->draw(); ?>
            </div>
        </div>
        <?php
        $this->footer();
    }
}