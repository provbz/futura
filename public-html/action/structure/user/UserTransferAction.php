<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once __DIR__ . '/../StructureUserAction.php';

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserTransferAction extends StructureUserAction{
    
    public $to_structure_id;
    public $school_year_id;
    
    public function _default(){
        if (StringUtils::isBlank($this->getGlobalSessionValue("backUrl"))){
            $this->setGlobalSessionValue("backUrl", $_SERVER["HTTP_REFERER"]);
        }

        $this->header();
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], $this->getGlobalSessionValue("backUrl")));
        $this->bradcrumbs->add(new Bradcrumb("Trasferimento"));
        
        $form = $this->manageForm();

        ?>
        <div class="content active tab-nuovo-studente row" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="medium-12 columns">
                    <h3>
                        Trasferimento dello studente
                    </h3>
                </div>
            </div>
            <div>
                <p>
                    Selezionare l'istituto in cui si trasferirà lo studente e l'anno scolastico di trasferimento. 
                    Se verrà selezionato l'anno scolastico corrente i dati saranno inviati immediatamente.
                    Se si selezionerà l'anno scolastico successivo l'istituto di destinazione avrà la possibilità di accedere agli allegati
                    dello studente ma il trasferimento effettivo avverrà solo al termine dell'anno scolastico.
                </p>
                <?php $form->draw(); ?>
            </div>
        </div>
        <?php

        $this->footer();
    }

    public function manageForm(){
        $form = new Form($this->actionUrl("_default", [
            "userId" => $this->user["user_id"]
        ]));
        $form->cancellAction = $this->getGlobalSessionValue("backUrl");
        
        $form->entity = EM::execQuerySingleResult("SELECT * 
                from user_transfer_request utr
                left join user u ON utr.user_id=u.user_id
                WHERE utr.user_id=:user_id 
                AND utr.type=:type 
                AND utr.status=:status 
                AND utr.from_structure_id=:from_structure_id", [
                    "user_id" => $this->user["user_id"],
                    "type" => UserTransferRequestType::FROM_SOURCE,
                    "status" => UserTransferService::STATUS_PENDING,
                    "from_structure_id" => $this->structureId
                ]);
        
        $codeField = new FormTextField("Codice", "", FormFieldType::STRING, false, $this->user["code"]);
        $codeField->readOnly = true;
        $form->addField($codeField);
        
        $codeField = new FormTextField("Nome", "", FormFieldType::STRING, false, $this->user["name"]);
        $codeField->readOnly = true;
        $form->addField($codeField);

        $codeField = new FormTextField("Cognome", "surname", FormFieldType::STRING, false, $this->user["surname"]);
        $codeField->readOnly = true;
        $form->addField($codeField);

        $codeField = new FormTextField("Data nascita", "birth_date", FormFieldType::STRING, false, $this->user["birth_date"]);
        $codeField->readOnly = true;
        $form->addField($codeField);

        $structures = StructureService::findAllRootAsDictionary(true);
        $structureToShow = [];
        foreach ($structures as $structureId => $structureName) {
            if ($structureId == $this->structureId){
                continue;
            }
            $structureToShow[$structureId] = $structureName;
        }
        $form->addField(new FormSelectField("Istituto comprensivo", "to_structure_id", FormFieldType::STRING, $structureToShow, false));
        
        $currentSchoolYear = SchoolYearService::findCurrentYear();
        $schoolYears = SchoolYearService::findAll();
        $schoolYearsValues[""] = "";
        foreach ($schoolYears as $schoolYear){
            if ($schoolYear['school_year_id'] < $currentSchoolYear['school_year_id']){
                continue;
            }
            $schoolYearsValues[$schoolYear['school_year_id']] = $schoolYear['school_year'];
        }
        
        $schoolYearField = new FormSelectField("Anno scolastico", "school_year_id", FormFieldType::STRING, $schoolYearsValues, true);
        if ($form->entity != null){
            $schoolYearField->readOnly = true;
        }
        $form->addField($schoolYearField);
        
        if ($form->isSubmit() && $form->checkFields()){
            if ($this->to_structure_id == ""){
                UserTransferService::deletePendingRequests($this->user['user_id'], $this->school_year_id);
                redirectWithMessage($this->actionUrl("_default", ["userId" => $this->user['user_id']]), "Richiesta di trasferimento rimossa.");
            } else {
                $utr = UserTransferService::addRequest($this->user['user_id'], 
                    $this->to_structure_id, 
                    UserTransferRequestType::FROM_SOURCE, 
                    $this->school_year_id
                );

                $userDropOutDocuments = DocumentService::findForUserAndYearAndType($this->user["user_id"], $currentSchoolYear["school_year_id"], DocumentType::DROP_OUT);
                $userDropOutDocument = $userDropOutDocuments->fetch();
                if ($userDropOutDocument != null){
                    DropOutService::insertUserTranser($userDropOutDocument["user_year_document_id"], $this->to_structure_id);
                }

                EM::deleteEntity("structure_class_user", [
                    "user_id" => $this->user["user_id"]
                ]);

                if ($this->school_year_id == $currentSchoolYear['school_year_id']){
                    UserTransferService::setStatus($utr['user_transfer_request_id'], UserTransferService::STATUS_APPROVED, "", false);
                    redirectWithMessage(UriService::buildPageUrl("/structure/HomeAction"), "Trasferimento eseguito");
                } else {
                    redirectWithMessage($this->actionUrl("_default", ["userId" => $this->user['user_id']]), "Trasferimento pianificato");
                }
            }
        }

        return $form;
    }
}
