<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once __DIR__ . '/FormCodesField.php';
require_once __DIR__ . "/../StructureAction.php"; 

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EmodelAction extends StructureAction {

    public $id;
    
    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());
        
        $this->header();
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row-100">
                    <div class="large-8 medium-7 small-7 columns">
                        <h3>Modello E</h3>
                    </div>
                    <div class="large-4 medium-3 small-5 columns buttontoolbar">
                        <a href="<?= $this->actionUrl("edit") ?>" class="button radius nuovo float-right">
                            <i class="<?= Icon::PLUS ?> margin-right"></i> Nuovo
                        </a>
                    </div>
                </div>
                
                <div class="mb-structure-e-model-table-container"></div>
                <script>
                    MbCreateVue('mb-structure-e-model-table', 'mb-structure-e-model-table-container', <?= json_encode([]) ?>);
                </script>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    public function edit(){
        $emodel = EModelService::find($this->id);
        if ($this->id != null && $emodel == null){
            redirect(UriService::accessDanyActionUrl());
        }
        if ($emodel != null && $emodel["structure_id"] != $this->structureId){
            redirect(UriService::accessDanyActionUrl());
        }

        $form = $this->buildForm($emodel);
        if ($emodel != null && $emodel["edited"] == 1){
            $form->displaySubmit = false;
            
            foreach($form->fields as $field){
                if ($field->name == "school_year_id"){
                    continue;
                }

                $field->readOnly = true;
            }
        }

        $this->persist($form);

        if ($form->isSubmit() && $form->isValid()){
            EM::updateEntity("e_model", [
                "edited" => 1
            ], [
                "e_model_id" => $this->id
            ]);
            
            if (!NoticeService::noticeExists(EntityName::E_MODEL, $this->id, NoticeType::E_MODEL_EDIT)){
                NoticeService::persist([
                    "type" => NoticeType::E_MODEL_EDIT,
                    "parameters" => json_encode(["e_model_id" => $this->id]),
                    "entity" => EntityName::E_MODEL,
                    "entity_id" => $this->id
                ]);
            }
            
            redirectWithMessage($this->actionUrl("_default"), "Dati salvati");
        }
        
        $this->header();
        $this->formScripts();

        $primariaId = PropertyService::find(PropertyNamespace::MODELLO_E, PropertyKey::primaria_current_year_id);
        $secondariaId = PropertyService::find(PropertyNamespace::MODELLO_E, PropertyKey::secondaria_current_year_id);
        $eModelFormData = [
            "primaria_school_year" => SchoolYearService::find($primariaId["value"]),
            "secondaria_school_year" => SchoolYearService::find($secondariaId["value"]),
        ];
        ?>
        <div class='MbEModelForm-container'></div>
        <script>
            MbCreateVue('MbEModelForm', 'MbEModelForm-container', <?= json_encode($eModelFormData) ?>);
        </script>
        
        <div class="section-title row">
            <div class="large-10 medium-9 small-7 columns">
                <h3>Modello E</h3>
            </div>
        </div>
        <div class="table-row row">
            <div class="large-12 columns">
                <div class="tabs-content">
                    <?php if ($emodel != null && $emodel["edited"] == 1){ ?>
                        <div class="mb-5 alert-box warning ">
                            Il modulo è in attesa di verifica da parte dell'amministratore. Non sarà possibile eseguire modifiche prima dell'avvenuta verifica.
                        </div>
                    <?php } ?>
                    <?php $form->draw(); ?>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function buildForm($emodel){
        $form = new Form($this->actionUrl("edit", ["id" => $this->id]));
        $form->cancellAction = $this->actionUrl("_default");
        
        $form->entity = $emodel;
        
        $plessi = StructureService::findAllWithParent($this->structureId);
        $values = ["" => ""];
        foreach ($plessi as $plesso) {
            $values[$plesso["structure_id"]] = $this->structure["name"] . " - " . $plesso["name"];
        }
        $form->addField(new FormSelectField("Plesso", "plesso_structure_id", FormFieldType::NUMBER, $values, true));

        $classes = ['' => ''] + DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $classField = new FormSelectField("Classe", "class", FormFieldType::STRING, $classes, true);
        $form->addField($classField);

        $form->addField(new FormTextField("Sezione", "sezione", FormFieldType::STRING, false, "", ["maxlength" => 20]));

        $alunniPerClasseField = new FormTextField("Numero alunni per classe", "alunni_per_classe", FormFieldType::NUMBER, false, "", [
            "numeric" => true
        ]);
        $form->addField($alunniPerClasseField);

        $form->addField(new FormCheckField("Pluriclasse", "pluriclasse", FormFieldType::NUMBER, [1 => "Sì"]));

        $scollYears = SchoolYearService::findAllAsObject();
        $yearField = new FormSelect2Field("Anno scolastico", "school_year_id", FormFieldType::NUMBER, $scollYears, true);
        $yearField->readOnly = true;
        
        $form->addField($yearField);

        $codeField = new FormTextField("Codice popcorn", "codice", FormFieldType::STRING, true, "", ["maxLength" => 25]);
        $codeField->checkFunction = function($field, $code) use ($emodel, $classField) {
            $primariaSchoolYearId = PropertyService::find(PropertyNamespace::MODELLO_E, PropertyKey::primaria_current_year_id);
            $schoolYearId = $primariaSchoolYearId["value"];

            $secondariaSchoolYearId = PropertyService::find(PropertyNamespace::MODELLO_E, PropertyKey::secondaria_current_year_id);

            $class = $classField->getValue($emodel);
            if (strpos($class, "S2") !== false){
                $schoolYearId = $secondariaSchoolYearId["value"];
            }
            
            $dbEntity = EModelService::findByCodeAndSchoolYear($code, $schoolYearId);
            if ($dbEntity == null || $dbEntity["e_model_id"] == $this->id){
                return;
            }

            $message = "Codice in uso per un altro modello E";
            if ($dbEntity["structure_id"] != $this->structureId){
                $structure = StructureService::find($dbEntity["structure_id"]);
                if ($structure != null){
                    $message .= " nell'istituto: " . $structure["name"];
                }
            }

            $field->addFieldError($message);
        };
        $form->addField($codeField);

        $form->addField(new FormTextField("Nome", "nome", FormFieldType::STRING, true, "", ["maxLength" => 100]));
        $form->addField(new FormTextField("Cognome", "cognome", FormFieldType::STRING, true, "", ["maxLength" => 100]));

        /* Fine dati condivisi */
        $tipoFrequenzaField = new FormSelectField("Tipo frequenza", "tipo_frequenza_dictionary_id", FormFieldType::NUMBER, null, true);
        $tipoFrequenzaField->addEmpty = true;
        $tipoFrequenzaField->dictionaries = DictionaryService::findGroup(Dictionary::MODEL_E_TIPO_FREQUENZA, true);
        $form->addField($tipoFrequenzaField);

        $monteOreField = new FormTextField("Monte ore scolastico", "monte_ore_scolastico", FormFieldType::NUMBER, false, "", [
            "numeric" => true
        ]);
        $monteOreField->note = "Esclusa la mensa e le pause";
        $form->addField($monteOreField);

        $tipoObiettivi = new FormMultiSelectField('Unità oraria', 'unita_oraria');
        $tipoObiettivi->options = DictionaryService::findGroup(Dictionary::e_model_unita_oraria);
        $tipoObiettivi->optionsWithTextArea = ["altro"];
        $tipoObiettivi->inputType = "radio";
        $form->addField($tipoObiettivi);

        $form->addField(new FormHtmlField("<h4>Dati richiesta</h4>"));

        $diagnosyTypes = array_merge(["" => ""], DictionaryService::findGroupAsDictionary(Dictionary::MODEL_E_DIAGNOSY_TYPE));
        $tipoDiagnosiField = new FormSelectField("Tipo diagnosi", "diagnosy_type_dictionary", FormFieldType::STRING, $diagnosyTypes, true);
        $form->addField($tipoDiagnosiField);

        $diagnosiRefertoField = new FormAttachmentsField("Diagnosi/referto", 
            AttachmentType::DIAGNOSI_REFERTO, EntityName::E_MODEL, $this->id);
        $diagnosiRefertoField->extensions[] = "pdf";
        $diagnosiRefertoField->persist = false;
        $diagnosiRefertoField->manAttachments = 4;
        $diagnosiRefertoField->checkFunction = function($field, $attachments) use ($tipoDiagnosiField) {
            $tipo = $tipoDiagnosiField->getValueForDb();
            if (strpos($tipo, "104") === false && strpos($tipo, "170") === false){
                return;
            }

            if (count ($attachments) == 0){
                $field->addFieldError("In base al tipo di diagnosi richiesto l'allegato è necessario.");
            }
        };
        $form->addField($diagnosiRefertoField);
        
        $dataAccertamentoField = new FormDateField("Data dell'invio della richiesta di accertamento", "data_di_accertamento", FormFieldType::DATE);
        $dataAccertamentoField->checkFunction = function($field, $value) use ($tipoDiagnosiField){
            if ($tipoDiagnosiField->getValueForDb() == "in_accertamento" && StringUtils::isBlank($value)){
                $field->addFieldError("Data richiesta");
            }
        };
        $form->addField($dataAccertamentoField);

        if ($this->structure["is_scuola_paritaria"] == 0){
            $hasOreRichiesteField = new FormSelectField("Richiede ore collaboratore", "has_ore_richieste", FormFieldType::NUMBER, [
                0 => 'No',
                1 => 'Sì'
            ], false);
            $hasOreRichiesteField->labelClass = "";
            $hasOreRichiesteField->itemClass = "";

            $oreRichiesteField = new FormTextField("Ore richieste collaboratore", "ore_richieste", FormFieldType::NUMBER, false, "", [
                "numeric" => true
            ]);
            $oreRichiesteField->labelClass = "";
            $oreRichiesteField->itemClass = "";
            $oreRichiesteField->checkFunction = function($field, $value) use ($hasOreRichiesteField){
                if ($hasOreRichiesteField->getValueForDb() == 0){
                    return;
                }

                if (StringUtils::isBlank($value)){
                    $field->addFieldError("Valore richiesto");
                }

                $intValue = intval($value);

                if (strval($intValue) !== strval($value)){
                    $field->addFieldError("È possibile specificare solamente un valore intero.");
                } else if ($intValue > 38){
                    $field->addFieldError("Numero ore massimo consentito: 38");
                } else if ($intValue < 0){
                    $field->addFieldError("È necessario specificare un valore positivo");
                }
            };

            $form->addField(new FormHtmlField("<div class='row-100 form-row' id='ore_richieste_container'>
                <div class='columns small-6'>"));
                $form->addField($hasOreRichiesteField);
            $form->addField(new FormHtmlField("
                </div>
                <div class='columns small-6'>"));
                $form->addField($oreRichiesteField);
            $form->addField(new FormHtmlField("
                </div>
            </div>"));
        } else {
            $oreField = new FormTextField("Richiesta contributo personale coll.all'integrazione/educatore specificare ore settimanali", "ore_richieste", FormFieldType::NUMBER, false, "", [
                "numeric" => true
            ]);
            $oreField->checkFunction = function($field, $value){
                if (StringUtils::isBlank($value)){
                    return;
                }

                $intValue = intval($value);

                if (strval($intValue) !== strval($value)){
                    $field->addFieldError("È possibile specificare solamente un valore intero.");
                } else if ($intValue > 38){
                    $field->addFieldError("Numero ore massimo consentito: 38");
                } else if ($intValue < 0){
                    $field->addFieldError("È necessario specificare un valore positivo");
                }
            };
            $form->addField($oreField);

            $tipoParitariaContributoField = new FormSelectField("Richiesta contributo personale docente di sostegno", "paritaria_contributo_personale_dictionary_id", FormFieldType::NUMBER, null, false);
            $tipoParitariaContributoField->addEmpty = true;
            $tipoParitariaContributoField->dictionaries = DictionaryService::findGroup(Dictionary::MODEL_E_TIPO_FREQUENZA, true);
            $form->addField($tipoParitariaContributoField);
        }
        
        $form->addField(new FormHtmlField("<div class='row-100 form-row '>
            <div class='columns small-6'>"));
        $graveCompromissioneField = new FormCheckField("Grave compromissione sociale", "grave_compromissione_sociale", FormFieldType::NUMBER, [1 => "Sì"]);
        $graveCompromissioneField->labelClass = "";
        $graveCompromissioneField->itemClass = "";
        $form->addField($graveCompromissioneField);
        
        $dataEmissioneField = new FormDateField("Data emissione", "data_emissione", FormFieldType::DATE);
        $dataEmissioneField->labelClass = "";
        $dataEmissioneField->itemClass = "";
        
        $dataEmissioneField->checkFunction = function($field, $value) use ($graveCompromissioneField){
            if ($graveCompromissioneField->getValueForDb() != 0 && StringUtils::isBlank($value)){
                $field->addFieldError("Specificare la data di emissione");
            }
        };

        $form->addField(new FormHtmlField("</div>
            <div class='columns small-6'>"));
        $form->addField($dataEmissioneField);
        $form->addField(new FormHtmlField("
                </div>
            </div>"));
        
        $dbCodes = [];
        if ($emodel != null){
            $dbCodes = EModelService::findCodes($emodel["e_model_id"]);
            $dbCodes = $dbCodes->fetchAll();
        }

        $codesFields = new FormCodesField("Codici diagnosi", "codes", $dbCodes);
        $codesFields->required = false;
        $codesFields->checkFunction = function($field, $value) use ($tipoDiagnosiField){
            if ($tipoDiagnosiField->getValueForDb() == "in_accertamento"){
                return;
            }

            if ($value == null || count($value) == 0){
                $field->addFieldError("Data richiesta");
            }
        };
        $form->addField($codesFields);

        if ($emodel != null && $emodel["closed"] === 1){
            foreach($form->fields as $field){
                $field->readOnly = true;
            }

            $form->displaySubmit = false;
        }

        return $form;
    }

    public function persist($form){
        if (!$form->isSubmit() || !$form->checkFields()){
            return;
        }

        foreach($form->fields as $key => $value){
            if (StringUtils::isBlank($value->name)){
                continue;
            }

            if ($value->name == "codes" || 
                !$value->persist || 
                $value->readOnly){
                continue;
            }
            
            $emodel[$value->name] = $value->getValueForDb();
        }

        $journalAction = JournalAction::UPDATE;
        $emodel["structure_id"] = $this->structureId;
        $emodel["last_edit_date"] = "NOW()";
        $emodel["last_edit_user_id"] = $this->currentUser['user_id'];

        $oldEmodel = EModelService::find($this->id);
        $changes = $this->prepareChanges($oldEmodel, $emodel, $form);

        if ($this->id == null){
            $emodel["insert_date"] = "NOW()";
            $emodel["insert_user_id"] = $this->currentUser['user_id'];

            if (strpos( $emodel["class"], "S2") === false){
                $primariaSchoolYearId = PropertyService::find(PropertyNamespace::MODELLO_E, PropertyKey::primaria_current_year_id);
                $emodel["school_year_id"] = $primariaSchoolYearId["value"];
            } else {
                $secondariaSchoolYearId = PropertyService::find(PropertyNamespace::MODELLO_E, PropertyKey::secondaria_current_year_id);
                $emodel["school_year_id"] = $secondariaSchoolYearId["value"];
            }
            
            $this->id = EM::insertEntity("e_model", $emodel);
            $journalAction = JournalAction::ADDED;
        } else {
            EM::updateEntity("e_model", $emodel, ["e_model_id" => $this->id]);
        }
        
        $codesField = $form->getFieldWithName("codes");
        if (!$codesField->readOnly){
            $oldCodes = EModelService::findCodes($this->id);
            $codeChange = $this->getCodeChanges($oldCodes->fetchAll(), $codesField->values);
            if ($codeChange != null){
                $changes[] = $codeChange;
            }

            EModelService::detachCodes($this->id);
            foreach($codesField->values as $code){
                EM::insertEntity("e_model_code", [
                    "code" => $code->code,
                    "description" => $code->description,
                    "gravita_dictionary_id" => property_exists($code, 'gravita_dictionary_id') ? $code->gravita_dictionary_id : null,
                    "e_model_id" => $this->id
                ]);
            }
        }

        if (count($changes) > 0){
            $journalId = EM::insertEntity("journal", [
                "entity" => EntityName::E_MODEL,
                "entity_id" => $this->id,
                "insert_date" => "NOW()",
                "action" => $journalAction,
                "user_id" => $this->currentUser["user_id"]
            ]);

            foreach ($changes as $change) {
                $change["journal_id"] = $journalId;
                EM::insertEntity("journal_row", $change);
            }
        }

        $attachmentField = $form->getFieldWithName(AttachmentType::DIAGNOSI_REFERTO);
        if (!$attachmentField->readOnly){
            $attachmentField->persist($this->id);
        }
    }

    private function getCodeChanges($old, $new){
        $changes = null;

        if ($old == null){
            $old = [];
        }

        $gravitaDictionary = DictionaryService::findGroupAsDictionary(Dictionary::e_model_gravia, false, "dictionary_id");

        $oldCodes = [];
        foreach ($old as $code) {
            $entry = [
                "code" => $code["code"],
                "description" => $code["description"],
                "gravita_dictionary_id" => null
            ];

            if (isset($gravitaDictionary[$code["gravita_dictionary_id"]])){
                $entry["gravita_dictionary_id"] = $gravitaDictionary[$code["gravita_dictionary_id"]];
            }

            $oldCodes[] = $entry;
        }

        $newCodes = [];
        foreach($new as $code){
            $entry = [
                "code" => $code->code,
                "description" => $code->description,
                "gravita_dictionary_id" => null
            ];
            
            if (property_exists($code, "gravita_dictionary_id") && $code->gravita_dictionary_id != null && isset($gravitaDictionary[$code->gravita_dictionary_id])){
                $entry["gravita_dictionary_id"] = $gravitaDictionary[$code->gravita_dictionary_id];
            }

            $newCodes[] = $entry;
        }

        if ($this->hasDifferentCodes($oldCodes, $newCodes)){
            $changes = [
                "field_name" => "e_model_codes",
                "old_value" => json_encode( $oldCodes ),
                "new_value" => json_encode( $newCodes )
            ];
        }

        return $changes;
    }

    private function hasDifferentCodes($oldCodes, $newCodes){
        if (count($oldCodes) != count($newCodes)){
            return true;
        }

        foreach($oldCodes as $code){
            if (!ArrayUtils::contains($newCodes, $code)){
                return true;
            }
        }

        foreach($newCodes as $code){
            if (!ArrayUtils::contains($oldCodes, $code)){
                return true;
            }
        }
        return false;
    }

    private function prepareChanges($old, $new, $form){
        $changes = [];
        $excludeKeys = [];

        $formFieldNames = [];
        foreach ($form->fields as $field) {
            if ($field->readOnly){
                continue;
            }

            $formFieldNames[] = $field->name;
        }

        if ($old == null){
            $old = [];
        }

        $keysMerge = array_merge(array_keys($old), array_keys($new));
        $keys = array_unique($keysMerge, SORT_REGULAR);
        
        foreach($keys as $key){
            if (in_array($key, $excludeKeys)){
                continue;
            }

            if (!in_array($key, $formFieldNames)){
                continue;
            }

            $oldValue = isset($old[$key]) ? $old[$key] : null;
            $newValue = isset($new[$key]) ? $new[$key] : null;

            if (strpos($key, "_date") !== false || strpos($key, "data_") !== false){
                if ($oldValue != null && strlen($oldValue) > 10){
                    $oldValue = substr($oldValue, 0, 10);
                }
                if ($newValue != null && strlen($newValue) > 10){
                    $newValue = substr($newValue, 0, 10);
                }   
            }

            if ($oldValue == $newValue){
                continue;
            }

            if (strpos($key, "_dictionary_id") !== false){
                if ($oldValue != null){
                    $oldValue = json_encode( DictionaryService::find($oldValue) );
                }

                if ($newValue != null){
                    $newValue = json_encode( DictionaryService::find($newValue) );
                }
            } else if (strpos($key, "structure_id") !== false){
                if ($oldValue != null){
                    $oldValue = json_encode( StructureService::find($oldValue) );
                }

                if ($newValue != null){
                    $newValue = json_encode( StructureService::find($newValue) );
                }
            } else if (strpos($key, "diagnosy_type_dictionary") !== false){
                if ($oldValue != null){
                    $oldValue = json_encode( DictionaryService::findByGroupAndKey(Dictionary::MODEL_E_DIAGNOSY_TYPE, $oldValue) );
                }

                if ($newValue != null){
                    $newValue = json_encode( DictionaryService::findByGroupAndKey(Dictionary::MODEL_E_DIAGNOSY_TYPE, $newValue) );
                }
            } else if ($key == "class"){
                if ($oldValue != null){
                    $oldValue = json_encode( DictionaryService::findByGroupAndKey(Dictionary::CLASS_GRADE, $oldValue) );
                }

                if ($newValue != null){
                    $newValue = json_encode( DictionaryService::findByGroupAndKey(Dictionary::CLASS_GRADE, $newValue) );
                }
            }

            $changes[] = [
                "field_name" => $key,
                "old_value" => $oldValue,
                "new_value" => $newValue
            ];
        }

        return $changes;
    }

    public function formScripts(){
        ?>
        <script type="text/javascript">
            
            $(function(){
               $('#diagnosy_type_dictionary').on('change', diagnosy_type_dictionary);
               diagnosy_type_dictionary();
               
               $("#grave_compromissione_sociale_1").on('change', grave_compromissione_sociale)
               grave_compromissione_sociale();

               $('#has_ore_richieste').on('change', onChangeHasOreRichieste)
               onChangeHasOreRichieste();
            });

            function onChangeHasOreRichieste(){
                var val = $("#has_ore_richieste").val();
                
                $('#campo_form_ore_richieste').hide();
                if (val == 1){    
                    $('#campo_form_ore_richieste').show();
                }
            }

            function diagnosy_type_dictionary(){
                var val = $('#diagnosy_type_dictionary').val();
                
                $('#campo_form_data_di_accertamento').hide();
                $('#campo_form_diagnosi_referto').hide();
                $('#ore_richieste_container').hide();
                $('#campo_form_grave_compromissione_sociale').hide();
                $('#campo_form_data_emissione').hide();
                $('#campo_form_codes').hide();
                $('#campo_form_paritaria_contributo_personale_dictionary_id').hide();

                if (val == 'in_accertamento'){
                    $('#campo_form_data_di_accertamento').show();    
                } else if (val != ''){
                    $('#ore_richieste_container').show();
                    $('#campo_form_diagnosi_referto').show();
                    $('#campo_form_grave_compromissione_sociale').show();
                    $('#campo_form_data_emissione').show();
                    $('#campo_form_codes').show();
                    $('#campo_form_paritaria_contributo_personale_dictionary_id').show();

                    grave_compromissione_sociale();
                    onChangeHasOreRichieste();
                }
            }

            function grave_compromissione_sociale(){
               var value = $("#grave_compromissione_sociale_1").prop('checked');
               $('#campo_form_data_emissione').hide();
               if (value){
                    $('#campo_form_data_emissione').show();    
               } else {
                   $('#data_emissione').val("");
               }
            }
        </script>
        <?php
    }

    public function remove(){
        $emodel = EModelService::find($this->id);
        if ($emodel == null){
            return;
        }

        if ($emodel["structure_id"] != $this->structureId){
            return;
        }

        EM::deleteEntity("e_model", ["e_model_id" => $emodel["e_model_id"]]);
    }
}
