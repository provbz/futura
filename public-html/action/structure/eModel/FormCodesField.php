<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormCodesField
 *
 * @author Marco
 */
class FormCodesField extends FormField{
    
    public $values;
    
    public function __construct($label, $name, $values) {
        parent::__construct($label, $name, FormFieldType::STRING, true);
        $this->persist = false;
        
        $this->values = $values;
        if ($this->values == null){
            $this->values = [];
        }
    }
    
    public function check() {
        $value = getRequestValue($this->name . "_data");
        $this->values = $this->decodedValues($value);
        
        if ($this->required && ($this->values == null || count($this->values) == 0)){
            $this->addFieldError("Dato non facoltativo");
            return;
        }

        foreach ($this->values as $item) {
            if (StringUtils::isBlank($item->description)){
                $this->addFieldError("La descrizione è richiesta");
                return;
            }
        }

        if ($this->checkFunction != NULL){
            $fn = $this->checkFunction;
            $fn($this, $this->values);
        }
    }

    public function draw($entity = NULL) {
        ?>
        <div class="mb_e_model_codes_container"></div>
        <script type="text/javascript">
            <?php
            $parameters = [
                "name" => $this->name,
                "read_only" => $this->readOnly,
                "value" => $this->values,
                "errors" => $this->validationErrors
            ];
            ?>
            MbCreateVue('mb-e-model-codes-field', 'mb_e_model_codes_container', <?= json_encode($parameters) ?>)
        </script>
        <?php
    }
    
    public function encodeValue($value){
        $value = json_encode($value);
        $value = htmlentities($value);
        return $value;
    }
    
    public function decodedValues($value){
        $val = urldecode($value);
        return json_decode($val);
    }
}
