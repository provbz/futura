<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once __DIR__ . '/PdpLinguaBaseAction.php';

/**
 * Description of PdpLinguaBaseAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SituazionePartenzaAction extends PdpLinguaBaseAction {
    
    public function _default(){
        $entity = [];
        ReflectionUtils::fillWithPrefix($this->userMetadata, "um_", $entity);
        
        $form = new Form($this->actionUrl("_default", ["documentId" => $this->documentId]));
        $form->entityName = "user";
        $form->entity = $entity;
        $form->cancellAction = UriService::buildPageUrl("/structure/UserAction", NULL, ["documentId" => $this->documentId]);

        $riferimenti = new FormMultiSelectField('Facendo riferimento a', 'um_pdp_lin_riferimenti');
        $riferimenti->options = DictionaryService::findGroup(Dictionary::PDP_LIN_RIFERIMENTI);
        $form->addField($riferimenti);

        $form->addField(new FormHtmlField("<p>Si riscontrano i seguenti:</p>"));

        $puntiForza = new FormMultiSelectField('Punti di forza', 'um_pdp_lin_pforza');
        $puntiForza->options = DictionaryService::findGroup(Dictionary::PDP_LIN_PUNTI_FORZA);
        $altroDictionary = $puntiForza->options[count($puntiForza->options) - 1];
        if ($altroDictionary != null){
            $puntiForza->optionsWithTextArea = [
                $altroDictionary["key"]
            ];
        }
        $form->addField($puntiForza);

        $puntiDebolezza = new FormMultiSelectField('Punti di debolezza', 'um_pdp_lin_pdebolezza');
        $puntiDebolezza->options = DictionaryService::findGroup(Dictionary::PDP_LIN_PUNTI_DEBOLEZZA);
        $altroDictionary = $puntiDebolezza->options[count($puntiDebolezza->options) - 1];
        if ($altroDictionary != null){
            $puntiDebolezza->optionsWithTextArea = [
                $altroDictionary["key"]
            ];
        }
        $form->addField($puntiDebolezza);

        $form->addField(new FormTextAreaField("Altre osservazioni", "um_pdp_lin_altre_oss", FormFieldType::STRING, null, ["maxLength" => 2000]));

        $journal = JournalService::findLastFor(JournalEntity::PEI, $this->document["user_year_document_id"], JournalSection::PDP_LINGUA_PARTENZA);
        if ($journal != null){
            ob_start();
            ?>
            <div class="row text-center" style="margin-top: 30px;">
                <strong>Modificato il: </strong> <?= _t(TextService::formatDate( $journal["insert_date"] )) ?>
                <?php if (UserRoleService::canCurrentUserDo(UserPermission::LAST_EDIT_USER_VIEW)) { ?>
                    <strong>da:</strong> <?= _t($journal["ue_name"]) ?> <?= _t($journal["ue_surname"]) ?>
                <?php } ?>
            </div>
            <?php
            $jdata = ob_get_clean();

            $form->addField(new FormHtmlField($jdata));
        }

        if ($form->isSubmit() && $form->checkFields()){
            $data = [];
            foreach($form->fields as $key => $value){
                $data[$value->name] = $value->getValueForDb();
            }

            $metadata = ReflectionUtils::getPropertyWithPrefix($data, "um_");
            MetadataService::removeMetadataWithKey(MetadataEntity::USER, $this->user['user_id'], $metadata);
            MetadataService::persist(MetadataEntity::USER, $this->user['user_id'], $metadata);
            $this->updateMetadata();

            DocumentService::updateLastEdit($this->document["user_year_document_id"]);

            EM::insertEntity("journal", [
                "user_id" => $this->currentUser["user_id"],
                "insert_date" => "NOW()",
                "entity" => JournalEntity::PEI,
                "entity_id" => $this->document["user_year_document_id"],
                "section" => JournalSection::PDP_LINGUA_PARTENZA,
                "action" => JournalAction::UPDATE
            ]);
            
            redirect( UriService::buildPageUrl("/structure/UserAction", "_default", ["documentId" => $this->documentId]));
        }

        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], UriService::buildPageUrl("/structure/UserAction", null, [
            "documentId" => $this->documentId
        ])));
        $this->bradcrumbs->add(new Bradcrumb("Partenza"));

        $this->header();
        ?>
        <div class="content active tab-nuovo-studente row" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="medium-12 columns">
                    <h3>Situazione di partenza</h3>
                </div>
            </div>
            <div class="sezioni">
                <?= $form->draw(); ?>
            </div>
        </div>
        <?php
        
        $this->footer();
    }

}
