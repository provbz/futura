<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once __DIR__ . '/PdpLinguaBaseAction.php';

/**
 * Description of CompetenzeLinguisticheAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class CompetenzeLinguisticheAction extends PdpLinguaBaseAction {
    
    public function _default(){
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], UriService::buildPageUrl("/structure/UserAction", null, [
            "documentId" => $this->documentId
        ])));
        $this->bradcrumbs->add(new Bradcrumb("Competenze linguistiche"));

        $data = [
            "documentId" => $this->documentId,
            "value" => isset($this->userMetadata["un_pdp_lin_competenze"]) ? json_decode($this->userMetadata["un_pdp_lin_competenze"]) : [],
            Dictionary::PDP_LIN_LIVELLO_COMPETENZA => DictionaryService::findGroup(Dictionary::PDP_LIN_LIVELLO_COMPETENZA),
        ];

        $this->header();
        ?>
        <div class="content active tab-nuovo-studente row" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="medium-12 columns">
                    <h3>
                        Competenze linguistiche<br/>
                        <small>(secondo il Quadro comune europeo di riferimento per le lingue QCER)</small>
                    </h3>
                    
                </div>
            </div>
            <div class="sezioni">
                <div class="form-container"></div>
                <script>
                    MbCreateVue("MbPdpLinguaForm", "form-container", <?= json_encode($data) ?>);
                </script>
            </div>
        </div>
        <?php
        
        $this->footer();
    }

    public function persist(){
        $data = json_decode(file_get_contents('php://input'), true);

        $metadata = [];
        $metadata['un_pdp_lin_competenze'] = $data;

        MetadataService::removeMetadataWithKey(MetadataEntity::USER, $this->user['user_id'], $metadata);
        MetadataService::persist(MetadataEntity::USER, $this->user['user_id'], $metadata);
        $this->updateMetadata();

        echo "ok";
    }

}
