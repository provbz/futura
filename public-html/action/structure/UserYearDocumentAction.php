<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserTransferRequestAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserYearDocumentAction extends StructureAction{

    public $documentId;
    public $userId;
    public $year;
    public $user_year_id;
    public $type;
    public $u_code;
    public bool $isReplace;

    public function remove(){
        $document = DocumentService::find($this->documentId);
        if ($document == null){
            redirect(UriService::accessDanyActionUrl());
        }

        if ($document["status"] == UserYearDocumentStatus::DELETED){
            redirect(UriService::accessDanyActionUrl());
        }

        if (!StructureService::hasUserStructureRole($document["user_id"], $this->structureId, UserRole::STUDENTE)){
            redirect(UriService::accessDanyActionUrl());
        }

        $replacedDocuments = DocumentService::findReplacedDocuments($this->documentId)->fetchAll();
        if (count( $replacedDocuments ) > 0 && $document["replaced_by_document_id"] != null){
            redirect(UriService::accessDanyActionUrl());
        }
        
        DocumentService::updateStatus($this->documentId, UserYearDocumentStatus::DELETED);
        redirectWithMessage($this->actionUrl("_default"), "Elemento rimosso");
    }

    public function newUser(){
        $currentYear = SchoolYearService::findCurrentYear();
        
        $form = new Form($this->actionUrl("newUser"));
        $form->cancellAction = $this->actionUrl("_default");
        
        $popcornField = new FormTextField("Codice identificativo (POPCORN)", "u_code", FormFieldType::STRING, true);
        $popcornField->maxLength = 45;
        $popcornField->checkFunction = function($f, $value){
            if (strlen($value) < 3 || strlen($value) > 45){
                $f->addFieldError("Codice non valido");
            }   
        };
        $popcornField->note = "Il codice identificativo non sarà più modificabile dopo aver creato l'utente!";
        
        $form->addField($popcornField);
        
        $this->header();
        
        ?>
        <style type="text/css">
            .message p{
                margin-bottom: 48px;
            }
        </style>
        <div class="content active tab-nuovo-studente" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-9 medium-7 columns">
                    <h3>Tipo documento: <?= _t(DocumentService::getDocumentName($this->documentType)) ?></h3>
                    <h3>Dati studente</h3>
                </div>
            </div>
            <div class="sezioni">
                <div class="row">
                    <?php
                    if ($form->isSubmit() && $form->checkFields()){
                        $code = str_replace(' ', '', $popcornField->getValue(NULL));
                        $user = UserService::findByCode($code);
                        if ($user == null){
                            redirect($this->actionUrl("newUserFull", ["u_code" => $code,
                                "documentType" => $this->documentType]));
                            return;
                        } 
                        
                        $isStructureStudent = StructureService::hasUserStructureRole($user['user_id'], $this->structureId, UserRole::STUDENTE);
                        
                        $request = UserTransferService::findFromUserToStructure($user['user_id'], $this->structureId);
                        $otherStructures = StructureService::findUserStructures($user['user_id']);
                        $otherStructure = null;
                        foreach ($otherStructures as $structure) {
                            if ($structure["structure_id"] != $this->structureId){
                                $otherStructure = $structure;
                            }
                        }

                        if ($request != null){ 
                            $this->waitRequest($request, $user);
                        } else if ($otherStructure != NULL){ 
                            $this->otherStructureUser($otherStructure, $user);
                        } else if ($isStructureStudent){  
                            $this->thisStructureUser($code, $user);
                        } else {
                            echo 'Dati non inseriti correttamente, associazione alla struttura assente.';
                        }
                    } else {
                        $form->draw();
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function thisStructureUser($code, $user, $additionalRequestParameters = []){
        $this->setGlobalSessionValue("goBack", null);
        
        $currentYear = SchoolYearService::findCurrentYear();
        $document = EM::execQuerySingleResult("SELECT * FROM user_year_document 
            WHERE user_id=:user_id 
                AND school_year_id=:school_year_id
                AND type = :type
                AND status = :statusReady", [
                    "user_id" => $user['user_id'],
                    "school_year_id" => $currentYear["school_year_id"],
                    "type" => $this->documentType,
                    "statusReady" => UserYearDocumentStatus::READY
                ]);

        $documentDeleted = EM::execQuerySingleResult("SELECT * FROM user_year_document 
            WHERE user_id=:user_id 
                AND school_year_id=:school_year_id
                AND type = :type
                AND status = :statusDeleted
                ORDER BY user_year_document_id DESC", [
                    "user_id" => $user['user_id'],
                    "school_year_id" => $currentYear["school_year_id"],
                    "type" => $this->documentType,
                    "statusDeleted" => UserYearDocumentStatus::DELETED
                ]);

        $prevYearDocument = EM::execQuerySingleResult("SELECT * FROM user_year_document 
            WHERE user_id=:user_id 
                AND type = :type
                AND school_year_id < :school_year_id
                AND status = :statusReady
            ORDER BY school_year_id DESC", [
                "user_id" => $user['user_id'],
                "type" => $this->documentType,
                "school_year_id" => $currentYear["school_year_id"],
                "statusReady" => UserYearDocumentStatus::READY
            ]);
        
        $otherDocuments = DocumentService::findOtherDocuments($this->documentType, $user["user_id"], $currentYear);
        
        ?>
        <div class="message">
            <p>Codice studente: <?= _t($user['code']) ?></p>
        </div>
        <?php 
        if ($document != null){
            ?>
            <div class="message">
                <p>Il codice risulta assegnato ad uno studente registrato in questa scuola.</p>
                <p>Per questo anno scolastico risulta in compilazione un <?= _t(strtoupper( $document['type'] )) ?></p>
                <p>Controllare la lista degli studenti, se il codice non viene visualizzato contattare il referente BES e richiedere la condivisione.</p>
                <a class="button tiny alert" href="<?= $this->actionUrl("newUser", $additionalRequestParameters) ?>">
                    <i class="<?= Icon::CANCELL ?>"></i>
                    Annulla
                </a>
            </div>
            <?php 
        } else if (count( $otherDocuments ) > 0){
            ?>
            <div class="message">
                <p>Il codice risulta assegnato ad uno studente registrato in questa scuola.</p>
                <p>Per questo anno scolastico risulta in compilazione un <?= _t(strtoupper( $otherDocuments[0]['type'] )) ?></p>
                <p>Procedendo, verrà creato un messaggio di sostituzione del documento esistente.</p>
                <a class="button tiny alert" href="<?= $this->actionUrl("newUser", ["documentType" => $this->documentType]) ?>">
                    <i class="<?= Icon::CANCELL ?>"></i>
                    Annulla
                </a>
                <a class="button tiny" href="<?= $this->actionUrl("newUserFull", ["u_code" => $code, 
                    "documentType" => $this->documentType,
                    "isReplace" => true
                    ]) ?>">
                    Procedi
                </a>
            </div>
            <?php
        } else if ($documentDeleted != null){
            ?>
            <div style="padding-left: 25px;">
                <p>Il codice risulta assegnato ad uno studente registrato in questa scuola.</p>
                <p>
                    Un documento associato allo studente risulta cancellato. Se si vuole procedere con il recupero del codice scrivere una mail all'indirizzo servizio.inclusione@provincia.bz.it per richiederne il recupero.
                </p>
                <a class="button tiny alert" href="<?= $this->actionUrl("newUser", ["documentType" => $this->documentType]) ?>">
                    <i class="<?= Icon::CANCELL ?>"></i>
                    Annulla
                </a>
                <!--
                <a class="button tiny" href="<?= $this->actionUrl("restoreDocument", ["documentId" => $documentDeleted["user_year_document_id"]]) ?>">
                    Recupera
                </a>
                --> 
                <a class="button tiny" href="<?= $this->actionUrl("newUserFull", ["u_code" => $code, "documentType" => $this->documentType]) ?>">
                    Crea nuovo 
                </a>
            </div>
            <?php
        } else if ($prevYearDocument != null){
            ?>
            <div class="message">
                <p>Il codice risulta assegnato ad uno studente registrato in questa scuola.</p>
                <p>Un documento associato allo studente risulta caricato in un anno scolastico precedente.</p>
                <p>Come si desidera procedere?</p>
                <a class="button tiny alert" href="<?= $this->actionUrl("newUser") ?>">
                    <i class="<?= Icon::CANCELL ?>"></i>
                    Annulla
                </a>
                <a class="button tiny" href="<?= $this->actionUrl("updateSchoolYearBtn", ["documentId" => $prevYearDocument["user_year_document_id"]]) ?>">
                    Recupera
                </a> 
                <a class="button tiny" href="<?= $this->actionUrl("deletePrevYears", ["userId" => $user['user_id'], "documentType" => $this->documentType, "u_code" => $code]) ?>">
                    Crea nuovo 
                </a>
            </div>
            <?php
        } else {
            redirect($this->actionUrl("newUserFull", ["u_code" => $code, "documentType" => $this->documentType]));
        }
    }

    public function deletePrevYears(){
        EM::execQuery("UPDATE user_year_document
            set status=:deleted
            where user_id=:user_id AND type=:type", [
                "deleted" => UserYearDocumentStatus::DELETED,
                "type" => $this->documentType,
                "user_id" => $this->userId
            ]);

        redirect($this->actionUrl("newUserFull", ["u_code" => $this->u_code]));
    }

    public function restoreDocument(){
        DocumentService::updateStatus($this->documentId, UserYearDocumentStatus::READY);
        redirect($this->actionUrl("_default"));
    }

    public function otherStructureUser($otherStructure, $user, $additionalRequestParameters = []){
        $backAction = $this->actionUrl("newUser", $additionalRequestParameters);
        
        $structure = StructureService::find($otherStructure['structure_id']);
        ?>
            <div class="message">
                <table>
                    <?php if (isset($user['section']) && $user['section'] == UserSection::MONDO_PAROLE) { ?>
                        <tr><td>Nome:</td><td><?= _t($user['name']) ?></td></tr>
                        <tr><td>Cognome:</td><td><?= _t($user['surname']) ?></td></tr>
                        <tr><td>Data nascita:</td><td><?= TextService::formatDate( _t($user['birth_date']), true ) ?></td></tr>
                    <?php } else { ?>
                        <tr><td>Codice:</td><td><?= _t( $user['code'] ) ?></td></tr>
                    <?php } ?>
                </table>
            </div>
            <div class="message">
                <p>Lo studente risulta assegnato ad un'altra scuola: <strong><?= $structure['name'] ?></strong>.</p>
                <p>E'possibile inviare una richiesta di trasferimento dei dati direttamente al responsabile della scuola premendo il pulsante sotto.</p>
                <p style="padding-bottom:20px;">Per completare la procedura sarà necessario attendere l'approvazione da parte del precedente referente.</p>
                <p style="padding-bottom:20px;"><strong>ATTENZIONE:</strong> prima di procedere all'invio della richiesta verificare con attenzione i dati.</p>
                <a class="button tiny" onclick="return confirm('Si è sicuri di voler procedere?');" href="<?= $this->actionUrl("transferRequest", 
                    array_merge(['userId' => $user['user_id']], $additionalRequestParameters)) ?>">
                    Invia richiesta
                </a>
                <a class="button tiny alert" href="<?= $backAction ?>">
                    <i class="<?= Icon::CANCELL ?>"></i>
                    Annulla
                </a>
            </div>
        <?php 
    }
    
    public function waitRequest($request, $user, $additionalParameters = null){
        $backAction = $this->actionUrl("_default", $additionalParameters);
        
        ?>
        <div class="message">
            <p>La richiesta di trasferimento per lo studente è in attesa di approvazione.</p>
            <table>
                <tr><td>Identificativo:</td><td><?= _t($request['user_transfer_request_id']) ?></td></tr>
                <tr><td>Data:</td><td><?= _t($request['insert_date']) ?></td></tr>
                <tr><td>Codice:</td><td><?= _t($user['code']) ?></td></tr>
                <tr><td>Nome:</td><td><?= _t($user['name']) ?></td></tr>
                <tr><td>Cognome:</td><td><?= _t($user['surname']) ?></td></tr>
                <tr><td>Stato:</td><td><?= _t($request['status']) ?></td></tr>
            </table>
            <div class="padding-top">
                <a class="button tiny alert" href="<?= $backAction ?>">
                    <i class="<?= Icon::CANCELL ?>"></i>
                    Annulla
                </a>
            </div>
        </div>
        <?php
    }
    
    public function importFromPrevYear(){
        $this->setGlobalSessionValue("goBack", $this->actionUrl());

        $this->header();
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row-100">
                    <div class="large-8 medium-7 small-7 columns">
                        <h3>Recupera da anni precedenti</h3>
                    </div>
                </div>
                
                <div class="mb-structure-recupera-documenti-table"></div>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function updateSchoolYearBtn(){
        $this->updateDocumentSchoolYear($this->documentId);
        redirect($this->actionUrl("_default"));
    }

    public function updateSchoolYear(){
        if ($_SERVER['REQUEST_METHOD'] != "POST"){
            http_response_code(400);
            die();
        }
        
        $input = file_get_contents('php://input');
        $request = json_decode($input);

        if (!property_exists($request, "documentIds")){
            http_response_code(400);
            die();
        }

        if ($request->documentIds == null || count($request->documentIds) == 0){
            http_response_code(400);
            die();
        }

        foreach ($request->documentIds as $documentId) {
            $this->updateDocumentSchoolYear($documentId);
        }

        http_response_code(200);
        die();
    }

    private function updateDocumentSchoolYear($documentId){
        $document = DocumentService::find($documentId);
        if ($document == null){
            return;
        }

        $userStructure = StructureService::hasUserStructureRole($document['user_id'], $this->structureId, UserRole::STUDENTE);
        if ($userStructure == NULL){
            return;
        }
        
        $userYear = UserYearService::findUserYearByUser($document["user_id"]);
        if ($userYear == null){
            return;
        }

        $fromClass = $userYear["classe"];
        $toClass = ClassGradeService::findNextClass($fromClass);
        $currentSchoolYear = SchoolYearService::findCurrentYear();

        EM::insertEntity("user_year_document_import", [
            "user_year_document_id" => $documentId,
            "from_class" => $fromClass,
            "to_class" => $toClass,
            "date" => "NOW()",
            "edit_user_id" => $this->currentUser["user_id"],
            "school_year_id" => $currentSchoolYear["school_year_id"]
        ]);

        EM::updateEntity("user_year", [
            "classe" => $toClass,
        ], [
            "user_year_id" => $userYear["user_year_id"]
        ]);

        UserUserService::remove($document["user_id"]);
        DocumentService::toCurrentYear($documentId);
        PeiService::updatePeiStatus($document["user_year_document_id"]);

        EM::execQuery("UPDATE user_year_document
            set status=:deleted
            where user_id=:user_id AND type=:type and user_year_document_id <> :user_year_document_id", [
                "deleted" => UserYearDocumentStatus::DELETED,
                "type" => $document["type"],
                "user_id" => $document["user_id"],
                "user_year_document_id" => $document["user_year_document_id"]
            ]);
    }
    
    public function transferRequest(){
        if (StringUtils::isBlank($this->userId)){
            redirect(UriService::accessDanyActionUrl());
        }

        $user = UserService::find($this->userId);
        if ($user == null){
            redirect(UriService::accessDanyActionUrl());
        }
        
        $this->header();
        ?>
        <div class="content active tab-nuovo-studente" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-9 medium-7 columns">
                    <h3>Richiesta di trasferimento</h3>
                </div>
            </div>
            <div class="sezioni row">
                <?php
                    try{
                        $request = UserTransferService::addRequest($this->userId, $this->structureId);
                        ?>
                        <p>Richiesta inserita:</p>
                        <table>
                            <tr><td>Identificativo:</td><td><?= _t($request['user_transfer_request_id']) ?></td></tr>
                            <tr><td>Data:</td><td><?= _t($request['insert_date']) ?></td></tr>
                            <tr><td>Codice:</td><td><?= _t($user['code']) ?></td></tr>
                            <tr><td>Nome:</td><td><?= _t($user['name']) ?></td></tr>
                            <tr><td>Cognome:</td><td><?= _t($user['surname']) ?></td></tr>
                            <tr><td>Stato:</td><td><?= _t($request['status']) ?></td></tr>
                        </table>
                    <?php } catch (Exception $ex){ ?>
                        <p>Erriri nella richiesta</p>
                        <p><?= _t($ex->getMessage()) ?></p>
                    <?php }
                ?>
                <a href="<?= $this->actionUrl("_default") ?>" class="button tiny">Ok</a>
            </div>
        </div>
        <?php                          
        $this->footer();
    }
    
    public function createPopCornField($user){
        $popcornField = new FormTextField("Codice identificativo (POPCORN)", "u_code", FormFieldType::STRING, true);
        $popcornField->maxLength = 45;
        $popcornField->note = "Il codice è assegnabile solo in fase di creazione dell'utente. Se il codice risulta sbagliato sarà necessario procedere ad una nuova creazione.";
        $popcornField->checkFunction = function($field, $value) use ($user){
            $dbUser = UserService::findByCode($value);
            if (strlen($value) < 3 || strlen($value) > 45){
                $field->addFieldError("Codice non valido");
            }   

            if ($dbUser != null && $dbUser['user_id'] != $user['user_id']){
                $field->addFieldError("Sono già presenti utenti identificati da questo codice. Verificare di non aver già creato questo utente.");
            }
        };

        if ($user != null && StringUtils::isNotBlank($user['code'])){
            $popcornField->persist = false;
            $popcornField->required = false;
            $popcornField->readOnly = true;
        }

        return $popcornField;
    }
}