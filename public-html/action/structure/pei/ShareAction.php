<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ShareAction extends StructureUserDocumentAction{
    
    public $toUserId;
    
    public function _default(){
        $form = new Form($this->actionUrl("_default", ["documentId" => $this->documentId]));
        $form->cancellAction = UriService::buildPageUrl("/structure/UserAction", "_default", ["documentId" => $this->documentId]);
        $form->submitButtonLabel = "Aggiungi";

        $users = StructureService::findUserStructureWithRoles($this->structureId, [
            UserRole::INSEGNANTE_PEI_PDP,
            UserRole::COLLABORATORE_INTEGRAZIONE,
        ]);

        $userShares = UserUserService::findForUserId($this->user['user_id']);

        $userValues = [];
        foreach ($users as $userStructure) {
            foreach ($userShares as $userShare) {
                if($userShare['to_user_id'] == $userStructure['user_id']){
                    break;
                }
            }
            $user = UserService::find($userStructure['user_id']);
            $userValues[$user["user_id"]] = $user['name'] . ' ' . $user['surname'];
        }

        $userField = new FormSelect2Field("Utente", "user_id", FormFieldType::STRING, $userValues, true);
        $userField->checkFunction = function($field) {
            $userId = $field->getValueForDb();
            $user = UserService::find($userId);
            if ($user == null){
                $field->addFieldError("Utente non presente nel sistema. Se desiderate aggiungere un utente contattare la segreteria o il dirigente.");
                return;
            }
            
            $userStructureAdmin = StructureService::hasUserStructureRoleAnyOf($user['user_id'], $this->structureId, [
                UserRole::DIRIGENTE,
                UserRole::REFERENTE_BES,
                UserRole::AMMINISTRATIVI
            ]);
            if ($userStructureAdmin != null){
                $field->addFieldError("L'utente ha un ruolo amministrativo nella scuola quindi ha accesso ai documenti senza bisogno di effettuare una condivisione.");
                return;
            }
            
            //Francesca.Pappalardo@scuola.alto-adige.it
            $userStructure = StructureService::hasUserStructureRoleAnyOf($user['user_id'], $this->structureId, [
                UserRole::INSEGNANTE_PEI_PDP,
                UserRole::COLLABORATORE_INTEGRAZIONE
            ]);
            if (!$userStructure){
                $field->addFieldError("Utente non presente nella scuola. Se desiderate aggiungere un utente contattare la segreteria o il dirigente.");
                return;
            }
            
            $userUser = UserUserService::find($this->user['user_id'], $user['user_id']);
            if ($userUser != null){
                $field->addFieldError("L'utente ha già diritto di accesso ai dati.");
                return;
            }
        };
        $form->addField($userField);
        
        if ($form->isSubmit() && $form->checkFields()){
            $userId = $userField->getValueForDb();
            
            UserUserService::persist([
                "user_id" => $this->user['user_id'],
                "to_user_id" => $userId
            ]);
            
            redirect($this->actionUrl("_default", ["documentId" => $this->documentId]));
        }
        
        $table = new Table();
        $table->entityName = "user_user uu";
        $table->addJoin("LEFT JOIN user u ON u.user_id = uu.to_user_id");
        $table->addWhere(sprintf("uu.user_id=%d", $this->user['user_id']));
        $table->addColumn(new TableColumn("email", "e-mail"));
        $table->addColumn(new TableColumn("name", "Nome"));
        $table->addColumn(new TableColumn("surname", "Cognome"));
        $table->addColumn(new TableColumn("to_user_id", "", function($toUserId){
            $out = Formatter::buildTool("", $this->actionUrl("remove", [
                "toUserId" => $toUserId,
                "documentId" => $this->documentId
            ]), Icon::TRASH, "Revocare la condivisione?");
            return $out;
        }));
        
        $this->header();
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], UriService::buildPageUrl("/structure/UserAction", null, [
            "documentId" => $this->documentId
        ])));
        $this->bradcrumbs->add(new Bradcrumb("Condividi"));
        ?>
        <div class="content active tab-nuovo-studente row" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="medium-12 columns">
                    <h3>Condividi documento</h3>
                </div>
            </div>
            <div class="sezioni">
                <p>
                    È possibile condividere l'accesso al documento con altri utenti del sistema.
                    Tutti gli utenti selezionati potranno modificare i contenuti.<br/>
                    <strong>Nota: </strong>non è necessario condividere i documenti con Referenti BES, Dirigenti di istituto e Amministrativi. Questi ruoli consentono la visione di tutti i documenti.
                </p>
            </div>
        
            <?php $table->draw(); ?>
            
            <h3>Aggiungi nuovo</h3>
            
            <?php $form->draw(); ?>
        </div>
        <?php
        $this->footer();
    }
    
    public function remove(){
        if (StringUtils::isBlank($this->toUserId)){
            redirect($this->actionUrl("_default"));    
        }
        
        EM::deleteEntity("user_user", [
            "user_id" => $this->user["user_id"],
            "to_user_id" => $this->toUserId
        ]);
        
        redirect($this->actionUrl("_default", ["documentId" => $this->documentId]));
    }
}
