<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once(__DIR__ .'/PeiBaseAction.php');

/**
 * Description of PattoAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PattoAction extends PeiBaseAction{
    
    public $is_submit;
    public $patto_type;
    public $patto_note;
    public $patto_uscute_didattiche;
    public $patto_attivita;
    public $comportamento;
    
    public function _default(){
        $attivitaField = new FormMultiSelectField("", "patto_attivita");
        $attivitaField->inputType = "checkbox";
        $attivitaField->options = Constants::$pattoAttivita;
        $attivitaField->optionsWithTextArea = ["o_1", "o_2", "o_altro"];

        if ($this->is_submit == "1"){
            if ($this->patto_type != null && $this->patto_type != '1' && $this->patto_type != '2'){
                die('valore patto impostato non valido');
            }

            if ($this->comportamento != null && $this->comportamento != 'classe' && $this->comportamento != 'obiettivi'){
                die('valore comportamento impostato non valido');
            }

            if ($this->patto_note != null && strlen($this->patto_note) > 2000){
                die('valore note eccede lunghezza massima');
            }

            EM::updateEntity("user_year", [
                "patto_type" => $this->patto_type,
                "patto_note" => EncryptService::encrypt( $this->patto_note ),
                "patto_uscute_didattiche" => EncryptService::encrypt( $this->patto_uscute_didattiche ),
                "patto_attivita" => $attivitaField->getValueForDb(),
                "comportamento" => $this->comportamento
            ], ["user_year_id" => $this->userYear['user_year_id']]);
            $this->updateUserYear();

            DocumentService::updateLastEdit($this->document["user_year_document_id"]);

            EM::insertEntity("journal", [
                "user_id" => $this->currentUser["user_id"],
                "insert_date" => "NOW()",
                "entity" => JournalEntity::PEI,
                "entity_id" => $this->document["user_year_document_id"],
                "section" => JournalSection::PEI_PATTO,
                "action" => JournalAction::UPDATE
            ]);

            redirect( UriService::buildPageUrl("/structure/UserAction", "_default", ["documentId" => $this->documentId]));
        }
        
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], UriService::buildPageUrl("/structure/UserAction", null, [
            "documentId" => $this->documentId
        ])));
        $this->bradcrumbs->add(new Bradcrumb("Patto"));
        $this->header();
        
        ?>
<style type="text/css">
    .row.patto{
        margin: 5px 5px 5px 5px;
        padding: 5px;
    }
    .selected{
        border: 1px solid green;
        box-shadow: 2px 2px 3px 0px #cccccc;
    }
</style>
<script type="text/javascript">
    <?php if ($this->documentType == DocumentType::PDP || $this->documentType == DocumentType::PDP_LINGUA){ ?>
        $(function(){
            $('#patto_type').change(update);
            update();
        });
        function update(){
            $('.patto').removeClass('selected');

            var checked = $('#patto_type')[0].checked;
            if (checked){
                $('.patto').addClass('selected');
            }
        }
    <?php } else { ?>
        var type = <?= $this->userYear['patto_type'] == null ? 0 : $this->userYear['patto_type'] ?>;
        $(function(){
            $('.patto_' + type).addClass('selected');
            $('input', '.patto_' + type).prop('checked', true);
            $('input', '.patto').click(selectType);
        });
        function selectType(){
            type = $(this).val();
            $('.patto').removeClass('selected');
            $('.patto_' + type).addClass('selected');
        }
    <?php } ?>
</script>

        <div class="content active tab-nuovo-studente row" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="medium-12 columns">
                    <h3>Patto con la famiglia</h3>
                </div>
            </div>
            <div class="sezioni padding">
                <form method="post" action="<?= $this->actionUrl("_default", ["documentId" => $this->documentId]) ?>">
                    <input type="hidden" id="is_submit" name="is_submit" value="1"/>
                    
                    <?php if ($this->documentType == DocumentType::PEI_TIROCINIO) { ?>
                        <strong>Patto con la famiglia/con gli esercenti la responsabilità genitoriale</strong><br/><br/>

                        <p>
                            A seguito di accurate osservazioni e valutazioni delle competenze dell’alunna, si definiscono obiettivi e interventi e assieme alla famiglia si prendono i seguenti accordi: 
                        </p>

                        <p>
                            Alla fine del percorso formativo viene rilasciata una certificazione delle competenze acquisite.
                        </p>
                    <?php } else if ($this->documentType == DocumentType::PDP || $this->documentType == DocumentType::PDP_LINGUA){ 
                        $checked = '';
                        if ($this->userYear['patto_type']){
                            $checked = 'checked';
                        }
                        ?>
                        <div class="row patto patto_1">
                            <div class="large-1 columns">
                                <input type="checkbox" id="patto_type" name="patto_type" value="1" <?= $checked ?>></input>
                            </div>
                            <div class="large-11 columns">
                                <p>
                                    A seguito dell'osservazione e delle valutazioni delle competenze dell'alunna/o in accordo con i genitori/esercenti la responsabilità genitoriale ovvero con l'alunna/o maggiorenne, il Consiglio di classe si impegna ad adottare, se necessario, misure dispensative e strumenti compensativi, interventi pedagogico-didattici e opportune forme di valutazione - nelle diverse discipline così come viene indicato in questo documento - allo scopo di favorire il successo formativo dell'alunna/o, secondo quanto previsto dalla normativa vigente.
                                    Il Consiglio di classe si impegna ad adottare il presente PDP; il/i genitore/i, esercente/i la responsabilità genitoriale, l'alunno maggiorenne condividono/condivide queste modalità dell'azione pedagogico-didattica, supportano e si impegnano nel lavoro a scuola e a casa relativamente a questa linea di azione
                                </p>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="row patto patto_1">
                            <div class="large-1 columns">
                                <input type="radio" name="patto_type" value="1"></input>
                            </div>
                            <div class="large-11 columns">
                                <p>
                                    Dopo accurate osservazioni da parte del consiglio di classe (si veda il verbale della seduta per la stesura del piano educativo individualizzato) e in accordo con gli esercenti la responsabilità genitoriale ovvero con l’alunno/l’alunna maggiorenne, si stabilisce in data odierna che il suddetto alunno/la suddetta alunna  seguirà gli 
                                    <strong>obiettivi della classe fino ad una eventuale revoca</strong> del presente accordo. *
                                </p>
                            </div>
                        </div>
                        <div class="row patto patto_2">
                            <div class="large-1 columns">
                                <input type="radio" name="patto_type" value="2"></input>
                            </div>
                            <div class="large-11 columns"">
                                <p>
                                    Dopo accurate osservazioni da parte del consiglio di classe (si veda il verbale della seduta per la stesura del piano educativo individualizzato) e in accordo con gli esercenti la responsabilità genitoriale ovvero con l’alunno/l’alunna maggiorenne, si stabilisce in data odierna che il suddetto alunno/la suddetta alunna  seguirà in una o più discipline un programma con 
                                    <strong>obiettivi differenziati fino ad una eventuale revoca</strong> del presente accordo.  *
                                </p>
                            </div>
                        </div>

                        <?php if (strpos($this->userYear['classe'], 'I') === false ){ ?>
                            <div>
                                <p>
                                    *Nel primo ciclo d’istruzione conseguono un titolo di studio avente valore legale (diploma conclusivo del primo ciclo d’istruzione) sia gli alunni/le alunne che seguono gli obiettivi della classe, sia gli alunni/le alunne per i quali sono previsti obiettivi differenziati in una o più discipline.
                                </p>
                                <p>
                                    Nel secondo ciclo d’istruzione conseguono un titolo di studio avente valore legale (diploma conclusivo del secondo ciclo d’istruzione e/o diploma di qualifica professionale) solo quegli alunni/quelle alunne che seguono gli obiettivi della classe in tutte le discipline. Agli alunni/alle alunne che seguono obiettivi differenziati in una o più discipline verrà invece rilasciata una certificazione delle competenze acquisite.
                                </p>
                            </div>
                        <?php } ?>

                        <div class="my-5">
                            <h5>Comportamento</h5>
                            <div>
                                <input type="radio" name="comportamento" value="classe" <?= $this->userYear["comportamento"] == 'classe' ? 'checked' : '' ?>>
                                <label>Il comportamento è valutato in base agli stessi criteri adottati per la classe.</label>
                            </div>
                            <div>
                                <input type="radio" name="comportamento" value="obiettivi" <?= $this->userYear["comportamento"] == 'obiettivi' ? 'checked' : '' ?>>
                                <label>Il comportamento è valutato in base agli obiettivi definiti nel documento.</label>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($this->documentType == DocumentType::PDP_LINGUA){ ?>
                    <p class="mt-5">
                        Il presente documento ha carattere transitorio.
                    </p>
                    <?php } ?>

                    <?php if ($this->documentType == DocumentType::PEI) { ?>
                        <div class="my-5">
                            <h5>Uscite didattiche, visite guidate e viaggi di istruzione</h5>

                            <p>
                                Interventi previsti per consentire allo/a studente/essa di partecipare alle uscite didattiche visite guidate e viaggi di istruzione organizzati per la classe:
                            </p>

                            <textarea maxlength="2000" name="patto_uscute_didattiche" class=""><?= _t( EncryptService::decrypt($this->userYear['patto_uscute_didattiche']) ) ?></textarea>
                        </div>
                        
                        <div class="my-5">
                            <h5>Attività inclusive</h5>

                            <?php $attivitaField->draw($this->userYear); ?>
                        </div>
                    <?php } ?>
                
                    <div class="mt-5">
                        <h5>Note:</h5>
                        <textarea id="patto_note" name="patto_note" maxLength="2000"><?= _t( EncryptService::decrypt($this->userYear['patto_note']) ) ?></textarea>
                    </div>

                    <?php if ($this->documentType == DocumentType::PEI) { ?>
                        <div class="mt-5">
                            <p class="mt-10">
                                Le ore di sostegno sono state stabilite sulla base dei criteri elaborati dal GLI in base alla valutazione del fabbisogno e gli obiettivi definiti nel PEI dell'anno precedente se presente.
                            </p>
                        </div>
                    <?php } ?>
        
                    <?php
                    $journal = JournalService::findLastFor(JournalEntity::PEI, $this->document["user_year_document_id"], JournalSection::PEI_PATTO);
                    if ($journal != null){
                        ?>
                        <div class="row text-center" style="margin-top: 30px;">
                            <strong>Modificato il: </strong> <?= _t(TextService::formatDate( $journal["insert_date"] )) ?>
                            <?php if (UserRoleService::canCurrentUserDo(UserPermission::LAST_EDIT_USER_VIEW)) { ?>
                                <strong>da:</strong> <?= _t($journal["ue_name"]) ?> <?= _t($journal["ue_surname"]) ?>
                            <?php } ?>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="row-100 campiObbligatori">
                        <div class="large-3 medium-3 columns">
                            <p>*campi obbligatori</p>
                        </div>
                        <div class="large-5 medium-5 end columns">
                            <a href="javascript:submit('form');" class="button radius salva"><i class="far fa-save"></i>Salva</a>
                            <a href="<?= UriService::buildPageUrl("/structure/UserAction", "_default", ["documentId" => $this->documentId]) ?>" class="button radius alert"><i class="fa fa-undo"></i>Annulla</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php
        
        $this->footer();
    }
    
}