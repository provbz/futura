<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PeiViewAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
require_once 'PeiAction.php';
require_once './template/report/PeiODT.php';
require_once(__DIR__ .'/PeiBaseAction.php');

class PeiViewAction extends PeiAction {
    
    public $printDate;
    public $download;
    public $typefile;
    public $content;
    
    protected $fullName = "PEI completo";
    protected $documentTitle = "PEI";
    protected $documentSubtitle = "Piano Educativo Individualizzato";
    
    public function _default(){
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'],
                UriService::buildPageUrl("/structure/UserAction", "", ["documentId" => $this->documentId] )));
        $this->bradcrumbs->add(new Bradcrumb("Visualizza"));
        
        $this->addScriptFile("jquery.cookie.js");
        $this->header();
        
        setcookie("pdf_loading", 0);
        
        if (StringUtils::isBlank($this->printDate)){
            $this->printDate = TextService::formatDate( DateUtils::getDate( $this->pei['insert_date'] ) );
        }
        ?>
        <script type="text/javascript">
            
            function openPdf(){
                $('form')[0].submit();
            }
        </script>
        
        <div class="tabs-content">
            <div class="content active" id="panelTabStudenti">
                <div class="section-title row">
                    <?php $this->bradcrumbs->draw(); ?>
                    <div class="large-6 medium-4 columns">
                        <h3>Visualizza e scarica</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="small-12 columns">
                        <form action="<?= $this->actionUrl('report') ?>">
                            <input type="hidden" name="peiId" value="<?= _t($this->peiId) ?>"/>
                            <input type="hidden" name="code" value="<?= _t($this->code) ?>"/>
                            
                            <div class="row-100 form-row " id="campo_form_um_birth_place" style="">
                                <div class="large-3 medium-3 columns">
                                    <label class="inline codice  bold" for="um_birth_place">
                                        Note
                                    </label>
                                </div>
                                <div class="large-9 medium-9 columns">
                                    <div class="">
                                        <textarea name="note" style="width:100%; height: 100px;"><?= _t($this->pei['note']) ?></textarea>
                                    </div>            
                                    <div class="form_field_note" id="um_birth_place_note"></div>
                                </div>
                            </div>

                            <div class="row-100 form-row " id="campo_form_um_birth_place" style="">
                                <div class="large-3 medium-3 columns">
                                    <label class="inline codice  bold" for="um_birth_place">
                                        Autore
                                    </label>
                                </div>
                                <div class="large-9 medium-9 columns">
                                    <div class="">
                                        <input type="text" name="author" value="<?= _t(StringUtils::isBlank( $this->pei['author']) ? $this->currentUser['name']. ' '. $this->currentUser['surname'] : $this->pei['author']) ?>"/>
                                    </div>            
                                    <div class="form_field_note" id="um_birth_place_note"></div>
                                </div>
                            </div>
                            
                            <div class="row-100 form-row " id="campo_form_um_birth_place" style="">
                                <div class="large-3 medium-3 columns">
                                    <label class="inline codice  bold" for="um_birth_place">
                                        Elementi da stampare               
                                    </label>
                                </div>
                                <div class="large-9 medium-9 columns">
                                    <div class="">
                                        <input name="content[]" type="checkbox" checked value="<?= PeiODT::COPERTINA ?>"/> Copertina<br/>
                                        <input name="content[]" type="checkbox" checked value="<?= PeiODT::ANAGRAFICA ?>"/> Anagrafica<br/>

                                        <?php if ($this->pei["type"] == DocumentType::PEI_TIROCINIO){ ?>
                                            <input name="content[]" type="checkbox" checked value="<?= PeiODT::PROPOSTA_PROGETTO ?>"/> Proposta di progetto<br/>
                                            <input name="content[]" type="checkbox" checked value="<?= PeiODT::AZIENDE ?>"/> Aziende<br/>
                                        <?php } ?>

                                        <?php if ($this->pei["type"] == DocumentType::PDP_LINGUA){ ?>
                                            <input name="content[]" type="checkbox" checked value="<?= PeiODT::SITUAZIONE_PARTENZA ?>"/> Situazione di partenza<br/>
                                            <input name="content[]" type="checkbox" checked value="<?= PeiODT::COMPETENZE_LINGUISTICHE ?>"/> Competenze linguistiche<br/>
                                        <?php } ?>

                                        <?php if ($this->pei["type"] == DocumentType::PEI){ ?>
                                            <input name="content[]" type="checkbox" checked value="<?= PeiODT::PCTO ?>"/> PCTO<br/>
                                        <?php } ?>
                                        
                                        <input name="content[]" type="checkbox" checked value="<?= PeiODT::OSSERVAZIONI ?>"/> Osservazioni, Obiettivi, Attività<br/>
                                        <div style="padding-left: 15px">
                                            <input name="content[]" type="checkbox" checked value="<?= PeiODT::OSSERVAZIONI_DESCRIZIONE ?>"/> Sezione descrittiva<br/>
                                        </div>
                                        <input name="content[]" type="checkbox" checked value="<?= PeiODT::CALENDARIO ?>"/> Calendario<br/>
                                        <input name="content[]" type="checkbox" checked value="<?= PeiODT::PATTO ?>"/> Patto con la famiglia<br/>
                                    </div>            
                                    <div class="form_field_note" id="um_birth_place_note"></div>
                                </div>
                            </div>

                            <div class="row-100 form-row " id="campo_form_um_birth_place" style="">
                                <div class="large-3 medium-3 columns">
                                    <label class="inline codice  bold" for="um_birth_place">
                                        Tipo file                       
                                    </label>
                                </div>
                                <div class="large-9 medium-9 columns">
                                    <div class="">
                                        <select name="typefile">
                                            <option value="<?= PeiODT::DOC ?>">doc</option>
                                            <option value="<?= PeiODT::ODT ?>">odt</option>
                                        </select>
                                    </div>            
                                    <div class="form_field_note" id="um_birth_place_note"></div>
                                </div>
                            </div>

                            <div class="row-100 form-row " id="campo_form_um_birth_place" style="">
                                <div class="large-3 medium-3 columns">
                                    <label class="inline codice  bold" for="um_birth_place">
                                        Data documento                          
                                    </label>
                                </div>
                                <div class="large-9 medium-9 columns">
                                    <div class="">
                                        <input class="datepicker" value="<?= _t($this->printDate) ?>" type="text" name="printDate" />
                                    </div>            
                                    <div class="form_field_note" id="um_birth_place_note"></div>
                                </div>
                            </div>
                            
                            <div class="row-100 campiObbligatori">
                                <div class="large-3 medium-3 columns">
                                    <p> </p>
                                </div>
                                <div class="large-5 medium-5 end columns">
                                    <p>
                                        <strong>Nota: </strong> scaricare e allegare l'
                                        <a href="/files/autorizzazione_invio_documentazione_3.docx">Autorizzazione all'invio dei della documentazione</a>.
                                    </p>
                                    <a onclick="return openPdf(1);" class="button radius salva"><i class="far fa-save margin-right"></i>Salva</a>
                                    <a href="<?= UriService::buildPageUrl("/structure/UserAction", "", ["documentId" => $this->documentId] ) ?>" class="button radius alert"><i class="fa fa-undo margin-right"></i>Annulla</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
        
        $this->footer();
    }
    
    public function report(){
        $report = new PeiODT();
        
        $params = [
            'author' => $this->author, 
            'note' => $this->note
        ];

        if (TextService::parseDate($this->printDate) != null){
            $params["insert_date"] = TextService::parseDate($this->printDate);
        }

        EM::updateEntity("user_year_document", 
                $params,
                sprintf('user_year_document_id=%d', $this->pei['user_year_document_id']));
        
        $this->document = DocumentService::find($this->pei['user_year_document_id']);
        $this->updatePei();
        
        $report->title = $this->documentTitle;
        $report->content = $this->content;
        $report->subTitle = $this->documentSubtitle;
        $report->download = $this->download;
        $report->pei = $this->pei;
        $report->user = $this->user;
        $report->userYear = $this->userYear;
        $report->userMetadata = $this->userMetadata;
        $report->printDate = $this->printDate;
        $report->typefile = $this->typefile;
        $report->code = $this->code;
        setcookie("pdf_loading", '1');
        
        $report->draw();
    }
}
