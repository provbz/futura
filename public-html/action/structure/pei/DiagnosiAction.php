<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once(__DIR__ .'/PeiBaseAction.php');

/**
 * Description of DiagnosiAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DiagnosiAction extends PeiBaseAction{
    
    public function _default(){
        redirect("/");
        
        $entity = [];
        ReflectionUtils::fillWithPrefix($this->userMetadata, "um_", $entity);
        
        $form = new Form($this->actionUrl("_default", ["documentId" => $this->documentId]));
        $form->entityName = "user";
        $form->entity = $entity;
        $form->cancellAction = UriService::buildPageUrl("/structure/UserAction", NULL, ["documentId" => $this->documentId]);
        
        $form->addField(new FormHtmlField('<div class="sezione">
                <div class="info">'));
        
        $documentType = DictionaryService::findGroupAsDictionary(Dictionary::DIAGNOSI_DOCUMENT_TYPE);
        $form->addField(new FormSelectField("Tipologia documento", "um_document_type", FormFieldType::STRING, $documentType, false));
        $form->addField(new FormDateField("Data", "um_document_data", false));
        $form->addField(new FormTextAreaField("Motivo della valutazione/Quesito", "um_valutation_motif", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        
        $diagnosy = new FormTextAreaField("Diagnosi", "um_diagnosis", FormFieldType::STRING);
        $form->addField($diagnosy);
        
        $form->addField(new FormTextField("Codici ICD-10", "um_icd10_codes", FormFieldType::STRING, false, null, ["maxLength" => 1000]));
        $icfField = new FormTextField("Codici ICF-CY", "um_icf_codes", FormFieldType::STRING, false, null, ["maxLength" => 1000]);
        $icfField->note = "È possibile inserire i codici completi di qualificatori es: xxx.2";
        $form->addField($icfField);
        
        $form->addField(new FormTextField("Codici DSM®", "um_dsm_codes", FormFieldType::STRING));
        
        $dsmVersions = DictionaryService::findGroupAsDictionary(Dictionary::DSM_VERSION);
        $form->addField(new FormSelectField("Versione DSM®", "um_dsm_ver", FormFieldType::STRING, $dsmVersions, false));
        
        $form->addField(new FormTextAreaField("Aggiornamenti/Integrazioni", "um_diagnosis_update", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        
        $socialOccupation = DictionaryService::findGroupAsDictionary(Dictionary::SOCIAL_OCCUPATION);
        $form->addField(new FormAssociatedText("Figure sanitarie di riferimento", "um_figure_riferimento", FormFieldType::STRING, $socialOccupation, false, null, ["maxLength" => 2000]));
        $form->addField(new FormTextField("Altre figure sanitarie di riferimento", "um_figure_riferimento_altre", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        $form->addField(new FormTextAreaField("Dati anamnestici rilevanti", "um_dati_anamnestici", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        $form->addField(new FormHtmlField('</div></div>'));
        
        
        $form->addField(new FormHtmlField('<div class="sezione">
                <div class="section-title margin row">
                    <div class="large-9 medium-7 columns">
                        <h3>Descrizione funzionale (punti di forza e di debolezza)</h3>
                    </div>
                </div>
                <div class="info">'));
        $cognitiveAbilityField = new FormTextAreaField("a) Abilità cognitive", "um_abilita_cognitive", FormFieldType::STRING, false, null, ["maxLength" => 2000]);
        $cognitiveAbilityField->note = "(ragionamento, attenzione, percezione, linguaggio…)";
        $form->addField($cognitiveAbilityField);
        $form->addField(new FormTextAreaField("b) Abilità scolastiche", "um_abilita_scolastiche", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        $form->addField(new FormTextAreaField("c) Ambito sensoriale", "um_ambito_sensoriale", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        $form->addField(new FormTextAreaField("d) Ambito motorio", "um_ambito_motorio", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        $form->addField(new FormTextAreaField("e) Aspetti medici rilevanti", "um_aspetti_medici_rilevanti", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        $form->addField(new FormTextAreaField("f) Ambito socio-emozionale", "um_ambito_socio_emozionale", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        
        $yesNoValues = DictionaryService::findGroupAsDictionary(Dictionary::YES_NO);
        $compromissioneFunzField = new FormRadioField("Grave compromissione del funzionamento sociale:", "um_compromissione_funzionamento_sociale", FormFieldType::STRING, $yesNoValues, false);
        $compromissioneFunzField->labelBold = false;
        $form->addField($compromissioneFunzField);
        
        $serviziValues = DictionaryService::findGroupAsDictionary(Dictionary::DIAGNOSI_SERVIZI);
        $serviziField = new FormCheckboxMultiField('Sono coinvolti nel processo di diagnosi e di valutazione i seguenti Servizi', 
                'um_servizi_coinvolti', FormFieldType::STRING, $serviziValues, false);
        $serviziField->labelBold = false;
        $form->addField($serviziField);
        
        $serviziAltroField = new FormTextField("Se altro specificare", "um_servizi_altro", FormFieldType::STRING, false, null, ["maxLength" => 2000]);
        $serviziAltroField->labelBold = false;
        $form->addField($serviziAltroField);
        
        $serviziResponsabileField = new FormTextField("La responsabilità clinica per il caso è in capo al seguente Servizio", "um_servizi_responsabile", FormFieldType::STRING, false, null, ["maxLength" => 2000]);
        $serviziResponsabileField->labelBold = false;
        $form->addField($serviziResponsabileField);
        
        $form->addField(new FormTextAreaField("g) Autonomia e partecipazione", "um_autonomia_partecipazione", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        
        $compromissioneLevel = DictionaryService::findGroupAsDictionary(Dictionary::COMPROMISSIONE_LEVEL);
        $compromissioneField = new FormSelectField("Grado di compromissione funzionale", "um_autonomia_partecipazione_compromissione", FormFieldType::STRING, $compromissioneLevel, false);
        $compromissioneField->labelBold = false;
        $form->addField($compromissioneField);
        
        $interventiValues = DictionaryService::findGroupAsDictionary(Dictionary::TIPO_INTERVENTO);
        $interventiField = new FormCheckboxMultiField('Interventi previsti (solo per Diagnosi Funzionali e Referti Clinici)', 
                'um_interventi_field', FormFieldType::STRING, $interventiValues, false);
        $form->addField($interventiField);
        $interventiAltroField = new FormTextField("Altri interventi", "um_interventi_field_other", FormFieldType::STRING, false, null, ["maxLength" => 2000]);
        $interventiAltroField->labelBold = false;
        $form->addField($interventiAltroField);
        
        $form->addField(new FormTextField("Visita di controllo consigliata (a scopi clinici)", "um_visita_controllo", FormFieldType::STRING, false, null, ["maxLength" => 2000]));
        
        $operatoreType = DictionaryService::findGroupAsDictionary(Dictionary::DIAGNOSI_OPERATORE_TYPE);
        $form->addField(new FormSelectField("Operatore", "um_operatore_1_type", FormFieldType::STRING, $operatoreType, false));
        $form->addField(new FormTextField("Nome e cognome", "um_operatore_1", FormFieldType::STRING, false, null, ["maxLength" => 200]));
        
        $form->addField(new FormSelectField("Eventuale secondo operatore", "um_operatore_2_type", FormFieldType::STRING, $operatoreType, false));
        $form->addField(new FormTextField("Nome e cognome", "um_operatore_2", FormFieldType::STRING, false, null, ["maxLength" => 200]));
        
        $journal = JournalService::findLastFor(JournalEntity::PEI, $this->document["user_year_document_id"], JournalSection::PEI_DIAGNOSI);
        if ($journal != null){

            ob_start();
            ?>
            <div class="row text-center" style="margin-top: 30px;">
                <strong>Modificato il: </strong> <?= _t(TextService::formatDate( $journal["insert_date"] )) ?>
                <?php if (UserRoleService::canCurrentUserDo(UserPermission::LAST_EDIT_USER_VIEW)) { ?>
                    <strong>da:</strong> <?= _t($journal["ue_name"]) ?> <?= _t($journal["ue_surname"]) ?>
                <?php } ?>
            </div>
            <?php
            $jdata = ob_get_clean();

            $form->addField(new FormHtmlField($jdata));
        }

        $form->addField(new FormHtmlField('</div></div>'));
        
        if ($form->isSubmit() && $form->checkFields()){
            $data = [];
            foreach($form->fields as $key => $value){
                $data[$value->name] = $value->getValueForDb();
            }

            $metadata = ReflectionUtils::getPropertyWithPrefix($data, "um_");
            MetadataService::removeMetadataWithKey(MetadataEntity::USER, $this->user['user_id'], $metadata);
            MetadataService::persist(MetadataEntity::USER, $this->user['user_id'], $metadata);
            $this->updateMetadata();

            DocumentService::updateLastEdit($this->document["user_year_document_id"]);

            EM::insertEntity("journal", [
                "user_id" => $this->currentUser["user_id"],
                "insert_date" => "NOW()",
                "entity" => JournalEntity::PEI,
                "entity_id" => $this->document["user_year_document_id"],
                "section" => JournalSection::PEI_DIAGNOSI,
                "action" => JournalAction::UPDATE
            ]);
            
            redirect( UriService::buildPageUrl("/structure/UserAction", "_default", ["documentId" => $this->documentId]));
        }

        $this->addStyleFile("jquery.tagit.css");
        $this->addScriptFile("tag-it.min.js");
        $this->header();
        
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], UriService::buildPageUrl("/structure/UserAction", null, [
            "documentId" => $this->documentId
        ])));
        $this->bradcrumbs->add(new Bradcrumb("Diagnosi"));
        
        ?>
        <script type="text/javascript">
        $(function(){
            $("#um_icf_codes").tagit({
                autocomplete: {
                    showAutocompleteOnFocus: true,
                    source: '<?= UriService::buildPageUrl("/icf/IcfAction", "treeSearch") ?>'
                }
            });
            $("#um_icf_codes").hide();

            $("#um_icd10_codes").hide();
            $("#um_icd10_codes").tagit();
            
            $('#um_document_type').on('change', documentTypeChange);
            documentTypeChange();
        });
        
        function documentTypeChange(){
            var type = $('#um_document_type').val();
            if (type == 'diagnosi_funzionale' || type == 'referto_clinico'){
                $('#campo_form_um_interventi_field').show();
                $('#campo_form_um_interventi_field_other').show();
            } else {
                $('#campo_form_um_interventi_field :input').each(function(){
                    $(this).prop('checked', false);
                });
                $('#campo_form_um_interventi_field').hide();
                
                $('#campo_form_um_interventi_field_other').hide();
                $('#um_interventi_field_other').val('');
            }
        }
        </script>
        <div class="content active tab-nuovo-studente row" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="medium-12 columns">
                    <h3>DIAGNOSI-CERTIFICAZIONE PER LA SCUOLA<br/>(Informazioni fornite dalla Sanità)</h3>
                </div>
            </div>
            <div class="sezioni">
                <?= $form->draw(); ?>
            </div>
        </div>
        <?php
        
        $this->footer();
    }
    
}

class FormAssociatedText extends FormField{
    
    public $values;
    
    public function  __construct($label, $name, $type, $values, $required = false, $defaultValue = NULL, $parameters = NULL) {
        $this->values = $values;
        parent::__construct($label, $name, $type, $required, $defaultValue, $parameters);
    }

    public function draw($entity = NULL) {
        $value = $this->getValue($entity);
        $selectedCheckbox = [];
        $text = [];
        if (is_array($value)) {
            $selectedCheckbox = $value;
            $text = getRequestValue($this->name . '_text');
        } else if (StringUtils::isNotBlank( $value )){
            $data = json_decode($value);
            if (is_object($data)){
                $selectedCheckbox = $data->checkbox;
                $text =  $data->text;
            }
        }
        
        $out = '<div class="check_boxes large-8 medium-7 end columns" id='.$this->name.'>';
        $i = 0;
        foreach ($this->values as $key => $value){
            $selected = "";
            $nameStyle = "display:none;";
            if (is_array($selectedCheckbox) && in_array($key, $selectedCheckbox) !== FALSE){
                $selected = "checked='checked'";
                $nameStyle = "";
            }
            $out .= '<div class="check_field large-12 medium-12 end columns">';
                $out .= '<div class="columns large-12" style="padding-left:0px;">';
                $out .= '<input type="checkbox" name="'.$this->name.'[]" value="'.$key.'" '.$selected.'><label>'.$value.'</label>';
                $out .= '</div>';
                $out .= '<div class="columns large-12" style="'.$nameStyle.'">';
                $out .= '<input type="text" name="'.$this->name.'_text[]" value="'. (count($text) > $i ? $text[$i] : '' ) . '" />';
                $out .= '</div>';
            $out .= '</div>';
            $i++;
        }
        ob_start();
        ?>
            <script type="text/javascript">
                $('#<?= $this->name ?> input:checkbox').change(function(){
                    var container = $(this).parent().parent();
                    var tf = $('div', container)[1];
                    if($(this).is(':checked')){
                        $(tf).show('slow');
                    } else {
                        $(tf).hide('slow');
                        $(tf).children('input').val('');
                    }
                });
            </script>
        <?php
        $out .= ob_get_clean();
        $out .= '</div>';
        echo $out;
    }
    
    public function getValueForDb() {
        $value = parent::getValueForDb();
        $text = getRequestValue($this->name . "_text");
        $obj = [
            "checkbox" => $value,
            'text' => $text
        ];
        return json_encode($obj);
    }
}

