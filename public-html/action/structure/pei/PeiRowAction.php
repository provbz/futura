<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once(__DIR__ .'/PeiBaseAction.php');

/**
 * Description of PeiRowAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
require_once 'PeiAction.php';
class PeiRowAction extends PeiAction{
    
    public $peiRowId;
    protected $peiRow;
    public $rowTargetId;
    public $level;
    public $term;
    public $status;
    public $node;
    
    public function _prepare(){
        parent::_prepare();
        
        if (StringUtils::isNotBlank($this->peiRowId)){
            $_SESSION['selectedPeiRowId'] = $this->peiRowId;
        } else {
            $this->peiRowId = $_SESSION['selectedPeiRowId'];
        }
        
        $this->peiRow = UserPeiService::findPeiRow($this->peiRowId);
        if ($this->peiRow == NULL || $this->peiRow['pei_id'] != $this->pei['user_year_document_id']){
            redirect(UriService::accessDanyActionUrl());
        }
        
        $this->node = PeiService::findNodeByUri( $this->peiRow['uri'], $this->documentType );
    }
    
    protected function buildBradcrumb(){
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'],
                UriService::buildPageUrl("/structure/UserAction", null, ["documentId" => $this->peiRow["pei_id"]])));
        
        if ($this->pei['type'] == DocumentType::PEI){
            $this->bradcrumbs->add(new Bradcrumb("Elabora", UriService::buildPageUrl("/structure/pei/PeiAction", "details", ["documentId" => $this->peiRow["pei_id"]])));
        } else {
            $this->bradcrumbs->add(new Bradcrumb("Analisi", UriService::buildPageUrl("/structure/pei/PeiAction", "details", ["documentId" => $this->peiRow["pei_id"]])));
        }

        if (is_array($this->node)){
            $this->bradcrumbs->add(new Bradcrumb($this->node['label']));
        } else {
            $this->bradcrumbs->add(new Bradcrumb($this->peiRow['title']));
        }
    }
    
    public function _default(){
        $this->buildBradcrumb();
        
        $this->header();
        ?>
        <script type="text/javascript">
            $(function(){
                $('#pdp_strategie_compensative_sc_19').on('change', updateStrategieCompensazione);
                updateStrategieCompensazione();

                $('#pdp_ausili_strumenti_as_9').on('change', updateAnalisiStrumenti);
                updateAnalisiStrumenti();

                $('#pdp_misure_dispensative_md_9').on('change', updateMisureCompensative);
                updateMisureCompensative();

                $('#pdp_svolgimento_prove_msp_14').on('change', updateModalitaScolgimento);
                updateModalitaScolgimento();
            });

            function removeRowTarget(){
                if (confirm("Rimuovere l'elemento?")){
                    closeDialog('#dialog-new_activity');
                    $('#row_target_' + selectedRowTargetId).hide('slow');
                    $.post('<?= $this->actionUrl("deleteRowTarget") ?>',
                        {'peiRowTargetId': selectedRowTargetId},
                        function (){
                        }
                    );
                }
            }

            function updateModalitaScolgimento(){
                var val = $('#pdp_svolgimento_prove_msp_14').is(':checked');
                
                $('#campo_form_pdp_svolgimento_prove_altro').hide();
                if (val){
                    $('#campo_form_pdp_svolgimento_prove_altro').show();
                }
            }

            function updateMisureCompensative(){
                var val = $('#pdp_misure_dispensative_md_9').is(':checked');
                
                $('#campo_form_pdp_misure_dispensative_altro').hide();
                if (val){
                    $('#campo_form_pdp_misure_dispensative_altro').show();
                }
            }

            function updateStrategieCompensazione(){
                var val = $('#pdp_strategie_compensative_sc_19').is(':checked');
                
                $('#campo_form_pdp_strategie_compensative_altro').hide();
                if (val){
                    $('#campo_form_pdp_strategie_compensative_altro').show();
                }
            }

            function updateAnalisiStrumenti(){
                var val = $('#pdp_ausili_strumenti_as_9').is(':checked');
                
                $('#campo_form_pdp_ausili_strumenti_altro').hide();
                if (val){
                    $('#campo_form_pdp_ausili_strumenti_altro').show();
                }
            }
        </script>
        <div class="content active tab-studente personalizza-pei row" id="panelTabStudenti">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-8 medium-6 columns">
                    <h3>Dettagli processo</h3>
                </div>
            </div>
            
            <?php 
            $this->drawNodeDetails($this->node);
            ?>

            <?php if ($this->pei['type'] == DocumentType::PEI || $this->pei['type'] == DocumentType::PEI_TIROCINIO) { ?>
                <div class="mt-10">
                    <h4>Osservazioni obiettivi e attività</h4>
                    <?= $this->drawActivities(); ?>
                </div>
            <?php } else if ($this->pei['type'] == DocumentType::PDP_LINGUA) { ?>
                <h4>Dati</h4>

                <div class="alert alert-danger my-5">
                    Si ricorda che ciò che viene spuntato o scritto nei campi editabili deve poi trovare un'effettiva applicazione nella pratica didattica.
                </div>

                <?= $this->drawPdpLinguaForm(); ?>
            <?php } else { ?>
                <h4>Dati</h4>

                <div class="alert alert-danger my-5">
                    Si ricorda che ciò che viene spuntato o scritto nei campi editabili deve poi trovare un'effettiva applicazione nella pratica didattica.
                </div>
                
                <?= $this->drawPDPFormJS(); ?>
                <?= $this->drawPdpForm(); ?>
            <?php } ?>
        </div>
        <?php
        $this->footer();
    }

    private function drawPdpLinguaForm(){
        $stmt = PeiService::findPeiRowTargets($this->peiRowId);
        $target = $stmt->fetch();
        $form = new Form($this->actionUrl("", ['pei_row_id' => $this->peiRowId, "documentId" => $this->document["user_year_document_id"]]));
        $form->entityName = "pei_row_target";
        $form->cancellAction = UriService::buildPageUrl("/structure/pei/PeiAction", "details", ["documentId" => $this->document["user_year_document_id"]] );
        
        if ($target != null){
            $form->entity = $target;
        }

        $form->addField( new FormTextareaField("Osservazioni", "observation", FormFieldType::STRING, false) );
        
        $tipoObiettivi = new FormMultiSelectField('Tipologia di obiettivi', 'pdp_lin_tipologia_obiettivi');
        $tipoObiettivi->options = DictionaryService::findGroup(Dictionary::PDP_LIN_TIPOLOGIA_OBIETTIVI);
        $tipoObiettivi->inputType = "radio";
        $form->addField($tipoObiettivi);

        $form->addField( new FormTextareaField("Note obiettivi", "pdp_lin_tipologia_obiettivi_note", FormFieldType::STRING, false) );

        $puntiForza = new FormMultiSelectField('Attività e interventi', 'pdp_lin_attivita_interventi');
        $puntiForza->options = DictionaryService::findGroup(Dictionary::PDP_LIN_ATTIVITA_INTERVENTI);
        $altroDictionary = $puntiForza->options[count($puntiForza->options) - 1];
        if ($altroDictionary != null){
            $puntiForza->optionsWithTextArea = [
                $altroDictionary["key"]
            ];
        }
        $form->addField($puntiForza);

        $puntiForza = new FormMultiSelectField('Strategie compensative / approcci metodologici', 'pdp_lin_strategie_compensative');
        $puntiForza->options = DictionaryService::findGroup(Dictionary::PDP_LIN_STRATEGIE_COMPENSATIVE);
        $form->addField($puntiForza);

        $puntiForza = new FormMultiSelectField('Strumenti compensativi', 'pdp_lin_strumenti_compensativi');
        $puntiForza->options = DictionaryService::findGroup(Dictionary::PDP_LIN_STRUMENTI_COMPENSATIVI);
        $altroDictionary = $puntiForza->options[count($puntiForza->options) - 1];
        if ($altroDictionary != null){
            $puntiForza->optionsWithTextArea = [
                $altroDictionary["key"]
            ];
        }
        $form->addField($puntiForza);

        $puntiForza = new FormMultiSelectField('Misure dispensative', 'pdp_lin_misure_dispensative');
        $puntiForza->options = DictionaryService::findGroup(Dictionary::PDP_LIN_MISURE_DISPENSATIVE);
        $altroDictionary = $puntiForza->options[count($puntiForza->options) - 1];
        if ($altroDictionary != null){
            $puntiForza->optionsWithTextArea = [
                $altroDictionary["key"]
            ];
        }
        $form->addField($puntiForza);

        $puntiForza = new FormMultiSelectField('Modalità di svolgimento delle prove di verifica', 'pdp_lin_modalita_verifica');
        $puntiForza->options = DictionaryService::findGroup(Dictionary::PDP_LIN_MODALITA_VERIFICA);
        $altroDictionary = $puntiForza->options[count($puntiForza->options) - 1];
        if ($altroDictionary != null){
            $puntiForza->optionsWithTextArea = [
                $altroDictionary["key"]
            ];
        }
        $form->addField($puntiForza);
        
        $puntiForza = new FormMultiSelectField('Modalità di valutazione degli apprendimenti', 'pdp_lin_modalita_valutazione_app');
        $puntiForza->options = DictionaryService::findGroup(Dictionary::PDP_LIN_MODALITA_VALUTAZIONE_APP);
        $altroDictionary = $puntiForza->options[count($puntiForza->options) - 1];
        if ($altroDictionary != null){
            $puntiForza->optionsWithTextArea = [
                $altroDictionary["key"]
            ];
        }
        $form->addField($puntiForza);

        if ($form->isSubmit() && $form->checkFields()){
            if ($target == null){
                $form->persist("", [
                    'pei_row_id' => $this->peiRowId,
                    'insert_date' => 'NOW()',
                    'last_edit_date' => 'NOW()',
                    'last_edit_user_id' => $this->currentUser["user_id"]
                ]);
            } else {
                $form->persist("pei_row_target_id=".$target['pei_row_target_id'], [
                    'last_edit_date' => 'NOW()',
                    'last_edit_user_id' => $this->currentUser["user_id"]
                ]);
            }

            EM::updateEntity("pei_row", [
                'last_edit_date' => 'NOW()',
                'last_edit_user_id' => $this->currentUser["user_id"]
            ], [
                "pei_row_id" => $this->peiRowId
            ]);

            redirect( UriService::buildPageUrl("/structure/pei/PeiAction", "details", ["documentId" => $this->document["user_year_document_id"]]));
        } else {
            $form->draw();
        }
    }
    
    private function drawPDPFormJS(){
        ?>
        <script type="text/javascript">
            var otherFields = {
                '#pdp_strategie_compensative_19': '#campo_form_pdp_strategie_compensative_altro',
                '#pdp_ausili_strumenti_9': '#campo_form_pdp_ausili_strumenti_altro',
                '#pdp_misure_dispensative_9': '#campo_form_pdp_misure_dispensative_altro',
                '#pdp_svolgimento_prove_14': '#campo_form_pdp_svolgimento_prove_altro'
            };
            
            $(function (){
                for (var id in otherFields){
                    $(id).on('click', updateCheckboxAltro);
                    updateCheckboxAltro(null, $(id));
                }
            });
            
            function updateCheckboxAltro(ev, el){
                var input = el;
                if (ev != null){
                    input = ev.currentTarget;
                }
                
                var id = $(input).attr('id');
                var otherFieldId = otherFields['#' + id];
                
                if ($(input).is(':checked')){
                    $(otherFieldId).show('slow');
                } else {
                    $(otherFieldId).hide();
                }
            }
        </script>
        <?php
    }
    
    private function drawPdpForm(){
        $stmt = PeiService::findPeiRowTargets($this->peiRowId);
        $target = $stmt->fetch();
        $form = new Form($this->actionUrl("", ['peiRowId' => $this->peiRowId, "documentId" => $this->document["user_year_document_id"]]));
        $form->entityName = "pei_row_target";
        $form->cancellAction = UriService::buildPageUrl("/structure/pei/PeiAction", "details", ["documentId" => $this->document["user_year_document_id"]]);
        
        if ($target != null){
            $form->entity = $target;
        }
        
        $proField = new FormTextareaField("Punti di forza", "pro", FormFieldType::STRING);
        $proField->isEnchripted = true;
        $form->addField($proField);
        
        $consField = new FormTextareaField("Punti di debolezza/deficit", "cons", FormFieldType::STRING);
        $consField->isEnchripted = true;
        $form->addField($consField);
        
        $targetField = new FormTextareaField("Altre osservazioni", "observation", FormFieldType::STRING, false);
        $targetField->isEnchripted = true;
        $form->addField($targetField);
        
        $targetField = new FormTextareaField("Obiettivi", "target", FormFieldType::STRING, false);
        $targetField->isEnchripted = true;
        $form->addField($targetField);
        
        $status = DictionaryService::findGroupAsDictionary(Dictionary::PEI_ROW_TARGET_STATUS);
        $form->addField(new FormSelectField("Verifica Degli obiettivi", "status", FormFieldType::STRING, $status, false, PeiRowTargetStatus::IN_PROGRESS));
        
        $activityField = new FormTextareaField("Attività e interventi", "activity", FormFieldType::STRING, false);
        $activityField->isEnchripted = true;
        $form->addField($activityField);
        
        $values = DictionaryService::findGroupAsDictionary(Dictionary::STRATEGIE_COMPENSATIVE);
        if ($this->documentType == DocumentType::PDP){
            unset($values['sc_2']);
        }
        $strategieFields = new FormCheckboxMultiField('Strategie compensative/approcci metodologici', 
                'pdp_strategie_compensative', FormFieldType::STRING, $values, false);
        $strategieFields->isEnchripted = true;
        $form->addField($strategieFields);
        
        $activityField = new FormTextareaField("Se altro specificare", "pdp_strategie_compensative_altro", FormFieldType::STRING, false);
        $activityField->isEnchripted = true;
        $form->addField($activityField);
        
        $values = DictionaryService::findGroupAsDictionary(Dictionary::AUSILI_STRUMENTI_COMPENSATIVI);
        $strategieFields = new FormCheckboxMultiField('Ausili e strumenti compensativi', 
                'pdp_ausili_strumenti', FormFieldType::STRING, $values, false);
        $strategieFields->isEnchripted = true;
        $form->addField($strategieFields);
        
        $activityField = new FormTextareaField("Se altro specificare", "pdp_ausili_strumenti_altro", FormFieldType::STRING, false);
        $activityField->isEnchripted = true;
        $form->addField($activityField);
        
        $values = DictionaryService::findGroupAsDictionary(Dictionary::MISURE_DISPENSATIVE);
        $misureDispensativeField = new FormCheckboxMultiField('Misure dispensative', 
                'pdp_misure_dispensative', FormFieldType::STRING, $values, false);
        $misureDispensativeField->isEnchripted = true;
        $form->addField($misureDispensativeField);
        
        $activityField = new FormTextareaField("Se altro specificare", "pdp_misure_dispensative_altro", FormFieldType::STRING, false);
        $activityField->isEnchripted = true;
        $form->addField($activityField);
        
        $values = DictionaryService::findGroupAsDictionary(Dictionary::MODALITA_SVOLGIMENTO_PROVE);
        $misureDispensativeField = new FormCheckboxMultiField('Modalità di svolgimento delle prove di verifica/valutazione degli apprendimenti', 
                'pdp_svolgimento_prove', FormFieldType::STRING, $values, false);
        $misureDispensativeField->isEnchripted = true;
        $form->addField($misureDispensativeField);
        
        $activityField = new FormTextareaField("Se altro specificare", "pdp_svolgimento_prove_altro", FormFieldType::STRING, false);
        $activityField->isEnchripted = true;
        $form->addField($activityField);
        
        $noteField = new FormTextareaField("Note", "note", FormFieldType::STRING, false);
        $noteField->isEnchripted = true;
        $form->addField($noteField);

        if ($form->isSubmit() && $form->checkFields()){
            if ($target == null){
                $form->persist("", [
                    'pei_row_id' => $this->peiRowId,
                    'insert_date' => 'NOW()',
                    'last_edit_date' => 'NOW()',
                    'last_edit_user_id' => $this->currentUser["user_id"]
                ]);
            } else {
                $form->persist("pei_row_target_id=".$target['pei_row_target_id'], [
                    'last_edit_date' => 'NOW()',
                    'last_edit_user_id' => $this->currentUser["user_id"]
                ]);
            }

            EM::updateEntity("pei_row", [
                'last_edit_date' => 'NOW()',
                'last_edit_user_id' => $this->currentUser["user_id"]
            ], [
                "pei_row_id" => $this->peiRowId
            ]);

            redirect( UriService::buildPageUrl("/structure/pei/PeiAction", "details", ["documentId" => $this->document["user_year_document_id"]]));
        } else {
            $form->draw();
        }
    }

    protected function drawNodeDetails($node){
        $path = "";
        if ($this->peiRow['uri'] == 'apprendimento'){
            $path = 'APPRENDIMENTO -> ' . $this->peiRow['title'];
        } else if (is_array($node['uri'])){
            $path = PeiService::buildNodePath( $node['uri'], $this->documentType );
        }
        if (StringUtils::isNotBlank($path)){
        ?>
        <div class="row">
            <div class="medium-3 columns">
                <p><strong>Processo</strong></p>
            </div>
            <div class="medium-9 columns">
                <p><?= _t($path) ?></p>
            </div>
        </div>
        <?php } ?>
        <?php if($node && StringUtils::isNotBlank($node['description'])){ ?>
        <div class="row">
            <div class="medium-3 columns">
                <p><strong>Definizione</strong></p>
            </div>
            <div class="medium-9 columns">
                <p><?= _t($node['description']) ?></p>
            </div>
        </div>
        <?php } ?>
        <?php if ($node && $node['icf_codes'] != NULL){ ?>
        <div class="row">
            <div class="medium-3 columns">
                <p><strong>Codici ICF-CY</strong></p>
            </div>
            <div class="medium-9 columns">
                <div class="row">
                    <?php foreach ( json_decode( $node['icf_codes'] ) as $code) { 
                        $icfCode = IcfService::find($code);
                        if ($icfCode != null) {
                        ?>
                        <dic class="columns small-2">
                            <?= _t($code) ?>
                        </dic>
                        <dic class="columns small-10">
                            <?= _t($icfCode['name']) ?>
                        </dic>
                    <?php 
                        } else {
                            echo $code;
                        }
                    } ?>
                </div>
            </div>
        </div>
        <?php } ?>

        <?php if ($node && $node['icd10_codes'] != NULL){ ?>
        <div class="row">
            <div class="medium-3 columns">
                <p><strong>Codici ICD10</strong></p>
            </div>
            <div class="medium-9 columns">
                <p>
                    <?php 
                        $out = "";
                        foreach ( json_decode( $node['icd10_codes'] ) as $code) { 
                            $out .= $out == "" ? '' : ', ';
                            $out .= $code;
                        } 
                        echo $out;
                    ?>
                </p>
            </div>
        </div>
        <?php } ?>
        <?php
        if ($node){
            $questions = PeiService::findNodeQuestion($node['pei_node_id'], $this->documentType);
            
            if ($questions->rowCount() > 0){ ?>
                <div class="row">
                    <div class="medium-3 columns">
                        <p><strong>Domande esplicative</strong></p>
                    </div>
                    <div class="medium-9 columns">
                        <p>
                            <?php 
                            $i = 0;
                            foreach($questions as $question){ ?>
                                <?= _t($question['question']) ?><br/>
                            <?php } ?>
                        </p>
                    </div>
                </div>
            <?php         
            }
        }
    }
    
    public function drawActivities(){
        $status = DictionaryService::findGroupAsDictionary(Dictionary::PEI_ROW_TARGET_STATUS);
        $targets = UserPeiService::findPeiRowTargetByRow($this->peiRowId );
        
        if ($targets->rowCount() > 0){
            foreach($targets as $target){
                $data = [
                    'Osservazioni' => EncryptService::decrypt( $target['observation'] ),
                    'Obiettivo' => TextService::printText( EncryptService::decrypt( $target['target'] ) ),
                    'Note' => TextService::printText( EncryptService::decrypt( $target['note'] ) ),
                    'Ultima modifica' => TextService::formatDate( $target["last_edit_date"] )
                ];

                if (UserRoleService::canCurrentUserDo(UserPermission::LAST_EDIT_USER_VIEW)){
                    $data["Modificato da"] = $target["ue_name"]. ' ' . $target["ue_surname"];
                }
            ?>
                <div class="row" id="row_target_<?= $target['pei_row_target_id'] ?>" style="margin-bottom: 20px;">
                    <div class="small-8 columns">
                        <?php $this->drawTargetData($data) ?>
                    </div>
                    
                    <div class="column small-4 text-right">
                        <a class="button rounded tiny" href="<?= UriService::buildPageUrl("/structure/pei/PeiRowTargetAction", "", ["peiRowId" => $this->peiRowId, 'peiRowTargetId' => $target['pei_row_target_id']]) ?>">
                            <i class="<?= Icon::PENCIL ?>"></i>
                        </a>
                        <a class="button rounded alert tiny" href="javascript:doConfirm('Rimuovere i dati selezionati?','<?= $this->actionUrl('deleteRowTarget', ['peiRowTargetId' => $target['pei_row_target_id']]) ?>');">
                            <i class="<?= Icon::TRASH ?>"></i>
                        </a>
                    </div>
                </div>
            <?php 
            }
        } else { ?>
            <div class="row">
                <div class="small-12 columns">
                    <p>Non sono stati inseriti obiettivi e attività.</p>
                </div>
            </div>
        <?php }
        
        ?>
        <a class="button radius" href="<?= UriService::buildPageUrl("/structure/pei/PeiRowTargetAction", "", ["peiRowId" => $this->peiRowId]) ?>">
            <fa class="fa fa-plus"></fa>
            Aggiungi
        </a>
        
        <a class="button radius alert" href="<?= UriService::buildPageUrl("/structure/pei/PeiAction", "details", ["documentId" => $this->document["user_year_document_id"]] ) ?>">
            <fa class="fa fa-undo"></fa>
            Annulla
        </a>
        <?php  
    }
    
    private function drawTargetData($data){
        foreach ($data as $key => $value){
            if (StringUtils::isBlank($value)){
                continue;
            }
            ?>
            <div class="row mb-1">
                <div class="columns medium-3">
                    <strong><?= $key ?></strong>
                </div>
                <div class="columns medium-9">
                    <?= _t($value) ?>
                </div>
            </div>
            <?php
        }
    }
    
    public function deleteRowTarget(){
        if (StringUtils::isBlank( $this->peiRowTargetId )){
            redirectAccessDenied();
        }
        
        $rt = PeiService::findPeiRowTarget($this->peiRowTargetId);
        if ($rt == NULL){
            redirectAccessDenied();
        }
        
        $row = PeiService::findPeiRow($rt['pei_row_id']);
        if ($row['pei_id'] != $this->pei['user_year_document_id']){
            LogService::error_($this, "Tentativo di rimozione di un obiettivo di un PEI non correlato " . $this->peiRowTargetId);
            redirectAccessDenied();
        }
        
        EM::deleteEntity("pei_row_target", ['pei_row_target_id' => $this->peiRowTargetId]);
        redirect($this->actionUrl("_default"));
    }
}
