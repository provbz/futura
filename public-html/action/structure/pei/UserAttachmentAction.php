<?php
/* 
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Manage user attachments for PEI and PDP
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserAttachmentAction extends StructureUserDocumentAction{
    
    public $type;
    public $fileName;
    public $method;
    public $attachmentId;
    
    public function _default(){
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], UriService::buildPageUrl("/structure/UserAction", null, [
            "documentId" => $this->documentId
        ])));
        $this->bradcrumbs->add(new Bradcrumb("Allegati"));
        $this->header();
        
        ?>
        <style>
            h4{
                margin-top: 50px;
                margin-bottom: 20px;
                border-bottom: 1px solid grey;
            }
        </style>
        <div class="content active tab-studente row" id="panelTabStudenti">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="medium-12 columns">
                    <h3>Allegati</h3>
                </div>
            </div>
            <div class="sezioni">
                <div style="margin: 20px 0px 30px 0px; line-height: 1.5;">
                    <strong>Attenzione:</strong> i files presenti in questa sezione contengono dati sensibili, se i files vengono scaricati sul dispositivo dell'utente 
                    dovrà essere cura dell'utente procedere alla completa rimozione al termine dell'utilizzo.<br/>
                    Inoltre, è a carico dell'utente evitare qualsiasi tipo di diffusione non autorizzata.
                </div>

                <?= $this->displayAttachments() ?>
            </div>
        </div>
        <?php
        
        $this->footer();
    }    
    
    public function displayAttachments($canRemove = true, $readOnly = false){
        $form = new Form($this->actionUrl("_default", ["documentId" => $this->documentId]));
        $form->displaySubmit = false;

        if ($this->documentType == DocumentType::PEI || 
            $this->documentType == DocumentType::PDP || 
            $this->documentType == DocumentType::DROP_OUT ){
            $types = DictionaryService::findGroupAsDictionary(Dictionary::ATTACHMENT_TYPE);        

            foreach($types as $key =>  $type){ 
                if ($key == AttachmentType::PEI_PDP_ALTRO){
                    continue;
                }
                
                $this->attachmentField($form, $type, $key, $canRemove, $readOnly);
            }

            if (StringUtils::isNotBlank($this->userYear['classe']) && strstr($this->userYear['classe'], "S2") !== false) {
                $this->attachmentField($form, "Progetto individuale", AttachmentType::pei_progetto_individuale, true, true);
            }
        }

        if ($this->documentType == DocumentType::PDP_LINGUA){
            $this->attachmentField($form, "Consenso privacy", AttachmentType::AT_CONSENSO, $canRemove, $readOnly);
        }

        if ($this->documentType == DocumentType::PEI_TIROCINIO){
            $this->attachmentField($form, "Diagnosi", AttachmentType::AT_DIAGNOSI, $canRemove, $readOnly);
            $this->attachmentField($form, "Consenso privacy", AttachmentType::AT_CONSENSO, $canRemove, $readOnly);
        }

        if ($this->documentType == DocumentType::PEI){
            $form->addField(new FormHtmlField("<h4 class=''>Verbali</h4>"));
            $form->addField(new FormHtmlField("<p>Si consiglia la redazione di un verbale iniziale – intermedio e finale rispetto al percorso individuato per l’alunno/a</p>"));
            $this->displayVerbali($form, $readOnly);

            $form->addField(new FormHtmlField("<h4 class=''>Certificazione delle competenze</h4>"));
            $this->attachmentField($form, "5° classe primaria",  AttachmentType::at_certificazione_competenze_5p, $canRemove, $readOnly);
            $this->attachmentField($form, "3° classe secondaria di primo grado", AttachmentType::at_certificazione_competenze_3p1, $canRemove, $readOnly);
            $this->attachmentField($form, "2° classe secondaria di secondo grado", AttachmentType::at_certificazione_competenze_5s2, $canRemove, $readOnly);
        }
        else
        {
            if ($this->hasAttachmentsOfType($this->user["user_id"], AttachmentType::pei_verbale . '_%')){
                $form->addField(new FormHtmlField("<h4 class=''>Verbali</h4>"));
                $form->addField(new FormHtmlField("<p>files provenienti da precedenti PEI</p>"));
                $this->displayVerbali($form, true);
            }

            if ($this->hasAttachmentsOfType($this->user["user_id"], AttachmentType::at_certificazione_competenze_base . '_%')){
                $form->addField(new FormHtmlField("<h4 class=''>Certificazione delle competenze</h4>"));
                $form->addField(new FormHtmlField("<p>Files provenienti da precedenti PEI</p>"));
                $this->attachmentField($form, "5° classe primaria",  AttachmentType::at_certificazione_competenze_5p, false, true);
                $this->attachmentField($form, "3° classe secondaria di primo grado", AttachmentType::at_certificazione_competenze_3p1, false, true);
                $this->attachmentField($form, "2° classe secondaria di secondo grado", AttachmentType::at_certificazione_competenze_5s2, false, true);    
            }
        }

        $form->addField(new FormHtmlField("<h4 class=''>Documenti elaborati</h4>"));
        $this->displayDocumentYearAttachment($form, $readOnly);

        $form->addField(new FormHtmlField("<h4 class=''>Altri documenti</h4>"));
        //$this->attachmentField($form, "Altro", AttachmentType::PEI_PDP_ALTRO, $canRemove, $readOnly, 0);
        $this->dysplayAttachmentsByYearForDocument($form, $readOnly, AttachmentType::PEI_PDP_ALTRO, "Altro", true, true, 0);

        $form->draw();
    }

    private function hasAttachmentsOfType($userId, $type){
        $hasAttachments = EM::execQuerySingleResult("SELECT 1 as c
            FROM attachment_entity
            WHERE entity = :entity AND entity_id = :entity_id AND type LIKE :attachment_type", [
                ":entity" => EntityName::USER,
                ":entity_id" => $userId,
                ":attachment_type" => $type
            ]);
        return $hasAttachments != null;
    }

    private function displayVerbali($form, $readOnly = false){
        $schoolYears = SchoolYearService::findAll();
        
        foreach($schoolYears as $sy){
            if ($sy["current"]){
                $this->attachmentField($form, $sy["school_year"], $this->buildAttachmentType(AttachmentType::pei_verbale, $sy["school_year_id"]), true, $readOnly, 3);
            } else {
                $attachments = AttachmentService::findAllForEntity(EntityName::USER, $this->user["user_id"], $this->buildAttachmentType(AttachmentType::pei_verbale, $sy["school_year_id"]));
                if ($attachments->rowCount() > 0){
                    $this->attachmentField($form, $sy["school_year"], $this->buildAttachmentType(AttachmentType::pei_verbale, $sy["school_year_id"]), true, $readOnly, 3);
                }
            }
        }
    }

    private function displayDocumentYearAttachment($form, $readOnly = false){
        $this->dysplayAttachmentsByYearForDocument($form, $readOnly, DocumentType::PEI, "PEI");
        $this->dysplayAttachmentsByYearForDocument($form, $readOnly, DocumentType::PEI_TIROCINIO, "PEI di tirocinio");
        $this->dysplayAttachmentsByYearForDocument($form, $readOnly, DocumentType::PDP, "PDP");
        $this->dysplayAttachmentsByYearForDocument($form, $readOnly, DocumentType::PDP_LINGUA, "PDP per bisogni linguistici");
    }

    private function dysplayAttachmentsByYearForDocument($form, $readOnly, $documentType, $label, $displayNoYear = false, $alwaysDisplayCurentYear = false, $maxAttachments = 1){
        $schoolYears = SchoolYearService::findAll();
        
        $isAdmin = UserRoleService::hasCurrentUser(UserRole::ADMIN);

        if ($displayNoYear){
            $attachments = AttachmentService::findAllForEntity(EntityName::USER, $this->user["user_id"], $documentType);
            if ($attachments->rowCount() > 0 || $isAdmin){
                $this->attachmentField($form, $label, $documentType, $isAdmin, $readOnly, $maxAttachments);
            }
        }

        foreach($schoolYears as $sy){
            if ($sy["current"] && ($documentType == $this->documentType || $alwaysDisplayCurentYear)){
                $this->attachmentField($form, $label . ' ' . $sy["school_year"], $this->buildAttachmentType($documentType, $sy["school_year_id"]), true, $readOnly, $maxAttachments);
            } else {
                $attachments = AttachmentService::findAllForEntity(EntityName::USER, $this->user["user_id"], $this->buildAttachmentType($documentType, $sy["school_year_id"]));
                if ($attachments->rowCount() > 0 || $isAdmin){
                    $this->attachmentField($form, $label . $sy["school_year"], $this->buildAttachmentType($documentType, $sy["school_year_id"]), $isAdmin, $readOnly, $maxAttachments);
                }
            }
        }        
    }

    private function buildAttachmentType($documentName, $schoolYearId){
        $type = $documentName . "_sy_". $schoolYearId;
        return $type;
    }

    private function attachmentField($form, $label, $name, $canRemove, $readOnly = false, $maxAttachments = 1){
        $attachmentField = new FormAttachmentsField($label, $name, EntityName::USER, $this->user["user_id"], false);
        $attachmentField->removeUrl = $this->actionUrl("removeAttachment");
        
        $attachmentField->manAttachments = $maxAttachments;
        
        $attachmentField->noTemp = true;
        $attachmentField->canRemove = $canRemove;
        $attachmentField->readOnly = $readOnly;
        $attachmentField->extensions = ["pdf"];
        $form->addField($attachmentField);
    }
    
    public function removeAttachment(){
        $attachmentEntities = AttachmentService::findAttachmentEntityForAttachmentId($this->attachmentId);
        $attachmentEntity = $attachmentEntities->fetch();
        if ($attachmentEntity == null){
            return;
        }

        if ($attachmentEntity["entity"] != EntityName::USER 
            || $attachmentEntity["entity_id"] != $this->user["user_id"]){
            return;
        }

        AttachmentService::deleteAttachmentAndAttachmentEntity($this->attachmentId);

        PeiService::updatePeiStatus($this->document["user_year_document_id"]);
    }
    
    public function getAttachment(){
        if (StringUtils::isBlank($this->fileName)){
            redirectWithMessage($this->actionUrl("_default"), "File non specificato.");
        }
            
        $filePath = UriService::buildUserAttachmentPath($this->user['user_id']) .'/'. $this->type .'/'. $this->fileName;
        if (!file_exists($filePath)){
            redirectWithMessage($this->actionUrl("_default"), "File non valido.");
        }
        
        if ($this->method != 'inline' && $this->method != 'attachment'){
            redirectWithMessage($this->actionUrl("_default"), "Metodo non valido.");
        }
        
        $fileContent = EncryptService::decryptfileInMemory($filePath);
        
        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public");
        //header("Content-Type: ". $attachment["mime_type"]);
        
        $mimeType = "";
        $extension = substr($this->fileName, strpos($this->fileName, "."));
        switch ($extension){
            case ".pdf": $mimeType = "application/pdf"; break;
            case ".doc": $mimeType = "application/msword"; break;
            case ".docx": $mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; break;
            default: $mimeType = "application/octet-stream"; break;
        }
        if (StringUtils::isNotBlank($mimeType)){
            header("Content-Type: ". $mimeType);
        }
        
        header("Content-Transfer-Encoding: Binary");
        //header("Content-Length:" . $attachment['size']);
        header("Content-Disposition: " . $this->method ."; filename=" . $this->fileName);
         
        echo $fileContent;
        die();
    }
}

