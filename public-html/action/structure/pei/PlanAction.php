<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once(__DIR__ .'/PeiBaseAction.php');

/**
 * Description of PlanAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PlanAction extends PeiBaseAction{
    
    public $time;
    public $plan;
    
    public function _prepare() {
        parent::_prepare();
        
        $this->plan = [];
        if (StringUtils::isNotBlank($this->userYear['plan'])){
            $this->plan = json_decode($this->userYear['plan'], true);
        }
    }

    public function _default(){
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], UriService::buildPageUrl("/structure/UserAction", null, [
            "documentId" => $this->documentId
        ])));
        $this->bradcrumbs->add(new Bradcrumb("Calendario"));
        $this->header();
        
        $days = ['Orario', 'Lun', 'Mar', 'Mer', 'Giov', 'Ven', 'Sab'];
        $hours = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        ?>
        <style type="text/css">
            .titles{
                font-weight: bold;
            }
            .hour-row{
                height: 100px;
                border-bottom: 1px solid #CCC;
                margin-left: 0px;
                margin-right: 0px;
            }
            .hour-cell{
                display: inline-block;
                width: 8%;
                height: 100px;
                text-align: center;
                top: -46px;
                position: relative;
            }
            .day-cell{
                overflow: hidden;
                height: 100px;
                font-size: 0.9em;
                padding-left: 2px;
                padding-right: 2px;
                margin-left: 2px;
                margin-right: 2px;
                display: inline-block;
                width: 13.5%;
                cursor: pointer;
                text-align:center;
                line-height: 100px;
            }
            .day-cell:hover{
                border: 1px solid #00CC00;
            }
            .day-cell span{
                display: inline-block;
                vertical-align: middle;
                line-height: 15px;
            }
            .placeholder{
                color: lightgray;
                text-align: center;
                width: 100%;
                display: block;
            }
        </style>
        <script type="text/javascript">
            function editPlan(time){
                openDialog('#editPlanDialog');
                loadDiv('#editPlanDialog_content', '<?= $this->actionUrl('planDetails', ["documentId" => $this->documentId]) ?>', {'time': time });
            }
            function activitySubmit(){
                var selectorId = "#form_generic";
                var data = $(selectorId).serialize();
                var url = $(selectorId).attr('action');
                if (url.indexOf("?") === -1){
                    url += "?" + data;
                } else {
                    url += "&" + data;
                }
                loadDiv(selectorId + "_container", url);
            }
            function remove(time){
                if (!confirm("rimuovere l'attività?")){
                    return;
                }
                window.location = '<?= $this->actionUrl("remove", ["documentId" => $this->documentId]) ?>&time=' + time;
            }
        </script>
        <div id="editPlanDialog" class="reveal-modal" data-reveal>
            <div class="row">        
                <div class="small-12 medium-12 large-12 columns">
                    <h4>Modifica valore</h4> 
                </div>        	
            </div>
            <div id="editPlanDialog_content">
                
            </div>
        </div>
        
        <div class="content active tab-procedimento-pei" id="panelTabStudenti">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-6 medium-4 columns">
                    <h3>Calendario settimanale</h3>
                </div>
            </div>
            
            <div class="row">
                <div class="large-12 medium-12 columns">
                    <div class="titles row">
                        <?php foreach ($days as $dKey => $day){ ?>
                        <div class="columns" style="width: 14%">
                            <?= _t($day) ?>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="content">
                        <?php foreach ($hours as $hKey => $hour){ ?>
                        <div class="hour-row">
                            <?php foreach ($days as $dKey => $day){ 
                                $timeKey = $this->buildTimeKey($day, $hour);
                                ?>
                            <div class="day-cell" onclick="editPlan('<?= $timeKey ?>');">
                                <?php if (! isset($this->plan[$timeKey])){ ?>
                                <span class='placeholder'>
                                    <?php if ($day == 'Orario'){ ?>
                                    Dalle-Alle
                                    <?php } else { ?>
                                    Cliccare per inserire l'attività
                                    <?php } ?>
                                </span>
                                <?php } else { ?>
                                <span>
                                    <?= _t( $this->plan[$timeKey] ); ?>
                                </span>
                                <?php } ?>
                            </div>
                            <?php } ?>    
                        </div>
                        <?php } ?>

                        <?php
                        $journal = JournalService::findLastFor(JournalEntity::PEI, $this->document["user_year_document_id"], JournalSection::PEI_PLAN);
                        if ($journal != null){
                            ?>
                            <div class="row text-center" style="margin-top: 30px;">
                                <strong>Modificato il: </strong> <?= _t(TextService::formatDate( $journal["insert_date"] )) ?>
                                <?php if (UserRoleService::canCurrentUserDo(UserPermission::LAST_EDIT_USER_VIEW)) { ?>
                                    <strong>da:</strong> <?= _t($journal["ue_name"]) ?> <?= _t($journal["ue_surname"]) ?>
                                <?php } ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    private function buildTimeKey($day, $hour){
        $key = $day .'_'. $hour;
        $key = str_replace(":", "_", $key);
        return $key;
    }
    
    public function remove(){
        if (StringUtils::isBlank($this->time)){
            redirect(UriService::accessDanyActionUrl());
            return;
        }
        
        if (isset($this->plan[$this->time])){
            $this->plan[$this->time] = null;
        }
        
        $jsonPlan = json_encode($this->plan);
        EM::updateEntity("user_year", ['plan' => $jsonPlan], ['user_year_id' => $this->userYear['user_year_id']]);
        $this->updateUserYear();
        
        redirect($this->actionUrl("_default", ["documentId" => $this->documentId]));
    }
    
    public function planDetails(){
        if (StringUtils::isBlank($this->time)){
            echo "Dati non validi";
            return;
        }
        
        $text = "";
        if (isset($this->plan[$this->time])){
            $text = $this->plan[$this->time];
        }
        
        $form = new Form($this->actionUrl("planDetails", ["documentId" => $this->documentId]));
        $form->cancellAction = "javascript:closeDialog('#editPlanDialog');";
        $form->submitAction = "javascript:activitySubmit();";
        
        if (StringUtils::isNotBlank($text)){
            ob_start();
            ?>
            <a href="javascript:remove('<?= $this->time ?>');" class="button radius alert">
                <i class="fa fa-trash"></i>Rimuovi
            </a>
            <?php
            $form->additionalButtons = ob_get_clean();
        }
        
        $form->addField(new FormHiddenField("time", FormFieldType::STRING, $this->time));
        $label = "Attività";
        if(strpos($this->time, "Orario") !== false){
            $label = "Dalle - Alle";
        }
        $form->addField(new FormTextareaField($label, "text", FormFieldType::STRING, true, $text));
        
        if ($form->isSubmit() && $form->checkFields()){
            $field = $form->getFieldWithName("text");
            $value = $field->getValue(null);
            $this->plan[$this->time] = $value;
            $jsonPlan = json_encode($this->plan);
            EM::updateEntity("user_year", ['plan' => $jsonPlan], ['user_year_id' => $this->userYear['user_year_id']]);
            $this->updateUserYear();

            DocumentService::updateLastEdit($this->document["user_year_document_id"]);

            EM::insertEntity("journal", [
                "user_id" => $this->currentUser["user_id"],
                "insert_date" => "NOW()",
                "entity" => JournalEntity::PEI,
                "entity_id" => $this->document["user_year_document_id"],
                "section" => JournalSection::PEI_PLAN,
                "action" => JournalAction::UPDATE
            ]);
            ?>
                <script type="text/javascript">
                    window.location = '<?= $this->actionUrl("_default", ["documentId" => $this->documentId]) ?>';
                </script>
            <?php
        } else {
            $form->draw();
        }
    }
}
