<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
*/
require_once(__DIR__ .'/PeiBaseAction.php');

/**
 * Description of PeiRowTarget
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
require_once 'PeiRowAction.php';
class PeiRowTargetAction extends PeiRowAction{
    
    public $peiRowTargetId;
    public $peiRowTarget;
    
    public function buildBradcrumb() {
        parent::buildBradcrumb();
        
        $this->bradcrumbs->getLast()->link = UriService::buildPageUrl("/structure/pei/PeiRowAction", "", ["peiRowId" => $this->peiRowId]);
        $this->bradcrumbs->add(new Bradcrumb("Osservazioni Obiettivi e attività"));
    }

    public function _default(){
        $this->buildBradcrumb();
        
        $form = new Form($this->actionUrl("", ['peiRowTargetId' => $this->peiRowTargetId, "peiRowId" => $this->peiRowId]));
        $form->entityName = "pei_row_target";
        if ($this->peiRowTargetId != null){
            $form->entity = PeiService::findPeiRowTarget($this->peiRowTargetId);
        }
        
        $form->cancellAction = UriService::buildPageUrl("/structure/pei/PeiRowAction", "", ["peiRowId" => $this->peiRowId]);
        $form->addField(new FormHtmlField('<div class="sezione">
                <div class="info">'));
        
        $targetField = new FormTextareaField("Osservazioni", "observation", FormFieldType::STRING, false, null, ["maxLength" => 3000]);
        $targetField->isEnchripted = true;
        $form->addField($targetField);
        
        $proField = new FormTextareaField("Punti di forza", "pro", FormFieldType::STRING, false, null, ["maxLength" => 3000]);
        $proField->isEnchripted = true;
        $form->addField($proField);
        
        $consField = new FormTextareaField("Punti di debolezza/deficit", "cons", FormFieldType::STRING, false, null, ["maxLength" => 3000]);
        $consField->isEnchripted = true;
        $form->addField($consField);
        
        $targetField = new FormTextareaField("Obiettivi", "target", FormFieldType::STRING, false, null, ["maxLength" => 3000]);
        $targetField->isEnchripted = true;
        $form->addField($targetField);
        
        $curriculaType = DictionaryService::findGroupAsDictionary(Dictionary::PEI_CURRICULA_TYPE);            
        $curriculaType = array_merge(["" => ""], $curriculaType);
        $form->addField(new FormSelectField("Tipologia obiettivi", "type", FormFieldType::STRING, $curriculaType));
        
        $status = DictionaryService::findGroupAsDictionary(Dictionary::PEI_ROW_TARGET_STATUS);
        $status = array_merge(["" => ""], $status);
        $form->addField(new FormSelectField("Verifica obiettivi", "status", FormFieldType::STRING, $status));
        
        $strategieFields = new StrategieField('Strategie e strumenti metodologici', 
                'pei_strategie_approcci_metodologici');
        $strategieFields->displayHeader = true;
        $strategieFields->displayFacilitatore = false;
        $strategieFields->options = DictionaryService::findGroup(Dictionary::PEI_STRATEGIE_APPROCCI_METODOLOGICI);
        $form->addField($strategieFields);

        $approcciFields = new StrategieField('Approcci metodologici, tecniche, metodi', 
                'pei_approcci_metodologici');
        $approcciFields->options = DictionaryService::findGroup(Dictionary::PEI_APPROCCI_METODOLOGICI);
        $form->addField($approcciFields);

        $activityField = new FormTextareaField("Attività e interventi", "activity", FormFieldType::STRING, false, null, ["maxLength" => 3000]);
        $activityField->isEnchripted = true;
        $form->addField($activityField);
        
        $form->addField(new FormHtmlField("
            <div class='row-100 form-row'>
                <div class='columns small-6'>"));

        $activityField = new FormTextareaField("Barriere (elementi e/o fattori che ostacolano la partecipazione e l’apprendimento dell’alunno/a)", "barriere", FormFieldType::STRING, false, null, ["maxLength" => 3000]);
        $activityField->labelClass = "";
        $activityField->itemClass = "";
        $activityField->isEnchripted = true;
        $form->addField($activityField);

        $form->addField(new FormHtmlField("
                </div>
                <div class='columns small-6'>"));

        $facilitatoriField = new FormTextareaField("Facilitatori (elementi e/o fattori che aiutano la partecipazione e l'apprendimento dell'alunno/a)", "facilitatori", FormFieldType::STRING, false, null, ["maxLength" => 3000]);
        $facilitatoriField->labelClass = "";
        $facilitatoriField->itemClass = "";
        $facilitatoriField->isEnchripted = true;
        $form->addField($facilitatoriField);
        $form->addField(new FormHtmlField("
                </div>
            </div>"));

        $modalitàVerificaField = new ModalitaverificaField('Modalità di valutazione/verifica', 'pei_modalita_verifica');
        $modalitàVerificaField->options = DictionaryService::findGroup(Dictionary::PEI_MODALITA_VERIFICA);
        $form->addField($modalitàVerificaField);
        
        $noteField = new FormTextareaField("Note", "note", FormFieldType::STRING, false);
        $noteField->isEnchripted = true;
        $form->addField($noteField);
        
        $form->addField(new FormHtmlField('</div></div>'));
        
        $this->header();
        ?>
        <script type="text/javascript">
        $(function () {
            $('#pei_strategie_approcci_metodologici_sam_28').on('change', onStrategiaOther);
            onStrategiaOther();
        })

        function onStrategiaOther(){
            var val = $('#pei_strategie_approcci_metodologici_sam_28').prop("checked");
            if(val){
                $('#campo_form_pei_strategie_approcci_altro').show();
            } else {
                $('#campo_form_pei_strategie_approcci_altro').hide();
                $('#campo_form_pei_strategie_approcci_altro textarea').val("");
            }
        }
        </script>
        <div class="content active tab-nuovo-studente row" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-9 medium-7 columns">
                    <h3>Osservazioni, obiettivi e attività</h3>
                </div>
            </div>

            <?php 
            $this->drawNodeDetails($this->node);
            ?>

            <div class="sezioni">
                <div class="alert alert-danger my-5">
                    Si ricorda che ciò che viene spuntato o scritto nei campi editabili deve poi trovare un'effettiva applicazione nella pratica didattica.
                </div>

                <?php 
                if($form->isSubmit()){ 
                    self::emptyfields($form);
                }

                if (count($form->errors) == 0 && $form->isSubmit() && $form->checkFields()){
                    if ($this->peiRowTargetId == null){
                        $form->persist("", [
                            'pei_row_id' => $this->peiRowId,
                            'insert_user_id' => $this->currentUser["user_id"],
                            'insert_date' => 'NOW()',
                            'last_edit_user_id' => $this->currentUser["user_id"],
                            'last_edit_date' => 'NOW()'
                        ]);
                    } else {
                        $form->persist("pei_row_target_id=".$this->peiRowTargetId, [
                            'last_edit_user_id' => $this->currentUser["user_id"],
                            'last_edit_date' => 'NOW()'
                        ]);
                    }

                    EM::updateEntity("pei_row", [
                        "last_edit_date" => "NOW()",
                        "last_edit_user_id" => $this->currentUser["user_id"]
                    ], [
                        "pei_row_id" => $this->peiRowId
                    ]);
                    
                    DocumentService::update($this->document["user_year_document_id"]);
                    
                    redirectWithMessage(UriService::buildPageUrl("/structure/pei/PeiRowAction", "", ["peiRowId" => $this->peiRowId]), "Dati inseriti correttamente");
                } 

                $form->draw();
                ?>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    public function emptyfields($form){
        $err=false;
        $entity = $form->getEntity();
        foreach ($entity as $key => $value){
            if(StringUtils::isNotBlank($value)){
                $err = true;
            }
        }
        if($err == false){
            array_push($form->errors, "Almeno un campo deve essere compilato.");
        }
    }
}

class StrategieField extends FormField{

    public $options;
    public $displayHeader;
    public $displayFacilitatore;

    public function __construct($label, $name) {
        parent::__construct($label, $name, FormFieldType::STRING, false);
    }

    public function draw($entity = NULL) {
        $value = $this->getValue($entity);
        ?>
        <div id="<?= $this->name ?>" class="field-codes">
            <div class="<?= $this->name ?>-container"></div>
            
            <?= $this->drawErrorTag() ?>
        </div>
        <script type="text/javascript">
            <?php
            $parameters = [
                "inputName" => $this->name,
                "value" => $value,
                "readOnly" => $this->readOnly,
                "options" => $this->options,
                "displayHeader" => $this->displayHeader,
                "displayFacilitatore" => $this->displayFacilitatore
            ];
            ?>
            MbCreateVue('MbPeiTargetStrategieField', '<?= $this->name ?>-container', <?= json_encode($parameters) ?>);
        </script>
        <?php
    }
    
    public function encodeValue($value){
        $value = json_encode($value);
        $value = htmlentities($value);
        return $value;
    }
    
    public function decodedValues($value){
        $val = urldecode($value);
        return json_decode($val);
    }

    public function getValueForDb(){
        $value = getRequestValue($this->name);
        return urldecode($value);
    }
}

class ModalitaverificaField extends FormField{

    public $options;

    public function __construct($label, $name) {
        parent::__construct($label, $name, FormFieldType::STRING, false);
    }

    public function draw($entity = NULL) {
        $value = $this->getValue($entity);
        ?>
        <div id="<?= $this->name ?>" class="field-codes">
            <div class="<?= $this->name ?>-container"></div>
            
            <?= $this->drawErrorTag() ?>
        </div>
        <script type="text/javascript">
            <?php
            $taKeys = [];
            foreach ($this->options as $v){
                $taKeys[] = $v['key'];
            }

            $parameters = [
                "inputName" => $this->name,
                "value" => $value,
                "readOnly" => $this->readOnly,
                "options" => $this->options,
                'optionsWithTextArea' => $taKeys
            ];
            ?>
            MbCreateVue('MbPeiModalitaVerificaField', '<?= $this->name ?>-container', <?= json_encode($parameters) ?>);
        </script>
        <?php
    }
    
    public function encodeValue($value){
        $value = json_encode($value);
        $value = htmlentities($value);
        return $value;
    }
    
    public function decodedValues($value){
        $val = urldecode($value);
        return json_decode($val);
    }

    public function getValueForDb(){
        $value = getRequestValue($this->name);
        return urldecode($value);
    }

}
