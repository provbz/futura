<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AttachmentSummaryAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
require_once __DIR__ . "/../UserYearDocumentAction.php";

class AttachmentSummaryAction extends UserYearDocumentAction {
    
    public $type;
    public $plessoId;
    public $classeId;

    public function _default(){
        $plessi = [];
        foreach (StructureService::findAllWithParent($this->structureId) as $plesso) {
            $plessi[$plesso["structure_id"]] = $plesso["name"];
        }

        $classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);

        $this->header();
        ?>
        <div class="">
            <div class="section-title row-100">
                <div class="large-8 medium-7 small-7 columns">
                    <h3>Stato caricamento file</h3>
                </div>
            </div>

            <div class="mb-structure-pei-file-stats-content"></div>
            <script>
                MbCreateVue("mb-structure-pei-file-stats", "mb-structure-pei-file-stats-content", <?= json_encode([
                    "structures" => $plessi,
                    "classi" => $classes
                ]) ?>);
            </script>
        </div>
        <?php
        $this->footer();
    }
    
    public function getData(){
        $currentSchoolYear = SchoolYearService::findCurrentYear();
        $classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);

        $qParams = [
            "structure_id" => $this->structure["structure_id"],
            "school_year_id" => $currentSchoolYear["school_year_id"],
            "ready" => RowStatus::READY,
            "document_type" => $this->documentType
        ];

        $query = "SELECT user_year_document_id, uyd.user_id, code, ae.type, ae.attachment_id, plesso.name plesso_name, uy.classe, uy.sezione
            FROM user_year_document uyd
            LEFT JOIN user_year uy ON uyd.user_id  = uy.user_id
            LEFT JOIN user u ON u.user_id  = uy.user_id
            LEFT JOIN user_structure us ON u.user_id  = us.user_id
            left join attachment_entity ae ON ae.entity='user' AND ae.entity_id=uyd.user_id
            left join structure plesso ON uy.plesso_structure_id = plesso.structure_id
            WHERE us.structure_id = :structure_id
            AND school_year_id = :school_year_id
            AND uyd.status = :ready
            AND uyd.type = :document_type ";

        if (StringUtils::isNotBlank($this->plessoId)){
            $query .= " AND plesso.structure_id = :plessoId ";
            $qParams["plessoId"] = $this->plessoId;
        }
        if (StringUtils::isNotBlank($this->classeId)){
            $query .= " AND uy.classe = :classeId ";
            $qParams["classeId"] = $this->classeId;
        }

        $query .= " ORDER BY plesso.name, uy.classe, uy.sezione";

        $rows = EM::execQuery($query, $qParams)->fetchAll();

        $result = [];

        if ($this->documentType == DocumentType::PDP_LINGUA){
            $result = [
                "document_type" => $this->documentType,
                "file_types" => [
                    strtolower(AttachmentType::AT_CONSENSO) => "Consenso privacy",
                ],
                "students" => []
            ];
        } else {
            $result = [
                "document_type" => $this->documentType,
                "file_types" => [
                    strtolower(AttachmentType::PEI_PDP_DIAGNOSI) => "Diagnosi",
                    strtolower(AttachmentType::PEI_PDP_PDF) => "PDF",
                    strtolower(AttachmentType::PEI_PDP_CONSENSO) => "Consenso privacy",
                    strtolower(AttachmentType::at_certificazione_competenze_5p) => "Certificazione competenze<br/>5° classe primaria",
                    strtolower(AttachmentType::at_certificazione_competenze_3p1) => "Certificazione competenze<br/>3° classe secondaria di primo grado",
                    strtolower(AttachmentType::at_certificazione_competenze_5s2) => "Certificazione competenze<br/>2° classe secondaria di secondo grado°",
                ],
                "students" => []
            ];
        }

        $schoolYeara = SchoolYearService::findLast(3);
        foreach ($schoolYeara as $sy) {
            $key = strtolower($this->documentType . '_sy_' . $sy["school_year_id"]);
            $result["file_types"][$key] = $this->documentType . " " . $sy["school_year"];
        }

        $fileData = [];
        foreach ($rows as $row) {
            if (!isset($fileData[$row["user_year_document_id"]])){
                $fileData[$row["user_year_document_id"]] = [
                    "user" => [
                        "plesso_name" => $row["plesso_name"],
                        "classe" => $row["classe"],
                        "sezione" => $row["sezione"],
                        "user_year_document_id" => $row["user_year_document_id"],
                        "user_id" => $row["user_id"],
                        "code" => $row["code"]
                    ],
                    "files" => []
                ];

                if (isset($classes[$row["classe"]])){
                    $fileData[$row["user_year_document_id"]]["user"]["classe"] = $classes[$row["classe"]];
                }
            }

            if ($row["type"] != null){
                $fileData[$row["user_year_document_id"]]["files"][] = strtolower( $row["type"] );
            }
        }

        $students = [];
        $addedIds = [];
        foreach($rows as $row){
            if (in_array($row["user_year_document_id"], $addedIds)){
                continue;
            }

            $addedIds[] = $row["user_year_document_id"];
            $students[] = $fileData[$row["user_year_document_id"]];
        }

        $result["students"] = $students;

        echo json_encode($result);
        die();
    }

    public function download(){
        $options = new ZipStream\Option\Archive();
        $options->setSendHttpHeaders(true);

        $currentSchoolYear = SchoolYearService::findCurrentYear();
        $filename = $this->type .'_' . $currentSchoolYear["school_year"] . '.zip';
        $zip = new ZipStream\ZipStream($filename, $options);

        set_time_limit(300);
        $this->addAttachmentsToZip($this->type, $zip);
        
        $zip->finish();
    }

    private function addAttachmentsToZip($type, $zip) {
        $currentSchoolYear = SchoolYearService::findCurrentYear();

        $query = "SELECT user_year_document_id, uyd.user_id, code, ae.entity, ae.entity_id, ae.type, ae.attachment_id, a.filename, plesso.name plesso_name, uy.classe, uy.sezione
            FROM user_year_document uyd
            LEFT JOIN user_year uy ON uyd.user_id  = uy.user_id
            LEFT JOIN user u ON u.user_id  = uy.user_id
            LEFT JOIN user_structure us ON u.user_id  = us.user_id
            left join attachment_entity ae ON ae.entity='user' AND ae.entity_id=uyd.user_id
            left join attachment a ON a.attachment_id = ae.attachment_id
            left join structure plesso ON uy.plesso_structure_id = plesso.structure_id
            WHERE us.structure_id = :structure_id
            AND school_year_id = :school_year_id
            AND uyd.status = :ready
            AND uyd.type = :document_type
            AND ae.type = :type
            ";

        $qParams = [
            "structure_id" => $this->structure["structure_id"],
            "school_year_id" => $currentSchoolYear["school_year_id"],
            "ready" => RowStatus::READY,
            "document_type" => $this->documentType,
            "type" => $type
        ];

        if (StringUtils::isNotBlank($this->plessoId)){
            $query .= " AND plesso.structure_id = :plessoId ";
            $qParams["plessoId"] = $this->plessoId;
        }

        if (StringUtils::isNotBlank($this->classeId)){
            $query .= " AND uy.classe = :classeId ";
            $qParams["classeId"] = $this->classeId;
        }

        $rows = EM::execQuery($query, $qParams);

        foreach($rows as $row){
            $attachments = AttachmentService::findAllForEntity($row["entity"], $row["entity_id"], $row["type"]);
            foreach ($attachments as $attachment){
                $path = AttachmentService::buildAttachmentPath($attachment);
                $fileContent = EncryptService::decryptfileInMemory($path);

                //get extension
                $ext = pathinfo($row["filename"], PATHINFO_EXTENSION);
                $zip->addFile($row['plesso_name'] . '/' .  $row['code'] . '_' . $row["classe"] . '_' . $row["sezione"] . '.' . $ext, $fileContent);
            }
        }
    }
}
