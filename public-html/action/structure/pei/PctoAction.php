<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once(__DIR__ .'/PeiBaseAction.php');

/**
 * Description of PctoAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PctoAction extends PeiBaseAction {
    
    public function _default(){
        $form = new Form($this->actionUrl("_default", ["documentId" => $this->documentId]));
        $form->entityName = "user_pcto";
        $form->entity = UserPctoService::find($this->user["user_id"]);
        if ($form->entity == null){
            $form->entity = [];
        }

        $form->cancellAction = UriService::buildPageUrl("/structure/UserAction", "_default", ["documentId" => $this->documentId]);
        
        $schoolYear = SchoolYearService::findCurrentYear();

        $form->addField(new FormTextField("Anno scolastico", "anno_scolastico", FormFieldType::STRING, false, $schoolYear["school_year"], [
            "maxLength" => 45
        ]));
        $form->addField(new FormTextField("Tipo di percorso<br/>(indicare se istituzionalizzato o individualizzato)", "tipo_percorso", FormFieldType::STRING, false, "", [
            "maxLength" => 100
        ]));
        $form->addField(new FormTextField("Struttura", "nome_struttura", FormFieldType::STRING, false, "", [
            "maxLength" => 100
        ]));
        $form->addField(new FormTextField("Ore presso struttura", "ore_struttura", FormFieldType::NUMBER, false, 0, [
            "numeric" => true
        ]));
        $form->addField(new FormTextField("Ore in aula", "ore_aula", FormFieldType::NUMBER, false, 0, [
            "numeric" => true
        ]));

        $journal = JournalService::findLastFor(JournalEntity::PEI, $this->document["user_year_document_id"], JournalSection::PEI_PCTO);
        if ($journal != null){

            ob_start();
            ?>
            <div class="row text-center" style="margin-top: 30px;">
                <strong>Modificato il: </strong> <?= _t(TextService::formatDate( $journal["insert_date"] )) ?>
                <?php if (UserRoleService::canCurrentUserDo(UserPermission::LAST_EDIT_USER_VIEW)) { ?>
                    <strong>da:</strong> <?= _t($journal["ue_name"]) ?> <?= _t($journal["ue_surname"]) ?>
                <?php } ?>
            </div>
            <?php
            $jdata = ob_get_clean();

            $form->addField(new FormHtmlField($jdata));
        }
        
        if ($form->isSubmit() && $form->checkFields()){
            if (!isset( $form->entity["user_pcto_id"] )){
                $form->entity["user_pcto_id"] = $form->persist(null, [
                    "user_id" => $this->user["user_id"],
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser["user_id"]
                ]);
            } else {
                $form->persist("user_pcto_id=" . $form->entity["user_pcto_id"]);
            }

            EM::updateEntity("user_pcto", [
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser["user_id"]
            ], [
                "user_pcto_id" => $form->entity["user_pcto_id"]
            ]);

            DocumentService::updateLastEdit($this->document["user_year_document_id"]);

            EM::insertEntity("journal", [
                "user_id" => $this->currentUser["user_id"],
                "insert_date" => "NOW()",
                "entity" => JournalEntity::PEI,
                "entity_id" => $this->document["user_year_document_id"],
                "section" => JournalSection::PEI_PCTO,
                "action" => JournalAction::UPDATE
            ]);

            redirect( UriService::buildPageUrl("/structure/UserAction", "_default", ["documentId" => $this->documentId]));
        }

        $this->header();
        
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], UriService::buildPageUrl("/structure/UserAction", null, [
            "documentId" => $this->documentId
        ])));
        $this->bradcrumbs->add(new Bradcrumb("PCTO"));
        
        ?>
        <div class="content active tab-nuovo-studente row" id="panelTabNuovoStudente">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="medium-12 columns">
                    <h3>PCTO</h3>
                </div>
            </div>
            <div class="sezioni">
                <?= $form->draw(); ?>
            </div>
        </div>
        <?php
        
        $this->footer();
    }
    
}

