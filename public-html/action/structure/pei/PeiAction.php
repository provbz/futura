<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */
require_once(__DIR__ .'/PeiBaseAction.php');

/**
 * Description of PeiAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PeiAction extends PeiBaseAction {
    
    public $peiId;
    public $code;
    public $status;
    public $uri;
    public $matter;
    public $curriculum_type;
    
    protected $pei;
    
    public $peiRowId;
    public $peiRowTargetId;
    
    public $type;
    public $printUserData;
    public $printDiagnosis;
    
    public $level;
    public $term;
    
    public $author;
    public $note;
    
    public function _prepare() {
        parent::_prepare();
        
        $this->pei = $this->document;
        $this->peiId = $this->documentId;
        $this->updatePei();
        if ($this->pei == null){
            redirect(UriService::accessDanyActionUrl());
        }
    }
    
    protected function updatePei(){
        $this->pei = $this->document;
        $this->setGlobalSessionValue("PeiAction_pei", $this->pei);
    }
    
    public function details(){
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], UriService::buildPageUrl("/structure/UserAction", null, [
            "documentId" => $this->documentId
        ])));
        
        if ($this->pei['type'] == DocumentType::PEI){
            $this->bradcrumbs->add(new Bradcrumb("Elabora"));
        } else {
            $this->bradcrumbs->add(new Bradcrumb("Analisi"));
        }
        
        $this->header(); 
        ?>
        <style type="text/css">
            .node_row_link i{
                display: none;
            }
            .node_row_link.compiled i{
                display: inline;
            }
        </style>
        <script type="text/javascript">
        var selectedRowTargetId = null;

        $(function(){
            $(document).on('open.fndtn.reveal', '[data-reveal]', function () {
                var id = $(this).attr('id');
                switch(id){
                    case 'dialog-find_nodes':
                        $('#find_nodes').attr('src', '<?= UriService::buildPageUrl('/structure/pei/TreeAction', '', 
                            [
                                'displayHeader' => false, 
                                'displayActivities' => false,
                                'displayAddButton' => true,
                                "documentId" => $this->documentId
                            ]
                        ) ?>');
                        break;
                }
            });
        });
        
        function changePeiRowStatus(peiRowId, status){
            if (! confirm("Rimuovere il processo selezionato?")){
                return;
            }

            window.location = '<?= $this->actionUrl('changePeiRowStatus') ?>?documentId=<?= $this->documentId ?>&peiRowId=' + peiRowId + '&status=' + status;
        }
        
        function refreshPei(){
            loadDiv('#pei_container', '<?= $this->actionUrl('loadPei') ?>', [], function(){});
        }
        
        function addPeiRow(uri){
            closeDialog("#dialog-find_nodes");
            $.post(
                '<?= $this->actionUrl('addPeiRow') ?>',
                {'uri': uri},
                function(){
                    refreshPei();
                }
            );
        }
                
        function checkMatter(){
            var value = $('#matter').val();
            if (StringUtils.isBlank(value)){
                $('#matter_error').show();
                return false;
            }
            return true;
        }

        function restoreNodes(){
            window.vueBus.$emit("right-panel-show", {
					title: "Recupera righe cancellate",
                    componentType: "MbPeiRestoreNode",
                    parameters: {
                        "documentId": <?= $this->documentId ?>
                    }
                });
        }
        </script>
                
        <div id="dialog-new_matter" class="dialog reveal-modal" data-reveal>
            <div class="row">
                <div class="large-12 columns">
                    <h4>Aggiungi una nuova materia per l'ambito apprendimento.</h4>
    		</div>
            </div>
            <form method="POST" id="add_matter_form" action="<?= $this->actionUrl('addMatter', ["documentId" => $this->documentId]) ?>" onsubmit="return checkMatter();">
                <div class="row">
                    <div class="large-3 medium-3 columns">
                        <label class="bold inline codice" for="materia">*Materia</label>
                    </div>
                    <div class="large-4 medium-4 end columns">
                        <input type="text"  name="matter" id="matter" placeholder="Inserisci" maxLength="45">
                    </div>
                </div>
                <div class="row campiObbligatori">
                    <div class="large-3 medium-3 small-12 columns">
                        <small>*campi obbligatori</small>
                    </div>
                    <div class="large-5 medium-5 small-12 end columns">
                        <a href="javascript:submit('#add_matter_form');" class="button radius with-itocn"><i class="far fa-save"></i>Salva</a>
                        <a href="javascript:closeDialog('#dialog-new_matter');" class="button radius elimina with-itocn"><i class="fa fa-undo"></i>Annulla</a>
                    </div>
                </div>
            </form>
            <a class="close-reveal-modal">×</a>
        </div>
        
        <div id="dialog-find_nodes" class="dialog reveal-modal" data-reveal>
            <iframe id="find_nodes" width="100%" height="500px" style="border: none;"></iframe>
            <a class="close-reveal-modal">×</a>
        </div>
        
        <div id="dialog-new_activity" class="dialog reveal-modal" data-reveal>
        </div>
        
        <div class="content active tab-studente personalizza-pei" id="panelTabStudenti">
            <div class="section-title row-100">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-12 medium-12 columns">
                    <?php if ($this->pei['type'] == DocumentType::PEI){ ?>
                        <h3>Elabora</h3>
                    <?php } else { ?>
                        <h3>
                            Analisi della situazione attuale nelle singole aree e
                            individuazione delle relative possibilità di sviluppo<br/>
                            <small>
                                Obiettivi programmati ed attività proposte in sezione/intersezione
                            </small>
                        </h3>
                    <?php } ?>
                </div>
            </div>
            <div class="section-title row-100 mb-5">
                <div class="large-12 medium-12 columns">
                    <?php if ($this->pei["type"] == DocumentType::PDP_LINGUA){ ?>
                        <a class="button radius nuovo" href="javascript:openDialog('#dialog-new_matter');">
                            <i class="fa fa-plus-circle margin-right"></i> Materia
                        </a>
                    <?php } else if ($this->pei["type"] == DocumentType::PDP || $this->pei["type"] == DocumentType::PEI) { ?>
                        <a class="button radius nuovo separator-right" href="javascript:openDialog('#dialog-find_nodes');">
                            <i class="fa fa-search margin-right"></i> Ricerca
                        </a>
                        <a class="button radius nuovo" href="javascript:openDialog('#dialog-new_matter');">
                            <i class="fa fa-plus-circle margin-right"></i> Materia
                        </a>
                        <a class="button radius nuovo" href="<?= $this->actionUrl("addMatter", ["documentId" => $this->documentId, "matter" => "Tutte le materie"]) ?>">
                            <i class="fa fa-plus-circle margin-right"></i> Tutte le materie
                        </a>
                        <?php if ($this->pei['type'] == DocumentType::PEI) { ?>
                            <a class="button radius nuovo" href="<?= UriService::buildPageUrl("/structure/pei/ProgettiInclusiviAction", "", ['documentId' => $this->documentId]) ?>">
                                <i class="fas fa-paperclip margin-right"></i> Progetti inclusivi
                            </a>
                        <?php } }?>

                        <?php if (UserRoleService::canCurrentUserDo(UserPermission::PEI_RECUPERA_NODO)) { ?>
                            <a class="button radius nuovo float-right color" href="javascript:restoreNodes();">
                                <i class="fas fa-database margin-right"></i> Recupera
                            </a>
                        <?php } ?>
                </div>
            </div>
            
            <div class="row-100" id="pei_container">
                <?php $this->loadPei(); ?>
            </div>
        </div>
        <?php
        $this->footer();
    }

    public function loadDeletedNodes(){
        $stmt = PeiService::findPeiRowWithStatus($this->documentType, $this->pei['user_year_document_id'], PeiRowStatus::DELETED);
        $res = $stmt->fetchAll();

        echo json_encode($res);
        die();
    }
    
    public function loadPei(){
        $rootNodeLabel = "";
        
        $lastSelectedRowId = 0;
        if (isset($_SESSION['selectedPeiRowId'])){
            $lastSelectedRowId = $_SESSION['selectedPeiRowId'];
        }
        
        $this->preparePeiTirocinio();

        $stmt = PeiService::findPeiRowWithStatus($this->documentType, $this->pei['user_year_document_id'], PeiRowStatus::SELECTED);
        
        if (($this->document["type"] == DocumentType::PEI || $this->document["type"] == DocumentType::PDP) && $stmt->rowCount() == 0){?>
            <div class="paddingrightleft" style="margin-left: 15px; margin-top: 20px;">
                <p>
                    Premere sul pulsante “Ricerca” per inserire gli ambiti di lavoro dell’alunno.
                </p>
            </div>
            <?php
            return;
        }
        
        ?>
        <div class="medium-12 columns">
            <div class="personalizza-pei percorso margin-top">
                <table id="review_table">
                    <tbody>
                    <?php
                    foreach ($stmt as $row){
                        if ($row['pr_uri'] == 'apprendimento'){
                            $path = 'APPRENDIMENTO -> ' . $row['title'];
                        } else {
                            $path = PeiService::buildNodePath( $row['pr_uri'], $this->documentType );
                        }
                        
                        $stmt = PeiService::findPeiRowTargets($row['pei_row_id']);
                        $hasDetails = $stmt->rowCount() > 0;
                        
                        ?>
                        <tr id="pei_row_<?= $row['pei_row_id'] ?>">
                            <td>
                                <?php if (!$hasDetails){ ?>
                                <i class="fa fa-exclamation-circle"></i>
                                <?php } ?>
                            </td>
                            <td style="width: 100%;">
                                <a href="<?= UriService::buildPageUrl("/structure/pei/PeiRowAction", "_default", ['peiRowId' => $row['pei_row_id']]) ?>">
                                    <div class="path">
                                        <strong><?= _t($path) ?></strong>
                                    </div>
                                    <div class="activity_description">
                                        <?= _t($row['pn_description']) ?>
                                        <?php if ($this->document["type"] != DocumentType::PEI_TIROCINIO) { ?>
                                            <?= _t($row['uri']) ?>
                                        <?php } ?>
                                    </div>
                                </a>
                                <?php if ($row["last_edit_date"] != null) { ?>
                                    <div>
                                        Modificato il: <?= _t(TextService::formatDate( $row["last_edit_date"]) ) ?>
                                        <?php if (UserRoleService::canCurrentUserDo(UserPermission::LAST_EDIT_USER_VIEW)) { ?>
                                            da <?= _t($row["ue_name"]) ?> <?= _t($row["ue_surname"]) ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </td>
                            <?php if ($this->document["type"] != DocumentType::PEI_TIROCINIO){ ?>
                                <td>
                                    <a href="javascript:changePeiRowStatus(<?= $row['pei_row_id'] ?>, '<?= PeiRowStatus::DELETED ?>');" class="button radius elimina noMargin has-tip tiny" title="elimina il processo" data-tooltip aria-haspopup="true"><i class="<?= Icon::TRASH ?>"></i></a>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
        <?php
    }

    private function preparePeiTirocinio(){
        if ($this->document["type"] != DocumentType::PEI_TIROCINIO){
            return;
        }

        $nodes = EM::execQuery("SELECT * FROM pei_tirocinio_node");
        $rows = PeiService::findPeiRowWithStatus($this->documentType, $this->pei['user_year_document_id'], 'SELECTED');
        if ($nodes->rowCount() == $rows->rowCount()){
            return;
        }

        foreach ($nodes as $node) {
            $position = $this->buildNodeOrder($node['pei_node_id']);
            EM::insertEntity("pei_row", 
                [
                    'pei_id' => $this->pei['user_year_document_id'],
                    'uri' => urldecode($node["uri"]),
                    'status' => PeiRowStatus::SELECTED,
                    'weight' => 0,
                    'position' => $position
                ]);
        }
    }
        
    public function addMatter(){
        if ($this->document["type"] == DocumentType::PEI_TIROCINIO){
            return;
        }

        if (StringUtils::isBlank($this->matter) || strlen($this->matter) > 45){
            return;
        }
        
        $row = [
            'weight' => 0,
            'uri' => 'apprendimento',
            'status' => PeiRowStatus::SELECTED,
            'pei_id' => $this->pei['user_year_document_id'],
            'position' => '7000000',
            'title' => $this->matter,
            "last_edit_date" => "NOW()",
            "last_edit_user_id" => $this->currentUser["user_id"]
        ];
        $rowId = EM::insertEntity("pei_row", $row);

        DocumentService::update($this->pei['user_year_document_id']);

        $_SESSION['selectedPeiRowId'] = $rowId;
        redirect( $this->actionUrl('details', ["documentId" => $this->documentId] ) );
    }
    
    public function addPeiRow(){
        if ($this->document["type"] == DocumentType::PEI_TIROCINIO){
            return;
        }

        if (StringUtils::isBlank($this->uri)){
            return;
        }
        
        $row = PeiService::findPeiRowByUri($this->pei['user_year_document_id'], $this->uri);
        if ($row == NULL){
            $node = PeiService::findNodeByUri($this->uri, $this->documentType);
            $position = $this->buildNodeOrder($node['pei_node_id']);
            EM::insertEntity("pei_row", 
                [
                    'pei_id' => $this->pei['user_year_document_id'],
                    'uri' => urldecode($this->uri),
                    'status' => PeiRowStatus::SELECTED,
                    'weight' => 0,
                    'position' => $position,
                    "last_edit_date" => "NOW()",
                    "last_edit_user_id" => $this->currentUser["user_id"]
                ]);
        } else {
            EM::updateEntity("pei_row", 
                [
                    'status' => PeiRowStatus::SELECTED
                ], 
                sprintf('pei_row_id=%d', $row['pei_row_id']));
        }

        DocumentService::update($this->pei['user_year_document_id']);
    }
        
    private function buildNodeOrder($nodeId){
        $str = '';
        do{
            $node = PeiService::findNode( $nodeId, $this->documentType );
            $str = $node['order'] . $str;
            $nodeId = $node['father_pei_node_id'];
        } while ($nodeId != NULL);
        
        for ($i = strlen($str); $i < 6; $i++){
            $str .= '0';
        }
        
        return $str;
    }
    
    public function deletePeiRow(){
        if (StringUtils::isBlank($this->peiRowId)){
            return;
        }

        if ($this->document["type"] == DocumentType::PEI_TIROCINIO){
            return;
        }
        
        EM::deleteEntity("pei_row", sprintf('pei_row_id=%d AND pei_id=%d', $this->peiRowId, $this->pei['user_year_document_id']));
        $_SESSION['selectedPeiRowId'] = NULL;

        DocumentService::update($this->pei['user_year_document_id']);
    }
     
    public function changePeiRowStatus(){
        $this->changePeiRowStatusNoRedirect();
        redirect(UriService::buildPageUrl("/structure/pei/PeiAction", 'details', ["documentId" => $this->documentId]));
    }

    public function changePeiRowStatusNoRedirect(){
        $peiRow = PeiService::findPeiRow($this->peiRowId);
        if ($peiRow["pei_id"] != $this->documentId){
            redirectWithMessage($this->actionUrl("details", ["peiId" => $this->documentId]), "Dati non validi");
            return;
        }

        EM::updateEntity("pei_row",
            [
                'status' => $this->status,
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser["user_id"]
            ], 
            sprintf('pei_row_id=%d AND pei_id=%d', $this->peiRowId, $this->pei['user_year_document_id'])
        );
        
        DocumentService::update($this->pei['user_year_document_id']);
        $_SESSION['selectedPeiRowId'] = NULL;
    }
}
