<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */
require_once(__DIR__ .'/PeiBaseAction.php');

/**
 * Description of PeiCheckAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once 'PeiAction.php';

class PeiCheckAction extends PeiAction{
    
    public $peiRowTargetId;
    public $status;
    public $note;
    
    public function _default(){
        $terms = DictionaryService::findGroupAsDictionary(Dictionary::PEI_TERM);
        $status = DictionaryService::findGroupAsDictionary(Dictionary::PEI_ROW_TARGET_STATUS);
        
        $stmt = EM::prepare("SELECT *, pn.pei_node_id as pn_pei_node_id, prt.status as prt_status, prt.note as prt_note
                                FROM pei_row_target prt
                                LEFT JOIN pei_row pr ON prt.pei_row_id=pr.pei_row_id
                                LEFT JOIN pei_node pn ON pr.uri=pn.uri
                                WHERE pr.pei_id=:id AND pr.status=:row_status
                                ORDER BY pr.position");
        $stmt->execute(
            [
                'id' => $this->pei['user_year_document_id'],
                'row_status' => PeiRowStatus::SELECTED
            ]);
        
        $this->bradcrumbs->add($this->buildBackBradcrumb());
        $this->bradcrumbs->add(new Bradcrumb("Verifica"));
        $this->header();
        ?>
        <script type="text/javascript">
            var timer = null;
            var oldText = "";
            var textarea = null;
            
            $(function(){
                $('textarea').focusin(function(){
                    textarea = $(this);
                    oldText = $(textarea).val();
                    timer = setInterval(function(){
                        if (oldText != $(textarea).val()){
                            oldText = $(textarea).val();
                            save(textarea);
                        }
                    }, 1000);
                });
                $('textarea').focusout(function(){
                    save(this);
                    clearInterval(timer);
                });
                
                $('.customRadio').on('click', function(){
                    var rowTargetId = $(this).attr('name');
                    var status = $(this).attr('value');
                    
                    var row = $(this).parent().parent();
                    $('.customRadio', row).removeClass('active');
                    $(this).addClass('active');
                    
                    $.post('<?= $this->actionUrl('updateTarget') ?>',
                        {
                            'peiRowTargetId': rowTargetId,
                            'status': status
                        },
                        function(){});
                });
            });
            
            function save(textarea){
                var targetId = $(textarea).attr('name').replace('note_', '');
                var text = $(textarea).val();
                
                $.post('<?= $this->actionUrl('updateTarget') ?>',
                    {
                        'peiRowTargetId': targetId,
                        'note': text
                    },
                    function(){});
            }
        </script>
        
        <div class="content active tab-procedimento-pei" id="panelTabStudenti">
            <div class="section-title row">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-6 medium-4 columns">
                    <h3>Verifica</h3>
                </div>
            </div>
            
            <div class="row">
                <div class="large-12 medium-12 columns">
                    <table id="review_table" class="responsive">
                        <thead>
                            <tr>
                                <th style="width: 350px;">Obiettivo/Attività</th>
                                <?php foreach($status as $key => $value){ ?>
                                <th class="reply"><?= _t($value) ?></th>
                                <?php } ?>
                                <th style="width: 320px;">Note</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($stmt as $row){
                            $path = PeiService::buildNodePath( $row['pr_uri'], $this->documentType );
                            if ($row['pn_pei_node_id'] === NULL){
                                $path = 'Apprendimento -> '. $row['title'];
                            }   
                            ?>
                            <tr>
                                <td>
                                    <span class="path">
                                        <?= _t($path) ?>
                                    </span><br/>
                                    <strong>
                                        <a href="javascript:toggle('#activity_<?= $row['pei_row_target_id'] ?>');">
                                            <?= _t(EncryptService::decrypt( $row['target'] )) ?>
                                        </a>
                                    </strong>
                                    <p id="activity_<?= $row['pei_row_target_id'] ?>" class="activity_description" style="display:none;">
                                        <?= _t(EncryptService::decrypt( $row['activity'] )) ?>
                                    </p>
                                </td>
                                <?php foreach($status as $key => $value){ 
                                    $class = "";
                                    if ($row['prt_status'] == $key){
                                        $class = 'active';
                                    }
                                    ?>
                                    <td class="centered">
                                        <span class="show-for-small"><?= $value ?></span>
                                        <span value="<?= $key ?>" name="<?= $row['pei_row_target_id'] ?>" class="customRadio <?= $class ?>">
                                            <span></span>
                                        </span>
                                    </td>
                                <?php } ?>
                                <td>
                                    <textarea name="note_<?= $row['pei_row_target_id'] ?>" style="width:100%; margin-top: 30px;"><?= _t(EncryptService::decrypt( $row['prt_note'] )) ?></textarea><br/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    public function updateTarget(){
        $target = PeiService::findPeiRowTarget($this->peiRowTargetId);
        if ($target == NULL){
            redirect(UriService::accessDanyActionUrl());
        }
        
        $par = [];
        if (StringUtils::isNotBlank($this->status)){
            $par['status'] = $this->status;
        } 
        if (StringUtils::isNotBlank($this->note)){
            $this->note = str_replace("<br/>", "\n", $this->note);
            $this->note = htmlentities($this->note);
            $par['note'] = EncryptService::encrypt( $this->note );
        } 
        
        if (count($par) == 0){
            return;
        }
        
        EM::updateEntity("pei_row_target", 
                $par,
                sprintf('pei_row_target_id=%d', $this->peiRowTargetId));
    
    }
}
