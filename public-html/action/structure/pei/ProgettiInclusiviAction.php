<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once __DIR__ . '/PeiAction.php';

/**
 * Description of PlanAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ProgettiInclusiviAction extends PeiAction {

    public $attachmentId;

    public function _default(){
        $this->bradcrumbs->add(new Bradcrumb("Studente: " . $this->user['code'], UriService::buildPageUrl("/structure/UserAction", "", ["documentId" => $this->documentId])));
        
        if ($this->pei['type'] == DocumentType::PEI) {
            $this->bradcrumbs->add(new Bradcrumb("Elabora", UriService::buildPageUrl("/structure/pei/PeiAction", "details", ["documentId" => $this->peiId])));
        } else if ($this->pei['type'] == DocumentType::PDP ){
            $this->bradcrumbs->add(new Bradcrumb("Analisi", UriService::buildPageUrl("/structure/pei/PeiAction", "details", ["documentId" => $this->peiId])));
        }

        $this->bradcrumbs->add(new Bradcrumb("Progetti inclusivi"));
        
        $this->header();
        ?>
        <div class="content active tab-studente personalizza-pei row" id="panelTabStudenti">
            <div class="section-title row-100">
                <?php $this->bradcrumbs->draw(); ?>
                <div class="large-12 medium-12 columns">
                    <h3>Progetti inclusivi</h3>
                </div>
            </div>
            <div class="sezioni">
                <?php 
                $form = new Form($this->actionUrl());
                $form->displaySubmit = false;

                $schoolYears = SchoolYearService::findAll();
                foreach($schoolYears as $sy){
                    if ($sy["current"]){
                        $this->attachmentField($form, "Progetti inclusivi " . $sy["school_year"], 
                            $this->buildAttachmentType(AttachmentType::pei_progetti_inclusivi, $sy["school_year_id"]),
                            true, false); 
                    } else {
                        $attachments = AttachmentService::findAllForEntity(EntityName::USER, $this->user["user_id"], $this->buildAttachmentType(AttachmentType::pei_progetti_inclusivi, $sy["school_year_id"]));
                        if ($attachments->rowCount() > 0){
                            $this->attachmentField($form, $sy["school_year"], $this->buildAttachmentType(AttachmentType::pei_progetti_inclusivi, $sy["school_year_id"]), false, true);
                        }
                    }
                }

                $attachmentsBase = AttachmentService::findAllForEntity(EntityName::USER, $this->user["user_id"], AttachmentType::pei_progetti_inclusivi);
                if ($attachmentsBase->rowCount() > 0){
                    $this->attachmentField($form, "Progetti inclusivi", AttachmentType::pei_progetti_inclusivi, false, true);
                }
                
                $form->draw();
                ?>
            </div>
        </div>
        <?php
        $this->footer();
    }

    private function buildAttachmentType($documentName, $schoolYearId){
        $type = $documentName . "_sy_". $schoolYearId;
        return $type;
    }

    private function attachmentField($form, $label, $name, $canRemove, $readOnly = false){
        $attachmentField = new FormAttachmentsField($label, $name, EntityName::USER, $this->user["user_id"], false);
        $attachmentField->removeUrl = $this->actionUrl("removeAttachment");
        $attachmentField->noTemp = true;
        $attachmentField->canRemove = $canRemove;
        $attachmentField->readOnly = $readOnly;
        $attachmentField->extensions = ["pdf"];
        $form->addField($attachmentField);
    }
    
    public function removeAttachment(){
        $attachmentEntities = AttachmentService::findAttachmentEntityForAttachmentId($this->attachmentId);
        $attachmentEntity = $attachmentEntities->fetch();
        if ($attachmentEntities == null){
            return;
        }

        if ($attachmentEntity["entity_id"] != $this->user["user_id"]){
            return;
        }

        AttachmentService::deleteAttachmentAndAttachmentEntity($this->attachmentId);

        PeiService::updatePeiStatus($this->document["user_year_document_id"]);
    }
    
    public function getAttachment(){
        if (StringUtils::isBlank($this->fileName)){
            redirectWithMessage($this->actionUrl("_default"), "File non specificato.");
        }
            
        $filePath = UriService::buildUserAttachmentPath($this->user['user_id']) .'/'. $this->type .'/'. $this->fileName;
        if (!file_exists($filePath)){
            redirectWithMessage($this->actionUrl("_default"), "File non valido.");
        }
        
        if ($this->method != 'inline' && $this->method != 'attachment'){
            redirectWithMessage($this->actionUrl("_default"), "Metodo non valido.");
        }
        
        $fileContent = EncryptService::decryptfileInMemory($filePath);
        
        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public");
        //header("Content-Type: ". $attachment["mime_type"]);
        
        $mimeType = "";
        $extension = substr($this->fileName, strpos($this->fileName, "."));
        switch ($extension){
            case ".pdf": $mimeType = "application/pdf"; break;
            case ".doc": $mimeType = "application/msword"; break;
            case ".docx": $mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; break;
            default: $mimeType = "application/octet-stream"; break;
        }
        if (StringUtils::isNotBlank($mimeType)){
            header("Content-Type: ". $mimeType);
        }
        
        header("Content-Transfer-Encoding: Binary");
        //header("Content-Length:" . $attachment['size']);
        header("Content-Disposition: " . $this->method ."; filename=" . $this->fileName);
         
        echo $fileContent;
        die();
    }
}