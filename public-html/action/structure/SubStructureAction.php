<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SubStructureAction extends StructureAction{

    public $id;

    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());

        $table = new Table();
        $table->entityName = "structure";
        $table->addWhere(new TableCondition("parent_structure_id=:structure_id", ":structure_id", $this->structureId));
        $table->addWhere(new TableCondition("enabled=:enabled", ":enabled", 1));
        $table->addOrder("name");
        $table->addColumn(new TableColumn("structure_id", "ID"));
        $table->addColumn(new TableColumn("name", "Nome"));
        $table->addColumn(new TableColumn("meccanografico", "Meccanografico"));
        $table->addColumn(new TableColumn("address", "Indirizzo"));
        $table->addColumn(new TableColumn("students_num", "Numero studenti"));
        $table->addColumn(new TableColumn("structure_id", "", function($v){
            $out = Formatter::buildTool("", $this->actionUrl("edit").'?id='.$v, "fas fa-pencil-alt");
            $out .= Formatter::buildTool("", $this->actionUrl("remove").'?id='.$v, "fas fa-trash-alt", "Rimuovere i dati?");
            return $out;
        }));

        $this->header();
        ?>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row-100">
                    <div class="large-8 medium-7 columns">
                        <h3>Plessi</h3>
                    </div>
                    <div class="large-4 medium-3 columns">
                        <a href="<?= $this->actionUrl("edit") ?>" class="button radius nuovo float-right">
                            <i class="fa fa-plus-circle margin-right"></i> Nuovo
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $table->draw();

        $this->footer();
    }

    public function remove(){
        if(StringUtils::isBlank($this->id)){
            redirect($this->actionUrl("_default"));
        }
        
        $structure = StructureService::find($this->id);
        if ($structure == null || $structure['parent_structure_id'] != $this->structure['structure_id']){
            redirect(UriService::accessDanyActionUrl());
        }
        
        EM::updateEntity("structure", ["enabled" => 0], ['structure_id' => $this->id]);
        redirectWithMessage($this->actionUrl("_default"), "Plesso rimosso");
    }

    public function edit(){
        $form = new Form($this->actionUrl("edit", ["id" => $this->id]));
        $form->cancellAction = $this->actionUrl("_default");
        $form->entityName = "structure";
        
        $form->entity = StructureService::find($this->id);
        
        $nameField = new FormTextField("Nome", "name", FormFieldType::STRING, true, "", ["maxlength" => 255]);
        $nameField->checkFunction = function($field, $value){
            if (StringUtils::isBlank($value)){
                return;
            }

            $dbStructure = StructureService::findByName($value);
            if ($dbStructure != null && $dbStructure["structure_id"] != $this->id){
                $field->addFieldError("Il nome è in uso per " . $dbStructure["name"]);
            }
        };
        $form->addField($nameField);

        $meccanograficoField = new FormTextField("Meccanografico", "meccanografico", FormFieldType::STRING, true, "", ["maxlength" => 100]);
        $meccanograficoField->checkFunction = function($field, $value){
            if (StringUtils::isBlank($value)){
                return;
            }

            $dbStructure = StructureService::findByMeccanografico($value);
            if ($dbStructure != null && $dbStructure["structure_id"] != $this->id){
                $field->addFieldError("Il meccanografico è in uso per " . $dbStructure["name"]);
            }
        };
        $form->addField($meccanograficoField);

        $form->addField(new FormTextField("Indirizzo", "address", FormFieldType::STRING, false, "", ["maxlength" => 255]));
        $form->addField(new FormTextField("Numero alunni plesso", "students_num", FormFieldType::NUMBER, false, "", ["maxlength" => 6]));
        
        if ($form->isSubmit() && $form->checkFields()){
            if(StringUtils::isBlank($this->id)){
                $this->id = $form->persist("", [
                    "insert_date" => "NOW()",
                    "insert_user_id" => $this->currentUser['user_id'],
                    "parent_structure_id" => $this->structure["structure_id"],
                    "enabled" => true
                ]);
            } else {
                $form->persist(new TableCondition("structure_id=:structure_id", ":structure_id", $this->id));
            }
            
            EM::updateEntity("structure", [
                "last_edit_date" => "NOW()",
                "last_edit_user_id" => $this->currentUser['user_id']
            ], ["structure_id" => $this->id]);
            
            redirectWithMessage($this->actionUrl("_default"), "Progetto salvato");
        }
        
        $this->header();
        ?>
        <script type="text/javascript">
            $(function(){
               $('#ed_salute_type').on('change', function(){
                   edSaluteTypeChange();
               });
               edSaluteTypeChange();
               
               function edSaluteTypeChange(){
                   var val = $('#ed_salute_type').val();
                   $('#campo_form_type_other').hide();
                   if (val == 'altro'){
                        $('#campo_form_type_other').show();    
                   }
               }
            });
        </script>
        <div class="tabs-content user-ref">
            <div class="content operatori active" id="panel2-2">
                <div class="section-title row">
                    <div class="large-10 medium-9 small-7 columns">
                        <h3>Modifica plesso</h3>
                    </div>
                </div>
                <div class="table-row row">
                    <div class="large-12 columns">
                        <?php
                        $form->draw();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }
}