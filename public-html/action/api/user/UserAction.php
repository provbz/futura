<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once __DIR__ . '/../ApiBaseAction.php';

/**
 * Description of UserAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
 class UserAction extends ApiBaseAction{

    public $token = '';

    public function _default(){
        if (StringUtils::isBlank($this->token)){
            $this->badRequest("invalid token");
            return;
        }

        $user = UserService::findByToken($this->token);
        if ($user == null){
            $this->notFound();
            return;
        }

        if (!UserService::isOnline($user)){
            $this->badRequest("invalid token");
            return;
        }

        UserService::CreateAndGetToken($user["user_id"]);
        $response = [
            "email" => $user["email"],
            "name" => $user["name"],
            "surname" => $user["surname"],
            "roles" => [],
            "structures" => []
        ];

        $roles = UserRoleService::findUserRole($user["user_id"]);
        foreach($roles as $role){
            $response["roles"][] = [
                "role_id" => $role["role_id"],
                "level" => $role["level"],
                "name" => $role["name"],
            ];
        }

        $rootStructures = StructureService::findUserStructuresRoot($user["user_id"]);
        foreach ($rootStructures as $rs) {
            $structureRole = $this::findUserStructureRole($user["user_id"], $rs["structure_id"]);

            $structureResponse = [
                "structure_id" => $rs["structure_id"],
                "name" => $rs["name"],
                "meccanografico" => $rs["meccanografico"],
                "roles" => $structureRole->fetchAll(),
                "structures" => []
            ];

            $subStructures = StructureService::findAllWithParent($rs["structure_id"]);
            foreach($subStructures as $ss){
                $ssRoles = $this::findUserStructureRole($user["user_id"], $ss["structure_id"]);
                $subStructureResponse = [
                    "structure_id" => $ss["structure_id"],
                    "name" => $ss["name"],
                    "meccanografico" => $ss["meccanografico"],
                    "roles" => $ssRoles->fetchAll()
                ];

                $structureResponse["structures"][] = $subStructureResponse;
            }
            
            $response["structures"][] = $structureResponse;
        }
        

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
    }

    private function findUserStructureRole($userId, $structureId){
        return EM::execQuery("SELECT r.role_id, r.level, r.name
            FROM user_structure us
            LEFT JOIN user_structure_role usr ON us.user_structure_id = usr.user_structure_id
            LEFT JOIN role r ON usr.role_id = r.role_id
            WHERE us.structure_id = :structure_id 
                AND us.user_id = :user_id", [
                    "structure_id" => $structureId,
                    "user_id" => $userId
                ]);
    }
}