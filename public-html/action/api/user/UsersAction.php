<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

require_once __DIR__ . '/../model/ItemsResponse.php';

class Request extends RequestBaseClass{
    public $first = 0;
    public $limit = 100;
    public $conditions = [];
}

/**
 * Description of UsersAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
 class UsersAction extends ApiBaseAction{

    public function _default(){
        if ($this->getRequestMethod() != RequestMethod::POST){
            $this->notFound();
        }

        $request = new Request();
        $request->set($this->getBodyRequest());
        
        if ($request->first < 0){
            $this->badRequest("First: valore minimo 0");
        }

        if ($request->limit > 100){
            $this->badRequest("limit: valore massimo 100");
        }

        $response = new ItemsResponse();
        
        $countQueryData = $this->buildQuery($request, true);
        $countRes = EM::execQuerySingleResult($countQueryData["query"], $countQueryData["params"]);
        $response->count = $countRes["c"];

        $queryData = $this->buildQuery($request);
        $users = EM::execQuery($queryData["query"], $queryData["params"]);

        $rows = [];
        foreach ($users as $user) {
            $row = [
                "user_id" => $user["user_id"],
                "email" => $user["email"],
                "name" => $user["name"],
                "surname" => $user["surname"]
            ];

            $rows[] = $row;
        }
        $response->items = $rows;

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
    }

    private function buildQuery($request, $isCount = false){
        $params = [];

        $query = "SELECT ";
        if ($isCount){
            $query .= "COUNT(*) as c ";
        } else {
            $query .= "u.user_id, u.email, u.name, u.surname ";
        }
        
        $query .= "FROM user u ";
        
        $query .= "WHERE email is not NULL
            AND u.status = :user_enabled ";

        if (count((array) $request->conditions) > 0){
            $cond = "";
            foreach($request->conditions as $key => $value){
                $cond .= $cond == "" ? "" : " AND ";

                switch($key) {
                    case "meccanografico":
                        $cond .= "(s.meccanografico = :meccanografico)";
                        $params["meccanografico"] = $value;
                        break;
                    case "query": 
                        $cond .= "(u.name like :q1 OR u.surname like :q2)";
                        $params["q1"] = "%" . $value . "%";
                        $params["q2"] = "%" . $value . "%";
                        break;
                    case "role_id":
                        $cond .= "(ur.role_id = :r1 OR usr.role_id = :r2)";
                        $params["r1"] = $value;
                        $params["r2"] = $value;
                        break;
                    default : 
                        $cond .= "(1 <> 1)";
                        break;
                };
            }

            if ($cond != ""){
                $query .= " AND u.user_id IN (
                            SELECT DISTINCT(u.user_id)
                            FROM user u
                            LEFT JOIN user_role ur ON ur.user_id = u.user_id
                            LEFT JOIN user_structure us ON u.user_id = us.user_id
                            LEFT JOIN structure s ON s.structure_id = us.structure_id
                            LEFT JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id
                            WHERE {$cond}
                        )";
            }
        }

        $query .= " ORDER BY surname, name ";

        $params["user_enabled"] = UserStatus::ENABLED;
        
        if (!$isCount){
            $query .= "LIMIT :first, :limit";
            $params["first"] = $request->first;
            $params["limit"] = $request->limit;
        }

        return [
            "query" => $query,
            "params" => $params
        ];
    }

}