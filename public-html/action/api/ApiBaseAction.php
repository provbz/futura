<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ApiBaseAction {

    protected function getBodyRequest(){
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        return $data;
    }

    protected function getRequestMethod(){
        return $_SERVER['REQUEST_METHOD'];
    }

    protected function ok($message = ""){
        http_response_code(200);
        echo $message;
        die();
    }

    protected function badRequest($message = ""){
        http_response_code(400);
        echo $message;
        die();
    }

    protected function notFound($message = ""){
        http_response_code(404);
        echo $message;
        die();
    }
}