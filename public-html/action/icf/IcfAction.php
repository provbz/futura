<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of IcfAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class IcfAction extends Page {
    
    const CF_FILE = "data/ICFCY-EN-IT.xml";
    
    public $id;
    public $operation;
    public $term;
    
    public $search_query;
    
    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());
        
        $this->addScriptFile("jstree/jstree.js");
        $this->addStyleFile("jstree/themes/default/style.min.css");
        $this->addStyleFile("jstree.css");
        
        $this->header();
        ?>
        <script type="text/javascript">
            var appUrl = "";
            $(function () {
                <?php if (StringUtils::isBlank($this->search_query)){ ?>
                $("#jstree")
                    .on('changed.jstree', function (e, data) {
                        var i, j, select;
                        for(i = 0, j = data.selected.length; i < j; i++) {
                            select = data.instance.get_node(data.selected[i]).id.replace("node_","");
                            openDetails(select);
                            return;
                        }
                    })
                    .jstree({
                        "core" : {
                            "multiple" : false,
                            "check_callback" : true,
                            "themes" : { 
                                "variant" : "large"
                            },
                            'data' : {
                                'url' : "<?= $this->actionUrl("tree");?>",
                                'data' : function (n) {
                                    $('body').css('overflow', 'auto');
                                    return { 
                                        "operation" : "get_children", 
                                        "id" : n.id ? n.id.replace("node_","") : '' 
                                    }; 
                                }}
                        },
                        "types" : {
                            "folder" : {
                                
                            },
                            "leaf" : {
                                //"icon" : "<?= UriService::buildImageUrl('file.png') ?>"
                            }
                        },
                        "plugins" : [ 
                            
                        ]
                    });
                <?php } ?>
                    
                $( "#search_query" ).autocomplete({
                    source: "<?= $this->actionUrl("treeSearch") ?>",
                    minLength: 2,
                    select: function( event, ui ) {
                        if (ui.item){
                            $('#search_form').submit();
                        }
                    }
                });
                
                if ($('.search_result').length > 0){
                    $('.search_result').on('click', function(){
                        $('.search_result').removeClass('selected');
                        $(this).addClass("selected");
                        openDetails($(this).attr('code'));
                    });
                    
                    $('.search_result').first().trigger('click');
                }
            });
            
            function openDetails(id){
                loadDiv("#node_details", "<?= $this->actionUrl("details");?>?id=" + id);
            }
        </script>
        <div class="content icf-cy active" id="panel2-3">
            <div class="section-title row">
                <div class="large 12 columns">
                    <h3>Classificazione Internazionale del Funzionamento, della Disabilità e della Salute</h3>
                    
                    <div class="large-4 columns">
                        <form id="search_form" action="<?= $this->actionUrl() ?>"> 
                            <label class="search-wrap">
                                <span class="fa fa-search"></span>
                                <input id="search_query" name="search_query" value="<?= $this->search_query ?>" class="search-box" type="text" placeholder="Cerca per codice o parola chiave">
                            </label>
                        </form>
                    </div>
                    <?php if (StringUtils::isNotBlank($this->search_query)){ ?>
                    <div class="large-8 columns small-newimport">
                        <a href="<?= $this->actionUrl("_default") ?>" class="button radius nuovo"><i class="fa fa-angle-left"></i>Torna all'elenco principale</a>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
                <div class="large-5 medium-5 columns">
                    <?php if (StringUtils::isNotBlank($this->search_query)){ ?>
                        <ul class="elenco studenti icycf">
                            <?php $this->search(); ?>
                        </ul>
                    <?php } else { ?>
                        <div id="jstree"></div>
                    <?php } ?>
                </div>
                <div id="node_details" class="large-7 medium-7 columns">
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    public function search(){
        $stmt = EM::prepare("select * from icf where name like :search OR icf_id like :search2 LIMIT 0, 15");
        $stmt->execute(['search' => '%'. $this->search_query . '%', 'search2' => '%'. $this->search_query .'%']);
        foreach ($stmt as $result){ ?>
        <li>
            <a class="search_result" code="<?= $result['icf_id'] ?>">
                <?= _t($result['icf_id']) ?> <?= _t($result['name']) ?>
            </a>
        </li>
        <?php }
    }
    
    public function treeSearch(){
        header('Content-type: application/json');
        $reply = [];
        
        $query = sprintf("select * from icf where name like'%%%s%%' OR icf_id like'%%%s%%' LIMIT 0, 15",
                    ($this->term), ($this->term));
        $result = EM::execQuery($query);
        
        foreach ($result as $row){
            $label = $row['icf_id'] .": ".$row['name'];
            
            $reply[] = [ 
                "value" => $row['icf_id'],
                "label" => $label
            ];      
        }
        
        echo json_encode($reply);
    }
    
    public function tree(){
        header('Content-type: application/json');
        $reply = [];
        if ($this->operation == "get_children"){
            if ($this->id == '#'){
                $this->id = ''; 
            }
            
            $stmt = EM::prepare("SELECT * FROM icf WHERE parent_icf_id=:parent_icf_id");
            $stmt->execute(['parent_icf_id' => $this->id]);
            foreach ($stmt as $row){
                $numChildren = $this->hasChildren($row["icf_id"]);
                $type = "folder";
                if ($numChildren == 0){
                    $type = "leaf";
                }
                
                $text = $row["icf_id"] .': '. $row["name"];
                if (strlen($text) > 56){
                    $i = 0;
                    for ($i = 56; $i > 0; $i--){
                        if ($text[$i] == ' '){
                            break;
                        }
                    }
                    $text = substr($text, 0, $i);
                    
                    $text .= '...';
                }
                
                $reply[] = [
                        "id" => "node_" . $row["icf_id"],
                        "text" => $text,
                        "type" => $type,
                        "state" => [
                            "opened"    => false,
                            "disabled"  => false,
                            "selected"  => false
                        ],
                        "children" => $numChildren > 0
                ];       
            }
        }
        
        echo json_encode($reply);
    }
    
    private function hasChildren($nodeId){
        $stmt = EM::prepare("SELECT COUNT(*) as c FROM icf WHERE parent_icf_id=:parent_icf_id");
        $stmt->execute(['parent_icf_id' => $nodeId]);
        $res = $stmt->fetch();
        return $res['c'];
    }
    
    public function details(){
        if (StringUtils::isBlank($this->id)){
            return;
        }
        
        $icf = IcfService::find($this->id);
        ?>
        <div class="contenutotesto">
            <h3><?= _t($icf['icf_id']) ?>: <?= _t($icf['name']) ?></h3>
            <?php
            $metadataKey = MetadataService::findDictionaryForEntity("icf");

            $stmt = EM::prepare("SELECT * FROM icf_metadata WHERE icf_id=:icf_id");
            $stmt->execute(['icf_id' => $this->id]);
            $metadataGroup = [];

            foreach ($stmt as $row){
                $label = $metadataKey[$row['metadata_id']]['name'];
                if (!isset( $metadataGroup[$label] ) ){
                    $metadataGroup[$label] = [];
                }
                $metadataGroup[$label][] = $row['value'];
            }
            foreach($metadataGroup as $key => $value ){
                ?>
                <p>
                    <strong>
                        <?= _t($key) ?>
                    </strong>
                    <br/>
                    <?php foreach ($value as $k => $v2) { ?>
                        <?= _t($v2) ?><br/>
                    <?php } ?>
                </p>
                <?php
            }

            $query = sprintf("SELECT * FROM icf_modifier WHERE apply_to_codes like '%%%s%%'",
                                ($this->id));
            $resultModifier = EM::execQuery($query);
            foreach ($resultModifier as $modifier){
                ?>
                <p>
                    <strong>
                        <?= _t($modifier['title']) ?>
                    </strong><br/>
                    <?= _t($modifier['definition']) ?>
                    <?php
                        $dictionary = DictionaryService::findGroup($modifier['icf_modifier_id']);
                        echo '<ul>';
                        foreach($dictionary as $row){
                            echo '<li>'.$row['key']. ' ' . $row['value'] .'</li>';
                        } 
                        echo '</ul>';
                    ?>
                </p>
                <?php
            }
            ?>
        </div>
        <?php
    }
    
    public function definition(){
        if (StringUtils::isBlank($this->id)){
            echo 'Codice non valido.';
            return;
        }
        
        $icfCode = IcfService::find($this->id);
        if ($icfCode == NULL){
            echo 'Codice non valido.';
            return;
        }
        $metadata = MetadataService::findDictionary("icf", $this->id);
        ?>
        <strong><?= _t($icfCode['name']) ?></strong></br>
        <?= ReflectionUtils::getValue($metadata, 'ICF_DESCRIPTION') ?>
        <?php
    }
    
    public function import(){
        if ( ! file_exists(IcfAction::CF_FILE)) {
            echo 'file non trovato.';   
            return;
        }
        
        $xml = simplexml_load_file(IcfAction::CF_FILE);
        
        foreach ($xml->xpath('//Class[@kind="component"]') as $key){
            echo '<div style="margin-bottom: 10px;">';
            $a = $key->attributes();
            $code = $a['code'];
            $this->importNode($xml, $code, '');
            echo '</div>';
        }
    }
    
    private function importNode($xml, $code, $parent = NULL){
        foreach ($xml->xpath('//Class[@code="' . $code . '"]') as $key){
            set_time_limit(60);
            
            echo '<div style="margin-bottom: 10px; margin-left:10px;">';
            $a = $key->attributes();
            
            $code = (string) $a['code'];
            $name = "";
            $definitions = [];
            $notes = [];
            $remarks = [];
            $exclusions = [];
            $inclusions = [];
            
            foreach( $key->Rubric as $r){
                $value = $r->Label[1]->asXML();
                $at = $r->attributes();
                
                $value = str_replace('<Label xml:lang="it">', '', $value);
                $value = str_replace('</Label>', '', $value);
                
                if ($at['kind'] == 'preferred'){
                    $name = $value;
                } else if ($at['kind'] == 'definition'){
                    array_push($definitions, $value);
                } else if ($at['kind'] == 'note'){
                    array_push($notes, $value);
                } else if ($at['kind'] == 'remark'){
                    array_push($remarks, $value);
                } else if ($at['kind'] == 'exclusion'){
                    array_push($exclusions, $value);
                } else if ($at['kind'] == 'inclusion'){
                    array_push($inclusions, $value);
                }
            }
            $name = $this->prepareText($name);
            echo $code .' '. $name .'<br/>';
            
            EM::replaceEntity("icf", ["icf_id" => $code, 'name' => $name, 'parent_icf_id' => $parent ]);
            $this->insertMetadata($code, 'ICF_DESCRIPTION', $definitions);
            $this->insertMetadata($code, 'ICF_NOTE', $notes);
            $this->insertMetadata($code, 'ICF_REMARK', $remarks);
            $this->insertMetadata($code, 'ICF_EXLUSION', $exclusions);
            $this->insertMetadata($code, 'ICF_INCLUSION', $inclusions);
            
            foreach( $key->SubClass as $r){
                $sna = $r->attributes();
                $this->importNode($xml, $sna['code'], $code);
            }
            
            echo '</div>';
        }
    }
    
    private function insertMetadata($icfCode, $metadataId, $values){
        $p = 0;
        
        foreach ($values as $v){
            echo $icfCode .': '. $metadataId .' '. $v . '<br/>';
            $str = (string) $v;
            $str = $this->prepareText($str);
            echo $str;
            EM::replaceEntity("icf_metadata", [
                'icf_id' => $icfCode,
                'metadata_id' => $metadataId,
                'value' => $str,
                'position' => $p
            ]);
            
            $p++;
        }
    }
    
    private function prepareText($str){
        $str = str_replace("E'", "È", $str);
        $str = str_replace("A'", "À", $str);
        return $str;
    }
}