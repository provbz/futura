<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of IndicazioniProvincialeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class IndicazioniProvincialeAction extends Page{

    public $structure;
    
    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());
        $this->header();
        
        ?>
        <div class="tabs-content">
            <div class="content operatori active">
                <div class="section-title row-100">
                    <div class="large-10 medium-9 small-7 columns">
                        <h3>Indicazioni provinciali</h3>
                    </div>
                </div>
            
                <div class="row-100 padding" >
                    <div class="large-12 columns">
                        <ul>
                            <li><a target="_blank" href="/files/Indicazioni-infanzia-2.pdf">Indicazioni provinciali Scuola dell’Infanzia</a></li>
                            <li><a target="_blank" href="/files/Indicazioni provinciali I ciclo.pdf">Indicazioni provinciali Primo ciclo</a></li>
                            <li><a target="_blank" href="/files/Istituti tecnici.pdf">Indicazioni Istituti tecnici</a></li>
                            <li><a target="_blank" href="/files/Licei.pdf">Indicazioni Licei</a></li>
                            <li><a target="_blank" href="/files/Indicazioni Istruzione professionale.pdf">Indicazioni Istruzione professionale</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="section-title row-100">
                    <div class="large-10 medium-9 small-7 columns">
                        <h3>Privacy</h3>
                    </div>
                </div>

                <div class="row-100 padding" >
                    <div class="large-12 columns">
                        <ul>
                            <li><a target="_blank" href="/files/autorizzazione_invio_documentazione.pdf">Autorizzazione invio documentazione</a></li>
                            <li><a target="_blank" href="/files/Informativa_inclusione_3.doc">Informativa inclusione</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php
        
        $this->footer();
    }
    
    protected function prepareMenu(){
        $this->structureMenu();
    }
}
