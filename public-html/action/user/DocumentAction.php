<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DocumentAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DocumentAction extends Page{
    
    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());
        $this->header();
        ?>
        <div class="content active">
            <div class="section-title row">
                <div class="large-6 medium-4 columns">
                    <h3>Documenti utili</h3>
                </div>
            </div>
            
            <div class="row ">
                <div class="small-12 columns elenco_documento">
                    <i class="fa fa-file-pdf-o"></i>
                    Video introduttivo.
                </div>
                <div class="small-12 columns centered">
                    <iframe width="560" height="315" src="//www.youtube.com/embed/_cP0IGPL6RY" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="row ">
                <div class="small-12 columns elenco_documento">
                    <a target="_blank"  href="/files/SOFIA_guida.pdf">
                        <i class="fa fa-file-pdf-o"></i>
                        Guida all'utilizzo del sistema.
                    </a>
                </div>
            </div>
            <div class="row ">
                <div class="small-12 columns elenco_documento">
                    <a target="_blank"  href="/files/Regolamento Privacy SOFIA.pdf">
                        <i class="fa fa-file-pdf-o"></i>
                        Regolamento privacy.
                    </a>
                </div>
            </div>
            <div class="row ">
                <div class="small-12 columns elenco_documento">
                    <a target="_blank"  href="/files/Nomina Responsabile Esterno del Trattamento.pdf">
                        <i class="fa fa-file-pdf-o"></i>
                        Nomina responsabile esterno.
                    </a>
                </div>
            </div>
            <div class="row ">
                <div class="small-12 columns elenco_documento">
                    <a target="_blank" href="/files/Informativa privacy.pdf">
                        <i class="fa fa-file-pdf-o"></i>
                        Informativa privacy.
                    </a>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
}
