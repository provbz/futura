<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ChangePassword
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ChangePasswordAction extends Page{
    
    public $password1;
    
    public function _default(){
        $this->header();
        
        $form = new Form($this->actionUrl());
        require_once './template/user/UserFormTemplate.php';
        $form->addField(UserFormTemplate::buildPasswordField(NULL));
        $form->addField(UserFormTemplate::buildPassword2Field());
        
        if ($form->isSubmit() && $form->checkFields()){
            UserService::updatePassword($this->currentUser['user_id'], $this->password1);
            redirect(UriService::buildPageUrl("/public/LoginAction"));
        }
        
        $this->header();
        ?>
        <div class="intestazione">
            <h2>Definizione nuova password di accesso</h2>
            <p>
                I dati contenuti in <?= Constants::$APP_PRINT_NAME ?> sono considerati sensibili.
                Per questo motivo è necessario procedere con la definizione di una nuova password ogni 3 mesi.
            </p>
        </div>
        <?php 
        $form->draw();
        
        $this->footer();
    }
}
