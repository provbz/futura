<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of HomeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PrivacyAction extends Page {

    public $structure;
    
    public function _default(){
        $this->header();
        ?>
        <script type="text/javascript">
            function next(){
                var errors = 0;
                errors += check('c1');
                if (errors == 0){
                    self.location = '<?= $this->actionUrl("confirm") ?>';
                }
            }
            
            function check(name){
                if ($('input[name=' + name + ']:checked').val() != 'A'){
                    $('#' + name + '_errors').show();
                    return 1;
                } else {
                    $('#' + name + '_errors').hide();
                }
                return 0;
            }
        </script>
        </script>
        <div class="tabs-content">
            <div class="intestazione">
                <h2>Approvazione delle condizioni d'uso</h2>
            </div>
            
            <div class='block'>
                <iframe src="/files/privacy_1.pdf" style="width:100%; height: 500px;"></iframe>
                <div class="large-4 medium-4 columns">
                    <div class="radio_list">
                        <input type="radio" name="c1" id="c1_0" value="A"> 
                        <label for="c1_0">Acconsento</label>
                        <input type="radio" name="c1" id="c1_1" value="NA"> 
                        <label for="c1_1">Non acconsento</label>
                        <small id='c1_errors' style="display:none;" class='error'>Approvare le condizioni per procedere con la registrazione.</small>
                    </div>
                </div>
                <div class="right">
                    <a target="_blank" href="/files/privacy_1.pdf">
                        <i class="fa fa-print"></i>
                    </a>
                </div>
            </div>
            
            <div class="row campiObbligatori" style="clear:both; margin-top:20px;">
                <div class="large-3 medium-3 columns">
                    <p>*campi obbligatori</p>
                </div>
                <div class="large-5 medium-5 end columns">
                    <a href="javascript:next();" class="button radius salva"><i class="fa fa-floppy-o"></i>Avanti</a>
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
    public function confirm(){
        EM::updateEntity("user", ['privacy_version' => 1], "user_id=".$this->currentUser['user_id']);
        redirect("/");
    }
}