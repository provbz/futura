<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EditProfileAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EditProfileAction extends Page{
    
    public $password_1;
    public $structure;
    
    public function _default(){
        $this->menu->setSelectedMenuItem($this->actionUrl());
        
        $form = new Form($this->actionUrl());
        $form->entityName = "user";
        $form->cancellAction = "/";
        $form->entity = $this->currentUser;
        
        $tf = new FormTextField("Email", "email", FormFieldType::STRING);
        $tf->readOnly = true;
        $form->addField($tf);
        $form->addField(new FormTextField("Nome", "name", FormFieldType::STRING, true));
        $form->addField(new FormTextField("Cognome", "surname", FormFieldType::STRING, true));
        $form->addField(new FormHtmlField('
            <h4>Password locale</h4>
            <div class="columns small-12">
                <p>La password inserita ha valenza solo per l´accesso con le credenziali Futura (Login locale), 
                non se si sceglie di accedere utilizzando il pulsante “Accedi con credenziali LASIS”.</p>
            </div>
        '));

        $password = new FormSinglePasswordField("Password", "password_1", FormFieldType::STRING);
        $password->persist = false;
        $password->note = "Inserire la password solo se si desidera modificarla.";
        $password->checkFunction = function($field, $password){
            if (StringUtils::isBlank($password)){
                return;
            }
            if (!StringUtils::checkPassword($password)){
                $field->addFieldError("La password deve essere almeno 8 caratteri, deve contenere lettere maiuscole e minuscole, numeri e caratteri non alfanumerici.");
            }
            if (!UserService::checkLastPassword($this->currentUser, $password)){
                $field->addFieldError("La password inserita coincide con una delle ultime tre utilizzate");
            }
        };
        $form->addField($password);    
                
        $password2 = new FormSinglePasswordField("Conferma password", "password_2", FormFieldType::STRING);
        $password2->persist = false;
        $password2->checkFunction = function($field, $value){
            $password = getRequestValue("password_1");
            if (StringUtils::isBlank($password)){
                return;
            }
            if ($password != $value){
                $field->addFieldError("Le password non coincidono.");
            }
        };
        $form->addField($password2);
        
        if ($form->isSubmit() && $form->checkFields()){
            $form->persist("user_id=". $this->currentUser['user_id']);
            
            $password = getRequestValue('password_1');
            $par = [];
            if (StringUtils::isNotBlank($password)){
                UserService::updatePassword($this->currentUser['user_id'], $this->password_1);
                EM::updateEntity("user", $par, sprintf("user_id=%d", $this->currentUser['user_id']));
            }
            
            redirect("/");
        }
        
        $this->header(true, null, "row-100");
        ?>
        <div class="content active">
            <div class="section-title">
                <div class="large-12 medium-12 columns">
                    <h3>Modifica profilo</h3>
                </div>
            </div>
        
            <div class="row-100">
                <div class="large-12 columns">
                    <?php $form->draw(); ?>            
                </div>
            </div>
        </div>
        <?php
        $this->footer();
    }
    
}
