<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of CurriculaAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
require_once './action/taxonomy/PeiTreeAction.php';

class PeiCurriculaAction extends PeiTreeAction{
    
    public function _default() {
        $this->displayActivities = false;
        $this->displayAddButton = false;
        
        if ($this->displayHeader){
            $this->menu->setSelectedMenuItem($this->actionUrl());
            
            $this->addScriptFile("jstree/jstree.js");
            $this->addStyleFile("jstree/themes/default/style.min.css");
            $this->addStyleFile("jstree.css");

            $this->header($this->displayHeader);
        }
        ?>
        <div class="tabs-content">
            <div class="content active" id="panelTabStudenti">
                <div class="section-title row">
                    <div class="large-12 medium-12 columns">
                        <h3>Consulta la lista delle attività curriculari</h3>
                    </div>
                </div>
            
                <div class="row">
                    <?php $this->content(); ?>
                </div>
            </div>
        </div>
        <?php
        $this->footer($this->displayHeader);
    }
    
    public function tree(){
        header('Content-type: application/json');
        
        $reply = [];
        
        if ($this->peiNodeId == '#'){
            $this->peiNodeId = NULL;
        }
        
        if (StringUtils::isNotBlank($this->userYearId)){
            $userYear = UserYearService::find($this->userYearId);
            $nodes = PeiService::findNodeWithFather($this->peiNodeId, null, $userYear);
        } else {
            $nodes = PeiService::findNodeWithFather($this->peiNodeId);
        }
                
        foreach ($nodes as $node){
            if (strpos($node['uri'], 'apprendimento_') === false){
                continue;
            }
            if ($this->displayAddButton && strpos($node['uri'], 'apprendimento_') !== false){
                continue;
            }
            
            $numChildren = PeiService::countNodeChildren($node['pei_node_id']);
            $type = "folder";
            if ($numChildren == 0){
                $type = "leaf";
            }

            $text = $node['label'];
            if (strlen($text) > 56){
                $i = 0;
                for ($i = 56; $i > 0; $i--){
                    if ($text[$i] == ' '){
                        break;
                    }
                }
                $text = substr($text, 0, $i);

                $text .= '...';
            }
            
            $reply[] = [
                    "id" => 'node_'.$node['pei_node_id'],
                    "text" => $text,
                    "type" => $type,
                    "state" => [
                        "opened"    => false,
                        "disabled"  => false,
                        "selected"  => false
                    ],
                    "children" => $numChildren > 0
            ];       
        }
        
        echo json_encode($reply);
    }
    
}
