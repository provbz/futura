<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PeiAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PeiTreeAction extends StructureAction {
    
    const JSTREE_VIEW_TYPE_KEY = "jstree_view_type";
    
    public $uri;
    public $id;
    public $documentId;
    public $documentType;
    public $peiNodeId;
    public $displayHeader = true;
    public $displayAddButton = false;
    public $userYearId;
    public $query;
    public $viewType;
    
    protected $grades;
    protected $genders;

    public function __construct() {
        $this->grades = DictionaryService::findGroupAsDictionary(Dictionary::GRADE);
        
        $this->genders['Femmina'] = 'Femmina';
        $this->genders['Maschio'] = 'Maschio';
    }
    
    public function _prepare() {
        parent::_prepare();
        
        $this->documentType = $this->getGlobalSessionValue("documentType");
        if ($this->documentId != null){
            $document = DocumentService::find($this->documentId);
            $this->documentType = $document["type"];
        }

        if ($this->documentType == null){
            $this->documentType = DocumentType::PEI;
        }
        
        if (StringUtils::isBlank($this->viewType) && $this->documentType == DocumentType::PDP){
            $this->viewType = $this->getGlobalSessionValue($this::JSTREE_VIEW_TYPE_KEY);
            if (StringUtils::isBlank($this->viewType)){    
                if ( $this->documentType == DocumentType::PDP){
                    $this->viewType = "pdp_compact";
                }
            }
        } else {
            $this->setGlobalSessionValue($this::JSTREE_VIEW_TYPE_KEY, $this->viewType);
        }
    }

    public function _default(){
        if ($this->displayHeader){
            $this->menu->setSelectedMenuItem($this->actionUrl());
        }
        
        $this->addScriptFile("jstree/jstree.js");
        $this->addStyleFile("jstree/themes/default/style.min.css");
        $this->addStyleFile("jstree.css");
        
        $this->header($this->displayHeader);
        ?>
            <div class="content active" id="panelTabStudenti">
                <div class="section-title padding">
                    <div class="row">
                        <div class="columns small-10">
                            <p>
                                Navigando l’indice a sinistra selezionare gli ambiti di interesse in riferimento all’osservazione dell’alunno.<br/>
                                Nell’ambito apprendimenti è disponibile anche la selezione delle materie per la progettazione del percorso curricolare.
                            </p>  
                        </div>
                        <?php 
                        if ($this->documentType == DocumentType::PDP){ 
                            $checked = "";
                            if ($this->viewType != 'pdp_compact'){
                                $checked = "checked";
                            }
                            ?>
                        <div class="columns small-2">
                            <input type="checkbox" id="show_all" value="1" <?= $checked ?>> Mostra tutto
                        </div>
                        <?php } ?>
                    </div>
                    <form class="row" onsubmit="return doSearch();">
                        <div class="columns small-10">
                            <input id="query" type="text" placeholder="Testo" value="<?= _t($this->query) ?>"/>
                        </div>
                        <div class="columns small-2">
                            <a class="button" onclick="doSearch();">
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                    </form>
                </div>
                <div class="row padding">
                    <?php $this->content(); ?>
                </div>
            </div>
        <?php
        $this->footer($this->displayHeader);
    }
    
    public function content(){
        ?>
        <script type="text/javascript">
            var appUrl = "";
            var selectedNodeTargetId;
            
            function doSearch(){
                var query = $('#query').val();
                if (query == ""){
                    $('#jstree').removeClass('jstree');
                    $('#jstree').removeClass('jstree-2');
                    $('#jstree').removeClass('jstree-default');        
                    prepareJsTree();
                } else {
                    loadDiv("#jstree", "<?= $this->actionUrl("search") ?>", {
                        'query' : query,
                        "documentId": <?= $this->documentId ? $this->documentId : 'null' ?>
                    },function(){});
                }
                
                return false;
            }
            
            function loadNode(nodeId){
                if (nodeId == undefined){
                    nodeId = selectedNodeTargetId;
                }
                selectedNodeTargetId = nodeId;
                
                loadDiv("#node_details", "<?= $this->actionUrl("details", 
                    [
                        'displayAddButton' => $this->displayAddButton,
                        "documentId" => $this->documentId
                    ]);?>", {'peiNodeId' : selectedNodeTargetId},
                    function(){
                    });
            }
            
            function prepareJsTree(viewType = null){
                var url = "<?= $this->actionUrl("tree");?>";
                if (viewType != null){
                    url += "?viewType=" + viewType;    
                }
                
                $("#jstree")
                    .on('changed.jstree', function (e, data) {
                        var i, j, select;
                        for(i = 0, j = data.selected.length; i < j; i++) {
                            select = data.instance.get_node(data.selected[i]).id.replace('node_', '');
                            loadNode(select);
                            return;
                        }
                    })
                    .jstree({
                        "core" : {
                            "multiple" : false,
                            "animation" : 0,
                            "check_callback" : true,
                            "themes" : { "variant" : "large" },
                            'data' : {
                                'url' : url,
                                'data' : function (n) {
                                    $('body').css('overflow', 'auto');
                                    
                                    return { 
                                        "peiNodeId" : n.id ? n.id.replace("node_","") : '' ,
                                        'displayAddButton': '<?= $this->displayAddButton ?>',
                                        "userYearId": '<?= $this->userYearId ?>'
                                    };
                                }}
                        },
                        "types" : {
                            "folder" : {
                                
                            },
                            "leaf" : {
                                "icon" : "<?= UriService::buildImageUrl('file.png') ?>"
                            }
                        },
                        "plugins" : [ 
                                "state"//, "types", "wholerow"
                            ]
                    });
            }
            
            $(function () {
                prepareJsTree();
                
                $('#show_all').on('change', function(){
                    var value = $('#show_all').is(':checked');
                    
                    var viewType = "full";
                    if (!value){
                        viewType = "pdp_compact";
                    }   
                    
                    $('#jstree').removeClass('jstree');
                    $('#jstree').removeClass('jstree-2');
                    $('#jstree').removeClass('jstree-default');        
                    prepareJsTree(viewType);
                });
            });
        </script>
        <style type="text/css">
            .target{
                margin-bottom: 10px;
                border-bottom: 1px solid #CCCCCC;
            }
            .activities{
                margin-bottom: 10px;
                padding: 4px 4px 4px 4px;
                border: 1px solid #EEEEEE;
                font-size: 0.8em;
            }
        </style>
        <div id="jstree" class="large-4 medium-4 small-12 columns"></div>
        <div id="node_details" class="large-8 medium-8 small-12 columns"></div>
        <?php
    }
    
    public function search(){
        $stmt = PeiService::findByTitle($this->query, $this->documentType);
        if ($stmt->rowCount() == 0){
            echo '<p>Nessun risultato trovato.</p>';
            return;
        }
        
        echo 'Risultati ricerca';
        foreach ($stmt as $row){
            ?>
                <div style="padding: 10px;">
                    <a onclick="return loadNode('<?= $row['pei_node_id'] ?>');"><?= _t($row['label']) ?></a>
                </div>
            <?php
        }
    }
    
    public function icf(){
        echo $this->id;
    }
    
    public function details(){
        if ($this->documentType == DocumentType::PDP_LINGUA){
            $this->documentType = DocumentType::PDP;
        } else if ($this->documentType == DocumentType::PEI_TIROCINIO){
            $this->documentType = DocumentType::PEI;
        }

        $peiNode = PeiService::findNode($this->peiNodeId, $this->documentType);
        ?>
            <h3><?= _t($peiNode['label']) ?></h3>
            <p>
                <?= _t($peiNode['description']) ?>  
            </p>
            
            <?php if ($peiNode['icf_codes'] != NULL){ ?>
                <p>
                    <strong>Codici ICF-CY</strong><br/>
                    <?php foreach (json_decode($peiNode['icf_codes']) as $code) { 
                        $icf = IcfService::find($code);
                        ?>
                        <?= _t($code) ?>
                        <?php if (isset($icf['name'])){ ?>
                            : <?= _t($icf['name']) ?><br/>
                        <?php } ?>
                    <?php } ?>
                </p>
            <?php } ?>
            <?php if ( $peiNode['icd10_codes'] != NULL ){ ?>
                <p>
                    <strong>Codici ICD-10</strong><br/>
                    <?php 
                        $out = "";
                        foreach (json_decode( $peiNode['icd10_codes'] ) as $code) { 
                            $out .= $out == "" ? '' : ', ';
                            $out .= $code;
                        } 
                        echo $out;
                    ?>
                </p>
            <?php } ?>
            <?php
            $questions = PeiService::findNodeQuestions($peiNode['pei_node_id'], $this->documentType);
            if ($questions->rowCount() > 0){
            ?>
                <p>
                    <strong>Domande</strong><br/>
                    <?php foreach($questions as $question){ ?>
                        <?= _t($question['question']) ?><br/>
                    <?php } ?>
                </p>
            <?php 
            }
            
            if ($this->displayAddButton && $peiNode['father_pei_node_id'] != null && $peiNode['father_pei_node_id'] != 10921){ 
                $pei = $this->getGlobalSessionValue("PeiAction_pei");
                if ($pei != null && isset($peiNode['uri'])){
                    $node = PeiService::findPeiRowByUri($pei['user_year_document_id'], $peiNode['uri']);
                    if ($node == NULL || $node['status'] != PeiRowStatus::SELECTED){
                        ?>
                    <a class='button' href="javascript:parent.addPeiRow('<?= str_replace("'", "\'", $peiNode['uri']) ?>');">
                            <i class="fa fa-floppy-o"></i>Aggiungi</a>
                        <?php
                    }
                }
            }
    }
    
    public function tree(){
        $reply = [];
        
        if ($this->peiNodeId == '#'){
            $this->peiNodeId = NULL;
        }
        
        if ($this->viewType == null){
            $this->viewType = "full";
        }
        
        $nodes = PeiService::findNodeWithFather($this->documentType, $this->peiNodeId, NULL, NULL, $this->viewType);
        
        foreach ($nodes as $node){
            $numChildren = PeiService::countNodeChildren($this->documentType, $node['pei_node_id'], $this->viewType);
            $type = "folder";
            if ($numChildren == 0){
                $type = "leaf";
            }

            $text = $node['label'];
            if (strlen($text) > 56){
                $i = 0;
                for ($i = 56; $i > 0; $i--){
                    if ($text[$i] == ' '){
                        break;
                    }
                }
                $text = substr($text, 0, $i);

                $text .= '...';
            }
            
            $reply[] = [
                    "id" => 'node_'.$node['pei_node_id'],
                    "text" => $text,
                    "type" => $type,
                    "state" => [
                        "opened"    => false,
                        "disabled"  => false,
                        "selected"  => false
                    ],
                    "children" => $numChildren > 0
            ];       
        }
        
        header('Content-type: application/json');
        echo json_encode($reply);
    }
}
