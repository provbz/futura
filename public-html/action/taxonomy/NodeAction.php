<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of NodeAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class NodeAction extends PublicAction{
    
    public $id;
    public $node_id;
    public $grade;
    public $time;
    public $level;
    public $operation;
    public $title;
    
    private $node;
    
    public function _prepare() {
        parent::_prepare();
        
        if (StringUtils::isBlank( $this->id )){
            die();
        }
        
        $this->node = EM::execQuerySingleResult( sprintf("SELECT * FROM taxonomy_node WHERE taxonomy_node_id=%d", $this->id));
        if ($this->node == NULL){
            die();
        }
        if ($this->node["parent_taxonomy_node_id"] == 0){
            die();
        }
    }


    public function _default(){
        
        $this->addScriptFile("bootstrap-tagmanager.js");
        $this->addScriptFile("jquery.jstree.js");
        
        include_once 'template/Header.php';
        ?>
<style type="text/css">
    body{
        padding: 5px 5px 5px 5px;
    }
    h2{
        border-bottom: 1px solid #000000;
    }
</style>
<?php
        echo "<h2>".$this->node["name"]."</h2>";
        $tabs = new TabbedPanel(true);
        $tabs->addTab(new TabbedPanelTab("Generale", $this->actionUrl("nodeGeneralInfo", ["id" => $this->id])));
        $tabs->addTab(new TabbedPanelTab("Obiettivi Sc. Infanzia", UriService::buildPageUrl("/taxonomy/TargetAction", NULL, ["id" => $this->id, "grade" => 0])));
        $tabs->addTab(new TabbedPanelTab("Obiettivi Sc. Primaria", UriService::buildPageUrl("/taxonomy/TargetAction", NULL, ["id" => $this->id, "grade" => 1])));
        $tabs->addTab(new TabbedPanelTab("Obiettivi Sc. Secondaria di primo grado", UriService::buildPageUrl("/taxonomy/TargetAction", NULL, ["id" => $this->id, "grade" => 2])));
        $tabs->draw();
        ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#tabs ul').tabs({
            select : function (){
                $('body').css('overflow', 'auto');
            }
        });
    });
</script>
<?php
        
        include_once 'template/Footer.php';
    }
    
    private function buildForm(){
        
        $form = new Form($this->actionUrl("saveNodeData", [ "id" => $this->id]) );
        $form->entityName = "taxonomy_node";
        $form->entity = $this->node;
        $form->addField(new FormTextareaField("Glossario", "definition", FormFieldType::STRING));
        
        $questionsStr = "";
        $questions = EM::execQuery(sprintf("SELECT * FROM taxonomy_node_metadata_value ".
                "WHERE metadata_id='%s' AND taxonomy_node_id=%d ".
                "ORDER BY position",
                'NODE_QUESTION', $this->id));
        $i = 0;
        while ($question = mysql_fetch_assoc($questions)){
            $questionsStr .= $this->formatQuestionItem($i, $question["value"]);
            $i++;
        }
        mysql_free_result($questions);
        for (; $i < 2; $i ++){
            $questionsStr .= $this->formatQuestionItem($i);
        }
        
        $questionField = "<fieldset><label>Domande:</label>";
        $questionField .= "<div id='multi_field_questions' style='margin-left: 220px;'>";
        $questionField .= "<div id='questions_container'>".$questionsStr."</div>";
        $questionField .= "<a href='javascript:void(0);' onclick='return addQuestion();'>Aggiungi</a>";
        $questionField .= "</div>";
        $questionField .= "</fieldset>";
        $formQuestions = new FormHtmlField($questionField);
        $form->addField($formQuestions);
        
        $icfCodes = new FormTextField("Codici ICF", "NODE_ICF", FormFieldType::STRING, false);
        $icfCodes->persist = false;
        $icfCodes->parameters["style"] = "width: 100px";
        $icfCodes->parameters["placeholder"] = "Codice";
        $form->addField($icfCodes);
        
        $icd10Codes = new FormTextField("Codici ICD-10", "NODE_ICD10", FormFieldType::STRING, false);
        $icd10Codes->persist = false;
        $icd10Codes->parameters["style"] = "width: 100px";
        $icd10Codes->parameters["placeholder"] = "Codice";
        $form->addField($icd10Codes);
        
        $dsm0Codes = new FormTextField("Codici DSM", "NODE_DSM", FormFieldType::STRING, false);
        $dsm0Codes->persist = false;
        $dsm0Codes->parameters["style"] = "width: 100px";
        $dsm0Codes->parameters["placeholder"] = "Codice";
        $form->addField($dsm0Codes);
        
        $form->addField(new FormTextareaField("Note", "note", FormFieldType::STRING));
        
        return $form;
    }
    
    public function formatQuestionItem($i, $value = ""){
        $questionsStr = "<div class='question_item' id='question_item_$i'>";
        $questionsStr .= "<input type='text' name='question[]' id='question_$i' value='$value'/>";
        $questionsStr .= "<a href='javascript:void(0);' onclick='return deleteQuestion($i);'>X</a><br/>";
        $questionsStr .= "</div>";
        return $questionsStr;
    }
    
    private function prepareTags($metadataId){
        $icfCodes = "";
        $tags = EM::execQuery(sprintf("SELECT * FROM taxonomy_node_metadata_value ".
                "WHERE metadata_id='%s' AND taxonomy_node_id=%d ".
                "ORDER BY value",
                $metadataId, $this->id));
        while ($tag = mysql_fetch_assoc($tags)){
            $icfCodes .= $icfCodes == "" ? "" : ",";
            $icfCodes .= $tag["value"];
        }
        mysql_free_result($tags);
        return $icfCodes;
    }
    
    public function nodeGeneralInfo(){
        $form = $this->buildForm();
        $form->draw();
        
        $icfCodes = $this->prepareTags('NODE_ICF');
        $icd10Codes = $this->prepareTags('NODE_ICD10');
        $dsmCodes = $this->prepareTags('NODE_DSM');
        
        ?>
<script type="text/javascript">
$("#NODE_ICF").tagsManager({
    preventSubmitOnEnter: true,
    prefilled: "<?= $icfCodes; ?>"});
$("#NODE_ICD10").tagsManager({
    preventSubmitOnEnter: true,
    prefilled: "<?= $icd10Codes; ?>"});
$("#NODE_DSM").tagsManager({
    preventSubmitOnEnter: true,
    prefilled: "<?= $dsmCodes; ?>"});

function formatQuestion(i){
    var template = "<?= $this->formatQuestionItem("::") ?>";
    return template.replace(/::/g, i);
}

function addQuestion (){
    var nextId = $('.question_item').length;
    $('#questions_container').append(formatQuestion(nextId));
}
function deleteQuestion(num){
    $('#question_item_' + num).hide('fast', function(){ this.remove();});
}
</script>
        <?php
    }
    
    private function saveMetadata($metadataId){
        $tags = getRequestValue("hidden-".$metadataId);
        $arrayTags = mb_split(",", $tags);
        
        EM::deleteEntity("taxonomy_node_metadata_value", "taxonomy_node_id=".$this->id." AND metadata_id='$metadataId'");
        $i = 0;
        foreach ($arrayTags as $value){
            if (StringUtils::isBlank($value)){
                continue;
            }
            EM::insertEntity("taxonomy_node_metadata_value", 
                ["taxonomy_node_id" => $this->id,
                    "metadata_id" => $metadataId,
                    "value" => $value,
                    "position" => $i]);
            $i ++;
        }
    }
    
    public function saveNodeData(){
        $form = $this->buildForm();
        if (!$form->checkFields()){
            $this->nodeData(); 
        } else {
            $form->persist("taxonomy_node_id=" . $this->id);
            
            EM::deleteEntity("taxonomy_node_metadata_value", "taxonomy_node_id=".$this->id." AND metadata_id='NODE_QUESTION'");
            $questions = getRequestValue("question");
            $i = 0;
            foreach ($questions as $value){
                if (StringUtils::isBlank($value)){
                    continue;
                }
                
                EM::insertEntity("taxonomy_node_metadata_value", 
                    ["taxonomy_node_id" => $this->id,
                        "metadata_id" => "NODE_QUESTION",
                        "value" => $value,
                        "position" => $i]);
                $i++;
            }
            
            $this->saveMetadata("NODE_ICF");
            $this->saveMetadata("NODE_ICD10");
            $this->saveMetadata("NODE_DSM");
            
            redirectWithMessage($this->actionUrl("_default", ["id" => $this->id]), "Dati salvati.");
        }
    }
    
}

?>
