<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AxisAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once './action/taxonomy/PeiTreeAction.php';

class PeiAxisAction extends PeiTreeAction {
    
    public $query;
    public $displayActivities;
    
    public function _default() {
        $this->displayActivities = false;
        $this->displayAddButton = false;
        $this->viewType = "full";
        $this->documentType = DocumentType::PEI;
        
        parent::_default();
        $this->menu->setSelectedMenuItem($this->actionUrl());
    }
    
    public function tree(){
        $reply = [];
        
        if ($this->peiNodeId == '#'){
            $this->peiNodeId = NULL;
        }
        
        $this->viewType = "full";
        $this->documentType = DocumentType::PEI;
        
        $nodes = PeiService::findNodeWithFather($this->documentType, $this->peiNodeId, NULL, NULL, $this->viewType);
        
        foreach ($nodes as $node){
            if ($this->displayAddButton && strpos($node['uri'], 'apprendimento_') !== false){
                continue;
            }
            
            $numChildren = PeiService::countNodeChildren($this->documentType, $node['pei_node_id'], $this->viewType);
            $type = "folder";
            if ($numChildren == 0){
                $type = "leaf";
            }

            $text = $node['label'];
            if (strlen($text) > 56){
                $i = 0;
                for ($i = 56; $i > 0; $i--){
                    if ($text[$i] == ' '){
                        break;
                    }
                }
                $text = substr($text, 0, $i);

                $text .= '...';
            }
            
            $reply[] = [
                    "id" => 'node_'.$node['pei_node_id'],
                    "text" => $text,
                    "type" => $type,
                    "state" => [
                        "opened"    => false,
                        "disabled"  => false,
                        "selected"  => false
                    ],
                    "children" => $numChildren > 0
            ];       
        }
        
        header('Content-type: application/json');
        echo json_encode($reply);
    }
    
}
