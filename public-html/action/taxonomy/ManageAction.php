<?php

class ManageAction extends PublicAction{
    
    public $operation;
    public $id;
    public $position;
    public $title;
    public $type;
    public $ref;
    public $copy;
    
    private $node;
    
    public function _default(){
        
        $this->addScriptFile("jquery.jstree.js");
        
        include_once 'template/Header.php';
        
        ?>
<style type="text/css">
    .taxonomy_tree{
        width: 30%;
        height: 600px;
        overflow-y: auto;
        float:left;
    }
    #node_details{
        float: right;
        border: 0px;
        width: 70%;
        height: 100%;
    }
</style>
<div id="taxonomy_tree" class="taxonomy_tree">
</div>
<iframe id="node_details"></iframe>
<script type="text/javascript">
    $(function () {
       $("#taxonomy_tree").jstree({
           "plugins" : [ 
                    "themes","json_data","ui","crrm","cookies","dnd","types","contextmenu" 
            ],
            "json_data" : {
                "ajax" : {
                    "url" : "<?= $this->actionUrl("tree");?>",
                    "data" : function (n) {
                            return { 
                                    "operation" : "get_children", 
                                    "id" : n.attr ? n.attr("id").replace("node_","") : 0 
                            }; 
                    }}
                },
                "types" : {
                        "default" : {
                                // can have files and other folders inside of it, but NOT `drive` nodes
                                "valid_children" : [ "default"],
                                "icon" : {
                                        "image" : "./folder.png"
                                }
                        },
                        // The `drive` nodes 
                        "root" : {
                                "valid_children" : [ "default" ],
                                "icon" : {
                                        "image" : "./root.png"
                                },
                                "start_drag" : false,
                                "move_node" : false,
                                "delete_node" : false,
                                "remove" : false
                        }
                }
       }).bind("create.jstree", function (e, data) {
            $.post(
                "<?= $this->actionUrl("tree"); ?>", 
                { 
                    "operation" : "create_node", 
                    "id" : data.rslt.parent.attr("id").replace("node_",""), 
                    "position" : data.rslt.position,
                    "title" : data.rslt.name,
                    "type" : data.rslt.obj.attr("rel")
                }, 
                function (r) {
                    if(r.status) {
                            $(data.rslt.obj).attr("id", "node_" + r.id);
                    }
                    else {
                            $.jstree.rollback(data.rlbk);
                    }
                }, "json"
            );
	}).bind("remove.jstree", function (e, data) {
            data.rslt.obj.each(function () {
                var nodeId = this.id.replace("node_","");
                if (nodeId === "1"){
                    alert("Nodo non cancellabile.");
                    data.inst.refresh();
                    return;
                } else if (!confirm("Si desidera veramente eliminare il nodo?")){
                    data.inst.refresh();
                    return;
                }
                $.ajax({
                    async : false,
                    type: 'POST',
                    dataType: "json",
                    url: "<?= $this->actionUrl("tree"); ?>",
                    data : { 
                            "operation" : "remove_node", 
                            "id" : nodeId
                    }, 
                    success : function (r) {
                        if(!r.status) {
                            data.inst.refresh();
                        }
                    }
                });
            });
	}).bind("rename.jstree", function (e, data) {
            var nodeId = data.rslt.obj.attr("id").replace("node_","");
            if (nodeId === "1"){
                $.jstree.rollback(data.rlbk);
                return;
            }
            $.post(
                "<?= $this->actionUrl("tree"); ?>", 
                { 
                    "operation" : "rename_node", 
                    "id" : nodeId,
                    "title" : data.rslt.new_name
                }, 
                function (r) {
                    if(!r.status) {
                        $.jstree.rollback(data.rlbk);
                    }
                }, "json"
            );
	}).bind("move_node.jstree", function (e, data) {
            data.rslt.o.each(function (i) {
                    $.ajax({
                            async : false,
                            type: 'POST',
                            dataType: "json",
                            url: "<?= $this->actionUrl("tree"); ?>",
                            data : { 
                                "operation" : "move_node", 
                                "id" : $(this).attr("id").replace("node_",""), 
                                "ref" : data.rslt.cr === -1 ? 1 : data.rslt.np.attr("id").replace("node_",""), 
                                "position" : data.rslt.cp + i,
                                "title" : data.rslt.name,
                                "copy" : data.rslt.cy ? 1 : 0
                            },
                            success : function (r) {
                                if(!r.status) {
                                    $.jstree.rollback(data.rlbk);
                                } else {
                                    $(data.rslt.oc).attr("id", "node_" + r.id);
                                    if(data.rslt.cy && $(data.rslt.oc).children("UL").length) {
                                            data.inst.refresh(data.inst._get_parent(data.rslt.oc));
                                    }
                                }
                                $("#analyze").click();
                            }
                    });
            });
	}).bind("select_node.jstree", function (event, data) { 
            var nodeId = data.rslt.obj.attr("id").replace("node_","");
            $("#node_details").attr("src", "<?= UriService::buildPageUrl("/taxonomy/NodeAction");?>?id=" + nodeId );
        }); 
    });
</script>
    

        <?php
        
        include_once 'template/Footer.php';
    }
    
    public function tree(){
        $reply = array();
        if ($this->operation == "get_children"){
            $result = EM::execQuery(sprintf("SELECT * FROM taxonomy_node ".
                    "WHERE parent_taxonomy_node_id=%d ".
                    "ORDER BY position", $this->id));
            
            while ($row = mysql_fetch_assoc($result)){
                $reply[] = array(
                        "attr" => array("id" => "node_".$row["taxonomy_node_id"], "rel" => $row["type"]),
                        "data" => $row["name"],
                        "state" => "closed"
                );       
            }
            
        } else if ($this->operation == "create_node"){
            $id = EM::insertEntity("taxonomy_node", [
                "parent_taxonomy_node_id" => $this->id, 
                "name" => $this->title,
                "position" => $this->position,
                "type" => $this->type]);
            $reply["status"] = true;
            $reply["id"] = $id;
        } else if ($this->operation == "remove_node"){
            $node = TaxonomyService::find($this->id);
            $parentId = $node['parent_taxonomy_node_id'];
            
            EM::deleteEntity("taxonomy_node", ["taxonomy_node_id" => $this->id]);
            TaxonomyService::resortNodesWithParent($parentId);
            $reply["status"] = true;
        } else if ($this->operation == "rename_node"){
            EM::updateEntity("taxonomy_node", ["name" => $this->title], "taxonomy_node_id=".$this->id);
            $reply["status"] = true;
        } else if ($this->operation == "move_node"){
            $reply["status"] = false;
            
            if (!$this->copy){
                $node = TaxonomyService::find($this->id);
                $oldParentId = $node['parent_taxonomy_node_id'];
                
                EM::execQuery("UPDATE taxonomy_node SET position = position + 1 WHERE ".
                        "parent_taxonomy_node_id=".$this->ref. " AND position >=" . $this->position);
                
                EM::updateEntity("taxonomy_node", ["parent_taxonomy_node_id" => $this->ref, "position" => $this->position], "taxonomy_node_id=".$this->id);
                
                TaxonomyService::resortNodesWithParent($this->ref);
                TaxonomyService::resortNodesWithParent($oldParentId);
                
                $reply["status"] = true;
            }
        }
        
        echo json_encode($reply);
    }
}

?>
