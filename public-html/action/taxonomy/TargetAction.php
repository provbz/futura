<?php
/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ObjectiveAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class TargetAction extends PublicAction {
    
    public $id;
    public $node_id;
    public $grade;
    public $time;
    public $level;
    public $operation;
    public $title;
    public $position;
    public $activities;
    
    public function _default(){
        
        ?>
<style type="text/css">
    .target_tree{
    }
    .target_activities{
        
        padding: 10px 10px 10px 10px;
        background-color: #ffffdd;
    }
    .clear{
        clear: both;
    }
</style>
<div id="grade_container_<?= $this->grade; ?>">
    <div class="target_tree" id="target_tree_<?= $this->grade ?>"></div>
    <div class="target_activities"></div>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    var id = '<?= $this->id; ?>';
    var grade = '<?= $this->grade; ?>';
        
    $("#target_tree_" + grade).jstree({
       "plugins" : [ 
            "themes","json_data","ui","crrm","cookies","dnd","types","contextmenu" 
       ],
       "json_data" : {
                "ajax" : {
                    "url" : "<?= $this->actionUrl("tree");?>",
                    "data" : function (n) {
                            return { 
                                    "operation" : "get_children", 
                                    "id": id,
                                    "grade": grade,
                                    "time" : n.attr ? n.attr("time") : "", 
                                    "level" : n.attr ? n.attr("level") : "",
                                    "node_id" : n.attr ? n.attr("id").replace('node_', "") : 0
                            }; 
                    }}
        },
        "types" : {
                "valid_children" : [ "fixed"],
                "types":{
                    "default" : {
                            "valid_children" : [],
                            "icon" : {
                                    "image" : "/style/img/folder.png"
                            }
                    },
                    "fixed" : {
                            "valid_children" : [ "default", "fixed" ],
                            "icon" : {
                                    "image" : "/style/img/root.png"
                            },
                            "start_drag" : false,
                            "move_node" : false,
                            "delete_node" : false,
                            "remove" : false,
                            "rename" : false
                    }
                }
        }
        
    }).bind("create.jstree", function (e, data) {
            var time =  data.rslt.parent.attr("time");
            if (time === undefined){
                time = "";
            }
            
            var level = data.rslt.parent.attr("level");
            if (level === undefined){
                level = "";
            }
            
            var nodeId = data.rslt.parent.attr("id");
            if (nodeId === undefined){
                nodeId = "";
            } else {
                nodeId = nodeId.replace("node_", "");
            }
            
            $.post(
                "<?= $this->actionUrl("tree"); ?>", 
                { 
                    "id": id,
                    "operation" : "create_node", 
                    "time" :  time, 
                    "grade" :  grade, 
                    "level" : level, 
                    "node_id" : nodeId,
                    "position" : data.rslt.position,
                    "title" : data.rslt.name,
                    "type" : data.rslt.obj.attr("rel")
                }, 
                function (r) {
                    if(r.status) {
                        $(data.rslt.obj).attr("id", "node_" + r.id);
                    } else {
                        $.jstree.rollback(data.rlbk);
                    }
                }, "json"
            );
	}).bind("remove.jstree", function (e, data) {
            data.rslt.obj.each(function () {
                var nodeId = this.id.replace("node_","");
                if (nodeId === undefined){
                    sreturn;
                } else if (!confirm("Si desidera veramente eliminare il nodo?")){
                    data.inst.refresh();
                    return;
                }
                
                $.ajax({
                    async : false,
                    type: 'POST',
                    dataType: "json",
                    url: "<?= $this->actionUrl("tree"); ?>",
                    data : { 
                            "operation" : "remove_node", 
                            "node_id" : nodeId
                    }, 
                    success : function (r) {
                        if(!r.status) {
                            data.inst.refresh();
                        }
                    }
                });
            });
	}).bind("rename.jstree", function (e, data) {
            var nodeId = data.rslt.obj.attr("id");
            if (nodeId === undefined){
                return;
            }
            nodeId = nodeId.replace("node_", "");
            $.post(
                "<?= $this->actionUrl("tree"); ?>", 
                { 
                    "operation" : "rename_node", 
                    "node_id" : nodeId,
                    "title" : data.rslt.new_name
                }, 
                function (r) {
                    if(!r.status) {
                        $.jstree.rollback(data.rlbk);
                    }
                }, "json"
            );
	}).bind("select_node.jstree", function (event, data) { 
            $(".target_activities").html("");
            
            var container = $("#grade_container_" + grade + " .target_activities");
            var nodeId = data.rslt.obj.attr("id");
            if (nodeId === undefined){
                return;
            }
            nodeId = nodeId.replace("node_","");
            container.load("<?= $this->actionUrl("getActivities", [
                "id" => $this->id,
                "grade" => $this->grade] );?>&node_id=" +nodeId );
        }).bind("move_node.jstree", function (e, data) {
            data.rslt.o.each(function (i) {
                $.ajax({
                        async : false,
                        type: 'POST',
                        dataType: "json",
                        url: "<?= $this->actionUrl("tree"); ?>",
                        data : { 
                                "operation" : "move_node", 
                                "node_id" : $(this).attr("id").replace("node_",""), 
                                "ref" : data.rslt.cr === -1 ? 1 : data.rslt.np.attr("id").replace("node_",""), 
                                "position" : data.rslt.cp + i,
                                "title" : data.rslt.name,
                                "copy" : data.rslt.cy ? 1 : 0
                        },
                        success : function (r) {
                                if(!r.status) {
                                        $.jstree.rollback(data.rlbk);
                                }
                                else {
                                        $(data.rslt.oc).attr("id", "node_" + r.id);
                                        if(data.rslt.cy && $(data.rslt.oc).children("UL").length) {
                                                data.inst.refresh(data.inst._get_parent(data.rslt.oc));
                                        }
                                }
                                $("#analyze").click();
                        }
                });
            });
	});
</script>
        <?php
    }
    
    public function tree(){
        $reply = array();
        if ($this->operation == "create_node"){
            if (StringUtils::isBlank($this->time) || StringUtils::isBlank($this->level)){
                $reply["status"] = false;
            } else {
                $id = EM::insertEntity("taxonomy_node_target", [
                    "taxonomy_node_id" => $this->id, 
                    "name" => $this->title,
                    "position" => $this->position,
                    "time" => $this->time,
                    "level" => $this->level,
                    "grade" => $this->grade]);
                $reply["status"] = true;
                $reply["id"] = $id;
            }
        } else if ($this->operation == "rename_node"){
            EM::updateEntity("taxonomy_node_target", ["name" => $this->title], "taxonomy_node_target_id=".$this->node_id);
            $reply["status"] = true;
        } else if ($this->operation == "remove_node"){
            EM::deleteEntity("taxonomy_node_target", "taxonomy_node_target_id=".$this->node_id);
            $reply["status"] = true;
        } else if ($this->operation == "move_node"){
            
        } else if ($this->operation == "get_children"){
            $times = ["BT" => "Breve Termine", "MT" => "Medio Termine", "LT" => "Lungo Termine"];
            $levels = ["0" => "Lieve", "1" => "Medio", "2" => "Grave"];
                        
            if ($this->node_id == 0){
                foreach ($levels as $level => $levelValue){
                    $item = array(
                        "attr" => array("level" => $level, "rel" => "fixed"),
                        "data" => $levelValue,
                        "state" => "open",
                        "children" => array()
                        );
                    
                    foreach($times as $time => $timeValue){
                        $timeItem = array(
                                "attr" => array("time" => $time, "level" => $level, "rel" => "fixed"),
                                "data" => $timeValue,
                                "state" => "open",
                                "children" => array()
                            );
                        
                        $objectives = EM::execQuery(sprintf("SELECT * FROM taxonomy_node_target WHERE ".
                                "taxonomy_node_id=%d AND level='%s' AND time='%s' AND grade='%s' ".
                                "ORDER BY position",
                                $this->id, $level, $time, $this->grade));
                        while($objective = mysql_fetch_assoc($objectives)){
                            $timeItem["children"][] = array(
                                "attr" => array("id" => "node_".$objective["taxonomy_node_target_id"], "rel" => "default"),
                                "data" => $objective["name"],
                                "state" => "close",
                            );
                        }
                        mysql_free_result($objectives);
                        $item["children"][] = $timeItem;
                    }
                    
                    $reply[] = $item;
                }
            }
        }
        echo json_encode($reply);
    }
        
    private function activityTemplate($i, $value = ""){
        $questionsStr = "<div class='activity_item' id='activity_item_$i'>";
        $questionsStr .= "<textarea cols='100' rows='5' name='activitiey[]' id='activity_$i'>$value</textarea>";
        $questionsStr .= "<a href='javascript:void(0);' onclick='return deleteActivity($i);'>X</a><br/>";
        $questionsStr .= "</div>";
        return $questionsStr;
    }
    
    public function getActivities(){
        ?>
<script type="text/javascript">
function formatActivity(i){
    var template = "<?= $this->activityTemplate("::") ?>";
    return template.replace(/::/g, i);
}

function addActivity (){
    var nextId = $('.activity_item').length;
    $('#activities_container').append(formatActivity(nextId));
}

function deleteActivity(num){
    $('#activity_item_' + num).hide('fast', function(){ this.remove();});
}

function submitActivities(){
    var items = new Array();
    
    $(".activity_item input").each(function (){
        var value = $(this).val();
        if (value !== ""){
            items.push( value );
        }
    });
    
    $.post(
        '<?= $this->actionUrl("persistActivities"); ?>',
        { 
            node_id: <?= $this->node_id; ?>,
            activities: items
        },
        function (e){
            $('#activities_result').html("Dati salvati!");
        },
        'json'
    );   
    return false;
}
</script>
<div id="activities_result"></div>
<form id="activity_form" onsubmit="return submitActivities();">
    <div id="activities_container">
        <h3>Attività consigliate</h3>

        <?php
        $activities = EM::execQuery(sprintf("SELECT * FROM taxonomy_node_target_activity WHERE taxonomy_node_target_id=%d ".
                "ORDER BY position",
                $this->node_id));
        $i = 0;
        while($activity = mysql_fetch_assoc($activities)){
            echo $this->activityTemplate($i, $activity["description"]);
            $i++;
        }
        for ($k = $i ; $k < 2 || $k < ($i + 1); $k++){
            echo $this->activityTemplate($k);
        }
        mysql_free_result($activities);
        ?>
    </div>
    <a href="javascript:void(0);" onclick="return addActivity();">Aggiungi</a><br/>
    
    <input type="submit" value="Salva"/>
</form>
<?php
    }
    
    public function persistActivities(){
        EM::deleteEntity("taxonomy_node_target_activity", "taxonomy_node_target_id=".$this->node_id);
        if (is_array($this->activities)){
            $i = 0;
            foreach($this->activities as $value){
                EM::insertEntity("taxonomy_node_target_activity", 
                    [
                        "taxonomy_node_target_id" => $this->node_id,
                        "position" => $i,
                        "description" => $value
                    ]);
                $i++;
            }
        }
    }
}

?>
