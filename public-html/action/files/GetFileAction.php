<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of AdminUserAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

 class GetFileAction extends Page {

    public $structure;

    public function _default(){
        $filePath = __DIR__ . '/../../' . urldecode( $_SERVER['REQUEST_URI'] );
        if (!file_exists($filePath)){
            echo "No file";
            die();
        }

        $filename = pathinfo($filePath, PATHINFO_FILENAME);
        $filename .= '.' . pathinfo($filePath, PATHINFO_EXTENSION);
        $mime = mime_content_type($filePath);

        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public"); // needed for internet explorer
        header("Content-Type: " . $mime);
        header("Content-Transfer-Encoding: Binary");
        header("Content-Length:" . filesize($filePath));
        header("Content-Disposition: inline; filename=" . $filename);
        readfile($filePath);        
    }
    
 }