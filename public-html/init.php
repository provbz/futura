<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
*/

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
$local = false;
if (isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], 'local')){
    $local = true;
}

require_once(__DIR__ . '/vendor/autoload.php');

require_once(__DIR__ . '/constant/constants.php');
require_once(__DIR__ . '/env/Env.php');

require_once(__DIR__ . '/utils/StringUtils.php');
require_once(__DIR__ . '/utils/DateUtils.php');
require_once(__DIR__ . '/utils/Utils.php');
require_once(__DIR__ . '/utils/FormatterUtils.php');
require_once(__DIR__ . '/utils/ReflectionUtils.php');
require_once(__DIR__ . '/utils/ArrayUtils.php');
require_once(__DIR__ . '/utils/WebClient.php');

require_once(__DIR__ . '/model/KeyValuePair.php');

require_once(__DIR__ . '/trigger/ITrigger.php');

require_once(__DIR__ . '/service/data/EntityManager.php');
require_once(__DIR__ . '/service/data/EM.php');
require_once(__DIR__ . '/service/data/EMAlfa.php');
require_once(__DIR__ . '/service/data/EMEsis.php');

EM::init();
EMAlfa::init();
EMEsis::init();

require_once(__DIR__ . '/service/UriService.php');
require_once(__DIR__ . '/service/UserService.php');
require_once(__DIR__ . '/service/MetadataService.php');
require_once(__DIR__ . '/service/DictionaryService.php');
require_once(__DIR__ . '/service/UserRoleService.php');
require_once(__DIR__ . '/service/TextService.php');
require_once(__DIR__ . '/service/StructureService.php');
require_once(__DIR__ . '/service/LogService.php');
require_once(__DIR__ . '/service/PropertyService.php');
require_once(__DIR__ . '/service/SendMessageService.php');
require_once(__DIR__ . '/service/EncryptService.php');
require_once(__DIR__ . '/service/DocumentService.php');
require_once(__DIR__ . '/service/UserUserService.php');
require_once(__DIR__ . '/service/NoticeService.php');
require_once(__DIR__ . '/service/NoticeUserService.php');
require_once(__DIR__ . '/service/MailService.php');
require_once(__DIR__ . '/service/RecaptchaService.php');
require_once(__DIR__ . '/service/AttachmentService.php');
require_once(__DIR__ . '/service/BannedIpService.php');
require_once(__DIR__ . '/service/JournalService.php');

require_once(__DIR__ . '/service/local/UserYearService.php');
require_once(__DIR__ . '/service/local/TaxonomyService.php');
require_once(__DIR__ . '/service/local/UserPeiService.php');
require_once(__DIR__ . '/service/local/IcfService.php');
require_once(__DIR__ . '/service/local/SchoolYearService.php');
require_once(__DIR__ . '/service/local/PeiService.php');
require_once(__DIR__ . '/service/local/UserTransferService.php');
require_once(__DIR__ . '/service/local/EdSaluteService.php');
require_once(__DIR__ . '/service/local/EModelService.php');
require_once(__DIR__ . '/service/local/DropOutService.php');
require_once(__DIR__ . '/service/local/ClassGradeService.php');
require_once(__DIR__ . '/service/local/TestService.php');
require_once(__DIR__ . '/service/local/TestSogliaService.php');
require_once(__DIR__ . '/service/local/SomministrazioneService.php');
require_once(__DIR__ . '/service/local/SomministrazioneStatsService.php');
require_once(__DIR__ . '/service/local/SomministrazioneTestResultClassService.php');
require_once(__DIR__ . '/service/local/SomministrazioneTestResultClassViewService.php');
require_once(__DIR__ . '/service/local/StructureClassService.php');
require_once(__DIR__ . '/service/local/StructureClassUserService.php');
require_once(__DIR__ . '/service/local/ScriptService.php');
require_once(__DIR__ . '/service/local/AlfaService.php');
require_once(__DIR__ . '/service/local/EsisService.php');
require_once(__DIR__ . '/service/local/UserPctoService.php');
require_once(__DIR__ . '/service/local/InternalMessageService.php');
require_once(__DIR__ . '/service/NazioneService.php');
require_once(__DIR__ . '/service/SchedulerService.php');
require_once(__DIR__ . '/service/local/RilevazioneCattedreService.php');

require_once __DIR__ . '/action/api/ApiBaseAction.php';
require_once __DIR__ . '/action/api/RequestBaseClass.php';
require_once(__DIR__ . '/ui/Page.php');
require_once(__DIR__ . '/ui/Formatter.php');
require_once(__DIR__ . '/ui/table/Table.php');
require_once(__DIR__ . '/ui/tableVue/TableVue.php');
require_once(__DIR__ . '/ui/form/Form.php');
require_once(__DIR__ . '/ui/Report.php');
require_once(__DIR__ . '/ui/Bradcrumbs.php');
require_once(__DIR__ . '/ui/TabbedPanel.php');
require_once(__DIR__ . '/ui/menu/Menu.php');
require_once(__DIR__ . '/ui/MetadataFormatter.php');
require_once(__DIR__ . '/ui/form/FilterUtils.php');
require_once(__DIR__ . '/ui/form/FormCustomContentField.php');
require_once(__DIR__ . '/ui/form/FormSelect2Field.php');

require_once(__DIR__ . '/filter/LoginFilter.php');
require_once(__DIR__ . '/filter/AccessCheckFilter.php');
require_once(__DIR__ . '/filter/RequestFilter.php');
require_once(__DIR__ . '/filter/SelectedStructureFilter.php');
require_once(__DIR__ . '/filter/SecurityHeadersFilter.php');
 
require_once(__DIR__ . '/action/public/PublicAction.php');
require_once(__DIR__ . '/action/admin/AdminAction.php');
require_once(__DIR__ . '/action/structure/StructureAction.php');

require_once(__DIR__ . '/model/api/ApiResponse.php');