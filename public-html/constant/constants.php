<?php

define("VERSION", 56);

define('SESSION_REDIRECT_MESSAGE', 'SESSION_REDIRECT_MESSAGE');
define("JSON_CODE_ERROR", -1);
define("JSON_CODE_OK", 0);

class Constants {
    //Database Futura
    public static $db_user_name = '';
    public static $db_password = '';
    public static $db_host = '';
    public static $db_schema = '';
    
    //Database Alfa
    public static $db_enabled_alfa = true;
    public static $db_user_name_alfa = '';
    public static $db_password_alfa = '';
    public static $db_host_alfa = '';
    public static $db_schema_alfa = '';

    //Database Esis
    public static $db_enabled_esis = true;
    public static $db_user_name_esis = '';
    public static $db_password_esis = '';
    public static $db_host_esis = '';
    public static $db_schema_esis = '';

    //encrypt Key
    public static $encrypt_key = "";
    public static $encript_iv = "";
    
    //Path/URL
    public static $APP_PROTOCOL = "";
    public static $APP_URL = "";
    public static $APP_PRINT_NAME = "";
    public static $app_name = "";
    public static $ATTACHMENTS_DIR = "";
    public static $MAX_ATTACHMENTS_SIZE = 5000000;
    
    //SMTP
    public static $SMTP_FROM_NAME = "";
    public static $SMTP_FROM_MAIL = "";
    public static $SMTP_HOST = "";
    public static $SMTP_PORT = 587;
    public static $SMTP_MODE = '';
    public static $SMTP_USERNAME = "";
    public static $SMTP_PASSWORD = "";
    public static $ALLOWED_EMAIL_ADDRESSES = null;
    
    public static $maxSeenTimeDelay = 120;
    
    public static $defaultActionMethod = "_default";
    public static $onlineInterval = 60;
        
    public static $cookieSectret = "";
    public static $ENV_NAME = "";
    
    public static $devMode = false;
    public static $useVueDevServer = false;

    public static $errorReportingEnabled = false;

    public static $pattoAttivita = [
            [
                "value" => "Codocenze/compresenze",
                "key" => "o_1"
            ],
            [
                "value" => "Progetti inclusivi",
                "key" => "o_2"
            ],
            [
                "value" => "Altro",
                "key" => "o_altro"
            ]
        ];
}

class EnvName {
    public static $PROD = "prod";
    public static $TEST = "test";
    public static $DEV = "dev";
}

class Office365Constants {
    public static $CLIENT_ID = '';
    public static $CLIENT_SECRET = '';
    public static $REDIRECT_URI = '';
    public static $AUTHORITY_URL = '';
    public static $AUTHORIZE_ENDPOINT = '';
    public static $TOKEN_ENDPOINT = '';
    public static $RESOURCE_ID = '';
    public static $SCOPES= '';
}

class RecaptchaConstants {
    public static $ENABLED = false;
    public static $CLIENT_SECRET = '';
    public static $SERVER_SECRET = '';
    public static $CLIENT_INVISIBLE_SECRET = '';
    public static $SERVER_INVISIBLE_SECRET = '';
}

class RowStatus{
    const DELETED = "deleted";
    const READY = "ready";
}

class UserRole{
    const ADMIN = 1;
    const USER = 12;
    
    const DIRIGENTE = 2;
    const AMMINISTRATIVI = 13;
    const REFERENTE_BES = 5;
    const INSEGNANTE_PEI_PDP = 11;
    const INSEGNANTE = 26;
    const STUDENTE = 3;
    const COLLABORATORE_INTEGRAZIONE = 14;
    const INSEGNANTE_ANNO_DI_PROVA = 15;
    const AMMINISTRATORE_EDUCAZIONE_SALUTE = 16;
    const REFERENTE_EDUCAZIONE_SALUTE = 17;
    const REFERENTE_CYBERBULLISMO = 18;
    const AMMINISTRATORE_MODELLO_E = 19;
    const REFERENTE_MODELLO_E = 20;
    const AMMINISTRATORE_PEI_PDP = 21;
    const AMMINISTRATORE_PORTFOLIO = 22;
    const AMMINISTRATORE_STRUTTURE = 22;
    const AMMINISTRATORE_DROP_OUT = 24;
    const CORDINATORE_DROP_OUT = 25;
    const CORDINATORE_PLESSO_DROP_OUT = 31;
    
    const REFERENTE_PLESSO_SOMMINISTRAZIONI = 27;
    const AMMINISTRATORE_SOMMINISTRAZIONI = 28;
    const REFERENTE_ISTITUTO_SOMMINISTRAZIONI = 29;
    const INSEGNANTE_SOMMINISTRAZIONI = 30;

    const LEVEL_GLOBAL = "global";
    const LEVEL_CLIENT = "client";
    const LEVEL_PLESSO = "plesso";
    const LEVEL_CLASS = "class";
}

class MetadataEntity{
    const USER = "user";
    const USER_STRUCTURE = "user_structure";
}

class Metadata{
    const PEI_TIROCINIO_PROGETTO = "pei_tirocinio_progetto";
    const PEI_TIROCINIO_AZIENDA = "pei_tirocinio_azienda";
}

class UserPermission{
    const LAST_EDIT_USER_VIEW = "last_edit_user_view";

    const STRUCTURE_STUDENTS_READ_ALL = "structure_user_read_all";
    const STRUCTURE_ACCESS_ALL = "structure_access_all";

    const PORTFOLIO_TAB_SVILUPPO_ACCESS = "portfolio_tab_sviluppo_access";
    const PORTFOLIO_CONTENT_WRITE = "portfolio_content_write";

    const SOMMINISTRAZIONE_MODIFICA_DATI_BASE_STUDENTE = "somministrazione_modifica_dati_base_studente";
    const SOMMINISTRAZIONE_VISUALIZA_TUTTE = "somministrazione_visualizza_tutte";
    const SOMMINISTRAZIONE_NOTE_SOMMINISTRAZIONE_EDIT = "somministrazione_note_somministrazione_edit";
    const SOMMINISTRAZIONE_PLESSI_VISUALIZZA_TUTTI = "somministrazione_plessi_visualizza_tutti";
    const SOMMINISTRAZIONE_CLASSI_VISUALIZZA_TUTTI = "somministrazione_classi_visualizza_tutti";
    const SOMMINISTRAZIONE_REFERENTI_PLESSO_ADD = "somministrazione_referenti_plesso_add";
    const SOMMINISTRAZIONE_INSEGNANTI_CLASSE_ADD = "somministrazione_insegnanti_classe_add";
    const SOMMINISTRAZIONE_CLASSE_ADD = "somministrazione_classe_add";
    const SOMMINISTRAZIONE_STUDENTE_ADD = "somministrazione_studente_add";
    const somministrazione_studente_class_edit = "somministrazione_studente_class_edit";
    const SOMMINISTRAZIONE_CARICAMENTO_RISULTATI = "somministrazione_caricamento_risultati";
    const user_details = "user_details";
    const PEI_RECUPERA_NODO = "pei_recupera_nodo";

    const DROP_OUT_VIEW_ALL = "drop_out_view_all";
}

class DataSource{
    const FUTURA = "futura";
    const ALFA = "alfa";
    const ESIS = "esis";
}

class UserMetadata{
    const NAME = 'name';
    const SURNAME = 'surname';
    const SCHOOL_GRADE = 'school_grade';
    const CODE = 'code';
    const GENDER_ID = 'gender_id';
    const BIRTH_DATE = 'birth_date';
    const ICF_CODES = 'icf_codes';
    const ICD10_CODES = 'icd10_codes';
    const DEFICIT_VISIVO = 'deficit_visivo';
    const DEFICIT_UDITIVO = 'deficit_uditivo';
    const DEFICIT_PRASSICO = 'deficit_prassico';
    const CITTADINANZA = 'cittadinanza';
}

class UserStructureMetadata{
    const REFERENT_FOR = "referent_for";
}

class MessageStatus{
    const NEW_ = "NEW";
    const SENDED = 'SENDED';
    const ERROR = 'ERROR';
    const SKIPPED = 'SKIPPED';
}

class LogLevel{
    const INFO = 'INFO';
    const WARN = 'WARN';
    const ERROR = 'ERROR';
}

class Dictionary{
    const GENDER = "gender";
    const SCHOOL_YEAR = 'school_year';
    const CLASS_GRADE = 'class';
    const GRADE = 'grade';
    const PEI_LEVEL = "pei_level";
    const PEI_LEVEL_OLD = "pei_level_old";
    const PEI_CURRICULA_TYPE = "pei_curricula_type";
    const PEI_STRATEGIE_APPROCCI_METODOLOGICI = 'strategie_approcci_metodologici';
    const PEI_APPROCCI_METODOLOGICI = 'pei_approcci_metodologici';
    const PEI_MODALITA_VERIFICA = 'pei_modalita_verifica';
    const PEI_TERM = "pei_term";
    const PEI_DEFICIT = "pei_deficit";
    const PEI_ROW_TARGET_STATUS = "pei_row_target_status";
    const SOCIAL_OCCUPATION = "social_occupation";
    const DIAGNOSI_DOCUMENT_TYPE = "diagnosi_document_type";
    const DSM_VERSION = 'dsm_version';
    const YES_NO = 'yes_no';
    const DIAGNOSI_SERVIZI = 'diagnosi_servizi';
    const COMPROMISSIONE_LEVEL = 'compromissione_level';
    const TIPO_INTERVENTO = 'tipo_intervento';
    const DIAGNOSI_OPERATORE_TYPE = 'diagnosi_operatore_type';
    const STRATEGIE_COMPENSATIVE = 'strategie_compensative';
    const AUSILI_STRUMENTI_COMPENSATIVI = 'ausili_strumenti';
    const MISURE_DISPENSATIVE = 'misure_dispensative';
    const MODALITA_SVOLGIMENTO_PROVE = 'modalita_svolgimento_prove';
    const ATTACHMENT_TYPE = "attachment_type";
    const ED_SALUTE_TYPE = "ed_salute_type";
    const ED_SALUTE_STATUS = "ed_salute_status";
    const PROPOSTO_DA = "proposto_da";
    const SCHOOL_GRADE = "school_grade";
    const MODEL_E_DIAGNOSY_TYPE = "model_e_diagnosy_type";
    const MODEL_E_TIPO_FREQUENZA = "model_e_tipo_frequenza";
    const STRUCTURE_REFERENT = "structure_referent";

    const TIPO_PERMANENZA = "tipo_permanenza";
    const TIPOLOGIA_BES = "tipologia_bes";
    const PDP_LIN_RIFERIMENTI = "pdp_lin_riferimenti";
    const PDP_LIN_PUNTI_FORZA = "pdp_lin_punti_forza";
    const PDP_LIN_PUNTI_DEBOLEZZA = "pdp_lin_punti_debolezza";
    const PDP_LIN_LIVELLO_COMPETENZA = "pdp_lin_livello_competenze";
    const PDP_LIN_TIPOLOGIA_OBIETTIVI = "pdp_lin_tipologia_obiettivi";
    const PDP_LIN_ATTIVITA_INTERVENTI = "pdp_lin_attivita_interventi";
    const PDP_LIN_STRATEGIE_COMPENSATIVE = "pdp_lin_strategie_compensative";
    const PDP_LIN_STRUMENTI_COMPENSATIVI = "pdp_lin_strumenti_compensativi";
    const PDP_LIN_MISURE_DISPENSATIVE = "pdp_lin_misure_dispensative";
    const PDP_LIN_MODALITA_VERIFICA = "pdp_lin_modalita_verifica";
    const PDP_LIN_MODALITA_VALUTAZIONE_APP = "pdp_lin_modalita_valutazione_app";
    
    const DROP_OUT_MOTIVI = "drop_out_motivo";
    const DROP_OUT_ASSENZA_TYPE = "drop_out_assenza_type";
    const DROP_OUT_AZIONI = "drop_out_action";
    const DROP_OUT_ESITO = "drop_out_esito";
    const DROP_OUT_NON_AMMESSO_MOTIVAZIONI = "drop_out_non_ammesso_motivazioni";
    const DROP_OUT_STATO_ISCRIZIONE = "drop_out_stato_iscrizione";
    const DROP_OUT_NON_ISCRITTO_MOTIVAZIONI = "drop_out_non_iscritto_motivazioni";
    const DROP_OUT_ISCRITTO_SCUOLA = "drop_out_iscritto_scuola";

    const TEST_PERIODO = "test_periodo";
    const SOMMINISTRAZIONE_STASTUS = "somministrazione_status";
    const SOMMINISTRAZIONE_VALUTAZIONE = "somministrazione_valutazione";

    const LOG_LEVEL = "log_level";
    const ROLE_LEVEL = "role_level";

    const NAZIONALITA_ITA = "nazionalita_ita";

    const USER_TRANSFER_STATUS = "user_transfer_status";
    const USER_TRANSFER_TYPE = "user_transfer_type";

    const e_model_gravia = "e_model_gravia";
    const e_model_unita_oraria = "e_model_unita_oraria";
    const e_model_code = "e_model_code";
}

class PeiRowStatus{
    const _NEW = 'NEW';
    const SELECTED = 'SELECTED';
    const DELETED = "DELETED";
}

class PeiRowTargetStatus{
    const IN_PROGRESS = 'IN_PROGRESS';
    const ABORTED = 'ABORTED';
    const COMPLETED = 'COMPLETED';
}

class PdpSection{
    const PROFILE = 'profile';
    const ELABORA = 'elabora';
}

class UserStatus{
    const ENABLED = 'enabled';
    const DISABLED = 'disabled';
    const DELETED = 'deleted';
}

class StructureType{
    const STANDARD = "standard";
}

class DocumentType{
    const PEI = 'pei';
    const PEI_TIROCINIO = 'pei_tirocinio';
    const PDP = 'pdp';
    const PDP_LINGUA = 'pdp_lingua';
    const E_MODEL = 'e_model';
    const DROP_OUT = 'drop_out';
    const PORTFOLIO_INSEGNANTI = 'portfolio_insegnanti';
}

class Icon {
    const TRASH = "fas fa-trash-alt";
    const PENCIL = "fas fa-pencil-alt";
    const CANCELL = "fa fa-undo";
    const ADD = "fa fa-plus";
    const LIST = "fas fa-list";
    const SEARCH = "fas fa-search";
    const DATABASE = "fas fa-database";
    const PLUS = "fa fa-plus-circle";
    const RIGHT = "fas fa-arrow-right";
    const DOWNLOAD = "fas fa-download";
    const LOCK = "fas fa-lock";
    const CHECK_CIRCLE = "far fa-check-circle";
}

class NoticeUserChannel{
    const EMAIL = "email";
    const ANALYTICS = "analytics";
}

class NoticeUserType{
    const IMMEDIATE = "immediate";
    const DIGEST = "digest";
}

class AttachmentVisibility{
    const CUSTOM = "custom";
    const LOGGED_USER = "logged_user";
}

class AttachmentType{
    const AT_CONSENSO = "AT_CONSENSO";
    const AT_DIAGNOSI = "AT_DIAGNOSI";
    const PEI_PDP_DIAGNOSI = "AT_DIAGNOSI";
    const PEI_PDP_PDF = "AT_PDF";
    const PEI_PDP_CONSENSO = "AT_CONSENSO";
    const PEI_PDP_ALTRO = "AT_ALTRO";
    const pei_progetti_inclusivi = "pei_progetti_inclusivi";
    const pei_progetto_individuale = "pei_progetto_individuale";
    const pei_verbale = "pei_verbale";
    const at_certificazione_competenze_base = "at_certificazione_competenze";
    const at_certificazione_competenze_5p = "at_certificazione_competenze_5p";
    const at_certificazione_competenze_3p1 = "at_certificazione_competenze_3p1";
    const at_certificazione_competenze_5s2 = "at_certificazione_competenze_5s2";

    //Portfolio privato
    const PORTFOLIO_ATTIVITA_1 = "attivita_1";
    const PORTFOLIO_VALUTAZIONE_1 = "valutazione_1";
    const PORTFOLIO_VALUTAZIONE_2 = "valutazione_2";

    //Portfolio sotto sezione anno di prova
    const PF_AP_ANALISI_1 = "pf_ap_analisi_1";
    const PF_AP_ANALISI_2 = "pf_ap_analisi_2";
    const PF_AP_ANALISI_3 = "pf_ap_analisi_3";
    const PF_AP_ANALISI_4 = "pf_ap_analisi_4";
    const PF_AP_DOCUMENTAZIONE_1 = "pf_ap_documentazione_1";
    const PF_AP_DOCUMENTAZIONE_2 = "pf_ap_documentazione_2";
    const PF_AP_DOCUMENTAZIONE_3 = "pf_ap_documentazione_3";
    const PF_AP_DOCUMENTAZIONE_4 = "pf_ap_documentazione_4";
    const PF_AP_DOCUMENTAZIONE_5 = "pf_ap_documentazione_5";
    const PF_AP_DOCUMENTAZIONE_5_M_1 = "pf_ap_documentazione_5_M_1";
    const PF_AP_DOCUMENTAZIONE_5_M_2 = "pf_ap_documentazione_5_M_2";
    const PF_AP_DOCUMENTAZIONE_5_M_3 = "pf_ap_documentazione_5_M_3";
    const PF_AP_DOCUMENTAZIONE_5_M_4 = "pf_ap_documentazione_5_M_4";
    const PF_AP_DOCUMENTAZIONE_5_M_5 = "pf_ap_documentazione_5_M_5";

    //e Model
    const DIAGNOSI_REFERTO = "diagnosi_referto";

    //Portfolio pubblico
    const PORTFOLIO_ANALISI_1 = "analisi_1";
    const PORTFOLIO_ANALISI_2 = "analisi_2";
    const PORTFOLIO_ANALISI_3 = "analisi_3";
    const PORTFOLIO_ANALISI_4 = "analisi_4";
    const PORTFOLIO_DOCUMENTAZIONE_1 = "documentazione_1";
    const PORTFOLIO_DOCUMENTAZIONE_2 = "documentazione_2";
    const PORTFOLIO_DOCUMENTAZIONE_3 = "documentazione_3";
    const PORTFOLIO_DOCUMENTAZIONE_4 = "documentazione_4";
    const PORTFOLIO_DOCUMENTAZIONE_5 = "documentazione_5";
    const PORTFOLIO_MATERIALI_BES = "pf_materiali_bes";
    const PORTFOLIO_MATERIALI_INCLUSIONE = "pf_materiali_inclusione";
    const PORTFOLIO_MATERIALI_TEDESCO_L2 = "pf_materiali_tedesco_l2";
    const PORTFOLIO_MATERIALI_TECNOLOGIE_DIDATTICA = "pf_materiali_tecnologie_didattica";
    const PORTFOLIO_MATERIALI_TECNOLOGIE_VALUTAZIONE = "pf_materiali_valutazione";
    
    //ED_SALUTE
    const ED_SALUTE_DESCRIZIONE_PROGETTO = "descrizione_progetto";
    const ED_SALUTE_APPROVAZIONE_FINANZIARIA = "approvazione_finanziaria";

    //TEST
    const TEST_ISTRUZIONI = "test_istruzioni";
    const TEST_MATERIALI = "test_materiali";

    //Somministrazione
    const SOMMINISTRAZIONE_MATERIALI = "somministrazione_materiali";
    const SOMMINISTRAZIONE_TEST_MATERIALI = "somministrazione_test_materiali";

    const SOMMINISTRAZIONE_TEST_RESULT_CLASS_MATERIALI = "somministrazione_test_result_class_materiali";
    const SOMMINISTRAZIONE_TEST_RESULT_USER_NON_VALIDA = "somministrazione_test_result_user_non_valida";

    const PATTO_INCLUSIONE = "patto_inclusione";
}

class ModuleBaseAction{
    const PEI_PDP = "/structure/PEIAction";
    const PORTFOLIO_INSEGNANTI = "/structure/portfolio/PortfolioAction";
    const EDUCAZIONE_SALUTE = "/structure/edSalute/EdSaluteAction";
    const MODELLO_E = "/structure/eModel/EmodelAction";
    const DROP_OUT = "/structure/dropOut/DropOutUsersAction";
    const TEST_DIDATTICI = "/structure/test/SomministrazioniAction";

    static function getConstants() {
        $oClass = new ReflectionClass(__CLASS__);
        return $oClass->getConstants();
    }
}

class PropertyNamespace{
    const PEI_PDP = "pei_pdp";
    const PORTFOLIO_INSEGNANTI = "portfolio_insegnanti";
    const EDUCAZIONE_SALUTE = "educazione_salute";
    const MODELLO_E = "modello_e";
    const DROP_OUT = "drop_out";
    const TEST_DIDATTICI = "test_didattici";
    const RILEVAZIONE_CATTEDRE = "rilevazione_cattedre";
}

class PropertyKey{
    const ENABLED = "enabled";
    const primaria_current_year_id = "primaria_current_year_id";
    const secondaria_current_year_id = "secondaria_current_year_id";
}

class AttachmentStatus {
    const TEMP = "temp";
    const READY = "ready";
}

class EntityName {
    const SYSTEM = "system";
    const STRUCTURE = "structure";
    const DICTIONARY = "dictionary";
    const ED_SALUTE = "ed_salute";
    const USER = "user";
    const TEST = "test";
    const E_MODEL = "e_model";
    const SOMMINISTRAZIONE = "somministrazione";
    const SOMMINISTRAZIONE_TEST = "somministrazione_test";
    const SOMMINISTRAZIONE_TEST_RESULT_CLASS = "somministrazione_test_result_class";
    const SOMMINISTRAZIONE_TEST_RESULT_USER = "somministrazione_test_result_user";
    const USER_YEAR_DOCUMENT = "user_year_document";
    const DROP_OUT_ROW = "drop_out_row";
}

class UserSection {
    const PEI = "pei";
    const MONDO_PAROLE = "mondo_parole";
}

class UserTransferRequestType{
    const FROM_DESTINATION = "from_destination";
    const FROM_SOURCE = "from_source";
}

class NoticeType{
    const DOCUMENTS_DELETE_WARNING = "documents_delete_warnings";

    const PORTFOLIO_CLOSING = "portfolio_closing";

    const SCHOOL_YEAR_UPDATED = "school_year_updated";
    const STRUCTURE_CHECK_USERS = "structure_check_users";

    const USER_IMPERSONIFICATE = "user_impersonificate";
    const USER_ADD = "user_add";
    const USER_UPDATE = "user_update";
    const USER_ACCESS = "user_access";
    const USER_DELETED = "user_deleted";
    const USER_RETRIVE_DATA = "user_retrive_data";
    const USER_USER_ADD = "user_user_add";
    const USER_STRUCTURE_ADD = "user_structure_add";
    const USER_TRANSFER_ADD = "user_transfer_add";
    const USER_TRANSFER_UPDATE = "user_transfer_update";
    const USER_DIAGNOSI_NEW = "user_diagnosi_new";

    const E_MODEL_EDIT = "e_model_edit";
    const E_MODEL_APPROVED = "e_model_approved";

    const LOG_ERROR = "log_error";

    const DROP_OUT_NEW_DOCUMENT = "drop_out_new_document";
    const DROP_OUT_NEW_ROW = "drop_out_new_row";

    const MONDO_PAROLE_CAMPO_NOTE = "mondo_parole_campo_note";
}

class GlobalSessionData{
    const PREV_USER_ACCESS = "prev_user_access";
    const SELECTED_STRUCTURE_ID = "selected_structure_id";
    const DOCUMENT_TYPE = "documentType";
}

class UserYearDocumentStatus{
    const READY = "ready";
    const DELETED = "deleted";
}

class DropOutStatus {
    const GREEN = "green";
    const YELLOW = "yellow";
    const RED = "red";
}

class DropOutAzioneKey{
    const TELEFONATA_FAMIGLIA = "tel_fam";
    const CONVOCAZIONE_FAMIGLIA = "conv_fam";
    const LETTERA_FAMIGLIA = "lettera_famiglia";
    const SEGNALAZIONE_PROCURA = "segnalazione_procura";
    const SEGNALAZIONE_PROCURA_DICTIONARY_ID = 488;
    const ALTRO = "altro";
    const TRASFERIMENTO_DICTIONARY_ID = 549;
    const RIPRESA_FREQUENZA_DICTIONARY_ID = 494;
}

class DropOutRowType{
    const SEGNALAZIONE = "segnalazione";
    const RIPRESA_FREQUENZA = "ripresa_frequenza";
    const TRASFERIMENTO = "trasferimento";
    const CHIUSURA_ANNO = "chiusura_anno";
}

class DropOutEsito{
    const ammesso_classe_successiva_id = 524;
    const non_ammesso_classe_successiva = 526;
}

class DropOutStatoIscrizione{
    const iscritto_id = 527;
    const non_iscritto_id = 528;
}

class DropOutNonIscrittoMotivazioni{
    const abbandonmo_in_obbligo_id = 557;
    const abbandonmo_id = 558;
}

class SchedulerStatus{
    const READY = "ready";
    const RUNNING = "running";
    const ERROR = "error";
}

class PeiStatus {
    const RED = "red";
    const YELLOW = "yellow";
    const GREEN = "green";
}

class TestPeriodo{
    const INIZIALE = "iniziale";
    const FINALE = "finale";
}

class SomministrazioneStatus{
    const WAITING = "waiting";
    const OPEN = "open";
    const CLOSED = "closed";
}

class SomministrazioneUserStatus{
    const ASSENTE = "assente";
    const SOMMINISTRATA = "somministrata";
    const NON_VALIDA = "non_valida";
}

class TestVariableType{
    const RIGHTS = "rights";
    const ERRORS = "errors";
}

class ClassDictionaryValue{
    const C_3I = "3I";
}

class SchoolGrade{
    const INFANZIA = "infanzia";
    const PRIMARIA = "primaria";
}

class ServizioValutazioneConstants{
    const URL = "https://serviziovalutazione.futurabolzano.it";
}

class JournalAction {
    const ADDED = "added";
    const UPDATE = "update";
    const DELETE = "delete";
}

class JournalEntity {
    const PEI = "pei";
}

class JournalSection {
    const PEI_ANAGRAFICA = "pei_anagrafica";
    const PEI_DIAGNOSI = "pei_diagnosi";
    const PEI_PATTO = "pei_patto";
    const PEI_PLAN = "pei_plan";
    const PEI_PCTO = "pei_pcto";
    const PDP_LINGUA_PARTENZA = "pdp_lingua_partenza";
}

class RequestMethod{
    const GET = "GET";
    const POST = "POST";
}
