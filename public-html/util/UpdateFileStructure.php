<?php

require_once __DIR__ . '/../init.php';
EM::init();

restructureFiles();
prepareAttachments();

function myScanDir($path){
    $files = scandir($path);
    return array_diff($files, array('.', '..'));         
}

function getMimeType($extension){
    $res = EM::execQuerySingleResult("SELECT content_type from attachment where filename LIKE :filename",
        ["filename" => "%" . $extension]);
    if ($res != null){
        return $res["content_type"];
    }

    return null;
}

function prepareAttachments(){
    $usersPath = Constants::$ATTACHMENTS_DIR . "/user";
    $userIds = myScandir($usersPath);        

    foreach ($userIds as $userId){
        $userPath = $usersPath . "/" . $userId;
        $types = myScanDir($userPath);

        foreach($types as $type){
            $filesPath = $userPath . "/" . $type;
            $files = myScanDir($filesPath);        

            foreach ($files as $file) {
                $filePath = $filesPath ."/" . $file;

                $attachmentEntity = EM::execQuerySingleResult("SELECT * 
                    FROM attachment_entity ae 
                    LEFT JOIN attachment a ON ae.attachment_id=a.attachment_id
                    WHERE ae.entity=:entity AND ae.entity_id=:entity_id AND ae.type=:type AND a.filename=:filename", [
                        "entity" => EntityName::USER,
                        "entity_id" => $userId,
                        "type" => $type,
                        "filename" => $file
                    ]);
                
                if ($attachmentEntity == null){
                    echo "add attachment " . $file . "\n";

                    $extension = substr($file, strrpos($file, ".") + 1);
                    $mimeType = getMimeType($extension);
                    
                    $attachment = [
                        "filename" => $file,
                        "size" => filesize($filePath),
                        "content_type" => $mimeType,
                        "status" => AttachmentStatus::READY,
                        "insert_date" => "NOW()",
                        "path" => "/user/" . $userId . "/" . $type
                    ];
                    
                    $attachmentId = EM::insertEntity("attachment", $attachment);

                    EM::insertEntity("attachment_entity", [
                        "attachment_id" => $attachmentId,
                        "entity" => EntityName::USER,
                        "entity_id" => $userId,
                        "type" => $type,
                        "sort" => 0,
                        "insert_date" => "NOW()"
                    ]);
                }
            }
        }
    }
}

function restructureFiles(){
    $path = Constants::$ATTACHMENTS_DIR;
    $destPath = Constants::$ATTACHMENTS_DIR . "/user";
    if (!is_dir($destPath)){
        mkdir($destPath);
    }

    $files = scandir($path);        
    foreach ($files as $file){
        if ($file == '.' || $file == '..'){
            continue;
        }

        if (!is_numeric($file)){
            continue;
        }

        $user = UserService::find($file);
        if ($user == null){
            echo "No user " . $file .": delete dir\n";
            delTree($path . "/" . $file);
        }

        rename($path . "/" . $file, $destPath . "/" . $file);
    }
}

function delTree($dir) {
    $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
}