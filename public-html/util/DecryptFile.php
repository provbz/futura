<?php

require_once __DIR__ . '/../init.php';

// iterate over all files in the directory
$path = Constants::$ATTACHMENTS_DIR . '/decrypt';
$dir = new DirectoryIterator($path);
foreach ($dir as $fileinfo) {
    EncryptService::decryptfile($path . '/' . $fileinfo, $path . '/dec_' . $fileinfo );
}

