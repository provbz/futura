<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DateUtils
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DateUtils {
    const SECOND = 1;
    const MINUTE = 60;
    const HOUR = 3600;
    const DAY = 86400;
    const MONTH = 2592000;
    
    public static function utsToDate($uts){
        return date("Y-m-d", $uts);
    }
    
    public static function getDate($str){
        if (StringUtils::isNotBlank($str)){
            $a = mb_split(" ", $str);
            if (count($a) >= 1){
                return $a[0];
            }    
        }
        return "";
    }
    
    public static function getTime($str){
        if (StringUtils::isNotBlank($str)){
            $a = mb_split(" ", $str);
            if (count($a) == 2){
                return substr($a[1], 0, 5);
            }    
        }
        return "";
    }
    
    public static function convertDateToSQL($dataEur, $separator="-", $newSeparator="-"){
        $rsl = explode ($separator, $dataEur);
        if (count($rsl) > 1 && isset($rsl[2]) && strlen( $rsl[2] ) == 4){
            $rsl = array_reverse($rsl);
            return implode($newSeparator, $rsl);
        }
        return implode($newSeparator, $rsl);
    }
    
    public static function fillZero($arr){
        for ($i = 0; $i < count($arr); $i++) {
            if (strlen($arr[$i]) == 1){
                $arr[$i] = '0' . $arr[$i];
            }
        }

        return $arr;
    }

    public static function convertDate($dataEur, $separator="-", $newSeparator="-"){
        $rsl = explode ($separator, $dataEur);
        $rsl = self::fillZero($rsl);

        if (count($rsl) > 1 && isset($rsl[2]) && strlen( $rsl[2] ) == 4){
            return implode($newSeparator, $rsl);
        }

        $rsl = array_reverse($rsl);
        return implode($newSeparator, $rsl );
    }
    
    public static function formatSeconds($val){
        $sec = $val % 60;
        $min = intval( ($val / 60) % 60 );
        $hr = intval( ($min / 60) % 60 );
        
        if ($sec < 10){
            $sec = '0'.$sec;
        }
        if ($min < 10){
            $min = '0'.$min;
        }
        if ($hr < 10){
            $hr = '0'.$hr;
        }
        
        return $hr.':'.$min.':'.$sec;
    }
    
    public static function prettyFormat($date){
      
        $now = time();
        $delta = $now - strtotime( $date );
        
        if ($delta < 0){
          return "not yet";
        }
        if ($delta < 1 * self::MINUTE){
          return $delta == 1 ? "un secondo fa" : $delta . " secondi fa";
        }
        if ($delta < 2 * self::MINUTE){
          return "un minuto fa";
        }
        if ($delta < 45 * self::MINUTE){
          return (int)(date("i", $delta)) . " minuti fa";
        }
        if ($delta < 90 * self::MINUTE){
          return "un'ora fa";
        }
        if ($delta < 24 * self::HOUR){
          return (int) (date("H", $delta)) . " ore fa";
        }
        if ($delta < 48 * self::HOUR){
          return "ieri";
        }
        if ($delta < 30 * self::DAY){
          return  date("t", $delta) . " giorni fa";
        }
        if ($delta < 12 * self::MONTH){
          $months = (int) floor (date("t", $delta) / 30);
          return $months <= 1 ? "un  mese fa" : $months + " mesi fa";
        } else {
          $years = (int) floor (date("t", $delta) / 365);
          return $years <= 1 ? "un anno fa" : $years + " anni fa";
        }
    }
    
    public static function checkTime($value){
        $matches = [];
        preg_match("/^([0-2]?\d{1}):([0-5]?\d+)$/", $value, $matches);
        if (count($matches) < 3){
            return false;
        }
        
        $hr = (int) $matches[1];
        $min = (int) $matches[2];
        if ($hr > 23){
            return false;
        }
        if($min > 59){
            return false;
        }
        return $matches;
    }
    
}

?>
