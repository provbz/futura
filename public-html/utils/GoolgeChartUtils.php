<?php

class ChartData{
    public $cols = [];
    public $rows = [];
    
    public function addColumn($column){
        $this->cols[] = $column;
    }
    
    public function addRow($row){
        $this->rows[] = $row;
    }
    
}

class ChartColumn{
    public $id;
    public $label;
    public $pattern;
    public $type;
    
    public function __construct($id, $label, $pattern, $type ){
        $this->id = $id;
        $this->label = $label;
        $this->pattern = $pattern;
        $this->type = $type;
    }
}

class ChartRow{
    public $c = [];
    
    public function __construct($values = null) {
        if ($values != null){
            $this->addValues($values);
        }
    }
    
    public function addValues($values){
        foreach ($values as $value){
            $this->addValue($value);
        }
    }
    
    public function addValue($value){
        $this->c[] = new ChartRowValue($value);
    }
}

class ChartRowValue{
    public $v;
    
    public function __construct($value) {
        $this->v = $value;
    }
}

?>