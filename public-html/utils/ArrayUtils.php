<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ArrayUtils
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ArrayUtils {
    
    public static function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
                }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }
    
    public static function getIndex($array, $index, $default = null){
        if ($array == null || !isset($array[$index])){
            return $default;
        }
        return $array[$index];
    }
    
    public static function sum($array){
        $sum = 0;
        foreach($array as $value){
            if (is_array($value)){
                $sum += self::sum($value);
            } else {
                $sum += $value;
            }
        }

        return $sum;
    }

    public static function contains($array, $object){
        $objectJson = json_encode($object);

        foreach ($array as $item) {
            if (json_encode($item) == $objectJson){
                return true;
            }
        }

        return false;
    }

    public static function firstOrDefault($array, $func){
        foreach ($array as $value) {
            if ($func($value)){
                return $value;
            }
        }

        return false;
    }
}
