<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of WebClient
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class WebClient {
    const GET = "GET";
    const POST = "POST";
    const PUT = "PUT";
    const DELETE = "DELETE";
    
    public static $DEBUG_CALL = false;
    
    public static function exec($method, $url, $headers = null, $data = null, $authentication = null, $contentType = 'application/json'){
        $curl = curl_init();

        if (self::$DEBUG_CALL){
            echo "<br/><h2>New Request</h2>";
            echo $method . " " . $url.'<br/>';
            if ($data){
                echo 'Request Data: '. print_r($data, true) .'<br/<br/>';
            }
        }
        
        if ($method != self::GET && $contentType == 'application/json'){
            $data = json_encode($data);
        }

        switch ($method){
            case self::POST:
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: ' . $contentType));
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
                
                if ($data){
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case self::DELETE:
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: ' . $contentType));
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );

                if ($data){
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case self::PUT:
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: ' . $contentType,'Content-Length: ' . strlen( $data )));
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                break;
            default:
                if ($data){
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        
        if ($headers != null){
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        
        if (StringUtils::isNotBlank($authentication)){
            curl_setopt($curl, CURLOPT_USERPWD, $authentication);
        }
        
        $result = curl_exec($curl);
        $info = curl_getinfo($curl);
        
        if (self::$DEBUG_CALL){
            if ($result === false){
                echo "Error " . curl_error($curl).'<br/>';
            }
            echo "Response: " .$result.'<br/><br/>';
            echo "Info: <pre>" . print_r($info, true) ."</pre><br/><br/>";
        }

        curl_close($curl);
        
        $data = @json_decode($result);
        
        if ($data === null && json_last_error() !== JSON_ERROR_NONE) {
            return null;
        }
        
        return $data;
    }
    
}

