<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of RESTApp
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ServiceDescriptor{
    public $comment;
    public $action;
    public $request;
    public $response;
    
    public function __construct($comment, $action, $request, $response) {
        $this->comment = $comment;
        $this->action = $action;
        $this->request = $request;
        $this->response = $response;
    }
    
    public function draw(){
        $requestJson = '';
        if (StringUtils::isNotBlank($this->request) && class_exists($this->request)){
            $request = new $this->request();
            if ($request instanceof RESTDocumentation){
                $request->doc();
            }
            $requestJson = json_encode($request, JSON_PRETTY_PRINT);
        }
        
        $responseJson = '';
        if (StringUtils::isNotBlank($this->response) && class_exists($this->response)){
            $response = new $this->response();
            if ($response instanceof RESTDocumentation){
                $response->doc();
            }
            $responseJson = json_encode($response, JSON_PRETTY_PRINT);
        }
        ?>
        <?= $this->comment ?>
        <h4>Request</h4>
        <pre><?= $requestJson ?></pre>
        <h4>Response</h4>
        <pre><?= $responseJson ?></pre>
        <?php
    }
}

class Route{
    public $uri;
    public $regEx;
    
    /**
     *
     * @var ServiceDescriptor 
     */
    public $description;
}

class RESTApp{
    /**
     *
     * @var array
     */
    public $routes;
    
    public function __construct() {
        $this->routes = [
            'GET' => [],
            'POST' => []
        ];
    }
    
    private function printService(){
        foreach ($this->routes as $method => $rules){ ?>
            <div>
                <h2><?= $method ?></h2>
                <?php foreach ($rules as $rule){ ?>
                    <div>
                        <h3><?= $rule->uri ?></h3>
                        <div class='serviceDescription' style='padding-left: 30px;'>
                            <?php if ($rule->description != null){
                                $rule->description->draw();
                            } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php }
    }
    
    public function parse(){
        global $requestUri;
        
        if (isset( $_REQUEST['doc'])){
            $this->printService();
            return;
        }
        
        $method = $_SERVER['REQUEST_METHOD'];
        if (!isset($this->routes[$method])){
            echo 'invalid method ' . $method;
            return;
        }
        
        $executed = false;
        $request = str_replace("/rest", "", $requestUri);
        foreach ($this->routes[$method] as $route) {
            $matches = [];
            preg_match('/'.$route->regEx.'/', $request, $matches);
            if (count($matches) == 0){
                continue;
            }
            
            if (StringUtils::isNotBlank($route->description->action) && class_exists($route->description->action)){
                $executed = true;
                $action = new $route->description->action();
                
                try{
                    if ($method == 'POST'){
                        $requstBody = null;
                        if (StringUtils::isNotBlank($route->description->request) && class_exists($route->description->request)){
                            $requestBody = new $route->description->request();
                            $data = json_decode(file_get_contents('php://input'), true);
                            $requestBody->set($data);
                            $matches[1] = $requestBody;
                        }
                        $response = $action->exec($matches);
                    } else {
                        $response = $action->exec($matches);
                    }
                    
                    header('Content-Type: application/json');
                    echo json_encode($response);
                } catch (RESTException $e){
                    $this->http_response_code(400);
                    echo "error";
                }
            }
        }
        
        if (!$executed){
            $this->http_response_code(400);
            echo "Invalid request";
        }
    }
        
    public function http_response_code($code = NULL) {
        if ($code !== NULL) {

            switch ($code) {
                case 100: $text = 'Continue'; break;
                case 101: $text = 'Switching Protocols'; break;
                case 200: $text = 'OK'; break;
                case 201: $text = 'Created'; break;
                case 202: $text = 'Accepted'; break;
                case 203: $text = 'Non-Authoritative Information'; break;
                case 204: $text = 'No Content'; break;
                case 205: $text = 'Reset Content'; break;
                case 206: $text = 'Partial Content'; break;
                case 300: $text = 'Multiple Choices'; break;
                case 301: $text = 'Moved Permanently'; break;
                case 302: $text = 'Moved Temporarily'; break;
                case 303: $text = 'See Other'; break;
                case 304: $text = 'Not Modified'; break;
                case 305: $text = 'Use Proxy'; break;
                case 400: $text = 'Bad Request'; break;
                case 401: $text = 'Unauthorized'; break;
                case 402: $text = 'Payment Required'; break;
                case 403: $text = 'Forbidden'; break;
                case 404: $text = 'Not Found'; break;
                case 405: $text = 'Method Not Allowed'; break;
                case 406: $text = 'Not Acceptable'; break;
                case 407: $text = 'Proxy Authentication Required'; break;
                case 408: $text = 'Request Time-out'; break;
                case 409: $text = 'Conflict'; break;
                case 410: $text = 'Gone'; break;
                case 411: $text = 'Length Required'; break;
                case 412: $text = 'Precondition Failed'; break;
                case 413: $text = 'Request Entity Too Large'; break;
                case 414: $text = 'Request-URI Too Large'; break;
                case 415: $text = 'Unsupported Media Type'; break;
                case 500: $text = 'Internal Server Error'; break;
                case 501: $text = 'Not Implemented'; break;
                case 502: $text = 'Bad Gateway'; break;
                case 503: $text = 'Service Unavailable'; break;
                case 504: $text = 'Gateway Time-out'; break;
                case 505: $text = 'HTTP Version not supported'; break;
                default:
                    exit('Unknown http status code "' . htmlentities($code) . '"');
                break;
            }

            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
            header($protocol . ' ' . $code . ' ' . $text);
            $GLOBALS['http_response_code'] = $code;
        } else {
            $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);
        }

        return $code;
    }
    
    public function get($uri, $comment){
        $this->addRoute('GET', $uri, $comment);
    }
    
    public function post($uri, $comment){
        $this->addRoute('POST', $uri, $comment);
    }
    
    public function addRoute($method, $uri, $comment){
        $route = new Route();
        $route->uri = $uri;
        
        $regEx = str_replace("/", "\/", $route->uri);
        $regEx = str_replace("{", "(?P<", $regEx);
        $regEx = str_replace("}", ">\w+)", $regEx);
        
        $route->regEx = $regEx;
        
        $baseAction = str_replace("/", "", $route->uri);
        $baseAction = preg_replace("/{(\w+)}/", "", $baseAction);
        $baseAction = ucfirst( $baseAction );
        
        $action = 'REST'. $baseAction .'Action';
        $request = 'REST'. $baseAction .'Request';
        $response = 'REST'. $baseAction .'Response';
        
        $route->description = new ServiceDescriptor($comment, $action, $request, $response);
        array_push($this->routes[$method], $route);
    }
}

class RESTException extends ErrorException{
    
}

class RESTBaseAction{
    
}

interface RESTDocumentation{
    public function doc();
}

class JSONObject {
    public function set($data) {
        if (!is_array($data)){
            throw new RESTException();
        }
        
        foreach ($data AS $key => $value){
            $this->{$key} = $value;
        }
    }
}