<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of StringUtils
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

function _t($str){
    if (is_numeric($str)){
        return $str;
    }
    if (is_bool($str)){
        return $str;
    }
    if ($str == null || !is_string($str)){
        return null;
    }
    
    return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
}

function getValue($name, $default = ""){
    $value = getRequestValue($name, $default);
    if ($value == $default){
        $value = getSessionValue($name, $default);
        if ($value == $default){
            return $default;
        }
    }
    return $value;
}

function getRequestValue($name, $default = ""){
    $res = $default;
    if (isset($_REQUEST[$name])){
        $res = $_REQUEST[$name];
    }
    return $res;
}

function getSessionValue($name, $default = ""){
    $res = $default;
    
    if (isset($_SESSION[$name])){
        $res = $_SESSION[$name];
    }
    return $res;
}

function GUID()
{
    if (function_exists('com_create_guid') === true)
    {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

function booleanIntToString($value){
    if ($value == 0){
        return 'No';
    } else {
        return 'Si';
    }
}

function decToBase36($val){
    $numbers =  'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    $base = 36;

    $res = "";
    while ($val >= $base){
        $c = $val % $base;
        $res = $numbers[$c] . $res;
        $val = $val / $base;
    }
    $res = $numbers[$val] . $res;
    return $res;
}

function base36ToDec($val){
    $numbers =  'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    $res = 0;
    $base = 36;
    $exp = strlen($val) - 1;
    for ($i = 0; $i < strlen($val); $i ++){
        $res += strpos($numbers, $val[$i]) * pow($base, $exp);
        $exp--;
    }
    return $res;
}

function base36CheckSum($str){
    $res = base36ToDec("99");

    if (strlen($str) != 14){
        echo "error ".$str."<br/>";   
    }

    for ($i = 0; $i < strlen($str); $i += 2){
        $chunk = base36ToDec($str[$i].$str[$i + 1]);
        $res = $res ^ $chunk;
        $res = $res % 1295;
    }

    $hash = decToBase36($res);
    if (strlen($hash) < 2){
        for ($i = strlen($hash); $i < 2; $i++){
            $hash = "A".$hash;
        }
    }
    return $hash;
}

function md5_64bit($str){
    $md5 = md5($str);

    $base = hexdec("FFFFFFFF");
    
    $s0 = substr($md5, 0, 8);
    $s1 = substr($md5, 8, 8);
    $s2 = substr($md5, 16, 8);
    $s3 = substr($md5, 24, 8);
    
    $n1 = $base ^ hexdec($s0) ^ hexdec($s2);
    $n2 = $base ^ hexdec($s1) ^ hexdec($s3);

    $s1 = preFillWith(dechex($n1), '0', 8);
    $s2 = preFillWith(dechex($n2), '0', 8);
    
    return $s1.$s2;
}

function preFillWith($str, $fill, $minLength){
    $res = $str;
    for ($i = strlen($res); $i < $minLength; $i++){
        $res = $fill.$res;
    }
    return $res;
}

function redirect($url, $removeHeader = true){
    if ($removeHeader){
        header_remove();
    }
    header("Location: ".Constants::$APP_PROTOCOL.  Constants::$APP_URL . $url);
    die();
}

function redirectWithMessage($url, $message){
    header_remove();
    $_SESSION[SESSION_REDIRECT_MESSAGE] = $message;
    header("Location: ".Constants::$APP_PROTOCOL.  Constants::$APP_URL.$url);
    die();
}

function DESDecode($str, $key, $iv){
    $str = mcrypt_cbc(MCRYPT_DES, $key, base64_decode($str), MCRYPT_DECRYPT, $iv);

    for ($i = strlen($str) - 1; $i > 0; $i --){
        $ascii = ord($str[$i]);
        if ($ascii > 31){
            $str = substr($str, 0, $i + 1);
            break;
        }
    }

    return $str;
}

function DESEncode($str, $key, $iv){
    return mcrypt_cbc(MCRYPT_DES, $key, base64_encode($str), MCRYPT_ENCRYPT, $iv);
}

function checkPagePermission($functionId){
    if (!UserPermissionService::canUser($functionId)){
        echo 'Permesso negato!';
        include_once 'template/footer.php';
        die();
    }
}

function isSubmit(){
    if (getRequestValue("submit_check", NULL) != NULL){
        return true;
    }
    return false;
}