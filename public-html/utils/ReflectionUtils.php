<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ReflectionUtils
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ReflectionUtils {
    
    public static function fillWithRequestParameters($obj){
        foreach ($_REQUEST as $key => $value){
            if(property_exists($obj, $key)){
                $obj->$key = $value;
            }
        }
    }
    
    public static function getPropertyWithPrefix($source, $prefix){
        $res = [];
        foreach ($source as $key => $value){
            if (strpos($key, $prefix) === 0){
                $key = substr($key, strlen($prefix));
                $res[$key] = $value;
            }
        }
        return $res;
    }
    
    /**
     * 
     * @param type $obj
     */
    public static function fillWithSessionOrRequestParameters($obj){
        foreach ($_SESSION as $key => $value){
            $key = str_replace(get_class($obj). '_', "", $key);
            self::setValue($obj, $key, $value);
        }
        foreach ($_REQUEST as $key => $value){
            self::setValue($obj, $key, $value);
        }
    }
    
    private static function setValue($obj, $key, $value){
        $reflect = new ReflectionObject($obj);
        if($reflect->hasProperty($key)){
            $prop = $reflect->getProperty($key);
            if (! $prop->isPrivate()){
                $obj->$key = $value;
            }
        }
    }
    
    public static function fillWithPrefix($from, $prefix, &$to){
        if ($from == NULL){
            return;
        }
        
        foreach ($from as $key => $value){
            $property = $prefix.$key;
            if (is_array($to)){
                $to[$property] = $value;
            } else {
                if (property_exists($to, $property)){
                    $to->$property = $value;
                }
            }
        }
    }
    
    public static function getValue($dictionary, $key, $default = ''){
        if ($dictionary == NULL || !isset($dictionary[$key])){
            return $default;
        }
        return $dictionary[$key];
    }
    
    public static function copyKeys($source, $keys = NULL){
        $out = [];
        
        foreach ($source as $key => $value){
            if ($keys == NULL || array_search($key, $keys, true) !== false){
                $out[$key] = $value;
            }
        }
        
        return $out;
    }
    
    public static function copyNotNumericKeys($source){
        $out = [];
        foreach ($source as $key => $value){
            if (!is_numeric($key)){
                $out[$key] = $value;
            }
        }
        return $out;
    }
}

