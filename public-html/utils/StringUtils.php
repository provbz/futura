<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of StringUtils
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class StringUtils {
    
    public static function isJSON($str){
        if (StringUtils::isBlank($str)){
            return null;
        }
        
        json_decode($str);
        return (json_last_error() == JSON_ERROR_NONE);
    }
    
    public static function escapeSOLR($str){
        $SOLR_SPECIAL_CHARS = ["\\", "+", "-", "&", "|", "!", "(", ")", "{", "}", "[", "]", "^", "\"", "~", "*", "?", ":"]; 
        $SOLR_ESCAPED_CHARS = ["\\\\", "\\+", "\\-", "\\&", "\\|", "\\!", "\\(", "\\)", "\\{", "\\}", "\\[", "\\]", "\\^", '\"', "\\~", "\\*", "\\?", "\\:"]; 

        return str_replace($SOLR_SPECIAL_CHARS, $SOLR_ESCAPED_CHARS, $str);
    }
    
    public static function isBlank($str){
        if ($str === NULL){
            return true;
        }
        $str = trim($str);
        if ($str == ""){
            return true;
        }
        return false;
    }
    
    public static function isNotBlank($str){
        return !StringUtils::isBlank($str);
    }
    
    public static function truncate($text, $nbrChar = null, $append = ' ...') {
        if(strlen($text) > $nbrChar) {
            while ($text[$nbrChar] != " " || $nbrChar == 0){
                $nbrChar--;
            }
            $text = substr($text, 0, $nbrChar);
            $text .= $append;
        }
        return $text;
    }
    
    public static function generateRandomChars($length = 20, $letters = true, $numbers = true, $case = 'i'){
        $chars = array();

        if ($numbers){
            $chars = array_merge($chars, range(48, 57));
        }

        if ($letters OR !$numbers){
            $chars = array_merge($chars, range(65, 90), range(97, 122));
        }

        $string = "";
        do{
            $i = rand (0, count($chars) - 1);
            $string .= chr($chars[$i]);
        }while(strlen($string) < $length);

        switch ($case){
            case 'i': default: return $string;
            case 'u': return strtoupper($string);
            case 'l': return strtolower($string);
        }
    }
    
    public static function validEmail($email){
       $isValid = true;
       $atIndex = strrpos($email, "@");
       if (is_bool($atIndex) && !$atIndex){
          return false;
       } 
       
        $domain = substr($email, $atIndex+1);
        $local = substr($email, 0, $atIndex);
        $localLen = strlen($local);
        $domainLen = strlen($domain);
        if ($localLen < 1 || $localLen > 64)
        {
           // local part length exceeded
           $isValid = false;
        }
        else if ($domainLen < 1 || $domainLen > 255)
        {
           // domain part length exceeded
           $isValid = false;
        }
        else if ($local[0] == '.' || $local[$localLen-1] == '.')
        {
           // local part starts or ends with '.'
           $isValid = false;
        }
        else if (preg_match('/\\.\\./', $local))
        {
           // local part has two consecutive dots
           $isValid = false;
        }
        else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
        {
           // character not valid in domain part
           $isValid = false;
        } else if (preg_match('/\\.\\./', $domain)){
           // domain part has two consecutive dots
           $isValid = false;
        } else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',
                   str_replace("\\\\","",$local)))
        {
           // character not valid in local part unless 
           // local part is quoted
           if (!preg_match('/^"(\\\\"|[^"])+"$/',
               str_replace("\\\\","",$local)))
           {
              $isValid = false;
           }
        }
        /*if ($isValid && !(checkdnsrr($domain,"TXT") || checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
        {
           // domain not found in DNS
           $isValid = false;
        }*/
          
       return $isValid;
    }
    
    public static function addChars($char, $num){
        $out = "";
        for ($i = 0; $i < $num; $i++){
            $out .= $char;
        }
        return $out;
    }
    
    public static function clean($value){
        return trim($value);
    }
    
    public static function onlyAlphaNumeric($str){
        return preg_replace("/[^A-Za-z0-9 ]/", '', $str);
    }
    
    public static function formatPrice($value){
        return number_format($value, 2, ",", ".");
    }

    public static function checkPIva($variabile){ 
        if($variabile=='') 
            return false; //la p.iva deve essere lunga 11 caratteri 
        if(strlen($variabile)!=11) 
            return false; //la p.iva deve avere solo cifre 
        if(preg_match ("/^[0-9]+$/", $variabile) === false) {
            return false; 
        }
        
        $primo=0; 
        for($i=0; $i<=9; $i+=2) 
            $primo += ord($variabile[$i])-ord('0'); 
        for($i=1; $i<=9; $i+=2 ){ 
            $secondo = 2 * ( ord($variabile[$i])-ord('0') ); 
            if($secondo>9) 
                $secondo=$secondo-9; 
            $primo+=$secondo; 
            
        } 
        if( (10-$primo%10)%10 != ord($variabile[10])-ord('0') ) 
            return false; 
        
        return true;         
    } 
    
    public static function checkCodiceFiscale($cf){ 
        if($cf=='') {
            return false; 
        }
        if(strlen($cf)!= 16) {
            return false; 
        }
        $cf=strtoupper($cf); 
        if(!preg_match("/[A-Z0-9]+$/", $cf)) {   
            return false; 
        }
        
        $s = 0; 
        for($i=1; $i<=13; $i+=2){ 
            $c=$cf[$i]; 
            if('0'<=$c and $c<='9') 
                $s+=ord($c)-ord('0'); 
            else $s+=ord($c)-ord('A');     
        } 
        
        for($i=0; $i<=14; $i+=2){ 
            $c=$cf[$i]; 
            switch($c){ 
                case '0': 
                    $s += 1; 
                    break; 
                case '1': 
                    $s += 0; 
                    break; 
                case '2': $s += 5; break; 
                case '3': $s += 7; break; 
                case '4': $s += 9; break; 
                case '5': $s += 13; break; 
                case '6': $s += 15; break; 
                case '7': $s += 17; break; 
                case '8': $s += 19; break; 
                case '9': $s += 21; break; 
                case 'A': $s += 1; break; 
                case 'B': $s += 0; break; 
                case 'C': $s += 5; break; 
                case 'D': $s += 7; break; 
                case 'E': $s += 9; break; 
                case 'F': $s += 13; break; 
                case 'G': $s += 15; break; 
                case 'H': $s += 17; break; 
                case 'I': $s += 19; break; 
                case 'J': $s += 21; break; 
                case 'K': $s += 2; break; 
                case 'L': $s += 4; break; 
                case 'M': $s += 18; break; 
                case 'N': $s += 20; break; 
                case 'O': $s += 11; break; 
                case 'P': $s += 3; break; 
                case 'Q': $s += 6; break; 
                case 'R': $s += 8; break; 
                case 'S': $s += 12; break; 
                case 'T': $s += 14; break; 
                case 'U': $s += 16; break; 
                case 'V': $s += 10; break; 
                case 'W': $s += 22; break; 
                case 'X': $s += 25; break; 
                case 'Y': $s += 24; break; 
                case 'Z': $s += 23; break;                 
            } 
            
        } 
        if( chr($s%26+ord('A'))!=$cf[15] ) 
            return false; 
        
        return true; 
    }            
            
    public static function formatTimeFromMinutes($min){
        $hh = intval($min / 60);
        $mm = $min - $hh * 60;
        return sprintf("%02d:%02d", $hh, $mm);
    }
    
    public static function checkPassword($password){
        return preg_match_all('$\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$', $password);
    }
}
