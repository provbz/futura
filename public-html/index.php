<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
*/

/**
 * Description of AdminAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
date_default_timezone_set("Europe/Rome");

function exception_error_handler($errno, $errstr, $errfile, $errline ) {
    if (strstr($errfile, "MPDF") !== false){
        return;
    }
    
    // Log dell'errore
    $log_message = date('Y-m-d H:i:s') . " - Error $errno: $errstr in $errfile on line $errline";
    //error_log($log_message, 3, 'error.log'); // Registra l'errore in un file di log chiamato 'error.log'

    // Visualizzazione dell'errore, se visualizzazione errori è attiva
    if (ini_get('display_errors')) {
        echo "<p><strong>Errore:</strong> $errstr in $errfile on line $errline</p>";
    }

    throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
}

ini_set('session.cookie_httponly', 'On');
session_name("PEISID");
session_start();

header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time()));

$mantainanceMode = false;
if ($mantainanceMode){
    if (isset($_REQUEST["skip"])){
        $_SESSION["skip"] = $_REQUEST["skip"];
    } 
    if (isset($_SESSION["skip"])){
        $mantainanceMode = !$_SESSION["skip"];
    }
}

if ($mantainanceMode){
    ?>
        <htrml>
        </htrml>
        <body>
        <center>
            <h3>Sistema in fase di manutenzione</h3>
            <p>La piattaforma tornerà presto operativa.</p>
        </center>
        </body>
    <?php
    die();
}

// Gestione corretta dell'indirizzo IP del client
$requestIp = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

// Configura $_SERVER['HTTPS'] se la connessione è HTTPS
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
    $_SERVER['HTTPS'] = 'on';
}

// Abilita session.cookie_secure se la connessione è sicura
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
    ini_set('session.cookie_secure', 'On');
}

require_once('./init.php');

if (constants::$errorReportingEnabled) {
    error_reporting(E_ALL | E_STRICT);
    ini_set('display_errors', 'On');
    set_error_handler("exception_error_handler");
}

$app_name  = '';
$currentUser = NULL;
$currentUserRoles = [];
$currentUserAllowedAction = [];

$requestUri = str_replace($app_name, "", $_SERVER['REQUEST_URI']); 

if (Constants::$ENV_NAME != EnvName::$TEST && BannedIpService::isBannedIp($requestIp)){
    header("HTTP/1.0 404 Not Found");
    die();
}

if (strpos($requestUri, "/wp") !== false || strpos($requestUri, "/wp-admin") !== false){
    EM::insertEntity("banned_ip", [
        "ip" =>  ArrayUtils::getIndex($_SERVER, 'REMOTE_ADDR'),
        "insert_date" => "NOW()"
    ]);
}

$queryString = $_SERVER['QUERY_STRING'];
$requestUri = str_replace('?'.$queryString, "", $requestUri);

$requestPackage = substr($requestUri, 0, strrpos($requestUri, "/"));
$requestAction = substr(strrchr($requestUri, "/"), 1);
$requestMethod = NULL;

if (strpos($requestPackage, "/files") === 0){
    $requestPackage = "/files";
    $requestAction = "GetFileAction";
} else if (StringUtils::isBlank($requestAction)){
    $requestPackage = "/public";
    $requestAction = "LoginAction";
    $requestMethod = NULL;
} else {
    $requestMethod = substr(strrchr($requestAction, "-"), 1);
    $requestAction = str_replace("-".$requestMethod, "", $requestAction);
}

if (StringUtils::isBlank( $requestMethod )){
    $requestMethod = "_default";
}

$actionPath = 'action/'.$requestAction.'.php';
if (StringUtils::isNotBlank($requestPackage)){
    $actionPath = __DIR__ . '/action'.$requestPackage.'/'.$requestAction.'.php';
}

if (file_exists($actionPath) !== true){
    if (strpos($requestPackage, "/api/") === 0){
        http_response_code(400);
        die();
    }

    $requestAction = "AccessDenyAction";
    $actionPath = __DIR__ . '/action/public/'.$requestAction.'.php';
    $requestMethod = "_default";
}

require_once $actionPath;
$action = new $requestAction();

// Filtri
LoginFilter::exec();
RequestFilter::exec();
SelectStructureFilter::exec();
AccessCheckFilter::exec();
SecurityHeadersFilter::exec();

if(method_exists($action, "_prepare")){
    $action->_prepare();
}

if (!method_exists ($action, $requestMethod)){
    redirect(UriService::accessDanyActionUrl());
}

$action->$requestMethod();
