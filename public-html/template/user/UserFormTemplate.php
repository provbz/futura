<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserFormTemplate
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserFormTemplate {
    
    public static function buildPasswordField($user){
        $password1Field = new FormSinglePasswordField("Password", "password1", FormFieldType::STRING);
        $password1Field->note = "La password deve essere almeno di 8 caratteri, contenerenumeri, lettere maiuscole, minuscole e caratteri di punteggiatura o simboli ad esempio: \",.?!<>£$%&\".";
        $password1Field->checkFunction = function($field, $password) use ($user){
            if (!StringUtils::checkPassword($password)){
                $field->addFieldError("La password deve essere almeno 8 caratteri, "
                        . "deve contenere lettere maiuscole e minuscole, numeri e caratteri non alfanumerici.");
            }
            if ($user != NULL && !UserService::checkLastPassword($user, $password)){
                $field->addFieldError("La password coincide con una delle ultime tre inserite.");
            }
        };
        return $password1Field;
    }
    
    public static function buildPassword2Field(){
        $password2Field = new FormSinglePasswordField("Conferma password", "password2", FormFieldType::STRING);
        $password2Field->checkFunction = function($field, $password2){
            $password1 = getRequestValue("password1");
            if ($password2 !== $password1){
                $field->addFieldError("Le password non coincidono!");
            }
        };
        return $password2Field;
    }
    
}
