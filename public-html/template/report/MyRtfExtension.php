<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MyRtfExtension
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once './lib/PHPRtfLite/PHPRtfLite.php';

class MyRtfExtension extends PHPRtfLite{
    
    private $imagesUrls;
    
    public function __construct() {
        PHPRtfLite::registerAutoloader();
        parent::__construct();
        
        $this->imagesUrls = [];
    }
    
    public function drawHtml(){
        ?>
        <html>
            <head>
                <style type="text/css">
                    body{
                        background-color: #DDDDDD;
                    }
                    section{
                        width: 450px;
                        background-color: #FFFFFF;
                        min-height: 630px;
                        margin: 0 auto;
                        border: 1px solid #000000;
                        padding: 50px 40px 30px 40px;
                        margin-bottom: 10px;
                    }
                </style>
            </head>
            <body>
        <?php
        $imageIndex = 0;
        foreach ($this->_sections as $section){
            echo '<section>';
            foreach ($section->getElements() as $key => $element){
                if ($element instanceof PHPRtfLite_Image_Gd){
                    if (isset($this->imagesUrls[$imageIndex])){
                        echo '<img src="'.$this->imagesUrls[$imageIndex].'"/>';
                    }
                } else if ($element instanceof PHPRtfLite_Element){
                    $font = $element->getFont();
                    $format = $element->getParFormat();
                    $alignment = 'left';
                    if ($format != NULL){
                        $alignment = $format->getTextAlignment();
                    }

                    echo '<p style="font-size: '. $font->getSize() .'px; text-align:'.$alignment.';">';
                    echo $this->textToHtml( $element->_text );
                    echo '</p>';
                } else if ($element instanceof PHPRtfLite_Table){
                    echo '<table>';
                    foreach ($element->getRows() as $row){
                        echo '<tr>';
                        foreach ($element->getColumns() as $column){
                            $cell = $element->getCell($row->getRowIndex(), $column->getColumnIndex());
                            echo '<td style="width: '. ($column->getWidth() * 72) .'px;">';
                            foreach ($cell->getElements() as $el){
                                if ($el instanceof PHPRtfLite_Element){
                                    echo $el->_text;
                                }
                            }
                            echo '</td>';
                        }
                        echo '</tr>';
                    }
                    echo '</table>';
                } else if ($element instanceof PHPRtfLite_List_Enumeration){
                    echo '<ul>';
                    foreach($element->_items as $item){
                        echo '<li>'.$item->_text.'</li>';
                    }
                    echo '</ul>';
                }
            }
            echo '</section>';
        }
        ?>
            </body>
        </html>
        <?php
    }
    
    protected function textToHtml($text){
        return str_replace("\n", "<br/>", $text);
    }
    
    public function addImage($section, $path, $url = NULL, $width = 0, $height = 0){
        if (StringUtils::isNotBlank($url)){
            $this->imagesUrls[] = $url;
        }
        
        $image = $section->addImage($path);
        if ($height > 0){
            $image->setHeight($height);
        }
        if ($width > 0){
            $image->setWidth($width);
        }
    }
}
