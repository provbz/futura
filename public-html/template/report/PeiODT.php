<?php
/*
  -----------------------------------------------------------------------------------------
  This file is part of the application Futura

  Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/).

  This program is free software: you can redistribute it and/or modify it under the terms of
  the Affero GNU General Public License as published by the Free Software Foundation, either
  version 3 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the Affero GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this program.
  If not, see <http://www.gnu.org/licenses/>.
  -----------------------------------------------------------------------------------------
 */

/**
 * Description of PeiODT
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once './template/report/Pei.php';

//require_once './vendor/autoload.php';
class PeiODT extends Pei {

    const COPERTINA = "copertina";
    const ANAGRAFICA = "anagrafica";
    const SITUAZIONE_PARTENZA = "situazione_partenza";
    const COMPETENZE_LINGUISTICHE = "competenze_linguistiche";
    const DIAGNOSI = "diagnosi";
    const OSSERVAZIONI = "osservazioni";
    const OSSERVAZIONI_DESCRIZIONE = "osservazioni_descrizione";
    const CALENDARIO = "calendario";
    const PATTO = "patto";
    const STYLE_TITLE = "Title";
    const PCTO = "pcto";
    const PROPOSTA_PROGETTO = "proposta_progetto";
    const AZIENDE = "aziende";
    
    const ODT = "odt";
    const DOC = "docx";
    const PDF = "pdf";

    private $styleTitle1 = ['size' => 22];
    private $styleTitle2 = ['size' => 17];
    private $styleTitle3 = ['size' => 12];
    private $styleBold = ['bold' => true];
    private $styleTabellaStruttura = 'Tabella';
    private $currentSection;
    public $title;
    
    public $subTitle;
    public $typefile;
    public $code;
    
    public function draw() {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);

        $properties = $phpWord->getDocInfo();
        $properties->setTitle('PEI');
        $properties->setDescription('Piano Educativo Individualizzato');

        $this->setupStyles($phpWord);

        $this->currentSection = $phpWord->addSection();
        $this->currentSection->getStyle()->setPageNumberingStart(1);
        $footer = $this->currentSection->addFooter();
        $footer->addPreserveText('{PAGE}/{NUMPAGES}', null, ['align' => 'right']);
        
        if (array_search(PeiODT::COPERTINA, $this->content) !== false) {
            $this->addCopertina($phpWord);
        }

        if ($this->pei["type"] == DocumentType::PEI_TIROCINIO){
            $this->peiTirocinioIntro($phpWord);
        }

        if (array_search(PeiODT::ANAGRAFICA, $this->content) !== false) {
            $this->addAnagrafica($phpWord);
        }

        if (array_search(PeiODT::PROPOSTA_PROGETTO, $this->content) !== false) {
            $this->addPropostaProgetto($phpWord);
        }

        if (array_search(PeiODT::AZIENDE, $this->content) !== false) {
            $this->addAziende($phpWord);
        }

        if (array_search(PeiODT::SITUAZIONE_PARTENZA, $this->content) !== false) {
            $this->addSituazionePartenza($phpWord);
        }

        if (array_search(PeiODT::COMPETENZE_LINGUISTICHE, $this->content) !== false) {
            $this->addCompetenzeLinguistiche($phpWord);
        }

        if ($this->pei["type"] == DocumentType::PEI){
            if (array_search(PeiODT::PCTO, $this->content) !== false) {
                $this->addPCTO($phpWord);
            }
        }

        if (array_search(PeiODT::OSSERVAZIONI, $this->content) !== false) {
            $this->addOsservazioni($phpWord);
        }

        if (array_search(PeiODT::CALENDARIO, $this->content) !== false) {
            $this->addCalendario($phpWord);
        }

        if (array_search(PeiODT::PATTO, $this->content) !== false) {
            $this->addPatto($phpWord);
        }

        $this->download($phpWord);
    }

    private function setupStyles($phpWord) {
        $phpWord->addFontStyle(self::STYLE_TITLE, array('size' => 20));
        $phpWord->addFontStyle("bold", ['bold' => true]);

        $phpWord->addTableStyle($this->styleTabellaStruttura, [
            'width' => 17.013
        ]);
    }

    private function download($phpWord) {
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="' .$this->code .'_'. $this->title .'.'.$this->typefile.'"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        \PhpOffice\PhpWord\Settings::setPdfRendererPath(__DIR__ .  '/../../vendor/dompdf/dompdf');
        \PhpOffice\PhpWord\Settings::setPdfRendererName(\PhpOffice\PhpWord\Settings::PDF_RENDERER_DOMPDF);

        $objWriter = null;
        if($this->typefile == self::DOC){
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        } else if($this->typefile == self::ODT){
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'ODText');
        } else if($this->typefile == self::PDF){
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
        }

        // ibreoffice --headless --convert-to odt *.docx

        if ($objWriter != null){
            $objWriter->save("php://output");
        }
        die();
    }

    private function addCopertina($phpWord) {
        $currentYear = SchoolYearService::findCurrentYear();

        $this->drawText($currentYear['school_year'] . "\n\n\n\n\n\n\n", $this->styleTitle2);

        $this->drawText($this->title . "\n\n", $this->styleTitle1);
        $this->drawText($this->subTitle . "\n\n\n\n\n\n\n\n\n\n\n", $this->styleTitle1);

        $creationDate = $this->pei['insert_date'];
        if ($creationDate != null){
            $creationDate = substr($creationDate, 0, strpos($creationDate, " "));
            $this->drawText($creationDate . "\n");
        }
        $this->drawText($this->pei['author'] . "\n");
        $this->drawText($this->pei['note']);
    }

    private function formatDictionary($group, $key){
        if (StringUtils::isBlank($key)){
            return $key;
        }

        $d = DictionaryService::findByGroupAndKey($group, $key);
        if ($d == null){
            return null;
        }

        return $d["value"];
    }

    private function formatMultiSelectField($group, $value){
        if (StringUtils::isBlank($value)){
            return null;
        }

        try{
            $value = json_decode($value, true);
            $res = "";

            if (!isset($value["modalita"])){
                return null;
            }

            if (is_string($value["modalita"])){
                $value["modalita"] = [$value["modalita"]];
            }

            foreach ($value["modalita"] as $key){
                $dValue = $this->formatDictionary($group, $key);
                if (StringUtils::isNotBlank($dValue)){
                    $res .= $dValue;

                    if (isset($value["text"][$key])){
                        $res .= ': ' . $value["text"][$key];
                    }

                    $res .= "\n";
                }
            }

            return $res;
        } catch (Exception $e){
            LogService::error_("PeiODT", "Erore parsing json pei " . $this->pei["user_year_document_id"] . " group " . $group);
        }
    }

    private function addAnagrafica($phpWord) {
        $this->currentSection->addPageBreak();
        $this->drawText("Anagrafica", $this->styleTitle2);
        $this->drawText("\nDati Studente", $this->styleTitle3);

        $data = [
            'Codice identificativo' => $this->user['code'],
            'Data di nascita' => TextService::formatDate($this->printMetadata('birth_date')),
            'Luogo di nascita' => $this->printMetadata('birth_place'),
            'Sesso' => $this->printMetadata('gender_id'),
            'Lingua madre' => $this->printMetadata('language')
        ];

        if ($this->pei["type"] == DocumentType::PDP_LINGUA){
            $data["Lingue parlate in famiglia"] = $this->printMetadata("language_fam");
            $data["Lingue parlate tra i pari"] = $this->printMetadata("language_pari");
            $data["Altre lingue conosciute"] = $this->printMetadata("language_altre");
            $data['Data di arrivo in Italia'] = TextService::formatDate($this->printMetadata('date_italy'));

            $data["Permanenza"] = $this->formatDictionary(Dictionary::TIPO_PERMANENZA, $this->printMetadata("tipo_permanenza"));
            $data["Tipologia di BES"] = $this->formatMultiSelectField(Dictionary::TIPOLOGIA_BES, $this->printMetadata("tipologia_bes"));

            $data["Livello di scolarizzazione paese di origine"] = $this->printMetadata("livello_scol_paese_orig");
            $data["Precedente percorso scolastico"] = $this->printMetadata("percorso_scolastico");
            $data["Segnalazioni da parte della famiglia/esercenti la responsabilità genitoriale e dell’alunno"] = $this->printMetadata("segnalazioni_famiglia");
        }

        $this->drawTable($data, true);

        if ($this->printMetadata('pep') == "1") {
            $this->drawText("\n\nPEP\n", $this->styleTitle2);

            $dateInItalia = TextService::formatDate($this->printMetadata('date_italy'));
            $dateInItalia = substr($dateInItalia, 6);
            $data = [
                'Studente con background migratorio' => $this->printMetadata('pep'),
                'Data di arrivo in Italia' => $dateInItalia,
                'Livello di scolarizzazione paese di origine' => $this->printMetadata('pep_school')
            ];

            $this->drawTable($data, true);
        }

        $this->drawText("\n\nDati Scuola\n", $this->styleTitle3);

        $data = [
            'Plesso' => StructureService::printFullStructureName($this->userYear['plesso_structure_id']),
            'Classe' => $this->userYear['classe'] . ' ' . $this->userYear['sezione']
        ];

        if (strstr($this->userYear['classe'], "S2") !== false) {
            $data['Titolo di studio raggiunto'] = $this->printMetadata('titolo_di_studio');
        }

        $data['Dirigente'] = $this->userYear['manager'];
        $data['Referente BES/DSA'] = $this->userYear['ref_bes'];

        if ($this->pei["type"] == DocumentType::PDP_LINGUA){
            $data['Referente Intercultura'] = $this->printMetadata('referente_intercultura');
            $data['Coordinatore/Tutor di classe'] = $this->printMetadata('coordinatore_tutor');
            $data['Docente di sostegno linguistico'] = $this->printMetadata('docente_sost_ling');
            $data['Altri insegnanti di classe'] = $this->userYear['ref_others'];
            $data['Ore di frequenza scolastica'] = $this->printMetadata('school_hours');
            $data["Attività di sostegno linguistico"] = $this->formatDictionary(Dictionary::YES_NO, $this->printMetadata("attivita_sostegno_linguistico"));
            $data['Note'] = $this->printMetadata('note');
        } else {
            $data['Coordinatore/Tutor di classe'] = $this->printMetadata('coordinatore_tutor');

            $data['Insegnante di sostegno/ supplementare'] = $this->userYear['ref2'];
            $data['Ore di sostegno sulla classe'] = $this->printMetadata('sostegno_hours');

            $data['Collaboratori all’integrazione'] = $this->userYear['collaboratori_integrazione'];
            $data['Ore effettive di collaboratore escluse le ore di programmazione'] = (string) $this->userYear['collaboratori_integrazione_ore'];
            $data['Altri insegnanti di classe'] = $this->userYear['ref_others'];
            $data['Altre figure extrascolastiche'] = $this->userYear['others_extrasc'];

            $data['Ore di frequenza scolastica'] = $this->printMetadata('school_hours');

            if ($this->pei["type"] == DocumentType::PEI){
                $data['Servizio di trasporto'] = $this->printMetadata('servizio_trasporto');
            }

            $data['Osservazioni da parte dell\'insegnante'] = $this->printMetadata('teacher_note');
            $data['Precedente percorso scolastico'] = $this->printMetadata('precedente_percorso');

            $data['Osservazioni da parte dell\'insegnante'] = $this->printMetadata('teacher_note');
            $data['Precedente percorso scolastico'] = $this->printMetadata('precedente_percorso');
            
            if ($this->pei["type"] == DocumentType::PEI){
                $data['Quadro informativo famiglia'] = $this->printMetadata('family_note');
            }

            $data['Diagnosi'] = $this->userYear['diagnosi'];

            if (strstr($this->userYear['classe'], "S2") !== false) {
                $data['Segnalazioni da aprte dell\'alunno'] = $this->printMetadata('student_note');
                $data['Progetti in alternanza scuola lavoro'] = $this->printMetadata('school_work_projects');
            }
        }

        $this->drawTable($data, true);
    }

    private function peiTirocinioIntro($phpWord) {
        $this->currentSection->addPageBreak();

        $this->drawText("Obiettivi del corso “azione di orientamento e formazione al lavoro”", $this->styleBold);
        $this->currentSection->addListItem("Apprendere le competenze sociali e relazionali complementari al lavoro (rispetto delle regole, riconoscimento dei ruoli, autonomia lavorativa e negli spostamenti…);");
        $this->currentSection->addListItem("Sviluppare le capacitá metacognitive (attenzione, riflessione, consapevolezza);");
        $this->currentSection->addListItem("Vivere una vera situazione di lavoro con l’aiuto e il monitoraggio di esperti.");
        $this->drawText("");

        $this->drawText("Destinatari”", $this->styleBold);
        $this->currentSection->addListItem("Allieve/i che hanno compiuto 15 anni e che caratteristiche di personalitá e di apprendimento necessitano di sostegno e orientamento al lavoro;");
        $this->currentSection->addListItem("Gli allievi sono a tutti gli effetti iscritti alla formazione professionale;");
        $this->currentSection->addListItem("Non stabiliscono un rapporto di lavoro con l’azienda ospitante e non acquisiscono diritti né di ordine economico né di ordine giuridico nei confronti di essa.");
        $this->drawText("");

        $this->drawText("Come si svolge", $this->styleBold);
        $this->currentSection->addListItem("Frequenza da settembre a giugno con le sospensioni previste dal calendario scolastico;");
        $this->currentSection->addListItem("Fino ad un massimo di 32 ore settimanali di attivitá formativa e di azienda;");
        $this->drawText("");

        $this->drawText("Modalitá organizzative", $this->styleBold);
        $this->drawText("La formazione professionale stipula con le aziende una convenzione in cui definisce le modalitá di svolgimento e la copertura assicurativa.");
        $this->drawText("Le aziende non stabiliscono alcun rapporto di lavoro con le/gli allieve/i.");
        $this->drawText("");

        $this->drawText("Monitoraggio e valutazione", $this->styleBold);
        $this->drawText("L’attivitá di formazione in azienda viene monitorata dal personale scolastico in collaborazione con il personale dell’azienda.");
        $this->drawText("La valutazione è curata dagli operatori della Formazione professionale e dal tutor aziendale.");
        $this->drawText("");

        $this->drawText("Obblighi dello stagista:", ["underline" => true, "bold" => true]);
        $this->currentSection->addListItem("Svolgere il programma formativo concordato tra la Formazione Professionale e l’azienda;");
        $this->currentSection->addListItem("Seguire le indicazioni dei tutor e fare riferimento ad essi per qualsiasi esigenza di tipo organizzativo e per ogni altra evenienza;");
        $this->currentSection->addListItem("Rispettare i regolamenti aziendali e le norme in materia di sicurezza;");
        $this->currentSection->addListItem("Osservare per tutta la durata dello stage gli orari e le norme comportamentali interne all’azienda, nonché mantenere un buon comportamento nel rispetto dei responsabili aziendali");
        $this->currentSection->addListItem("Rispettare il segreto d’ufficio;");
        $this->currentSection->addListItem("Impegnarsi attivamente nella realizzazione del progetto formativo.");

        $this->drawText("");
        $this->drawText("La/il tirocinante è a conoscenza che l’esperienza di stage in azienda non costituisce alcun presupposto di rapporto di lavoro, ma che ha finalità esclusivamente formative.");
    }

    private function addPropostaProgetto($phpWord){
        $data = null;
        if (isset($this->userMetadata[Metadata::PEI_TIROCINIO_PROGETTO])){
            try{
                $data = json_decode( $this->userMetadata[Metadata::PEI_TIROCINIO_PROGETTO], true);
            } catch(Exception $e){
                LogService::error_($this, "Errore recupero dati", $ex);
            }
        }

        $this->currentSection->addPageBreak();
        $this->drawText("Proposta di progetto\n", $this->styleTitle2);

        $this->drawText("Vengono sviluppate le seguenti ipotesi:”", $this->styleBold);
        $this->currentSection->addListItem("Continuazione del percorso formativo in continuità con il calendario predisposto per " . ArrayUtils::getIndex($data, "p1_1") .
            " giornate alla settimana presso il corso " . ArrayUtils::getIndex($data, "p1_2") . 
            " Tale frequenza ha lo scopo di sviluppare " . ArrayUtils::getIndex($data, "p1_3") );
        $this->currentSection->addListItem("Secondo quanto concordato, e ritenendo utile lo sviluppo di un percorso di orientamento lavorativo, la frequenza di uno stage aziendale per " . ArrayUtils::getIndex($data, "p2_1") .
            " giorni alla settimana, per un totale settimanale di " . ArrayUtils::getIndex($data, "p2_2") . 
            " ore");

        $this->drawText("Obiettivi e modalità di svolgimento dello stage:", $this->styleTitle3);
        $this->drawText("L’allievo/a sarà affiancato/a dal tutor di progetto prof/essa " . ArrayUtils::getIndex($data, "p3_1") . 
            "; Tale affiancamento ha lo scopo primario di favorire l’inserimento dell’alunno/a in un ambiente “nuovo”, quello lavorativo, in modo da facilitargli/le l’esperienza e l’approccio con i colleghi con i quali si troverà a lavorare. Il tutor avrà inoltre il compito di sovrintenderne al buon funzionamento dello stesso verificando e controllando che ogni variabile metodologica ed organizzativa sia allineata con le finalità del corso; predisporrà inoltre il materiale per l’osservazione e la valutazione dell’esperienza lavorativa.
            ");
        $this->drawText("L’inserimento lavorativo dell’alunno/a nella struttura, oltre ad avere come finalità quella di sviluppare competenze ed abilità socio-cognitive, ovvero arricchire il suo bagaglio di esperienze personali e formative in funzione di un futuro collocamento in azienda, si prefigge lo scopo di incentivare anche quelle abilità complementari al lavoro quali il comportamento adeguato alle diverse circostanze, l’autonomia personale e di esecuzione delle mansioni, l’orientamento spazio-temporale, le relazioni interpersonali.");

        $this->drawText("");
        $this->drawText("Contesto", $this->styleTitle3);
        $this->drawText(ArrayUtils::getIndex($data, "contesto"));

        $this->drawText("");
        $this->drawText("Interessi particolari e capacità specifiche", $this->styleTitle3);
        $this->drawText(ArrayUtils::getIndex($data, "interessi"));

        $this->drawText("");
        $this->drawText("Altri interventi specifici", $this->styleTitle3);
        $this->drawText("(accompagnamento terapia, cooperazione con…,progetti, laboratori)");
        $this->drawText(ArrayUtils::getIndex($data, "interventi_specifici"));
    }

    private function addAziende($phpWord) {
        $data = null;
        if (isset($this->userMetadata[Metadata::PEI_TIROCINIO_AZIENDA])){
            try{
                $data = json_decode( $this->userMetadata[Metadata::PEI_TIROCINIO_AZIENDA], true);
            } catch(Exception $e){
                LogService::error_($this, "Errore recupero dati", $ex);
            }
        }

        if ($data == null || !isset($data["aziende"])){
            return;
        }

        $this->currentSection->addPageBreak();
        $this->drawText("Aziende", $this->styleTitle2);

        $i = 1;
        foreach ($data["aziende"] as $azienda) {
            $data = [
                "Sede di stage" => ArrayUtils::getIndex($azienda, "sede"),
                "Città" => ArrayUtils::getIndex($azienda, "citta"),
                "Telefono" => ArrayUtils::getIndex($azienda, "telefono"),
                "Durata dello stage" => ArrayUtils::getIndex($azienda, "dateFrom") . " " . ArrayUtils::getIndex($azienda, "dateTo"),
                "Giornate di stage" => ArrayUtils::getIndex($azienda, "giornate"),
                "Tutor aziendale" => ArrayUtils::getIndex($azienda, "tutorAziendale"),
                "Funzione aziendale" => ArrayUtils::getIndex($azienda, "funzioneAziendale"),
                "Mansioni di tirocinio" => ArrayUtils::getIndex($azienda, "mansioni")
            ];

            $this->drawText("\nAzienda " . $i, $this->styleTitle3);
            $this->drawTable($data, true);
            $i++;
        }
    }

    private function addSituazionePartenza($phpWord) {
        $data = [
            "Facendo riferimento a" => $this->formatMultiSelectField(Dictionary::PDP_LIN_RIFERIMENTI, $this->printMetadata("pdp_lin_riferimenti")),
            "Punti di forza" => $this->formatMultiSelectField(Dictionary::PDP_LIN_PUNTI_FORZA, $this->printMetadata("pdp_lin_pforza")),
            "Punti di debolezza" => $this->formatMultiSelectField(Dictionary::PDP_LIN_PUNTI_DEBOLEZZA, $this->printMetadata("pdp_lin_pdebolezza")),
            "Altre osservazioni" => $this->printMetadata("pdp_lin_altre_oss"),
        ];

        if ($this->countTableRows($data) == 0){
            return;
        }

        $this->currentSection->addPageBreak();
        $this->drawText("Situazione di partenza", $this->styleTitle2);
        $this->drawTable($data, true);
    }

    private function addCompetenzeLinguistiche($phpWord) {
        function formatDictionary($data, $prop){
            if (!isset($data[$prop])){
                return null;
            }

            $d = DictionaryService::findByGroupAndKey(Dictionary::PDP_LIN_LIVELLO_COMPETENZA, $data[$prop]);
            if ($d == null){
                return null;
            }

            return $d["value"];
        }

        $clJson = $this->printMetadata("un_pdp_lin_competenze");
        if (StringUtils::isBlank($clJson)){
            return;
        }

        try{
            $cl = json_decode($clJson, true);
            
            $this->currentSection->addPageBreak();
            $this->drawText("Competenze linguistiche\n\n", $this->styleTitle2);

            foreach($cl as $lingua){
                if (isset($lingua["lingua"])){
                    $this->drawText($lingua["lingua"], $this->styleTitle3);
                }

                $data = [
                    "Livello di comprensione orale" => formatDictionary($lingua, "comprensioneOrale"),
                    "Livello di produzione orale" => formatDictionary($lingua, "produzioneOrale"),
                    "Livello di comprensione scritta" => formatDictionary($lingua, "comprensioneScritta"),
                    "Livello di produzione scritta" => formatDictionary($lingua, "produzioneScritta"),
                    "Livello di interazione e mediazione" => formatDictionary($lingua, "interazioneMediazione"),
                    "Note" => isset( $lingua["note"]) ? $lingua["note"] : "",
                ];
                $this->drawTable($data, true);
                $this->drawText("\n");
            }
        } catch (Exception $ex){
            LogService::error_("PdpODT", "Errore gestione competenze linguistiche", $ex);
        }
    }

    private function addPcto($phpWord) {
        if (strstr($this->userYear['classe'], "S2") === false) {
            return;
        }

        $pcto = UserPctoService::find($this->user["user_id"]);
        if ($pcto == null){
            return;
        }

        $data = [
            'Anno scolastico' => $pcto["anno_scolastico"],
            'Tipo percorso' => $pcto["tipo_percorso"],
            'Nome struttura' => $pcto["nome_struttura"],
            'Ore presso struttura' => $pcto["ore_struttura"] != null && $pcto["ore_struttura"] > 0 ? (string)$pcto["ore_struttura"] : null,
            'Ore in aula' => $pcto["ore_aula"] != null && $pcto["ore_aula"] > 0 ? (string)$pcto["ore_aula"] : null
        ];

        $this->currentSection->addPageBreak();
        $this->drawText("PCTO", $this->styleTitle2);

        $this->drawTable($data, true);
    }

    private function addPatto($phpWord) {
        $this->currentSection->addPageBreak();
        $this->drawText("Patto con la famiglia/con gli esercenti la responsabilità genitoriale\n", $this->styleTitle2);
        
        if ($this->pei['type'] == DocumentType::PEI) {
            if ($this->userYear['patto_type'] == null || $this->userYear['patto_type'] == 1) {
                $this->drawText("Dopo accurate osservazioni da parte del consiglio di classe (si veda il verbale della seduta per la stesura del piano educativo individualizzato) e in accordo con gli esercenti la responsabilità genitoriale ovvero con l’alunno/l’alunna maggiorenne, si stabilisce in data odierna che il suddetto alunno/la suddetta alunna seguirà gli obiettivi della classe fino ad una eventuale revoca del presente accordo.");
            } else if ($this->userYear['patto_type'] == 2) {
                $this->drawText("Dopo accurate osservazioni da parte del consiglio di classe (si veda il verbale della seduta per la stesura del piano educativo individualizzato) e in accordo con gli esercenti la responsabilità genitoriale ovvero con l’alunno/l’alunna maggiorenne, si stabilisce in data odierna che il suddetto alunno/la suddetta alunna seguirà in una o più discipline un programma con obiettivi differenziati fino ad una eventuale revoca del presente accordo.");
            }

            if (strpos($this->userYear['classe'], 'I') === false ){
                $this->drawText("* Nel primo ciclo d’istruzione conseguono un titolo di studio avente valore legale (diploma conclusivo del primo ciclo d’istruzione) sia gli alunni/le alunne che seguono gli obiettivi della classe, sia gli alunni/le alunne per i quali sono previsti obiettivi differenziati in una o più discipline.");
                $this->drawText("Nel secondo ciclo d’istruzione conseguono un titolo di studio avente valore legale (diploma conclusivo del secondo ciclo d’istruzione e/o diploma di qualifica professionale) solo quegli alunni/quelle alunne che seguono gli obiettivi della classe in tutte le discipline. Agli alunni/alle alunne che seguono obiettivi differenziati in una o più discipline verrà invece rilasciata una certificazione delle competenze acquisite.");
            }

            $this->drawText("");
            $this->drawText('Comportamento:');
            if ($this->userYear["comportamento"] == "classe"){
                $this->drawText('Il comportamento è valutato in base agli stessi criteri adottati per la classe.');
            } else if ($this->userYear["comportamento"] == "obiettivi"){
                $this->drawText('Il comportamento è valutato in base agli obiettivi definiti nel documento.');
            }
        } else if ($this->pei['type'] == DocumentType::PEI_TIROCINIO) {
            $this->drawText("A seguito di accurate osservazioni e valutazioni delle competenze dell’alunna, si definiscono obiettivi e interventi e assieme alla famiglia si prendono i seguenti accordi:");
            $this->drawText("Alla fine del percorso formativo viene rilasciata una certificazione delle competenze acquisite.");
        } else {
            $this->drawText("A seguito dell'osservazione e delle valutazioni delle competenze dell'alunna/o in accordo con i genitori/esercenti la responsabilità genitoriale ovvero con l'alunna/o maggiorenne, il Consiglio di classe si impegna ad adottare, se necessario, misure dispensative e strumenti compensativi, interventi pedagogico-didattici e opportune forme di valutazione - nelle diverse discipline così come viene indicato in questo documento - allo scopo di favorire il successo formativo dell'alunna/o, secondo quanto previsto dalla normativa vigente. Il Consiglio di classe si impegna ad adottare il presente PDP; il/i genitore/i, esercente/i la responsabilità genitoriale, l'alunno maggiorenne condividono/condivide queste modalità dell'azione pedagogico-didattica, supportano e si impegnano nel lavoro a scuola e a casa relativamente a questa linea di azione.");
        }

        if ($this->pei['type'] == DocumentType::PDP_LINGUA) {
            $this->drawText("Il presente documento ha carattere transitorio.");
        }
        
        if ($this->pei['type'] == DocumentType::PEI) {
            $pattoUscite = EncryptService::decrypt($this->userYear['patto_uscute_didattiche']);
            if (StringUtils::isNotBlank($pattoUscite)){
                $this->drawText("");
                $this->drawText("Uscite didattiche, visite guidate e viaggi di istruzione:");
                $this->drawText("Interventi previsti per consentire allo/a studente/essa di partecipare alle uscite didattiche visite guidate e viaggi di istruzione organizzati per la classe:");
                $this->drawText($pattoUscite);
                $this->drawText("");
            }            

            if (StringUtils::isNotBlank( $this->userYear["patto_attivita"] )){
                try{
                    $pattoAttivita = json_decode($this->userYear["patto_attivita"]);
                    $attivitàDictionary = Constants::$pattoAttivita;

                    $this->drawText("Attività inclusive:");
                    foreach ($attivitàDictionary as $value) {
                        if (array_search($value["key"], $pattoAttivita->modalita) === false){
                            continue;
                        }

                        $text = "";
                        $key = $value["key"];
                        if (property_exists($pattoAttivita->text, $key)){
                            $text = ": " . $pattoAttivita->text->$key;
                        }
                        
                        if ($value != null){
                            $this->drawText($value["value"] . $text);
                        }
                    }

                    $this->drawText("");
                } catch (Error $e){

                }
            }
        }

        if (StringUtils::isNotBlank($this->userYear['patto_note'])){
            $this->drawText("Note:");
            $this->drawText(EncryptService::decrypt($this->userYear['patto_note']));
        }

        if ($this->pei['type'] == DocumentType::PEI) {
            $this->drawText("Le ore di sostegno sono state stabilite sulla base dei criteri elaborati dal GLI in base alla valutazione del fabbisogno e gli obiettivi definiti nel PEI dell'anno precedente se presente.");
        }

        $this->drawText("\n");
        $this->drawText("Data: _____________________________\n\n\n");

        if ($this->pei['type'] == DocumentType::PEI_TIROCINIO){
            $this->drawText("L'insegnante individuale                                           Il/La Dirigente\n\n\n\n");
            $this->drawText("Gli esercenti la responsabilità genitoriale");
            $this->drawText("___________________________________\n\n\n");    
        } else {
            $this->drawText("Il team del nido / scuola dell’infanzia / Il consiglio di classe\n\n");

            $userShares = UserUserService::findForUserId($this->user['user_id']);
            if ($userShares->rowCount() > 0){
                $consiglioDiClasse = [];
                foreach ($userShares as $userShare) {
                    $user = UserService::find($userShare["to_user_id"]);
                    $consiglioDiClasse[$user['name'] . ' ' . $user['surname']] = "___________________________________";
                }
                $this->drawTable($consiglioDiClasse);
                $this->drawText("\n");
            } else {
                $this->drawText("___________________________________\n\n");
                $this->drawText("___________________________________\n\n");
                $this->drawText("___________________________________\n\n");
                $this->drawText("___________________________________\n\n\n");
            }

            $this->drawText("Il/La Dirigente / Direttore / Direttrice \n\n");
            $this->drawText("___________________________________\n\n\n");

            $this->drawText("Firma degli esercenti la responsabilità genitoriale (1)\n");
            $this->drawText("___________________________________\n\n");
            $this->drawText("___________________________________\n\n\n");

            $this->drawText("L’alunno/a maggiorenne\n");
            $this->drawText("___________________________________\n\n\n");    
        }   

        $this->drawText("(1): Il/la sottoscritto/a, consapevole delle conseguenze amministrative e penali per chi rilasci dichiarazioni non corrispondenti a verità, ai sensi del D.P.R. 445 del 2000, dichiara di aver effettuato la scelta/richiesta in osservanza delle disposizioni sulla responsabilità genitoriale di cui agli artt. 316, 337 ter e 337 quater del codice civile, che richiedono il consenso di entrambi i genitori");     
    }

    private function addOsservazioni($phpWord) {
        $stmt = PeiService::findPeiRowWithStatus($this->pei["type"], $this->pei['user_year_document_id'], 'SELECTED');

        if ($stmt->rowCount() == 0) {
            $this->drawText("Procedere alla compilazione.");
            return;
        }

        $this->currentSection->addPageBreak();
        $this->drawText("Analisi della situazione attuale nelle singole aree e individuazione delle relative possibilità di sviluppo", $this->styleTitle2);
        
        foreach ($stmt as $row) {
            $path = "";
            if ($row['pr_uri'] == 'apprendimento') {
                $path = 'APPRENDIMENTO -> ' . $row['title'];
            } else {
                $path = PeiService::buildNodePath($row['pr_uri'], $this->pei["type"]);
            }

            $peiRow = UserPeiService::findPeiRow($row["pei_row_id"]);
            $stmtTargets = PeiService::findPeiRowTargets($row['pei_row_id']);
            $node = PeiService::findNodeByUri($peiRow['uri'], $this->pei["type"]);

            $tables = [];
            if ($this->pei['type'] == DocumentType::PEI || $this->pei['type'] == DocumentType::PEI_TIROCINIO) {
                foreach ($stmtTargets as $peiTarget) {
                    $data = [];
                                        
                    $data["Osservazioni"] = EncryptService::decrypt($peiTarget["observation"]);

                    $data["Punti di forza"] = EncryptService::decrypt($peiTarget["pro"]);
                    $data["Punti di debolezza/deficit"] = EncryptService::decrypt($peiTarget["cons"]);
                    $data["Obiettivi"] = EncryptService::decrypt($peiTarget["target"]);

                    $curriculaType = DictionaryService::findGroupAsDictionary(Dictionary::PEI_CURRICULA_TYPE);
                    $data["Tipologia obiettivi"] = ArrayUtils::getIndex($curriculaType, $peiTarget["type"], "");
                    
                    $status = DictionaryService::findGroupAsDictionary(Dictionary::PEI_ROW_TARGET_STATUS);
                    $data["Verifica obiettivi"] = ArrayUtils::getIndex($status, $peiTarget["status"], "");
                    
                    $data["Strategie e strumenti metodologici"] = $this->prepareStrategieEApprocciPEi($peiTarget,
                        "pei_strategie_approcci_metodologici",
                        DictionaryService::findGroup(Dictionary::PEI_STRATEGIE_APPROCCI_METODOLOGICI));

                    $data["Approcci metodologici, tecniche, metodi"] = $this->prepareStrategieEApprocciPEi($peiTarget,
                        "pei_approcci_metodologici",
                        DictionaryService::findGroup(Dictionary::PEI_APPROCCI_METODOLOGICI));

                    $data["Attività e interventi"] = EncryptService::decrypt($peiTarget["activity"]);

                    $data["Barriere (elementi e/o fattori che ostacolano la partecipazione e l’apprendimento dell’alunno/a)"] = EncryptService::decrypt($peiTarget["barriere"]);
                    $data["Facilitatori (elementi e/o fattori che aiutano la partecipazione e l'apprendimento dell'alunno/a)"] = EncryptService::decrypt($peiTarget["facilitatori"]);

                    $data["Modalità di valutazione"] = $this->prepareModalitaVerificaPEI($peiTarget);

                    $data["Note"] = EncryptService::decrypt($peiTarget["note"]);
                    
                    if ($this->countTableRows($data) > 0) {
                        $tables[] = $data;
                    }
                }
            } else if ($this->pei['type'] == DocumentType::PDP) {
                foreach ($stmtTargets as $peiTarget) {
                    $data = [];
                    $data["Punti di forza"] = EncryptService::decrypt($peiTarget["pro"]);
                    $data["Punti di debolezza/deficit"] = EncryptService::decrypt($peiTarget["cons"]);
                    $data["Altre osservazioni"] = EncryptService::decrypt($peiTarget["observation"]);
                    $data["Obiettivi"] = EncryptService::decrypt($peiTarget["target"]);

                    $status = DictionaryService::findGroupAsDictionary(Dictionary::PEI_ROW_TARGET_STATUS);
                    $data["Verifica degli obiettivi"] = ArrayUtils::getIndex($status, $peiTarget["status"], "");
                    $data["Attività e interventi"] = EncryptService::decrypt($peiTarget["activity"]);

                    $data["Strategie compensative / approcci metodologici"] = $this->prepareMultiValueWithOther($peiTarget, "pdp_strategie_compensative", "pdp_strategie_compensative_altro", Dictionary::STRATEGIE_COMPENSATIVE);
                    $data['Ausili e strumenti compensativi'] = $this->prepareMultiValueWithOther($peiTarget, "pdp_ausili_strumenti", "pdp_ausili_strumenti_altro", Dictionary::AUSILI_STRUMENTI_COMPENSATIVI);
                    $data['Misure dispensative'] = $this->prepareMultiValueWithOther($peiTarget, "pdp_misure_dispensative", "pdp_misure_dispensative_altro", Dictionary::MISURE_DISPENSATIVE);
                    $data['Modalità di svolgimento delle prove di verifica/valutazione degli apprendimenti'] = $this->prepareMultiValueWithOther($peiTarget, "pdp_svolgimento_prove", "pdp_svolgimento_prove_altro", Dictionary::MODALITA_SVOLGIMENTO_PROVE);

                    $data["Note"] = EncryptService::decrypt($peiTarget["note"]);

                    if ($this->countTableRows($data) > 0) {
                        $tables[] = $data;
                    }
                }
            } else if ($this->pei['type'] == DocumentType::PDP_LINGUA) {
                foreach ($stmtTargets as $peiTarget) {
                    $data = [
                        "Osservazioni" => $peiTarget["observation"],
                        "Tipologia di obiettivi" => $this->formatMultiSelectField(Dictionary::PDP_LIN_TIPOLOGIA_OBIETTIVI, $peiTarget["pdp_lin_tipologia_obiettivi"]),
                        "Note obiettivi" => $peiTarget["pdp_lin_tipologia_obiettivi_note"],
                        "Attività e interventi" => $this->formatMultiSelectField(Dictionary::PDP_LIN_ATTIVITA_INTERVENTI, $peiTarget["pdp_lin_attivita_interventi"]),
                        "Strategie compensative / approcci metodologici" => $this->formatMultiSelectField(Dictionary::PDP_LIN_STRATEGIE_COMPENSATIVE, $peiTarget["pdp_lin_strategie_compensative"]),
                        "Strumenti compensativi" => $this->formatMultiSelectField(Dictionary::PDP_LIN_STRUMENTI_COMPENSATIVI, $peiTarget["pdp_lin_strumenti_compensativi"]),
                        "Misure dispensative" => $this->formatMultiSelectField(Dictionary::PDP_LIN_MISURE_DISPENSATIVE, $peiTarget["pdp_lin_misure_dispensative"]),
                        "Modalità di svolgimento delle prove di verifica" => $this->formatMultiSelectField(Dictionary::PDP_LIN_MODALITA_VERIFICA, $peiTarget["pdp_lin_modalita_verifica"]),
                        "Modalità di valutazione degli apprendimenti" => $this->formatMultiSelectField(Dictionary::PDP_LIN_MODALITA_VALUTAZIONE_APP, $peiTarget["pdp_lin_modalita_valutazione_app"]),
                    ];
                    
                    if ($this->countTableRows($data) > 0) {
                        $tables[] = $data;
                    }
                }
            }

            if (count($tables) == 0) {
                continue;
            }

            $this->drawText("\n" . $path."\n", $this->styleTitle3);
            if ($this->typefile == self::ODT){
                $this->drawText("\n");
            }
                
            if (is_array($this->content) && array_search(PeiODT::OSSERVAZIONI_DESCRIZIONE, $this->content) !== false && 
                is_array($node) && StringUtils::isNotBlank($node['description'])) {
                $data = [];

                $data["Descrizione"] = $node['description'];

                if ($node['icf_codes'] != NULL) {
                    $out = '';
                    foreach (json_decode($node['icf_codes']) as $code) {
                        $icfCode = IcfService::find($code);
                        if ($icfCode != null){
                            $out .= $out == "" ? '' : ', ';
                                $out .= $code . ' ';
                            if (StringUtils::isNotBlank($icfCode['name'])) {
                                $out .= '(' . $icfCode['name'] . ') ';
                            }
                        }
                    }

                    $data["Codici ICF-CY"] = $out;
                }

                if ($node['icd10_codes'] != NULL) {
                    $out = "";
                    foreach (json_decode($node['icd10_codes']) as $code) {
                        $out .= $out == "" ? '' : ', ';
                        $out .= $code;
                    }
                    
                    $data["Codici ICD10"] = $out;
                }

                $questions = PeiService::findNodeQuestion($node['pei_node_id'], $this->pei["type"]);
                if ($questions->rowCount() > 0) {
                    $out = "";
                    foreach ($questions as $question) {
                        $out .= $question['question'] . "\n";
                    }

                    $data["Domande esplicative"] = $out;
                }

                $this->drawTable($data);
            }

            foreach ($tables as $table) {
                $this->drawTable($table, true);
                if ($this->typefile == self::ODT){
                    $this->drawText("\n");
                }
            }
        }
    }

    private function prepareModalitaVerificaPEI($peiTarget){
        if (!isset($peiTarget["pei_modalita_verifica"])){
            return "";
        }

        $value = $peiTarget["pei_modalita_verifica"];
        if (!StringUtils::isJSON($value)){
            return "";
        }

        $options = DictionaryService::findGroupAsDictionary(Dictionary::PEI_MODALITA_VERIFICA);
        $decoded = json_decode($value);

        $res = "";
        foreach($decoded->modalita as $key){
            $res .= $res == "" ? "" : "\n\n";
            $res .= $options[$key];

            if (property_exists($decoded->text, $key) && StringUtils::isNotBlank($decoded->text->$key)){
                $res .= ": " . $decoded->text->$key;
            }
        }

        return $res;
    }

    private function prepareStrategieEApprocciPEi($peiTarget, $field, $strategie){
        if (!isset($peiTarget[$field])){
            return "";
        }

        $value = $peiTarget[$field];
        if (!StringUtils::isJSON($value)){
            return "";
        }

        $strategieFacilitatori = json_decode($value, false);
        if (!is_object($strategieFacilitatori)){
            if (!is_array($strategieFacilitatori)){
                $strategieFacilitatori = [];
            }

            $strategieFacilitatori = (object)[
                "strategie" => $strategieFacilitatori,
                "facilitatori" => []
            ];
        }

        $res = "";
        foreach($strategie as $s){
            $strategia = false;
            $facilitatore = false;
            $text = "";

            if (is_array($strategieFacilitatori->strategie)){
                if (array_search($s["key"], $strategieFacilitatori->strategie) !== false){
                    $strategia = true;
                }
            }

            if (property_exists($strategieFacilitatori, 'facilitatori')){
                if (array_search($s["key"], $strategieFacilitatori->facilitatori) !== false){
                    $facilitatore = true;
                }
            }

            if (property_exists($strategieFacilitatori, "text") &&
                property_exists($strategieFacilitatori->text, $s["key"]) ){
                $text = ((array) $strategieFacilitatori->text)[$s["key"]];
            }

            if (!$strategia && !$facilitatore){
                continue;
            }

            $res .= $s["value"] . ' -> ';
            if ($strategia){
                $res .= "strategia";
            }
            if ($strategia && $facilitatore){
                $res .= " e ";
            }
            if ($facilitatore){
                $res .= "facilitatore";
            }

            if (StringUtils::isNotBlank($text)){
                $res .= "\n" . $text;
            }

            $res .= "\n";
        }

        return $res;
    }

    private function prepareMultiValueWithOther($peiTarget, $field, $fieldOther, $dictionary, $isEncrypted = true) {
        if (!isset($peiTarget[$field])){
            return "";
        }
        
        if ($isEncrypted){
            $multiValued = EncryptService::decrypt($peiTarget[$field]);
        } else {
            $multiValued = $peiTarget[$field];
        }
        
        $selected = [];
        if (StringUtils::isJSON($multiValued)){
            $selected = json_decode($multiValued, true);
        } else {
            $selected = [$multiValued];
        }
        $values = DictionaryService::findGroupAsDictionary($dictionary);
        
        $text = $this->prepareMultivalue($values, $selected);
        if (isset($peiTarget[$fieldOther])){
            $text .= EncryptService::decrypt($peiTarget[$fieldOther]);
        }
        return $text;
    }

    private function addCalendario($phpWord) {
        $this->currentSection->addPageBreak();
        $this->drawText("Calendario", $this->styleTitle2);
        $this->drawText("");

        /* $this->currentSection = $phpWord->addSection([
          'orientation' => 'landscape'
          ]); */

        $days = ['Orario', 'Lun', 'Mar', 'Mer', 'Giov', 'Ven', 'Sab'];
        $hours = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

        $plan = null;
        if ($this->userYear['plan'] != null){
            $plan = json_decode($this->userYear['plan'], true);
        }

        $table = $this->currentSection->addTable($this->styleTabellaStruttura, [
            'borderSize' => 0, 
            'borderColor' => 'F73605', 
            'afterSpacing' => 0, 
            'Spacing' => 0, 
            'cellMargin' => 0
        ]);

        $row = $table->addRow();
        foreach ($days as $dKey => $day) {
            $cell = $row->addCell(null, ['borderSize' => 6]);
            $cell->addText($day, 'bold');
        }

        foreach ($hours as $hKey => $hour) {
            $row = $table->addRow(900);
            foreach ($days as $dKey => $day) {
                $timeKey = $this->buildTimeKey($day, $hour);

                $value = "";
                if ($plan != null && isset($plan[$timeKey])) {
                    $value = $plan[$timeKey];
                }
                $cell = $row->addCell(2000, ['borderSize' => 6]);
                $cell->addText($value);
            }
        }
    }

    private function buildTimeKey($day, $hour) {
        $key = $day . '_' . $hour;
        $key = str_replace(":", "_", $key);
        return $key;
    }

    private function prepareMultivalue($dictionary, $selectedIndexes) {
        if (!is_array($selectedIndexes)) {
            return "";
        }

        $out = '';
        foreach ($selectedIndexes as $value) {
            if (!array_key_exists($value, $dictionary)){
                continue;
            }
            $out .= $dictionary[$value] . "\n";
        }
        return $out;
    }

    private function prepareFigureProfessionali() {
        $socialOccupation = DictionaryService::findGroupAsDictionary(Dictionary::SOCIAL_OCCUPATION);
        $values = json_decode($this->printMetadata('figure_riferimento'), true);
        if ($values == null){
            return '';
        }

        $out = '';
        $i = 0;
        foreach ($socialOccupation as $key => $label) {
            if ($values['text'] != null && 
                isset($values['text'][$i]) &&
                StringUtils::isBlank($values['text'][$i])) {
                $i++;
                continue;
            }

            $out .= $label . ": " . $values['text'][$i] . "\n";
            $i++;
        }

        return $out;
    }

    private function drawText($text, $style = null, $style2 = null) {
        if (StringUtils::isBlank($text)){
            $text = "";
        }

        $textlines = explode("\n", $text);

        for ($i = 0; $i < sizeof($textlines); $i++) {
            $this->currentSection->addText($textlines[$i], $style, $style2);
        }
    }

    private function countTableRows($data) {
        $numRows = 0;
        foreach ($data as $key => $value) {
            if ($value == null){
                continue;
            }
            if (is_string($value) && StringUtils::isBlank($value)) {
                continue;
            }

            $numRows++;
        }
        return $numRows;
    }

    private function drawTable($data, $exludeEmpty = false, $cellWidth = null) {
        $numRows = 0;

        if (!is_array($data)){
            return;
        }

        $table = $this->currentSection->addTable($this->styleTabellaStruttura);
        foreach ($data as $key => $value) {
            if ($exludeEmpty && is_string($value) && StringUtils::isBlank($value)) {
                continue;
            }

            $numRows++;

            $row = $table->addRow();

            $cell = $row->addCell($this->getCellWidt($cellWidth, 0, PhpOffice\PhpWord\Shared\Converter::cmToTwip(5)));
            $cell->addText($key, 'bold');

            $cell = $row->addCell($this->getCellWidt($cellWidth, 1, PhpOffice\PhpWord\Shared\Converter::cmToTwip(10)));

            if (is_string($value)){
                $textlines = explode("\n", $value);
                for ($i = 0; $i < sizeof($textlines); $i++) {
                    $escaped = preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]/', '', $textlines[$i]);
                    $entityDecoded = html_entity_decode( $escaped );
                    $cell->addText($entityDecoded);
                }
            }
        }

        return $numRows;
    }

    private function getCellWidt($cellsWidth, $cellIndex, $default){
        if ($cellsWidth == NULL){
            return $default;
        }
        
        if (!isset($cellsWidth[$cellIndex])){
            return $default;
        }
        
        return $cellsWidth[$cellIndex];
    }
}
