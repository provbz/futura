<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class user_diagnosi_new_mail extends MailBaseTemplate{
    
    public function getSubject() {
        return "Nuova diagnosi caricata"; 
    }
    
    public function getMessage() {
        $user = UserService::find($this->notice['entity_id']);
        $insertUser = UserService::find($this->par['insert_user_id']);
        $attachment = AttachmentService::find($this->par['attachment_id']);
        
        ob_start();
        echo $this->getHeader();
        ?>
        <div>
            <p>
                E' stata caricata una nuova diagnosi per lo studente <?= $user['code'] ?>.<br/>
                Inserita da <?= $insertUser['name'] . " " . $insertUser['surname'] ?>.<br/>
                Nome file: <?= $attachment['filename'] ?><br/>
                Data: <?= $attachment['insert_date'] ?><br/>
            </p>
        </div>
        <?php
        echo $this->getFooter();
        return ob_get_clean();    
    }
    
}
