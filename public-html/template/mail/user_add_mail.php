<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class user_add_mail extends MailBaseTemplate{
    
    public function getSubject() {
        return "Nuovo account creato";
    }
    
    public function getMessage() {
        $user = UserService::find($this->par['user_id']);
        $par = [];
        $par['ota'] = StringUtils::generateRandomChars();
        EM::updateEntity("user", $par, ["user_id" => $user['user_id']]);

        $link = UriService::builAbsoluteUrl() . UriService::buildPageUrl("/public/RetriveDataAction", "signup", [
            'email' => urlencode( $user['email'] ),
            'ota' => $par['ota']
        ]);
                
        ob_start();
        echo $this->getHeader();
        if ($this->isFirst){
        ?>
        È stato creato un account a suo nome sulla piattaforma <?= _t(Constants::$APP_PRINT_NAME) ?>.<br/>
        È possibile accedere utilizzando l'account LASIS (Office 365) direttamnete dalla pagina di login della piattaforma premendo il pulsante "Accedi con credenziali LASIS":<br/>
        <?php } ?>
        <a href="<?= UriService::builAbsoluteUrl() ?>"><?= UriService::builAbsoluteUrl() ?></a><br/><br/>

        In alternativa è possibile utilizzare le credenziali locali della piattaforma inserendo come indirizzo mail: <br/>
        <?= _t($user['email']) ?><br>
        E procedendo alla creazione di una nuova password tramite il seguente link:<br/>
        <a href="<?= $link ?>"><?= $link ?></a>
        <?php
        echo $this->getFooter();
        return ob_get_clean();
    }
    
}
