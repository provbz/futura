<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class user_user_add_mail extends MailBaseTemplate{
    
    public function getSubject() {
        return "Condivisione di documento"; 
    }
    
    public function getMessage() {
        $user = UserService::find($this->par['user_id']);
        $insertByUser = UserService::find($this->par['insert_by_user_id']);
        $toUser = UserService::find($this->par['to_user_id']);

        ob_start();
        echo $this->getHeader();
        ?>
        <div>
            <br/>
            L'utente <?= _t($insertByUser['email']) ?> ti ha aggiunto ai contributori per lo studente <?= _t($user['code']) ?>.<br/>
            Puoi inserire i tuoi contributi accedendo direttamente alla piattaforma.<br/><br/>
        </div>
        <?php
        echo $this->getFooter();
        return ob_get_clean();   
    }
}