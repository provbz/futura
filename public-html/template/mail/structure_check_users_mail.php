<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class structure_check_users_mail extends MailBaseTemplate{
    
    public $user;
    
    public function getSubject() {
        return "Verifica gli utenti del tuo IC";
    }
    
    public function getMessage() {
        $this->user = UserService::find($this->noticeUser['user_id']);
        
        ob_start();
        echo $this->getHeader();
        if ($this->isFirst){
        ?>
            <p>
                Di seguito il riepilogo degli utenti associati al tuo Istituto Comprensivo.<br/>
                Per garantire un utilizzo sicuro dei dati presenti nella piattaforma verificare con attenzione i nominativi e i ruoli.<br/>
                In caso di necessità procedere alla correzione dei dati dalla pagina "Utenti".
            </p>
        <?php
        }
        
        $structures = StructureService::findUserStructures($this->user["user_id"]);
        foreach($structures as $structure){
            $table = $this->buildTable($structure["structure_id"]);

            ?>
            <div style="margin-bottom: 20px;">
                <h4><?= _t($structure["name"]) ?></h4>
                <?php $table->draw(); ?>
            </div>
            <?php
        }
        
        echo $this->getFooter();
        return ob_get_clean();
    }

    public function buildTable($structureId){
        $referentFor = DictionaryService::findGroupAsDictionary(Dictionary::STRUCTURE_REFERENT);

        $table = new Table();
        $table->allowExportXLS = false;
        $table->paginateElements = false;

        $query = "SELECT * 
                    FROM user u
                    WHERE user_id IN (
                        select us.user_id
                        FROM user_structure us
                        LEFT JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id
                        LEFT JOIN structure s ON s.parent_structure_id=:structure_id_2
                        left join user_structure uss ON uss.user_id=us.user_id AND uss.structure_id = s.structure_id
                        LEFT JOIN user_structure_metadata usm_referer ON uss.user_structure_id=usm_referer.user_structure_id AND usm_referer.metadata_id=:metadata_referer_for
                        WHERE us.structure_id=:structure_id AND usr.role_id != :role_student
                    ";

        $pars = [
            "structure_id" => $structureId,
            "role_student" => UserRole::STUDENTE,
            "structure_id_2" => $structureId,
            "metadata_referer_for" => UserStructureMetadata::REFERENT_FOR
        ];

        $query .= ")
                ORDER By surname";

        $table->data = EM::execQuery($query, $pars);
          
        $roles = UserRoleService::findAll();
        $roleDictionary = [];
        foreach($roles as $role){
            $roleDictionary[$role['role_id']] = $role['name'];
        }
        
        $table->addColumn(new TableColumn("user_id", "Ruolo", function($userId) use($roleDictionary, $structureId){
            $userStructureRoles = StructureService::findUserStructureRole($userId, $structureId);
            $out = "";
            foreach ($userStructureRoles as $userRole){
                $out .= $out == "" ? "" : "<br/>";
                $out .= isset($roleDictionary[$userRole["role_id"]]) ? $roleDictionary[$userRole["role_id"]] : $userRole["role_id"];
            }
            return $out;
        }));
        $table->addColumn(new TableColumn("email", "e-mail", NULL));
        $table->addColumn(new TableColumn("name", "Nome", NULL, "width:260px;"));
        $table->addColumn(new TableColumn("surname", "Cognome", NULL));
        
        return $table;
    }
}
