<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class user_structure_add_mail extends MailBaseTemplate{
    
    public function getSubject() {
        return "Accesso attivato"; 
    }
    
    public function getMessage() {
        $structure = StructureService::find($this->par['structure_id']);
                   
        ob_start();
        echo $this->getHeader();
        ?>
        <div>
            Il tuo account per la piattaforma <?= _t(Constants::$APP_PRINT_NAME) ?> è stato associato alla scuola <?= _t($structure['name']) ?>.<br/><br/>

            Per eseguire l'accesso collegarsi a:<br/>
            <a href="<?= UriService::builAbsoluteUrl() ?>"><?= UriService::builAbsoluteUrl() ?></a><br/><br/>

            E accedere utilizzando il proprio account LASIS (Office 365) premendo sull'apposito pulsante o inserire le credenziali locali della piattaforma se create.<br/><br/>
        </div>
        <?php
        echo $this->getFooter();
        return ob_get_clean();    
    }
    
}
