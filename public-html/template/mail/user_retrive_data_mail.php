<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class user_retrive_data_mail extends MailBaseTemplate{
    public function getSubject() {
        return "Recupero dati di accesso"; 
    }
    
    public function getMessage() {
        $user = UserService::find($this->par['user_id']);
        $par = [];
        $par['ota'] = StringUtils::generateRandomChars();
        EM::updateEntity("user", $par, ["user_id" => $user['user_id']]);
        $link = UriService::builAbsoluteUrl() . UriService::buildPageUrl("/public/RetriveDataAction", "signup", [
            'email' => urlencode( $user['email'] ),
            'ota' => $par['ota']
        ]);

        ob_start();
        echo $this->getHeader();
        ?>
        <div>
            È stato richiesto il recuperare dei dati di accesso per l'utente associato a questo indirizzo mail.<br/>
            Per procedere accedere all'indirizzo:<br/>
            <a href="<?= $link ?>"><?= _t($link) ?></a><br/><br/>

            Se la richiesta non è stata eseguita ignorare la mail.<br/>
        </div>
        <?php
        echo $this->getFooter();
        return ob_get_clean();    
    }
}