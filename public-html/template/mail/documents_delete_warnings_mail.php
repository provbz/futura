<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class documents_delete_warnings_mail extends MailBaseTemplate{
    
    public function getSubject() {
        return "Rimozione documenti studenti";
    }
    
    public function getMessage() {
        $user = UserService::find($this->noticeUser['user_id']);
                
        ob_start();
        echo $this->getHeader();
        if ($this->isFirst){
        ?>
        <p>
            Si avvisa che il 30/06 verranno definitivamente rimossi tutti i dati dei seguenti studenti.<br/>
            L'elenco comprende gli studenti per i quali non è stata eseguita nessuna operazione nell'anno scolastico corrente.<br/>
            Se si desidera recuperare i dati: accedere alla piattaforma e generare un documento per l'anno corrente.<br/>
            Si ricorda che per garantire il rispetto del regolamento privacy i dati degli studenti non possono essere conservati oltre i termini previsti dalle finalità
            di elaborazione dei documenti.<br/>
        </p>
        <?php
        }
        
        $structures = StructureService::findUserStructures($user["user_id"]);
        foreach($structures as $structure){
            $usersToRemove = UserYearService::findUserYearToRemoveInStructure($structure["structure_id"]);
            if ($usersToRemove->rowCount() == 0){
                continue;
            }
            
            $data = [];
            foreach($usersToRemove as $utr){
                $student = UserService::find($utr["user_id"]);
                if ($student == null){
                    continue;
                }
                
                $userYear = UserYearService::findUserYearByUser($student["user_id"]);
                if ($userYear == null){
                    continue;
                }

                $data[] = [
                    "ic" => $userYear["ic"],
                    "plesso" => $userYear["plesso"],
                    "classe" => $userYear["classe"],
                    "sezione" => $userYear["sezione"],
                    "code" => $student["code"]
                ];
            }

            $table = new Table();
            $table->paginateElements = false;
            $table->allowExportXLS = false;
            $table->data = $data;
            $table->addColumn(new TableColumn("ic", "IC"));
            $table->addColumn(new TableColumn("plesso", "Plesso"));
            $table->addColumn(new TableColumn("classe", "Classe"));
            $table->addColumn(new TableColumn("sezione", "Sezione"));
            $table->addColumn(new TableColumn("code", "Codice studente"));
            ?>
            <dib>
                <h4><?= _t($structure["name"]) ?></h4>
                <?= $table->draw(); ?>
            </dib>
            <?php
        }
        
        echo $this->getFooter();
        return ob_get_clean();
    }
}
