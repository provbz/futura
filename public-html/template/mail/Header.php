<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <style type="text/css">
		body {
			font: 11px/16px Verdana;
			color: #333;
		}
		p {
			margin-top: 10px;
			padding: 0 8px;
		}
        #address {
			height: 50px;
			color: #AAA;
			font-size: 0.8em;
			line-height: 1.6em;
			padding-top: 3px;
			margin-top: 25px;
			padding: 0 8px;
        }
		#address a {
			color: #CCC;
		}
		#firma {
			font-style: italic;
		}
		#titolo {
			height: 37px;
			font-size: 16px;
			color: #FFF;
			margin: 0;
			padding-top: 8px;
		}
		#header {
			height: 55px;
		}
		#nome {
			font-size: 23px;
		}
		p a {
			color: #CD1316;
		}
		#logo {
			padding: 0 8px;
			margin-bottom: 20px;
		}
    </style>
</head>
<body>
    <!--<img id="logo" style="width: 200px;" src="<?= $this->getPlatformUrl(); ?>/img/logo_hi.png"/>-->
    <div id="content">