<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class portfolio_closing_mail extends MailBaseTemplate{
    
    public $user;
    
    public function getSubject() {
        return "Portfolio";
    }
    
    public function getMessage() {
        $this->user = UserService::find($this->noticeUser['user_id']);
                
        ob_start();
        echo $this->getHeader();
        if ($this->isFirst){
        ?>
        <p>
            Buongiorno,<br/>
            il portfolio è in fase di chiusura, di seguito viene riepilogata la situazione:
        </p>
        <?php
        }
        
        require_once __DIR__ . '/../../action/structure/portfolio/PortfolioAction.php';
        $portfolioAction = new PortfolioAction();
        $this->printCategory($portfolioAction->dataQualificazione);
        $this->printCategory($portfolioAction->dataSviluppo);
        
        echo $this->getFooter();
        return ob_get_clean();
    }

    public function printCategory($categories) {
        foreach($categories as $category){
            ?>
            <div style="margin-bottom: 20px;">
                <h3><?= _t($category['label']) ?></h3>
                <table>
                <?php foreach ($category['sections'] as $section){?>
                    <tr><td style="width: 250px;"><?= _t($section['title']) ?></td><td>
                    <?php
                    $attachments = AttachmentService::findAllForEntity(EntityName::USER, $this->user['user_id'], $section['type']);
                    if ($attachments->rowCount() == 0){ ?>
                        <span style="color: red;">Nessun documento caricato</span>
                    <?php 
                    } else {
                        foreach ($attachments as $attachment){
                            echo $attachment['filename'] .'<br/>';
                        }
                    }
                    ?>
                    </td></tr>
                    <?php
                }
                ?>
                </table>
            </div>
            <?php
        }
    }

}
