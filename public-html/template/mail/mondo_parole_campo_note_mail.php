<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of drop_out_new_row
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class mondo_parole_campo_note_mail extends MailBaseTemplate{
    
    public function getSubject() {
        return "Nuove note sulla prova"; 
    }
    
    public function getMessage() {
        $notice = NoticeService::find($this->noticeUser["notice_id"]);
        $strc = SomministrazioneTestResultClassService::find($notice["entity_id"]);
        $somministrazioneTest = SomministrazioneService::findSomministrazioneTest($strc["somministrazione_test_id"]);
        $test = TestService::find($somministrazioneTest["test_id"]);

        $class = StructureClassService::find($strc["structure_class_id"]);
        $subStructure = StructureService::find($class["structure_id"]);
        $structure = StructureService::find($subStructure["parent_structure_id"]);

        $classDictionary = DictionaryService::findByGroupAndKey(Dictionary::CLASS_GRADE, $class["class_dictionary_value"]);
        $className = $structure["name"] . ' - ' . $subStructure["name"] . ' - ' . $classDictionary["value"] .'/'. $class["section"];
        
        $url = 'https://www.futurabolzano.it/structure/test/SomministrazioneTestAction?selected_structure_id=' . $structure["structure_id"]  . 
                '&classId=' . $class["structure_class_id"] . 
                '&id=' . $somministrazioneTest["somministrazione_id"] . 
                '&testId=' . $somministrazioneTest["test_id"];

        ob_start();
        echo $this->getHeader();
        if ($this->isFirst){
            ?>
            <h4>
                Nuove note sulla prova per Mondo delle parole progetto Letto Scrittura:
            </h4>
            <p>
                Premi sul link per visualizzare le note inserite sulle somministrazioni delle seguenti classi:
            </p>
            <?php
        }
        ?>
        <div>
            <a href=<?= $url ?>"><?= $className ?> test: <?=  _t($test["name"]) ?></a>
        </div>
        <?php
        echo $this->getFooter();
        return ob_get_clean();    
    }
    
}
