<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of drop_out_new_document
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class drop_out_new_document_mail extends MailBaseTemplate{
    
    public function getSubject() {
        return "Nuova segnalazione inserita"; 
    }
    
    public function getMessage() {
        $notice = NoticeService::find($this->noticeUser["notice_id"]);
        $document = DocumentService::find($notice['entity_id']);
        $schoolYear = SchoolYearService::find($document["school_year_id"]);
        $user = UserService::find($document["user_id"]);
        $userStructure = StructureService::findUserStructureForUserIdId($document["user_id"]);
        $structure = StructureService::find($userStructure["structure_id"]);

        ob_start();
        echo $this->getHeader();
        if ($this->isFirst){
            ?>
            <h4>
                Nuove segnalazioni Drop-Out:
            </h4>
            <?php
        }
        ?>
        <div>
            <table>
                <tr><td>Anno scolastico: </td><td><?= _t($schoolYear["school_year"]) ?></td></tr>
                <tr><td>Istituto: </td><td><?= _t($structure["name"]) ?></td></tr>
                <tr><td>Studente: </td><td><?= _t($user["code"]) ?> - <?= _t($user["name"]) ?> <?= _t($user["surname"]) ?></td></tr>
            </table>
        </div>
        <?php
        echo $this->getFooter();
        return ob_get_clean();    
    }
    
}
