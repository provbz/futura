<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of e_model_approved
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class e_model_approved_mail extends MailBaseTemplate{
    
    public function getSubject() {
        return "Verifiche modello E"; 
    }
    
    public function getMessage() {
        $eModel = EModelService::find($this->notice["entity_id"]);

        $url = $this->getPlatformUrl() . '/structure/eModel/EmodelAction-edit?id=' . $this->notice["entity_id"];

        ob_start();
        echo $this->getHeader();
        if ($this->isFirst){
            ?>
            <h4>
                Verifica modello E
            </h4>
            <p>
                I seguenti modelli E sono stati ricevuti e verificati dall'amministrazione:
            </p>
            <?php
        }
        ?>
        <div>
            <a href=<?= $url ?>"><?= _t($eModel["codice"]) ?> - <?=  _t($eModel["nome"]) ?> <?=  _t($eModel["cognome"]) ?></a>
            <?php if (StringUtils::isNotBlank( $eModel["note_per_referenti"] )){ ?>
                <p>
                    <?= _t($eModel["note_per_referenti"]) ?>
                </p>
            <?php } ?>
        </div>
        <?php
        echo $this->getFooter();
        return ob_get_clean();    
    }
}
