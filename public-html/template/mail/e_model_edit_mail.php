<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of e_model_edit
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class e_model_edit_mail extends MailBaseTemplate{
    
    public function getSubject() {
        return "Modifica al modello E"; 
    }
    
    public function getMessage() {
        if ($this->par == null){
            return;
        }

        $eModel = EModelService::find($this->par["e_model_id"]);

        $url = $this->getPlatformUrl() . '/admin/eModel/EmodelAdminAction-edit?id=' . $this->par["e_model_id"];

        ob_start();
        echo $this->getHeader();
        if ($this->isFirst){
            ?>
            <h4>
                Modifiche al modello E
            </h4>
            <p>
                I seguenti moduli sono stati modificati e richiedono verifica:
            </p>
            <?php
        }
        ?>
        <div>
            <a href=<?= $url ?>"><?= _t($eModel["codice"]) ?> - <?=  _t($eModel["nome"]) ?> <?= _t($eModel["cognome"]) ?></a>
        </div>
        <?php
        echo $this->getFooter();
        return ob_get_clean();    
    }
}
