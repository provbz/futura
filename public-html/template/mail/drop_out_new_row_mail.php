<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of drop_out_new_row
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class drop_out_new_row_mail extends MailBaseTemplate{
    
    public function getSubject() {
        return "Nuova segnalazione inserita"; 
    }
    
    public function getMessage() {
        $document = DocumentService::find($this->par['user_year_document_id']);
        $user = UserService::find($document["user_id"]);
        $userStructure = StructureService::findUserStructureForUserIdId($document["user_id"]);
        $structure = StructureService::find($userStructure["structure_id"]);

        $assenza = DictionaryService::find($this->par["drop_out_assenza_motivazioni_dictionary_id"]);
        $action = DictionaryService::find($this->par["dro_out_row_type_dictionary_id"]);

        ob_start();
        echo $this->getHeader();
        if ($this->isFirst){
            ?>
            <h4>
                Nuove attività su Drop-Out:
            </h4>
            <?php
        }
        ?>
        <div>
            <table>
                <tr><td>Istituto: </td><td><?= _t($structure["name"]) ?></td></tr>
                <tr><td>Studente: </td><td><?= _t($user["code"]) ?> - <?= _t($user["name"]) ?> <?= _t($user["surname"]) ?></td></tr>
                <tr><td>Motivazione presunta: </td><td><?= _t($assenza["value"]) ?></td></tr>
                <tr><td>Azione: </td><td><?= _t($action["value"]) ?></td></tr>
            </table>
        </div>
        <?php
        echo $this->getFooter();
        return ob_get_clean();    
    }
    
}
