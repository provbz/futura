<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class log_error_mail extends MailBaseTemplate{
    
    public $user;
    
    public function getSubject() {
        return "Log errore";
    }
    
    public function getMessage() {
        $this->user = UserService::find($this->noticeUser['user_id']);
                
        ob_start();
        echo $this->getHeader();
        if ($this->isFirst){
        ?>
        <p>
            Errori piattaforma Futura
        </p>
        <?php
        }
        ?>
        <pre>
            <?php echo json_encode($this->par); ?>
        </pre>
        <?php
        
        echo $this->getFooter();
        return ob_get_clean();
    }
}