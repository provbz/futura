<?php
/*
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MailService
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once __DIR__ . '/MailBaseTemplate.php';
class user_transfer_add_mail extends MailBaseTemplate{
    
    public function getSubject() {
        if ($this->par["type"] == UserTransferRequestType::FROM_DESTINATION){
            return "Nuova richiesta di trasferimento"; 
        } else if ($this->par["type"] == UserTransferRequestType::FROM_SOURCE) {
            return "Nuovo avviso di trasferimento"; 
        }
    }
    
    public function getMessage() {
        $user = UserService::find($this->par['user_id']);
        $requesterUser = UserService::find($this->par['requester_user_id']);
        $fromStructure = StructureService::find($this->par['from_structure_id']);
        $toStructure = StructureService::find($this->par['to_structure_id']);
        
        ob_start();
        echo $this->getHeader();
        ?>
        <div>
            <?php if ($this->par["type"] == UserTransferRequestType::FROM_DESTINATION){ ?>
                Codice richiesta di trasferimento: <?= _t($this->par['user_transfer_request_id']) ?><br/><br/>

                L'utente <?= _t($requesterUser['email']) ?> ha inviato una richiesta di trasferimento per lo studente <?= $user['code'] ?>
                da "<?= _t($fromStructure['name']) ?>" a "<?= _t($toStructure['name']) ?>".<br/><br/>

                La richiesta può essere gestita collegandosi alla piattaforma e accedendo alla sezione "TRASFERIMENTI".<br/>
                Si fa presente che una gestione rapida delle richieste permetterà  un miglior lavoro da parte di tutti gli utenti.<br/>
            <?php } else if ($this->par["type"] == UserTransferRequestType::FROM_SOURCE) { ?>
                Codice avviso di trasferimento: <?= _t($this->par['user_transfer_request_id']) ?><br/><br/>

                L'utente <?= _t($requesterUser['email']) ?> ha inviato un avviso di trasferimento per lo studente <?= _t($user['code']) ?>
                da "<?= _t($fromStructure['name']) ?>" a "<?= _t($toStructure['name']) ?>".<br/><br/>

                Il trasferimento verrà completato automaticamente al passaggio di anno scolastico.<br/>
                È possibile visualizzare i documenti dello studente accedendo alla sezione "TRASFERIMENTI" della piattaforma Futura.<br/>
            <?php } ?>
        </div>
        <?php
        echo $this->getFooter();
        return ob_get_clean();    
    }
    
}
