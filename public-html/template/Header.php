<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MyRtfExtension
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

//UserService::updateOnlineUser();

mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_language('uni');
mb_regex_encoding('UTF-8');
ob_start('mb_output_handler');

header('Content-type: text/html; charset=utf-8');

?>
<!DOCTYPE html>
<html class="no-js" lang="it-IT" <?= isset($angularApp) && $angularApp != NULL ? 'ng-app="'.$angularApp.'"' : "" ?>>
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="/favicon.ico"></link>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
        
        <link rel="shortcut icon" href="<?php echo UriService::buildImageUrl("favicon.ico"); ?>" type="image/x-icon">
        <link rel="icon" href="<?php echo UriService::buildImageUrl("favicon.ico"); ?>" type="image/x-icon">
        
        <meta charset="ISO-8859-1">
        <meta name="robots" content="index, follow">
        <meta name="robots" content="noodp, noydr">   
            
        <title><?= _t(Constants::$APP_PRINT_NAME) ?>: Compilazione online di PEI e PDP</title>
        <meta name="description" content="<?= _t(Constants::$APP_PRINT_NAME) ?>: piattaforma online per la compilazione di PEI e PDP">
        <meta name="keywords" content="Piano Educativo Individualizzato, PEI, Piano Didattico Personalizzato, PDP">
        
        <link href='https://fonts.googleapis.com/css?family=Dosis:400,700,500,600' rel='stylesheet' type='text/css'/>
        <?php 
        if (is_array($this->additionalCSS)){
            foreach ($this->additionalCSS as $key => $value){
                echo '<link type="text/css" rel="stylesheet" href="'.UriService::buildCSSUrl($value) . '?version='.VERSION.'"/>';
                echo "\n";
            }
        }
        ?>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        
        <?php if (Constants::$useVueDevServer) { ?>
            <script src="http://localhost:8080/app.js?v=<?= VERSION ?>"></script>
        <?php } else { 
            ScriptService::includeVue();
        }
        ?>

        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <?php
        if (is_array($this->additionalScripts)){
            foreach ($this->additionalScripts as $key => $value){
                echo '<script type="text/javascript" src="'.UriService::buildScriptUrl( $value ) .'?version='.VERSION.'"></script>';
                echo "\n";
            }
        } ?>

        <script src="/script/summernote/summernote-lite.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/script/summernote/summernote-lite.min.css"">

        <link rel="stylesheet" type="text/css" href="/script/mb/DEDFile_2/DEDFileUpload.css?v=<?= VERSION ?>"">
        <script src="/script/mb/DEDFile_2/DEDFileUpload.js?v=<?= VERSION ?>"></script>
        
        <link rel="stylesheet" type="text/css" href="/script/vendor/pivottable/pivot.css?v=<?= VERSION ?>"">
        <script src="/script/vendor/pivottable/pivot.js?v=<?= VERSION ?>"></script>

        <script type="text/javascript">
        <?php
            global $currentUserAllowedAction;
        ?>
        var currentUserAlloedActions = <?= json_encode(array_keys($currentUserAllowedAction)) ?>;
        </script>

        <?php
        if(Constants::$ENV_NAME == "PROD"){
        ?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-98439991-6"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-98439991-6');
            </script>
        <?php } ?>
    </head>
    <body class="<?= isset($bodyClass) ? $bodyClass : '' ?>" style="zoom: 1; overflow:auto;">
        <?php
        global $currentUser, $requestPackage, $local;
        if ($currentUser != NULL){ 
            if (!$local){  ?>
                <div class="mb-inactive-user"></div>
            <?php } ?>
            <div class="mb-right-panel"></div>
        <?php } ?>
        
        <div id="dialog"></div>
        
        <?php if (!isset($displayPageHeader) || $displayPageHeader == true){ ?>
        <div class="row-100 ingradimento-1024 contenitore-loghi-erickson">   		
            <div class="large-5 medium-5 small-5 columns left text-left logo" style="display: flex;">
                <a href="/" >
                    <img id="header-logo" src="/img/logo_200.png" alt="Logo provincia di Bolzano"/>
                </a>

                <?php if (Constants::$ENV_NAME == EnvName::$TEST) { ?>
                    <h3 style="padding-left: 10px;">Ambiente di TEST</h3>
                <?php } ?>
            </div>
            
            <?php if ($currentUser != NULL){ ?>
                <div class="small-5 medium-5 large-5 columns areabottoni">
                    <?php 
                    $class = 'disabled';
                    $profileUrl = '';
                    if(UserRoleService::canCurrentUserDo(UriService::buildPageUrl("/user/EditProfileAction"))){
                        $profileUrl = UriService::buildPageUrl("/user/EditProfileAction");
                        $class = "";
                    }
                    ?>               
                    <div class="row header-buttons">
                        <?php
                        $userRootStructures = StructureService::findUserStructuresRoot($currentUser['user_id']);
                        if (UserRoleService::canCurrentUserDo(UserPermission::STRUCTURE_ACCESS_ALL) ){
                            $userRootStructures = StructureService::findAllRoot();
                        }
                        
                        $selectedId = getSessionValue(GlobalSessionData::SELECTED_STRUCTURE_ID, '');
                        $structures = [];
                        $selectedStructure = null;

                        foreach ($userRootStructures as $row) {
                            $structures[] = [
                                "text" => $row['name'],
                                "value" => $row['structure_id']
                            ];

                            if ($row["structure_id"] == $selectedId){
                                $selectedStructure = (object)[
                                    "text" => $row['name'],
                                    "value" => $row['structure_id']
                                ];
                            }
                        }
                        
                        ?>
                        <?php if (count($structures) > 1 || strpos($_SERVER['REQUEST_URI'], '/admin/') === 0){
                            $structureSelectParameters = [
                                "options" => $structures,
                                "value" => $selectedStructure
                            ];
                            ?>
                        <div class="columns small-5 large-6">
                            <div class="mb-structure-select-field-container"></div>
                            <script>
                                MbCreateVue('mb-structure-select-field', "mb-structure-select-field-container", <?= json_encode($structureSelectParameters) ?>);
                            </script>
                        </div>
                        <?php } ?>
                        
                        <?php 
                        $baseAction = $this->getBaseAction();
                        if (strpos($baseAction, "/admin/") === 0){ ?>
                            <a title="Amministrazione" href="<?= $baseAction ?>?selected_structure_id=0" class="header-link"><i class="fas fa-laptop"></i></a>
                        <?php } ?>
                        <a title="Profilo" href="<?= $profileUrl ?>" class="header-link profile <?= $class ?>"><i class="fa fa-lg fa-user"></i></a>
                        <a title="Esci" href="javascript:void(0);" onclick="return signout();" class="header-link logout"><i class="fa fa-lg fa-power-off"></i></a>
                    </div>
                </div>
            <?php } ?>
            
            <div class="large-2 medium-2 small-4 columns right text-right">
                <a href="http://www.provincia.bz.it/it/" target="_blank">	
                    <img id="logo-right" src="/img/logo_pr_it-de.png" alt="Provincia di Bolzano">
                </a>
            </div>
        </div>

        <header class="contenitore_preheader">
            &nbsp;
        </header>	
        
        <div class="global-wrapper">
            <!-- STRIPE -->
            <div class="row-100">
                <div class="large-12 columns">
                    
                <?php if ( $this->menu != NULL){ ?>
                    <?= $this->menu->drawFirstLevel(); ?>
                    <div class="tabs-content">
                <?php } ?> 
                
                <?php require_once('./template/Message.php'); ?>    
    <?php } ?>

    
