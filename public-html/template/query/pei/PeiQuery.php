<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PeiQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PeiQuery extends QueryBase implements IQuery{

    public function descriptor() {
        if ($this->descriptor != null){
            return $this->descriptor;
        }

        $structures = StructureService::findAllRootAsDictionary(true);

        $schoolYearsDb = SchoolYearService::findAll();
        $schoolYears = [];
        foreach($schoolYearsDb as $sy){
            array_push($schoolYears, [
                "value" => $sy["school_year_id"],
                "text" => $sy["school_year"]
            ]);
        }

        $currentSchoolYear = SchoolYearService::findCurrentYear();

        $status = [
            ["value" => RowStatus::DELETED, "text" => RowStatus::DELETED],
            ["value" => RowStatus::READY, "text" => RowStatus::READY],
        ];
        asort($status);

        $types = [
            ["value" => DocumentType::PEI, "text" => DocumentType::PEI],
            ["value" => DocumentType::PDP, "text" => DocumentType::PDP],
            ["value" => DocumentType::PDP_LINGUA, "text" => "PDP bisogni linguistici"],
            ["value" => DocumentType::PEI_TIROCINIO, "text" => "PEI tirocinio"]
        ];

        $descriptor = [
            "version" => 4,
            "permissions" => [
                "/admin/pei/PeiAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "structure_id" => [
                    "columnName" => "s.structure_id",
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true
                ],
                "user_year_document_id" => [
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                    "enableSorting" => true,
                    "label" => "ID"
                ],
                "school_year" => [
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "label" => "Anno scolastico"
                ],
                "user_id" => [
                    "columnName" => "u.user_id",
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true
                ],
                "s_name" => [
                    "label" => "Istituto",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "s.name"
                ],
                "code" => [
                    "label" => "Codice",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "u.code"
                ],
                "type" => [
                    "label" => "Tipo doc",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "uyd.type"
                ],
                "pei_status" => [
                    "label" => "Stato compilazione",
                    "displayAsTableColumn" => true
                ],
                "status" => [
                    "label" => "Stato",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "uyd.status"
                ]
            ],
            "conditions" => [
                "school_year_id" => [
                    "label" => "Anno scolastico",
                    "columnName" => "sy.school_year_id",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $schoolYears,
                    "defaultValue" => [
                        "value" => $currentSchoolYear["school_year_id"],
                        "text" => $currentSchoolYear["school_year"]
                    ]
                ],
                "structure_id" => [
                    "label" => "Istituto",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $structures,    
                    "addEmptyValue" => true
                ],
                "status" => [
                    "label" => "Stato",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $status,    
                    "addEmptyValue" => true
                ],
                "type" => [
                    "label" => "Tipo",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $types,    
                    "addEmptyValue" => true
                ],
                /*
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true,
                ],
                */
                "code" => [
                    "label" => "Codice",
                    "displayAsTableFilter" => true
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        $this->descriptor = $descriptor;
        return $this->descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        $referentFor = $this->getCondition("referent_for");
        $role = $this->getCondition("role");

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM user u
            LEFT JOIN user_structure us ON u.user_id=us.user_id
            LEFT JOIN structure s ON us.structure_id=s.structure_id
            LEFT JOIN user_year uy ON u.user_id=uy.user_id
            LEFT JOIN user_year_document uyd ON uy.user_id=uyd.user_id
            LEFT JOIN school_year sy ON uyd.school_year_id = sy.school_year_id
            WHERE u.code IS NOT NULL AND
            uyd.type IN (:pdp, :pei, :pei_tirocinio, :pdp_lingua)
        <?php 
        $this->params["pdp"] = DocumentType::PDP;
        $this->params["pei"] = DocumentType::PEI;
        $this->params["pei_tirocinio"] = DocumentType::PEI_TIROCINIO;
        $this->params["pdp_lingua"] = DocumentType::PDP_LINGUA;

        echo $this->addConditionEqual("school_year_id");
        echo $this->addConditionEqual("structure_id");
        echo $this->addConditionEqual("type");
        echo $this->addConditionEqual("status");
        echo $this->addConditionLike("code");

        $name = $this->getCondition("name");
        if (StringUtils::isNotBlank($name)){
            echo " AND CONCAT(u.name, u.surname) LIKE :name";
            $this->params["name"] = "%$name%";
        }

        if ($type != QueryType::COUNT){
            echo $this->buildSort("s.name, u.surname");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcelExport = false){
        $newLine = $isExcelExport ? "\n" : "<br/>";

        $updatedResults = [];
        foreach($results as $row){
            $updatedResults[] = $row;
        }

        return $updatedResults;
    }
}