<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserDetailsQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserDetailsQuery extends QueryBase implements IQuery{

    public function descriptor() {
        if ($this->descriptor != null){
            return $this->descriptor;
        }

        $descriptor = [
            "permissions" => [
                "user_details"
            ],
            "excelExportEnabled" => false,
            "enableOptions" => false,
            "fields" => [
            ],
            "conditions" => [
                "user_id" => [
                    "label" => "ID"
                ]
            ]
        ];
        
        $this->descriptor = $descriptor;
        return $this->descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        $userId = $this->getCondition("user_id");
        if (StringUtils::isBlank($userId)){
            throw new Exception("Specificare userId");
        }

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ ?>
            user_id, name, surname, code
        <?php } ?>
            FROM user u
            WHERE 1=1
        <?php 
        echo $this->addConditionEqual("user_id");

        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results){
        if (count($results) == 0){
            return $results;
        }

        $user = $results[0];
        if (!UserRoleService::canCurrentUserDo(UserPermission::user_details) && !$this->canLoggedUserViewUserDetails($user["user_id"])){
            return null;
        }

        $userYear = UserYearService::findUserYearByUser($user["user_id"]);

        $user["plesso"] = StructureService::printFullStructureName($userYear["plesso_structure_id"]);
        $user["classe"] = DictionaryService::findByGroupAndKey(Dictionary::CLASS_GRADE, $userYear["classe"]);
        $user["sezione"] = $userYear["sezione"];
        $user["nazionalita_ita_dictionary_key"] = DictionaryService::findByGroupAndKey(Dictionary::NAZIONALITA_ITA, $userYear["nazionalita_ita_dictionary_key"]);
        
        $classi = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);

        $somministrazioni = $this->findSomministrazioniForUser($user["user_id"]);

        $somministrazioniData = [];
        foreach ($somministrazioni as $somministrazione) {
            $s = [
                "classe" => ArrayUtils::getIndex($classi, $somministrazione["class_dictionary_value"]),
                "periodo" => $somministrazione["periodo_dictionary_value"],
                "data_inizio" => $somministrazione["data_inizio"],
                "data_fine" => $somministrazione["data_fine"],
                "test_data" => $this->findSomministrazioneResults($somministrazione["somministrazione_id"], $user["user_id"])
            ]; 

            $somministrazioniData[] = $s;
        }

        $user["somministrazioni"] = $somministrazioniData;
        
        return [$user];
    }

    private function findSomministrazioniForUser($userId){
        return EM::execQuery("select * 
            from somministrazione
            where somministrazione_id IN (
                select distinct(st.somministrazione_id)
                from somministrazione_test_result_user stru
                left join somministrazione_test st ON st.somministrazione_test_id=stru.somministrazione_test_id
                where user_id=:user_id
            ) order by data_inizio, data_fine", [
                "user_id" => $userId
            ]);
    }

    private function findSomministrazioneResults($somministrazioneId, $userId){
        $testRawDatas = EM::execQuery("select somministrazione_test_result_user_id, name, status, raw_data
            from somministrazione_test_result_user stru
            left join somministrazione_test st ON st.somministrazione_test_id=stru.somministrazione_test_id
            left join test t on st.test_id=t.test_id
            where user_id=:user_id and somministrazione_id=:somministrazione_id", [
                "user_id" => $userId,
                "somministrazione_id" => $somministrazioneId
            ]);

        $res = [];

        foreach ($testRawDatas as $td) {
            $td["variables"] = $this->findTestVariables($td["somministrazione_test_result_user_id"]);
            $td["raw_data"] = json_decode($td["raw_data"]);

            $res[] = $td;
        }

        return $res;
    }

    private function findTestVariables($resultUserId){
        $variables = EM::execQuery("select variable_name, value, fascia
            from somministrazione_test_result_user_variable
            where somministrazione_test_result_user_id=:id", [
                "id" => $resultUserId
            ]);
        
        return $variables->fetchAll();
    }

    private function canLoggedUserViewUserDetails($userId){
        if (UserRoleService::hasCurrentUser(UserRole::ADMIN)){
            return true;
        }

        if (UserRoleService::hasCurrentUser(UserRole::AMMINISTRATORE_SOMMINISTRAZIONI)){
            if (UserRoleService::hasUserRole($userId, UserRole::STUDENTE)){
                return true;
            }
        }

        if (UserRoleService::hasCurrentUser(UserRole::DIRIGENTE) || UserRoleService::hasCurrentUser(UserRole::REFERENTE_ISTITUTO_SOMMINISTRAZIONI)){
            if (StructureService::hasUserStructure($userId, StructureService::getCurrentStructureId())){
                return true;
            }
        }

        if (UserRoleService::hasCurrentUser(UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI)){
            $loggedUser = UserService::getLoggedUser();

            $res = EM::execQuerySingleResult("select count(1) as c
                from structure_class_user scu
                left join structure_class sc ON scu.structure_class_id=sc.structure_class_id
                where structure_id IN (
                    select structure_id
                    from user_structure_role usr
                    left join user_structure us ON usr.user_structure_id=us.user_structure_id
                    left join user u on us.user_id=u.user_id
                    where role_id=:referente_role_id and us.user_id=:referente_user_id
                )
                and user_id=:student_user_id", [
                    ":referente_role_id" => UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI,
                    ":referente_user_id" => $loggedUser["user_id"],
                    ":student_user_id" => $userId
                ]);
            if ($res["c"] == 1){
                return true;
            }
        }

        if (UserRoleService::hasCurrentUser(UserRole::INSEGNANTE_SOMMINISTRAZIONI)){
            $loggedUser = UserService::getLoggedUser();

            $res = EM::execQuerySingleResult("select count(1) as c
                from structure_class_user scu
                where structure_class_id IN (
                    select structure_class_id
                    from structure_class_user scu
                    where role_id=:insegnante_role_id and user_id=:insegnante_user_id
                )
                and user_id=:student_user_id", [
                    ":insegnante_role_id" => UserRole::INSEGNANTE_SOMMINISTRAZIONI,
                    ":insegnante_user_id" => $loggedUser["user_id"],
                    ":student_user_id" => $userId
                ]);
            if ($res["c"] == 1){
                return true;
            }
        }

        return false;
    }
}