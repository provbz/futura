<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DropOutQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DropOutQuery extends QueryBase implements IQuery{

    private $structureId;
    private $classes;
    private $currentSchoolYear;
    private $schoolGrades;
    private $schoolYearList;
    private $schoolYears;
    private $plessoStructureIds;

    public function __construct(){
        $this->structureId = ArrayUtils::getIndex($_SESSION, GlobalSessionData::SELECTED_STRUCTURE_ID, 0);
        $this->plessoStructureIds = [];
        $plessi = StructureService::findAllWithParent($this->structureId);
        foreach($plessi as $plesso){
            $this->plessoStructureIds[] = [
                "value" => $plesso["structure_id"],
                "text" => $plesso["name"]
            ];
        }

        $this->currentUser = UserService::getLoggedUser();
        $this->classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $this->currentSchoolYear = SchoolYearService::findCurrentYear();
        $this->schoolGrades = DictionaryService::findGroupAsDictionary(Dictionary::GRADE, false, "key", true);

        $years = SchoolYearService::findAll();
        $this->schoolYearList = [];
        foreach ($years as $year) {
            $this->schoolYears[$year["school_year_id"]] = $year["school_year"];
            $this->schoolYearList[] = [
                "value" => $year["school_year_id"],
                "text" => $year["school_year"]
            ];
        }
    }

    public function descriptor() {
        if ($this->descriptor != null){
            return $this->descriptor;
        }

        $subStructures = StructureService::findAllWithParent($this->structureId);
        $structures = ["" => ""];
        foreach($subStructures as $subStructure){
            $structures[$subStructure["structure_id"]] = $subStructure["name"];
        }

        $descriptor = [
            "version" => 4,
            "permissions" => [
                "/structure/dropOut/DropOutUsersAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "user_year_document_id" => [
                    "columnName" => "user_year_document_id",
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
                "school_year_id" => [
                    "alwaysSelect" => false,
                    "displayAsTableColumn" => true,
                    "label" => "Anno scolastico"
                ],
                "uy_plesso_structure_id" => [
                    "columnName" => "uy.plesso_structure_id",
                    "label" => "Plesso",
                    "displayAsTableColumn" => true
                ],
                "classe" => [
                    "label" => "Classe",
                    "displayAsTableColumn" => true
                ],
                "sezione" => [
                    "label" => "Sezione",
                    "displayAsTableColumn" => true
                ],
                "code" => [
                    "label" => "Codice",
                    "displayAsTableColumn" => true
                ],
                /*
                "name" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true
                ],
                */
                "um_value" => [
                    "label" => "Data di nascita",
                    "columnName" => "um.value",
                    "displayAsTableColumn" => true
                ],
                "drop_out_age" => [
                    "label" => "Età",
                    "displayAsTableColumn" => true
                ],
                "obb_scol" => [
                    "label" => "Posizione in merito obb. Scol.",
                    "displayAsTableColumn" => true,
                    "isComputedValue" => true
                ],
                "drop_out_status" => [
                    "label" => "Stato",
                    "displayAsTableColumn" => true
                ]
            ],
            "conditions" => [
                "school_year_id" => [
                    "label" => "Anno scolastico",
                    "displayAsTableFilter" => true,
                    "defaultValue" => [
                        "value" => $this->currentSchoolYear['school_year_id'],
                        "text" => $this->currentSchoolYear['school_year']
                    ],
                    "allowedValuesList" => $this->schoolYearList
                ],
                "plesso_structure_id" => [
                    "label" => "Plesso",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $this->plessoStructureIds
                ],
                "classe" => [
                    "label" => "Classe",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->classes
                ],
                "code" => [
                    "label" => "Codice",
                    "displayAsTableFilter" => true
                ],
                /*
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableFilter" => true
                ]
                */
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        $this->descriptor = $descriptor;
        return $this->descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM user_year_document uyd
            LEFT JOIN user_year uy ON uyd.user_id  = uy.user_id
            LEFT JOIN user u ON u.user_id  = uy.user_id
            LEFT JOIN user_structure us ON u.user_id  = us.user_id
            LEFT JOIN user_metadata um ON u.user_id  = um.user_id AND um.metadata_id='birth_date'
            WHERE structure_id=:structure_id
                AND uyd.status=:status_ready
                AND uyd.type=:document_type
        <?php 

        if (!UserRoleService::canCurrentUserDo(UserPermission::DROP_OUT_VIEW_ALL)){
            $structures = StructureService::findUserStructureWithParent($this->currentUser['user_id'], $this->structureId);
            $usq = '';
            $i = 0;
            foreach($structures as $structure){
                $usq .= $usq == '' ? '' : ', ';
                $usq .= ':us'.$i;
                $this->params['us'.$i] = $structure['structure_id'];
                $i++;
            }
            echo ' AND plesso_structure_id IN (' . $usq . ')';
        }

        $this->params["structure_id"] = $this->structureId;
        $this->params["status_ready"] = UserYearDocumentStatus::READY;
        $this->params["document_type"] = DocumentType::DROP_OUT;

        echo $this->addConditionEqual("school_year_id");
        echo $this->addConditionEqual("plesso_structure_id");
        echo $this->addConditionEqual("classe");
        echo $this->addConditionLike("code");
        echo $this->addConditionLike("name");
        echo $this->addConditionLike("surname");
        
        if ($type != QueryType::COUNT){
            echo $this->buildSort("plesso_structure_id, classe, sezione, u.name, u.surname, u.code");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcelExport = false){
        for($i = 0; $i < count($results); $i++){
            $results[$i]['school_year_id'] = $this->schoolYears[$results[$i]['school_year_id']];
            $results[$i]['uy_plesso_structure_id'] = StructureService::printFullStructureName($results[$i]['uy_plesso_structure_id']);
            $results[$i]['classe'] = ArrayUtils::getIndex($this->classes, $results[$i]['classe']);
            $results[$i]["um_value"] = $this->decryptBirthDate($results[$i]["um_value"]);

            $results[$i]["obb_scol"] = $this->computeObbligoScolastico($results[$i]["drop_out_age"]);
        }
        return $results;
    }

    private function computeObbligoScolastico($age){
        if (StringUtils::isBlank($age)){
            return "";
        }

        $stato = "";
        if ($age < 16){
            $stato = "Obbligo scolastico";
        } else if ($age >= 18){
            $stato = "Assolto il diritto-dovere all’istruzione e formazione";
        } else {
            $stato = "Assolto obbligo scolastico";
        }
        return $stato;
    }

    private function decryptBirthDate($v){
        $birthDate = EncryptService::decrypt($v);
        $decriptedValue = EncryptService::decrypt($v);
        return TextService::formatDate($decriptedValue);
    }
}