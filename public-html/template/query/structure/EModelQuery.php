<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EModelQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EModelQuery extends QueryBase implements IQuery {

    private $structureId;
    private $classes;
    private $currentSchoolYear;
    private $schoolGrades;
    private $schoolYearList;
    private $schoolYears;
    private $plessoStructureIds;
    private $diagnosyType = [];

    public function __construct(){
        $this->structureId = ArrayUtils::getIndex($_SESSION, GlobalSessionData::SELECTED_STRUCTURE_ID, 0);
        $this->plessoStructureIds = [];
        $plessi = StructureService::findAllWithParent($this->structureId);
        foreach($plessi as $plesso){
            $this->plessoStructureIds[] = [
                "value" => $plesso["structure_id"],
                "text" => $plesso["name"]
            ];
        }

        $this->currentUser = UserService::getLoggedUser();
        $this->classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $this->currentSchoolYear = SchoolYearService::findCurrentYear();
        $this->schoolGrades = DictionaryService::findGroupAsDictionary(Dictionary::GRADE, false, "key", true);
        $this->diagnosyType = DictionaryService::findGroupAsDictionary(Dictionary::MODEL_E_DIAGNOSY_TYPE, false, "key", true);

        $years = SchoolYearService::findAll();
        $this->schoolYearList = [];
        foreach ($years as $year) {
            $this->schoolYears[$year["school_year_id"]] = $year["school_year"];
            $this->schoolYearList[] = [
                "value" => $year["school_year_id"],
                "text" => $year["school_year"]
            ];
        }
    }

    public function descriptor() {
        if ($this->descriptor != null){
            return $this->descriptor;
        }

        $subStructures = StructureService::findAllWithParent($this->structureId);
        $structures = ["" => ""];
        foreach($subStructures as $subStructure){
            $structures[$subStructure["structure_id"]] = $subStructure["name"];
        }

        $descriptor = [
            "version" => 7,
            "permissions" => [
                "/structure/eModel/EmodelAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "e_model_id" => [
                    "columnName" => "e_model_id",
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
                "closed" => [
                    "label" => "Archiviato",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
                "school_year_id" => [
                    "alwaysSelect" => false,
                    "displayAsTableColumn" => true,
                    "label" => "Anno scolastico"
                ],
                "plesso_structure_id" => [
                    "columnName" => "plesso_structure_id",
                    "label" => "Plesso",
                    "displayAsTableColumn" => true
                ],
                "class" => [
                    "label" => "Classe",
                    "displayAsTableColumn" => true
                ],
                "sezione" => [
                    "label" => "Sezione",
                    "displayAsTableColumn" => true
                ],
                "codice" => [
                    "label" => "Codice",
                    "displayAsTableColumn" => true
                ],
                "nome" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true
                ],
                "cognome" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true
                ],

                "diagnosy_type_dictionary" => [
                    "label" => "Tipo diagnosi",
                    "displayAsTableColumn" => true
                ],
                "codici" => [
                    "label" => "Codici",
                    "displayAsTableColumn" => true,
                    "isComputedValue" => true
                ],
                
                "edited" => [
                    "label" => "Stato modifica",
                    "displayAsTableColumn" => true
                ],
                "last_edit_date" => [
                    "label" => "Data modifica",
                    "displayAsTableColumn" => true,
                    "columnName" => "em.last_edit_date"
                ],
                "last_edit_user_id" => [
                    "label" => "Modificato da",
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true,
                    "columnName" => "em.last_edit_user_id"
                ]
            ],
            "conditions" => [
                "school_year_id" => [
                    "label" => "Anno scolastico",
                    "displayAsTableFilter" => true,
                    "defaultValue" => [
                        "value" => $this->currentSchoolYear['school_year_id'],
                        "text" => $this->currentSchoolYear['school_year']
                    ],
                    "allowedValuesList" => $this->schoolYearList
                ],
                "plesso_structure_id" => [
                    "label" => "Plesso",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $this->plessoStructureIds
                ],
                "class" => [
                    "label" => "Classe",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->classes
                ],
                "sezione" => [
                    "label" => "Sezione",
                    "displayAsTableFilter" => true
                ],
                "codice" => [
                    "label" => "Codice",
                    "displayAsTableFilter" => true
                ],
                "nome" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ],
                "cognome" => [
                    "label" => "Cognome",
                    "displayAsTableFilter" => true
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        $this->descriptor = $descriptor;
        return $this->descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ ?>
            <?= $this->buildSelect(); ?> , u.name as u_name, u.surname as u_surname
        <?php } ?>
            FROM e_model em
            LEFT JOIN user u ON em.last_edit_user_id = u.user_id
            WHERE structure_id=:structure_id
        <?php 

        $this->params["structure_id"] = $this->structureId;
        
        echo $this->addConditionEqual("school_year_id");
        echo $this->addConditionEqual("plesso_structure_id");
        echo $this->addConditionEqual("class");
        echo $this->addConditionEqual("sezione");
        echo $this->addConditionLike("codice");
        echo $this->addConditionLike("nome");
        echo $this->addConditionLike("cognome");
        
        if ($type != QueryType::COUNT){
            echo $this->buildSort("plesso_structure_id, class, sezione, nome, cognome, codice");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcelExport = false){
        for($i = 0; $i < count($results); $i++){
            if (isset($results[$i]['school_year_id'])){
                $results[$i]['school_year_id'] = $this->schoolYears[$results[$i]['school_year_id']];
            }

            if (isset($results[$i]['plesso_structure_id'])){
                $results[$i]['plesso_structure_id'] = StructureService::printFullStructureName($results[$i]['plesso_structure_id']);
            }

            if (isset($results[$i]['class'])){
                $results[$i]['class'] = ArrayUtils::getIndex($this->classes, $results[$i]['class']);
            }

            if (isset($results[$i]['last_edit_user_id'])){
                $results[$i]['last_edit_user_id'] = $results[$i]['u_name'] . ' ' . $results[$i]['u_surname'];
            }

            $results[$i]["codici"] = $this->buildCodes($results[$i]["e_model_id"], $isExcelExport);
            $results[$i]["diagnosy_type_dictionary"] = $this->diagnosyType[$results[$i]["diagnosy_type_dictionary"]];
        }
        return $results;
    }

    private function computeObbligoScolastico($age){
        if (StringUtils::isBlank($age)){
            return "";
        }

        $stato = "";
        if ($age < 16){
            $stato = "Obbligo scolastico";
        } else if ($age >= 18){
            $stato = "Assolto il diritto-dovere all’istruzione e formazione";
        } else {
            $stato = "Assolto obbligo scolastico";
        }
        return $stato;
    }

    private function decryptBirthDate($v){
        $birthDate = EncryptService::decrypt($v);
        $decriptedValue = EncryptService::decrypt($v);
        return TextService::formatDate($decriptedValue);
    }

    private function buildCodes($id, $isExcel){
        $codes = EModelService::findCodes($id);
        if ($isExcel){
            $out = "";
            foreach($codes as $code){
                $out .= $out == "" ? "" : "\n";
                $out .= $code["code"] . ": " . $code['description'];
            }
            return $out;
        }

        $out = [];
        foreach($codes as $code){
            $out[] = [
                "code" => $code["code"],
                "description" => $code['description']
            ];
        }
        return $out;
    }
}