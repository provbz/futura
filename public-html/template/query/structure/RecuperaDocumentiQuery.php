<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PeiPdpQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class RecuperaDocumentiQuery extends QueryBase implements IQuery{

    private $structureId;
    private $classes;
    private $currentSchoolYear;
    private $plessoStructureIds;
    private $documentType;

    public function __construct(){
        $this->structureId = ArrayUtils::getIndex($_SESSION, GlobalSessionData::SELECTED_STRUCTURE_ID, 0);
        $this->documentType = ArrayUtils::getIndex($_SESSION, GlobalSessionData::DOCUMENT_TYPE, 0);
        $this->currentUser = UserService::getLoggedUser();
        $this->classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $this->currentSchoolYear = SchoolYearService::findCurrentYear();

        $this->plessoStructureIds = [];
        $plessi = StructureService::findAllWithParent($this->structureId);
        foreach($plessi as $plesso){
            $this->plessoStructureIds[] = [
                "value" => $plesso["structure_id"],
                "text" => $plesso["name"]
            ];
        }
    }

    public function descriptor() {
        if ($this->descriptor != null){
            return $this->descriptor;
        }

        $descriptor = [
            "version" => 4,
            "permissions" => [
                "/structure/PEIAction",
                "/structure/PDPAction",
                "/structure/eModel"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "user_year_document_id" => [
                    "columnName" => "user_year_document_id",
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
                "sy_school_year" => [
                    "columnName" => "sy.school_year",
                    "label" => "Anno scolastico",
                    "displayAsTableColumn" => true
                ],
                "uy_plesso_structure_id" => [
                    "columnName" => "uy.plesso_structure_id",
                    "label" => "Plesso",
                    "displayAsTableColumn" => true
                ],
                "classe" => [
                    "label" => "Classe",
                    "displayAsTableColumn" => true
                ],
                "sezione" => [
                    "label" => "Sezione",
                    "displayAsTableColumn" => true
                ],
                "code" => [
                    "label" => "Codice",
                    "displayAsTableColumn" => true
                ],
                "name" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true
                ],

            ],
            "conditions" => [
                "plesso_structure_id" => [
                    "label" => "Plesso",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $this->plessoStructureIds
                ],
                "classe" => [
                    "label" => "Classe",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->classes
                ],
                "code" => [
                    "label" => "Codice",
                    "displayAsTableFilter" => true
                ],
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableFilter" => true
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        $this->descriptor = $descriptor;
        return $this->descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        $referentFor = $this->getCondition("referent_for");
        $role = $this->getCondition("role");

        ob_start();

        $documentTypes = [$this->documentType];
        
        $documentIn = "";
        for($i = 0; $i < count($documentTypes); $i ++){
            $documentIn .= $documentIn == "" ? "" : ", ";
            $key = ":dt_{$i}";
            $documentIn .= $key;
            $this->params[$key] = $documentTypes[$i];
        }

        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM user_year_document uyd
            LEFT JOIN user_year uy ON uyd.user_id  = uy.user_id
            LEFT JOIN school_year sy ON sy.school_year_id = uyd.school_year_id
            LEFT JOIN user u ON u.user_id  = uy.user_id
            LEFT JOIN user_structure us ON u.user_id  = us.user_id
            WHERE us.structure_id=:structure_id
            AND uyd.school_year_id < :school_year_id
            AND uyd.status=:status_ready
            AND uyd.type IN (<?= $documentIn ?>)
        <?php 
    
        if (StringUtils::isBlank($this->documentType)){
            throw new Exception("Tipo documento non valido");
        }

        $this->params[":structure_id"] = $this->structureId;
        $this->params[":school_year_id"] = $this->currentSchoolYear["school_year_id"];
        $this->params[":status_ready"] = UserYearDocumentStatus::READY;
        
        echo $this->addConditionEqual("plesso_structure_id");
        echo $this->addConditionEqual("classe");
        echo $this->addConditionLike("code");
        echo $this->addConditionLike("name");
        echo $this->addConditionLike("surname");

        if ($type != QueryType::COUNT){
            echo $this->buildSort("plesso_structure_id, classe, sezione");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcelExport = false){
        for($i = 0; $i < count($results); $i++){
            $results[$i]['uy_plesso_structure_id'] = StructureService::printFullStructureName($results[$i]['uy_plesso_structure_id']);
            $results[$i]['classe'] = ArrayUtils::getIndex($this->classes, $results[$i]['classe']);
        }
        return $results;
    }
}