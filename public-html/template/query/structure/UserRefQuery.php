<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserRefQuery extends QueryBase implements IQuery{

    private $referentFor;
    private $roles;
    private $structureId;
    private $portfolioEnabled;

    public function __construct(){
        $this->structureId = ArrayUtils::getIndex($_SESSION, GlobalSessionData::SELECTED_STRUCTURE_ID, 0);
        $this->referentFor = DictionaryService::findGroupAsDictionary(Dictionary::STRUCTURE_REFERENT);
        $this->portfolioEnabled = PropertyService::find(PropertyNamespace::PORTFOLIO_INSEGNANTI, PropertyKey::ENABLED);

        $roles = UserRoleService::findAllWithLevel(UserRole::LEVEL_CLIENT);
        $this->roles = [];
        foreach($roles as $role){
            $this->roles[$role['role_id']] = $role['name'];
        }
    }

    public function descriptor() {
        $descriptor = [
            "version" => 1,
            "dataSource" => DataSource::FUTURA,
            "permissions" => [
                "/structure/UserRefAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "user_id" => [
                    "label" => "ID",
                    "alwaysSelect" => true,
                    "displayAsTableColumn" => true,
                ],
                "has_portfolio" => [
                    "displayAsTableColumn" => false,
                    "isComputedValue" => true
                ],
                "name" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "email" => [
                    "label" => "Email",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "roles" => [
                    "label" => "Ruoli",
                    "displayAsTableColumn" => true,
                    "isComputedValue" => true,
                    "enableSorting" => false
                ]
            ],
            "conditions" => [
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableFilter" => true
                ],
                "email" => [
                    "label" => "Email",
                    "displayAsTableFilter" => true
                ],
                "role_id" => [
                    "label" => "Ruolo",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->roles
                ],
                "referent_for" => [
                    "label" => "Referente per",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->referentFor,
                    "columnName" => "usm_referer.value"
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM user u
                WHERE user_id IN (
                    select us.user_id
                    FROM user_structure us
                    LEFT JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id
                    LEFT JOIN structure s ON s.parent_structure_id=:structure_id_2
                    left join user_structure uss ON uss.user_id=us.user_id AND uss.structure_id = s.structure_id
                    LEFT JOIN user_structure_metadata usm_referer ON uss.user_structure_id=usm_referer.user_structure_id AND usm_referer.metadata_id=:metadata_referer_for
                    WHERE us.structure_id=:structure_id AND usr.role_id != :role_student
                    <?= $this->addConditionEqual("role_id") ?>
                    <?= $this->addConditionEqual("referent_for") ?>
        <?php 
        
        $this->params["structure_id"] = $this->structureId;
        $this->params["structure_id_2"] = $this->structureId;
        $this->params["role_student"] = UserRole::STUDENTE;
        $this->params["metadata_referer_for"] = UserStructureMetadata::REFERENT_FOR;
        
/*
        
        if (StringUtils::isNotBlank($this->filter_referent_for_id)){
            $query .= "AND usm_referer.value=:referer_for ";
            $pars["referer_for"] = $this->filter_referent_for_id;
        }
        */
        //

        echo ")";
        echo $this->addConditionLike("email");
        echo $this->addConditionLike("name");
        echo $this->addConditionLike("surname");

        if ($type != QueryType::COUNT){
            echo $this->buildSort("surname");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results){
        for ($i = 0; $i < count($results); $i++) {
            $results[$i]["roles"] = $this->buildRoles($results[$i]["user_id"]);
            $results[$i]['has_portfolio'] = $this->hasPortfolio($results[$i]["user_id"]);
            $results[$i]['can_manage_users'] = $this->canLoggedUserManageUser($results[$i]["user_id"]);
        }

        return $results;
    }

    private function canLoggedUserManageUser($userId){
        if (UserRoleService::hasCurrentUser(UserRole::ADMIN) ||
            UserRoleService::hasCurrentUser(UserRole::AMMINISTRATIVI) || 
                UserRoleService::hasCurrentUser(UserRole::DIRIGENTE) || (
                        UserRoleService::hasCurrentUser(UserRole::REFERENTE_BES) && 
                        StructureService::hasUserStructureRole($userId, $this->structureId, UserRole::INSEGNANTE_PEI_PDP))){
            return true;
        }
        return false;
    }

    private function hasPortfolio($userId){
        if (!$this->portfolioEnabled["value"]){
            return false;
        }

        if (StructureService::hasUserStructureRole($userId, $this->structureId, UserRole::INSEGNANTE_ANNO_DI_PROVA)){
            if (UserRoleService::hasCurrentUser(UserRole::ADMIN) ||
                UserRoleService::hasCurrentUser(UserRole::AMMINISTRATIVI) || 
                UserRoleService::hasCurrentUser(UserRole::DIRIGENTE)){
                    return true;
            }
        }

        return false;
    }

    private function buildRoles($userId){
        $userStructureRoles = StructureService::findUserStructureRole($userId, $this->structureId);
        $out = [];
        foreach ($userStructureRoles as $userRole){
            if (isset($this->roles[$userRole["role_id"]])){
                $out[] = $this->roles[$userRole["role_id"]];
            }
        }
        return $out;
    }
}