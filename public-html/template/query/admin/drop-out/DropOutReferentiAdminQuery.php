<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DropOutReferentiAdminQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DropOutReferentiAdminQuery extends QueryBase implements IQuery{

    private $structureValues = [];
    private $classes = [];
    private $schoolYears = [];

    public function __construct (){
        $structures = StructureService::findAllRoot(true);
        $this->structureValues = [];
        foreach($structures as $structure){
            $this->structureValues[] = [
                "text" => $structure["name"],
                "value" => $structure["structure_id"]
            ];
        }
    }

    public function descriptor() {
        $this->classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        
        $schoolGrades = DictionaryService::findGroupAsDictionary(Dictionary::GRADE, false, "key", true);
        
        $currentSchoolYear = SchoolYearService::findCurrentYear();
        $years = SchoolYearService::findAll();
        $schoolYearList = [];
        foreach ($years as $year) {
            $this->schoolYears[$year["school_year_id"]] = $year["school_year"];

            $schoolYearList[] = [
                "value" => $year["school_year_id"],
                "text" => $year["school_year"]
            ];
        }

        $descriptor = [
            "version" => 1,
            "dataSource" => DataSource::FUTURA,
            "permissions" => [
                "/admin/dropOut/DropOutReferentiAdminAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "user_id" => [
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                    "enableSorting" => true,
                    "columnName" => "u.user_id"
                ],
                "structure_name" => [
                    "displayAsTableColumn" => true,
                    "columnName" => "s.name",
                    "label" => "Istituto"
                ],
                "email" => [
                    "label" => "e-Mail",
                    "displayAsTableColumn" => true
                ],
                "name" => [
                    "label" => "Nome",
                    "columnName" => "u.name",
                    "displayAsTableColumn" => true
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true
                ],
                "role" => [
                    "label" => "Ruolo",
                    "columnName" => "r.name",
                    "displayAsTableColumn" => true
                ]
            ],
            "conditions" => [
                "structure_id" => [
                    "label" => "Istituto",
                    "displayAsTableFilter" => true,
                    "columnName" => "s.structure_id",
                    "allowedValuesList" => $this->structureValues
                ],
                "name" => [
                    "label" => "Nome",
                    "columnName" => "u.name",
                    "displayAsTableFilter" => true
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableFilter" => true
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            from user u
            left join user_structure us ON u.user_id=us.user_id
            left join structure s ON us.structure_id=s.structure_id
            left join user_structure_role usr ON us.user_structure_id=usr.user_structure_id
            left join role r ON usr.role_id=r.role_id
            where usr.role_id IN (<?= UserRole::CORDINATORE_DROP_OUT ?>, <?= UserRole::CORDINATORE_PLESSO_DROP_OUT ?>)
            <?= $this->addConditionEqual("structure_id") ?>
            <?= $this->addConditionLike("name") ?>
            <?= $this->addConditionLike("surname") ?>
        <?php 

        if ($type != QueryType::COUNT){
            echo $this->buildSort("s.name ");
        }
        $query = ob_get_clean();
        
        return $query;
    }
}