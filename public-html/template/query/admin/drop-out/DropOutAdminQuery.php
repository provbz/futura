<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DropOutAdminQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DropOutAdminQuery extends QueryBase implements IQuery{

    private $classes;
    private $schoolYears = [];
    private $structureValues = [];

    private $dropOutEsito = [];
    private $dropOutStatoIscrizione = [];
    private $nonAmmessoMotivazioniDictionary = [];
    private $nonIscrtittoDictionaries = [];

    public function __construct (){
        $structures = StructureService::findAllRoot(true);
        $this->structureValues = [];
        foreach($structures as $structure){
            $this->structureValues[] = [
                "text" => $structure["name"],
                "value" => $structure["structure_id"]
            ];
        }

        $this->dropOutEsito = DictionaryService::findGroupAsDictionary(Dictionary::DROP_OUT_ESITO, false, "dictionary_id");
        $this->dropOutStatoIscrizione = DictionaryService::findGroupAsDictionary(Dictionary::DROP_OUT_STATO_ISCRIZIONE, false, "dictionary_id");
        $this->nonAmmessoMotivazioniDictionary = DictionaryService::findGroupAsDictionary(Dictionary::DROP_OUT_NON_AMMESSO_MOTIVAZIONI, false, "dictionary_id");
        $this->nonIscrtittoDictionaries = DictionaryService::findGroupAsDictionary(Dictionary::DROP_OUT_NON_ISCRITTO_MOTIVAZIONI, false, "dictionary_id");
        $this->classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
    }

    public function descriptor() {
        $schoolGrades = DictionaryService::findGroupAsDictionary(Dictionary::GRADE, false, "key", true);
        
        $currentSchoolYear = SchoolYearService::findCurrentYear();
        $years = SchoolYearService::findAll();
        $schoolYearList = [];
        foreach ($years as $year) {
            $this->schoolYears[$year["school_year_id"]] = $year["school_year"];

            $schoolYearList[] = [
                "value" => $year["school_year_id"],
                "text" => $year["school_year"]
            ];
        }

        $descriptor = [
            "version" => 7,
            "dataSource" => DataSource::FUTURA,
            "permissions" => [
                "/admin/dropOut/DropOutAdminAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "user_year_document_id" => [
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                    "enableSorting" => true,
                    "columnName" => "uyd.user_year_document_id"
                ],
                "school_year_id" => [
                    "alwaysSelect" => false,
                    "displayAsTableColumn" => true,
                    "label" => "Anno scolastico"
                ],
                "code" => [
                    "label" => "Codice",
                    "displayAsTableColumn" => true
                ],
                /*
                "name" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true,
                    "columnName" => "u.name"
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true
                ],
                */
                "drop_out_plesso_structure_id" => [
                    "alwaysSelect" => true,
                    "displayAsTableColumn" => false,
                ],
                "istituto" => [
                    "label" => "Istituto",
                    "displayAsTableColumn" => true,
                    "isComputedValue" => true
                ],
                "drop_out_classe" => [
                    "label" => "Classe",
                    "displayAsTableColumn" => true
                ],
                "drop_out_sezione" => [
                    "alwaysSelect" => true,
                    "displayAsTableColumn" => false
                ],
                "gender" => [
                    "label" => "Genere",
                    "displayAsTableColumn" => true,
                    "columnName" => "umg.value"
                ],
                "nation" => [
                    "label" => "Cittadinanza",
                    "displayAsTableColumn" => true,
                    "columnName" => "n.nome"
                ],
                "birth_date" => [
                    "label" => "Data di nascita",
                    "displayAsTableColumn" => true,
                    "columnName" => "umbd.value"
                ],
                "drop_out_age" => [
                    "label" => "Età",
                    "displayAsTableColumn" => true
                ],
                "drop_out_position" => [
                    "label" => "Posizione in merito obb. Scol.",
                    "displayAsTableColumn" => true,
                    "isComputedValue" => true
                ],
                "drop_out_status" => [
                    "label" => "Stato segnalazione",
                    "displayAsTableColumn" => true
                ],
                "drop_out_status" => [
                    "label" => "Stato segnalazione",
                    "displayAsTableColumn" => true
                ],
                "drop_out_segnalazione_procura" => [
                    "label" => "Segnalazione procura",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "drop_out_esito_dictionary_id" => [
                    "label" => "Esito",
                    "displayAsTableColumn" => true
                ],
                "drop_out_non_ammesso_motivazioni_dictionary_id" => [
                    "label" => "Non ammesso motiv.",
                    "displayAsTableColumn" => true
                ],
                "drop_out_non_iscritto_motivazioni_dictionary_id" => [
                    "label" => "Non iscritto motiv.",
                    "displayAsTableColumn" => true
                ],
                "drop_out_stato_iscrizione_dictionary_id" => [
                    "label" => "Stato iscrizione",
                    "displayAsTableColumn" => true
                ]
            ],
            "conditions" => [
                "school_year_id" => [
                    "label" => "Anno scolastico",
                    "displayAsTableFilter" => true,
                    "defaultValue" => [
                        "value" => $currentSchoolYear['school_year_id'],
                        "text" => $currentSchoolYear['school_year']
                    ],
                    "allowedValuesList" => $schoolYearList
                ],
                "structure_id" => [
                    "label" => "Istituto",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $this->structureValues,
                    "columnName" => "s.parent_structure_id"
                ],
                "grade" => [
                    "label" => "Grado",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $schoolGrades
                ],
                /*
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableFilter" => true
                ],
                */
                "code" => [
                    "label" => "Codice",
                    "displayAsTableFilter" => true
                ],
                "drop_out_segnalazione_procura" => [
                    "label" => "Segnalazione procura",
                    "displayAsTableFilter" => true,
                    "allowedValues" => [
                        0 => 'No',
                        1 => 'Sì'
                    ]
                ],
                "drop_out_esito_dictionary_id" => [
                    "label" => "Esito",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->dropOutEsito
                ],
                "drop_out_non_ammesso_motivazioni_dictionary_id" => [
                    "label" => "Non ammesso motiv.",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->nonAmmessoMotivazioniDictionary
                ],
                "drop_out_non_iscritto_motivazioni_dictionary_id" => [
                    "label" => "Non iscritto motiv.",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->nonIscrtittoDictionaries
                ],
                "drop_out_stato_iscrizione_dictionary_id" => [
                    "label" => "Stato iscrizione",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->dropOutStatoIscrizione
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            from user_year_document uyd
            LEFT JOIN structure s ON uyd.drop_out_plesso_structure_id=s.structure_id
            LEFT JOIN user_year uy ON uyd.user_id  = uy.user_id
            LEFT JOIN user u ON u.user_id  = uy.user_id
            LEFT JOIN user_structure us ON u.user_id  = us.user_id
            LEFT JOIN nazione n ON n.nazione_id  = uy.cittadinanza_1_nazione_id
            LEFT JOIN user_metadata umg ON umg.user_id=u.user_id AND umg.metadata_id='<?= UserMetadata::GENDER_ID ?>'
            LEFT JOIN user_metadata umbd ON umbd.user_id=u.user_id AND umbd.metadata_id='<?= UserMetadata::BIRTH_DATE ?>'
            WHERE uyd.status = :uyd_status_ready AND uyd.type = :type_drop_out
            <?= $this->addConditionEqual("school_year_id") ?>
            <?= $this->addConditionEqual("structure_id") ?>
            <?= $this->addConditionLike("name") ?>
            <?= $this->addConditionLike("surname") ?>
            <?= $this->addConditionLike("code") ?>
            <?= $this->addConditionEqual("drop_out_segnalazione_procura") ?>
            <?= $this->addConditionEqual("drop_out_esito_dictionary_id") ?>
            <?= $this->addConditionEqual("drop_out_non_ammesso_motivazioni_dictionary_id") ?>
            <?= $this->addConditionEqual("drop_out_non_iscritto_motivazioni_dictionary_id") ?>
            <?= $this->addConditionEqual("drop_out_stato_iscrizione_dictionary_id") ?>
        <?php 

        $grade = $this->getCondition("grade");
        if ($grade != null){
            echo "AND drop_out_classe LIKE :grade ";
            $this->params["grade"] = "%" . $grade;
        }

        $this->params['uyd_status_ready'] = RowStatus::READY;
        $this->params['type_drop_out'] = DocumentType::DROP_OUT;

        if ($type != QueryType::COUNT){
            echo $this->buildSort("uyd.user_year_document_id");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results){
        for ($i = 0; $i < count($results); $i++) {
            if($results[$i]["drop_out_plesso_structure_id"] != null){
                $results[$i]['istituto'] = StructureService::printFullStructureName($results[$i]["drop_out_plesso_structure_id"]);
            }

            if (isset($results[$i]['school_year_id'])){
                $results[$i]['school_year_id'] = $this->schoolYears[$results[$i]['school_year_id']];
            }

            if (isset($results[$i]['drop_out_classe'])){
                $results[$i]['drop_out_classe'] = ArrayUtils::getIndex($this->classes, $results[$i]['drop_out_classe']);
            }

            if (StringUtils::isNotBlank($results[$i]["drop_out_sezione"])){
                $results[$i]['drop_out_classe'] .= '/'. $results[$i]["drop_out_sezione"];
            }

            if (isset($results[$i]['gender'])){
                $results[$i]['gender'] = EncryptService::decrypt($results[$i]['gender']);
            }

            if (isset($results[$i]['birth_date'])){
                $results[$i]['birth_date'] = TextService::formatDate(EncryptService::decrypt($results[$i]['birth_date']));
            }

            if (isset($results[$i]['drop_out_age'])){
                $results[$i]['drop_out_position'] = $this->computePosition($results[$i]['drop_out_age']);
            }

            if (isset($results[$i]['drop_out_esito_dictionary_id'])){
                $results[$i]['drop_out_esito_dictionary_id'] = ArrayUtils::getIndex($this->dropOutEsito, $results[$i]['drop_out_esito_dictionary_id']);
            }

            if (isset($results[$i]['drop_out_non_ammesso_motivazioni_dictionary_id'])){
                $results[$i]['drop_out_non_ammesso_motivazioni_dictionary_id'] = ArrayUtils::getIndex($this->nonAmmessoMotivazioniDictionary, $results[$i]['drop_out_non_ammesso_motivazioni_dictionary_id']);
            }

            if (isset($results[$i]['drop_out_non_iscritto_motivazioni_dictionary_id'])){
                $results[$i]['drop_out_non_iscritto_motivazioni_dictionary_id'] = ArrayUtils::getIndex($this->nonIscrtittoDictionaries, $results[$i]['drop_out_non_iscritto_motivazioni_dictionary_id']);
            }


            if (isset($results[$i]['drop_out_stato_iscrizione_dictionary_id'])) {
                $results[$i]['drop_out_stato_iscrizione_dictionary_id'] = ArrayUtils::getIndex($this->dropOutStatoIscrizione, $results[$i]['drop_out_stato_iscrizione_dictionary_id']);
            }
        }

        return $results;
    }

    private function computePosition($age){
        if (StringUtils::isBlank($age)){
            return "";
        }

        $stato = "";
        if ($age < 16){
            $stato = "Obbligo scolastico";
        } else if ($age >= 18){
            $stato = "Assolto il diritto-dovere all’istruzione e formazione";
        } else {
            $stato = "Assolto obbligo scolastico";
        }
        return $stato;
    }
}