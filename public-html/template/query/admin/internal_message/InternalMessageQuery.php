<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DropOutAdminQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class InternalMessageQuery extends QueryBase implements IQuery{

    public function descriptor() {
        $descriptor = [
            "version" => 1,
            "dataSource" => DataSource::FUTURA,
            "permissions" => [
                "/admin/internal_message/InternalMessageAdminAcion"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "internal_message_id" => [
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                    "enableSorting" => true
                ],
                "insert_date" => [
                    "displayAsTableColumn" => true,
                    "label" => "Inserito il"
                ],
                "visible_from_date" => [
                    "displayAsTableColumn" => true,
                    "label" => "Visibile da"
                ],
                "visible_to_date" => [
                    "displayAsTableColumn" => true,
                    "label" => "Visibile a"
                ],
                "subject" => [
                    "displayAsTableColumn" => true,
                    "label" => "Oggetto"
                ]
            ],
            "conditions" => [
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        $visibleSections = InternalMessageService::getUserVisibleSections();
        $moduleInQuery = "";
        $i = 0;
        foreach ($visibleSections as $key => $value) {
            $moduleInQuery .= $moduleInQuery == '' ? '' : ', ';
            $moduleInQuery .= ':module_' . $i;
            $this->params[':module_' . $i] = $key;
            $i++;
        }

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            from internal_message 
            where internal_message_id IN(
                select distinct(internal_message_id)
                FROM internal_message_module
                WHERE module_base_action IN (<?= $moduleInQuery ?>)
            )
        <?php 

        if ($type != QueryType::COUNT){
            echo $this->buildSort("internal_message_id");
        }

        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results){
        return $results;
    }
}