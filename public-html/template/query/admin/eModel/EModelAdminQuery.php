<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EModelAdminQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EModelAdminQuery extends QueryBase implements IQuery{

    private $classes;
    private $schoolYearList = [];
    private $structureValues = [];
    private $diagnosyType = [];
    private $currentSchoolYear;

    public function __construct (){
        $structures = StructureService::findAllRoot(true);
        $this->structureValues = [];
        foreach($structures as $structure){
            $this->structureValues[] = [
                "text" => $structure["name"],
                "value" => $structure["structure_id"]
            ];
        }

        $this->classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        
        $years = SchoolYearService::findAll();
        $this->schoolYearList = [];
        foreach ($years as $year) {
            $this->schoolYearList[] = [
                "value" => $year["school_year_id"],
                "text" => $year["school_year"]
            ];
        }

        $this->currentSchoolYear = SchoolYearService::findCurrentYear();
        $this->diagnosyType = DictionaryService::findGroupAsDictionary(Dictionary::MODEL_E_DIAGNOSY_TYPE, false, "key", true);
    }

    public function descriptor() {
        $descriptor = [
            "version" => 6,
            "dataSource" => DataSource::FUTURA,
            "permissions" => [
                "/admin/eModel/EmodelAdminAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "e_model_id" => [
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                    "enableSorting" => true,
                    "columnName" => "em.e_model_id"
                ],
                "closed" => [
                    "label" => "Archiviato",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                ],
                "school_year" => [
                    "label" => "Anno scolastico",
                    "displayAsTableColumn" => true,
                    "columnName" => "sy.school_year"
                ],
                "istituto" => [
                    "label" => "Istituto",
                    "displayAsTableColumn" => true,
                    "columnName" => "istituto.name"
                ],
                "plesso" => [
                    "label" => "Plesso",
                    "displayAsTableColumn" => true,
                    "columnName" => "plesso.name"
                ],
                "class" => [
                    "label" => "Classe",
                    "displayAsTableColumn" => true
                ],
                "sezione" => [
                    "label" => "Sezione",
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true
                ],
                "codice" => [
                    "label" => "Codice",
                    "displayAsTableColumn" => true
                ],
                "nome" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true
                ],
                "cognome" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true
                ],
                "codici" => [
                    "label" => "Codici",
                    "displayAsTableColumn" => true,
                    "isComputedValue" => true
                ],
                "diagnosy_type_dictionary" => [
                    "label" => "Tipo diagnosi",
                    "displayAsTableColumn" => true
                ],
                "ore_richieste" => [
                    "label" => "Ore richieste",
                    "displayAsTableColumn" => true
                ],
                "ore_assegnate" => [
                    "label" => "Ore assegnate",
                    "displayAsTableColumn" => true
                ],
                "insert_date" => [
                    "label" => "Inserimento",
                    "displayAsTableColumn" => true,
                    "columnName" => "em.insert_date"
                ],
                "last_edit_date" => [
                    "label" => "Modifica",
                    "displayAsTableColumn" => true,
                    "columnName" => "em.last_edit_date"
                ],
                "last_edit_user_id" => [
                    "label" => "Utente modifica",
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true,
                    "columnName" => "em.last_edit_user_id"
                ]
            ],
            "conditions" => [
                "e_model_id" => [
                    "label" => "ID",
                    "displayAsTableFilter" => true,
                    "columnName" => "em.e_model_id"
                ],
                "school_year_id" => [
                    "label" => "Anno scolastico",
                    "displayAsTableFilter" => true,
                    "defaultValue" => [
                        "value" => $this->currentSchoolYear['school_year_id'],
                        "text" => $this->currentSchoolYear['school_year']
                    ],
                    "allowedValuesList" => $this->schoolYearList,
                    "columnName" => "em.school_year_id"
                ],
                "structure_id" => [
                    "label" => "Istituto",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $this->structureValues,
                    "columnName" => "em.structure_id"
                ],
                "class" => [
                    "label" => "Classe",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->classes
                ],
                "diagnosy_type_dictionary" => [
                    "label" => "Tipo diagnosi",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->diagnosyType
                ],
                "codice" => [
                    "label" => "Codice",
                    "displayAsTableFilter" => true
                ],
                "nome" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ],
                "cognome" => [
                    "label" => "Cognome",
                    "displayAsTableFilter" => true
                ],
                "insert_date_from" => [
                    "label" => "Data inserimento da",
                    "displayAsTableFilter" => true,
                    "columnName" => "em.insert_date",
                    "type" => "date"
                ],
                "insert_date_to" => [
                    "label" => "Data inserimento a",
                    "displayAsTableFilter" => true,
                    "columnName" => "em.insert_date",
                    "type" => "date"
                ],
                "last_edit_date_from" => [
                    "label" => "Data modifica da",
                    "displayAsTableFilter" => true,
                    "columnName" => "em.last_edit_date",
                    "type" => "date"
                ],
                "last_edit_date_to" => [
                    "label" => "Data modifica a",
                    "displayAsTableFilter" => true,
                    "columnName" => "em.last_edit_date",
                    "type" => "date"
                ],
                "ha_richiesta_ore" => [
                    "label" => "Richiesta ore",
                    "displayAsTableFilter" => true,
                    "allowedValues" => [
                        1 => "Sì",
                        0 => "No"
                    ]
                ],
                "ore_richieste" => [
                    "label" => "Numero ore richieste",
                    "displayAsTableFilter" => true,
                ],
                "ha_ore_assegnate" => [
                    "label" => "Ore assegnate",
                    "displayAsTableFilter" => true,
                    "allowedValues" => [
                        1 => "Sì",
                        0 => "No"
                    ]
                ],
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            $descriptor["excelFields"][$key] = $field;
        }

        $descriptor["excelFields"]["grave_compromissione_sociale"] = [
            "label" => "Grave compromissione",
            "displayAsTableColumn" => true,
        ];
        $descriptor["excelFields"]["data_emissione"] = [
            "label" => "Data emissione",
            "displayAsTableColumn" => true,
        ];
        $descriptor["excelFields"]["data_di_accertamento"] = [
            "label" => "Data richiesta accertamento",
            "displayAsTableColumn" => true,
        ];
        $descriptor["excelFields"]["disabilita_visiva"] = [
            "label" => "Disabilità visiva",
            "displayAsTableColumn" => true,
        ];
        $descriptor["excelFields"]["disabilita_uditiva"] = [
            "label" => "Disabilità uditiva",
            "displayAsTableColumn" => true,
        ];
        $descriptor["excelFields"]["disabilita_motoria"] = [
            "label" => "Disabilità motoria",
            "displayAsTableColumn" => true,
        ];
        $descriptor["excelFields"]["diagnosi"] = [
            "label" => "Diagnosi",
            "displayAsTableColumn" => true,
        ];
        $descriptor["excelFields"]["note_amministratore"] = [
            "label" => "Note amministratore",
            "displayAsTableColumn" => true,
        ];

        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ ?>
            <?= $this->buildSelect() ?>, ue.name as ue_name, ue.surname as ue_surname
         <?php } ?>
            from e_model em
            LEFT JOIN school_year sy ON sy.school_year_id = em.school_year_id
            LEFT JOIN user ue on em.last_edit_user_id = ue.user_id
            LEFT JOIN structure as istituto ON em.structure_id = istituto.structure_id
            LEFT JOIN structure as plesso ON em.plesso_structure_id = plesso.structure_id

            WHERE 1 = 1

            <?= $this->addConditionEqual("e_model_id") ?>
            <?= $this->addConditionEqual("school_year_id") ?>
            <?= $this->addConditionEqual("structure_id") ?>
            <?= $this->addConditionEqual("class") ?>
            <?= $this->addConditionEqual("diagnosy_type_dictionary") ?>
            <?= $this->addConditionLike("nome") ?>
            <?= $this->addConditionLike("cognome") ?>
            <?= $this->addConditionLike("codice") ?>
            <?= $this->addConditionDate("insert_date") ?>
            <?= $this->addConditionDate("last_edit_date") ?>
            <?= $this->addConditionEqual("ore_richieste") ?>
            <?= $this->addConditionEqual("edited") ?>

            <?php 
            $richiedeOre = $this->getCondition("ha_richiesta_ore");
            if ($richiedeOre === "0"){ ?>
                AND (ore_richieste IS NULL OR ore_richieste = 0)
            <?php } else if ($richiedeOre === "1"){ ?>
                AND ore_richieste > 0
            <?php } ?>

            <?php 
            $richiedeOre = $this->getCondition("ha_ore_assegnate");
            if ($richiedeOre === "0"){ ?>
                AND (ore_assegnate IS NULL OR ore_assegnate = 0)
            <?php } else if ($richiedeOre === "1"){ ?>
                AND ore_assegnate > 0
            <?php } ?>
        <?php 
        
        if ($type != QueryType::COUNT){
            echo $this->buildSort("em.e_model_id desc");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcel = false){
        for ($i = 0; $i < count($results); $i++) {
            $results[$i]['class'] = $this->classes[$results[$i]['class']];
            $results[$i]["codici"] = $this->buildCodes($results[$i]["e_model_id"], $isExcel);
            $results[$i]["diagnosy_type_dictionary"] = $this->diagnosyType[$results[$i]["diagnosy_type_dictionary"]];

            if (isset($results[$i]["last_edit_user_id"])){
                $results[$i]["last_edit_user_id"] = $results[$i]["ue_name"] . ' ' . $results[$i]["ue_surname"];
            }
        }

        return $results;
    }

    private function findUser($userId){
        $user = UserService::find($userId);
        if ($user == null){
            return "";
        }

        return $user['name']. ' ' . $user['surname'];
    }

    private function buildCodes($id, $isExcel){
        $codes = EModelService::findCodes($id);
        if ($isExcel){
            $out = "";
            foreach($codes as $code){
                $out .= $out == "" ? "" : "\n";
                $out .= $code["code"] . ": " . $code['description'];
            }
            return $out;
        }

        $out = [];
        foreach($codes as $code){
            $out[] = [
                "code" => $code["code"],
                "description" => $code['description']
            ];
        }
        return $out;
    }
}