<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EModelJournalQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EModelJournalQuery extends QueryBase implements IQuery{

    public function __construct (){
        
    }

    public function descriptor() {
        $descriptor = [
            "version" => 2,
            "dataSource" => DataSource::FUTURA,
            "permissions" => [
                "/admin/eModel/EmodelAdminAction-history"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "journal_id" => [
                    "label" => "ID",
                    "alwaysSelect" => true,
                    "columnName" => "j.journal_id"
                ],
                "insert_date" => [
                    "label" => "Data",
                    "alwaysSelect" => true,
                    "columnName" => "j.insert_date"
                ],
                "user_id" => [
                    "label" => "Utente",
                    "alwaysSelect" => true,
                    "columnName" => "j.user_id"
                ],
                "action" => [
                    "label" => "Azione",
                    "alwaysSelect" => true
                ],
                "message" => [
                    "label" => "Messaggio",
                    "alwaysSelect" => true
                ],
            ],
            "conditions" => [
                "entity_id" => [
                    "label" => "ID",
                    "displayAsTableFilter" => false
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            $descriptor["excelFields"][$key] = $field;
        }

        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ ?>
            <?= $this->buildSelect() ?>, u.name, u.surname, u.email
         <?php } ?>
            FROM journal j
            LEFT JOIN user u ON j.user_id = u.user_id
            
            WHERE entity = :entity_name

            <?= $this->addConditionEqual("entity_id") ?>

        <?php 
        $this->params["entity_name"] = EntityName::E_MODEL;

        if ($type != QueryType::COUNT){
            echo $this->buildSort("j.insert_date desc");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcel = false){
        for ($i = 0; $i < count($results); $i++) {
            $results[$i]["changes"] = $this->findChanges($results[$i]);
        }

        return $results;
    }

    private function findChanges($row){
        $rows = EM::execQuery("SELECT * 
            FROM journal_row
            WHERE journal_id=:id", [
                "id" => $row["journal_id"]
            ]);

        $changes = [];
        foreach ($rows as $row) {
            $change = [
                "field_name" => $row["field_name"],
                "old_value" => $this->prepareValue($row["old_value"]),
                "new_value" => $this->prepareValue($row["new_value"])
            ];

            $changes[] = $change;
        }

        return $changes;
    }

    private function prepareValue($value){
        if ($value == null){
            return null;
        }

        if (strpos($value, "{") === 0){
            return json_decode($value);
        }

        return $value;
    }
}