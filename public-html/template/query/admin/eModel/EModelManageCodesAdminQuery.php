<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EModelAdminQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EModelManageCodesAdminQuery extends QueryBase implements IQuery{

    private $classes;
    private $schoolYearList = [];
    private $structureValues = [];
    private $diagnosyType = [];
    private $currentSchoolYear;

    public function __construct (){
        $structures = StructureService::findAllRoot(true);
        $this->structureValues = [];
        foreach($structures as $structure){
            $this->structureValues[] = [
                "text" => $structure["name"],
                "value" => $structure["structure_id"]
            ];
        }

        $this->classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        
        $years = SchoolYearService::findAll();
        $this->schoolYearList = [];
        foreach ($years as $year) {
            $this->schoolYearList[] = [
                "value" => $year["school_year_id"],
                "text" => $year["school_year"]
            ];
        }

        $this->currentSchoolYear = SchoolYearService::findCurrentYear();
        $this->diagnosyType = DictionaryService::findGroupAsDictionary(Dictionary::MODEL_E_DIAGNOSY_TYPE, false, "key", true);
    }

    public function descriptor() {
        $descriptor = [
            "version" => 1,
            "dataSource" => DataSource::FUTURA,
            "permissions" => [
                "/admin/eModel/EmodelManageCodesAdminAction",
                "/structure/eModel/EmodelAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "dictionary_id" => [
                    "label" => "ID",
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true
                ],
                "key" => [
                    "label" => "Codice",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                ],
                "value" => [
                    "label" => "Descrizione",
                    "displayAsTableColumn" => true
                ]
            ],
            "conditions" => [
                "key" => [
                    "label" => "Codice",
                    "displayAsTableFilter" => true,
                    "alwaysSelect" => true,
                ],
                "value" => [
                    "label" => "Descrizione",
                    "displayAsTableFilter" => true,
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            $descriptor["excelFields"][$key] = $field;
        }
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ ?>
            <?= $this->buildSelect() ?>
         <?php } ?>
            FROM dictionary d
            WHERE `group` = '<?= Dictionary::e_model_code ?>'

            <?= $this->addConditionLike("key") ?>
            <?= $this->addConditionLike("value") ?>
        <?php 
        
        if ($type != QueryType::COUNT){
            echo $this->buildSort("`key` desc");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcel = false){
        return $results;
    }
}