<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserQuery extends QueryBase implements IQuery{

    private $roleList;

    public function __construct(){
        $result = UserRoleService::findAllWithLevel(UserRole::LEVEL_GLOBAL);
        $this->roleList = [];
        foreach($result as $row){
            $this->roleList[] = [
                "text" => $row['name'],
                "value" => $row["role_id"]
            ];
        }
    }

    public function descriptor() {
        $descriptor = [
            "version" => 2,
            "permissions" => [
                "/admin/user/UserAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "user_id" => [
                    "label"=> "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
                "email" => [
                    "label" => "e-mail",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "name" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "u.name"
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "role" => [
                    "label" => "Ruolo",
                    "displayAsTableColumn" => true,
                    "isComputedValue" => true
                ]
            ],
            "conditions" => [
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true,
                    "columnName" => "u.name"
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableFilter" => true
                ],
                "email" => [
                    "label" => "E-mail",
                    "displayAsTableFilter" => true
                ],
                "role" => [
                    "label" => "Ruolo",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $this->roleList
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        $role = $this->getCondition("role");

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM user u
            WHERE u.email IS NOT NULL
            AND u.status<>:user_status_deleted
            <?php if ($role != null){ ?>
                AND user_id IN (
                    select user_id
                    from user_role
                    WHERE role_id =:role_id
                )    
            <?php 
                $this->params["role_id"] = $role;
            } ?>
        <?php 
        $this->params["user_status_deleted"] = UserStatus::DELETED;

        echo $this->addConditionLike("name");
        echo $this->addConditionLike("email");
        echo $this->addConditionLike("surname");

        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcelExport = false){
        $newLine = $isExcelExport ? "\n" : "<br/>";

        $updatedResults = [];
        foreach($results as $row){
            $row['role'] = $this->getRoles($row['user_id']);

            $updatedResults[] = $row;
        }

        return $updatedResults;
    }

    private function getRoles($userId){
        $results = EM::execQuery("SELECT r.name as name
                        from role r
                        left join user_role ur ON ur.role_id=r.role_id
                        WHERE ur.user_id=:user_id
                        ORDER BY r.name", ["user_id" => $userId]);
        $roles = [];
        foreach ($results as $row) {
            $roles[] = $row["name"];
        }
        return $roles;
    }
}