<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserIcQuery extends QueryBase implements IQuery{

    public function descriptor() {
        $structures = StructureService::findAllRootAsDictionary(true);

        $result = UserRoleService::findAllWithLevel(UserRole::LEVEL_CLIENT);
        $roles = [
            [
                "text" => "",
                "value" => "",
            ]
        ];
        foreach($result as $row){
            $roles[] = [
                "text" => $row['name'],
                "value" => $row["role_id"]
            ];
        }
        asort($roles);

        $referentFor = array_merge(["" => ""], DictionaryService::findGroupAsDictionary(Dictionary::STRUCTURE_REFERENT));

        $descriptor = [
            "permissions" => [
                "/admin/user/UserICAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "structure_id" => [
                    "columnName" => "s.structure_id",
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true
                ],
                "user_structure_id" => [
                    "columnName" => "us.user_structure_id",
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true
                ],
                "user_id" => [
                    "columnName" => "u.user_id",
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true
                ],
                "s_name" => [
                    "label" => "Istituto",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "s.name"
                ],
                "roles" => [
                    "label" => "Ruoli",
                    "displayAsTableColumn" => true,
                    "sortable" => false,
                    "isComputedValue" => true
                ],
                "referent_for" => [
                    "label" => "Referente per",
                    "displayAsTableColumn" => true,
                    "sortable" => false,
                    "isComputedValue" => true
                ],
                "email" => [
                    "label" => "e-mail",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "name" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "u.name"
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "phone_number" => [
                    "label" => "Telefono",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
            ],
            "conditions" => [
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ],
                "email" => [
                    "label" => "E-mail",
                    "displayAsTableFilter" => true
                ],
                "s_name" => [
                    "label" => "Istituto",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $structures,
                ],
                "role" => [
                    "label" => "Ruolo",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $roles
                ],
                "referent_for" => [
                    "label" => "Referente per",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $referentFor
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        $referentFor = $this->getCondition("referent_for");
        $role = $this->getCondition("role");

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM user u
            LEFT JOIN user_structure us ON u.user_id=us.user_id
            LEFT JOIN structure s ON us.structure_id=s.structure_id

        <?php if (StringUtils::isNotBlank($role)){ ?>
            LEFT JOIN user_structure_role usr ON us.user_structure_id=usr.user_structure_id
        <?php } ?>

        <?php if (StringUtils::isNotBlank($referentFor)) { ?>
            LEFT JOIN user_structure us_sub ON us_sub.user_id=u.user_id AND us_sub.parent_structure_id=us.structure_id
            LEFT JOIN user_structure_metadata usm ON us_sub.user_structure_id=usm.user_structure_id AND usm.metadata_id=:metadata_referer_for
        <?php 
            $this->params[":metadata_referer_for"] = UserStructureMetadata::REFERENT_FOR;
        } ?> 

            WHERE u.email IS NOT NULL AND s.parent_structure_id IS NULL
        <?php 

        $name = $this->getCondition("name");
        if (StringUtils::isNotBlank($name)){
            echo " AND (u.name LIKE :name OR u.surname LIKE :surname) ";
            $this->params["name"] = "%" . $name . "%";
            $this->params["surname"] = "%" . $name . "%";
        }

        echo $this->addConditionLike("email");

        $sName = $this->getCondition("s_name");
        if (StringUtils::isNotBlank($sName)){
            echo " AND (s.structure_id = :structure_id) ";
            $this->params[":structure_id"] = $sName;
        }

        if (StringUtils::isNotBlank($role)){
            echo " AND (usr.role_id = :role_id) ";
            $this->params[":role_id"] = $role;
        }

        if (StringUtils::isNotBlank($referentFor)){
            echo " AND (usm.value = :referent) ";
            $this->params[":referent"] = $referentFor;
        }


        if ($type != QueryType::COUNT){
            echo $this->buildSort("s.name, u.surname");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcelExport = false){
        $newLine = $isExcelExport ? "\n" : "<br/>";

            $updatedResults = [];
        foreach($results as $row){
            if (isset($row["user_structure_id"])){
                $structureRoles = EM::execQuery("SELECT r.name as name
                    FROM user_structure_role usr
                    LEFT JOIN role r ON usr.role_id=r.role_id
                    WHERE user_structure_id=:user_structure_id", [
                        "user_structure_id" => $row["user_structure_id"]
                    ]);
                
                $row["roles"] = "";
                foreach ($structureRoles as $role) {
                    $row["roles"] .= $row["roles"] == "" ? "" : $newLine;
                    $row["roles"] .= $role["name"];
                }
            }
            
            if (isset($row["structure_id"])){
                $metas = EM::execQuery("SELECT d.value as value, us.structure_id as structure_id, s.name s_name
                    FROM user_structure us
                    LEFT JOIN user_structure_metadata usm ON usm.user_structure_id = us.user_structure_id
                    LEFT JOIN structure s ON us.structure_id = s.structure_id
                    LEFT JOIN dictionary d ON usm.value = d.key
                    WHERE us.parent_structure_id=:parent_structure_id
                            AND us.user_id=:user_id
                            AND usm.metadata_id=:metadata_id 
                        ", [
                        "metadata_id" => UserStructureMetadata::REFERENT_FOR,
                        "user_id" => $row["user_id"],
                        "parent_structure_id" => $row["structure_id"],
                    ]);

                $row["referent_for"] = "";
                foreach ($metas as $meta) {
                    $row["referent_for"] .= $row["referent_for"] == "" ? "" : $newLine;
                    if ($isExcelExport){
                        $row["referent_for"] .= $meta["s_name"] . " - ";
                    } else {
                        $row["referent_for"] .= "<small>". $meta["s_name"] . "</small> - ";
                    }
                    $row["referent_for"] .= $meta["value"];
                }
            }

            $updatedResults[] = $row;
        }

        return $updatedResults;
    }
}