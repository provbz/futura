<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserTransferRequestQuery extends QueryBase implements IQuery{

    private $structureValues = [];

    public function __construct (){
        $structures = StructureService::findAllRoot(true);
        $this->structureValues = [];
        foreach($structures as $structure){
            $this->structureValues[] = [
                "text" => $structure["name"],
                "value" => $structure["structure_id"]
            ];
        }
    }

    public function descriptor() {
        $descriptor = [
            "version" => 2,
            "permissions" => [
                "/admin/user/UserTransferRequestAdminAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "user_transfer_request_id" => [
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                    "enableSorting" => true
                ],
                "insert_date" => [
                    "label" => "Data inserimento",
                    "columnName" => "utr.insert_date",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "from_structure_name" => [
                    "label" => "Da istituto",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "sf.name"
                ],
                "to_structure_name" => [
                    "label" => "A istituto",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "st.name"
                ],
                "type" => [
                    "label" => "Tipo",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "utr.type",
                    "entity" => [
                        "type" => "dictionary",
                        "group" => Dictionary::USER_TRANSFER_TYPE
                    ]
                ],
                "status" => [
                    "label" => "Stato",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "utr.status",
                    "entity" => [
                        "type" => "dictionary",
                        "group" => Dictionary::USER_TRANSFER_STATUS
                    ]
                ],
                "sy_school_year" => [
                    "label" => "Anno scolastico",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "sy.school_year"
                ],
                "user_code" => [
                    "label" => "Codice",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "u.code"
                ],
                "user_name" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "u.name"
                ],
                "user_surname" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "u.surname"
                ],
                "user_req_email" => [
                    "label" => "Inserito da",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "ur.email"
                ],
            ],
            "conditions" => [
                "user_transfer_request_id" => [
                    "label" => "ID",
                    "displayAsTableFilter" => true
                ],
                "from_structure_id" => [
                    "label" => "Da istituto",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->structureValues,
                    "columnName" => "from_structure_id"
                ],
                "to_structure_id" => [
                    "label" => "A istituto",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $this->structureValues
                ],
                "u_code" => [
                    "label" => "Codice studente",
                    "displayAsTableFilter" => true,
                    "columnName" => "u.code"
                ],
                "u_name" => [
                    "label" => "Nome studente",
                    "displayAsTableFilter" => true,
                    "columnName" => "u.name"
                ],
                "u_surname" => [
                    "label" => "Cognome studente",
                    "displayAsTableFilter" => true,
                    "columnName" => "u.surname"
                ],
                "ur_email" => [
                    "label" => "Inserito da email",
                    "displayAsTableFilter" => true,
                    "columnName" => "ur.email"
                ],
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            from user_transfer_request utr
            left join structure sf ON utr.from_structure_id=sf.structure_id
            left join structure st ON utr.to_structure_id=st.structure_id
            left join user u ON utr.user_id=u.user_id
            left join user ur ON utr.requester_user_id=ur.user_id
            left join school_year sy ON utr.school_year_id=sy.school_year_id
            where 1=1
        <?php 

        echo $this->addConditionEqual("user_transfer_request_id");
        echo $this->addConditionEqual("from_structure_id");
        echo $this->addConditionEqual("to_structure_id");
        echo $this->addConditionLike("u_code");
        echo $this->addConditionLike("u_name");
        echo $this->addConditionLike("u_surname");
        echo $this->addConditionLike("ur_email");

        if ($type != QueryType::COUNT){
            echo $this->buildSort("utr.insert_date DESC");
        }
        $query = ob_get_clean();
        
        return $query;
    }

}