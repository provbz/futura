<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UserAccessQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UserAccessQuery extends QueryBase implements IQuery{

    private $roleList;

    public function __construct(){
        $result = UserRoleService::findAllWithLevel(UserRole::LEVEL_GLOBAL);
        $this->roleList = [];
        foreach($result as $row){
            $this->roleList[] = [
                "text" => $row['name'],
                "value" => $row["role_id"]
            ];
        }
    }

    public function descriptor() {
        $descriptor = [
            "version" => 1,
            "permissions" => [
                "/admin/user/UserAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "date" => [
                    "label"=> "Data",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
                "date_last_check" => [
                    "label"=> "Unltima presenza",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
                "ip" => [
                    "label"=> "IP",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
                "browser" => [
                    "label"=> "Browser",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
            ],
            "conditions" => [
                "user_id" => [
                    "label" => "Utente",
                    "displayAsTableFilter" => false
                ],
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        $role = $this->getCondition("role");

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM user_access ua
            WHERE 1 = 1
        <?php 

        echo $this->addConditionEqual("user_id");

        if ($type != QueryType::COUNT){
            echo $this->buildSort("date DESC");
        }

        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcelExport = false){
        $updatedResults = [];
        foreach($results as $row){
            $updatedResults[] = $row;
        }

        return $updatedResults;
    }
}