<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class RoleQuery extends QueryBase implements IQuery{

    public function descriptor() {
        $levels = DictionaryService::findGroupAsDictionary(Dictionary::ROLE_LEVEL);

        $descriptor = [
            "permissions" => [
                "/admin/user/RoleAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "role_id" => [
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                    "enableSorting" => true
                ],
                "level" => [
                    "label" => "Livello",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "name" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "home_action" => [
                    "label" => "Home page",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ]
            ],
            "conditions" => [
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ],
                "level" => [
                    "label" => "level",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $levels
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM role u
            WHERE 1 = 1
        <?php 

        echo $this->addConditionEqual("level");
        echo $this->addConditionLike("name");

        if ($type != QueryType::COUNT){
            echo $this->buildSort("level, name");
        }
        $query = ob_get_clean();
        
        return $query;
    }

}