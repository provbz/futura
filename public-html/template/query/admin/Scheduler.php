<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class Scheduler extends QueryBase implements IQuery{

    public function descriptor() {
        $descriptor = [
            "permissions" => [
                "/admin/technical/SchedulerAdminAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "scheduler_id" => [
                    "label" => "ID",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "name" => [
                    "label" => "Nome",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "trigger" => [
                    "label" => "Trigger",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "cron" => [
                    "label" => "Cron",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "status" => [
                    "label" => "Stato",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "last_run_date" => [
                    "label" => "Ultimo avvio",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ]
            ],
            "conditions" => [
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        
        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM scheduler s
            WHERE 1=1 

            <?= $this->addConditionLike("name"); ?>
        <?php 
        if ($type != QueryType::COUNT){
            echo $this->buildSort("name");
        }
        $query = ob_get_clean();
        
        return $query;
    }
}