<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class StudentQuery extends QueryBase implements IQuery{

    private $schoolYears;   

    public function descriptor() {
        $structures = StructureService::findAllRootAsDictionary(true);

        $currentSchoolYear = SchoolYearService::findCurrentYear();
        $years = SchoolYearService::findAll();
        $schoolYearList = [];
        foreach ($years as $year) {
            $this->schoolYears[$year["school_year_id"]] = $year["school_year"];

            $schoolYearList[] = [
                "value" => $year["school_year_id"],
                "text" => $year["school_year"]
            ];
        }

        $descriptor = [
            "version" => 2,
            "permissions" => [
                "/admin/test/ElencoStudentiAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => false,
            "fields" => [
                "user_id" => [
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                    "columnName" => "u.user_id",
                    "label" => "ID"
                ],
                "school_year_id" => [
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true,
                    "columnName" => "sy.school_year_id"
                ],
                "school_year" => [
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                    "label" => "Anno scolastico"
                ],
                "istituto" => [
                    "displayAsTableColumn" => true,
                    "columnName" => "s.name",
                    "label" => "Istituto"
                ],
                "s_structure_id" => [
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true,
                    "columnName" => "s.structure_id",
                ],
                "plesso" => [
                    "displayAsTableColumn" => true,
                    "columnName" => "sp.name",
                    "label" => "Plesso"
                ],
                "classe" => [
                    "displayAsTableColumn" => true,
                    "columnName" =>  "sc.class_dictionary_value",
                    "label" => "Classe"
                ],
                "sezione" => [
                    "displayAsTableColumn" => true,
                    "columnName" =>  "sc.section",
                    "label" => "Sezione"
                ],
                "status" => [
                    "displayAsTableColumn" => true,
                    "columnName" => "scu.row_status",
                    "label" => "Stato"
                ],
                "name" => [
                    "displayAsTableColumn" => true,
                    "columnName" =>  "u.name",
                    "label" => "Nome",
                    "enableSorting" => true
                ],
                "surname" => [
                    "displayAsTableColumn" => true,
                    "columnName" =>  "u.surname",
                    "label" => "Cognome",
                    "enableSorting" => true
                ],
                "birth_date" => [
                    "displayAsTableColumn" => true,
                    "columnName" => "u.birth_date",
                    "label" => "Data nascita"
                ],
            ],
            "conditions" => [
                "school_year_id" => [
                    "label" => "Anno scolastico",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $schoolYearList
                ],
                "s_structure_id" => [
                    "label" => "Istituto",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $structures,
                ],
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableFilter" => true
                ],
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        
        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            from user u
            left join structure_class_user scu ON scu.user_id=u.user_id
            left join structure_class sc ON scu.structure_class_id=sc.structure_class_id
            left join structure sp ON sc.structure_id=sp.structure_id
            left join structure s ON sp.parent_structure_id = s.structure_id
            left join school_year sy ON sy.school_year_id=sc.school_year_id
            WHERE email IS NULL AND code is null
            <?= $this->addConditionLike("name"); ?>
            <?= $this->addConditionLike("surname"); ?>
            <?= $this->addConditionEqual("school_year_id") ?>
            <?= $this->addConditionEqual("s_structure_id"); ?>
        <?php 

        if ($type != QueryType::COUNT){
            echo $this->buildSort("s.name, sp.name, sc.class_dictionary_value, sc.section, sy.school_year, u.surname, u.name");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results){
        
        return $results;
    }
}