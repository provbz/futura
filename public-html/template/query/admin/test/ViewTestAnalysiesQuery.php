<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ViewTestAnalysiesQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class ViewTestAnalysiesQuery extends QueryBase implements IQuery{

    public function descriptor() {
        $structures = StructureService::findAllRoot(true);
        $structureValues = [];
        foreach($structures as $structure){
            $structureValues[] = [
                "text" => $structure["name"],
                "value" => $structure["structure_id"]
            ];
        }

        $schoolYears = SchoolYearService::findAllAsObject();
        $schoolYearList = [];
        foreach($schoolYears as $id => $label){
            $schoolYearList[] = [
                "value" => $id,
                "text" => $label
            ];
        }

        $classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);

        $descriptor = [
            "versione" => 3,
            "permissions" => [
                "/admin/test/ViewTestCommentAdminAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "school_year" => [
                    "label" => "Anno",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "sy.school_year"
                ],
                "s_istituto_name" => [
                    "label" => "Istituto",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "s_istituto.name"
                ],
                "s_plesso_name" => [
                    "label" => "Plesso",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "s_plesso.name"
                ],
                "dc_value" => [
                    "label" => "Classe",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "dc.value"
                ],
                "section" => [
                    "label" => "Sezione",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "sc.section"
                ],
                "t_name" => [
                    "label" => "Test",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "t.name"
                ],
                "strc_insert_date" => [
                    "label" => "Data report",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "strc.insert_date"
                ],
                "strcv_insert_date" => [
                    "label" => "Data visualizzazione",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "strcv.insert_date"
                ],
                "u_name" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "u.name"
                ],
                "u_surname" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "u.surname"
                ],
            ],
            "conditions" => [
                "school_year_id" => [
                    "label" => "Anno",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $schoolYearList,
                    "columnName" => "sy.school_year_id"
                ],
                "s_istituto_name_id" => [
                    "label" => "Istituto",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $structureValues,
                    "columnName" => "s_istituto.structure_id"
                ],
                "dc_value_id" => [
                    "label" => "Classe",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $classes,
                    "columnName" => "dc.key"
                ],
                "strcv_insert_date_from" => [
                    "label" => "Da data visualizzazione",
                    "displayAsTableFilter" => true,
                    "type" => "date",
                    "columnName" => "strcv.insert_date"
                ],
                "strcv_insert_date_to" => [
                    "label" => "A data visualizzazione",
                    "displayAsTableFilter" => true,
                    "type" => "date",
                    "columnName" => "strcv.insert_date"
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        
        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            
            FROM somministrazione_test_result_class_view strcv
                left join user u ON strcv.user_id = u.user_id
                left join somministrazione_test_result_class strc ON strc.somministrazione_test_result_class_id = strcv.somministrazione_test_result_class_id
                left join structure_class sc ON sc.structure_class_id = strc.structure_class_id
                left join dictionary dc ON dc.key = sc.class_dictionary_value
                left join structure s_plesso ON s_plesso.structure_id = sc.structure_id
                left join structure s_istituto ON s_istituto.structure_id = s_plesso.parent_structure_id
                left join somministrazione_test st ON strc.somministrazione_test_id = st.somministrazione_test_id
                left join test t ON st.test_id = t.test_id
                left join somministrazione s ON st.somministrazione_id = s.somministrazione_id
                left join school_year sy ON sy.school_year_id = s.school_year_id
            
            WHERE 1 = 1

            <?= $this->addConditionEqual("school_year_id") ?>
            <?= $this->addConditionEqual("s_istituto_name_id") ?>
            <?= $this->addConditionEqual("dc_value_id") ?>
            <?= $this->addConditionDate("strc_insert_date") ?>
        <?php 

        if ($type != QueryType::COUNT){
            echo $this->buildSort("strc.insert_date desc");
        }
        $query = ob_get_clean();
        
        return $query;
    }
}