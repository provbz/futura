<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class TestSogliaQuery extends QueryBase implements IQuery{

    public function descriptor() {
        $descriptor = [
            "permissions" => [
                "/admin/test/TestAdminAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "test_soglia_id" => [
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true
                ],
                "variable_name" => [
                    "label" => "Variabile",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "s1" => [
                    "label" => "Soglia 1",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "s2" => [
                    "label" => "Soglia 2",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "in_use" => [
                    "label" => "In uso",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "insert_date" => [
                    "label" => "Data inserimento",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ]
            ],
            "conditions" => [
                "test_id" => [
                    "displayAsTableFilter" => false
                ],
                "in_use" => [
                    "label" => "In uso",
                    "displayAsTableFilter" => true
                ],
                "variable_name" => [
                    "label" => "Variabile",
                    "displayAsTableFilter" => true
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        
        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM test_soglia ts
            WHERE row_status=:row_status_ready
            <?= $this->addConditionLike("variable_name"); ?>
            <?= $this->addConditionEqual("in_use"); ?>
            <?= $this->addConditionEqual("test_id"); ?>
        <?php 

        $this->params["row_status_ready"] = RowStatus::READY;

        if ($type != QueryType::COUNT){
            echo $this->buildSort("variable_name, insert_date DESC");
        }
        $query = ob_get_clean();
        
        return $query;
    }
}