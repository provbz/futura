<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class EsisChildQuery extends QueryBase implements IQuery{

    public function findSchoolYears(){
        return EMEsis::execQuery("select DISTINCT(year)
            from school_evaluationyear");
    }

    public function descriptor() {
        $years = $this->findSchoolYears();
        $schoolYearList = [];
        foreach ($years as $year) {
            $schoolYearList[] = [
                "value" => $year["year"],
                "text" => $year["year"]
            ];
        }

        $descriptor = [
            "dataSource" => DataSource::ESIS,
            "permissions" => [
                "/admin/test/AlfaTableAdminAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "id" => [
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                    "enableSorting" => true,
                    "columnName" => "c.id"
                ],
                "year" => [
                    "label" => "Anno",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "sg_name" => [
                    "label" => "istituto",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "sg.name"
                ],
                "s_name" => [
                    "label" => "Scuola",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "s.name"
                ],
                "sc_name" => [
                    "label" => "Classe",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "sc.name"
                ],
                "name" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true,
                    "columnName" => "c.name"
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "sex" => [
                    "label" => "Genere",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "birth_date" => [
                    "label" => "Data di nascita",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "birth_place" => [
                    "label" => "Luogo di nascita",
                    "isComputedValue" => true,
                    "displayAsTableColumn" => true
                ],
                "italian_citizenship" => [
                    "label" => "Cittadinanza italiana",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "since_in_italy" => [
                    "label" => "In italia da",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ]
            ],
            "conditions" => [
                "year" => [
                    "label" => "Anno scolastico",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $schoolYearList
                ],
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ],
                "surname" => [
                    "label" => "Cognome",
                    "displayAsTableFilter" => true
                ],
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM child_child c
            left join school_evaluationyear sy on c.school_year_id=sy.id
            left join school_school s ON sy.school_id=s.id
            left join school_schoolgroup sg ON s.group_id=sg.id
            left join child_child_classes cc ON c.id=cc.child_id
            left join school_schoolclass sc ON cc.schoolclass_id=sc.id
            WHERE 1=1
            <?= $this->addConditionEqual("year") ?>
            <?= $this->addConditionLike("name") ?>
            <?= $this->addConditionLike("surname") ?>
        <?php 

        if ($type != QueryType::COUNT){
            echo $this->buildSort("year, sg.name, s.name, sc.name, c.surname, c.name");
        }
        $query = ob_get_clean();
        
        return $query;
    }
}