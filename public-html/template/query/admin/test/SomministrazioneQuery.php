<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class SomministrazioneQuery extends QueryBase implements IQuery{

    public function descriptor() {
        $schoolYears = SchoolYearService::findAllAsObject();
        $schoolYearList = [];
        foreach($schoolYears as $id => $label){
            $schoolYearList[] = [
                "value" => $id,
                "text" => $label
            ];
        }

        $classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $periodi = DictionaryService::findGroupAsDictionary(Dictionary::TEST_PERIODO);

        $descriptor = [
            "permissions" => [
                "/structure/test/SomministrazioniAction",
                "/admin/test/SomministrazioniAdminAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "somministrazione_id" => [
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true
                ],
                "school_year_id" => [
                    "label" => "Anno scolastico",
                    "displayAsTableColumn" => false,
                    "columnName" => "s.school_year_id"
                ],
                "school_year" => [
                    "label" => "Anno scolastico",
                    "displayAsTableColumn" => true,
                    "enableSorting" => false
                ],
                "data_inizio" => [
                    "label" => "Da",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "data_fine" => [
                    "label" => "A",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "status" => [
                    "label" => "Stato",
                    "isComputedValue" => true,
                    "displayAsTableColumn" => true
                ],
                "class_dictionary_value" => [
                    "label" => "Classe",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "periodo_dictionary_value" => [
                    "label" => "Periodo",
                    "displayAsTableColumn" => true,
                    "enableSorting" => true
                ],
                "tests" => [
                    "label" => "Test",
                    "isComputedValue" => true,
                    "displayAsTableColumn" => true
                ]
            ],
            "conditions" => [
                "school_year_id" => [
                    "label" => "Anno scolastico",
                    "displayAsTableFilter" => true,
                    "allowedValuesList" => $schoolYearList
                ],
                "class_dictionary_value" => [
                    "label" => "Classe",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $classes
                ],
                "periodo_dictionary_value" => [
                    "label" => "Periodo",
                    "displayAsTableFilter" => true,
                    "allowedValues" => $periodi
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM somministrazione s
            LEFT JOIN school_year sy ON s.school_year_id=sy.school_year_id
            WHERE row_status=:row_status_ready
            <?= $this->addConditionEqual("school_year_id"); ?>
            <?= $this->addConditionEqual("class_dictionary_value"); ?>
            <?= $this->addConditionEqual("periodo_dictionary_value"); ?>
        <?php 

        if (!UserRoleService::canCurrentUserDo(UserPermission::SOMMINISTRAZIONE_VISUALIZA_TUTTE)){
            if (UserRoleService::hasCurrentUser(UserRole::REFERENTE_ISTITUTO_SOMMINISTRAZIONI)){
                $this->addConditionForReferenteIstituto();
            } else if (UserRoleService::hasCurrentUser(UserRole::REFERENTE_PLESSO_SOMMINISTRAZIONI)){
                $this->addConditionForReferentePlesso();
            } else if (UserRoleService::hasCurrentUser(UserRole::INSEGNANTE_SOMMINISTRAZIONI)){
                $this->addConditionForInsegnante();
            } else {
                throw new Exception("Acceso negato");
            }
        }
        
        $this->params["row_status_ready"] = RowStatus::READY;

        if ($type != QueryType::COUNT){
            echo $this->buildSort("data_inizio DESC, class_dictionary_value, periodo_dictionary_value DESC");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    private function addConditionForInsegnante(){
        $constrants = StructureClassService::findAllInsegnanteConstraints($this->currentUser["user_id"]);
        if ($constrants->rowCount() == 0){
            throw new Exception("Accesso negato nessuna classe assegnata");
        }

        $i = 0;
        $in = "";
        foreach($constrants as $row){
            $in .= $in == "" ? $in .= "" : ", ";
            $in .= ":i_class_dictionary_value_$i";
            //echo " AND (s.school_year_id =:i_school_year_id_$i AND s.class_dictionary_value=:i_class_dictionary_value_$i) ";
            //$this->params["i_school_year_id_$i"] = $row["school_year_id"];
            $this->params["i_class_dictionary_value_$i"] = $row["class_dictionary_value"];
            $i++;
        }

        echo " AND (s.class_dictionary_value IN ($in)) ";
    }

    private function addConditionForReferentePlesso(){
        $this->addConditionForReferenteIstituto();
    }

    private function addConditionForReferenteIstituto(){
        $condition = "";
        $grades = MetadataService::findMetadataValue(MetadataEntity::USER, $this->currentUser["user_id"], UserMetadata::SCHOOL_GRADE);
        foreach ($grades as $grade) {
            if ($grade["value"] == SchoolGrade::INFANZIA){
                $condition .= $condition == "" ? "" : " OR ";
                $condition .= "class_dictionary_value LIKE :infanzia";
                $this->params["infanzia"] = "%I";
            }
            if ($grade["value"] == SchoolGrade::PRIMARIA){
                $condition .= $condition == "" ? "" : " OR ";
                $condition .= "class_dictionary_value LIKE :primaria";
                $this->params["primaria"] = "%P";
            }
        }

        if (StringUtils::isBlank($condition)){
            throw new Exception("Referente non associato ad un grado scolastico. Contattare il dirigente per eseguire l'associazione.");
        }

        echo " AND (" . $condition . ") ";
    }

    public function updateComputedColumns($results, $isExportExcel = false){
        $statuses = DictionaryService::findGroupAsDictionary(Dictionary::SOMMINISTRAZIONE_STASTUS);
        $classes = DictionaryService::findGroupAsDictionary(Dictionary::CLASS_GRADE);
        $periodi = DictionaryService::findGroupAsDictionary(Dictionary::TEST_PERIODO);

        for($i = 0; $i < count($results); $i++){
            $tests = SomministrazioneService::findAllTestsForSomministrazione($results[$i]["somministrazione_id"]);
            if ($isExportExcel){
                $results[$i]["tests"] = "";
                foreach($tests as $test){
                    $results[$i]["tests"] .= $test["name"] . ', ';
                }
                
                $status = SomministrazioneService::getStatus($results[$i]);
                $results[$i]["status"] = $status;
            } else {
                $results[$i]["tests"] = $tests->fetchAll();

                $status = SomministrazioneService::getStatus($results[$i]);
                $results[$i]["status"] = [
                    "key" => $status,
                    "value" => ArrayUtils::getIndex($statuses, $status, $status)
                ];
            }

            $results[$i]["class_dictionary_value"] = $classes[$results[$i]["class_dictionary_value"]];
            $results[$i]["periodo_dictionary_value"] = $periodi[$results[$i]["periodo_dictionary_value"]];
        }

        return $results;
    }
}