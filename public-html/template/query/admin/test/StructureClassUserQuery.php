<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class StructureClassUserQuery extends QueryBase implements IQuery{

    public function descriptor() {
        $descriptor = [
            "version" => 4,
            "permissions" => [
                "/structure/test/SomministrazioneStructureClassDetailsAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => false,
            "fields" => [
                "structure_class_user_id" => [
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true
                ],
                "user_id" => [
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true,
                    "columnName" => "u.user_id"
                ],
                "name" => [
                    "label" => "Nome",
                    "displayAsTableColumn" => true,
                    "enableSorting" => false
                ],
                "surname" => [
                    "label" => "Cognome",
                    "enableSorting" => true,
                    "displayAsTableColumn" => true
                ],
                "birth_date" => [
                    "label" => "Data di nascita",
                    "displayAsTableColumn" => true
                ],
            ],
            "conditions" => [
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];
        
        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            FROM structure_class_user su
            LEFT JOIN user u ON su.user_id=u.user_id
            WHERE row_status=:row_status_ready
            AND role_id=:user_role_student
            <?= $this->addConditionEqual("structure_class_id"); ?>
        <?php 

        $this->params["user_role_student"] = UserRole::STUDENTE;
        $this->params["row_status_ready"] = RowStatus::READY;

        if ($type != QueryType::COUNT){
            echo $this->buildSort("surname, name");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results){
        
        return $results;
    }
}