<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of EModelQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class RilevazioneCattedreQuery extends QueryBase implements IQuery {

    private $structureId;
    private $currentSchoolYear;
    private $schoolYearList;
    private $schoolYears;

    public function __construct(){
        $this->currentUser = UserService::getLoggedUser();
        $this->currentSchoolYear = SchoolYearService::findCurrentYear();
        
        $years = SchoolYearService::findAll();
        $this->schoolYearList = [];
        foreach ($years as $year) {
            $this->schoolYears[$year["school_year_id"]] = $year["school_year"];
            $this->schoolYearList[] = [
                "value" => $year["school_year_id"],
                "text" => $year["school_year"]
            ];
        }
    }

    public function descriptor() {
        if ($this->descriptor != null){
            return $this->descriptor;
        }

        $subStructures = StructureService::findAllWithParent($this->structureId);
        $structures = ["" => ""];
        foreach($subStructures as $subStructure){
            $structures[$subStructure["structure_id"]] = $subStructure["name"];
        }

        $descriptor = [
            "version" => 2,
            "permissions" => [
                "/admin/rilevazione_cattedre/RilevazioneCattedreAdminAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "rilevazione_cattedre_id" => [
                    "columnName" => "rilevazione_cattedre_id",
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
                "school_year_id" => [
                    "alwaysSelect" => false,
                    "displayAsTableColumn" => true,
                    "label" => "Anno scolastico"
                ],
                "structure_name" => [
                    "label" => "IC",
                    "columnName" => "s.name",
                    "displayAsTableColumn" => true
                ],
                "numero_alunni_no_italiano" => [
                    "label" => "Numero totale di alunne/i neoarrivate/i in Italia non italofone/i e non in grado di utilizzare l'Italiano come lingua di comunicazione e di studio, inserite/i a scuola negli ultimi tre anni scolastici (incluso l’anno scolastico corrente)",
                    "displayAsTableColumn" => true
                ],
                "numero_alunni_sostegno_linguistico" => [
                    "label" => "Numero di alunne/i neoarrivate/i nell’anno scolastico corrente con necessità di sostegno linguistico (incluse le iscrizioni pervenute prima dell’inizio dell’anno scolastico in corso, ovvero da giugno a fine agosto)",
                    "displayAsTableColumn" => true
                ],
                "uso_risorse_proprie" => [
                    "label" => "Uso risorse proprie",
                    "displayAsTableColumn" => true
                ],
                "dettaglio_risorse" => [
                    "label" => "Dettaglio risorse",
                    "displayAsTableColumn" => true
                ],
                "last_edit_date" => [
                    "label" => "Data modifica",
                    "displayAsTableColumn" => true,
                    "columnName" => "em.last_edit_date"
                ],
                "last_edit_user_id" => [
                    "label" => "Modificato da",
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true,
                    "columnName" => "em.last_edit_user_id"
                ]
            ],
            "conditions" => [
                "school_year_id" => [
                    "label" => "Anno scolastico",
                    "displayAsTableFilter" => true,
                    "defaultValue" => [
                        "value" => $this->currentSchoolYear['school_year_id'],
                        "text" => $this->currentSchoolYear['school_year']
                    ],
                    "allowedValuesList" => $this->schoolYearList
                ],
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        $this->descriptor = $descriptor;
        return $this->descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ ?>
            <?= $this->buildSelect(); ?>, u.name as u_name, u.surname as u_surname
        <?php } ?>
            FROM rilevazione_cattedre em
            LEFT JOIN user u ON em.last_edit_user_id = u.user_id
            LEFT JOIN structure s ON em.structure_id = s.structure_id
            WHERE 1 = 1
        <?php 
        
        echo $this->addConditionEqual("school_year_id");
        
        if ($type != QueryType::COUNT){
            echo $this->buildSort("school_year_id desc");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcelExport = false){
        for($i = 0; $i < count($results); $i++){
            if (isset($results[$i]['school_year_id'])){
                $results[$i]['school_year_id'] = $this->schoolYears[$results[$i]['school_year_id']];
            }

            if (isset($results[$i]['plesso_structure_id'])){
                $results[$i]['plesso_structure_id'] = StructureService::printFullStructureName($results[$i]['plesso_structure_id']);
            }

            if (isset($results[$i]['last_edit_user_id'])){
                $results[$i]['last_edit_user_id'] = $results[$i]['u_name'] . ' ' . $results[$i]['u_surname'];
            }
        }
        return $results;
    }

    private function computeObbligoScolastico($age){
        if (StringUtils::isBlank($age)){
            return "";
        }

        $stato = "";
        if ($age < 16){
            $stato = "Obbligo scolastico";
        } else if ($age >= 18){
            $stato = "Assolto il diritto-dovere all’istruzione e formazione";
        } else {
            $stato = "Assolto obbligo scolastico";
        }
        return $stato;
    }

    private function decryptBirthDate($v){
        $birthDate = EncryptService::decrypt($v);
        $decriptedValue = EncryptService::decrypt($v);
        return TextService::formatDate($decriptedValue);
    }

    private function buildCodes($id, $isExcel){
        $codes = EModelService::findCodes($id);
        if ($isExcel){
            $out = "";
            foreach($codes as $code){
                $out .= $out == "" ? "" : "\n";
                $out .= $code["code"] . ": " . $code['description'];
            }
            return $out;
        }

        $out = [];
        foreach($codes as $code){
            $out[] = [
                "code" => $code["code"],
                "description" => $code['description']
            ];
        }
        return $out;
    }
}