<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of PeiRestoreQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class PeiRestoreQuery extends QueryBase implements IQuery {

    private $structureId;
    private $currentSchoolYear;
    private $schoolYearList;
    private $schoolYears;

    public function __construct(){
        $this->currentUser = UserService::getLoggedUser();
        $this->currentSchoolYear = SchoolYearService::findCurrentYear();
        
        $years = SchoolYearService::findAll();
        $this->schoolYearList = [];
        foreach ($years as $year) {
            $this->schoolYears[$year["school_year_id"]] = $year["school_year"];
            $this->schoolYearList[] = [
                "value" => $year["school_year_id"],
                "text" => $year["school_year"]
            ];
        }
    }

    public function descriptor() {
        if ($this->descriptor != null){
            return $this->descriptor;
        }

        $subStructures = StructureService::findAllWithParent($this->structureId);
        $structures = ["" => ""];
        foreach($subStructures as $subStructure){
            $structures[$subStructure["structure_id"]] = $subStructure["name"];
        }

        $descriptor = [
            "version" => 2,
            "permissions" => [
                "/admin/pei/PeiRecuperaTableAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "school_year" => [
                    "columnName" => "sy.school_year",
                    "label" => "Anno scolastico",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
                "date" => [
                    "columnName" => "uydi.date",
                    "label" => "Data",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true
                ],
                "name" => [
                    "label" => "Eseguito da",
                    "columnName" => "eu.name",
                    "displayAsTableColumn" => true
                ],
                "surname" => [
                    "label" => "Eseguito da cognome",
                    "columnName" => "eu.surname",
                    "displayAsTableColumn" => false,
                    "alwaysSelect" => true
                ],
                "code" => [
                    "label" => "Codice studente",
                    "columnName" => "u.code",
                    "displayAsTableColumn" => true,
                ],
                "from_class" => [
                    "label" => "Codice studente",
                    "columnName" => "dfc.value",
                    "displayAsTableColumn" => true,
                ],
                "to_class" => [
                    "label" => "Codice studente",
                    "columnName" => "dtc.value",
                    "displayAsTableColumn" => true,
                ]
            ],
            "conditions" => [
                "school_year_id" => [
                    "label" => "Anno scolastico",
                    "columnName" => "uydi.school_year_id",
                    "displayAsTableFilter" => true,
                    "defaultValue" => [
                        "value" => $this->currentSchoolYear['school_year_id'],
                        "text" => $this->currentSchoolYear['school_year']
                    ],
                    "allowedValuesList" => $this->schoolYearList
                ],
                "code" => [
                    "label" => "Codice studente",
                    "displayAsTableFilter" => true
                ],
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        $this->descriptor = $descriptor;
        return $this->descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ ?>
            <?= $this->buildSelect(); ?>, u.name as u_name, u.surname as u_surname
        <?php } ?>
        from user_year_document_import uydi
        left join school_year sy ON sy.school_year_id = uydi.school_year_id
        left join user eu ON eu.user_id = uydi.edit_user_id
        left join user_year_document uyd ON uydi.user_year_document_id = uyd.user_year_document_id
        left join user u ON u.user_id = uyd.user_id
        left join dictionary dfc ON dfc.key = uydi.from_class
        left join dictionary dtc ON dtc.key = uydi.to_class
        WHERE 1 = 1
        <?php 
        
        echo $this->addConditionEqual("school_year_id");
        echo $this->addConditionLike("code");
        
        if ($type != QueryType::COUNT){
            echo $this->buildSort("date desc");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results, $isExcelExport = false){
        return $results;
    }
}