<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of StructureAdminQuery
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class StructureAdminQuery extends QueryBase implements IQuery{

    public function __construct (){

    }

    public function descriptor() {
        $descriptor = [
            "version" => 4,
            "dataSource" => DataSource::FUTURA,
            "permissions" => [
                "/admin/user/StructureAdminAction"
            ],
            "excelExportEnabled" => true,
            "enableOptions" => true,
            "fields" => [
                "structure_id" => [
                    "label" => "ID",
                    "displayAsTableColumn" => true,
                    "alwaysSelect" => true,
                    "enableSorting" => true
                ],
                "name" => [
                    "alwaysSelect" => false,
                    "displayAsTableColumn" => true,
                    "label" => "Nome"
                ],
                "plessi" => [
                    "displayAsTableColumn" => true,
                    "isComputedValue" => true,
                    "label" => "Plessi"
                ],
                "meccanografico" => [
                    "label" => "Meccanografico",
                    "displayAsTableColumn" => true
                ],
                "enabled" => [
                    "label" => "Attiva",
                    "displayAsTableColumn" => true
                ],
                "is_scuola_professionale" => [
                    "label" => "Professionale",
                    "displayAsTableColumn" => true
                ],
                "is_scuola_paritaria" => [
                    "label" => "Paritaria",
                    "displayAsTableColumn" => true
                ]
            ],
            "conditions" => [
                "name" => [
                    "label" => "Nome",
                    "displayAsTableFilter" => true
                ],
                "meccanografico" => [
                    "label" => "Meccanografico",
                    "displayAsTableFilter" => true
                ],
                "is_scuola_professionale" => [
                    "label" => "Professionale",
                    "displayAsTableFilter" => true,
                    "allowedValues" => [
                        1 => "Sì",
                        0 => "No"
                    ]
                ],
                "is_scuola_paritaria" => [
                    "label" => "Paritaria",
                    "displayAsTableFilter" => true,
                    "allowedValues" => [
                        1 => "Sì",
                        0 => "No"
                    ]
                ]
            ]
        ];

        foreach ($descriptor["fields"] as $key => $field) {
            if (!$field["displayAsTableColumn"]){
                continue;
            }
            $descriptor["excelFields"][$key] = $field;
        }
        
        return $descriptor;
    }

    public function buildQuery($type) {
        $this->params = [];

        ob_start();
        ?>
        SELECT 
        <?php if ($type == QueryType::COUNT){ ?>
            COUNT(*)
        <?php } else if ($type == QueryType::DATA){ 
            echo $this->buildSelect();
         } ?>
            from structure s
            WHERE parent_structure_id IS NULL
            <?= $this->addConditionLike("name") ?>
            <?= $this->addConditionLike("meccanografico") ?>
            <?= $this->addConditionEqual("is_scuola_professionale") ?>
            <?= $this->addConditionEqual("is_scuola_paritaria") ?>
        <?php 

        if ($type != QueryType::COUNT){
            echo $this->buildSort("name");
        }
        $query = ob_get_clean();
        
        return $query;
    }

    public function updateComputedColumns($results){
        for ($i = 0; $i < count($results); $i++) {
            if($results[$i]["structure_id"] != null){
                $results[$i]['plessi'] = $this->findPlessi($results[$i]["structure_id"]);
            }
        }

        return $results;
    }

    private function findPlessi($structureId){
        $res = EM::execQuery("SELECT name FROM structure WHERE parent_structure_id=:id AND enabled=1", ["id" => $structureId]);
        return $res->fetchAll();
    }
}