<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of ShareAction
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
abstract class QueryBase implements IQuery{
    public $request;
    public $params = [];
    public $descriptor;
    public $currentUser;

    public abstract function descriptor();
    public abstract function buildQuery($type);

    private function escapeFieldName($name){
        $chunks = explode(".", $name);
        $field = "";
        foreach ($chunks as $chunk) {
            $field .= $field == "" ? "" : ".";
            $field .= '`'. $chunk . '`';
        }
        return $field;
    }

    public function buildSelect(){
        $fields = "";
        
        foreach ($this->descriptor["fields"] as $fieldName => $field) {
            if (!isset($field["alwaysSelect"]) || !$field["alwaysSelect"]){
                continue;
            }

            $columnName = $this->escapeFieldName($fieldName);
            if (isset($field["columnName"]) && StringUtils::isNotBlank($field["columnName"])){
                $columnName = $this->escapeFieldName($field["columnName"]) . ' as ' . $fieldName;
            }

            $fields .= $fields == "" ? "" : ", ";
            $fields .= $columnName;  
        }

        if (is_array($this->request->fields)){
            foreach ($this->request->fields as $fieldName) {
                if (!isset( $this->descriptor["fields"][$fieldName])){
                    continue;
                }

                $field = $this->descriptor["fields"][$fieldName];
                if ($field == null || (isset($field["isComputedValue"]) &&  $field["isComputedValue"])){
                    continue;
                }

                $columnName = $this->escapeFieldName($fieldName);
                if (isset($field["columnName"]) && StringUtils::isNotBlank($field["columnName"])){
                    $columnName = $this->escapeFieldName($field["columnName"]) . ' as ' . $fieldName;
                }

                $fields .= $fields == "" ? "" : ", ";
                $fields .= $columnName;
            }
        }
        return $fields;
    }

    public function buildSort($defaultSort = ""){
        $order = "";
        if (!property_exists($this->request, "orders") || count((array)$this->request->orders) == 0){
            $order = $defaultSort;
        } else {
            foreach ($this->request->orders as $key => $value) {
                $order .= $order == "" ? "" : ", ";
                $order .= $key . " " . $value;
            }
        }

        if (StringUtils::isNotBlank($order)){
            return " ORDER BY " . $order;
        } 
        return "";
    }

    public function findColumnName($name){
        if (isset($this->descriptor["fields"][$name])){
            $field = $this->descriptor["fields"][$name];
            if (isset($field["columnName"])){
                return $field["columnName"];
            }
        } 
        if (isset($this->descriptor["conditions"][$name])){
            $field = $this->descriptor["conditions"][$name];
            if (isset($field["columnName"])){
                return $field["columnName"];
            }
        }

        return $name;
    }

    public function getCondition($name, $default = null){
        if (!property_exists($this->request, "conditions") || !property_exists($this->request->conditions, $name)){
            return $default;
        }

        return $this->request->conditions->$name;
    }

    public function addConditionLike($name){
        $condition = $this->getCondition($name);
        if (StringUtils::isBlank($condition)){
            return "";
        }

        $this->params[":" . $name] = "%" . $condition . "%";

        $columnName = $this->findColumnName($name);
        return " AND (" . $this->escapeFieldName( $columnName ) . ' LIKE :'. $name . ') ';
    }

    public function addConditionEqual($name){
        $condition = $this->getCondition($name);
        if (StringUtils::isBlank($condition)){
            return "";
        }
        
        $this->params[":" . $name] = $condition;

        $columnName = $this->findColumnName($name);
        return " AND (" . $columnName . ' = :'. $name . ') ';
    }

    public function addConditionDate($name){
        $nameFrom = $name . "_from";
        
        $conditionFrom = $this->getCondition($nameFrom);
        if (StringUtils::isNotBlank($conditionFrom)){
            $this->params[":" . $nameFrom] = $conditionFrom;
            $columnName = $this->findColumnName($nameFrom);
            return " AND (date(" . $columnName . ') >= :'. $nameFrom . ') ';
        }
        
        $nameTo = $name . "_to";
        $conditionTo = $this->getCondition($nameTo);
        if (StringUtils::isNotBlank($conditionFrom)){
            $this->params[":" . $nameTo] = $conditionTo;
            $columnName = $this->findColumnName($nameTo);
            return " AND (date(" . $columnName . ') >= :'. $nameTo . ') ';
        }
        
    }
}