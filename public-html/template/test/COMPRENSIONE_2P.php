<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MyRtfExtension
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class COMPRENSIONE_2P implements ITest{

    public function getVariablesNames(){
        return [
            "risposte_corrette" => [
                "name" => "risposte_corrette",
                "label" => "Risposte corrette",
                "type" => TestVariableType::RIGHTS
            ]
        ];
    }

    public function adminFormFields($somministrazione){
        $fields = [];

        $letterValues = [
            "A" => "A",
            "B" => "B",
            "C" => "C"
        ];

        $countReplies = 10;
        if ($somministrazione["periodo_dictionary_value"] == TestPeriodo::FINALE){
            $countReplies = 15;
        }

        for ($i = 0; $i < $countReplies; $i++){
            $fields[] = new FormSelect2Field("Risposta " . ($i + 1), "risposta_" . $i, FormFieldType::STRING, $letterValues, false);
        }

        return $fields;
    }
}