<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MyRtfExtension
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

class COMPRENSIONE_1P implements ITest{

    public function getVariablesNames(){
        return [
            "risposte_corrette" => [
                "name" => "risposte_corrette",
                "label" => "Risposte corrette",
                "type" => TestVariableType::RIGHTS
            ]
        ];
    }

    public function adminFormFields($somministrazione){
        $fields = [];

        $letterValues = [
            "A" => "A",
            "B" => "B",
            "C" => "C"
        ];
        for ($i = 0; $i < 5; $i++){
            $fields[] = new FormSelect2Field("Figura giusta " . ($i + 1), "figura_" . $i, FormFieldType::STRING, $letterValues, true);
        }

        $siNoValues = [
            "Sì" => "Sì",
            "No" => "No",
        ];
        for ($i = 0; $i < 5; $i++){
            $fields[] = new FormSelect2Field("Frase giusta " . ($i + 6), "frase_" . $i, FormFieldType::STRING, $siNoValues, true);
        }
        return $fields;
    }
}