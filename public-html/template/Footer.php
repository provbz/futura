<?php 
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MyRtfExtension
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

if (!isset($displayFooter) || $displayFooter == true){ ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="push"></div>
        
        <!-- footer wrapper -->
        <div class="footer-wrapper">
          <!-- footer row -->
          <div class="row-100 footer">
            <div class="large-12 columns margin-top padding-top">
                <a href="http://www.provincia.bz.it/it/privacy.asp">Normativa privacy</a>
                <br><br>
                <a href="https://futura-user-guide.readthedocs.io/en/latest/">Guida</a>
            </div>
          </div>
        </div>
    </div>
    <?php } ?>
    
    <script type="text/javascript">
        var appUrl = '<?= UriService::basePath(); ?>';
          
        <?php
        global $currentUser;
        if ($currentUser != NULL){ 
        ?>
        $(document).ready(function(){
            var t = setInterval(function (){
                $.get("<?php echo UriService::buildPageUrl("/public/OnlineUser", "setOnline"); ?>");
            }, <?php echo (Constants::$onlineInterval * 1000); ?>);
        });
        <?php } ?>
    </script>
  </body>
</html>

