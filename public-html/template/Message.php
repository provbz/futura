<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MyRtfExtension
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

if (isset($_SESSION[SESSION_REDIRECT_MESSAGE])){
    $message = $_SESSION[SESSION_REDIRECT_MESSAGE]; 
    unset ($_SESSION[SESSION_REDIRECT_MESSAGE]);
    ?>
    <div id="redirect_message" style="">
        <span>
            <?= _t($message) ?>
        </span>
    </div>
    <script type="text/javascript">
        function hideMessage(){
            $('#redirect_message').hide('slow');
        }
        $(document).ready(
            function (){
                var winWidth = $(window).width();
                var width = $('#redirect_message').width();
                $('#redirect_message').css("left", (winWidth - width) / 2 + "px");
                $('#redirect_message').show('slow');
                
                setTimeout("hideMessage()", 4000);
            }
        );
    </script>
<?php
}

if (isset($_SESSION[GlobalSessionData::PREV_USER_ACCESS])){
    global $currentUser;
    
    $prevUserAccess = $_SESSION[GlobalSessionData::PREV_USER_ACCESS];
    unset($_SESSION[GlobalSessionData::PREV_USER_ACCESS]);
    
    ?>
    <div id="prev-access-message">
        <h4>Accessi registrati</h4>
        <div class="row-100">
            <div class="columns small-12">
                <p>
                    Precedente accesso registrato il: <strong><?= _t($prevUserAccess['insert_date']) ?></strong><br/>
                    Se non si ritiene di aver eseguito l'accesso indicato si invita a:
                    <ul>
                        <li>Cambiare immediatamente la password.</li>
                        <li>Avvisare l'assistenza tecnica.</li>
                    </ul>
                </p>
                <button class="btn btn-primary btn-small" onclick="$('#prev-access-message').hide('slow');">Chiudi</button>
            </div>
        </div>
    </div>
    <?php
}

