<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of Bradcrumbs
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once(dirname(__FILE__) . '/Bradcrumb.php');

class Bradcrumbs {
    
    public $bradcrumbs;
    
    public function add($bc){
        if ($this->bradcrumbs == NULL){
            $this->bradcrumbs = array();
        }
        array_push($this->bradcrumbs, $bc);
    }
    
    public function getLast(){
        return $this->bradcrumbs[count($this->bradcrumbs) - 1];
    }
    
    public function draw(){
        if ($this->bradcrumbs == NULL){
            return "";
        }
        ?>
        <ul class="breadcrumbs">
            <?php foreach($this->bradcrumbs as $key => $value){ ?>
            <li class="<?= $value->link == "" ? "current" : "" ?>"><a href="<?= $value->link ?>"><?= _t( $value->label ) ?></a></li>
            <?php } ?>
        </ul>
        <?php
    }
}
