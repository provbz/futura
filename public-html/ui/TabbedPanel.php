<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of TabbedPanel
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
require_once dirname(__FILE__).'/TabbedPanelTab.php';

class TabbedPanel {
    private $tabs;
    private $async;
    public $containerId = "tabs";
    
    public function __construct($async = false) {
        $this->tabs = array();
        $this->async = $async;
    }
    
    public function addTab($tab){
        array_push($this->tabs, $tab);
    }
    
    public function draw(){
        $currentUrl = $_SERVER['REQUEST_URI'];
        
        if (!$this->async){
            echo '<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">';
                echo '<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">';
                foreach ($this->tabs as $value){
                    $class = "ui-state-default ui-corner-top";
                    if (strpos($currentUrl, $value->url) !== false ){
                        $class = "ui-state-default ui-corner-top ui-tabs-selected ui-state-active";
                    }
                    echo '<li class="'.$class.'"><a href="'.$value->url.'">'.$value->label.'</a><br/></li>';
                }
                echo '</ul>';
            echo '</div>';
        } else {
            echo '<div id="'.$this->containerId.'">';
            echo '<ul>';
            $i = 1;
            foreach ($this->tabs as $value){
                echo '<li id="ui_'.$this->containerId.'_'.$i.'"><a href="'.$value->url.'">'.$value->label.'</a></li>';
                $i++;
            }
            echo '</ul>';
            echo '</div>';
               ?>
<script type="text/javascript">
    TabManager('#<?= $this->containerId; ?>');
</script>
<?php
        }
    }
}

?>
