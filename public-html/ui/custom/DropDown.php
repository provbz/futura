<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of DropDown
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class DropDown {
    
    public $id;
    public $label;
    public $content;
    
    public function __construct($id, $label, $content) {
        $this->id = $id;
        $this->label = $label;
        $this->content = $content;
    }
    
    public function draw(){
        ?>
        <script type="text/javascript">
            $(function() {
                $('#<?= $this->id; ?>_button').removeAttr('href');
                $('#<?= $this->id; ?>_button').mouseup(function(login) {
                    $('#<?= $this->id; ?>_box').css("right", $('#<?= $this->id; ?>_box').parent().x +'px');
                    $('#<?= $this->id; ?>_box').toggle();
                    if($('#<?= $this->id; ?>_box').css('display') == 'block'){
                        activeDropDown = "<?= $this->id; ?>";
                    } else {
                        activeDropDown = "";
                    }
                });
                $(this).mouseup(function(login) {
                    if(activeDropDown == "<?= $this->id; ?>" && !($(login.target).parent('#<?= $this->id; ?>_button').length > 0) &&
                            !($(login.target).parent('#<?= $this->id; ?>_box').length > 0) && !($(login.target).parent('#<?= $this->id; ?>').length > 0)) {
                        $('#<?= $this->id; ?>_box').hide();
                        activeDropDown = "";
                    }
                }); 
            }); 
        </script>
        <div id="<?= $this->id; ?>_dropdown" class="drop_down">
            <a id="<?= $this->id; ?>_button" class="drop_down_button button">
                <img src="<?= UriService::buildImageUrl("add_icon.png"); ?>" width="20"/>
                <?= _t($this->label) ?>
            </a>
            <div style="clear:both;"></div>
            <div id="<?= $this->id; ?>_box" style="display:none;" class="drop_down_box">
                <?= _t($this->content) ?>
            </div>
        </div>
        <?php
    }
    
}
