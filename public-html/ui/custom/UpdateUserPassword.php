<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of UpdateUserPassword
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class UpdateUserPassword {
    
    public $actionUrl;
    
    public function __construct() {
        $this->actionUrl = UriService::buildPageUrl("/user/UserProfileAction", "updateUserPassword");
    }
        
    public function draw(){
        ?>
<script type="text/javascript">
function updateStudentPassword(username, userId){
    $('#dup_username').html(username);
    
    $('#dialog_update_password').dialog({
        title: "Modifica password",
        show: "blind",
        hide: "blind",
        resizable: false,
        modal: true,
        width: 300,
        buttons: {
            Annulla: function() {
                $( this ).dialog( "close" );
                $('#dup_password').val("");
            },
            Ok: function() {
                var password = $('#dup_password').val();
                if (StringUtils.isNotBlank( password )){
                    $.post("<?= $this->actionUrl; ?>", 
                            {userId: userId, password:password}, 
                            function (r){
                                 $('#pwd_' + userId).html(password);
                                 $('#dialog_update_password').dialog( "close" );
                                 $('#dup_password').val("");
                            }, 'json');
                } else {
                    alert('Password non valida!');
                }
            }
        }
    });
}
</script>
<div id="dialog_update_password" style="display:none;">
    <p>Inserire la nuova password per lo studente <span id="dup_username"></span>:</p>
    <input type="text" id="dup_password"/>
</div>
<?php
    }
    
}

?>
