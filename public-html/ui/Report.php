<?php
/* 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of Report
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
require_once(dirname(__FILE__) . '/ReportField.php');

class Report {
    private $fields;
    private $entity;

    public function __construct($entity = null){
        $this->entity = $entity;
    }

    public function addField($field){
        if ($this->fields == null){
            $this->fields = array();
        }
        array_push($this->fields, $field);
    }

    public function draw(){
        echo '<table class="report">';
        foreach($this->fields as $key => $value){
            $fv = '';
            if ($this->entity != null && isset($this->entity[$value->key])){
                $fv = $this->entity[$value->key];
            } else if ($value->value != null){
                $fv = $value->value;
            }
            echo '<tr><td class="report_caption">'.$value->label.'</td><td>';
            if ($value->formatter !== null){
                $frm = $value->formatter;
                echo $frm($fv, $this->entity);
            } else {
                echo $fv;
            }
            echo '</td></tr>';
        }
        echo '</table>';
    }
}
?>
