<?php
/* 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of Table
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require_once(dirname(__FILE__) . '/TableColumn.php');
require_once(dirname(__FILE__) . '/TableCondition.php');
require_once(dirname(__FILE__) . '/TableFilter.php');
require_once(dirname(__FILE__) . '/TableFilterCustom.php');

class Table {

    public $id = "dynamic_table";
    private $async;
    public $afterReloadJS = "";
    public $drawHeader = true;
    
    public $allowExportXLS;
    public $xlsFileName;
    
    public $select = array();
    public $countSelect = "*";
    public $entityName;
    public $joins = array();
    public $whereArray = array();
    public $orderArray = array();
    public $filters = array();
    public $groupBy;

    public $refreshUrl;
    
    public $paginateElements = true;
    public $currentPage = 0;
    public $elementsPerPage = 20;
    public $firstElement = 0;
    public $totalPages = 0;
    
    public $columns = NULL;
    public $formatKey = NULL;
    public $tools = NULL;
    public $defaultClickAction;
    
    public $formatTrClass;

    public $data;
    public $count;
    
    public $params;
    public $baseParams = [];
    public $query;
    
    public $isExportingExcel = false;
    
    public function __construct($async = false) {
        $this->async = $async;
        
        $this->xlsFileName = Constants::$APP_PRINT_NAME . '_esportazione_dati';
        if (getRequestValue("export_table") == "true"){
            $this->isExportingExcel = true;
        }
    }

    public function addSelect($column){
        if ($this->select == null){
            $this->select = array();
        }
        array_push($this->select, $column);
    }

    public function addWhere($condition, $paramName = "", $paramValue = ""){
        if ($this->whereArray == null){
            $this->whereArray = array();
        }
        array_push($this->whereArray, $condition);

        if (StringUtils::isNotBlank($paramName)){
            $this->baseParams[$paramName] = $paramValue;
        }
    }

    public function addOrder($condition){
        if ($this->orderArray == null){
            $this->orderArray = array();
        }
        array_push($this->orderArray, $condition);
    }

    public function addColumn($column){
        if ($this->columns == NULL){
            $this->columns = array();
        }
        array_push($this->columns, $column);
    }

    public function addJoin($join){
        array_push($this->joins, $join);
    }
    
    public function addFilter($filter){
        array_push($this->filters, $filter);
    }

    private function buildQuery(){
        $query = "";
        
        $query .= " FROM {$this->entityName}";

        $joins = "";
        foreach($this->joins as $key => $value){
            $joins .= $joins == "" ? "" : " ";
            $joins .= $value;
        }
        if ($joins != ""){
            $query .= " ".$joins;
        }

        $query .= $this->buildWheres();
        
        if (StringUtils::isNotBlank($this->groupBy)){
            $query .= ' GROUP BY '.$this->groupBy;
        }
        
        if ($this->orderArray){
            $orders = "";
            foreach ($this->orderArray as $key => $value){
                $orders .= $orders == "" ? "" : ", ";
                $orders .= $value;
            }
            $query .= " ORDER BY ".$orders;
        }
        
        return $query;
    }
    
    private function findElements(){
        $this->params = $this->baseParams;
        
        $query = "SELECT";

        if (count($this->select) > 0){
            $select = " ";
            foreach($this->select as $key => $value){
                $select .= $select == " " ? " " : ", ";
                $select .= $value;
            }
            $query .= $select;
        } else {
            $query .= " * ";
        }
        
        $query .= $this->buildQuery();
        
        if ($this->paginateElements){
            $query .= ' LIMIT '.$this->firstElement.', '.$this->elementsPerPage;
        }
        $this->query = $query;

        /*
        echo $query;
        print_r($this->params);
        */
        
        $res = [];
        try{
            $res = EM::execQuery($query, $this->params);
        } catch (PDOException $e){
            echo "Errore generazione tabella<br/>";
            LogService::error_($this, "Error executing query " . $query, $e);
        }

        return $res;
    }

    private function countElements(){
        $this->params = $this->baseParams;
        
        $query = "SELECT COUNT(".$this->countSelect.") as c ";
        $query .= $this->buildQuery();
        
        $result = EM::execQuerySingleResult($query, $this->params);
        return $result['c'];
    }

    private function buildWheres(){
        $wheres = "";
        if ($this->whereArray && count($this->whereArray) > 0){
            foreach ($this->whereArray as $key => $value){
                if ($value instanceof TableCondition){
                    $wheres .= $wheres == "" ? "" : " AND ";
                    $wheres .= $value->condition;
                    $this->params[$value->parameterName] = $value->parameterValue;
                } else {
                    $wheres .= $wheres == "" ? "" : " AND ";
                    $wheres .= "(" . $value . ")";
                }
            }
            $wheres = " WHERE ".$wheres;
        }
        
        if (is_array($this->filters) && count($this->filters) > 0 ){
            $wf = "";
            
            foreach($this->filters as $key => $value){
                $name = $value->name;
                if ($value instanceof TableFilter){
                    $name = $value->alias;
                }

                $requestValue = getRequestValue("filter_" . $name);
                if (StringUtils::isBlank($requestValue)){
                    if ($value->defaultValue == NULL){
                        continue;
                    } else {
                        $requestValue = $value->defaultValue;
                    }
                }

                if ($value instanceof TableFilterCustom){   
                    $wfc = $value->format($requestValue);
                    if (StringUtils::isNotBlank($wfc)){
                        $wf .= $wf == "" ? "" : " AND ";
                        $wf .= $wfc;
                    }
                } else if ($value instanceof TableFilter){
                    $paramName = $this->nameToParamName($value->name);
                    $columnName = $this->escapeColumnName($value->name);

                    $wf .= $wf == "" ? "" : " AND ";
                    $wf .= $columnName . $value->operator . ':' .  $paramName;
                    $this->params[':' . $paramName] = $requestValue;
                } 
            }
            
            if (StringUtils::isNotBlank($wf)){
                if (StringUtils::isBlank($wheres)){
                    $wheres = " WHERE ".$wf;
                } else {
                    $wheres .= ' AND '.$wf;
                }
            }
        }
        
        return $wheres;
    }

    private function escapeColumnName($name){
        $chunks = explode(".", $name);
        $res = "";
        foreach($chunks as $value){
            $res .= $res == "" ? "" : ".";
            $res .= "`" . $value . "`";
        }
        return $res;
    }

    private function nameToParamName($name){
        return str_replace(".", "_", $name);
    }

    public function prepare(){
        if (getRequestValue("export_table") == "true"){
            $this->isExportingExcel = true;
            $this->paginateElements = false;
        }

        $this->currentPage = getRequestValue("dt_current_page", 0);
        if (!is_numeric($this->currentPage) || $this->currentPage < 0){
            $this->currentPage = 0;
        }
        $this->firstElement = $this->currentPage * $this->elementsPerPage;

        if ($this->data == NULL && !is_array($this->data)){
            $this->data = $this->findElements();
            $this->count = $this->countElements();
        }
        
        $this->totalPages = intval($this->count / $this->elementsPerPage);
        if($this->count % $this->elementsPerPage > 0){
            $this->totalPages++;
        }
    }
    
    public function export($fileName = "export.xls"){
        $objPHPExcel = new Spreadsheet();
        $objPHPExcel->getProperties()->setCreator(Constants::$APP_PRINT_NAME)
                    ->setLastModifiedBy(Constants::$APP_PRINT_NAME);

        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        foreach ($this->columns as $key => $column){
            if ($column instanceof TableColumn){
                if (! $column->exportable){
                   continue;
                }
                
                $sheet->getColumnDimensionByColumn($key + 1)->setAutoSize(true);
                $sheet->getStyleByColumnAndRow($key + 1, 1)->getFont()->setBold(true);
                $sheet->setCellValueByColumnAndRow($key + 1, 1, $column->name);        
                echo $key . " " . $column->name;
            }
        }
        
        $rowCount = 2;
        foreach($this->data as $row){
            foreach ($this->columns as $key => $value){
                $v = "";
                if ($value instanceof TableColumn){
                    if (! $value->exportable){
                        continue;
                    }
                    if ($value->valueFormatter !== NULL){
                        $frm = $value->valueFormatter;
                        if ($value->key !== NULL){
                            $rv = $row[$value->key];
                            $v .= $frm($rv, $row);
                        } else {
                            $v = $frm($row);
                        }
                    } else if (isset($row[$value->key])){
                        $v = $row[$value->key];
                    }
                } else {
                    $v = $row[$key];
                }
                
                $sheet->setCellValueByColumnAndRow($key + 1, $rowCount, $v);        
            }
            $rowCount++;
        }
        
        $objPHPExcel->setActiveSheetIndex(0);

        ob_end_clean();
        
        header_remove();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, 'Xls');
        $objWriter->save('php://output');
        exit();
    }

    public function draw(){
        global $requestPackage, $requestAction, $requestMethod;
        
        $this->prepare();

        if ( $this->isExportingExcel){
            if (getRequestValue("table_id") == $this->id){
                $this->export();
            }
            return;
        }
        
        if ($this->currentPage == 0 || !$this->async){
            $par = "";
            foreach ($_REQUEST as $key => $value){
                if ($key != "dt_current_page" && !is_array($value)){
                    $par .= '&'.$key.'='. $value;
                }
            }
                        
            if (StringUtils::isBlank($this->refreshUrl)){
                $url = UriService::buildPageUrl($requestPackage.'/'.$requestAction, $requestMethod );
            } else{
                $url = $this->refreshUrl;
            }
            if (strpos($url, "?") === false){
                $url .= "?";
            } else {
                $url .= "&";
            }
                        
            ?>
            <script type="text/javascript">
                function refreshTable(){
                    openPage(<?= $this->currentPage; ?>);
                }
                
                function openPage(page){
                    var url = "<?= $url ?>dt_current_page=" + page + "<?= $par ?>";
                    javascript:console.log(url);
                    var async = <?= $this->async != NULL ? $this->async : 0 ?>;
                    
                    if (async){
                        loadDiv(
                        <?php
                        $out = "'#$this->id', url, ''";
                        if (StringUtils::isNotBlank($this->afterReloadJS)){
                            $out .= ', '. $this->afterReloadJS;
                        }
                        echo $out;
                        ?>);
                    } else {
                        window.location = url;
                    }
                }
            </script>
            <?php
        }
        
        ?>
        <div id="<?= $this->id ?>">
        <?php
            $this->buildTable($this->formatKey, true, true, $this->defaultClickAction, $this->formatTrClass);
            if ($this->paginateElements){
                $this->drawPaginator();
            }
            if ($this->allowExportXLS){
                $link = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : "";
                if (StringUtils::isNotBlank($link)){
                    if (strpos($link, "?") !== FALSE){
                        $link .= '&';
                    } else {
                        $link .= '?';
                    }
                    $link .= 'export_table=true&table_id='.$this->id;
                    
                    echo '<a href="' . $link . '" class="fa fa-file-excel-o"> Scarica Excel</a>';
                }
            }
        ?>
        </div>
        <?php
    }
    
    public function buildTable($formatKey = NULL, $print = false, $enableJs = false, $defaultClickAction = "", $formatTrClass = NULL){
        $res = "";

        if ($this->tools != NULL){
            $res .= "<div class='table_tools'>";

            if (count($this->tools) > 0){
                $res .= Formatter::writeTool(new TableTool("Tutti", "tableSelectAll()"));
                $res .= Formatter::writeTool(new TableTool("Nessuno", "tableDeselectAll()"));
            }

            foreach($this->tools as $key => $value){
                if ($value instanceof TableTool){
                    if ($value->formatter != null){
                        $frm = $value->formatter;
                        $res .= $frm();
                    } else {
                        $res .= Formatter::writeTool($value);
                    }
                }
            }

            $res .= "</div>";
        }

        $res .= '<table class="elenco">';
        
        if ($this->drawHeader){
            $res .= "<thead>";
            $res .= '<tr>';
            if ($this->tools != null && count( $this->tools ) > 0){
                $res .= '<th></th>';
            }
            
            foreach ($this->columns as $key => $value){
                if ($value instanceof TableColumn){
                    $res .= '<th style="'. $value->style .'">'.$value->name.'</th>';
                } else {
                    $res .= '<th>'.$value.'</th>';
                }
            }
            $res .= '</tr>';
            $res .= "</thead>";
        }

        $res .= "<tbody>";

        $rowCount = 0;
        foreach($this->data as $row){
            $res .= '<tr class="table_row ';
            if ($formatTrClass != NULL){
                $res .= $formatTrClass($row);
            }
            $res .= '"';
            if ($formatKey != NULL){
                $res .= 'id="tr_'.$formatKey($row).'"';
            }
            $res .= '>';
            
            if ($this->tools != null && count($this->tools) > 0){
                $res .= '<td>';
                $res .= '<input class="select_row" type="checkbox" id="row_'.$rowCount.'" value="';
                if ($formatKey == NULL){
                    $res .= $rowCount;
                }else {
                    $res .= $formatKey($row);
                }
                $res .= '"/>';

                if ($formatKey != NULL){
                    $res .= '<input type="hidden" value="'.$formatKey($row).'"/>';
                }
                $res .= '</td>';
            }

            foreach ($this->columns as $key => $value){
                $res .= '<td>';
                if ($value instanceof TableColumn){
                    if (!isset($row[$value->key])){
                        $res .= " ";
                    } else if ($value->valueFormatter !== NULL){
                        $frm = $value->valueFormatter;

                        if ($value->key !== NULL){
                            $rv = $row[$value->key];
                            $res .= $frm($rv, $row);
                        } else {
                            $res .= $frm($row);
                        }
                    } else if (isset($row[$value->key])){
                        $res .= _t( $row[$value->key] );
                    } else {
                        $res .= " ";
                    }
                } else {
                    $res .= _t( $row[$key] );
                }
                $res .= '</td>';
            }
            $res .= "</tr>\n";
            $rowCount++;
        }
        $res .= "</tbody>";
        $res .= "</table>\n";

        if ($enableJs && $print){
            ?>
            <script type="text/javascript">
                $(document).ready(
                    function (){
                        $('table.data tbody tr.table_row').mouseover(function() {
                        $(this).addClass('table_row_hover');
                    }).mouseout(function() {
                        $(this).removeClass('table_row_hover');
                    }).click(function() {
                            var id = $('td:first > input', this).val();;
                            <?php echo $defaultClickAction.'(id)'; ?>;
                    });
                });
            </script>
            <?php
        }
        
        if ($print){
            echo $res;
        }else {
            return $res;
        }
    }

    private function drawPaginator(){
        $displayPages = 10;
        ?>
        <ul class="pagination">
            <div class="pagine">
            <?php if ($this->currentPage > 0){ ?>
                <li class="arrow"><a href="javascript:openPage('<?= $this->currentPage - 1 ?>');">&laquo;</a></li>
            <?php } else { ?>
                <li class="arrow unavailable">&laquo;</li>
            <?php }

            $firstPage = $this->currentPage - $displayPages / 2;
            if ($firstPage < 0){
                $firstPage = 0;
            }
        
            $lastPage = $this->totalPages;
            if ($lastPage - $firstPage > $displayPages){
                $lastPage = $firstPage + $displayPages;
            }

            for ($i = $firstPage; $i < $lastPage; $i++){
                if ($i == $this->currentPage){ ?>
                    <li class="current"><a href=""><?= ($i + 1) ?></a></li>
                <?php } else { ?>
                    <li class=""><a href="javascript:openPage(<?= $i ?>);"><?= ($i + 1) ?></a></li>
                <?php } ?>
            <?php
            }

            if ($this->currentPage + 1 < $this->totalPages){ ?>
                <li class="arrow"><a href="javascript:openPage(<?= ($this->currentPage + 1) ?>)">&raquo;</a></li>
            <?php } else { ?>
                <li class="arrow unavailable"><a href="javascript:openPage(<?= ($this->currentPage + 1) ?>)">&raquo;</a></li>
            <?php } ?>
        </ul>
        <p>Pagine: <?= $this->totalPages ?> - Elementi: <?= $this->count ?></p>
        <?php
    }
}
