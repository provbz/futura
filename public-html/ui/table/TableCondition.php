<?php
class TableCondition{
    public $condition;
    public $parameterName;
    public $parameterValue;

    function __construct($condition, $parameterName, $parameterValue){
        $this->condition = $condition;
        $this->parameterName = $parameterName;
        $this->parameterValue = $parameterValue;
    }
}