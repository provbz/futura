<?php
/* 
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormTextField extends FormField{

    public $allowedChars = '';
    public $match = '';
    public $maxLength = 0;
    
    public function draw($entity = NULL){
        if ($this->readOnly){
            $this->parameters["disabled"] = "true";
        }

        if (isset( $this->parameters['maxLength'] )){
            $this->maxLength = $this->parameters['maxLength'];
        }

        $type = "text";
        if (isset($this->parameters["numeric"])){
            $type = "number";
        }

        ?>
        <div class="">
            <input type="<?= $type ?>" name="<?= _t($this->name) ?>" id="<?= _t($this->name) ?>"
                <?php
                if (StringUtils::isNotBlank($this->allowedChars)){
                    echo ' allowedChars="' . $this->allowedChars . '" ';
                }
                if (StringUtils::isNotBlank($this->match)){
                    echo ' match="' . $this->match . '" ';
                }
                if ($this->required){
                    echo ' required ';
                }
                if ($this->hasErrors()){
                    echo ' data-invalid';
                }
                if ($this->readOnly){
                    echo ' disabled';
                }
                if($this->maxLength > 0){
                    echo ' maxLength="' . $this->maxLength .'" ';
                }
                ?>
                <?= $this->prepareParameters() ?> value="<?php
                $value = $this->getValue($entity);
                if ($value != NULL) {
                    if (is_string($value)){
                        echo _t($value, ENT_QUOTES );
                    } else {
                        echo htmlentities(json_encode($value));
                    }
                }
                ?>"
                ></input>

                <?= $this->drawErrorTag() ?>
        </div>
        <?php
    }
}
