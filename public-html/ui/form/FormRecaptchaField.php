<?php

/* 
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */
class FormRecaptchaField extends FormField{

    public $allowedChars = '';
    public $match = '';
    
    public function  __construct() {
        $this->label = "Controlla";
        $this->persist = false;
    }
    
    public function draw($entity = NULL){
        if ($this->readOnly){
            $this->parameters["disabled"] = "true";
        }
        ?>
        
        <div class="g-recaptcha" data-sitekey="<?= RecaptchaConstants::$CLIENT_SECRET ?>"></div>
        <?= $this->drawErrorTag() ?>
        
        <?php
    }
    
    public function check(){
        if (!isset($_REQUEST['g-recaptcha-response'])){
            $this->addFieldError("Confermare la richiesta.");
        }
        
        $recaptcha = $_REQUEST['g-recaptcha-response'];
        if (StringUtils::isBlank($recaptcha)){
            $this->addFieldError("Confermare la richiesta.");
        }
        
        $res = RecaptchaService::check($recaptcha);
        
        if (!$res->success){
            LogService::warn("recaptcha", json_encode($res));
            $this->addFieldError("Richiesta non valida.");
        }
    }
}
    