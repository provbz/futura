<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormCheckField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormCheckField extends FormField{
    
    private $values;
    
    public function __construct($label, $name, $type, $values, $required = false ) {
        parent::__construct($label, $name, $type, $required);
        $this->values = $values;
    }
    
    public function draw($entity = NULL){
        $sel = $this->getValue($entity);
        ?>
        <div class="checkbox">
        <?php
        
        $i = 0;
        foreach($this->values as $key => $value){
            $checked = $sel !== NULL && $sel == $key ? "checked" : ''; ?>
            <input type="checkbox" id="<?= $this->name.'_'.++$i ?>" name="<?= $this->name ?>" 
                value="<?= $key ?>" <?= $checked ?>
                <?= $this->readOnly ? " disabled " : "" ?>/>
            <label for="<?= $this->name . '_'. $i ?>"><?= _t($value) ?></label></br>'
        <?php } ?>
        </div>
        <?php
    }
    
    function getValueForDb() {
        $value = parent::getValueForDb();
        if (StringUtils::isNotBlank($value)){
            return $value;
        }
        if (count($this->values) == 1){
            return 0;
        }
    }
    
}
