<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormFieldSlider
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormSliderField extends FormField {

    public $min;
    public $max;
    public $step;
    
    public function __construct($label, $name, $type, $required = false, $defaultValue = NULL, $parameters = NULL, $min=0, $max=1, $step= 0.1) {
        parent::__construct($label, $name, $type, $required, $defaultValue, $parameters);
        
        $this->min = $min;
        $this->max = $max;
        $this->step = $step;
    }
    
    public function draw($entity = NULL) {
        $out = "<script> \n".
                "$(function() { \n".
                    "$('#slider_$this->name').slider({ \n".
                    "	value:".$this->getValue($entity).", \n".
                    "	min: $this->min, \n".
                    "	max: $this->max, \n".
                    "	range: 'min', \n".
                    "	step: $this->step, \n".
                    "    slide: function( event, ui ) { ".
                                "var value = $( '#slider_$this->name' ).slider( 'value' );\n".
                                "$('#$this->name').val( value );\n".
                                "$('#text_$this->name').html( value );\n".
		    "	} ".
                    "}); \n".
                
                    "var value = $( '#slider_$this->name' ).slider( 'value' );\n".
                    "$('#$this->name').val( value );\n".
                    "$('#text_$this->name').html( value );\n".
                "}); \n".
                "</script> \n";
        
        
        $out .= '<div id="slider_'.$this->name.'" style="width:50%;"></div>';
        $out .= '<span id="text_'.$this->name.'"></span>';
        $out .= '<input id="'.$this->name.'" name="'.$this->name.'" type="hidden"/>';
        echo $out;;
    }
    
}
