<?php
/* 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormPasswordField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormPasswordField extends FormField{

    private $id;

    public function  __construct($label, $name, $id, $parameters = NULL) {
        $this->label = $label;
        $this->name = $name;
        $this->id = $id;
        
        $this->parameters = $parameters;
        $this->defaultValue = "";

        $this->required = false;
    }

    public function encodeFunction(){
        $value = getRequestValue($this->id.'_1');
        if ($value == ""){
            return "";
        } else {
            return md5(md5($value) . Constants::$cookieSectret);
        }
    }

    public function draw($entity = NULL){
        $out = '<input type="password" name="'.$this->id.'_1" id="'.$this->name.'"';
        $out .= $this->prepareParameters();
        $out .= ' value=""></input><br/>';

        $out .= '<input type="password" name="'.$this->id.'_2" id="'.$this->name.'"';
        $out .= $this->prepareParameters();
        $out .= ' value=""></input> (ridigita)';
        
        echo $out;
    }

    public function check(){
        $value1 = getRequestValue($this->id.'_1');
        $value2 = getRequestValue($this->id.'_2');

        if ($value1 != $value2){
            $this->addFieldError("Le password non coincidono");
        }
    }
}
