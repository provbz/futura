<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormRadioField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormRadioField extends FormField{
    
    public $values;

    public function  __construct($label, $name, $type, $values, $required = false, $defaultValue = NULL, $parameters = NULL) {
        $this->values = $values;
        parent::__construct($label, $name, $type, $required, $defaultValue, $parameters);
    }

    public function draw($entity = NULL) {
        $v = $this->getValue($entity);
        if ($v == null){
            $v = $this->defaultValue;
        }
        ?>
        <div class="radio_list">
            <?php
            $i = 0;
            foreach ($this->values as $key => $value){
                ?>
                <input type='radio'
                    <?php if (StringUtils::isNotBlank($this->name)){ ?>
                         name='<?= _t($this->name) ?>' id='<?= _t($this->name) . "_" . $i ?>'
                    <?php } ?>
                    value='<?= _t($key) ?>'
                    <?php if ($v != NULL && $key == $v){ ?>
                        checked
                    <?php } ?>
                    <?= $this->prepareParameters() ?>/> 
                    <label for='<?= $this->name."_".$i ?>'><?= _t($value) ?></label>
                <?php
                $i++;
            } ?>
            <?php $this->drawErrorTag() ?>
        </div>
        <?php
    }
}
