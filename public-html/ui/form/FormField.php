<?php
/* 
---------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
abstract class FormField {

    public $form;
    public $label;
    public $labelBold = true;
    public $required;
    public $name;
    public $readOnly;
    public $parameters;
    public $defaultValue;
    public $type;
    public $persist = true;
    public $note;
    public $checkFunction;
    public $blockStyle;
    public $blockAdditionalClass;
    public $escapeHtmlOnPersist = true;
    public $isEnchripted = false;
    public $displayFormRow = true;
    public $labelClass = "large-3 medium-3 columns";
    public $itemClass = "large-9 medium-9 columns";
    
    public $validationErrors = [];
    
    public function  __construct($label, $name, $type, $required = false, $defaultValue = NULL, $parameters = NULL) {
        $this->parameters = $parameters;
        $this->name = $name;
        $this->type = $type;
        $this->required = $required;
        $this->label = $label;
        $this->defaultValue = $defaultValue;
    }

    abstract public function draw($entity = NULL);
    
    public function check(){
        $value = $this->getValue(null);

        if ($value == "" && $this->required){
            $this->addFieldError("Dato richeisto.");
        }

        if (is_string($value) && StringUtils::isNotBlank($value)){
            if (isset ($this->parameters['maxLength']) && strlen($value) > $this->parameters['maxLength']){
                $this->addFieldError("La lunghezza massima del campo è ". $this->parameters['maxLength']);
            }
            if (isset ($this->parameters['minLength']) && strlen($value) < $this->parameters['minLength']){
                $this->addFieldError("La lunghezza minima del campo è ". $this->parameters['minLength']);
            }
            if (isset ($this->parameters['numeric'])){
                if (!is_numeric($value)){
                    $this->addFieldError("È richiesto un valore numerico");
                }
            }
        }

        if ($this->checkFunction != NULL){
            $fn = $this->checkFunction;
            $fn($this, $value);
        }
    }

    public function hasErrors(){
        return $this->validationErrors != NULL && count($this->validationErrors) > 0;
    }

    public function addFieldError($message){
        array_push($this->validationErrors, $message);
        if(isset($_REQUEST[$this->name.'_error'])){
            unset($_REQUEST[$this->name.'_error']);
        }
        $error = '';
        foreach($this->validationErrors as $key => $value){
            $error .= $value.'<br/>';
        }
        $error .= '';
        $_REQUEST[$this->name .'_error'] = $error;
    }

    public function getValue($entity){
        $value = $this->defaultValue;

        if ($entity != NULL && !is_array( $entity )) {
            $entity = get_object_vars($entity);
        }

        if ($entity != null && isset($entity[$this->name])){
            $value = $entity[$this->name];
            if ($this->isEnchripted){
                $value = EncryptService::decrypt($value);
            }
        }

        if (!$this->readOnly){
            if ($this->form != null && $this->form->isSubmit()){
                $value = getRequestValue($this->name);
            } else if (getRequestValue($this->name, "default_value___") != "default_value___"){
                $value = getRequestValue($this->name);
            }
        }

        if (isset ($this->parameters['numeric'])){
            $value = str_replace(",", ".", $value);
        }

        return $value;
    }

    public function prepareParameters(){
        $out = "";
        if (!is_array($this->parameters)){
            return $out;
        }

        foreach($this->parameters as $key => $value){
            $out .= ' '.$key.'="'.$value.'" ';
        }
        return $out;
    }
    
    public function getValueForDb(){
        $value = getRequestValue($this->name);
        if ($this->type == FormFieldType::NUMBER){
            if (StringUtils::isBlank($value)){
                return null;
            }
            
            if (isset ($this->parameters['numeric'])){
                $value = str_replace(",", ".", $value);
            }

            return (float) $value;
        } else if ($this->type == FormFieldType::DATE){
            return TextService::parseDate($value);
        }

        return $value;
    }
    
    public function drawErrorTag(){
        $out = '';
        if ($this->hasErrors()){
            $out .= '<small class="error" id="'.$this->name.'_errors">';
            $out .= $this->getErrors();
            $out .= '</small>';
        }
        return $out;
    }
    
    public function getErrors(){
        return getRequestValue($this->name.'_error');
    }

    public function getDecodedValue($entity = null){
        $value = $this->getValue($entity);
        if ($value == null){
            return null;
        }
        if (is_array($value)){
            return $value;
        }

        $value = urldecode($value);
        $decoded = json_decode($value, true);
        $error = json_last_error();
        if ($error){
            return $value;
        }
        return $decoded;
    }
}

