<?php

/*
 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormDateMultipleField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormDateMultipleField extends FormField{
    
    public $startYear;
    public $endYear;
    public $displayYear = true;
    public $displayMonth = true;
    public $displayDay = true;
    
    public function __construct($label, $name, $type, $required = false, $defaultValue = NULL, $parameters = NULL) {
        parent::__construct($label, $name, $type, $required, $defaultValue, $parameters);
        
        if (StringUtils::isBlank($this->startYear)){
            $this->startYear = 1950;
        }
        if (StringUtils::isBlank($this->endYear)){
            $d = new DateTime();
            $this->endYear = $d->format('Y');
        }
    }
    
    public function draw($entity = NULL){
        $value = $this->getValue($entity);
        
        $date = ['', '', ''];
        if (StringUtils::isNotBlank( $value )){
            $chuncks = mb_split("/", $value);
            
            for ($i = 0; $i < count($date); $i++){
                if (count($chuncks) > $i){
                    $date[$i] = $chuncks[$i];
                }
            }
            
            if (count($chuncks) != 3){
                $this->addFieldError("Data non valida.");
            } else {
                if (checkdate((int)$chuncks[1], (int)$chuncks[2], (int) $chuncks[0] ) !== true){
                    $this->addFieldError("Data non valida.");    
                }
            }
        }
        ?>
        <div class="">
            <div id="<?= _t($this->name) ?>_container" class="date_select_field">
            <?php
                if ($this->displayDay){
                    $this->drawSelect($this->name . '_day', 1, 31, $date[2]);
                    echo '-';
                }
                if ($this->displayMonth){
                    $this->drawSelect($this->name . '_month', 1, 12, $date[1]);
                    echo '-';
                }
                if ($this->displayYear){
                    $this->drawSelect($this->name . '_year', $this->endYear, $this->startYear, $date[0]);
                }
                ?>
            </div>
            <?= $this->drawErrorTag() ?>
        </div>
        <?php
    }
    
    public function getValue($entity = NULL) {
        if (StringUtils::isNotBlank( getRequestValue($this->name.'_year') )){
            $date = "";
            if ($this->displayYear){
                $date = getRequestValue($this->name .'_year');
            } else {
                $date = "2000";
            }
            
            $date .= '/';
            
            if ($this->displayMonth){
                $date .= getRequestValue($this->name .'_month');
            } else {
                $date .= "01";
            }
            $date .= '/';
            
            if ($this->displayDay){
                $date .= getRequestValue($this->name .'_day');
            } else {
                $date .= "01";
            }
            
            return $date;
        }
        return parent::getValue($entity);
    }
    
    public function getValueForDb() {
        return $this->getValue();
    }
    
    private function drawSelect($name, $from, $to, $selectedValue){
        echo '<select name='.$name.'>';
        
        $cond = function($a, $b){
            return $a <= $b;
        };
        
        $step = 1;
        if ($from > $to){
            $step = -1;
            
            $cond = function($a, $b){
                return $a >= $b;
            };
        }
        
        $checked = '';
        if ( intval( $selectedValue ) == $selectedValue){
            $checked = 'selected="true"';
        }
        echo '<option value="" '.$checked.'></option>';
        
        for ($i = $from; $cond($i, $to); $i += $step){
            $checked = '';
            if ( intval( $selectedValue ) == $i){
                $checked = 'selected="true"';
            }
            echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
        }
        echo '</select>';
    }
}
