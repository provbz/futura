<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormCheckboxMultiField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormCustomContentField extends FormField {

    public $drawFn;
    public $checkFn;

    public function  __construct($label, $name, $type, $required = false, $defaultValue = NULL, $parameters = NULL) {
        $this->escapeHtmlOnPersist = false;
        parent::__construct($label, $name, $type, $required, $defaultValue, $parameters);
    }

    public function draw($entity = NULL) {
        if ($this->drawFn != null){
            $fn = $this->drawFn;
            $fn($this);
        }
    }

    public function check(){
        parent::check();
        if ($this->checkFn != null){
            $fn = $this->checkFn;
            $fn($this);
        }
    }
}