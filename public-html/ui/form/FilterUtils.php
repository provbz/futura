<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FilterUtils
 *
 * @author Marco
 */
class FilterUtils {
    
    public static function drawSelectFilter($label, $name, $values, $selected = "") {
        ?>
        <label class="columns medium-4">
            <?= _t($label) ?>
        </label>
        <div class="columns medium-8">
            <?= Formatter::buildSelect($name, $values, $selected);?>
        </div>
        <?php
    }
    
    public static function drawTextFilter($label, $name){
        $value = getRequestValue($name);
        ?>
        <label class="columns medium-4">
            <?= _t($label) ?>: 
        </label>
        <div class="columns medium-8">
            <input type="text" name="<?= _t($name) ?>" id="<?= _t($name) ?>" value="<?= _t($value) ?>"/>
        </div>
        <?php
    }
    
    public static function drawDateFilter($label, $name){
        $field = new FormDateField($label, $name, FormFieldType::DATE);
        ?>
        <label class="columns medium-4">
            <?= _t($label) ?>
        </label>
        <div class="columns medium-8">
            <?= $field->draw(); ?>
        </div>
        <?php
    }
    
    public static function drawSubmit(){
        ?>
        <div class="row-100">
            <div class="columns small-12 medium-6 large-4">
                <label class="columns medium-4">
                </label>
                <div class="columns medium-8">
                    <a class="button small expanded secondary" onclick="return submitParentForm(this);">
                        <i class="<?= Icon::SEARCH ?>"></i> Cerca
                    </a>
                </div>
            </div>
        </div>
        <?php
    }
}
