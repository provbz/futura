<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormSelect2Field
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormSelect2Field extends FormField {

    public $values = [];
    public $multiple = false;
    public $taggable = false;

    public function  __construct($label, $name, $type, $values, $required = false, $defaultValue = NULL, $parameters = NULL) {
        $this->values = $values;
        $this->escapeHtmlOnPersist = false;
        parent::__construct($label, $name, $type, $required, $defaultValue, $parameters);
    }

    public function draw($entity = NULL) {
        $preparedValues = [];
        foreach ($this->values as $v => $text) {
            $entry = [];

            if (is_object($text) || is_array($text)){
                $entry = $text;
            } else {
                $entry = [
                    "text" => $text,
                    "value" => $v
                ];
            }

            $preparedValues[] = $entry;
        }

        $value = $this->getDecodedValue($entity);
        if (!is_array($value)){
            $value = [["value" => $value]];
        }

        for ($i = 0; $i < count($value); $i++) {
            if ($value[$i] === null){
                continue;
            }

            if (!is_array($value[$i]))    {
                $value[$i] = ["value" => $value[$i]];
            }

            foreach ($preparedValues as $entry) {
                if ($entry["value"] == $value[$i]["value"]){
                    $value[$i]["text"] = $entry["text"];
                }
            }
        }

        $options = [
            'inputName' => $this->name,
            'values' => $preparedValues,
            'value' => $value,
            'multiple' => $this->multiple,
            'taggable' => $this->taggable,
            "readOnly" => $this->readOnly
        ];

        ?>
        <div id="select2-<?= _t($this->name) ?>" class="select2-<?= _t($this->name) ?>"></div>
        <?= $this->drawErrorTag(); ?>
        <script type="text/javascript">
        MbCreateVue("mb-input-select-2", "select2-<?= $this->name ?>", <?= json_encode($options) ?>);
        </script>
        <?php
    }

    public function getValueForDb(){
        $value = $this->getDecodedValue(null);
        if ($this->multiple || !is_array($value)){
            return $value;
        } else if (count($value) > 0){
            return $value[0];
        }
    }

    public function check(){
        parent::check();
        $value = $this->getDecodedValue();
        if ($this->required){
            if ($value == null || $value[0] === null){
                $this->addFieldError("Dato non facoltativo");
            }
        }
    }
}