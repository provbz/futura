<?php
/*
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormCheckboxMultiField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormCheckboxMultiField extends FormField {
    
    public $values;
    public $options;
    
    public function  __construct($label, $name, $type, $values, $required = false, $defaultValue = NULL, $parameters = NULL) {
        $this->values = $values;
        $this->escapeHtmlOnPersist = false;
        parent::__construct($label, $name, $type, $required, $defaultValue, $parameters);
    }

    public function draw($entity = NULL) {
        $value = $this->getValue($entity);
        $data = [];
        if (is_array($value)){
            $data = $value;
        } else if (StringUtils::isJSON($value)){
            $data = json_decode($value);
        } else if (StringUtils::isNotBlank( $value )) {
            $data = [$value];    
        }

        ?>
        <div class="radio_list">
        <?php
        if (is_array($this->values)) {
            foreach ($this->values as $key => $value){ 
                $selected = "";
                if (is_array($data) && in_array($key, $data) !== FALSE){
                    $selected = "checked='checked'";
                }
                ?>
                <input type="checkbox" id="<?= $this->name.'_'.$key ?>" name="<?= $this->name ?>[]" value="<?= $key ?>" <?= $selected ?>>
                <label for="<?= $this->name.'_'.$key ?>"><?= _t($value) ?></label><br/>
            <?php } 
        } ?>
        <?php 
        if (is_array($this->options)) {
            foreach ($this->options as $option) { 
                $selected = "";
                if (is_array($data) && in_array($option['value'], $data) !== FALSE){
                    $selected = "checked='checked'";
                }
                ?>
                <input type="checkbox" id="<?= $this->name . '_' . $option['value'] ?>" name="<?= $this->name ?>[]" value="<?= $option['value'] ?>" <?= $selected ?>>
                <label for="<?= $this->name . '_' . $option['value'] ?>"><?= _t($option['label']) ?></label><br/>
            <?php } 
        }?>
        
        <?= $this->drawErrorTag() ?>
        </div>
        <?php
    }
    
    public function getValueForDb() {
        $value = parent::getValueForDb();
        return json_encode($value);
    }

    public function check(){
        parent::check();

        $value = $this->getValue(null);

        if ($this->required){
            if (is_array($value) && count($value) == 0){
                $this->addFieldError("Dato non facoltativo.");
            }
        }
    }
}
