<?php
/* 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormSelectField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormSelectField extends FormField{


    public $values;
    public $options;
    public $dictionaries;
    public $addEmpty;

    /**
     * 
     * @param type $label
     * @param type $name
     * @param type $type
     * @param array $values
     * @param type $required
     * @param type $defaultValue
     * @param type $parameters
     */
    public function  __construct($label, $name, $type, $values, $required = false, $defaultValue = NULL, $parameters = NULL) {
        $this->values = $values;
        parent::__construct($label, $name, $type, $required, $defaultValue, $parameters);
    }

    private function findKeyInValues($key){
        if ($this->dictionaries){
            foreach ($this->dictionaries as $d) {
                if ($d["dictionary_id"] == $key){
                    return $d["value"];
                }
            }
        } else if ($this->values != null){
            foreach ($this->values as $index => $value) {
                if ($value instanceof KeyValuePair && $value->key == $key){
                    return $value->value;
                } else if($index == $key){
                    return $value;
                }
            }
        }
        return null;
    }

    public function draw($entity = NULL, $disableDecoration = false) {
        $v = $this->getValue($entity);
        $out = "";

        $disabled = "";
        if ($this->readOnly){
            $disabled = "disabled";
        }
        
        ob_start();
        if ($this->readOnly){
        ?>
            <input type='hidden' name="<?= _t($this->name) ?>" id="<?= _t($this->name) ?>"" value="<?= $v ?>" />
        <?php } ?>
        <select 
            <?= $disabled ?>
            <?php 
            if (StringUtils::isNotBlank($this->name)){ ?>
                name="<?= _t($this->name) ?>" id="<?= _t($this->name) ?>"
            <?php
            }
            ?>
            <?= $this->prepareParameters() ?>
        >
        <?php
        if ($this->values != null && is_array($this->values)){
            foreach ($this->values as $key => $value){
                if ($value instanceof KeyValuePair){
                    $key = $value->key;
                    $value = $value->value;
                }
                
                $selected = "";
                if ($key == $v){
                    $selected = " selected";
                }

                ?>
                <option value="<?= htmlentities( $key, ENT_QUOTES ) ?>" <?= $selected ?>><?= _t($value) ?></option>
                <?php
            }
        } else if ($this->dictionaries != null){
            if ($this->addEmpty){
                ?>
                <option></option>
                <?php 
            }

            foreach ($this->dictionaries as $d) {
                $selected = "";
                if ($d["dictionary_id"] == $v){
                    $selected = " selected";
                }

                ?>
                <option value="<?= _t($d["dictionary_id"] ) ?>" <?= $selected ?>><?= _t($d["value"]) ?></option>
                <?php
            }
        }
        ?>
        </select>
        <?php
        $out = ob_get_clean();
        $out .= $this->drawErrorTag();
        
        echo $out;
    }

    public function check(){
        parent::check();
        $value = $this->getValue(null);

        if ($value == ""){
            return;
        }

        $key = $this->findKeyInValues($value);
        if ($key == null){
            $this->addFieldError("Valore non consentito.");
        }
    }
}
