<?php
/* 
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormDateField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormDateField extends FormField{

    public function __construct($label, $name, $type, $required = false, $defaultValue = NULL, $parameters = NULL) {
        parent::__construct($label, $name, $type, $required, $defaultValue, $parameters);
        $this->note = "gg/mm/aaaa";
    }
    
    public function  draw($entity = NULL) {
        $value = $this->getValue($entity);
        $value = DateUtils::getDate( TextService::formatDate( $value ) );

        $disabled = "";
        if ($this->readOnly){
            $disabled = "disabled";
        }
        ?>
        <div class="">
            <script type="text/javascript">
            $(function() {
                $('#<?= $this->name; ?>').datepicker( );
            });
            </script>
            <input type="text" name="<?= $this->name ?>" id="<?= _t($this->name) ?>" value="<?= _t($value) ?>" <?= $disabled ?>></input>
            <?= $this->drawErrorTag() ?>
        </div>
        <?php
    }
    
    public function check() {
        parent::check();
        
        $value = getRequestValue($this->name);
        if (StringUtils::isNotBlank($value)){
            if (preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $value) == 0){
                $this->addFieldError("Formato non valido.");
            }
        }
    }

    public function getValueForDb(){
        $value = getRequestValue($this->name);
        if (preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $value) == 0){
            return null;
        }
        
        return TextService::parseDate($value);
    }
}
