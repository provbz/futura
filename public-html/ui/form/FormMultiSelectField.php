<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormMultiSelectField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormMultiSelectField extends FormField{

    public $options;
    public $inputType = "checkbox";
    public $optionsWithTextArea;

    public function __construct($label, $name) {
        parent::__construct($label, $name, FormFieldType::STRING, false);
    }

    public function draw($entity = NULL) {
        $value = $this->getValue($entity);
        ?>
        <div id="<?= $this->name ?>" class="field-codes">
            <div class="<?= $this->name ?>-container"></div>
            
            <?= $this->drawErrorTag() ?>
        </div>
        <script type="text/javascript">
            <?php
            $parameters = [
                "inputName" => $this->name,
                "value" => $value && !StringUtils::isBlank($value)  ? $value : null,
                "readOnly" => $this->readOnly,
                "options" => $this->options,
                "inputType" => $this->inputType,
                'optionsWithTextArea' => $this->optionsWithTextArea
            ];
            ?>
            MbCreateVue('MbPeiModalitaVerificaField', '<?= $this->name ?>-container', <?= json_encode($parameters) ?>);
        </script>
        <?php
    }
    
    public function encodeValue($value){
        $value = json_encode($value);
        $value = htmlentities($value);
        return $value;
    }
    
    public function decodedValues($value){
        $val = urldecode($value);
        return json_decode($val);
    }

    public function getValueForDb(){
        $value = getRequestValue($this->name);
        return urldecode($value);
    }

}
