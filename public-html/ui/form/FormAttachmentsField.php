<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormAttachmentsField
 *
 * @author marco
 */
class FormAttachmentsField extends FormField{
    
    public $attachments;
    public $manAttachments = 0;
    public $noTemp = false;
    public $removeUrl;
    public $downloadUrl;
    public $entity;
    public $entityId;
    public $canRemove = true;
    public $extensions = [];
    public $maxSize = 10;
    
    public function __construct($label, $name, $entity, $entityId, $required = false ) {
        parent::__construct($label, $name, FormFieldType::STRING, $required);
        $this->persist = false;        
        $this->entity = $entity;
        $this->entityId = $entityId;
            
        $attachments = AttachmentService::findAllForEntityInMV($this->entity, $this->entityId, $this->name);
        foreach ($attachments as $attachment) {
            $attachment->FileName = str_replace("%22", "“", $attachment->FileName);
        }
        $this->defaultValue = $this->encodeValue($attachments);
    }

    public function draw($entity = NULL) {
        $value = $this->getValue($entity);

        ?>
        <div class="attachment-component <?= $this->readOnly ? 'read-only' : 'not-read-only' ?> attachment_<?= $this->name ?>">
            <input id="<?= $this->name ?>" name="<?= $this->name ?>" value="<?= $value ?>" type="hidden" />
            <div id="attachment-<?= $this->name ?>">
                <div v-if="attachments.length > 0" class="attachments-list">
                    <div v-for="(attachment, index) in attachments" class="attachment">
                        <a v-if="attachment.FileName && attachment.FileName.indexOf('.png') != -1 || attachment.FileName.indexOf('.jpg') != -1" class="image" v-bind:href="attachment.Url" target="_blank" v-bind:style="{ backgroundImage: 'url(' + attachment.Url + '&disposition=inline)'}"></a>
                        <a v-else class="file" v-bind:href="attachment.Url + '&disposition=inline'" target="_blank"><i class="fas fa-file"></i></a>

                        <div class="attachment-data">
                            <strong>
                                {{attachment.FileName}}
                            </strong>
                            
                            ({{attachment.Size | prettyBytes}})<br/>
                            {{attachment.insertDate | dateTime }}
                        </div>
                        <?php if (!$this->readOnly) { ?>
                            <div class="attachment-tools">
                                <a @click="onMoveLeft(index)" v-show="index > 0" class="attachment-move">
                                    <i class="fas fa-caret-left"></i>
                                </a>
                                <a @click="onMoveRight(index)" v-show="index < attachments.length - 1" class="attachment-move">
                                    <i class="fas fa-caret-right"></i>
                                </a>
                                <a v-if="canRemove" @click="removeAttachment(index)" class="attachment-remove">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                
                <?php if (!$this->readOnly) { ?>
                    <div v-show="maxAttachments == 0 || attachments.length < maxAttachments" id="upload-<?= $this->name ?>" method="post" enctype="multipart/form-data" class="box js" >
                        <div class="box__input">
                            <input type="file" name="file" class="box__file" :accept="accept"/>
                            <label for="file"><strong>Scegli un file</strong><span class="box__dragndrop"> o trascinalo qui.</span>.</label>
                            <div v-if="extensions && extensions.length > 0" class="allowed_extensions">
                                Tipi consentiti: 
                                <span v-for="extension in extensions">{{extension}}</span>
                            </div>
                            <div v-if="maxSize > 0" class="allowed_extensions">
                                Dimensione massima: {{maxSize}} MB
                            </div>
                            <button type="submit" class="box__button">Carica file</button>
                        </div>

                        <div class="box__uploading">Caricamento....</div>
                        <div class="box__success">Fatto! <a href="" class="box__restart" role="button">Altro da caricare?</a></div>
                        <div class="box__error">Errore! <span></span>. <a class="box__restart" role="button">Riprova!</a></div>
                    </div>
                <?php } ?>
                <?= $this->drawErrorTag() ?>
            </div>

            <script type="text/javascript">
                <?php
                $componentParameters = [
                    "selector" => $this->name,
                    "MaxAttachments" => $this->manAttachments,
                    "extensions" => $this->extensions,
                    "maxSize" => $this->maxSize,
                    "removeUrl" => $this->removeUrl,
                    "canRemove" => $this->canRemove,
                    "downloadUrl" => $this->downloadUrl,
                    "uploadUrl" => UriService::buildPageUrl("/public/api/file/UploadAction", "", [
                            "entity" => $this->entity,
                            "entityId" => $this->entityId,
                            "noTemp" => $this->noTemp,
                            "type" => $this->name,
                        ])
                ];
                ?>
                DEDFileUplad(<?= json_encode($componentParameters) ?>);
            </script>
        </div>
        <?php
    }
    
    function check() {
        $attachments = $this->getDecodedValues();
        if ($this->required && ($attachments == null || count($attachments) == 0)){
            $this->addFieldError("Dato non facoltativo.");
        }

        if ($this->checkFunction != NULL){
            $fn = $this->checkFunction;
            $fn($this, $attachments);
        }
    }

    public function encodeValue($value){
        $value = json_encode($value);
        $value = htmlentities($value);
        return $value;
    }
    
    public function getDecodedValues(){
        $val = $this->getValue(null);
        $val = urldecode($val);
        return json_decode($val);
    }
    
    public function persist($entityId){
        global $currentUser;
        $this->entityId = $entityId;
        
        $attachments = $this->getDecodedValues();
        $i = 0;

        EM::deleteEntity("attachment_entity", [
            "entity" => $this->entity,
            "entity_id" => $this->entityId, 
            "type" => $this->name
        ]);

        foreach($attachments as $attachmentMv){
            $attachment = AttachmentService::find($attachmentMv->AttachmentId);
            if ($attachment['status'] == AttachmentStatus::TEMP){
                $filePath = "/" . $this->entity . "/" . $this->entityId . "/" . $this->name;
                $fullPath = UriService::buildAttachmentPath() . $filePath;
                if (!file_exists($fullPath)){
                    mkdir($fullPath, 0777, true);
                }

                $tmpPath = UriService::buildAttachmentTmpPath() . "/" . $attachment['attachment_id'];
                rename($tmpPath, $fullPath . "/" . $attachment['filename']);

                EM::updateEntity("attachment", [
                    "status" => AttachmentStatus::READY,
                    "path" => $filePath
                    ], ["attachment_id" => $attachmentMv->AttachmentId]);
            }
            
            EM::insertEntity("attachment_entity", [
                "attachment_id" => $attachment['attachment_id'],
                "entity" => $this->entity,
                "entity_id" => $this->entityId,
                "type" => $this->name,
                "sort" => $i,
                "insert_date" => "NOW()",
                "insert_user_id" => $currentUser != null ? $currentUser['user_id'] : null
            ]);
            $i++;
        }
    }
}
