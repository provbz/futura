<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of FormTextareaField
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class FormTextareaField extends FormField{
    
    public $isWysiwyg = false;

    public function draw($entity = NULL){
        $addictionalClass = "";
        if ($this->isWysiwyg){
            $addictionalClass .= "wysiwyg summernote";
        }

        ?>
        <div class="">
            <textarea name="<?= _t($this->name) ?>" id="<?= _t($this->name) ?>"
                class="<?= $addictionalClass ?>"
                <?php
                if ($this->parameters == NULL || !isset ($this->parameters['rows'])){
                    echo 'rows="3" cols="80"';
                }?>
                <?= $this->readOnly ? 'disabled' : '' ?>
                <?= $this->prepareParameters(); ?>
                ><?= _t( $this->getValue($entity)) ?></textarea>

            <?= $this->drawErrorTag() ?>
        </div>
        <?php
    }
    
}