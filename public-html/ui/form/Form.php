<?php
/* 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of Form
 *
 * @author Marco Buccio <info@mbuccio.it>
 */

require_once(dirname(__FILE__) . '/FormField.php');
require_once(dirname(__FILE__) . '/FormFieldType.php');
require_once(dirname(__FILE__) . '/FormTextField.php');
require_once(dirname(__FILE__) . '/FormPasswordField.php');
require_once(dirname(__FILE__) . '/FormSinglePasswordField.php');
require_once(dirname(__FILE__) . '/FormHiddenField.php');
require_once(dirname(__FILE__) . '/FormSelectField.php');
require_once(dirname(__FILE__) . '/FormDateField.php');
require_once(dirname(__FILE__) . '/FormDateMultipleField.php');
require_once(dirname(__FILE__) . '/FormTextareaField.php');
require_once(dirname(__FILE__) . '/FormCheckField.php');
require_once(dirname(__FILE__) . '/FormPresentationField.php');
require_once(dirname(__FILE__) . '/FormHtmlField.php');
require_once(dirname(__FILE__) . '/FormSliderField.php');
require_once(dirname(__FILE__) . '/FormCheckboxMultiField.php');
require_once(dirname(__FILE__) . '/FormRadioField.php');
require_once(dirname(__FILE__) . '/FormContainerField.php');
require_once(dirname(__FILE__) . '/FormRecaptchaField.php');
require_once(dirname(__FILE__) . '/FormAttachmentsField.php');
require_once(__DIR__ . '/FormMultiSelectField.php');

class Form {

    public $id = "form_generic";
    public $action;
    public $async;
    public $formStyleClass = "default_form";
    public $entityName;
    public $primaryKey = "id";
    public $entity;
    public $parameters;
    public $submitButtonLabel = "Salva";
    public $submitAction;
    public $fields;
    public $errors = [];
    public $displaySubmit = true;
    public $cancellAction = '';
    public $additionalButtons;
    public $checkFunction = null;
    public $fieldErrors = false;

    public function __construct($action, $parameters = NULL, $fields = NULL){
        $this->action = $action;
        $this->parameters = $parameters;
        $this->async = false;

        if ($fields != NULL){
            $this->fields = $fields;
        } else {
            $this->fields = array();
        }
    }

    public function addField($field){
        $field->form = $this;
        array_push($this->fields, $field);
    }

    public function draw(){ ?>
        <div id="<?= $this->id; ?>_container">
        <form id="<?= $this->id; ?>" method="post" action="<?= $this->action ?>" class="<?= $this->formStyleClass; ?>" <?= $this->prepareParameters(); ?>>
            
        <?php
        if ($this->errors != NULL && is_array($this->errors)){
            echo '<div class="error" style="display:block;margin-bottom: 20px;color: red;">';
            foreach($this->errors as $key => $value){
                echo $value.'<br/>';
            }
            echo '</div>';
        }
        
        if ($this->fields != NULL){
            foreach ($this->fields as $key => $value){
                if ($value instanceof FormHiddenField || $value instanceof FormPresentationField){
                    $value->draw($this->entity);
                } else {
                    $class = '';
                    if ($value->hasErrors()){
                        $class = 'error';
                    }
                    if (StringUtils::isNotBlank($value->blockAdditionalClass)){
                        $class .= ' ' .$value->blockAdditionalClass;
                    }
                    ?>
                    <?php if ($value->displayFormRow) { ?>
                    <div class="row-100 form-row <?= $class ?>" id="campo_form_<?= $value->name ?>" style="<?= $value->blockStyle ?>">
                    <?php } ?>
                        <div class="<?= $value->labelClass ?>">
                        <?php
                        if (StringUtils::isNotBlank($value->label)){
                            $labelClass = 'inline codice '.$class;
                            if ($value->labelBold){
                                $labelClass .= ' bold';
                            }?>
                            <label class="<?= $labelClass ?>" for="<?= $value->name ?>">
                            <?php if ($value->required){ ?>
                                <small>*</small>
                            <?php } ?>
                            <?= ((string)($value->label)) ?>
                            </label>
                        <?php }?>
                        </div>
                        <div class="<?= $value->itemClass ?>">
                            <?php
                            if ($value instanceof FormField){
                                $value->draw($this->entity);
                            }
                            ?>
                            
                            <div class="form_field_note" id="<?= $value->name ?>_note">
                            <?php
                            if (StringUtils::isNotBlank($value->note)){
                                echo '<span class="inline codice">';
                                echo $value->note;
                                echo '</span>';
                            }
                            ?>
                            </div>
                        </div>
                    <?php if ($value->displayFormRow) { ?>
                    </div>
                    <?php } ?>
                    <?php
                }
            }
        }
        
        if (StringUtils::isBlank($this->submitAction)){
            $this->submitAction = "javascript:submit('#". $this->id ."');";
        }
                ?>
                
                <input type="hidden" name="submit_check" value="submit_check"/>
                <?php if($this->displaySubmit){ ?>
                <div class="row-100 campiObbligatori">
                    <div class="large-3 medium-3 columns">
                        <p>*campi obbligatori</p>
                    </div>
                    <div class="large-5 medium-5 end columns">
                        <a href="<?= $this->submitAction ?>" class="button radius salva"><i class="far fa-save margin-right"></i><?= $this->submitButtonLabel ?></a>
                        <?php if (StringUtils::isNotBlank( $this->cancellAction )){ ?>
                        <a href="<?= $this->cancellAction ?>" class="button radius alert"><i class="fa fa-undo margin-right"></i>Annulla</a>
                        <?php } ?>
                        <?= $this->additionalButtons ?>
                    </div>
                    <script>
                        function resetFormSubmit(formId){
                            $(formId).find('.salva').removeClass('disabled');
                            $(formId).find('.salva').html('<i class="far fa-save margin-right"></i>Salva');
                        }

                        $(document).ready(function(){
                            $('.salva').click(function(e){
                                if ($(this).hasClass('disabled')){
                                    e.preventDefault();
                                    return false;
                                }

                                $(this).addClass('disabled');
                                $(this).html('<i class="fas fa-spinner fa-spin margin-right"></i>Attendere...');
                            });
                        });
                    </script>
                </div>
                <?php } ?>
            </form>
        </div>
        <?php if ($this->async){ ?>
            <script type="text/javascript">
                var formManager = new FormManager('#<?= $this->id; ?>', '');
            </script>   
        <?php }
    }
    
    public function isSubmit(){
        if (getRequestValue("submit_check", NULL) != NULL){
            return true;
        }
        return false;
    }
    
    public function prepareParameters(){
        $out = "";
        if ($this->parameters != null){
            foreach($this->parameters as $key => $value){
                $out .= " ".$key."=\"".$value."\"";
            }
        }
        return $out;
    }

    public function  checkFields(){
        $this->fieldErrors = false;
        if ($this->fields != null && is_array($this->fields)){
            foreach($this->fields as $key => $value){
                if ($value->readOnly || ($value->parameters != null && 
                        isset($value->parameters['disabled']) && 
                        $value->parameters['disabled'] == 'true')){
                    continue;
                }
                
                $value->check();
                if ($value->hasErrors()){
                    $this->fieldErrors = true;
                }
            }
        }

        if ($this->checkFunction != null){
            $fn = $this->checkFunction;
            $fn($this);
        }

        return $this->isValid();
    }

    public function isValid(){
        return !$this->fieldErrors && count($this->errors) == 0;
    }

    public function findFieldValue($name){
        if (StringUtils::isBlank($name)){
            return NULL;;
        }
        
        $field = $this->getFieldWithName($name);
        if ($field == NULL){
            return NULL;
        }
        return getRequestValue($field->name);
    }
    
    public function getFieldWithName($name){
        if ($this->fields != null && is_array($this->fields)){
            foreach($this->fields as $key => $value){
                if ($value->name == $name){
                    return $value;
                }
            }
        }
        return NULL;
    }
    
    public function getEntity(){
        $entity = [];
        
        foreach($this->fields as $key => $value){
            if (!$value->persist || $value->readOnly    ){
                continue;
            }
            if (StringUtils::isBlank( $value->name )){
                continue;
            }

            if ($value->parameters != null && isset($value->parameters['disabled']) && $value->parameters['disabled'] == 'true'){
                continue;
            }

            $v = $value->getValueForDb();
            $entity[$value->name] = $v;
        }
        
        return $entity;
    }

    public function getFieldsValues(){
        $entity = [];
        if ($this->fields == null || !is_array($this->fields)){
            return $entity;
        }

        foreach($this->fields as $key => $value){
            if (!$value->persist || $value->readOnly){
                continue;
            }
            if (StringUtils::isBlank($value->name)){
                continue;
            }
            
            if ($value->parameters != null && isset($value->parameters['disabled']) && $value->parameters['disabled'] == 'true'){
                continue;
            }
            
            $entity[$value->name] = $value->getValueForDb();
        }

        return $entity;
    }
    
    public function persist($update = NULL, $additionalFields = NULL, $fieldsToEnchrypt = NULL){
        $query = "";
        if ($update == NULL){
            $query = "INSERT INTO ";
        } else {
            $query = "UPDATE ";
        }
        $query .= $this->entityName." SET ";
        $par = [];

        $fields = "";
        if ($this->fields != null && is_array($this->fields)){
            foreach($this->fields as $key => $value){
                if (!$value->persist || $value->readOnly){
                    continue;
                }
                if (StringUtils::isBlank($value->name)){
                    continue;
                }
                
                if ($value->parameters != null && isset($value->parameters['disabled']) && $value->parameters['disabled'] == 'true'){
                    continue;
                }
                
                $v = $value->getValueForDb();
                
                if ($value->type == FormFieldType::DATE || $value instanceof FormDateField){
                    if ($v != "NOW()"){
                        $v = TextService::parseDate($v);
                    }
                    
                    if (StringUtils::isNotBlank($v)){
                        $fields .= ($fields == "") ? "" : ", ";
                        $fields .= "`" . $value->name ."` = :".$value->name;
                        $par[$value->name] = htmlspecialchars($v);
                    }
                    continue;
                }
                
                if ($value instanceof FormPasswordField){
                    $v = $value->encodeFunction();
                    if($v == ""){
                        continue;
                    }
                }

                if ($value->type == FormFieldType::NUMBER){
                    if (StringUtils::isBlank($v)){
                        continue;
                    }
                }
                
                $fields .= ($fields == "") ? "" : ", ";
                $fields .= "`" . $value->name ."` = :$value->name";
                if ($value->type == FormFieldType::NUMBER){
                    $par[$value->name] = $v;
                } else {
                    if (!$value->escapeHtmlOnPersist){
                        $par[$value->name] = $v;
                    } else {
                        $par[$value->name] = $v;
                    }
                    
                    if ($value->isEnchripted){
                        $par[$value->name] = EncryptService::encrypt($par[$value->name]);
                    }
                }
            }
            $query .= $fields;
        }
        
        if ($additionalFields != null && is_array($additionalFields)){
            $set = "";
            foreach($additionalFields as $key => $value){
                $set .= $set == "" ? "": ", ";
                if (is_string($value) && ($value == "NOW()" || strpos($value, "INTERVAL") !== false)){
                    $set .= sprintf("`%s`=%s", $key, $value);
                } else {
                    $set .= sprintf("%s=:%s", $key, $key);
                    
                    if (is_object ($value) ){
                        $set .= sprintf("`%s`=:%s", $key, $key);
                        $par[$key] = json_encode( $value );
                    } else {
                        $par[$key] = $value;
                    }
                }
            }
            
            $query .= $fields == "" ? "" : ", ";
            $query .= " " . $set;
        }

        if ($update instanceof TableCondition){
            $query .= " WHERE " . $update->condition;
            $par[$update->parameterName] = $update->parameterValue;
        }
        else if (StringUtils::isNotBlank($update)){
            $query .= " WHERE ".$update;
        }
        
        $stmt = EM::prepare($query);
        $stmt->execute($par);
        $id = EM::$entityManager->db->lastInsertId();
        
        return $id;
    }
}
