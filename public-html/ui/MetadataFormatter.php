<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MetadataFormatter
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class MetadataFormatter {
    
    public static function buildFormFor($entity, $id, $type = ""){
        $fields = array();
        $metadata = array();
        
        if (StringUtils::isNotBlank($id)){
            $metadata = MetadataService::metadataObject($entity, $id);
        }
        $metaKeys = MetadataService::findKeys($entity, $type);
        
        while ($mdk = mysql_fetch_assoc($metaKeys)){
            $label = ucfirst(strtolower($mdk['label']));
            $value = "";
            if ($metadata != NULL && isset($metadata[$mdk['metadata_key_id']])){
                $value = $metadata[$mdk['metadata_key_id']];
            }
            
            $field = NULL;
            
            if ($mdk['ui_type'] == 'FormSelectField' || $mdk['ui_type'] == 'FormRadioField'){
                $values = array();
                
                if ($mdk["add_empty_value"] == 1){
                    $values [""] = "";
                }
                
                if ($mdk['category_key'] == 'subprocess'){
                    $out = array();
                    $result = SubprocessService::findAll();
                    while ($row = mysql_fetch_array($result)){
                        $str = SubprocessService::formatSubprocess($row);
                        $out[$row['subprocess_code']] = $str . ' ('.$row['subprocess_code'].')';
                    }
                    mysql_free_result($result);
                    $values += $out;
                } else {
                    $values += CategoryService::findObject($mdk['category_key']);
                }
                
                $field = new $mdk['ui_type'](
                                $label,
                                $mdk['metadata_key_id'], 
                                FormFieldType::STRING, 
                                $values,
                                $mdk["required"],
                                $value);
                
            } else {
                 $field = new $mdk['ui_type']($label, $mdk['metadata_key_id'], FormFieldType::STRING, $mdk['required'], $value );
            }
            
            if ($field != NULL){
                if (StringUtils::isNotBlank($mdk['description'])){
                    $field->note = $mdk['description'];
                }
                
                if ($mdk['editable'] == 0){
                    $field->readOnly = true;
                }
                
                $field->persist = false;
                $fields[] = $field;
            }
        }
        mysql_free_result($metaKeys);
        
        return $fields;
    }
    
    public static function persist($entity, $id, $form, $type = "", $customPeristFunction = NULL){
        $metaKeys = MetadataService::findKeys($entity, $type);
        while ($mdk = mysql_fetch_assoc($metaKeys)){
            $metadataKey = $mdk['metadata_key_id'];
            $value = $form->findFieldValue($mdk['metadata_key_id']);
            if($customPeristFunction == NULL || $customPeristFunction($metadataKey, $value)){
                MetadataService::persist($entity, $id, $metadataKey, $value);    
            }
        }
    }
    
    public static function draw($entity, $id, $type = ""){
        $metadata = MetadataService::metadataObject($entity, $id);
        $metaKeys = MetadataService::findKeys($entity, $type);
        $result = array();
        
        while ($row = mysql_fetch_assoc($metaKeys)){
            $label = ucfirst(strtolower($row['label']));
            $value = "";
            if ($metadata != NULL && isset($metadata[$row['metadata_key_id']])){
                $value = $metadata[$row['metadata_key_id']];
            }
            if ($row['ui_type'] == 'FormSelectField'){
                $values = array();
                $values = CategoryService::find($row['category_key']);
                while($rowValues = mysql_fetch_assoc( $values)){
                    if($rowValues['key'] == $value){
                        $value = $rowValues['value'];
                    }
                }
            } else {
                if ($metadata != NULL && isset($metadata[$row['metadata_key_id']])){
                    $value = $metadata[$row['metadata_key_id']];
                }
            }
            
            $result[$label] = $value;
        }
        return $result;
    }
    
}

?>
