<?php
/* 
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of Page
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
        
class Page extends ApiBaseAction {
    
    public $currentUser = NULL;
    public $additionalScripts;
    public $additionalCSS;
    
    public $bradcrumbs;
    public $menu;
    public $async = false;
    
    public $backAction;
    
    public $errors;
    
    public $pageTitle = "ePlanning";
    
    public function _prepare(){
        $this->bradcrumbs = new Bradcrumbs();
        $this->menu = new Menu();
        
        if ($this->canDisplayMenu()){
            $this->prepareMenu();
        }
        
        $this->additionalScripts = [];
        $this->additionalCSS = [];
        
        $this->addScriptFile("jquery-1.10.2.min.js");
        $this->addScriptFile("jquery-ui-1.10.2.custom.min.js");
        $this->addStyleFile("custom-theme/jquery-ui-1.10.4.custom.css");
        
        $this->addScriptFile("utils.js");
        $this->addScriptFile("jquery.placeholder.min.js");
       
        $this->addScriptFile("jquery.autosize.min.js");
        
        $this->addScriptFile("cookie/js/privacy.js");
        $this->addStyleFile("cookie/css/privacy.css");
        
        $this->addScriptFile("foundation.min.js");
        $this->addScriptFile("vendor/modernizr.js");
        $this->addScriptFile("mb/VueComponents.js");
        
        $this->addStyleFile("foundation.css");
        $this->addStyleFile("responsive.css");
        $this->addStyleFile("style.css");
        $this->addStyleFile("developer.css");
    }
    
    private function canDisplayMenu(){
        $url = $_SERVER['REQUEST_URI'];
        if (strpos($url, "/user/ChangePasswordAction") !== false){
            return false;
        }
        return true;
    }
    
    protected function prepareMenu(){
        
    }

    protected function structureMenu(){
        $structureId = getSessionValue(GlobalSessionData::SELECTED_STRUCTURE_ID);
        $structure = StructureService::find($structureId);
        if ($structure == null){
            return;
        }

        StructureService::addStructurePermissions();

        $this->menu->addMenuItem(new MenuItem("<i class=\"fas fa-home\"></i>", UriService::buildPageUrl("/structure/HomeAction")));
        $this->menu->addMenuItem(new MenuItem("PEI", UriService::buildPageUrl("/structure/PEIAction")));
        if ($structure["is_scuola_professionale"] == 1){
            $this->menu->addMenuItem(new MenuItem("PEI Tirocinio", UriService::buildPageUrl("/structure/PEITirocinioAction")));
        }

        $this->menu->addMenuItem(new MenuItem("PDP", UriService::buildPageUrl("/structure/PDPAction")));
        $this->menu->addMenuItem(new MenuItem("PDP bis. ling.", UriService::buildPageUrl("/structure/PDPLinguistocoAction")));
        
        $this->menu->addMenuItem(new MenuItem("Tassonomia", UriService::buildPageUrl("/taxonomy/PeiAxisAction")));

        if (SomministrazioneService::isStructureEnabled($structure)){
            $this->menu->addMenuItem(new MenuItem("Mondo delle Parole e Progetto Letto-Scrittura", UriService::buildPageUrl("/structure/test/SomministrazioniAction")));
        }

        if (StructureService::hasUserStructureRole($this->currentUser['user_id'], $structureId, UserRole::INSEGNANTE_ANNO_DI_PROVA)){
            $this->menu->addMenuItem(new MenuItem("Portfolio", UriService::buildPageUrl("/structure/portfolio/PortfolioAction")));
        }
        
        if (RilevazioneCattedreService::isStructureEnabled($structure)){
            $this->menu->addMenuItem(new MenuItem("Rilevazione cattedre A023ter", UriService::buildPageUrl("/structure/rilevazione_cattedre/RilevazioneCattedreTableAction")));
        }

        if (EModelService::isStructureEnabled($structure)){
            $this->menu->addMenuItem(new MenuItem("Modello E", UriService::buildPageUrl("/structure/eModel/EmodelAction")));
        }

        if (DropOutService::isStructureEnabled($structure)){
            $this->menu->addMenuItem(new MenuItem("Drop out", UriService::buildPageUrl("/structure/dropOut/DropOutUsersAction")));
        }

        $educazioneSaluteEnabled = PropertyService::find(PropertyNamespace::EDUCAZIONE_SALUTE, PropertyKey::ENABLED);
        if ($educazioneSaluteEnabled['value']){
            $this->menu->addMenuItem(new MenuItem("Ed. Salute", UriService::buildPageUrl("/structure/edSalute/EdSaluteAction")));
        }

        $this->menu->addMenuItem(new MenuItem("Serv. valutazione", UriService::buildPageUrl("/structure/servizio_valutazione/HomeAction")));
        $this->menu->addMenuItem(new MenuItem("Piano Inclusione", UriService::buildPageUrl("/structure/patto_inclusione/PattoInclusioneAction")));
        
        $this->menu->addMenuItem(new MenuItem("Trasferimenti", UriService::buildPageUrl("/structure/UserTransferRequestAction")));
        $this->menu->addMenuItem(new MenuItem("Plessi", UriService::buildPageUrl("/structure/SubStructureAction")));
        $this->menu->addMenuItem(new MenuItem("Utenti", UriService::buildPageUrl("/structure/UserRefAction")));
        $this->menu->addMenuItem(new MenuItem("Referenti", UriService::buildPageUrl("/structure/referent/ReferentAction")));
        $this->menu->addMenuItem(new MenuItem("Documenti", UriService::buildPageUrl("/user/IndicazioniProvincialeAction")));
    }
    
    public function addScriptFile($fileName){
        array_push($this->additionalScripts, $fileName);
    }
    
    public function addStyleFile($fileName){
        array_push($this->additionalCSS, $fileName);
    }
    
    public function actionUrl(string $method= "", $parameters = NULL){
        global $requestAction, $requestPackage, $requestMethod;
        if (StringUtils::isBlank($method)){
            $method = $requestMethod;
        }
        return UriService::buildPageUrl($requestPackage.'/'.$requestAction, $method, $parameters);
    }
    
    public function addMessage($msg){
        $sm = "";
        if (isset ($_SESSION[SESSION_REDIRECT_MESSAGE])){
            $sm = $_SESSION[SESSION_REDIRECT_MESSAGE].'<br/>';
        }
        $_SESSION[SESSION_REDIRECT_MESSAGE] = $sm.$msg;
    }
    
    public function getParameters(){
        $par = array();
        foreach($this as $var => $value) {
            if (is_string($value)){
                $par[$var] = $value;
            }
        }
        return $par;
    }
    
    public function setGlobalSessionValue($key, $value){
        $_SESSION[$key] = $value;
    }
    
    public function getGlobalSessionValue($key){
        if (!isset($_SESSION[$key])){
            return NULL;
        }
        return $_SESSION[$key];
    }
    
    public function getSessionValue($key, $default = NULL){
        if (isset($_SESSION[get_class($this) . '_' . $key])){
            return $_SESSION[get_class($this) . '_' . $key];
        }
        return $default;
    }
    
    public function setSessionValue($key, $value){
        $_SESSION[get_class($this) . '_' . $key] = $value;
    }
    
    public function removeSessionValue($key){
        unset($_SESSION[get_class($this) . '_' . $key]);
    }
    
    public function header($displayPageHeader = true, $angularApp = NULL){
        require_once './template/Header.php';
    }
    
    public function footer($displayFooter = true){
        require_once './template/Footer.php';
    }
    
    public function getBaseAction(){
        global $currentUserRoles;
        foreach ($currentUserRoles as $role) {
            if (StringUtils::isNotBlank($role['home_action'])){
                return $role['home_action'];
            }
        }
        return "";
    }

    protected function getPostBody(){
        return file_get_contents('php://input');
    }

    protected function getPostBodyAsArray(){
        return json_decode($this->getPostBody(), true);
    }

    protected function getPostBodyAsObject(){
        return json_decode($this->getPostBody());
    }
}
