<?php
/* 
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of Formatter
 *
 * @author Marco Buccio <info@mbuccio.it>
 */
class Formatter {

    public static function tabbedPanel($idPrefix, $values){
        $divContentId = $idPrefix.'_content';

        echo '<div id="'.$idPrefix.'" class="TabbedPanels">';
            echo '<ul class="TabbedPanelsTabGroup">';
            $i = 0;
            $firstValue = "";
            foreach ($values as $key => $value){
                if ($firstValue == ""){
                    $firstValue = $value;
                }
                echo '<li id="'.$idPrefix.'_'.$i.'" class="TabbedPanelsTab" onclick="return openTab(\''.$idPrefix.'\',\''.$i.'\', \'#'.$divContentId.'\',\''.$value.'\');">';
                echo $key;
                echo '</li> ';
                $i++;
            }
            echo '</ul>';

            ?>
            <script type="text/javascript">
            $(document).ready(
                function(){
                    openTab('<?php echo $idPrefix; ?>','0', '#<?php echo $divContentId; ?>', '<?php echo $firstValue; ?>');
                }
            );
            </script>
            <?php

            echo '<div id="'.$divContentId.'" class="TabbedPanelsContentGroup">';
            echo '</div>';
         echo '</div>';
    }

    private static function writeTool($tool){
        return '<a href="javascript:void(0);" onclick="return '.$tool->onClick.';">'.$tool->label.'</a> | ';
    }

    public static function inputText($name, $parameters = NULL, $printOutput = true){
        $out = '<input type="text" name="'.$name.'" id="'.$name.'"';
        if ($parameters != null){
            foreach($parameters as $key => $value){
                $out .= " ".$key."=\"".$value."\"";
            }
        }
        $value = getRequestValue($name);
        $out .= ' value="'.$value.'"></input>';

        if ($printOutput){
            echo $out;
        }
        return $out;
    }

    public static function textarea($name, $parameters){
        echo '<textarea name="'.$name.'" id="'.$name.'"';
        foreach($parameters as $key => $value){
            echo " ".$key."=\"".$value."\"";
        }
        $value = getRequestValue($name);
        echo '>'.$value.'</textarea>';
    }
    
    public static function buildTool($label, $url, $icon = NULL, $confirm=NULL, $parameters = NULL ){
        if ($url != "javascript:void(0);" && !UserRoleService::canCurrentUserDo($url)){
            return;
        }
        
        if (StringUtils::isNotBlank($url) && $url != "javascript:void(0);" && !UserRoleService::canCurrentUserDo($url)){
            return;
        }
        
        $out = '<a class="button tiny secondary radius" ';
        if (StringUtils::isNotBlank($confirm)){
            $out .= ' href="javascript:void(0);" onclick="return doConfirm(\''.$confirm.'\', \''.$url.'\')" ';
        } else {
            $out .= ' href="'.$url.'" ';
        }
        if (is_array($parameters)){
            foreach($parameters as $key => $value){
                $out .= ' '.$key.'="'.$value.'" ';
            }
        }
        $out .= '>';
        if (StringUtils::isNotBlank( $icon )){
            $out .= '<i class="'.$icon.'"></i>';
        }
        $out .= ' '.$label.'</a>';
        return $out;
    }
    
    public static function progressBar($value, $total){
        if ($total <= 0){
            $perc = 0;
        } else {
            $perc = $value * 100 / $total;
        }
        
        $out = '<div id="progressbar" class="ui-progressbar ui-widget ui-widget-content ui-corner-all" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="37">';
        $out .=    '<div class="ui-progressbar-value ui-widget-header ui-corner-left" style="width: '.$perc.'%;"></div>';
        $out .= '</div>';
        return $out;
    }
    
    public static function buildSelect($name, $values, $defaultValue = ""){
        $out = "<select name='" . _t($name) ."' id='" . _t($name) . "'>";
        
        $selectedValue = $defaultValue;
        $req = getRequestValue($name);
        if (StringUtils::isNotBlank($req)){
            $selectedValue = $req;
        }
        
        foreach ($values as $key => $value){
            $out .= '<option value="' . _t($key) .'" ';
            if ($key == $selectedValue){
                $out .= " selected";
            }
            $out .= ">" . _t($value) . "</option>";
        }
        $out .= "</select>";
        return $out;
    }
    
    public static function formatUserLink($userId, $label=""){
        if (StringUtils::isBlank($label)){
            $user = UserService::find($userId);
            $label = $user['username'];
        }
        $url = UriService::buildPageUrl("/user/UserProfileAction", "", array("userId"=>$userId));
        
        return '<a href="'.$url.'">'.$label.'</a>';
    }
    
    public static function formatTime($v){
        $int = intval($v);
        $sec = intval($int % 60);
        if ($sec < 10){
            $sec = '0'.$sec;
        }
        $min = intval($int % 3600 / 60);
        if ($min < 10){
            $min = '0'.$min;
        }
        $hr = intval($int / 3600);
        if ($hr < 10){
            $hr = '0'.$hr;
        }

        return $hr.':'.$min.':'.$sec;
    }
    
}
