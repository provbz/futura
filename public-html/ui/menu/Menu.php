<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of Menu
 *
 * @author giorgia.Marco Buccio <info@mbuccio.it>lini
 */

require_once(dirname(__FILE__) . '/MenuItem.php');

class Menu {
    //put your code here
    
    public $menuItemArray;
    
    public function addMenuItem($mi){
        if ($this->menuItemArray == NULL){
            $this->menuItemArray = array();
        }
        array_push($this->menuItemArray, $mi);
    }
    
    public function removeItem($label){
        $arr = array();
        foreach ($this->menuItemArray as $key => $value){
            if ($value->label != $label){
                $arr[$key] = $value;
            }
        }
        $this->menuItemArray = $arr;
    }
    
    public function drawFirstLevel(){
        $out = '';
        if ($this->menuItemArray == NULL){
            return $out;
        }
        
        $selectedItem = getSessionValue("selected_menu_item");
        echo '<ul class="tabs" data-tab>';
        foreach($this->menuItemArray as $value){
            if (UserRoleService::canCurrentUserDo($value->link) ){
                $active = $selectedItem == $value->link ? 'active' : '';
                ?>
                <li class="tab-title <?= $active ?>" go-to-page="true">
                    <a href="<?= $value->link ?>"><?= ($value->label) ?></a>
                </li>    
                <?php
            }
        }
        
        echo '</ul>';
    }
    
    public function setSelectedMenuItem($str){
        $_SESSION['selected_menu_item'] = $str;
    }
    
    public function drawSubMenu($label){
        $node = $this->findNode($label);
        $out = $this->drawSubMenuFromNode($node);
        return $out;
    }
    
    /**
     * 
     * @param MenuItem[] $node
     * @return type
     */
    private function prepareSubnodesWithVisibility($menuItems){
        $resItems = [];
        foreach ($menuItems as $k => $menuItem){
            if (StringUtils::isBlank($menuItem->link) && is_array($menuItem->menuItemArray)){
                $subNodes = $this->prepareSubnodesWithVisibility($menuItem->menuItemArray);
                if (count($subNodes) > 0){
                    $resItems[] = $menuItem;
                }
            } else if (UserRoleService::canCurrentUserDo($menuItem->link)){
                $resItems[] = $menuItem;
            }
        }
                
        return $resItems;
    }
    
    private function drawRecursiveSubMenu($menuItems){
        $out = '<ul class="sub_menu">';
        foreach ($menuItems as $k => $subNode){
            $out .= '<li>';
            if (StringUtils::isBlank($subNode->link)){
                $out .= $subNode->label;
            } else {
                $out .= '<a href="' . $subNode->link . '">' . $subNode->label . '</a>';
            }
            
            if (is_array($subNode->menuItemArray)){
                $out .= $this->drawSubMenuFromNode($subNode);
            }
            
            $out .= '</li>';
        }
        $out .= '</ul>';
        return $out;
    }
    
    /**
     * 
     * @param MenuItem $node
     * @return string
     */
    public function drawSubMenuFromNode($node){
        if ($node == null){
            return "";
        }
        
        $menuItems = $this->prepareSubnodesWithVisibility($node->menuItemArray);
        return $this->drawRecursiveSubMenu($menuItems);
    }
        
    /**
     * 
     * @param type $label
     * @return MenuItem
     */
    public function findNode($label){
        if (!is_array($this->menuItemArray)){
            return;
        }
        
        foreach ($this->menuItemArray as $key => $node){
            if ($node->label == $label){
                return $node;
            }
        }
    }
    
    public function draw($firstBradcrumb){
        global $requestAction, $requestPackage;
        
        $out = "";
        
        $uri = UriService::buildPageUrl($requestPackage.'/'.$requestAction);
        $selectedMenuItemIndex = -1;
        if ($this->menuItemArray == NULL){
            return $out;
        }
        
        $out .= '<ul class="menu">';
        foreach($this->menuItemArray as $value){
            if ($value instanceof MenuItem){
              if($uri == $value->link){
                  if($value->menuItemArray == null){
                      $out .= $this->writeMenuItem($value, true, false);
                  } else {
                      $selectedMenuItemIndex = array_search($value, $this->menuItemArray);
                      $out .= $this->writeMenuItem($value, true, false);
                  }
              } else {
                 if($value->menuItemArray!=null){
                     $valueIsSelected = false;
                     foreach($value->menuItemArray as $subValue){
                        if ($subValue instanceof MenuItem){
                            if($uri == $subValue->link || ($firstBradcrumb!=null && $firstBradcrumb->link == $subValue->link)){
                                $out .= $this->writeMenuItem($value, true, false);
                                $selectedMenuItemIndex = array_search($value, $this->menuItemArray);
                                $valueIsSelected = true;
                            }
                        }
                    }

                    if(!$valueIsSelected){
                        $out .= $this->writeMenuItem($value, false, true);
                    }
                 }else{
                     if($firstBradcrumb!=null && $firstBradcrumb->link == $value->link){
                         $out .= $this->writeMenuItem($value, true, false);
                     }else{
                         $out .= $this->writeMenuItem($value, false, true);
                     }
                 }
              }
            }
        }
        $out .= "</ul>";

        if($selectedMenuItemIndex>-1){
            $value = $this->menuItemArray[$selectedMenuItemIndex];
            $out .= '<ul class="sottomenu">';
            foreach($value->menuItemArray as $subValue){
                if ($subValue instanceof MenuItem){
                    if($uri == $subValue->link || ($firstBradcrumb!=null && $firstBradcrumb->link == $subValue->link)){
                        $out .= $this->writeMenuItem($subValue, true, false);
                    }else{
                        $out .= $this->writeMenuItem($subValue, false, true);
                    }
                }
            }
            $out .= "</ul>";
        }
   
        echo $out;
    }
    
    private function writeMenuItem($item, $isSelected, $allowLink){
        global $app_name;
        
        $itemRow = "";
        
        $action = str_replace($app_name, "", $item->link);
        if (!UserRoleService::canCurrentUserDo($action)){
            return "";
        }
        
        if($item->isImage){
            if($isSelected){
                 $imageUrl = UriService::buildImageUrl("selected_".$item->label);
            }else{
                 $imageUrl = UriService::buildImageUrl($item->label);
            }
            $itemRow .= '<li class="menu_item"><a';
            if($allowLink){
                $itemRow .= " href=".$item->link;
            }
            $itemRow .= "><img src=".$imageUrl;
            if($item->imageClass){
                $itemRow .= " class=".$item->imageClass." ";
            }
            $itemRow .= "/></a></li>";
        }else{
            $itemRow .= "<li";
            if($isSelected){
                 $itemRow .= " class = 'menu_item selected'><a class = 'selected'";
            }else{
                 $itemRow .= " class='menu_item'><a";
            }
            if($allowLink){
                $itemRow .= " href=".$item->link;
            }
            $itemRow .= ">".$item->label."</a></li>";
        }
        return $itemRow;
    }
}
