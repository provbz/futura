<?php

/*
 
 ----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 
 */

/**
 * Description of MenuItem
 *
 * @author giorgia.Marco Buccio <info@mbuccio.it>lini
 */
class MenuItem {
    //put your code here
    public $label;
    public $link;
    public $menuItemArray;
    public $isImage;
    public $imageClass;

    public function __construct($label, $link=null, $isImage = false, $imageClass = null ) {
        $this->label = $label;
        $this->link = $link;
        $this->isImage = $isImage;
        $this->imageClass = $imageClass;
    }
    
    public function addMenuItem($mi){
        if ($this->menuItemArray == NULL){
            $this->menuItemArray = array();
        }
        array_push($this->menuItemArray, $mi);
    }
}
