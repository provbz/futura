<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TableVue
 *
 * @author Marco
 */
class TableVue {

    public $name;
    public $customContent;
    public $customTbody;
    public $totalRow;
    public $customColumns;
    public $additionalColumns;
    public $queryName;
    public $autoload = true;
    public $excelFileName = "export";
    public $displayTools = true;
    
    public function __construct($name, $queryName) {
        $this->name = $name;
        $this->queryName = $queryName;
    }

    public function draw() {
        ?>
        <div class="table-vue" id="table_<?= $this->name ?>">
            <div class="query-api">
                <div v-if="isPreparingExcel" style="text-align: center;">
                    <div class="loadme-circular">
                        <img v-bind:src="BasePath + '/img/loading.gif'" width="50" /></div>
                    <h5>Generazione file excel</h5>
                    <h5>Attendere...</h5>
                    <h6>Se la richiesta comprende molti dati potrebbero essere necessari svariati minuti.</h6>
                </div>
                <div v-else>
                    <div v-if="displayTools" class="table-tools" style="text-align: right;">
                        <a href="javascript:void(0);" v-bind:class="{'btn':true, 'btn-info': true, 'active': form.displayFilters}" v-if="queryDescriptor != null && !isEmptyObject(queryDescriptor.conditions)" v-on:click="onToggleFilters()" data-toggle="button" aria-pressed="false" autocomplete="off">
                            <i class="fas fa-filter"></i>
                            <span v-if="hasFilters" class="badge badge-danger">!</span>
                        </a>
                        <a v-if="queryDescriptor && queryDescriptor.enableOptions" href="javascript:void(0);" v-bind:class="{'btn':true, 'btn-info': true, 'active': form.displaySettings}" v-on:click="onToggleSettings()" data-toggle="button" aria-pressed="false" autocomplete="off">
                            <i class="fas fa-cogs"></i>
                        </a>
                        <a v-if="queryDescriptor && queryDescriptor.excelExportEnabled" href="javascript:void(0);" v-bind:class="{'btn':true, 'btn-info': true, 'active': form.displayExportExcel}" v-on:click="onToggleExcelExport()" data-toggle="button" aria-pressed="false" autocomplete="off">
                            <i class="fas fa-file-excel"></i>
                        </a>
                    </div>

                    <form class="query-form card" v-if="displayTools && queryDescriptor != null && queryDescriptor.conditions" v-on:submit.prevent>
                        <div class="card-body">
                            <div class="form-group row">
                                <template v-for="(filter, filterName) in queryDescriptor.conditions" v-if="form.displayFilters && filter.displayAsTableFilter">
                                    <label class="col-xs-4 col-sm-2 col-xl-1 col-form-label">{{filter.label != null ? filter.label : filterName}}:</label>
                                    <div class="xol-xs-8 col-sm-4 col-xl-3">
                                        <div v-if="filter.type == 'List<String>'">
                                            <span v-for="(value, key) in filter.allowedValues">
                                                <input v-model="form[filterName]" :value="key" type="checkbox"/> {{value}}
                                            </span>
                                        </div>
                                        <select v-else-if="filter.allowedValues" v-model="form[filterName]" class="form-control">
                                            <option v-if="filter.addEmptyValue" value=""></option>
                                            <option v-for="(value, key) in filter.allowedValues" v-bind:value="key">
                                                {{value}}
                                            </option>
                                        </select>
                                        <select v-else-if="filter.allowedValuesList" v-model="form[filterName]" class="form-control">
                                            <option v-if="filter.addEmptyValue" value=""></option>
                                            <option v-for="entry in filter.allowedValuesList" :value="entry.Key">
                                                {{entry.Value}}
                                            </option>
                                        </select>
                                        <input v-else-if="filter.type == 'date'" v-bind:data-field-name="filterName" class="form-control datapickerelement" type="text" v-model="form[filterName]" v-datepicker />
                                        <my-input-autocomplete-2 v-else-if="filter.type == 'autocompleter'" v-model="form[filterName]" v-bind:query-name="filter.QueryName"></my-input-autocomplete-2>
                                        <input v-else v-model="form[filterName]" class="form-control" type="text" />
                                    </div>
                                    <div class="note">
                                        {{filter.note}}
                                    </div>
                                </template>
                            </div>

                            <div v-if="!loading && form.displayFilters" class="form-group text-right">
                                <button v-on:click="doSearch" class="btn btn-primary">
                                    <i class="fas fa-search"></i>Search
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="query-results">
                        <div v-if="hasErrors">
                            <h5>Attenzione</h5>
                            {{errors}}
                        </div>

                        <div v-if="loading" style="text-align: center; margin-top: 5px;">
                            <div class="loadme-circular">
                                <img v-bind:src="BasePath + '/img/loading.gif'" width="50" />
                            </div>
                        </div>

                        <div v-if="!loading && !hasErrors" class="table-responsive">
                            <?php if (StringUtils::isNotBlank($this->customContent)) { ?>
                                <?= $this->customContent ?>
                            <?php } else { ?>
                            <table class="table displaytable table-striped table-sm table-hover">
                                <thead>
                                    <tr>
                                        <th v-for="(column, columnId) in columns" v-if="!requestColumns || requestColumns.indexOf(columnId) != -1" scope="col">
                                            <a v-if="column.enableSorting" v-on:click="updateOrders(columnId)" class="sortable">{{column.label}}
                                                <span v-if="form.orders[columnId]">
                                                    <i v-if="form.orders[columnId] == 'ASC'" class="fas fa-sort-alpha-up"></i>
                                                    <i v-else-if="form.orders[columnId] == 'DESC'" class="fas fa-sort-alpha-down"></i>
                                                    {{getOrderIndex(columnId)}}
                                                </span>
                                                <i v-else class="fa fa-sort"></i>
                                            </a>
                                            <span v-else>{{column.label}}</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (StringUtils::isNotBlank($this->customTbody)){ ?>
                                        <?= $this->customTbody ?>
                                    <?php } else { ?>
                                    <tr v-for="content in contents">
                                        <?php if(StringUtils::isNotBlank(CustomColumns)) { ?>
                                            <?= $this->customColumns ?>
                                        <?php } else { ?>
                                            <td v-for="(column, columnId) in columns" v-if="!requestColumns || requestColumns.indexOf(columnId) != -1" v-html="format(content, columnId)"></td>
                                            <?= $this->additionalColumns ?>
                                        <?php } ?>
                                    </tr>
                                    <?php } ?>
                                    <?= $this->totalRow ?>
                                </tbody>
                            </table>
                            <?php } ?>
                        </div>
                    </div>

                    <transition name="fade" mode="out-in">
                        <div class="table-right-panel" v-show="form.displaySettings || form.displayExportExcel">
                            <div v-show="form.displaySettings" class="request-fields card">
                                <div class="card-body">
                                    <div class="margin" style="margin: 30px 0px 20px 0px;">
                                        <h5>Auto reload</h5>
                                        <div v-if="displayPageSizeSelector">
                                            Reload automatico dei dati: 
                                            <select v-model="form.autoReload" v-on:change="openPage(0)">
                                                <option v-for="value in autoReloadValues" v-bind:value="value.value" >
                                                    {{value.text}}
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="margin" style="margin: 30px 0px 20px 0px;">
                                        <h5>Impostazioni</h5>
                                        <div v-if="displayPageSizeSelector">
                                            Risultati per pagina: 
                                            <select v-model="form.rowPerPage" v-on:change="openPage(0)">
                                                <option v-for="value in pageSizeSelectorValues" v-bind:value="value.value" >
                                                    {{value.text}}
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                    <h5>Colonne visualizzate</h5>
                                    <div v-for="(column, columnName) in columns" class="form-group">
                                        <label>
                                            <input v-model="requestColumns" v-bind:value="columnName" type="checkbox"/> {{column.label}}
                                        </label>
                                    </div>
                                    <div class="form-group text-right">
                                        <button v-on:click="doSearch" class="btn btn-primary">
                                            <i class="fas fa-sync-alt"></i>
                                            Aggiorna
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div v-show="form.displayExportExcel" class="request-fields card">
                                <div class="card-body">
                                    <div class="margin" style="margin: 30px 0px 20px 0px;">
                                        <h5>Esporta Excel</h5>
                                        <div>
                                            <div class="scroll-container">
                                                <div v-for="(column, columnName) in excelColumns" class="form-group">
                                                    <label>
                                                        <input v-model="requestExcelColumns" v-bind:value="columnName" type="checkbox"/> {{column.label}}
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group text-right">
                                                <button v-on:click="doExcelExport" class="btn btn-primary">
                                                    <i class="fas fa-download"></i>
                                                    Scarica
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>

                    <div v-if="!loading && !hasErrors" class="query-paginatorr row">
                        <div v-if="paginatorType == 'PAGINATOR'" class="col-6">
                            <ul class="pager pagination">
                                <li class="page-item" v-if="paginator.hasPreview" v-on:click="openPage(form.pageNum - 1)">
                                    <span class="page-link">« </span>
                                </li>
                                <li class="page-item" v-for="page in paginator.pages" v-bind:class="{'on active': page == form.pageNum}" v-on:click="onClickPage(page)">
                                    <span class="page-link">{{page + 1}}</span>
                                </li>
                                <li class="page-item" v-if="paginator.hasNext" v-on:click="openPage(form.pageNum + 1)">
                                    <span class="page-link">» </span>
                                </li>
                            </ul>
                        </div>
                        <div v-if="paginatorType == 'MORE_RESULTS' && hasMoreResults" class="more-results">
                            <a v-on:click="openPage(form.pageNum + 1)">Show more</a>
                        </div>

                        <div class="col-12">
                            <span v-if="paginatorType == 'PAGINATOR'">Pagine: {{paginator.totalPages}}, Risultati: {{paginator.totalResults}},
                            </span>
                        </div>

                        <div v-if="queryDescriptor && queryDescriptor.excelEnableExport">
                            <a class="btn" v-on:click="downloadExcel">
                                <i class="fa fa-file-excel-o"></i>
                                Export
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            var vueComponents = vueComponents || {};
            vueComponents['<?= $this->name ?>'] = new QueryApi({
                autoload: <?= $this->autoload ?>,
                selector: "#table_<?= $this->name ?>",
                queryName: "<?= $this->queryName ?>",
                rowPerPage: 20,
                excelFileName: "<?= $this->excelFileName ?>",
                displayTools: <?= $this->displayTools ?>
            });
        </script>
        <?php
    }
}
