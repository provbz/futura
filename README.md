Futura
======

Il repository contiene il progetto Futura, piattaforma sviluppata per la Provincia Autonoma di Bolzano.


Funzionalità
------------
Le funzionalità principali del progetto sono:
* Supporto alla stesura di PEI (Piano Educativo Individualizzato) e PDP (Piano Didattico Personalizzato).

* Gestione del Portfolio Docenti in anno di prova.

* Gestione delle comunicazioni relative ai progetti di Educazione alla Salute.

* Gestione segnalazione Modello E

* Gestione Drop Out

* Progetto Mondo delle parole e Letto-Scrittura: caricamento dei dai dei test, valutazione e analisi dei dati

* Gestione refereinti di plessi e istituti

* Sistema di messaggistica interna


Autori
------
Marco Buccio <info@mbuccio.it>


Licenza
-------
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
The program FUTURA is free software: you can redistribute it and/or modify it under the terms of 
the **Affero GNU General Public License** as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the Affero GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>.
