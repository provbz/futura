module.exports = {
    outputDir: '../public-html/script/mb/vue-components/',
    configureWebpack: config => {
        if (process.env.NODE_ENV === 'production') {
            config.devtool = 'source-map';
        } else {
            config.devtool = 'eval-source-map';
        }
    },
    devServer: {
        allowedHosts: [
            'futurabolzano.mbuccio.local'
        ],
        disableHostCheck: true,
        headers: {
            'Access-Control-Allow-Origin': 'http://futurabolzano.mbuccio.local'
        }
    },
    filenameHashing: true,
    productionSourceMap: false,
    publicPath: process.env.NODE_ENV === 'production' ? '/script/mb/vue-components/' : 'http://localhost:8080/'
}