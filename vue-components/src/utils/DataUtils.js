/**
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 

@author Marco Buccio <info@mbuccio.it> 
**/
export default{
	encodeData(value){
		if (!value){
			return null;
		}
		
		var encoded = JSON.stringify(value);
		encoded = encoded.trim();
		encoded = encodeURI(encoded);
		return encoded;
	},
	decodeData(encoded, def){
		var value = null;
		try {
			value = decodeURI(encoded);
			
			while (value[value.length - 1] == ",") {
				value = value.substr(0, value.length - 1);
			}
	
			value = JSON.parse(value);
		} catch (ex) { }

		if (!value || typeof value == 'undefined' || value == null || value == "" || value == "undefined") {
			return def;
		}

		return value;
	},
	MBPutData(selector, data) {
		var encoded = this.encodeData(data);
		$(selector).val(encoded);
	},
	MBGetData(selector, def) {
		var encoded = $(selector).val();
		return this.DecodeData(encoded);
	}
	
}