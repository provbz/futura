/**
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 

@author Marco Buccio <info@mbuccio.it> 
**/

import Vue from 'vue'

import '../node_modules/vue-multiselect/dist/vue-multiselect.min.css';
import '../public/style.css';

import Date from "@/filters/Date.js"
import DateTime from "@/filters/DateTime.js"
import PrettyBytes from "@/filters/PrettyBytes.js"
import MbTable from "@/components/ui/table/MbTable.vue"
import { Widgets } from './widgets';

window.vueComponents = window.vueComponents || {}

Vue.config.productionTip = false

document.addEventListener("DOMContentLoaded", function() {
  Object.keys(Widgets).forEach(name => {
    var els = document.getElementsByClassName(name)

    if (els.length == 0) {
      return
    }

    Array.prototype.forEach.call(els, el => {
      let comp = Widgets[name]
      if (isFunction(comp)){
        comp().then((res) => {
          var vi = new Vue({
            render: h => h(res.default)
          }).$mount(el)
        })
      } else {
        var vi = new Vue({
          render: h => h(comp)
        }).$mount(el)
      }
    });
  });
});

window.MbCreateVue = function(widgetName, selector, parameters){
  var els = document.getElementsByClassName(selector);
  if (els.length == 0){
    return;
  }

  var el = els[0];

  var comp = Widgets[widgetName];
  if (!comp){
    console.log("Component not found " + widgetName)
  } else if (!isFunction(comp)){
    addComponent(selector, el, comp, parameters)
  } else {
    comp().then((res) => {
      addComponent(selector, el, res.default, parameters)
    }).catch((error) => {
      console.log("Error loading component: " + error)
    })
  }
}

function addComponent(selector, el, cmp, parameters){
  var ComponentClass = Vue.extend(cmp)
  var instance = new ComponentClass();
  for(var p in parameters){
    instance[p] = parameters[p];
  }
  instance.$mount();
  el.appendChild(instance.$el);

  window.vueComponents[selector] = instance;
}

function isFunction(functionToCheck) {
  return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

const Components = {
  MbTable
};

Object.keys(Components).forEach(name => {
  Vue.component(name, Components[name]);

  if (window.Vue){
    window.Vue.component(name, Components[name]);
  }
});

const Filters = {
  Date,
  DateTime,
  PrettyBytes
};

Object.keys(Filters).forEach(name => {
  Vue.filter(name, Filters[name]);

  if (window.Vue){
    window.Vue.filter(name, Filters[name]);
  }
});