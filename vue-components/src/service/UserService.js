export default{
    canUserAccessUrl(url){
        if (typeof window.currentUserAlloedActions === undefined){
            return false
        }

        for(let i in window.currentUserAlloedActions){
            let re = new RegExp(window.currentUserAlloedActions[i].trim(), "i")
            if (re.test(url)){
                return true
            }
        }

        return false
    }
}