import UserService from "@/service/UserService"

export default{
    data(){
        return {
            SOMMINISTRAZIONE_REFERENTI_PLESSO_ADD: "somministrazione_referenti_plesso_add",
            SOMMINISTRAZIONE_INSEGNANTI_CLASSE_ADD: "somministrazione_insegnanti_classe_add",
            SOMMINISTRAZIONE_CLASSE_ADD: "somministrazione_classe_add",
            SOMMINISTRAZIONE_CLASSE_REMOVE: "somministrazione_classe_remove",
            SOMMINISTRAZIONE_STUDENTE_ADD: "somministrazione_studente_add",
            SOMMINISTRAZIONE_STUDENTE_REMOVE: "somministrazione_studente_remove",
            SOMMINISTRAZIONE_STUDENTE_TRANSFER: "somministrazione_studente_transfer",
            SOMMINISTRAZIONE_CARICAMENTO_RISULTATI: "somministrazione_caricamento_risultati"
        }
    },
    methods: {
        canUserAccessUrl(url){
            return UserService.canUserAccessUrl(url)
        }
    }
}