export default{
	storePut: function(key, value) {
		value = JSON.stringify(value);

		if (typeof Storage !== "undefined") {
			localStorage.setItem(key, value);
		} else {
			// Storing in cookies
		}
	},
	storeGet: function(key, defaultValue) {
		var value = null;

		if (typeof Storage !== "undefined") {
			value = localStorage[key];
		}

		try {
			value = JSON.parse(value);
		} catch (e) {
			value = defaultValue;
		}

		return value;
	}
}