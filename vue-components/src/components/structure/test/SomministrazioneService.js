import Axios from "axios"
import SomministrazioneStatus from "./SomministrazioneStatus"

const baseUrl = "/structure/test/SomministrazioneApi-"

export default {
    addUserToStructureReferent(userId, structureId){
        return Axios.post(baseUrl + 'addUserToStructureReferent', {
            userId: userId,
            structureId: structureId
        })
    },
    removeUserFromStructureReferent(userId, structureId){
        return Axios.post(baseUrl + 'removeUserFromStructureReferent', {
            userId: userId,
            structureId: structureId
        })
    },
    addUserToStructureClassTeacher(userId, structureClassId){
        return Axios.post(baseUrl + 'addUserToStructureClassTeacher', {
            userId: userId,
            structureClassId: structureClassId
        })
    },
    removeUserFromStructureClassTeacher(userId, structureClassId){
        return Axios.post(baseUrl + 'removeUserFromStructureClassTeacher', {
            userId: userId,
            structureClassId: structureClassId
        })
    },
    validate(results){
        for (let i = 0; i < results.students.length; i++){
            let student = results.students[i]
            if (student.status == SomministrazioneStatus.SOMMINISTRATA){
                for(let rdi in student.raw_data){
                    if (typeof student.raw_data[rdi] == undefined || student.raw_data[rdi] === null || student.raw_data[rdi] === ''){
                        alert("Inserire i valori nelle celle con il simbolo di allerta!")
                        return false
                    }
                }
            }
        }

        return true
    }
}