/**
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 

@author Marco Buccio <info@mbuccio.it> 
**/
import TestVariableType from "./TestVariableType"

export default{

    evaluate(soglia, score){
        if (typeof soglia == 'undefined' || soglia === null){
            return null
        }

        if (typeof score !== 'number' || score == null){
            return null
        }

        if (soglia.type == TestVariableType.RIGHT){
            if (typeof soglia.s1 == 'number' && score < soglia.s1){
                return 0
            }
            if (typeof soglia.s1 == 'number' && typeof soglia.s2 == 'number' && score >= soglia.s1 && score < soglia.s2){
                return 1
            }
            if (typeof soglia.s2 == 'number' && score >= soglia.s2){
                return 2
            }
        } else {
            if (typeof soglia.s1 == 'number' && score < soglia.s1){
                return 2
            }
            if (typeof soglia.s1 == 'number' && typeof soglia.s2 == 'number' && score >= soglia.s1 && score < soglia.s2){
                return 1
            }
            if (typeof soglia.s2 == 'number' && score >= soglia.s2){
                return 0
            }
        }
    }

}