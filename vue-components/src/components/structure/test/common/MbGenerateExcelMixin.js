import axios from "axios"

export default{
    methods: {
        generateExcel(data){
            axios({
                url: "/public/api/xlsx/JsonToXlsxAction",
                method: 'POST',
                responseType: 'blob',
                data: data
            }).then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data]))
                const link = document.createElement('a')
                link.href = url
                link.setAttribute('download', 'report.xls')
                document.body.appendChild(link)
                link.click()
            })
        }
    }
}