/**
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 

@author Marco Buccio <info@mbuccio.it> 
**/

import Axios from 'axios'

export default{
    props: {
        data: {
            type: Array,
            default(){
                return []
            }
        },
        note: {
            type: String,
            default: ''
        },
        parameters: {
            type: Object,
            default(){
                return {}
            }
        },
        soglie: {
            type: Object,
            default() {
                return {}
            }
        },
        readOnly: {
            type: Boolean,
            default: false
        },
        backAction: {
            type: String,
            default: ''
        },
        submitAction: {
            type: String,
            default: ''
        },
        noteCompilazione: {
            type:String
        },
        bag: {
            type: Object
        }
    },
    data(){
        return {
            isLoading: false,
            errors: [],
            results: {
                note: '',
                students: []
            }
        }
    },
    computed: {
        displayAlert(){
            if (this.isReadOnly){
                return false
            }

            if (!this.data){
                return false
            }

            for (let row of this.data){
                if (!row.status){
                    return true
                }
            }

            return false
        }
    },
    watch: {
        note(nv){
            this.results.note = nv
        },
        data(nv){
            this.results.students = nv
        }
    },
    methods: {
        validateData(){
            return true
        },
        onClickSubmit(){
            if (!this.validateData()){
                return
            }
            
            this.isLoading = true

            Axios.post(this.submitAction, this.results)
                .then((response) => {
                    this.isLoading = false
                    if (!response.data.success){
                        this.errors = response.data.errors
                    }
                }).catch((errorResponse) => {
                    this.isLoading = false
                    this.errors = [
                        errorResponse
                    ]
                })
        },
        generateExcel(data){
            Axios({
                url: "/public/api/xlsx/JsonToXlsxAction",
                method: 'POST',
                responseType: 'blob',
                data: data
            }).then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data]))
                const link = document.createElement('a')
                link.href = url
                link.setAttribute('download', 'report.xls')
                document.body.appendChild(link)
                link.click()
            })
        }
    }
}