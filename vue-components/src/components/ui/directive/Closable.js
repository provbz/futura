/**
----------------------------------------------------------------------------------------- 
This file is part of the application Futura  
 
Copyright (c) 2019 Provincia Autonoma di Bolzano (http://www.provincia.bz.it/). 
 
This program is free software: you can redistribute it and/or modify it under the terms of 
the Affero GNU General Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version. 
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the Affero GNU General Public License for more details. 
 
You should have received a copy of the GNU General Public License along with this program.  
If not, see <http://www.gnu.org/licenses/>. 
----------------------------------------------------------------------------------------- 

@author Marco Buccio <info@mbuccio.it> 
**/

let handleOutsideClick;

export default{
	bind (el, binding, vnode) {
		handleOutsideClick = function(e) {
			e.stopPropagation()
			const { handler, exclude } = binding.value
			
			let clickedOnExcludedEl = false
			if (exclude){
				exclude.forEach(refName => {
					if (!clickedOnExcludedEl) {
						const excludedEl = vnode.context.$refs[refName]
						if (excludedEl){
							clickedOnExcludedEl = excludedEl.contains(e.target)
						}
					}
				});
			}

			if (!el.contains(e.target) && !clickedOnExcludedEl) {
				vnode.context[handler]()
			}
		};

		// Register click/touchstart event listeners on the whole page
		document.addEventListener('click', handleOutsideClick);
		document.addEventListener('touchstart', handleOutsideClick);
	},
	unbind () {
		// If the element that has v-closable is removed, then
		// unbind click/touchstart listeners from the whole page
		document.removeEventListener('click', handleOutsideClick)
		document.removeEventListener('touchstart', handleOutsideClick)
	}
}