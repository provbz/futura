import MbRightPanel from "@/components/ui/layout/MbRightPanel";
import MbInputSelect2 from "@/components/ui/base/MbInputSelect2.vue";
import MbInputStructureRef from "@/components/structure/user-ref/MbInputStructureRef.vue";
import MbTable from "@/components/ui/table/MbTable.vue";
import MbInactiveUserVue from "./components/MbInactiveUser.vue";

export const Widgets = {
		"mb-right-panel": MbRightPanel,
		"mb-table": MbTable,
		"mb-input-select-2": MbInputSelect2,
		"mb-input-structure-ref": MbInputStructureRef,
		"mb-inactive-user": MbInactiveUserVue,
		"mb-user-ic-table": () =>
			import(
				/* webpackChunkName: "mb-user-ic-table" */ "@/components/admin/user/MbUserIcTable.vue"
			),
		"mb-admin-pei-table": () =>
			import(
				/* webpackChunkName: "mb-admin-pei-table" */ "@/components/admin/pei/MbPeiTable.vue"
			),
		"mb-test-admin-table": () =>
			import(
				/* webpackChunkName: "mb-test-admin" */ "@/components/admin/test/MbTestAdminTable"
			),
		"mb-test-soglia-admin-table": () =>
			import(
				/* webpackChunkName: "mb-test-admin" */ "@/components/admin/test/MbTestSogliaAdminTable"
			),
		"mb-test-stats-admin-page": () =>
			import(
				/* webpackChunkName: "mb-test-admin" */ "@/components/admin/test/MbTestStatsAdminPage"
			),
		"mb-elenco-studenti-admin-table": () =>
			import(
				/* webpackChunkName: "mb-test-admin" */ "@/components/admin/test/MbElencoStudentiAdminTable"
			),
		"mb-somministrazione-admin-table": () =>
			import(
				/* webpackChunkName: "mb-test-admin" */ "@/components/admin/test/MbSomministrazioneAdminTable"
			),
		"mb-somministrazione-stats-admin-page": () =>
			import(
				/* webpackChunkName: "mb-test-admin" */ "@/components/admin/test/MbSomministrazioneStatsAdminPage"
			),
		"mb-view-test-analyses-admin-page": () =>
			import(
				/* webpackChunkName: "mb-test-admin" */ "@/components/admin/test/MbViewTestAnalysiesAdminPage"
			),
		"mb-somministrazione-table": () =>
			import(
				/* webpackChunkName: "mb-somministrazione-table" */ "@/components/structure/test/MbSomministrazioneTable"
			),
		"mb-somministrazione-structure": () =>
			import(
				/* webpackChunkName: "mb-somministrazione-structure" */ "@/components/structure/test/MbSomministrazioneStructure"
			),
		"mb-somministrazione-class": () =>
			import(
				/* webpackChunkName: "mb-somministrazione-class" */ "@/components/structure/test/MbSomministrazioneClass"
			),
		"mb-role-table": () =>
			import(
				/* webpackChunkName: "mb-role-table" */ "@/components/admin/user/MbRoleTable"
			),
		"mb-test-DETTATO_1P": () =>
			import(
				/* webpackChunkName: "test-dettato-1p" */ "./components/structure/test/DETTATO_1P/MbTest"
			),
		"mb-test-DETTATO_2P": () =>
			import(
				/* webpackChunkName: "test-dettato-2p" */ "./components/structure/test/DETTATO_2P/MbTest"
			),
		"mb-test-COMPRENSIONE_1P": () =>
			import(
				/* webpackChunkName: "test-comprensione-1p" */ "./components/structure/test/COMPRENSIONE_1P/MbTest"
			),
		"mb-test-COMPRENSIONE_2P": () =>
			import(
				/* webpackChunkName: "test-comprensione-2p" */ "./components/structure/test/COMPRENSIONE_2P/MbTest"
			),
		"mb-test-NARRAZIONE_3I": () =>
			import(
				/* webpackChunkName: "test-narrazione-3i" */ "./components/structure/test/NARRAZIONE_3I/MbTest"
			),
		"mb-test-METAFONOLOGIA_3I": () =>
			import(
				/* webpackChunkName: "test-narrazione-3i" */ "./components/structure/test/METAFONOLOGIA_3I/MbTest"
			),
		"mb-test-COPIA_3I": () =>
			import(
				/* webpackChunkName: "test-copia-3i" */ "./components/structure/test/COPIA_3I/MbTest"
			),
		"mb-test-COPIA_3I_F": () =>
			import(
				/* webpackChunkName: "test-copia-3i_f" */ "./components/structure/test/COPIA_3I_F/MbTest"
			),
		"mb-test-MEMORIA_VISIVA_3I": () =>
			import(
				/* webpackChunkName: "test-memoria-visiva-3i" */ "./components/structure/test/MEMORIA_VISIVA_3I/MbTest"
			),
		"mb-test-MEMORIA_VERBALE_3I": () =>
			import(
				/* webpackChunkName: "test-memoria-verbale-3i" */ "./components/structure/test/MEMORIA_VERBALE_3I/MbTest"
			),
		"mb-test-NUMERO_3I": () =>
			import(
				/* webpackChunkName: "test-numero-3i" */ "./components/structure/test/NUMERO_3I/MbTest"
			),
		"mb-scheduler-admin-page": () =>
			import(
				/* webpackChunkName: "mb-scheduler-admin-page" */ "@/page/admin/MbSchedulerAdminPage"
			),
		"mb-somministrazione-stats-table": () =>
			import(
				/* webpackChunkName: "mb-somministrazione-stats-table" */ "@/components/structure/test/MbSomministrazioneStatsTable"
			),
		"mb-somministrazione-class-stats-table": () =>
			import(
				/* webpackChunkName: "mb-somministrazione-class-stats-table" */ "@/components/structure/test/MbSomministrazioneClassStatsTable"
			),
		"mb-alfa-table-admin": () =>
			import(
				/* webpackChunkName: "mb-alfa-table-admin" */ "@/components/admin/test/MbAlfaTableAdmin"
			),
		"mb-alfa-child-admin": () =>
			import(
				/* webpackChunkName: "mb-alfa-table-admin" */ "@/components/admin/test/MbAlfaChildAdmin"
			),
		"mb-esis-table-admin": () =>
			import(
				/* webpackChunkName: "mb-esis-table-admin" */ "@/components/admin/test/MbEsisTableAdmin"
			),
		"mb-esis-child-admin": () =>
			import(
				/* webpackChunkName: "mb-esis-table-admin" */ "@/components/admin/test/MbEsisChildAdmin"
			),
		"mb-e-model-codes-field": () =>
			import(
				/* webpackChunkName: "mb-e-model-admin" */ "@/components/structure/e_model/MbCodesField"
			),
		"mb-e-model-admin-table": () =>
			import(
				/* webpackChunkName: "mb-e-model-admin" */ "@/components/admin/e_model/MbEModelAdminTable"
			),
		"mb-e-model-manage-codes-admin-table": () =>
			import(
				/* webpackChunkName: "mb-e-model-admin" */ "@/components/admin/e_model/MbEModelManageCodesAdminTable"
			),
		"mb-e-model-close-admin": () =>
			import(
				/* webpackChunkName: "mb-e-model-admin" */ "@/components/admin/e_model/MbEModelCloseAdmin"
			),
		"mb-e-model-edited-admin-table": () =>
			import(
				/* webpackChunkName: "mb-e-model-admin" */ "@/components/admin/e_model/MbEModelEditedAdminTable"
			),
		MbEModelJournalTable: () =>
			import(
				/* webpackChunkName: "mb-e-model-admin" */ "@/components/admin/e_model/MbEModelJournalTable"
			),
		"mb-drop-out-admin-table": () =>
			import(
				/* webpackChunkName: "mb-drop-out-admin" */ "@/components/admin/drop-out/MbDropOutAdminTable"
			),
		"mb-drop-out-admin-pivot": () =>
			import(
				/* webpackChunkName: "mb-drop-out-admin" */ "@/components/admin/drop-out/MbDropOutAdminPivot"
			),
		"mb-drop-out-referenti-admin-table": () =>
			import(
				/* webpackChunkName: "mb-drop-out-admin" */ "@/components/admin/drop-out/MbDropOutReferentiAdminTable"
			),
		"mb-structure-user-ref-table": () =>
			import(
				/* webpackChunkName: "mb-structure-user-ref-table" */ "@/components/structure/user-ref/MbStructureUserRefTable"
			),
		"mb-structure-pei-pdp-table": () =>
			import(
				/* webpackChunkName: "mb-structure-pei-pdp" */ "@/components/structure/pei/MbStructurePeiPdpTable"
			),
		"mb-structure-pei-file-stats": () =>
			import(
				/* webpackChunkName: "mb-structure-pei-pdp" */ "@/components/structure/pei/MbStructurePeiFileStats"
			),
		"mb-structure-recupera-documenti-table": () =>
			import(
				/* webpackChunkName: "mb-structure-pei-pdp" */ "@/components/structure/pei/MbStructureRecuperaDocumentiTable"
			),
		"mb-admin-user-transfer-table": () =>
			import(
				/* webpackChunkName: "mb-admin-user-transfer" */ "@/components/admin/user/MbAdminUserTrasferTable"
			),
		"mb-admin-user-table": () =>
			import(
				/* webpackChunkName: "mb-admin-user" */ "@/components/admin/user/MbAdminUserTable"
			),
		"mb-admin-user-access-table": () =>
			import(
				/* webpackChunkName: "mb-admin-user" */ "@/components/admin/user/MbAdminUserAccessTable"
			),
		"mb-structure-drop-out-table": () =>
			import(
				/* webpackChunkName: "mb-structure-drop-out" */ "@/components/structure/drop-out/MbStructureDropOutTable"
			),
		"mb-structure-e-model-table": () =>
			import(
				/* webpackChunkName: "mb-structure-e-model" */ "@/components/structure/e_model/MbStructureEModelTable"
			),
		MbEModelForm: () =>
			import(
				/* webpackChunkName: "mb-structure-e-model" */ "@/components/structure/e_model/MbEModelForm"
			),
		MbPeiTargetStrategieField: () =>
			import(
				/* webpackChunkName: "mb-structure-pei" */ "@/components/structure/pei/MbPeiTargetStrategieField"
			),
		MbPeiModalitaVerificaField: () =>
			import(
				/* webpackChunkName: "mb-structure-pei" */ "@/components/structure/pei/MbPeiModalitaVerificaField"
			),
		"mb-admin-internal-message-table": () =>
			import(
				/* webpackChunkName: "mb-internal-message" */ "@/components/admin/internal_message/MbAdminInternalMessageTable"
			),
		"mb-structure-select-field": () =>
			import(
				/* webpackChunkName: "mb-ui" */ "@/components/MbStructureSelectField"
			),
		MbStructureUserRef: () =>
			import(
				/* webpackChunkName: "mb-user-referent" */ "@/components/structure/referent/MbStructureUserRef"
			),
		MbStructureAdminTable: () =>
			import(
				/* webpackChunkName: "mb-user-referent" */ "@/components/admin/structure/MbStructureAdminTable"
			),
		MbPeiTirocinioProgetto: () =>
			import(
				/* webpackChunkName: "mb-pei-tirocinio" */ "@/components/structure/pei_tirocinio/MbPeiTirocinioProgetto"
			),
		MbPeiTirocinioAzienda: () =>
			import(
				/* webpackChunkName: "mb-pei-tirocinio" */ "@/components/structure/pei_tirocinio/MbPeiTirocinioAzienda"
			),
		MbPdpLinguaForm: () =>
			import(
				/* webpackChunkName: "mb-pei-tirocinio" */ "@/components/structure/pdp_lingua/MbPdpLinguaForm"
			),
		MbPeiRestoreNode: () =>
			import(
				/* webpackChunkName: "mb-structure-pei" */ "@/components/structure/pei/MbPeiRestoreNode"
			),
		MbStructureRilevazioneCattedreTable: () =>
			import(
				/* webpackChunkName: "mb-structure-pei" */ "@/components/structure/rilevazione_cattedre/MbStructureRilevazioneCattedreTable"
			),
		MbAdminRilevazioneCattedreTable: () =>
			import(
				/* webpackChunkName: "mb-structure-pei" */ "@/components/admin/rilevazione_cattedre/MbAdminRilevazioneCattedreTable"
			),
		"mb-admin-pei-restore-table": () =>
			import(
				/* webpackChunkName: "mb-admin-pei" */ "@/components/admin/pei/MbAdminPeiRestoreTable"
			)
}
